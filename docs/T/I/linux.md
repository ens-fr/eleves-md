# Pour commencer on va parler Linux

## Linux

!!! d "Sa création"
    **Linux** ou **GNU/Linux** est une famille de systèmes **d'exploitation open source** de `type Unix` fondé sur le noyau Linux, créé en 1991 par _Linus Torvalds_.
    
    ??? linus "C'est lui"
        <center>![Linus Torvalds](https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/LinuxCon_Europe_Linus_Torvalds_03_%28cropped%29.jpg/800px-LinuxCon_Europe_Linus_Torvalds_03_%28cropped%29.jpg){ width="300" }</center>
    
    **Linus Torvalds** est lui très connu pour avoir crée le `noyau Linux` est un informaticien américano-finlandais.
    Cet homme est le fondateur de Linux et a permis grace a son système d'exploitation d'ouvrir de nombreuses distributions Linux et constituent un important vecteur de popularisation du mouvement du logiciel libre. Si à l'origine, Linux a été développé pour les ordinateurs compatibles PC, il n'a jamais équipé qu'une très faible part des ordinateurs personnels. ![Tux](https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/800px-Tux.svg.png){ width="15" }
    
    
    
    C'est bel et bien ce pingouin qui est est un des piliers du numérique et qui est une divinité pour certain comme on le veras par la suite.

    ??? gnu "Le projet GNU"

        <center>![GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Heckert_GNU_white.svg/langfr-1024px-Heckert_GNU_white.svg.png){ width="300"}</center>
    
    
        Il faut également savoir que le `noyau Linux` est accompagné des logiciels **GNU**.
        Le projet GNU étant un projet informatique pour développer le système d’exploitation GNU. Le projet est maintenu par une communauté de _hackers_ organisée en sous-projets.
        Chaque brique du projet est un logiciel libre utilisable de par sa nature dans des projets tiers, mais dont la finalité est de s’inscrire dans une logique _cohérente_.

## Type Unix

!!! tunix "Le Type Unix"

    Un système d’exploitation de **type Unix** (en anglais : _Unix-like_) est un système d’exploitation qui se comporte d’une façon semblable à un système Unix, bien que n’étant pas nécessairement conforme ou certifié par une quelconque version de la Single UNIX specification.

    ![TUnix](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Unix_timeline.en.svg/1280px-Unix_timeline.en.svg.png){ width="600"}


    Le système UNIX est multi-utilisateur et multitâche, il a été construit pour être sûr, vis-à-vis des manipulations illicites des utilisateurs pour accéder à des données d'un autre utilisateur ou vis-à-vis des erreurs lors de l'exécution d'un programme.
    On vois ici très vite que Linux est un système d'exploitation très sûr.

## Origine de création

!!! og "D'où vient donc son idée ?"

    A la base l'idée de Linus Torvalds de créer Linux est venue en 1991, quand l’étudiant finlandais, indisposé par la faible disponibilité du serveur informatique UNIX de l’université d'Helsinki, entreprend le développement d’un noyau de système d'exploitation, qui prendra le nom de « noyau Linux ».

    Linus Torvalds utilisait et appréciait Minix. Le 25 août 1991, il annonce sur le forum Usenet comp.os.minix le développement du noyau Linux.

    ??? unix "Unix"

        Unix, officiellement UNIX, est une famille de systèmes d'exploitation multitâche et multi-utilisateur dérivé du Unix d'origine créé par AT&T, le développement de ce dernier ayant commencé dans les années 1970 au centre de recherche de Bell Labs mené par Kenneth Thompson. Il repose sur un interpréteur ou superviseur (le shell) et de nombreux petits utilitaires, accomplissant chacun une action spécifique, commutables entre eux (mécanisme de « redirection ») et appelés depuis la ligne de commande.


        ![Unix](https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Unix_history-simple.svg/langfr-1280px-Unix_history-simple.svg.png){ width="800"}

## L'évolution de Linux

!!! evo "Comment Linux a-t'il évoluer ?"
    À l'origine, l'installation d'un système opérationnel GNU/Linux nécessitait des connaissances solides en informatique et obligeait à trouver et installer les logiciels un à un.

    Rapidement, des ensembles de logiciels formant un système complet prêt à l'usage ont été disponibles : ce sont les premières distributions Linux. On peut citer par ordre chronologique :
    
    === "Les deux disquettes"
        Le créateur a tout d'abord commencer par deux disquettes 5,25 pouces boot et root créées par HJ Lu, et contenant un minimum de logiciels. 
    === "MCC Interim Linux"
        Ensuite on a MCC Interim Linux qui est une collection de disquettes sortie en février 1992.
    === "TAMU"
        Après TAMU 1.0A faite par la Texas A&M University pour ses besoins internes, mais distribuée, et première distribution incluant X Window.
    === "Softlanding Linux System"
        Softlanding Linux System sortie en mai 1992, qui sera reprise pour faire Slackware.
    === "Yggdrasil Linux/GNU/X"
        Ensuite on a Yggdrasil Linux/GNU/X sortie en décembre 1992, première distribution avec possibilité de live CD.
    === "Slackware"
        Après Slackware, sortie en juillet 1993 est la plus ancienne distribution encore maintenue.
    === "Debian"
        Et pour finir on le connu de tous qui est Debian, sortie en août 1993, est une des principales distributions actuelles.

    ___
    C’est dans le monde des serveurs informatiques que GNU/Linux a eu le plus d’impact, notamment avec le très populaire LAMP. Sur les serveurs, GNU/Linux a souvent été utilisé pour remplacer d’autres systèmes de type Unix ou éviter l'achat de licences Windows NT et se retrouve être un des acteurs majeurs. Dès 2003, Microsoft semble faire appel lui-même en partie à GNU/Linux9.
    On voit donc une énorme importance de Linux dans le monde informatique et une très grande evolution dans les années 1990, ou l'informatique ne faisait que émerger 
    et cela n'est pas anormal car pour un logiciel libre et gratuit avec également une bonne sécurité cela est mérité et il ne faut pas écouter les personnes comme
    [Richard Stallman](https://www.developpez.com/actu/332740/Richard-Stallman-s-exprime-sur-l-etat-du-mouvement-du-logiciel-libre-et-declare-que-les-Macintosh-continuent-d-etre-des-prisons-pour-les-utilisateurs/#:~:text=Richard%20Stallman%2C%20fondateur%20de%20la,et%20Canonical%20dans%20ces%20propos.)
## Principale distribution linux

| Classement Alexa (en Avril 2020)    | Classement SililarWeb (en Juin 2020)    |
| ----------------------------------- | --------------------------------------- |
| Distribution / Classement           | Distribution / Visites totales          |
| Ubuntu	/ 2 551e                  | Ubuntu / 7.54 millions                  |
| RedHat	/ 4 273e                  | RedHat / 5.50 millions                  |
| Debian / 8 711e	                  | Linux Mint / 3.61 millions              |
| CentOS / 9 366e	                  | ArchLinux / 3.18 millions               |
| KALI Linux / 9 725e	              | Debian	/ 2.68 millions                 |
| Linux Mint / 10 692e	              | Kali Linux / 2.26 millions              |
| Archlinux / 11 515e	              | Manjaro / 1.87 millions                 |
| Manjaro Linux / 21 195e	          | CentOS / 1.69 millions                  |
| Ubuntu Kylin / 24 147e	          | Deepin / 1.44 millions                  |
| KDE neon / 28 472e            	  | OpenSUse / 775 060                      |
| Deepin / 28 600e	                  | Fedora / 751 190                        |
| Opensuse / 34 647e	              | Gentoo / 579 690                        |
| Gentoo Linux / 45 140e	          | RancherOs / 541 690                     |
| Rancher OS / 56 794e	              | FreeBSD / 525 580                       |
| Tails / 63 978e	                  | PopOS! / 482 570                        |

## Les problèmes de Linus de nos jours

!!! pbl "Mais Lunix à quand même quelques problèmes"
    Malgrès toutes ces bonnes chose Linux rencontre de nos jours quelques problème que ce soit en popularité ou même en utilisation,
     en tout cas quand on parle des outils de type ordinateur. 
     
    En effet le système Android est un système Linux mais la popularité de Linux a énormement baisser maintenant 
    avec la plus part des ordinateurs étant sous 
    Windows car Windows ![wind](https://cdn-icons-png.flaticon.com/512/732/732225.png){ width="15"} est dévenu beaucoup plus populaire et plus pratique sur certain point et surtout meilleur pour une personne lambda même si Linux est meilleur pour
    coder par exemple.