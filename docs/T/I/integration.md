# Comment s'intégrer dans cette belle communauté ?

## Pour commencer les problème de s'intégrer
!!! stl "En quoi cela est dur ?"
    Quand vous êtes nouveau dans un groupe quelques fois c'est dur de bien s'intégrer. Les nouveaux arrivant sur `Linux` trouve souvent que c'est dur de s'intégrer avec les utilisateurs de **longue date**.
    ??? e "Un peu comme eux"
        <center>![ex_eux](https://conf.archlinux.org/images/conf/groupphoto.jpg)</center>
    L'utilisateur de Linux semble presque qu'il vient du culture différente, il a de différentes traditions, on dirait même qu'il parle d'une autre langue.
    ??? ex "Voici donc un exemple absolument pas dégradant"
        <center>![ex_utilisateur](images/ex_user.png)</center>
    Alors qu'est ce que les **nouveaux utilisateurs Linux** peuvent faire pour **s'intégrer dans cette magnifique communauté**.

## Tout ce dont vous avez besoin pour les rejoindre
!!! allin "Tout ce qu'il faut que vous fassiez"
    !!! premier "Pour commencer la barbe"
        La toute **première étape** est une étape très importante. Il faut tout d'abord **se laisser pousser** une _barbe du cou_ (en anglais: _neck-beard_), en effet chaque groupe, chaque tribu de personne on des tradition vestimentaire ou corporelle propres a chaque groupe, tout autant que les tatouages sont une partie vitale des *insulaire Pacifique* (en anglais : [_Pacific Islander_](https://en.wikipedia.org/wiki/Pacific_Islander)). Comme pour eux la tradition de la communauté Linux c'est la barbe du cou, pousse la toujours plus longue et porte la avec fierté.
        ??? ex2 "Regarder donc cette personne qui en ait le parfait exemple"
            <center>![barbe du cou](images/ex_barbe_cou.png)</center>
    !!! second "Ensuite ton équipement"
        Ensuite la **deuxième** chose que vous pouvez faire est d'uniquement utilisé de l'équipement très usé. Vous n'allez jamais gagner du **respect dans la communauté Linux** en utilisant du bon équipement, **le plus vieux** et **le plus médiocre** votre ordinateur est, plus cela est **mieux** pour s'intégrer.
        ??? ex3 "Parfait exemple ci-dessous"
            ![ordi_vieux](images/ordi%20vieux.png)
        Le vieux matériel c'est assez standard dans la communauté et la **régle** dans la communauté est que vous ne pouvez pas dépenser `plus de 50€ sur votre ordinateur portable usé`. Et aussitot que vous avez votre ordinateur soyez sur de mettre le plus de **stickers** possible, le plus possible avec des **pingouin** evidement, tout autant que des stickers **GNU** et en toute évidence des stickers de **hacking et de sécurité**.
        ??? ex4 "On peut voir différents exemples parfait ci-dessous"
            ![pingouin](images/stickers_pingouin.png){ width="700"}
            ![gnu/hack](images/sticker-hack-gnu.png){ width="700"}
        
        Et maintenant vous êtes donc prêt à passer à la troisième étape.
    !!! out "Pourquoi sortir dehors ?"
        Maintenant il faut que vous arretiez de sortir a l'extérieur, il n'y a rien pour vous dehors dans tous les cas, tout ce que vous voulez dans votre vie peut être trouvé dans l'invite de commande alors entraîner vous a reconnaitre des chose comme la lumière du soleil et de l'herbe. Et quand vous les voyez fuyez, retour à votre ordinateur, est un lieu sûr.
        ??? ex5 "Représentation plus simple pour votre compréhension"
            <center>![Linux user](images/Linux%20user.png)</center>
    !!! hyg "A quoi sert l'hygiène ?"
        Ici, se raser, se couper les cheuveux et prendre des douches sont mal vu, pourquoi essayez vous d'avoir bonne mine, vous n'allez plus quittez votre maison dans tous les cas, et pourquoi essayez vous de sentir bon, il n'y a que vous dans votre maison/appartement.
    !!! soc "Comment devez-vous vous sociabilisé ?"
        Les relations amoureuses n'est pas quelque chose d'énormement conseillé pour un utilisateur de Linux. En effet le plus conseillé, sont des amis, mais pas n'importe quels amis, vous ne pouvez avoir qui des amis en ligne, ce genre d'amis sont les seuls autoriser.
    !!! vim "Et pour finir utilisé VIM"
        Les utilisateur de longues date de Linux se vante constamment d'a quel point ils sont incroyable cars ils utilise VIM.
        ??? v "Vim"
            <center>![vm](images/vim.png)</center>
        Maintenant je sais ce que vous pensez Vim est difficile a apprendre mais pas de panique vous n'avez pas réellement besoin d'apprendre a utilisé Vim ou même l'utiliser, just dite leurs que vous l'utiliser mais secrétement continuer a utiliser Nano comme une personne normal.
        ??? nano "A quoi ressemble Nano"
            <center>![nano](images/nano.png)</center>

