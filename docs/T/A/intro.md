# La langue finnoise

![Suomen lippu](https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_of_Finland.svg){width=500}



!!! tldr "Introduction"
    Le finnois appartient à la famille des langues Fenniques comme l'estonien. Elle est majoritairement parlée en Finlande,
    pays de l'UE à ouest de la Russie et à l'est de la Scandinavie (Suède, Norvège, Danemark...), ainsi qu'en Suède,
    pays frontalier à la Finlande. Elle est parlée par près de 6 millions de personnes, dont la majorité des locuteurs
    se trouve en Finlande.  

    ![Suomen maa](https://upload.wikimedia.org/wikipedia/commons/6/6e/EU-Finland.svg){width=500}

    À l'image des langues finno-ougriennes (finnois, estonien, hongrois...), la langue est très différente de la plupart
    des autres langues européennes. Son fonctionnement est complexe sur de nombreux points comme la présence d'un grand nombre
    de déclinaisons (+ de 16 cas), sachant qu'il y a beaucoup de personnes qui n'arrive déjà pas à mémoriser les déclinaisons allemandes ou latines.

    !!! tip "Finnois, Estonien, Hongrois"
        Comme dit plus tôt, ces trois langues font partie de la même famille, mais en revanche, là où encore le finnois et l'estonien reste relativement proche au niveau de la langue, le hongrois est quant à lui très différent.

        !!! example "Exemple"

            <strong>La Finlande est un pays froid.</strong>

            - Finnois : Suomi on kylmä maa. <a href="../audios/intro/suomi.ogg">▶️</a>
            - Estonien : Soome on külm maa <a href="../audios/intro/Soome.ogg">▶️</a>
            - Hongrois : Finnország hideg ország <a href="../audios/intro/Finnorszag.ogg">▶️</a>

!!! info "Précision"
    Le but de ce site est de vous introduire à la langue finnoise et à ces particularités, concernant ça phonologie, la grammaire, la syntaxe...

## La phonologie du finnois

!!! tldr "Détails"
    La phonologie, soit les sons présents dans une langue, du finnois est assez simple pour la quasi-totalité des lettres de son alphabet. 
    En effet, cette langue a l'avantage, plus ou moins comme en espagnol, de se prononcer exactement comme elle se lit. Un francophone n'aurait 
    en théorie pas vraiment de difficulté à prononcer la langue bien que certains sons puissent être difficile à maîtriser au début tel que 
    le **"r"** et le **"v"** ou alors la distinction entre le **"a"** et le **"ä"**
    
### Les voyelles

!!! Voyelles 
    Il y a en finnois 8 voyelles : ^^a, o, u, ä, ö, y, e^^ et ^^i^^

    Voici un tableau qui présente la prononciation de ces voyelles avec leurs équivalents en Français :

    !!! info "Audio"
        Vous pouvez cliquer sur les lettres pour entendre leur prononciation.

    | Lettres | phonétique (en [API](https://fr.wikipedia.org/wiki/Alphabet_phon%C3%A9tique_international)) | Prononciation approximative | Exemple en finnois |
    | --------- | --------------------- | ---------------------------- | ----- |
    | <a href="../audios/lettres/voyelles/a.ogg">a</a> | /ɑ/ | Rare mais existe en français avec la lettre "â" comme dans "pâte" | Ja (et) |
    | <a href="../audios/lettres/voyelles/o.ogg">o</a> | /o/ | Se prononce "o" | Olla (être) |
    | <a href="../audios/lettres/voyelles/u.ogg">u</a> | /u/ | Se prononce "ou" | Hullu (fou) |
    | <a href="../audios/lettres/voyelles/a1.ogg">ä</a> | /æ/ | N'existe pas en français mais correspond au "a" de "cat" en Anglais | Minä (je) |
    | <a href="../audios/lettres/voyelles/o1.ogg">ö</a> | /ø/ | comme dans le mot "noeud" | Tyttö (fille) |
    | <a href="../audios/lettres/voyelles/y.ogg">y</a> | /y/ | Comme le "u" français | Kylä (village) |
    | <a href="../audios/lettres/voyelles/e.ogg">e</a> | /e/ | Comme le son "é" | Tehdä (faire) |
    | <a href="../audios/lettres/voyelles/i.ogg">i</a> | /i/ | Se prononce "i" | Nimi (nom) |

    !!! cite "Autre"
        Il existe aussi dans l'alphabet finnois la lettre Å qui est une lettre d'emprunt au suédois. En effet, vu la proximité 
        et les nombreux échanges entre la Suède et la Finlande, cette lettre s'est révélée nécessaire pour pouvoir 
        écrire les nom propre Suédois ainsi que quelques mots d'emprunt.  
        Elle se prononce comme un "o ouvert long", soit le même "o" que dans "comme" qui durerait plus longtemps.

    ??? warning "L'harmonie vocalique"
        En finnois, il existe un système que l'on nomme "L'harmonie vocalique". Cela consiste en la classification des voyelles
         dans un groupe qui établira les voyelles présents dans un mot.
        Pour faire simple, les mots finlandais ne peuvent être constituer qu'avec des voyelles d'un même groupe soit :  
        ![schéma harmonie vocalique](https://upload.wikimedia.org/wikipedia/commons/e/e3/Finnish_vowel_harmony_Venn_diagram.svg){width=500}  
        - ^^**a o u**^^ constituant les voyelles arrières  
        - ^^**ä ö y**^^ constituant les voyelles avants  
        - ^^**e i**^^ constituant les voyelles neutres, étant compatible avec les deux groupes cités précédement  

        Ainsi, les mots peuvent être exclusivement constitués de voyelles avants (avec voyelles neutres) ou exclusivement de voyelles arrières.

        !!! warning "Exception"
            Cette particularité ne s'applique par pour les mots composés. En effet, comme en allemand, le finnois peut avoir recourt à ce 
            qu'on appelle de "l'agglutination", c'est-à-dire que l'on va coller plusieurs mots ensembles pour créer un nouveau mot/idée/concepte.  
            !!! example "Exemple"
                **Aurinko** = soleil : il est constitué de ^^voyelles arrières et neutres^^  
                **Pimennys** = coupure/extinction : constitué de ^^voyelles avant et neutres^^  
                **Auringonpimennys** = Eclipse solaire : mot composé, ^^présence des trois groupes^^ de voyelles.
        
        !!! tip "Information supplémentaire"
            L'harmonie vocalique est important car elle permet de déterminer l'orthographe correcte des déclinaisons et terminaisons des verbes qu'on verra plus tard.  
            Par exemple : Kylä (village) => Menen kylä<span style="color:green">än</span> maanantaina. (Je vais/irai au village Lundi)  
            Suomi (Finlande) => Kävimme Suome<span style="color:green">ssa</span> viime viikko. (Nous sommes allés en Finlande la semaine dernière)

### Les consonnes

!!! Consonnes

    !!! info "Audio"
        Vous pouvez cliquer sur les lettres pour entendre leur prononciation.

    | Lettres | phonétique (en [API](https://fr.wikipedia.org/wiki/Alphabet_phon%C3%A9tique_international)) | Prononciation approximative | Exemple en finnois |
    | ------- | ------------------------------------------------------------------------------------------- |---------------------------- | ------------------ |
    | <a href="../audios/lettres/consonnes/b.ogg">b</a> | /b/ | même son qu'en français | Uniquement dans les mots d'emprunt |
    | <a href="../audios/lettres/consonnes/c.ogg">c</a> | /s/ /k/ | soit "s" soit "k" | Uniquement dans les mots d'emprunt |
    | <a href="../audios/lettres/consonnes/d.ogg">d</a> | /d/ | même son qu'en français | Äidin (mère [au génitif]) |
    | <a href="../audios/lettres/consonnes/f.ogg">f</a> | /f/ | même son qu'en français | Flunssa (grippe) |
    | <a href="../audios/lettres/consonnes/g.ogg">g</a> | /g/ | même son qu'en français | Auringon (soleil) |
    | <a href="../audios/lettres/consonnes/h.ogg">h</a> | /h/ | comme le h anglais (aspiré) | Häiritä (déranger) |
    | <a href="../audios/lettres/consonnes/j.ogg">j</a> | /j/ | se prononce "y" | Ja (et) |
    | <a href="../audios/lettres/consonnes/k.ogg">k</a> | /k/ | même son qu'en français | Kaksi (deux) |
    | <a href="../audios/lettres/consonnes/l.ogg">l</a> | /l/ | même son qu'en français | Lämmin (chaud) |
    | <a href="../audios/lettres/consonnes/m.ogg">m</a> | /m/ | même son qu'en français | Maito (lait) |
    | <a href="../audios/lettres/consonnes/n.ogg">n</a> | /n/ | même son qu'en français | Naali (renard arctique) |
    | <a href="../audios/lettres/consonnes/p.ogg">p</a> | /p/ | même son qu'en français | Punainen (rouge) |
    | <a href="../audios/lettres/consonnes/k.ogg">q</a> | /k/ | même son qu'en français | Uniquement dans les mots d'emprunt |
    | <a href="../audios/lettres/consonnes/r.ogg">r</a> | /r/ | comme le "r" espagnol (r roulé) | Raha (argent) |
    | <a href="../audios/lettres/consonnes/s.ogg">s</a> | /s/ | se prononce toujours "s" | Sunnuntai (dimanche) |
    | <a href="../audios/lettres/consonnes/t.ogg">t</a> | /t/ | même son qu'en français | Tapahtua (se passer/se dérouler) |
    | <a href="../audios/lettres/consonnes/v.ogg">v</a> | /ʋ/ | proche du "v" prononcé uniquement avec les lèvres | Vauva (bébé) |
    | <a href="../audios/lettres/consonnes/x.ogg">x</a> | /ks/ | se prononce "ks" | Uniquement dans les mots d'emprunt |
    | <a href="../audios/lettres/consonnes/z.ogg">z</a> | /ts/ | se prononce "ts" | Uniquement dans les mots d'emprunt |

    !!! cite "Autre"
        Il y a en finnois 3 autres lettre qui ne font pas partie de l'alphabet mais qui sont présent : il s'agit du "w" /ʋ/; Š /ʃ/ (comme le "ch") et Ž /ʒ/ (comme notre "j").  
        Elles ne sont utilisées que dans des mots d'emprunt, et dans le cas de Š et Ž, souvent pour transcrire des mots d'origine slave ou autres.  
        Azerbaidžan (Azerbaïdjan); Fidži (Fidji)...

    ??? warning "Alternance consonantique"
        Il existe en finnois un autre système, tout aussi important que l'harmonie vocalique, qui est la gradation de consonne (ou Alternance consonantique). C'est le système le moins évident des deux. Elle consiste en la modification des consonnes d'un mots (substantifs, adjectifs, verbes) selon le contexte (cas grammaticaux, pluralité)  
        Elle se divise en deux types de gradation : La gradation <span style="color:green">***directe***</span> et <span style="color:green">***iverse***</span>
        ??? example "Gradation Directe"
            ^^Comparaison entre substantifs nominatif et génitif ou verbes conjugués^^  
              
            | Type | Gradation Forte | Gradation Faible |
            | ---- | --------------- | ---------------- |
            | pp : p | Kaa<u>pp</u>i | Kaa<u>p</u>in |
            | tt : t | Ken<u>tt</u>ä | Ken<u>t</u>än |
            | kk : k | Nu<u>kk</u>e | Nu<u>k</u>en |
            | mp : mm | La<u>mp</u>i | La<u>mm</u>en |
            | nt : nn | Tu<u>nt</u>i | Tu<u>nn</u>en |
            | nk : ng | Auri<u>nk</u>o | Auri<u>ng</u>on |
            | lp : lv | Ha<u>lp</u>a | Ha<u>lv</u>an |
            | lt : ll | Si<u>lt</u>a | Si<u>ll</u>an |
            | lk : l | Pe<u>lk</u>o | Pe<u>l</u>on |
            | lk(i) : lj | Jä<u>lk</u>i | Jä<u>lj</u>en |
            | rp : rv | Tu<u>rp</u>a | Tu<u>rv</u>an |
            | rt : rr | Vi<u>rt</u>a | Vi<u>rr</u>an |
            | rk : r | Ko<u>rk</u>o | Ko<u>r</u>on |
            | rk(i) : rj | Kä<u>rk</u>i | Kä<u>rj</u>en |
            | ht : hd | Le<u>ht</u>i | Le<u>hd</u>en |
            | hk : h | Vi<u>hk</u>o | Vi<u>h</u>on |
            | hk(i) : hj | - | - |
            | p : v | So<u>p</u>u | So<u>v</u>un |
            | t : d | Tai<u>t</u>o | Tai<u>d</u>on |
            | k : _ | Jo<u>k</u>i | Joen |
            | k : v | Pu<u>k</u>u | Pu<u>v</u>un |

        ??? example "Gradation Inverse"
            ^^Comparaison entre substantifs nominatif et génitif ou verbes conjugués^^  
              
            | Type | Gradation Faible | Gradation Forte |
            | ---- | --------------- | ---------------- |
            | pp : p | O<u>p</u>as | O<u>pp</u>aan |
            | tt : t | Syy<u>t</u>ön | Syy<u>tt</u>ömän |
            | kk : k | Ri<u>k</u>as | Ri<u>kk</u>aan |
            | mp : mm | La<u>mm</u>as | La<u>mp</u>aan |
            | nt : nn | Ra<u>nn</u>e | Ra<u>nt</u>een |
            | nk : ng | Ka<u>ng</u>as | Ka<u>nk</u>aan |
            | lp : lv (__uniquement vb__) | Ke<u>lv</u>ata| Ke<u>lp</u>aan |
            | lt : ll | A<u>ll</u>as | A<u>lt</u>aan |
            | lk : l (__uniquement vb__) | Hy<u>l</u>ätä | Hylk</u>ään |
            | lk(i) : lj | Hy<u>lj</u>e | Hy<u>lk</u>een |
            | rp : rv | Ta<u>rv</u>e | Ta<u>rp</u>een |
            | rt : rr | Po<u>rr</u>as | Po<u>rt</u>aan |
            | rk : r | Va<u>r</u>as | Va<u>rk</u>aan |
            | rk(i) : rj (__uniquement vb__)| Ta<u>rj</u>eta | Ta<u>rk</u>enen |
            | ht : hd | Vii<u>hd</u>e | Vii<u>ht</u>een |
            | hk : h | Pyy<u>h</u>e | Pyy<u>hk</u>een |
            | hk(i) : hj | La<u>hj</u>e | La<u>hk</u>een |
            | p : v | Tai<u>v</u>e | Tai<u>p</u>een |
            | t : d | Tai<u>d</u>e | Tai<u>t</u>een |
            | k : _ | Koe | Ko<u>k</u>een |
            | k : v | - | - |
        
## Les allongements et pauses

!!! tldr "Allongements et Pauses"
    Quand je disais plus tôt que le finnois est une langue qui se prononce comme elle s'écrit, c'est littéralement le cas. 
    Outre la phonologie qui a déjà été abordé, nous allons maintenant observer le cas où deux lettre de même nature se succèdent. 
    Il existe en finnois une opposition fondamentale, tel que pour l'harmonie vocalique, entre les lettres brèves et les lettres longues. 
    Dans tous les cas, cet allongement sera représenté par un redoublement de la voyelle ou de la consonnes. Un allongement manqué peut 
    entraîner une modification dans le sens du mot (Kylä (Village) / Kyllä (Oui); Tuli (Feu) / Tuuli (Vent)...)

    !!! tip "Allongement Vocalique"
        Les allongements vocaliques correspond pour faire simple au fait de prononcer une voyelle plus longtemps. Elle se présente par un redoublement de la voyelle.  
        - <a href="../audios/allongements/syy.ogg">S**yy**</a> (Raison)  
        - <a href="../audios/allongements/maa.ogg">M**aa**</a> (Pays)  
        - <a href="../audios/allongements/maanantai.ogg">M**aa**nantai</a> (Lundi) 

    !!! tip "Allongement Consonantique"
        Les allongement consonantique est dans le même principe que pour les voyelles. Par contre, selon la nature de la consonne, 
        la manière d'exécuter le son sera différente.  
        - Pour les consonnes p, t, k... _(soit les consonnes plosives, c'est à dire les consonnes que l'ont ne peut pas maintenir somme le /s/)_, 
        la pause se caractérisera par une brève pause dans l'exécution du son, au moment où l'on s'apprète de à produire le son,
         on le bloque pour une durée brève. Le passage de l'air est obstrué, ainsi aucun son ne se produit pendant la pause.  
        - Pour les consonnes s, r... _(soit les fricatives ou roulée, c'est à dire les consonnes que l'ont peut maintenir sans arrêt)_, 
        il suffit de les maintenir plus longtemps.  
        - Pour les consonnes m, n, l (soit nasale pour m et n et liquide pour l), l'exécution de ces son est similaire 
        aux consonnes plosives mais contrairement à elle, un son continue de se produire dit nasalisé.

        ??? warning "Précision"
            Cela parait difficile à première vu, avec tous ces termes et manières d'expliquer cela peut paraitre 
            complexe dans l'exécution des allongements mais est bien plus facile que ça. Il est simplement difficile d'expliquer la sonarité à l'écrit.

        !!! danger "Attention"

            Les allongements ne se pas à prendre à la légère car il est possible de faire un contre-sens si vous le
            malheur de mal prononcer un allongement/pause.

            Voici quelques exemples avec audios :

            - <a href="../audios/allongements/tili.ogg">▶️</a> Tili (compte bancaire) / Tilli (aneth) / Tiili (brick)
            - <a href="../audios/allongements/tuli.ogg">▶️</a> Tuli (feu) / Tulli (douane) / Tuuli (vent)
            - <a href="../audios/allongements/kyla.ogg">▶️</a> Kylä (village) / Kyllä (oui)
            - <a href="../audios/allongements/valita.ogg">▶️</a> Valita (choisir) / Vallita (gouverner) / Valittaa (se pleindre) / Välittää (prendre soin)
            - <a href="../audios/allongements/kuola.ogg">▶️</a> Kuola (salive) / Kuolla (mourir)
