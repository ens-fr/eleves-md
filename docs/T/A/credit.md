# Crédit

!!! tip "Mes sources dans la conception de ce site"
    Apprenant le finnois en autodidacte, internet m'a été d'une grande aide dans la concrétisation
    du site, mais aussi livre que j'ai eu l'occasion de lire. Ainsi, je trouve important le fait de les créditer.

    - <a href="https://uusikielemme.fi">Uusi Kielemme</a>, signifiant "Notre nouvelle langue" en finnois, est un site finlandais
    offrant du contenue très riche et clair pour nous permettre d'apprendre la langue. La plateforme est multilingue avec pour langue
    principale l'anglais, mais on y retrouve aussi le contenu en français, en russe, en chinois, en suédois et en espagnol.
    - <a href="https://yle.fi">Yle</a> est le plus grand site d'actualité finlandais. Le mot "yle" peut signifier plusieurs choses selon quelle racine on
    imagine : "Yleisö" signifiant "public" ou encore "Yleinen" signifiant "général" ou aussi "public". La plateforme est aussi multilingue avec comme
    langue par défaut le finnois, avec aussi l'anglais et le suédois qui sont tous deux les deux autres majoritaires du pays. On peut aussi retrouver sur
    la plateforme l'actualité en Sáme, qui est une langue de la même famille que le finnois.
    - <a href"https://fr.wikipedia.org/wiki/Finnois">Wikipédia</a> aussi. Certe plus limité au niveau du contenu mais offre quand même quelques
    informations précieuses.
    - "Le Finnois" de Tuula Laakkonen, qui est un manuel de poche en français. Il est très complet et clair.
    - <a href="https://discord.gg/Hjrs6gu2">Discord</a>, au travers de serveur dont celui de <a href="https://uusikielemme.fi">Uusi Kielemme</a>.
    - Les locuteurs natifs que j'ai rencontré quand nous étions partis en Novembre dernier.