# Exercices

!!! tip "Exercices"

    Pour conclure ce site, je veux propose quelques exercices simples afin
    de tester votre compréhension de la langue.

## La conjugaison

!!! example "Exercice 1"

    - La fille marche dans le parc => Tyttö <select name="p2"><option value="kävelen">kävelen</option><option value="kävellen">kävellen</option><option value="kavelee">kavelee</option><option value="käveli">käveli</option></select> puistossa.<BR><BR>  

    ??? faq "Réponse"

        Tyttö <u>kävelee</u> puistossa.  

    - Mange avec nous ! => <select name="p2"><option value="Syödä">Syödä</option><option value="Söi">Söi</option><option value="Syöt">Syöt</option><option value="Syö">Syö</option></select> meidän kanssa !<BR><BR>  

    ??? faq "Réponse"

        <u>Syö</u> meidän kanssa !  

    - Un jouet fait par ma mère. => Minun äidin <select name="p2"><option value="tekemä">tekemä</option><option value="tehnyt">tehnyt</option><option value="tee">tee</option><option value="tekevät">tekevät</option></select> lelu.<BR><BR>  

    ??? faq "Réponse"

        Minun äidin <u>tekemä</u> lelu.

## La règle de l'objet

!!! example "Exercice 2"

    - Je mange une pomme => Syön <select name="p2"><option value="omena">omena</option><option value="omenan">omenan</option><option value="omenaa">omenaa</option><option value="omenat">omenat</option></select>.<BR><BR>

    ??? faq "Réponse" 

        Syön <u>omenaa</u>.

    - Nous regardons le chien => Näemme <select name="p2"><option value="koiran">koiran</option><option value="koiria">koiria</option><option value="koiralle">koiralle</option><option value="koirassa">koirassa</option></select>.<BR><BR>

    ??? faq "Réponse" 

        Näemme <u>koiran</u>.