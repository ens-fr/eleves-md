# La conjugaison en finnois

## Le finnois ? Complexe ? Non... pas du tout...

!!! faq "Comment fonctionne la conjugaison en finnois ?"
    Il faut le dire, la conjugaison en finnois est très complexe. Il y a certes peu d'exception dans la conjugaison, 
    mais énormément de règles régissant et encadrant très précisément les différentes conjugaisons de la langue, 
    tel le système d'harmonie vocalique et d'alternance consonantique mentionnée dans la partie sur la phonologie.  
    Dans cette partie, je vais vous détailler le fonctionnement de la conjugaison de la langue finnoise en essayant 
    d'être le plus précis et clair possible.

### Groupe de verbe

!!! info "Groupes verbaux"
    On peut catégoriser les verbes en 6 groupes :
      
    -1^er^ groupe : les verbes finissant en <span style="color:#4BADE5">-aa, -ea, -eä, -ia, -iä, -oa, -ua, -yä, -ää, -öä</span>  
    -2^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-da/-dä</span>  
    -3^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-lla/-llä, -nna/-nnä, -rra/-rrä, -sta/-stä</span>  
    -4^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-ata/-ätä, -ota/-ötä, -uta/-ytä</span>  
    -5^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-ita/-itä</span>  
    -6^ème^ groupe : les verbes finissant en <span style="color:#4BADE5">-eta/-etä</span>  
    
    !!! warning "Les groupes 4,5 et 6"
        - Autre :

        Alors que le système des types de groupe finlandais a très peu d'exceptions, il existe des verbes des groupes 4, 5 et 6 
        qui passent d'un groupe à un autre. Ces verbes ne correspondent pas aux règles (simplifiées) utilisées dans la plupart 
        des manuels de cours.  
          
        Principalement à cause de ce problème avec les groupe finlandais 4, 5 et 6, certains linguistes considèrent ces trois 
        types de verbes comme un seul grand groupe de verbes se terminant par -Vta (voyelle + ta), qui comporte trois sous-groupes. 
        De cette façon, ils évitent complètement la question de ces exceptions. Cependant, pour les apprenants de la langue finnoise, 
        la combinaison de ces trois groupes n’est pas pratique.

### Terminaisons personnelles

!!! warning "Important"
    Tout comme en espagnol, en japonais, en italien... le pronoms personnelles n'est pas nécessaire à la compréhension du sujet du verbe. 
    En effet, surtout en finnois, on a recourt à ce qu'on pourrait appeler des "terminaisons personnelles". Il suffixer ces terminaisons 
    à la racine conjuguée pour indiquer la personne.
    Petite précision : pour la 3ème personne du singulier et la 3ème personne du pluriel, le pronom est ici nécessaire, sinon cela risque de 
    créer des ambigüités avec la forme passive.

    Les "terminaisons personnelles" sont :

    | Pronoms | Terminaison | Exemple |
    | ------- | ----------- | ------- |
    | Minä (je) | -n | Sano<u>n</u> |
    | Sinä (tu) | -t | Puhu<u>t</u> |
    | Hän (il/elle) | -V^*1^ ou rien | Tarkoitta<u>a</u> / Syö |
    | Me (nous) | -mme | Ole<u>mme</u> |
    | Te (vous) | -tte | Tee<u>tte</u> |
    | He (ils/elles) | -vAt^*2^ | Näke<u>vät</u> |

    *1 'V' signifie voyelle, ici pour signifier le redoublement de la voyelle précédente.

    *2 'A' majuscule se rapporte à 'a' ou 'ä' (se référer à l'harmonie vocalique), cette règle s'applique aussi pour 'U' ('u' / 'y') et 'O' ('o' / 'ö')

### Les temps et modes en finnois

!!! info "Temps et modes en finnois"

    La langue se compose de 4 modes : l'indicatif, le conditionnel, le potentiel et l'impératif.

    ``` mermaid
    graph LR
        Base[Conjugaison finnoise par mode] --> Ind[Indicatif]
        Ind --> Prs[Présent]
        Ind --> Imp[Imparfait]
        Ind --> Prf[Parfait]
        Ind --> Pqp[Plus-que-Parfait]
        Base --> Cond[Conditionnel]
        Cond --> Prscond[Présent]
        Cond --> Prfcond[Parfait]
        Base --> Pot[Potentiel]
        Pot --> Prspot[Présent]
        Pot --> Prfpot[Parfait]
        Base --> Imper[Impératif]
        Imper --> Prsimp[Présent]
        Imper --> Prfimp[Parfait]
        Base --> Nom['Nominaalimuodot' Forme nominale]
    ```

---

## Les temps de l'indicatif

!!! warning "Information"
    Avant de commencer, il faut savoir que le futur, n'existe pas. Cela ne veut pas dire qu'on ne peut pas l'exprimer. En effet, 
    l'expression du futur est déterminé par le contexte, la déclinaison appliquée sur le mot qui suit ou par le biais d'un auxiliaire.

??? tip "Le présent"
    Le présent est le temps le plus facile en finnois.

    !!! Usage
        L'usage est le même qu'en français, il n'y a rien de spécial à dire.
        !!! example "Exemple"
            - Je bois du café. => Juo**<span style="color:#EC1865">n</span>** kahvia.
            - Nous allons au japon cet été. => Mene**<span style="color:#EC1865">mme</span>** Japaniin tänä kesänä.

    === "Premier groupe"
        
        Il suffit de remplacer le -a/-ä final par les terminaisons personnelles :

        | Présent | Puhua (parler) | Sanoa (dire) | Kysyä (demander) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Puhu<u>n</u> | Sano<u>n</u> | Kysy<u>n</u> |
        | Sinä (tu) | Puhu<u>t</u> | Sano<u>t</u> | Kysy<u>t</u> |
        | Hän (il/elle) | Puhu<u>u</u> | Sano<u>o</u> | Kysy<u>y</u> |
        | Me (nous) | Puhu<u>mme</u> | Sano<u>mme</u> | Kysy<u>mme</u> |
        | Te (vous) | Puhu<u>tte</u> | Sano<u>tte</u> | Kysy<u>tte</u> |
        | He (ils/elles) | Puhu<u>vat</u> | Sano<u>vat</u> | Kysy<u>vät</u> |



    === "Deuxième groupe"
        
        Il suffit de remplacer le -da/-dä par les terminaisons personnelles : (la 3PS ne prend pas d'allongement)

        | Présent | Saada (obtenir) | Juoda (boire) | Syödä (manger) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Saan | Juon | Syön |
        | Sinä (tu) | Saat | Juot | Syöt |
        | Hän (il/elle) | Saa | Juo | Syö |
        | Me (nous) | Saamme | Juomme | Syömme |
        | Te (vous) | Saatte | Juotte | Syötte |
        | He (ils/elles) | Saavat | Juovat | Syövät |

    === "Troisième groupe"

        Il suffit de remplacer -la/-lä, -na/-nä, -ra/-rä,ou-ta/-tä. Et à cette racine, rajouter -e- suivi par les terminaisons personnelles.

        | Présent | Tulla (venir) | Mennä (aller) | Nousta (se tenir debout) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Tulen | Menen | Nousen |
        | Sinä (tu) | Tulet | Menet | Nouset |
        | Hän (il/elle) | Tulee | Menee | Nousee |
        | Me (nous) | Tulemme | Menemme | Nousemme |
        | Te (vous) | Tulette | Menette | Nousette |
        | He (ils/elles) | Tulevat | Menevät | Nousevat |

    === "Quatrième groupe"

        Il suffit de remplacer le -ta/-tä par un -a- suivi des terminaisons personnelles.  
        Dans le cas où le verbe est en -ata/-ätä, alors on ne redouble pas la voyelle une nouvelle fois au 3PS pour éviter de se retrouver 
        avec trois voyelle identiques qui se suivent.   

        | Présent | Haluta (vouloir) | Osata (être capable de) | Pakata (emballer) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Haluan | Osaan | Pakkaan |
        | Sinä (tu) | Haluat | Osaat | Pakkaat |
        | Hän (il/elle) | Haluaa | Osaa | Pakkaa |
        | Me (nous) | Haluamme | Osaamme | Pakkaamme |
        | Te (vous) | Haluatte | Osaatte | Pakkaatte |
        | He (ils/elles) | Haluavat | Osaavat | Pakkaavat |

    === "Cinquième groupe"

        Il faut transformer le -ita/-itä en -itse- pui rajouter les terminaisons personnelles.

        | Présent | Häiritä (déranger) | Tarvita (avoir besoin) | Hallita (contrôler) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Häiritsen | Tarvitsen | Hallitsen |
        | Sinä (tu) | Häiritset | Tarvitset | Hallitset |
        | Hän (il/elle) | Häiritsee | Tarvitsee | Hallitsee |
        | Me (nous) | Häiritsemme | Tarvitsemme | Hallitsemme |
        | Te (vous) | Häiritsette | Tarvitsette | Hallitsette |
        | He (ils/elles) | Häiritsevät | Tarvitsevat | Hallitsevat |

    === "Sixième groupe"

        Il faut ici remplacer le -ta/-tä par -ne- suivi des terminaisons personnelles.

        | Présent | Vanheta (vieillir) | Lämmetä (réchauffer) | Noureta (Rajeunir) |
        | ------- | -------------- | ------------ | ---------------- |
        | Minä (je) | Vanhenen | Lämpenen | Nuorenen |
        | Sinä (tu) | Vanhenet | Lämpenet | Nuorenet |
        | Hän (il/elle) | Vanhenee | Lämpenee | Nuorenee |
        | Me (nous) | Vanhenemme | Lämpenemme | Nuorenemme |
        | Te (vous) | Vanhenette | Lämpenette | Nuorenette |
        | He (ils/elles) | Vanhenevat | Lämpenevät | Nuorenevat |

    === "Autre"

        **Les verbes suivants ressemblent au groupe 5 (-ita / -itä) mais se conjuguent comme au groupe 4. Voir les mots barrés pour savoir 
        comment ils auraient été conjugués si nous suivions les règles à la lettre.**    
          
        selvitä (devenir clair) - selviän ~~(pas selvitsen)~~  
        hävitä (perdre, disparaître) - häviän ~~(pas hävitsen)~~  
          
        **Ensuite, nous avons des verbes qui ressemblent au groupe 6 (-eta / -etä) mais qui se conjuguent comme le groupe 4.**  
          
        hävetä (avoir honte) - häpeän ~~(pas häpenen)~~  
        kiivetä (monter) - kiipeän ~~(pas kiipenen)~~  
        ruveta (commencer) - rupean ~~(pas rupenen)~~  
        todeta (établir) - totean ~~(not totenen)~~  
          
        **Enfin, voici quelques verbes qui ressemblent au groupe 4 mais qui se conjuguent au groupe 6.**
          
        hapata (acidifier) - happanee ~~(pas happaa)~~  
        loitota (détourner) - loittonee ~~(pas loittoaa)~~  
        helpota (être plus facile) - helpponee ~~(pas helppoaa)~~  
        parata (aller mieux) - paranee ~~(pas paraa)~~

        ??? warning "Verbes irréguliers"
            Il n'y a que 2 verbes irrégulier en finnois, ainsi qu'un verbe qui possède une légère irrégularité. Il s'agit de Nähdä (voir), 
            Tehdä (faire) et Olla (être)  
            Nähdä et Tehdä se conjugue de la même manière :

            | Nähdä | Tehdä |
            | ----- | ----- |
            | Näen | Teen |
            | Näet | Teet |
            | Näkee | Tekee |
            | Näemme | Teemme |
            | Näette | Teette |
            | Näkevät | Tekevät |

            Le verbe Olla (être) quant à lui, est un verbe du troisième groupe régulier sauf à la troisième personne du singulier.

            | Olla |
            | ----- |
            | Olen |
            | Olet |
            | <span style="color:#F78E01">On</span> |
            | Olemme |
            | Olette |
            | <span style="color:#F78E01">Ovat</span> |


    ??? danger "Alternance consonatique au présent"
        IMPORTANT : Au présent, certains verbes subissent une alternance consonnatique.
        
        | Groupe | Infinitif | Minä | Sinä | Hän | Me | Te | He |
        | ------ | --------- | ---- | ---- | --- | -- | -- | -- |
        | Groupe 1 | <span style="color:#F78E01">Fort</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 2 | - | - | - | - | - | - | - |
        | Groupe 3 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 4 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 5 | - | - | - | - | - | - | - |
        | Groupe 6 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |

??? tip "L'imparfait"
    Le temps imparfait est assez compliqué pour certains groupes, et assez simple pour d'autres. Dans l'ensemble, il y a beaucoup de choses à 
    prendre en considération : la gradation des consonnes, les types de verbes et les lettres dans lesquelles les verbes se terminent. Chacun 
    de ceux-ci a un effet sur le verbe au temps imparfait.  
    Le marqueur de l'imparfait est -i-, mais peut également apparaître sous la forme -si- et -oi-.
      
    !!! Usage
        L'imparfait est un des 3 temps du passé de la langue finnoise. L'usage diffère du français. On va s'en servir pour des évènements passés 
        dans le cas de phrase où la période de l'action est défini par des mots tel que "Eilen" (hier), "Viimme viikolla" (durant la semaine dernière)...
        !!! example "Exemple"
            Sö**<span style="color:#EC1865">in</span>** eilen puuroa. => J'ai mangé du porridge hier.  
            He istu**<span style="color:#EC1865">ivat</span>** penkillä väsyneinä. => Ils s'assirent fatigués sur le banc.  
            Tarvits**<span style="color:#EC1865">imme</span>** apua. =>	Nous avions besoin d'aide.  

    ??? tip "Sous-groupes du groupe 1"
        === "1^er^ Sous-groupe"
            Pour les verbes qui ont un infinitif qui se termine par -ua, -yä, -oa ou -öä, il faut des -i- derrière les u, y, o et ö sans apporter de modifications 
            à la tige. Il faut noter que la troisième personne du singulier se termine par un seul -i.

            | Imparfait | Sanoa (dire) |
            | --------- | ------------ |
            | Minä | Sano<u>in</u> |
            | Sinä | Sano<u>it</u> |
            | Hän | Sano<u>i</u> |
            | Me | Sano<u>imme</u> |
            | Te | Sano<u>itte</u> |
            | He | Sano<u>ivat</u> |

        === "2^nd^ Sous-groupe"
            Pour les verbes qui se terminent par -ea, -eä ou -ää dans leur forme de base (infinitif), il faut supprimer 
            le -e- ou -ä- lors de l'ajout du -i- de l'imparfait.

            | Imparfait | Itkeä (pleurer) |
            | --------- | ------------ |
            | Minä | Itk<u>in</u> |
            | Sinä | Itk<u>it</u> |
            | Hän | Itk<u>i</u> |
            | Me | Itk<u>imme</u> |
            | Te | Itk<u>itte</u> |
            | He | Itk<u>ivät</u> |

        === "3^ème^ Sous-groupe"
            Pour les verbes dont la forme de base se termine par -ia / iä, le présent et le passé semblent identiques dans la 
            plupart des formes. Par exemple, pour le verbe "tanssia", le présent et le passé sont "minä tanssin" : «Je danse» ET «J'ai dansé».  
              
            C’est le cas parce que vous supprimez d’abord le -i- (tanss-) de la tige, puis ajoutez le -i- de l’imparfait. En tant que tel, vous vous retrouvez 
            avec le même nombre de i aux deux temps. La seule personne où il y a une différence est la troisième personne: le présent est "hän tanssii" et le passé "hän tanssi".

            | Imparfait | Tanssia (danser) |
            | --------- | ------------ |
            | Minä | Tanss<u>in</u> |
            | Sinä | Tanss<u>it</u> |
            | Hän | Tanss<u>i</u> |
            | Me | Tanss<u>imme</u> |
            | Te | Tanss<u>itte</u> |
            | He | Tanss<u>ivat</u> |

        === "4^ème^ Sous-groupe"
            Cette catégorie est appelée verbes "a… a → a… oi" parce que ce sont des mots de deux syllabes qui ont un -a- dans la première et la dernière syllabe. Ce second -a- se transformera en -oi- à l'imparfait.

            | Imparfait | Antaa (donner) |
            | --------- | ------------ |
            | Minä | **<span style="color:#118BC8">A</span>**nn<u>oin</u> |
            | Sinä | **<span style="color:#118BC8">A</span>**nn<u>oit</u> |
            | Hän | **<span style="color:#118BC8">A</span>**nn<u>oi</u> |
            | Me | **<span style="color:#118BC8">A</span>**nn<u>oimme</u> |
            | Te | **<span style="color:#118BC8">A</span>**nn<u>oitte</u> |
            | He | **<span style="color:#118BC8">A</span>**nn<u>oivat</u> |

        === "5^ème^ Sous-groupe"
            Pour tous les verbes se terminant par -aa à l'exception de ceux du 4eme sous-groupe, le -i- remplacera la voyelle finale.

            | Imparfait | Ostaa (acheter) |
            | --------- | ------------ |
            | Minä | Ost<u>in</u> |
            | Sinä | Ost<u>it</u> |
            | Hän | Ost<u>i</u> |
            | Me | Ost<u>imme</u> |
            | Te | Ost<u>itte</u> |
            | He | Ost<u>ivat</u> |
        
        === "6^ème^ Sous-groupe"
            Le dernier sous-groupe du groupe 1 est assez rare et concerne les verbes finissant en -ltaa/-ltää, -rtaa/-rtää, -ntaa/-ntää, –vvtaa/-vvtää, mais comprend quelques verbes très fréquemment utilisés (par exemple, tietää (-vvtää) et ymmärtää (-rtää) en font partie).

            | Imparfait | Tietää (savoir) |
            | --------- | ------------ |
            | Minä | Tie<u>sin</u> |
            | Sinä | Tie<u>sit</u> |
            | Hän | Tie<u>si</u> |
            | Me | Tie<u>simme</u> |
            | Te | Tie<u>sitte</u> |
            | He | Tie<u>sivät</u> |

            | Imparfait | Ymmärtää (savoir) |
            | --------- | ------------ |
            | Minä | Ymmär<u>sin</u> |
            | Sinä | Ymmär<u>sit</u> |
            | Hän | Ymmär<u>si</u> |
            | Me | Ymmär<u>simme</u> |
            | Te | Ymmär<u>sitte</u> |
            | He | Ymmär<u>sivät</u> |

    ??? tip "Sous-groupes du groupe 2"

        Le deuxième groupe a trois sous-groupes. Il a également un verbe irrégulier: le verbe käydä (minä kävin, sinä kävit, hän kävi...).

        === "1^er^ Sous-groupe"
            Les verbes de type "Myydä" sont courts: ils ont deux syllabes. Dans la première syllabe, ils peuvent avoir une voyelle longue ou une diphtongue.  
            Dans le cas d'une voyelle longue, il faut supprimer l’une des voyelles longues lorsque vous ajoutez le -i de l’imparfait.

            | Imparfait | Myydä (acheter) |
            | --------- | ------------ |
            | Minä | My<u>in</u> |
            | Sinä | My<u>it</u> |
            | Hän | My<u>i</u> |
            | Me | My<u>imme</u> |
            | Te | My<u>itte</u> |
            | He | My<u>ivät</u> |

            Pour les verbes qui ont une diphtongue dans la première syllabe (vie-dä, juo-da, syö-da), on dois supprimer la première voyelle de la première syllabe. 
            Après cela, on ajoute le -i- imparfait à la fin de la tige.

            | Imparfait | Syödä (manger) |
            | --------- | ------------ |
            | Minä | Sö<u>in</u> |
            | Sinä | Sö<u>it</u> |
            | Hän | Sö<u>i</u> |
            | Me | Sö<u>imme</u> |
            | Te | Sö<u>itte</u> |
            | He | Sö<u>ivät</u> |

            | Imparfait | Juoda (boire) |
            | --------- | ------------ |
            | Minä | Jo<u>in</u> |
            | Sinä | Jo<u>it</u> |
            | Hän | Jo<u>i</u> |
            | Me | Jo<u>imme</u> |
            | Te | Jo<u>itte</u> |
            | He | Jo<u>ivat</u> |

        === "2^ème^ Sous-groupe"
            Le modèle ci-dessus n'est pas vrai pour les verbes du groupe 2 qui se terminent par -ida / -idä. Tous les verbes du groupe 2 qui se 
            terminent par -ida ont exactement la même apparence lorsqu'ils sont conjugués au présent et au passé. Ceci est dû à la même raison 
            que pour les verbes de type "Tanssia" (danser): lors de la conjugaison de ces verbes, vous remplacez le –i– du radical du verbe par 
            le –i– de l'imparfait.  
              
            Ce groupe comprend à la fois des verbes courts et plus longs: des verbes comme "Uida" de deux syllabes qui se terminent par -ida 
            et des verbes comme "Tupakoida" plus longs (3 syllabes et plus). On pourrait dire que ces verbes sont les plus simples, car leur 
            conjugaison au présent et au passé semble identique.

            | Imparfait | Voida (pouvoir) |
            | --------- | ------------ |
            | Minä | Vo<u>in</u> |
            | Sinä | Vo<u>it</u> |
            | Hän | Vo<u>i</u> |
            | Me | Vo<u>imme</u> |
            | Te | Vo<u>itte</u> |
            | He | Vo<u>ivat</u> |

            | Imparfait | Tupakoida (fumer) |
            | --------- | ------------ |
            | Minä | Tupako<u>in</u> |
            | Sinä | Tupako<u>it</u> |
            | Hän | Tupako<u>i</u> |
            | Me | Tupako<u>imme</u> |
            | Te | Tupako<u>itte</u> |
            | He | Tupako<u>ivat</u> |

        === "3^ème^ Sous-groupe"
            En plus des verbes nähdä (voir) et tehdä faire, le temps imparfait a un verbe irrégulié: käydä (visiter un lieu).

            | Présent | Nähdä | Tehdä | Käydä |
            | ------- | -------------- | ------------ | ---------------- |
            | Minä | Nä<u>in</u> | Te<u>in</u> | Käv<u>in</u> |
            | Sinä | Nä<u>it</u> | Te<u>it</u> | Käv<u>it</u> |
            | Hän | Näk<u>i</u> | Tek<u>i</u> | Käv<u>i</u> |
            | Me | Nä<u>imme</u> | Te<u>imme</u> | Käv<u>imme</u> |
            | Te | Nä<u>itte</u> | Te<u>itte</u> | Käv<u>itte</u> |
            | He | Näk<u>ivät</u> | Tek<u>ivät</u> | Käv<u>ivät</u> |

    ??? tip "Les groupes 3, 4, 5 et 6"
        C'est groupe-là eux non pas de sous groupe (oufff....)
        === "3^ème^ groupe"
            Dans le 3eme groupe, les verbes ont tous en commun qu'il y aura un -e- à la fin du radical. Ce -e- sera remplacé par un -i- dans l'imparfait. 
            Pour la troisième personne, les deux -e- disparaîtront.

            | Imparfait | Nousta (se tenir debout) |
            | --------- | ------------ |
            | Minä | Nous<u>in</u> |
            | Sinä | Nous<u>it</u> |
            | Hän | Nous<u>i</u> |
            | Me | Nous<u>imme</u> |
            | Te | Nous<u>itte</u> |
            | He | Nous<u>ivat</u> |
        
        === "4^ème^ groupe"
            On remplace le -ta/tä du verbe et on le remplace par -si- suivi de la terminaison personnelle

            | Imparfait | Haluta (vouloir) |
            | --------- | ------------ |
            | Minä | Halu<u>sin</u> |
            | Sinä | Halu<u>sit</u> |
            | Hän | Halu<u>si</u> |
            | Me | Halu<u>simme</u> |
            | Te | Halu<u>sitte</u> |
            | He | Halu<u>sivat</u> |

        === "5^ème^ groupe"
            Le 5eme groupe ressemble beaucoup au groupe 3. Dans le sens que les deux ont un -e- à la fin de leur racine, et ce -e- est 
            remplacé par un -i- à l'imparfait pour les deux groupes.

            | Imparfait | Tarvita (avoir besoin) |
            | --------- | ------------ |
            | Minä | Tarvits<u>in</u> |
            | Sinä | Tarvits<u>it</u> |
            | Hän | Tarvits<u>i</u> |
            | Me | Tarvits<u>imme</u> |
            | Te | Tarvits<u>itte</u> |
            | He | Tarvits<u>ivat</u> |

        === "6^ème^ groupe"
            Comme pour les groupes 3 et 5, il suffit de changer le -e- final en -i-.

            | Imparfait | Tarvita (avoir besoin) |
            | --------- | ------------ |
            | Minä | Vanhen<u>in</u> |
            | Sinä | Vanhen<u>it</u> |
            | Hän | Vanhen<u>i</u> |
            | Me | Vanhen<u>imme</u> |
            | Te | Vanhen<u>itte</u> |
            | He | Vanhen<u>ivat</u> |
            
    ??? danger "Alternance consonatique à l'imparfait"
        IMPORTANT : À l'imparfait, certains verbes subissent une alternance consonnatique. Il s'agit de la même alternance qu'au présent.
        
        | Groupe | Infinitif | Minä | Sinä | Hän | Me | Te | He |
        | ------ | --------- | ---- | ---- | --- | -- | -- | -- |
        | Groupe 1 | <span style="color:#F78E01">Fort</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 2 | - | - | - | - | - | - | - |
        | Groupe 3 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 4 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |
        | Groupe 5 | - | - | - | - | - | - | - |
        | Groupe 6 | <span style="color:#38CB98">Faible</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> | <span style="color:#F78E01">Fort</span> |

??? tip "Le parfait"
    Le parfait est en finnois un temps composé. Il se forme avec le verbe "Être" au présent suivi du verbe au participe passé actif, que nous auront l'occasion de voir plus tard.
    !!! note "Usage du temps Parfait"
        Lorsque vous regardez quand utiliser le temps parfait, vous devez le faire par rapport au temps imparfait. Les deux temps sont utilisés pour des choses dans le passé, mais signifient quelque chose de différent. La façon dont ils sont utilisés en français n'est pas toujours comme en finnois.
          
        __1.1) Quand quelque chose se passe encore__  
          
        Premièrement, le temps parfait est utilisé pour des choses qui ont commencé dans le passé, mais qui se poursuivent encore au moment de parler.  
        !!! example "exemple"
            **<span style="color:#8E9190">Exemple 1 :</span>**  
              
	        - <span style="color:#FF3600">Parfait:</span>  
              
            <span style="color:#FF3600">Olen asunut</span> Suomessa yhden vuoden.  
            "Je vis en Finlande depuis un an." *(Je vis toujours en Finlande)*  
              
	        - <span style="color:#37A500">Imparfait:</span>  
              
            <span style="color:#37A500">Asuin</span> Suomessa yhden vuoden.  
            "J'ai vécu en Finlande pendant un an." *(À l'heure actuelle, je ne vis pas en Finlande, mais j'y ai vécu pendant un an.)*  
              
            **<span style="color:#8E9190">Exemple 2 :</span>**  
              
	        - <span style="color:#FF3600">Parfait:</span>  
              
		    <span style="color:#FF3600">Olen lukenut</span> tätä kirjaa monta tuntia.  
	        "Je lis ce livre depuis de nombreuses heures." *(Je vais toujours continuer.)*  
              
	        - <span style="color:#37A500">Imparfait:</span>  
              
		    <span style="color:#37A500">Luin</span> tätä kirjaa monta tuntia.  
	        "J'ai lu ce livre pendant de nombreuses heures." *(Je ne vais plus continuer.)*

        __1.2) Quand quelque chose est pertinent pour le moment actuel__  
          
        Lorsque quelque chose est fait, mais que le résultat est pertinent pour le moment actuel, vous utilisez également le temps parfait.  
        !!! example "exemple"
            - Nyt <span style="color:#FF3600">olen syönyt</span> tarpeeksi.  
	        "Maintenant, j'ai assez mangé."  
	        *(J'ai fini de manger, mais le résultat (que j'ai fait maintenant) est pertinent pour le moment.)*  
              
            - Kuka <span style="color:#FF3600">on kirjoittanut</span> Työmiehen vaimon?  
	        "Qui a écrit le livre‘ työmiehen vaimo ’?"  
	        *(Le livre a été écrit il y a longtemps mais les informations sont pertinentes en ce moment.)*  

        __1.3) Les phrases avec "Oletko Koskaan"__  
          
        Lorsque vous parlez de votre vie passée et de ce que vous avez fait ou non, vous utilisez le temps parfait en finnois.  
              
        Dans les phrases négatives, cela signifie que vous n’avez pas fait l’action, mais qu'on pourrai le faire plus tard. Par exemple, 
        la phrase __«En ole käynyt Espanjassa»__ (je ne suis pas allé en Espagne) n’exclut pas la possibilité d’y aller plus tard.  
              
        Une question demandant si quelqu'un a déjà fait quelque chose dans le passé commence toujours par «oletko koskaan». La 
        réponse directe n’aura pas non plus d’heure précise mentionnée (par exemple : __Kyllä, olen käynyt USA:ssa__ «Oui, j’ai été aux États-Unis.»). 
        Lorsqu'on spécifie précisément le moment de l'événement, on utilise le temps imparfait à la place (par exemple: __Kävin USA:ssa viime vuonna__ 
        «J'étais aux États-Unis l'année dernière»).  
        !!! example "exemple"
            <span style="color:#56AFBF">Oletko koskaan</span> matkustanut Thaimaahan? *(As-tu déjà voyagé en Thaïlande?)*  
            -En ole, mutta haluaisin kyllä! *(Non, mais je voudrais bien !)*  
              
            <span style="color:#56AFBF">Oletteko koskaan</span> syönyt thaimaalaista ruokaa? *(As-tu déjà mangé de la nourriture Thaï?)*  
            -Kyllä, olemme syönyt thaimaalaista ruokaa *(Oui, J'ai mangé de la nourriture Thaï.)*  
            -Söimme thaimaalaista ruokaa viime viikolla *(J'ai mangé de la nourriture Thaï la semaine dernière)*

    


??? tip "Le plus-que-Parfait"
    Comme le parfait, il s'agit d'un temps composé. Il se forme avec le verbe "Être" à l'imparfait suivi du verbe au participe passé actif.  

    !!! note "Usage"
        Le Plus-que-parfait exprime un événement antérieur à un autre événement, les deux événements ayant lieu dans le passé. Pour cette raison, 
        la manière la plus courante d'utiliser le Plus-que-parfait est de le combiner avec l'imparfait. Son usage est très similaire au francais.  
        Dans les phrases avec une imparfait + plus-que-parfait, l'imparfait raconte quelque chose qui s'est passé dans le passé. Ensuite, 
        le Plus-que-parfait vous informe d'un événement qui a eu lieu avant ce moment.

        !!! example "Exemple"
            **<span style="color:#8E9190">Exemple 1</span>**  
              
            Maija <span style="color:#37A500">lähti</span> lenkille, kun <span style="color:#845BE3">oli tehnyt</span> läksynsä.  
            "Maija est allée faire du jogging, quand elle avait fait ses devoirs"  
            = L'imparfait *"lähti"* (est parti) s'est produit dans le passé, et le plus-que-parfait *"oli tehnyt"* (avait fait) est arrivé avant cela.  
              
            **<span style="color:#8E9190">Exemple 2</span>**  
              
            <span style="color:#845BE3">Olin asunut</span> Suomessa kolme viikkoa, kun <span style="color:#37A500">aloitin</span> suomen kielen opiskelun.  
            «J'avais vécu en Finlande pendant trois semaines lorsque j'ai commencé mon apprentissage du finnois.  
            = L'imparfait *"aloitin"* (j'ai commencé) s'est produit dans le passé, et le plus-que-parfait *"olin asunut"* (j'avais habité) est arrivé avant cela.  

## Le conditionnel

!!! tip "Le conditionnel"
    Le mode conditionnel est très similaire dans son usage au français. Il se matérialise par -isi- suivi des terminaisons personnels.

    Il n'y que deux temps au conditionnel : le présent et le parfait (passé).

    !!! example "Example"

        Je chanterais si je pouvais. => Laula**<span style="color:#EC1865">isin</span>**, jos osa**<span style="color:#EC1865">isin</span>**.   
        Si j'avais des vacances, j'irais en Espagne. => Jos minulla ol**<span style="color:#EC1865">isi</span>** lomaa, men**<span style="color:#EC1865">isin</span>** Espanjaan.

    !!! info "Formation"

        ??? tip "Présent"

            === "Premier groupe"
        
                Il faut ajouter -isi- à la racine du verbe. Si le verbe a pour terminaison -ea ou -eä, le -e est directement remplacé par
                -isi-.

                | Présent | Puhua (parler) | Itkeä (pleurer) | Kysyä (demander) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Puhu<u>isin</u> | Itk<u>isin</u> | Kysy<u>isin</u> |
                | Sinä (tu) | Puhu<u>isit</u> | Itk<u>isit</u> | Kysy<u>isit</u> |
                | Hän (il/elle) | Puhu<u>isi</u> | Itk<u>isi</u> | Kysy<u>isi</u> |
                | Me (nous) | Puhu<u>isimme</u> | Itk<u>isimme</u> | Kysy<u>isimme</u> |
                | Te (vous) | Puhu<u>isitte</u> | Itk<u>isitte</u> | Kysy<u>isitte</u> |
                | He (ils/elles) | Puhu<u>isivat</u> | Itk<u>isivät</u> | Kysy<u>isivät</u> |

            === "Deuxième groupe"
                
                Se référer à la conjugaison du deuxième groupe à l'imparfait : la transformation du radical est basé sur le même principe.

                | Présent | Saada (obtenir) | Juoda (boire) | Syödä (manger) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Sa<u>isin</u> | Jo<u>isin</u> | Sö<u>isin</u> |
                | Sinä (tu) | Sa<u>isit</u> | Jo<u>isit</u> | Sö<u>isit</u> |
                | Hän (il/elle) | Sa<u>isi</u> | Jo<u>isi</u> | Sö<u>isi</u> |
                | Me (nous) | Sa<u>isimme</u> | Jo<u>isimme</u> | Sö<u>isimme</u> |
                | Te (vous) | Sa<u>isitte</u> | Jo<u>isitte</u> | Sö<u>isitte</u> |
                | He (ils/elles) | Sa<u>isivat</u> | Jo<u>isivat</u> | Sö<u>isivät</u> |

            === "Troisième groupe"

                Il suffit de remplacer le -e- par -isi-.

                | Présent | Tulla (venir) | Mennä (aller) | Nousta (se tenir debout) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Tul<u>isin</u> | Men<u>isin</u> | Nous<u>isin</u> |
                | Sinä (tu) | Tul<u>isit</u> | Men<u>isit</u> | Nous<u>isit</u> |
                | Hän (il/elle) | Tul<u>isi</u> | Men<u>isi</u> | Nous<u>isi</u> |
                | Me (nous) | Tul<u>isimme</u> | Men<u>isimme</u> | Nous<u>isimme</u> |
                | Te (vous) | Tul<u>isitte</u> | Men<u>isitte</u> | Nous<u>isitte</u> |
                | He (ils/elles) | Tul<u>isivat</u> | Men<u>isivät</u> | Nous<u>isivat</u> |

            === "Quatrième groupe"

                Les verbes en -ata/-ätä => -aisi-/-äisi-; et ceux en -ota/-ötä => -oaisi-/-öäisi- et -uta/-ytä => -uaisi-/-yäisi-.
 
                | Présent | Haluta (vouloir) | Osata (être capable de) | Pakata (emballer) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Hal<u>uaisin</u> | Os<u>aisin</u> | Pakk<u>aisin</u> |
                | Sinä (tu) | Hal<u>uaisit</u> | Os<u>aisit</u> | Pakk<u>aisit</u> |
                | Hän (il/elle) | Hal<u>uaisi</u> | Os<u>aisi</u> | Pakk<u>aisi</u> |
                | Me (nous) | Hal<u>uaisimme</u> | Os<u>aisimme</u> | Pakk<u>aisimme</u> |
                | Te (vous) | Hal<u>uaisitte</u> | Os<u>aisitte</u> | Pakk<u>aisitte</u> |
                | He (ils/elles) | Hal<u>uaisivat</u> | Os<u>aisivat</u> | Pakk<u>aisivat</u> |

            === "Cinquième et sixième groupe"

                Il suffit de remplacer le -e- par -isi-.

                | Présent | Häiritä (déranger) | Tarvita (avoir besoin) | Lämmetä (réchauffer) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Häirits<u>isin</u> | Tarvits<u>isin</u> | Lämpen<u>isin</u> |
                | Sinä (tu) | Häirits<u>isit</u> | Tarvits<u>isit</u> | Lämpen<u>isit</u> |
                | Hän (il/elle) | Häirits<u>isi</u> | Tarvits<u>isi</u> | Lämpen<u>isi</u> |
                | Me (nous) | Häirits<u>isimme</u> | Tarvits<u>isimme</u> | Lämpen<u>isimme</u> |
                | Te (vous) | Häirits<u>isitte</u> | Tarvits<u>isitte</u> | Lämpen<u>isitte</u> |
                | He (ils/elles) | Häirits<u>isivät</u> | Tarvits<u>isivat</u> | Lämpen<u>isivät</u> |

        ??? tip "Parfait"

            Le conditionnel parfait se forme à la manière du Parfait de l'indicatif, il s'agit d'un temps composé. Il se forme
            ainsi avec l'auxiliaire "Olla" (être) au conditionnel suivi du participe passé du verbe.

            | Parfait | Kävellä (marcher) |
            | ------- | ------- |
            | Minä (je) | **<span style="color:#EC1865">Olisin</span>** Kävel<u>lyt</u> |
            | Sinä (tu) | **<span style="color:#EC1865">Olisit</span>** Kävel<u>lyt</u> |
            | Hän (il/elle) | **<span style="color:#EC1865">Olisi</span>** Kävel<u>lyt</u> |
            | Me (nous) | **<span style="color:#EC1865">Olisimme</span>** Kävel<u>lyt</u> |
            | Te (vous) | **<span style="color:#EC1865">Olisitte</span>** Kävel<u>lyt</u> |
            | He (ils/elles) | **<span style="color:#EC1865">Olisivat</span>** Kävel<u>lyt</u> |


## Le potentiel

!!! tip "Le potentiel"
    Le potentiel est souvent décrit comme le « mode de probabilité ». Il est utilisé pour exprimer qu'une action ou un état est probable mais pas certain. 
    Le potentiel n'existe pas en français, mais se traduit par l'ajout de "peut-être" ou "probablement" devant le verbe.

    !!! warning "Précision"
        Dans un discours normal, vous entendrez rarement le potentiel. Le plus souvent, il est utilisé dans les informations et dans les propositions
        écrites officielles lors de réunions par exemple. Il est beaucoup plus courant d'utiliser des adverbes de probabilité ou des verbes comme mahtaa, 
        saattaa et taitaa, qui eux sont entre gros guillements des verbes signifiant peut-être.

    !!! example "Example"

        Anja va sans doute bientôt arriver. => Anja tul**<span style="color:#EC1865">lee</span>** kohta.  
        Le bâtiment sera probablement construit en été => Talo rakennetta**<span style="color:#EC1865">ne</span>**en kesällä.

    !!! info "Formation"

        ??? tip "Présent"

            === "Premier groupe"

                Il suffit de remplacer le -a/-ä final par -ne.

                | Présent | Puhua (parler) | Itkeä (pleurer) | Kysyä (demander) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Puhu<u>nen</u> | Itke<u>nen</u> | Kysy<u>nen</u> |
                | Sinä (tu) | Puhu<u>net</u> | Itke<u>net</u> | Kysy<u>net</u> |
                | Hän (il/elle) | Puhu<u>nee</u> | Itke<u>ne</u> | Kysy<u>nee</u> |
                | Me (nous) | Puhu<u>nemme</u> | Itke<u>nemme</u> | Kysy<u>nemme</u> |
                | Te (vous) | Puhu<u>nette</u> | Itke<u>nette</u> | Kysy<u>nette</u> |
                | He (ils/elles) | Puhu<u>nevat</u> | Itke<u>nevät</u> | Kysy<u>nevät</u> |

            === "Deuxième groupe"
                
                Il faut simplement remplacer -da/-dä par -ne.

                | Présent | Saada (obtenir) | Juoda (boire) | Syödä (manger) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Saa<u>nen</u> | Juo<u>nen</u> | Syö<u>nen</u> |
                | Sinä (tu) | Saa<u>net</u> | Juo<u>net</u> | Syö<u>net</u> |
                | Hän (il/elle) | Saa<u>nee</u> | Juo<u>nee</u> | Syö<u>nee</u> |
                | Me (nous) | Saa<u>nemme</u> | Juo<u>nemme</u> | Syö<u>nemme</u> |
                | Te (vous) | Saa<u>nette</u> | Juo<u>nette</u> | Syö<u>nette</u> |
                | He (ils/elles) | Saa<u>nevat</u> | Juo<u>nevat</u> | Syö<u>nevät</u> |

            === "Troisième groupe"

                Il faut redoubler la consonne précédente puis ajouter -e.

                | Présent | Tulla (venir) | Mennä (aller) | Nousta (se tenir debout) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Tul<u>len</u> | Men<u>nen</u> | Nous<u>sen</u> |
                | Sinä (tu) | Tul<u>let</u> | Men<u>net</u> | Nous<u>set</u> |
                | Hän (il/elle) | Tul<u>lee</u> | Men<u>nee</u> | Nous<u>see</u> |
                | Me (nous) | Tul<u>lemme</u> | Men<u>nemme</u> | Nous<u>semme</u> |
                | Te (vous) | Tul<u>lette</u> | Men<u>nette</u> | Nous<u>sette</u> |
                | He (ils/elles) | Tul<u>levat</u> | Men<u>nevät</u> | Nous<u>sevat</u> |

            === "Quatrième, cinquième et sixième groupe : les verbes en -VtA"

                Il faut remplacer le -ta/-tä par -nne.
 
                | Présent | Haluta (vouloir) | Osata (être capable de) | Pakata (emballer) |
                | ------- | -------------- | ------------ | ---------------- |
                | Minä (je) | Halu<u>nnen</u> | Osa<u>nnen</u> | Pakka<u>nnen</u> |
                | Sinä (tu) | Halu<u>nnet</u> | Osa<u>nnet</u> | Pakka<u>nnet</u> |
                | Hän (il/elle) | Halu<u>nnee</u> | Osa<u>nnee</u> | Pakka<u>nnee</u> |
                | Me (nous) | Halu<u>nnemme</u> | Osa<u>nnemme</u> | Pakka<u>nnemme</u> |
                | Te (vous) | Halu<u>nnette</u> | Osa<u>nnette</u> | Pakka<u>nnette</u> |
                | He (ils/elles) | Halu<u>nnevat</u> | Osa<u>nnevat</u> | Pakka<u>nnevat</u> |

            !!! warning "Olla : une exception à la règle"

                Le verbe "Olla" (être), un verbe pourtant du troisième groupe, est un irrégulier fort au mode potentiel.

                | Présent | Olla (être) |
                | ------- | ----------- |
                | Minä | Lienen |
                | Sinä | Lienet |
                | Hän | Lienee |
                | Me | Lienemme |
                | Te | Lienette |
                | He | Lienevät |

        ??? tip "Parfait"

            Le potentiel parfait se forme à la manière du Parfait de l'indicatif, il s'agit d'un temps composé. Il se forme
            ainsi avec l'auxiliaire "Olla" (être) au potentiel suivi du participe passé du verbe.

            | Parfait | Kävellä (marcher) |
            | ------- | ------- |
            | Minä (je) | **<span style="color:#EC1865">Lienen</span>** Kävel<u>lyt</u> |
            | Sinä (tu) | **<span style="color:#EC1865">Lienet</span>** Kävel<u>lyt</u> |
            | Hän (il/elle) | **<span style="color:#EC1865">Lienee</span>** Kävel<u>lyt</u> |
            | Me (nous) | **<span style="color:#EC1865">Lienemme</span>** Kävel<u>lyt</u> |
            | Te (vous) | **<span style="color:#EC1865">Lienette</span>** Kävel<u>lyt</u> |
            | He (ils/elles) | **<span style="color:#EC1865">Lienevät</span>** Kävel<u>lyt</u> |

## L'impératif

!!! tip "Impératif"

    En finnois, l'impératif est plus ou moins comparable avec l'espagnol, dans le sens où il existe avec tous les pronoms excepté avec 
    "JE" (minä). Il est très facile à conjuguer.
    
    - Avec "Sinä" (tu) : Il faut prendre la forme conjuguer à la première personne du présent du verbe et lui retirer sa terminaison.

    !!! example "Exemple"
        
        | Verbe | Présent (Minä) | Impératif |
        | ----- | -------------- | --------- |
        | Antaa (donner) | Annan (je donne) | Anna ! (donne !) |
        | Nukkua (dormir) | Nukun (je dors) | Nuku ! (dors !) |
        | Juoda (boire) | Juon (je bois) | Juo ! (bois !) |
        | Tehdä (faire) | Teen (je fais) | Tee ! (fais !) |
    
    - Avec "Hän" (il/elle) : il faut remplacer le -A/-dA/-CA par -kOOn.

    !!! example "Exemple"

        | Verbe | Impératif |
        | ----- | --------- |
        | Antaa (donner) | Antakoon (qu'il donne !) |
        | Nukkua (dormir) | Nukkukoon (qu'il dorme !) |
        | Juoda (boire) | Juokoon (qu'il boive !) |
        | Tehdä (faire) | Tehköön (qu'il fasse !) |

    - Avec "Me" (nous) : il faut remplacer le -A/-dA/-CA par -kAAmme.

    !!! example "Exemple"

        | Verbe | Impératif |
        | ----- | --------- |
        | Antaa (donner) | Antakaamme (donnons) |
        | Nukkua (dormir) | Nukkukaamme (dormons) |
        | Juoda (boire) | Juokaamme (buvons) |
        | Tehdä (faire) | Tehkäämme (faisons) |

    - Avec "Te" (vous) : il faut remplacer le -A/-dA/-CA par -kAA.

    !!! example "Exemple"

        | Verbe | Impératif |
        | ----- | --------- |
        | Antaa (donner) | Antakaa (donnons) |
        | Nukkua (dormir) | Nukkukaa (dormons) |
        | Juoda (boire) | Juokaa (buvons) |
        | Tehdä (faire) | Tehkää (faites) |

    - Avec "He" (ils/elles) : il faut remplacer le -A/-dA/-CA par -kOOt.

    !!! example "Exemple"

        | Verbe | Impératif |
        | ----- | --------- |
        | Antaa (donner) | Antakoot (qu'ils donnent !) |
        | Nukkua (dormir) | Nukkukoot (qu'ils dorment !) |
        | Juoda (boire) | Juokoot (qu'ils boivent !) |
        | Tehdä (faire) | Tehkööt (qu'ils fassent !) |

## Les formes nominales du verbe

### Formes nominales des verbes : Les infinitfs

!!! info "Les formes nominatives"
    En plus de nous embêter avec leurs conjugaisons, les verbes en finnois possèdent aussi plusieurs infinitifs. On constate
    5 classes d'infinitif : l'infinitif de base, dit 1^er^ infinitif ou infinitif-A qui s'accompagne du version longue; le 2^nd^ infinitif
    constitué de 2 cas; le 3^ème^ infinitif, dit infinitif-mA, composé de 5 cas; le 4^ème^ infinitif soit la nominalisation des verbes; et
    enfin le 5^ème^ infinitif qui est un peu à part.  

    Tous ces infinitifs seront détaillés par la suite.

    | Infinitif | Cas / Type | Actif | Passif |
    | ------------- | - |----- | ------ |
    |1^er^ | <span style="color:#3D72FF">Court</span> | Tulla |  |
    | - | <span style="color:#3D72FF">Long</span> | Tullakseen |  |
    |  |
    |2^nd^ | <span style="color:#3D72FF">Inessif</span> | Tulle<span style="color:#FD0054">ssa</span> | Tul<u>tae</u><span style="color:#FD0054">ssa</span> |
    | - | <span style="color:#3D72FF">Instructif</span> | Tulle<span style="color:#FD0054">n</span> | Ø |
    |  |
    | 3^ème^ | <span style="color:#3D72FF">Inessif</span> | Tule<u>ma</u><span style="color:#FD0054">ssa</span> | Ø |
    | - | <span style="color:#3D72FF">Elatif</span> | Tule<u>ma</u><span style="color:#FD0054">sta</span> | Ø |
    | - | <span style="color:#3D72FF">Illatif</span> | Tule<u>ma</u><span style="color:#FD0054">an</span> | Ø |
    | - | <span style="color:#3D72FF">Adessif</span> | Tule<u>ma</u><span style="color:#FD0054">lla</span> | Ø |
    | - | <span style="color:#3D72FF">Abessif</span> | Tule<u>ma</u><span style="color:#FD0054">tta</span> | Ø |
    | - | <span style="color:#3D72FF">Instructif</span> | Tule<u>ma</u><span style="color:#FD0054">n</span> | Tul<u>tama</u><span style="color:#FD0054">n</span> |
    |  |
    | 4^ème^ | <span style="color:#3D72FF">Nominatif</span> | Tuleminen |  |
    | - | <span style="color:#3D72FF">Partitif</span> | Tulemista |  |
    |  |
    | 5^ème^ |  | Tulemaisillaan |  |

### Usage des infinitifs

!!! info "Les 5 infinitifs du verbe"
    Dans cette section, nous allons voir l'usage de chacun des infinitifs en détails !

!!! faq "Comment les utiliser ?"

    === "1^er^ infinitif"

        Le premier infinitif est aussi appelé "la forme de base". Un autre nom qu'il porte est l'infinitif-A.  
        En tant que tel, il est considéré comme important pour les apprenants en langues.  

        Il est important de notifier que le 1^er^ infinitif possède 2 formes : une forme courte et une forme longue.  
        
        La forme courte est la forme standard du verbe.

        !!! Example "1^er^ infinitif : forme courte"
            - Puhu<u>a</u> = Parler
            - Näh<u>dä</u> = Voir
            - Men<u>nä</u> = Aller
            - Tarv<u>ita</u> = Avoir besoin (de)

        La forme longue elle est une forme alternative, caractérisée par une terminaison en -Akse- + suffixe possessif. Il se traduit plus ou moins comme
        "Afin de..." ou "Dans le but de...", de manière à indiquer l'objectif d'une action.

        !!! Example "1^er^ infinitif : forme longue"
            - Puhu<u>akseen</u> = Pour parler / Afin de parler
            - Nähd<u>äkseen</u> = Pour voir / Afin de voir
            - Menn<u>äkseen</u> = Pour aller / Afin d'aller
            - Tarv<u>itakseen</u> = Pour avoir besoin / Afin d'avoir besoin  

            Il faut que tu vois ça <u>pour que tu te rendre compte de</u> la situation. => Sinun on nähtävä tämä <u>tajutaksesi</u> tilanteen.

    === "2^ème^ infinitif"

        Le deuxième infinitif peut apparaître au cas <u>inessif</u> pour exprimer des choses qui se passent en même temps. Ceci est utilisé 
        dans la construction du substitut temporel (= temporaalirakenne = temporaalinen lauseevastike). Par exemple, maksaessani signifie 
        "pendant que je payais" et odottaessanne signifie "pendant que vous attendiez". Il existe aussi une forme passive inessive.  

        Il peut également figurer dans le cas <u>instructif</u>. Dans ce cas, il fait référence à la manière dont quelque chose est fait. 
        Vous utilisez ce formulaire dans la construction de substitution modale (= modaalirakenne = modaalinen lauseenvastike). Par exemple, 
        "puhun huuta<u>en</u>" signifie "je parle en criant", tandis que "puhun kuiskat<u>en</u>" signifie "je parle en chuchotant".

    === "3^ème^ infinitif"

        Le troisième infinitif est également appelé l'infinitif mA (mA-infinitiivi), en raison du fait que vous y utiliserez le marqueur -mA-. 
        Cet infinitif ne peut apparaître que dans certains cas. Il n'apparaît jamais seul dans une phrase. Au contraire, il fait toujours partie 
        d'une chaîne verbale, avec un verbe principal combiné avec le troisième infinitif d'un autre verbe.

        Le troisième infinitif est utilisé lorsque deux verbes sont combinés. Les cas les plus courants dans lesquels vous le rencontrerez seront 
        l'illatif (-maan), l'inessif (-massa) et l'élatif (-masta), souvent en relation avec les verbes mennä, olla et tulla.

        !!! tip "Formation"

            | Cas | Nukkua | Tehdä | Ajatella | Kammata | Valita |
            | --- | ------ | ----- | -------- | ------- | ------ |
            | Radical | nukkuma– | tekemä– | ajattelema– | kampaama– | valitsema– |
            | Missä | nukkumassa | tekemässä | ajattelemassa | kampaamassa | valitsemassa |
            | Mistä | nukkumasta | tekemästä | ajattelemasta | kampaamasta | valitsemasta |
            | Mihin | nukkumaan | tekemään | ajattelemaan | kampaamaan | valitsemaan |
            | Millä | nukkumalla | tekemällä | ajattelemalla | kampaamalla | valitsemalla |
            | Abessif | nukkumatta | tekemättä | ajattelematta | kampaamatta | valitsematta |
            | Instructif | nukkuman | tekemän | ajatteleman | kampaaman | valitseman |

        === "Inessif"

            !!! info "Missa (olen lukemassa)"
                Dans les situations où un lieu apparaît sous la forme missä (par exemple "istun <u>ravintolassa</u>" = "Je suis assis <u>au restaurant</u>), 
                nous pouvons également avoir un verbe décrivant l'action que nous faisons à cet endroit (par exemple "istun <u>syömässä</u>" = "je suis assis 
                <u>en train de manger</u>"). Vous pouvez dire les deux dans la même phrase (par exemple "istun <u>ravintolassa</u> <u>syömässä</u>" = 
                "je suis assis <u>au restaurant</u> <u>en train de manger</u>"), mais les deux peuvent également être utilisés sans accompagnement.

            !!! example "Exemple"

                | Finnois | Français |
                | ------- | -------- |
                | Istun bussissa luke<u>massa</u>. | Je suis assis dans le bus en train de lire. |
                | Makaan sängyssä syö<u>mässä</u>. | Je suis couché sur le lit en train de manger. |
                | Seison jonossa odotta<u>massa</u>. | Je suis debout dans la file d'attente en train d'attendre. |
                | Olen eläintarhassa otta<u>massa</u> kuvia. | Je suis au zoo en train de prendre des photos. |

        === "Elatif"

            !!! info "Mistä (tulen lukemasta)"
                Des verbes comme lähteä "partir" et tulla "venir (de)" expriment un mouvement d'éloignement d'un lieu. En finnois, vous utiliserez la forme 
                mistä pour ces situations (par exemple "tulen <u>koulusta</u>" = "Je viens <u>de l'école</u>"). Si nous ajoutons une action que nous revenons 
                de faire, cette action sera sous la forme masta (par exemple "tulen opiskelemasta" = "Je viens <u>d'étudier</u>). Les deux peuvent également apparaître dans la même phrase : Tulen koulusta opiskelemasta. (assez difficilement traductible proprement en français)

            !!! example "Exemple"

                | Finnois | Français |
                | ------- | -------- |
                | Tulen Prismasta osta<u>masta</u> kukkia. | Je reviens de Prisma "d'où" j'ai acheté des fleurs. |
                | Nousen vihdoin sängystäni luke<u>masta</u>. | Je me lève enfin de mon lit "duquel" je lisais. |
                | Lähden kylpyhuoneesta itke<u>mästä</u>. | Je reviens de la salle de bain "duquel" je pleurais. |
                | Tulen Espanjasta lomaile<u>masta</u>. | Je viens d'Espagne "où" j'étais en vacances. |

        === "Illatif" 

            !!! info "Mihin (menen lukemaan)"
                Des verbes comme mennä, saapua et astua expriment un mouvement vers un lieu (par exemple "menen kouluun" = "Je vais à l'école"). 
                Pour les verbes comme pour les noms, nous utiliserons la forme mihin dans ces situations (par exemple "menen kouluun opiskelemaan" 
                = "je vais à l'école étudier").

            !!! example "Exemple"

                | Finnois | Français |
                | ------- | -------- |
                | Menen kauppaan osta<u>maan</u> kukkia. | Je vais au magasin acheter des fleurs. |
                | Lähden puistoon otta<u>maan</u> aurinkoa. | Je pars au parc prendre le soleil. |
                | Saavun baariin tapaa<u>maan</u> kavereitani. | J'arrive au bar pour retrouver mes amis. |
                | Menen Egyptiin sukelta<u>maan</u>. | Je vais en Egypte faire de la plongée |

        === "Adessif"

            !!! info "Millä (opin puhumalla)"
                Ceci est utilisé lorsque vous voulez dire comment vous faites quelque chose, "en faisant quoi".

            !!! example "Example"

                | Finnois | Français |
                | ------- | -------- |
                | Opin suomea puhu<u>malla</u> ystävilleni. | J'apprends le finnois en parlant avec mes amis. |
                | Rentoudun hengittä<u>mällä</u> rauhallisesti. | Je me calme en respirant calmement. |
                | Lukitsen oven kääntä<u>mällä</u> avaimen. | Je verrouille la porte en tournant la clé. |
                | Varoitin ihmisiä huuta<u>malla</u>. | J'averti les gens en criant. |


        === "Abessif"

            !!! info "Abessif (olin syömättä)"
                Ceci est utilisé lorsque vous voulez dire qu'une chose est faite sans qu'une autre soit accomplie.

            !!! example "Example"

                | Finnois | Français |
                | ------- | -------- |
                | Olin 3 päivää syö<u>mättä</u>. | J'ai passé trois jours sans manger. |
                | Poistuin talosta hengittä<u>mättä</u> savua. | J'ai quitté la maison sans respirer la fumer. |
                | Hän aloitti luke<u>matta</u> sääntöjä. | J'ai commencer sans lire les règles. |
                | Lähdin sano<u>matta</u> sanaakaan. | Je suis partie sans dire un mot. |


        === "Instructif"

            !!! info "Pitää sanoman"
                La forme instructive du troisième infinitif est à peine utilisée. Il peut apparaître avec le verbe pitää 
                dans des phrases comme "Mitä minun pitikään sanoman(i)" "Qu'est-ce que je voulais encore dire" ou 
                "Sinun pitää lähtemän" = "Tu devrais partir".
            

    === "4^ème^ infinitif"

        Dans sa forme la plus théorique, le quatrième infinitif n'est utilisé que dans les phrases de type "sinun on tekeminen" (tu dois faire...), 
        extrêmement rares et archaïques.
        Lorsque vous utilisez certains verbes en relation avec d'autres verbes, vous devez parfois les transformer en substantif. En anglais, vous 
        faites cela en ajoutant -ing au verbe (par exemple, "I like read<u>ing</u>" : "J'aime lire"). En finnois, vous pouvez reconnaître cette forme 
        spécifique par la terminaison -minen. La forme –minen du verbe est le verbe « nominalisé » !

    === "5^ème^ infinitif"

        !!! info "Olen nukkumaisillani"

            Le troisième infinitif est également à la base d'une construction de phrase qui se traduit approximativement par 
            « sur le point de faire quelque chose ». On utilise toujours le marqueur –mAisillA- avec un suffixe possessif à la toute fin.

        !!! example "Example"

            | Finnois | Français |
            | ------- | -------- |
            | Olin nukku<u>maisillani</u> kun kello soi. | J'étais <u>sur le point de dormir</u> quand l'alarme a sonné. |
            | Hän oli hyppää<u>mäisillään</u> sillalta. | Il était <u>sur le point de sauter</u> du pont. |


### Formes nominales des verbes : Les participes et agentifs

!!! faq "Que sont les participes ?"
    Un participe est une forme spécifique du verbe, utilisée soit pour transformer un verbe en un adjectif, un nom ou pour 
    remplacer une clause subordonnée. C'est une description assez large. Tous ces participes peuvent être utilisés d'une multitude 
    de manières différentes.

!!! tip "Apparence des participes"

    | Participes | olla | tehdä | tulla | lukea | tavata |
    | ---------- | ---- | ----- | ----- | ----- | ------ |
    | Présent actif | ole<u>va</u> | teke<u>vä</u> | tule<u>va</u> | luke<u>va</u> | tapaa<u>va</u> |
    | Présent passif | ol<u>tava</u> | teh<u>tävä</u> | tul<u>tava</u> | lue<u>ttava</u> | tava<u>ttava</u> |
    | Passé actif | ol<u>lut</u> | teh<u>nyt</u> | tul<u>lut</u> | luke<u>nut</u> | tava<u>nnut</u> |
    | Passé passif | ol<u>tu</u> | teh<u>ty</u> | tul<u>tu</u> | lue<u>ttu</u> | tava<u>ttu</u> |
    | Agent | ole<u>ma | teke<u>mä</u> | tule<u>ma</u> | luke<u>ma</u> | tapaa<u>ma</u> |

!!! info "Les 5 participes"
    Il y a donc 5 participes en finnois : Le présent actif et passif, le passé actif et passif, et l'agent (on peut aussi
    mentionner l'agent négatif)

    === "Le participe présent actif"

        Commençons par regarder le participe-VA. C'est la forme « oleva ». Ce participe a deux usages très courants, ainsi 
        que d'autres plus rares. Examinons les deux situations les plus courantes où le participe VA remplace soit une phrase 
        "joka" soit une phrase "että".

        <strong>1. Participe-VA utilisé pour remplacer les phrases "joka"</strong>

        Premièrement, ce participe remplace les phrases «joka». Par exemple, la phrase "Tyttö, joka istuu vieressäni, lukee" 
        ("La fille qui est assise à côté de moi lit") pourrait être condensée en "Vieressäni istu<u>va</u> tyttö lukee" ("La fille 
        assise à côté de moi lit").


        <strong>2. Participe-VA utilisé pour remplacer les phrases "että"</strong>

        Deuxièmement, ce participe remplace souvent une phrase "että". Par exemple, la phrase "Minä tiedän, että minä olen oikeassa" 
        peut être condensée en "Minä tiedän ole<u>va</u>ni oikeassa". Ces deux phrases sont traduites exactement de la même manière en 
        français : "Je sais que j'ai raison". 


    === "Le participe présent passif"

        <strong>1. Participe-TAVA utilisé pour exprimer la nécessité</strong>
        Le participe présent passif (tehtävä, luettava, sanottava) est le plus souvent utilisé pour exprimer la nécessité. Il peut 
        remplacer une phrase "minun täytyy/pitää"; par exemple "Minun on lue<u>ttava</u> tämä kirja"

        <strong>2. Participe-TAVA utilisé pour remplacer une phrase "joka"</strong>
        Une autre utilisation courante du participe TAVA est l'équivalent passif de la phrase "joka". La phrase "Katson taloa, jota 
        rakennetaan" peut être condensée en "Katson rakenne<u>ttavaa</u> taloa". Ces deux phrases sont traduites par "Je regarde la 
        maison qui se construit".

    === "Le participe passé actif"

        <strong>1. Participe-NUT utilisé au passé</strong>
        On le retrouve dans la conjugaison des verbes au temps imparfait négatif, au temps parfait ou au temps plus-que-parfait.  
        Par exemple En syö<u>nyt</u>, "Juo<u>nut</u> enkä nukku<u>nut</u> viikkoon." = "Je n'ai pas mangé, bu ni dormi de toute la semaine."

        <strong>2. participe-NUT utilisé pour remplacer une phrase "joka"</strong>
        Le participe passé actif est principalement utilisé pour remplacer une phrase "joka" au passé. Par exemple "Kirja, joka oli pöydällä, 
        on hävinnyt" ("Le livre qui était sur la table a disparu") peut devenir "Pöydällä ol<u>lut</u> kirja on hävinnyt".


    === "Le participe passé passif"

        <strong>1. Participe-TU utilisé au passé</strong>
        Le participe passé passif est utilisé avec les temps passés lorsqu'ils sont passifs.  

        - Asuntoa ei siivo<u>ttu</u> eikä tuulete<u>ttu</u>. = L'appartement n'a pas été nettoyé ni aéré.
        - Auto on katsaste<u>ttu</u>, rekisteröi<u>ty</u> ja myy<u>ty</u>. = La voiture a été inspectée, immatriculée et vendue.

        <strong>2. Participe-TU comme adjectifs</strong>

        - Tykkään sekä keite<u>ty</u>istä että paiste<u>tu</u>ista perunoista. = J'aime les pommes de terre bouillies et cuites au four.
        - Ava<u>ttu</u>a pakettia ei voi palauttaa. = Un colis ouvert ne peut pas être retourné.

    === "Le participe agent"

        L'utilisation principale du participe agent est comme un adjectif exprimant l'auteur d'une action.

        - [Äidin teke<u>mä</u> ruoka] tuoksuu herkulliselta. = [La nourriture que maman a faite] sent bon.

        - [Kirjoitta<u>ma</u>ni novelli] myytiin nopeasti loppuun. = [Le roman que j'ai écrit] s'est vendu rapidement.

## Mais comment forme-t-on alors le futur ?

!!! warning "Le futur"

    il n’y a pas de futur en finnois ! Ca ne signifie pas cependant que les finnois ne peuvent pas parler du futur. Ils 
    le font juste d’une autre façon que par la conjugaison d’un verbe au futur.

    ⭐ - Minä menen olohuoneeseen. = Je vais dans le salon.  
    ⭐ - Minä menen olohuoneeseen. = Je vais aller dans le salon.  

    Pour parler des événements à venir en finnois, il va falloir adopter une façon différente de penser. Le français «Je vais dans le salon» 
    se traduit par “Minä menen olohuoneeseen“. Jusqu’ici, tout est clair, les deux langues utilisent le présent. Néanmoins, là où le français 
    utiliserait le futur — «J’irai dans le salon» — on utiliserait toujours la même phrase : “Minä menen olohuoneeseen“.

!!! faq "Mais alors comment ça marche ?"
    Bien que le futur n'existe pas par le biais d'une conjugaison comme en français, cela ne signifie pas non plus que les finlandais sans dans
    l'incapacité de prévoir des choses dans l'avenir ou de parlais du futur. Le présent finnois à proprement parlé n'est pas réellement le
    présent, mais plutôt du "Non-passé", un peu à la manière du Japonais qui fonctionne sur le même principe.
    Pour parler alors de l'avenir sans ambigüité, il y a 5 manières de le faire.

### Formation

!!! note
    <u>1. Utiliser le présent pour exprimer le futur : le contexte</u>  

    En finnois, on utilise normalement le présent pour exprimer ce qui arrive dans le futur. Dans ce genre de phrases, on peut déterminer 
    si on parle du présent ou du futur uniquement par le contexte de la phrase.

    !!! example "Exemple"

        | Finnois | Français |
        | ------- | -------- |
        | Minä autan sinua. | Je t’aide. – <span style="color:#4BADE5">OU</span> – Je t’aiderai. |
        | Menetkö töihin bussilla? | Est-ce que tu vas au travail en bus ? – <span style="color:#4BADE5">OU</span> – Est-ce que tu iras au travail en bus ? |
        | Odota, soitan äidille! | Attends, j’appelle maman ! – <span style="color:#4BADE5">OU</span> – Attends, je vais appeler maman ! |
        | Menemme kauppaan autolla. | Nous allons au magasin en voiture. – <span style="color:#4BADE5">OU</span> – Nous allons aller au magasin en voiture. |

    <u>2. Utiliser des adverbes pour faire référence au futur</u>  

    On peut compléter une phrase au présent avec une expression temporelle pour indiquer qu’on parle d’un évènement futur tel que pian «bientôt», 
    huomenna «demain», ensi viikolla «la semaine prochaine» ou ensi vuonna «l’année prochaine». C'est aussi un moyen en français de se passer de
    la conjugaison futur.

    !!! example "Exemple"
        | Finnois | Français |
        | ------- | -------- |
        | Teen kotitehtäväni <u>huomenna</u>. | Je ferai mes devoirs <u>demain</u>. |
        | Maija tulee vasta <u>ylihuomenna</u>. | Maija ne viendra qu’<u>après-demain</u>. |
        | <u>Ensi viikolla</u> sataa lunta. | Il neigera <u>la semaine prochaine</u>. |
        | <u>Ensi maanantaina</u> annan sinulle kirjan. | Je te donnerai le livre <u>lundi prochain</u>. |

    <u>3. Utiliser l’objet pour faire référence à une action future complète</u>  

    Dans les phrases qui contiennent un objet, le choix entre l’utilisation du partitif ou du génitif peut permettre d’exprimer son intention.

    !!! tip "<strong>La règle de l’objet</strong>"

        Voici deux règles afin de choisir correctement la déclinaison à utiliser. Pour plus de détails, <a href="http://127.0.0.1:8000/experience/déclinaison/#les-regles-de-lobjet-en-finnois">cliquez ici</a>

        - Quand on utilise le présent et un objet décliné au partitif, celà signifie généralement que l’action est en cours.  
        - Quand on utilise le présent et un objet décliné au génitif, celà signifie qu’on a l’intention de faire quelque chose dans le futur.

    | # | Finnois | Français |
    | - | ------- | -------- |
    | 1 | Luen kirjaa. | Je lis un livre. |
    | 2 | Luen kirjan. | Je vais lire un livre. |
    | 1 | Katsomme elokuvaa. | On regarde un film. |
    | 2 | Katsomme elokuvan. | On va regarder un film. |
    | 1 | Kirjoitan sähköpostia sinulle. | Je t’écris un e-mail. |
    | 2 | Kirjoitan sähköpostin sinulle. | Je vais t’écrire un e-mail. |

    <u>4. Utiliser un verbe pour exprimer son intention</u>

    Si on veut insister sur le caractère futur de l’action, on peut utiliser le verbe Aikoa. Il signifie «avoir l’intention de, prévoir de». 
    Dans le langage parlé, on utilisera aussi le verbe meinata, qui a le même sens.

    | Finnois | Français |
    | ------- | -------- |
    | Aion ostaa auton. | Je prévois d’acheter une voiture. |
    | Mihin aiotte matkustaa? | Où as-tu l’intention de voyager ? |
    | Aiomme mennä naimisiin ensi kesänä. | Nous prévoyons de nous marier l’été prochain. |
    | Antti aikoo muuttaa Helsinkiin. | Antti a l’intention de déménager à Helsinki. |
    | Mitä ohjelmaa aiot katsoa? | Quel programme est-ce que tu vas regarder ? |
    | Meinaatko käydä kaupassa tänään? | Tu prévois d’aller au magasin aujourd’hui ? |

    Cette façon d’exprimer le futur est sûrement la plus simple lorsque l’on apprend la langue, mais ce n’est certainement pas la façon la plus 
    commune de le faire. À la place de “Mihin aiot matkustaa“, il est bien plus courant de demander “Mihin matkustat ensi kesänä“, où “ensi kesänä” 
    exprime que l’on parle d’un évènement futur.

    <u>5. Utiliser un verbe de mouvement</u>

    Des verbes tels que mennä (aller), tulla (venir) et lähteä (partir) peuvent aussi être utilisés pour exprimer le futur indirectement. Ils 
    expriment un mouvement, ce qui permet de pointer vers des évènements à venir.

    | Finnois | Français |
    | ------- | -------- |
    | Menen uimaan. | Je vais aller nager. |
    | Tuletko juhliini? | Est-ce que tu viens à ma fête ? |
    | Lähden kauppaan. | Je vais au magasin. |

    Certains de ces verbes de mouvement nécessitent d’utiliser le troisième infinitif quand on associe deux verbes. 
    C’est une rection verbale.

    | Finnois | Français |
    | ------- | -------- |
    | Oletko tulossa meille syömään? | Est-ce que tu viens manger ? |
    | Olen tulossa juhliin tanssimaan. | Je viens à la fête pour danser. |
    | He ovat menossa Italiaan opiskelemaan. | Ils vont en Italie pour étudier. |
    | Olen menossa nukkumaan. | Je vais aller dormir. |
    | Olen lähdössä juhlimaan! | Je vais faire la fête ! |



## La négation

!!! info "La négation en finnois : Différent, mais efficace"
    En finnois, la construction de phrase négative est assez particulière. En effet, il ne s'agit pas simplement de rajouter un petit mot avant ou 
    après le verbe comme en anglais ou en français ; en fait, il s'agit d'une conjugaison à part entière. Il ne faut pas en avoir peur, c'est assez 
    intuitif en vérité, surtout pour le présent.

!!! note "L'exemple de la négation au présent"
    Pour former le négatif en finnois, c'est très facile à retenir et à faire. La négation se divise en 2 parties : l'"auxiliaire négatif" (on va en parler) 
    suivi du verbe à la forme négative.  
    Pour donner une idée de la négation, voici un tableau qui compare la forme affirmative et négative du verbe "Tulla" (venir)

    | Présent | Affirmatif | Négatif |
    | ------- | ---------- | ------- |
    | Minä | Tulen | En tule |
    | Sinä | Tulet | Et tule |
    | Hän | Tulee | Ei tule |
    | Me | Tulemme | Emme tule |
    | Te | Tulette | Ette tule |
    | He | Tulevat | Eivät tule |

    Si on regarde bien, on observe que l'auxiliaire de négation change à toutes les personnes, il s'agit des six formes de "Ei" qui signifie "Non", 
    qui changera selon la personne. Et on peut constater que le verbe "Tulla" pert sa terminaison personnelle. C'est tout. La formation du négatif 
    se forme avec l'auxiliaire négatif correspondant à la personne désirée suivi du verbe conjugué à qui on a retiré sa terminaison personnelle.
    Cette règle s'appliquera de la même manière sur tout les verbes.
    !!! example "Application"
        - <a href="../audios/conjugaison/negatif/sunkaa.ogg">▶️</a> Je <span style="color:#04C294">vais</span> à l'école avec toi => Menen kouluun sinun kanssa  <span style="color:#04C294">(Affirmatif)</span>
        - <a href="../audios/conjugaison/negatif/buusilla.ogg">▶️</a> Je <span style="color:#C21004">ne vais pas</span> à l'école en bus => En mene kouluun bussilla <span style="color:#C21004">(Négatif)</span>
          
        - <a href="../audios/conjugaison/negatif/sisko.ogg">▶️</a> Nous <span style="color:#04C294">mangerons</span> à la maison vendredi mais ma soeur <span style="color:#C21004">ne sera pas</span> là. => 
        <span style="color:#04C294">Syömme</span> kotiin perjuntaina mutta siskoni <span style="color:#C21004">ei ole</span> siellä. (usage mixte)

## Poser une question en finnois

!!! info "La question"
    Pour formuler une question en finnois, on peut dire que c'est plutôt similaire au français malgré une petite différence notable.  

!!! tip "La question fermée"
    La formulation d'une question fermée (soit "est-ce que...) est assez facile, car similaire au français, il suffit de faire l'inversion sujet-verbe 
    MAIS il faut aussi rajouter à la fin du verbe le suffixe **-ko/-kö** qui exprime à lui seul le "est-ce que". On a pu l'apercevoir dans l'expression 
    de "Oletko koskaan" dans la partie sur le Parfait.
    !!! example "Application"
        - Peux-tu me donner l'eau, s'il te plaît ? => Voit<u>ko</u> sä antaa minulle vettä, ole kiltti ?  
        - Est-ce qu'ils ont besoin d'aide ? => Tarvitsevat<u>ko</u> he apua ?
!!! tip "La question ouverte"
    La formulation des questions ouvertes est similaire au français de part l'usage des pronoms interrogatifs (Quoi, qui, où...). La particule **-ko/-kö** ne 
    s'utilise pas ici.
    !!! example "Application"
        - Qui êtes-vous ? => Kuka te olette ?  
        - Väinö ! Où es-tu ?! => Väinö ! Missä olet ?!  
        - Quelle genre de voiture est-ce ? => Millainen auto on ?
        - Où vont les enfants ? => Mihin lapset menevät ?

## Conjugueur de verbe finnois : Présent de l'indicatif

!!! info "Conjugueur" 
    Ce conjugueur en langage python permet comme son nom l'indique, de conjuguer au présent de l'indicatif les verbes qui sont
    fournis en entrée.

    !!! faq "Comment fonctionne-t-il ?"
        Le prgramme fonctionne en plusieurs étapes :

        - Dans un premier temps, il va set le paramètre de l'harmonie vocalique (a / ä).

        - Ensuite, il va comparer l'entrée avec la liste de verbes irréguliers stockés afin de pouvoir conjuguer le verbe de la bonne
        façon : Si le verbe est irrégulier, alors le programme conjuguera "manuellement" le verbe; sinon il passe à l'étape suivante.

        - Le verbe sera ensuite analysé par une première fonction qui determinera le groupe du verbe en observant sa terminologie. 
        Cette dernière renvoie un entier : le groupe du verbe.

        - Une fois le groupe determiné, une seconde fonction s'occupera de conjuguer le verbe et renverra le résultat.

    !!! warning "Précision"
        Le programme reste cependant imparfait. En effet, si vous vous rappeler de ce dont on a parlé dans l'introduction, les verbes ainsi que les
        substantifs, adjectifs... sont soumis à la gradation consonnantique. Ce programme ne le prends pas en compte car difficile à mettre en place.
        Par exemple, avec le verbe "Kuu<u>nn</u>ella" (écouter), qui subit une gradation au niveau de "nn", le programme renverra > "Kuu<u>nn</u>elen" 
        au lieu de "Kuu<u>nt</u>elen".

    


??? tip "Le programme"
    ``` python
        def groupe(verbe : str) -> int: #Renvoie groupe du verbe

            if verbe[-2:] in term_g1 : return 1 #Groupe 1 -VA
            elif verbe[-2:] == 'da' or verbe[-2:] == 'dä' : return 2 #Groupe 2 -dA
            elif verbe[-3:] in term_g3 : return 3 #Groupe 3 -CCA/-stA
            elif verbe[-2:] == 'ta' or verbe[-2:] == 'tä' : #Groupe 4, 5 ou 6 -VtA
                if verbe[-3:] in term_g4 : return 4 #Groupe 4 -VtA -> V(A, O, U)
                elif verbe[-3:] == 'ita' or verbe[-2:] == 'itä' : return 5 #Groupe 5 -itA
                elif verbe[-3:] == 'eta' or verbe[-2:] == 'etä' : return 6 #Groupe 6 - etA
            

        def gradation(verbe : str, groupe : int, gradation_dict : dict) -> str:

            
            
            return

        def flexion_présent_rég(verbe : str, term_personnelles : list) -> str: #conjugaison

            if harmonie == 'ä': #Harmonie vocalique A
                term_personnelles[5] = 'vät'

            ###Régulier
            
            if groupe(verbe) == 1 :
                radical = verbe[:-1] #suppr -_A
                term_personnelles[2] = verbe[-2]
                
            if groupe(verbe) == 2 :
                radical = verbe[:-2] #suppr -dA

            if groupe(verbe) == 3 :
                radical = verbe[:-2] #suppr -CA
                radical += 'e'
                term_personnelles[2] = 'e'
            
            if groupe(verbe) == 4 :
                if verbe[-3:] == 'ata' or verbe[-3:] == 'ätä':
                    radical = verbe[:-2]
                    radical += harmonie
                else : 
                    radical = verbe[:-2]
                    radical += harmonie
                    term_personnelles[2] = harmonie
                
            if groupe(verbe) == 5 :
                radical = verbe[:-2]
                radical += 'tse'
                term_personnelles[2] = 'e'
            
            if groupe(verbe) == 6 :
                radical = verbe[:-2]
                radical += 'ne'
                term_personnelles[2] = 'e'

            for terminaison in term_personnelles :
                print(radical + terminaison)

        def flexion_présent_irr(verbe : str, term_personnelles : list) -> str: #Cas où vb = irr

            if harmonie == 'ä': #Harmonie vocalique A
                term_personnelles[5] = 'vät'

            pronoms = 0

            if verbe == 'Olla' : #G3 irr.3P

                term_personnelles[2] = 'n'

                for terminaison in term_personnelles :
                    if pronoms == 2 or pronoms == 5 : print('O'+terminaison)
                    else : print('Ole' + terminaison)
                    pronoms += 1

            if verbe == 'Nähdä' :
                term_personnelles[2] = 'e'
                for terminaison in term_personnelles :
                    if pronoms == 2 or pronoms == 5 : print('Näke'+terminaison)
                    else : print('Näe' + terminaison)
                    pronoms +=1

            if verbe == 'Tehdä' :
                term_personnelles[2] = 'e'
                for terminaison in term_personnelles :
                    if pronoms == 2 or pronoms == 5 : print('Teke'+terminaison)
                    else : print('Tee' + terminaison)
                    pronoms +=1

            if verbe == 'Selvitä' :
                radical = verbe[:-2]
                radical += harmonie
                term_personnelles[2] = harmonie
                for terminaison in term_personnelles :
                    print(radical + terminaison)

        #Utilitaires

        term_g1 = ["aa", "ää", "ea", "eä", "ia", "iä", "oa", "ua", "yä"]
        term_g3 = ["nna", "nnä", "lla", "llä", "rra", "rrä", "sta", "stä"]
        term_g4 = ["ata", "ätä", "ota", "ötä", "uta", "ytä"]

        term_personnelles = ['n', 't', '', 'mme', 'tte', 'vat']
        verbe_irr = ['Olla', 'Nähdä', 'Tehdä', 'Selvitä']

        gradation_dict = {'kk' : 'k', 'pp' : 'p', 'tt' : 't', 
                        'nt' : 'nn', 'nk' : 'ng', 'mp' : 'mm',
                        'lt' : 'll', 'rt' : 'rr',
                        't' : 'd', 'k' : '', 'p' : 'v',
                        'lk' : 'lj', 'rk' : 'rj'}

        #Entrée
        verbe = capitalize(input('Entrez un verbe (en finnois) : '))
        harmonie = verbe[-1]
        if verbe in verbe_irr : flexion_présent_irr(verbe, term_personnelles)
        else : flexion_présent_rég(verbe, term_personnelles)
    ```