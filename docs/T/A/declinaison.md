# Le système déclinatoire finnois

!!! info "Introduction"
    Le finnois est une langue qui fait usage de beaucoup de déclinaison, s'appliquant aussi bien sur les 
    noms communs que les noms propres ainsi que les verbes.  
    Il y a 15 cas de 'bases', mais en vérité il y en a un peu plus. On verra ça au fur et à mesure.
      
    Chose importante à précisé avant d'aller plus loin : Le finnois est une langue sans détermiant. 
    Ainsi, les mots "un/une" "le/la" "de/des" et "les" n'existe pas.

## Les cas dit sémantique

!!! info "Les 4 cas sémantiques du finnois"
    Les cas sémantiques sont les cas que l'on pourrait qualifier de fondamentaux. Il s'agit du Nominatif, 
    de l'Accusatif du Génitif et du Partitif.
    !!! warning "L'accusatif"
        En vérité, il existe un quatrième cas qui est l'accusatif, mais celui-ci n'existe en soit réellement 
        uniquement pour les pronoms personnelles. On détaillera cela plus tard.

### Le nominatif

!!! tip "Le nominatif"
    Le nominatif est le cas de "base" des mots. Il s'agit du cas qui exprime le sujet du verbe ou l'attribut du sujet.  
    Les mots sont initialement dans le cas nominatif.  
    Le nominatif possède 2 formes, le singulier et le pluriel.  
    Le singulier étant la forme de base _(Lapsi = un/l'enfant ; Koira = un/le chien)_ et le pluriel est tout 
    simplement la forme plurielle des noms _(Lapset = des/les enfants ; Koirat = des/ les chiens)_ qui est reprèsenté 
    par un _"-t"_ à la fin du mot.
    !!! example "Application"
        - Les enfants jouent dans la maison => Lapse<u>t</u> Leikkivät talossa
        - La fille est petite => Tyttö on pieni

### Le génitif

!!! tip "Le génitif"
    L'usage du génitif est très varié. En effet, contrairement à dans beaucoup d'autres langues, le génitif 
    finnois ne se contente pas d'indiquer la possession.  
    Le génitif permet dans sa fonction fondamentale de montrer la possession.  
    Il s'utilise en tant qu'objet pour indiquer une action complète ou à venir (un moyen d'exprimer le futur)  
    Pour exprimer le devoir.  
    Et devant certaines postpositions.  
    Il s'exprime au moyen d'un -n final  
    !!! example "Application"
        - **Possession** - L'oiseau de Lisa => Lisa<u>n</u> lintu  
        - **Action complète** - Je mange la pomme *(dans son intégralité)* => Syön omena<u>n</u>  
        - **Action à venir** - Je construirai la maison => Minä rakennan talo<u>n</u>  
        - **Le devoir** - Il doit partir => Hän<u>en</u> täytyy lähteä  
        - **Devant des postpositions** - Je vois ce chien avec mon père => Näen tämän koiran isä<u>n</u> kanssa  

### Le partifif

!!! tip "Le partitif"
    L'usage du partitif est plutôt étrange et très fréquent. Il est représenté par un -a/-ä, -ta/-tä, -tta/-ttä.  
    Il exprime littéralement la partie d'un tout. Il existe en français sous la construction "de le..." (Je mange de la pomme).  
    Il s'utilise après les nombres.  
    Après les mots qui exprime une masse ou une quantité.  
    Avec la possession d'une quantité.   
    Dans les phrases négatives.  
    Phrase irrésultive (action en cours)  
    Avec certains verbes.  
    Avec des prépositions.
    !!! example "Application"
        - **Nombre** - J'ai trois chiens => Minulla on kolme koira<u>a</u>  
        - **Quantité** - Une bouteille de vin => Pullo viini<u>ä</u>  
        - **Possession de masse** - J'ai du café => Minulla on Kahvi<u>a</u>  
        - **Pour une quantité non-déterminée:** Vous buvez de l'eau => Juotte ve<u>ttä</u> 
        - **Négation** - Je n'ai pas de chien => Minulla ei ole koira<u>a</u>  
        - **Certains verbes** - Je t'aime => Rakastan su<u>a</u>  
        - **Irrésultif** - Je suis en train de lire un livre => Luen kirja<u>a</u>  
        - **Préposition** - Je suis venu à la maison avant toi => Tulin kotiin ennen su<u>a</u>

### L'accusatif

!!! danger "L'accusatif"
    L'accusatif est un cas un peu à part, en fait, il s'agit d'une fusion entre le génitif singulier et le 
    nominatif pluriel. Ainsi, dans les phrases où l'objet singulier est au génitif, alors ce même objet si 
    l'on souhaite parler au pluriel, obtiendra à la place la terminaison nominative singulière.  
    
    !!! example "Application"
        - Je mange les pommes *(dans son intégralité)* => Syön omena<u>t</u>  
        - Je vois des chiens => Näen koira<u>t</u>  
    En vérité l'accusatif n'existe réellement qu'avec les pronoms personnels :
    
    | Nominatif | Accusatif |
    | --------- | --------- |
    | Minä | Minut |
    | Sinä | Sinut |
    | Hän | Hänet |
    | Me | Meidät |
    | Te | Teidät |
    | He | Heidät |

### Les règles de l'objet en finnois

!!! faq "Comment choisir le bon cas ?"

    Il est difficile pour un débutant de choisir le bon cas
    pour définir l'objet; Le nominatif, l'accusatif, génitif ou le partitif,
    que choisir ?
    Il existe pour cela différentes règles qui régissent les cas à appliquer
    selon le contexte.

    ``` mermaid
        graph LR
            aff_negat[La phrase est-elle affirmative ou négative ?] --> |Négative| part[Partitif]
            nb_indef[Est-ce que l'objet est en quantité indéfini ou précédé d'un nombre ?] --> |Oui| part
            act_actu[Est-ce que l'action sur l'objet est en cours ?] --> |Oui| part
            vb_part[Est-ce que le verbe requière l'usage du partitif ?] --> |Oui| part
            pr_perso[Est-ce que l'objet est un pronom personnel ?] --> |Oui| acc[Accusatif]
            obj_plur[Est-ce que l'objet est au pluriel ?] --> |Oui| nom_plur[Nominatif Pluriel]
            imp[Est-ce que la phrase est à l'impératif ?] --> |Oui| nom[Nominatif]
            passiivi[Est-ce que la phrase est passive ?] --> |Oui| nom
            neces[Est-ce que la phrase a une construction exprimant la nécessité ?] --> |Oui| nom
            olla_adj[Est-ce que la phrase a une construction 'Olla' + adj + vb ?] --> |Oui| nom
            poss[Est-ce que la pharse a une construction possessive ?] --> |Oui| nom
            else[Si aucune des règles précédentes ne s'applique, alors :] --> gen[Génitif]

    ```

## Les cas dit locatifs

!!! info "Les 6 cas locatifs du finnois"
    En finnois, il y a 6 cas qui exprime une localisation en finnois qui possède chacun un usage précis. 
    Je n'expliquerai ici que leur usage fondamental et non les autres usages.  

### Les cas locatifs "Internes"

!!! tldr "Les cas internes"
    Il y a en finnois 3 cas dit internes qui sont l'inessif, l'élatif et l'illatif.  
    !!! tip "L'inessif"
        il se caractérise par -ssa/-ssä
        !!! note "Usage"
            - **Pour dire que quelque chose est ou se passe dans quelque chose :** Je suis dans la maison => Olen talo<u>ssa</u>  
            - **Quand un objet possède quelque chose :** La voiture a quatre roues => Auto<u>ssa</u> on neljä rengasta  
            - **Avec le verbe Käydä (visiter) :** J'ai visité la Finlande => Kävin Suome<u>ssa</u>  
    !!! tip "L'élatif"
        Il se caractérise par -sta/-stä
        !!! note "Usage"
            - **Pour indiquer un point de départ / un lieu de séparation :** Nous venons de France => Tulemme Ranska<u>sta</u>  
            - **Pour donner son opinion :** D'après moi, Paris est une belle ville => Minu<u>sta</u> Pariisi on kaunis kaupunki  
            - **Avec certains verbes :** Il aime le chocolat => Pitää sukla<u>sta</u>  
    !!! tip "L'illatif"
        Il se caractérise par un redoublement de la voyelle suivi de -n
        !!! note "Usage"
            - **Pour dire dans quoi l'on va :** Je vais à l'école => Menen Koulu<u>un</u>  
            - **Pour indiquer une limite dans le temps :** Je travaille jusqu'à lundi => Työskentelen maanantai<u>hin</u>  

### Les cas locatifs "Externes"

!!! tldr "Les cas externes"
    Il y a en finnois 3 cas dit internes qui sont l'adessif, l'ablatif et l'allatif.
    !!! tip "L'adessif"
        Il se caractérise par -lla/-llä
        !!! note "Usage"
            - **Pour dire qu'on est sur ou proche de quelque chose :** Le pain est sur la table => Leipä on pöydä<u>llä</u>  
            - **Pour parler de lieux ouverts :** On attend à l'arrêt de bus => Odatamme bussipysäki<u>llä</u>  
            - **Pour exprimer la possession (car le verbe avoir n'existe pas):** Vous avez un chat = Tei<u>llä</u> on kissa  
            - **Pour exprimer le moyen :** Je vais à Paris en Bus => Menen Pariisiin bussi<u>lla</u>  
    !!! tip "L'adessif"
        Il se caractérise par -lta/-ltä
        !!! note "Usage"
            - **Quand on quitte un lieu ouvert :** Je pars de la cour => Lähden pois piha<u>lta</u>.  
            - **Pour parler de l'heure :** Je mange à 9 heures => Syön yhdeksä<u>ltä</u>  
    !!! tip "L'allatif"
        Il se caractérise par -lle
        !!! note "Usage"
            - **Pour indiquer sur quoi l'on va :** Je vais dans la cour => Menen piha<u>lle</u>  
            - **Pour indiquer un destinataire :** Je parle à mon père => Puhun isä<u>lle</u>  
             - **Pour indiquer un destinataire :** Tu lui donnes de la viande => Annat häne<u>lle</u> lihaa

---

## Flexion des pronoms et des determinants démonstratifs

!!! info "Les flexions"
    Dans cette partie nous aborderons les flexions des pronoms et des determinants, des éléments essentielle dans
    l'apprentissage de la langue.
    Ce n'est pas très compliqué, vous pourrez voir une certaine redondance dans la forme que prennent les pronoms
    et determinants, rendant plus aisée la tâche.

### Pronoms personnelles et relatifs

!!! tip "Pronoms personnelles"

    | Cas | Je | Tu | Il/Elle | Nous | Vous | Ils/Elles |
    | --- | -- | -- | ------- | ---- | ---- | --------- |
    | Nominatif | Minä | Sinä | Hän | Me | Te | He |
    | Partitif | minua | sinua | häntä | meitä | teitä | heitä |
    | Genitif | minun | sinun | hänen | meidän | teidän | heidän |
    | Accusatif | minut | sinut | hänet | meidät | teidät | heidät |
    | Inessif | minussa | sinussa | hänessä | meissä | teissä | heissä |
    | Elatif | minusta | sinusta | hänestä | meistä | teistä | heistä |
    | Illatif | minuun | sinuun | häneen | meihin | teihin | heihin |
    | Adessif | minulla | sinulla | hänellä | meillä | teillä | heillä |
    | Ablatif | minulta | sinulta | häneltä | meiltä | teiltä | heiltä |
    | Allatif | minulle | sinulle | hänelle | meille | teille | heille |

!!! tip "Pronoms relatifs"

    - "Joka" est un pronom relatif qui peut être traduit par "Qui" ou "Quoi".  
    - "Mikä" est lui davantage comparable au "What" en anglais. Il se traduit
     aussi par "Quoi" ou "Quelle". Il s'agit d'un pronom interrogatif.  

    | Cas | Singulier | Pluriel |
    | --- | --------- | ------- |
    | Nominatif | joka | jotka |
    | Partitif | jota | joita |
    | Genitif | jonka | joiden |
    | Inessif | jossa | joissa |
    | Elatif | josta | joista |
    | Illatif | johon | joihin |
    | Adessif | jolla | joilla |
    | Ablatif | jolta | joilta |
    | Allatif | jolle | joille |

    ??? example "Exemple"
        - Kirja, <u>joka</u> on pöydällä, on mielenkiintoinen. = Le livre, <u>qui</u> est sur la table, est intéressant.  
        - Iso mies, <u>jonka</u> auto on punainen, on täällä. = L'homme, <u>à qui</u> la voiture est rouge, est ici.  
        - Nuori nainen, <span style="color:#4BADE5"><u>jolla</u> on</span> kaunis kaulakoru, menee töihin. = La jeune femme, 
        <u>qui</u> a un beau collier, va au travail.
        
        !!! warning "La structure "-lla on" "
            Dans la phrase "Nuori nainen, <span style="color:#4BADE5"><u>jolla</u> on</span> kaunis kaulakoru, menee töihin." 
            il n'y a pas de verbe "avoir", pourtant "jolla on kaunis kaulakoru" ce traduit "qui a un beau collier". En fait, 
            il faut savoir que le verbe "Avoir" n'existe pas en finnois, pour pouvoir alors former la possession, il faut donc
            utilisé une structure spécifique : "-llA + Olla (être)".  

            - Minu<u>lla on</u> koira = J'ai un chien  
            - Naise<u>lla oli</u> sormus = La femme avait une bague  


    | Cas | Mikä |
    | --- | --------- |
    | Nominatif | Mikä |
    | Partitif | Mitä |
    | Genitif | Minkä |
    | Accusatif | Mitkä |
    | Inessif | Missä |
    | Elatif | Mistä  |
    | Illatif | Mihin | 
    | Adessif | Millä |
    | Ablatif | Miltä |
    | Allatif | Mille |

    ??? example "Exemple"

        - <u>Mikä</u> tämä on ? = Qu'est-ce que c'est ?  
        - <u>Mikä</u> sinun lempiväri on ? = Quelle est ta couleur préférée ?  
        - <u>Mitä</u> materiaalia tämä sormus on? = En quelle matière est cette bague ?

!!! tip "Pronoms personnels renforcés"

    Les pronoms renforcés sont, pour rappel, "moi-même", "toi-même"...
    Ils se forme en finnois avec la base "itse" + suffixe possessif que voici :

    | Personne | Itse |
    | -------- | ---- |
    | Minä -ni | Itse<u>ni</u> |
    | Sinä -si | Itse<u>si</u> |
    | Hän -nsä | Itse<u>nsä</u> |
    | Me -mme | Itse<u>mme</u> |
    | Te -tte | Itse<u>tte</u> |
    | He -nsä | Itse<u>nsä</u> |

    "Itse" se décline comme tout les autres (on prendra l'exemple avec "Itseni" (moi-même))

    | Cas | Itseni |
    | --- | ---- |
    | Nominatif | Itseni |
    | Partitif | Itseäni |
    | Génitif | Itseni |
    | Inessif | Itsessäni |
    | Elatif | Itsestäni |
    | Illatif | Itseeni |
    | Adessif | Itselläni |
    | Ablatif | Itseltäni |
    | Allatif | Itselleni |
    