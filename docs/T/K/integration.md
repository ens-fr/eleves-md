# Chapitre : Intégration TS

$f$ est une fonction sur un intervalle
$Cf$ la représentation de $f$ dans un repère $( O; I ; J )$

!!! faq "Problème"
    Comment déterminer l'aire sous une courbe de fonction ?
    
!!! example "Exemple" 
    Déterminer l'aire $\mathscr A$ hachurée:

    ![Cf + integrale](images/Courbe1.png){width=900}

## I - integrale d'une fonction continue et positive

!!! info "Definition"
    Soit $f$ un fonction continue et positive sur un intervalle.
    On appelle intégrale de $f$ entre $a$ et $b$, l'aire délimitée par: 
    la courbe $Cf$, l'axe des abscisses, la droite d'équation $x=a$ et la droite d'équation $x=b$.
    Cette intégrale se note: $\int_a^b f(x) \, \mathrm dx$ 

    ![cf + integrale (a -> b)](images/Courbe2.png){width=900}

    L'aire est exprimée en unité d'aire du repère (le rectangle de cotés $OI$×$OJ$) 
    !!! tip "Remarque"
        $a$ et $b$ sont appelés les bornes de l'intégrale.


!!! note "Méthodes"
    Pour calculer cette aire, il existe differente méthode de calcul par valeur approchée d'intégrale: 

    - [Méthode de Monte-Carlo](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Monte-Carlo)
    - [Méthode de trapèzes](https://fr.wikipedia.org/wiki/M%C3%A9thode_des_trap%C3%A8zes), (ou Méthode des rectangles)
    - [Méthode de Simpson](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Simpson)


!!! example "Exemples avec: $∫$"
    !!! info inline ""
        $$\int_0^1 x^2 \, \mathrm dx$$
    !!! info inline ""
        $$\int_1^m \dfrac{1}{x} \, \mathrm dx$$ 
    !!! info inline ""
        $$\int_0^1 e^{x} \, \mathrm dx$$ 
    !!! info  inline ""
        $$\int_0^1 e^{-x^2} \, \mathrm dx$$
    !!! info  inline ""
        $$\int_0^1 x\ln(x) \, \mathrm dx$$
    !!! info  inline ""
        $$\int_0^1 \dfrac{7x+9}{2} \, \mathrm dx$$

    

!!! warning "Algorithme"
    - Faire un programme caculant l'aire de $\int_0^1 x^2\, \mathrm dx$ par la méthode Monte-Carlo
    ??? done "Solution"
        === "Classique"

            ```py
            import random

            def f(x):
                return x**2

            somme = 0
            for _ in range(1_000_000):
                x = random.random()    
                y = random.random()
                if f(x) >= y:
                    somme += 1
            print(somme / 1_000_000)
            ```

        === "Une ligne !"

            ```py
            import random
            print(sum([1 for _ in range(1_000_000) if random.random()**2 >= random.random()])/1_000_000)
            ```

!!! danger "DM"
    Calculer l'aire d'une fonction continue positive dee votre choix par la méthode des rectangles + Autres méthodes

## II - Integrale d'une fonction de signe quelconque

!!! info "Definition" 
    Soit $f$ une fonction continue et négative sur un intervalle.
    $\int_a^b f(x) \, \mathrm dx$ est l'opposé de l'aire délimitée par: la courbe $Cf$,
    l'axe des abscisses, la droite d'équation $x=a$ et la droite d'équation $x=b$.

    ![cf + integrale (a -> b)](images/Courbe3.png){width=900}

    $\int_a^b f(x) \, \mathrm dx = -A$

    Si $f$ change de signe sur $I$ , on coupe l'intervalle en intervalles partiels où $f$ garde un signe
    constant et on applique les définitions précédentes.
    !!! tip "Remarque"
        $\int_a^b f(x) \, \mathrm dx = \int_a^b f(t) \, \mathrm dt = \int_a^b f(u) \, \mathrm du$ → la variable à l'interieur de l'intégrale est muette
        
    !!! note "Valeur moyenne"
        La valeur moyenne de la fonction $f$ sur $[a;b]$ est le réel:

        $$\mu = \dfrac{1}{b-a} \int_a^b f(x) \, \mathrm dx$$

        Si $f$ est positive, $\mu$ correspond à la valeur de la hauteur du rectangle de largeur $b–a$ de même
        aire que $\int_a^b f(x) \, \mathrm dx$

!!! tip "Remarque"
   
    Avec $a < b$ :

    $$\int_b^a f(x) \, \mathrm dx = - \int_a^b f(x) \, \mathrm dx$$

## III - Propriétés de l'intégrale

!!! info "Caractéristiques"
    === "Propriétés"
        $$\int_a^a f(x) \, \mathrm dx = 0$$
    === "Linéarité"
        * Pour $k\in \Bbb R$

        $$\int_a^b k\times f(x) \, \mathrm dx =  k\times \int_a^b f(x) \, \mathrm dx $$

        $$\int_a^b f(x)+g(x) \, \mathrm dx = \int_a^b f(x) \, \mathrm dx + \int_a^b g(x) \, \mathrm dx $$
    === "Encadrement"
        * Si pour $\forall x \in [a;b]$, $g(x) \leq f(x) \leq h(x)$:

        $$\int_a^b f(x) \, \mathrm dx \leq \int_a^b g(x) \, \mathrm dx \leq \int_a^b h(x) \, \mathrm dx$$

        (En particulier si $f$ est positive: $\int_a^b f(x) \, \mathrm dx \geq 0$)
    === "Relation de Chales"
        $$\int_a^c f(x) \, \mathrm dx + \int_c^b f(x) \, \mathrm dx = \int_a^b f(x) \, \mathrm dx  $$

## IV - Intégrale et primitives

!!! info "Définition"
    Soit $f$ une fonction définie sur un intervalle $I$
    Soit $F$ une fonction définie et dérivable sur $I$
    On fit que $F$ est une primitive de $f$ sur $I$ si, pour tout $x\in I, F '(x) = f (x)$
    !!! example "Exemple"
        $x^2$ est **une** primitive de $2x$ sur $\Bbb R$

!!! prop "Propriétés"
    Soit $f$ une fonction définie sur un intervalle $I$
    Soit $F$ une primitive de $f$ sur $I$
    L'ensemble des primitives de $f$ sur $I$ est l'ensemble des fonctions de la forme $F + k$ , où $k \in \Bbb R$
    De plus si on fixe un point, la primitive est unique (la constante est fixée)
    !!! tip "Remarque"
        On connait les primitives quand on connait les dérivées.
        On fera attention à: 

        * $u'e^{u}$
        * $u'u^{n}$
        * $\dfrac{u'}{\sqrt{u}}$
        * $\dfrac{u'}{u}$
        * $\dfrac{u'}{u^2}$


!!! prop "Propriétés"
    Toute fonction continue sur un intervalle admet des primitives.
    !!! tip "remarque" 
        * certaines fonctions comme $x \rightarrow e−x^{2}$ n'ont pas de primitives explicites_
        * introduire le lien entre primitives et intégrales dans le cas d'une fonction continue et positive : voir _p182_

!!! warning "Théorème"
    Soit $f$ une fonction continue sur un intervalle $I$ et $a \in I$ .     
    $F : x \rightarrow \int_a^x f(t) \, \mathrm dt$ est l'unique primitive qui s'annule en $a$
    c'est à dire:    
    pour $\forall x\in I, F'(x) = f (x)$ et $F(a) = 0$
    (voir demo)
!!! warning "Théorème"
    Soit $f$ une fonction continue sur un intervalle $I$ avec $a\in I$ et $b\in I$ .
    Soit $F$ une primitive de $f$ sur $I$
    Alors $\int_a^b f(x) \, \mathrm dx = F(b) - F(a)$
    !!! note "Notation"
        On écrit donc:

        $$\int_a^b f(x) \, \mathrm dx = [F(x)]_a^b = F(b) - F(a)$$

## V - Intégration par parties (IPP)

!!! note "Méthode"
    Soient $f$ , $u$ et $v$ des fonctions continues et dérivables sur l'intervalle $[a;b]$ telles que:
    $\forall x$ de $[a;b]: f(t) = u(t)v(t)$.    
    
    D'après les formules de dérivation : $f'(x)= (u(t)v(t))'= u'(t)v(t) + v'(t)u(t)$     
    
    Donc $\int_a^b f'(t) \, \mathrm dt = \int_a^b (u(t)v(t))' \, \mathrm dt  = \int_a^b (u'(t)v(t) + v'(t)u(t))\, \mathrm dt$
    
    Donc $\int_a^b (u(t)v(t))' \, \mathrm dt = \int_a^b u'(t)v(t)\, \mathrm dt$ + $\int_a^b v'(t)u(t)\, \mathrm dt$
    
    Or il est évident que $\int_a^b (u(t)v(t))' \, \mathrm dt = [u(x)v(t)]_a^b$
    
    Nous avons donc la formule suivante:    
    
    $$\int_a^b u(t)v'(t) \, \mathrm dt = [u(x)v(t)]_a^b - \int_a^b u'(t)v(t) \, \mathrm dt$$
    
    !!! tip "Remarque"
        Cette formule permet juste de déplacer le problème d'une intégrale à une autre et peut-être pourrons
        nous calculer cette dernière.
    
## VI . Exercices d'application

!!! danger "Exercices"
    Calculer les intégrales suivantes:
    !!! example "Intégration classique"
        !!! info ""
            $$I_1 = \int_1^5 \dfrac{x^{2}}{\sqrt{x^3+1}} \, \mathrm dx$$
        !!! info ""
            $$I_2 = \int_{-2}^{-1} (t+1)^{6} - 5t + \dfrac{1}{t^{2}} \, \mathrm dt$$
        !!! info ""
            $$I_3 = \int_{\dfrac{\pi}{3}}^{\dfrac{\pi}{6}} \cos(x)\sin(x)  \, \mathrm dx$$
    
    !!! example "I.P.P."
        !!! info ""
            $$I_1 = \int_0^{\ln(2)} \dfrac{e^{2x}}{e^{2x}+1} \, \mathrm dx$$
        !!! info ""
            $$I_2 = \int_{\sqrt{2}}^9 \dfrac{3t}{t^{2}+2} \, \mathrm dt$$
        !!! info ""
            $$I_3 = \int_1^{e-1} x^{2}\ln(x) \, \mathrm dx$$

_Site basé sur le cours d'un autre professeur de mathématiques._
