# Les satellites 

(Cette base servira pour mon sujet de grand oral !)

## Qu'es ce qu'u satellite ?
 Dans cet partie du document le therme de "**[satellite](https://www.larousse.fr/encyclopedie/divers/satellite_artificiel/90178#:~:text=Engin%20construit%20par%20l'homme,astre%20de%20masse%20plus%20importante.)**" vise ce crée par l'homme et nous les "**satellites naturels**" !

!!! todo "Description"
    Un *satellite* est une installation créée par l’homme, le *Satellite* est installé avec l’aide d’un vaisseau spatial qui se charge de le mener dans l’espace et qui le projète à l’aide d’un lanceur, une fois dans l'espace il sert à diverses utilisatons scientifiques.

Les satellites sont soumis à des difficultés une fois mis en places dans l'espaces avec les différents objets en gravitation au tour de la terre qu'ils doivent èviter pour ne pas être detruit, aux forces physiques et autres éléments qui peuvent entréner leurs pertes.

??? todo "Exemple d'objets en gravitation autour de la Terre"
    [image débrits spatiaux](https://www.cieletespace.fr/media/default/0001/18/debris500x500-5e32.jpeg){ .md-button .md-button--primary }

Les *satellites* sont des machines crée pour aider les scientifiques, et qui ont ouvert à la course spatial.

On peut distinguer plusieurs types de satellites existant en gravitation autour de la teur, destiné à accomplir différentes missions:

!!! note "Les types de satellites"
- Télécommunications
- Observations
- Localisations (pour les GPS)
- Navigations
- Militaires

??? cite "Point historique"
Le premier satellite lancé par les russes en 1957 s’appelait **Spoutnik I** , de nos jours jusqu’en 2007 c’est plus de 5500 satellites qui ont été lancés.

## Comment sont maintenue nos satellites dans l'espace ?

!!! danger "Les Forces exercées ssur les satellites"

    ??? warning "Le coût d'un satellite"
        Selon le site spécialisé Avions-militaires.net(je n'ais pas réussi à accéder au site!!!!), les derniers *coûts unitaires* de production oscillent entre 53,4 et 60,8 millions €.

        -	Pourquoi le satellite ne s'écrase pas sur la Terre ?

        Il est en fait installé dans un *équilibre délicat*, attiré à la fois par la **Terre** et sa gravité et par le **vide intersidéral** à cause de sa vitesse rapide qui le *pousse* vers l'extérieur de sa courbe.
    
    !!! info "Comment s'appelle le type d'interaction entre la Terre et son satellite ?"

    L'interaction gravitationnelle s'exerce bien entre ces objets mais à l'échelle de leur masse cette interaction est si faible qu'elle est incapable de vaincre ne serait que les frottements de l'air qui sépare les objet.

    ![satellite](https://upload.wikimedia.org/wikipedia/commons/a/a8/GOES_O_from_above.jpg){width=250}

    !!! info "Quel est l'interaction gravitationnelle ?"

    On parle d'[interaction gravitationnelle](https://www.encyclopedie.fr/definition/Interaction_gravitationnelle) lorsque deux corps ponctuels A et B de masse mA et mB, séparés par une distance d, exercent l'un sur l'autre une force attractive.


type de propulsion s'appuyant sur l'éjection de gaz à grande vitesse en arrière du véhicule au travers d'une tuyère. Ce genre particulier de propulsion est appelé [moteur-fusée](https://fr.wikipedia.org/wiki/Moteur-fus%C3%A9e).
    

!!! danger "Photos en rapport avec le sujet"


<p align="left, center, right">
  <img width="300" height="300" src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Shuttle_Main_Engine_Test_Firing.jpg?uselang=fr">
  <img width="300" height="300" src="https://upload.wikimedia.org/wikipedia/commons/9/9a/Falcon_Heavy_Pad_39A_%2821048044876%29.jpg">
</p>



??? hint "Source utilisée pour ce document"
    - [Wikipédia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal)
    - Le monde
    - Google
    - [Encyclopédie.fr](https://www.encyclopedie.fr/)


Il y aura de nouvelle version du site très prochainement !
En répondent à ces questions :
- En quoi un satellite peut etre complexe a entretenir dans l'espace ?
- En rapport avec l'éffet de fronde,
A très vite...

<p align="center">
  <img width="350" height="300" src="http://www.fillmurray.com/460/300">
</p>
