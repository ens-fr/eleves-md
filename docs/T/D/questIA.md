## Qu'est ce que l'IA ?
!!! info "__L'intelligence artificielle__ aussi appellée __IA__: *Désigne la possibilité pour une machine de reproduire des comportements liés aux humains, tels que le raisonnement, la planification et la créativité.*"
    ![Intelligence-artificielle](https://blog.digitalcook.fr/wp-content/uploads/2021/01/Intelligence-artificielle-1024x597.jpg){width=800}

## Histoire de l'IA
!!! faq "Quand est-ce que ce concepte est apparue pour la première fois ?"
    C'est dans les années 50 que le terme d'__inteligence artificielle__ apparaît pour la 1ère fois avec __Alan Turing__[^1], grand mathématicien de l'époque, il publie d'ailleurs un article intitulé "*__« Computing Machinery and Intelligence »__*" dans lequel il évoque __l'intention de donner aux machines la capacité d’intelligence__. C’est là qu’est né le concept du __test de Turing__[^2], permetant alors d’identifier la capacité d’une machine à tenir une conversation humaine. Ainsi, si une personne n’est pas capable de dire si elle a conversé avec un humain ou une machine, alors le test de Turing est réussi.
    !!! info "(L’officialisation de l’intelligence artificielle comme véritable domaine scientifique date de 1956)"

!!! important "Pour en savoir plus sur __Alain Turing__ vous pouvez aller voir le film voici le trailer pour un avant gout."
    <iframe width="894" height="503" src="https://www.youtube.com/embed/nuPZUUED5uk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    
    !!! info "Cependant, la première IA remonte à 1642 avec __Blaise Pascal__[^2] et la __Pascaline__[^4] (la premiere machine a calculer)."

## Evolution de l'IA

!!! note "De la calculatrice de Blaise Pascal à aujourd'hui, il y a eu d'énormes avancées dans le domaine de l'IA."

    Voici un compartif des prémisses de l'IA jusqu'à aujoud'hui.

    === "Avant"

        | Pascal | Machine a calculer |
        |:----------------:|:--------------------------------------:|
        |  Ferranti Mark I | premier ordi électronique commercialisé|
    === "Après"
        | chercheurs de Google Brain | AutoML: une intelligence artificielle capable de créer ses propres IA |
        |:----------------:|:--------------------------------------:|
        |  Elias St Elmo Lewis | Ai-Da peut faire des portrait humain|       

        
[^1]: [Alain Turing](https://fr.wikipedia.org/wiki/Alan_Turing).

[^2]: [Test de Turing](https://fr.wikipedia.org/wiki/Test_de_Turing)

[^3]: [Blaise Pascal](https://fr.wikipedia.org/wiki/Blaise_Pascal).

[^4]: [La Pascaline](https://fr.wikipedia.org/wiki/Pascaline)

## Quelles sont les domaines où l'IA intervient 

L'IA intervient dans quasiment tout les domaines: Relation client, réseaux sociaux, soins médicaux, secteur animalier ou même l’administration publique sont des domaines/secteur d’activité ou l’intelligence artificielle joue un véritable rôle et/ou les exemples d’application sont nombreux.
 <iframe width="894" height="503" src="https://youtu.be/4eFkTmH7Xu8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

 !!! info "On peut donc constater que l'IA et de partout dans notre société*"

!!! warning "Certaines perssones sont contre l'évolution de l'ia pour ne pas que l'ia controles mondes XD*"


