#  pourquoi cette oiseau et tant aprécier ?

## leurs espèces

??? note "qu'es qu'un colibris"

    Le colibri est un des plus petits oiseaux dans le monde, mesurant seulement environ 4 pouces de hauteur. Cette espèce d'oiseau se trouve uniquement en Amérique du Nord et du Sud et se développe dans une variété d'environnements, y compris les forêts tropicales, forêts de montagne, prairies, paysages côtiers et même dans le désert shirts. La plupart des espèces de colibris sont identifiés par leur petite taille et leurs longs becs minces, appropriées pour aspirer le nectar. Ils sont en grande partie nectarivores qui se nourrissent de sucre du nectar riche produite par plusieurs espèces de fleurs. Beaucoup de plantes et de la flore dans les forêts tropicales de colibris dépendent entièrement de la pollinisation. Les colibris sont de merveilleuses créatures colorées, aux variétés de couleurs de brun et de vert au rouge vif, mauve et violet.  Il y aurait plus de 300 types d' espèces dans le monde. Certaines espèces comme le Colibri roux et celui d'Allen sont très semblables en apparence, mais on les différencie par leurs positions.

=== "colibri traditionelle"

    ensemble des oiseaux-mouches

    ![un colibri](https://lemagdesanimaux.ouest-france.fr/images/dossiers/2020-06/mini/colibri-070149-650-400.jpg){width=300}

=== "Trochilidés"

    comprend aussi des oiseaux appelées bec-en-faucille

    ![oiseau bec en faucille](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Haaksnavelkolibrie.jpg/1200px-Haaksnavelkolibrie.jpg){width=300}
    
### les origines des principales espèces

``` mermaid
graph LR
    base[principales zones d'amérique] --> AmeS[amériques du sud]
    AmeS --> Top[topazes]
    AmeS --> Bri[brillant]
    AmeS --> Cro[croquette]
    base --> AmeN[amérique du nord]
    AmeN --> Her[hermite]
    AmeN --> Bee[bees]
```

!!! tip "le plus ancien colibri"

    On estime que le dernier ancêtre commun de tous les colibris actuels vivait il y a 22,4 millions d'années quelque part en Amérique du Sud.

## le plus aprécier chez les colibris

!!! abstract "les couleurs"

    | la plus aprécier | le colibris roux        |  
    | ---------------- | ----------------------- |
    | la plus deteste  | le colibris vert foncer |
