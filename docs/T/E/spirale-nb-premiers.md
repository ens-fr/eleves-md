
[comment]: <> (J'ai pas finit mon site, mais je pense le continuer sur mon temps libre plus tard, car c'est très fun à faire !)

# ^^Signification des rayons dans le tracé polaire des nombres premiers^^

## 1/ Introduction :

Ma principale source va être une question posée sur le site _[math.stackexchange.com](https://math.stackexchange.com)_ (lien de _[l'article](https://math.stackexchange.com/questions/885879/meaning-of-rays-in-polar-plot-of-prime-numbers/885894?newreg=fe2fc13efbf442a2a4adfe9412784605)_) avec sa réponse ainsi que la vidéo youtube qui m'a fait découvrir le sujet :_[Why do prime numbers make these spirals ?](https://www.youtube.com/watch?v=EK32jo7i5LQ)_ du Youtubeur _[3Blue1Brown](https://www.youtube.com/watch?v=EK32jo7i5LQ)_, dont je recommande énormément les vidéos.  
Mots clés : ``nombres premiers``, ``coordonnées polaires``.  
La question a été posé par l'utilisateur **dwymark**, et elle a été répondu par l'utilisateur **Greg Martin**.


## 2/ Question :

Le contexte est le suivant, dans un plan avec des ^^coordonées polaires^^[^cp], l'utilisateur s'est amusé à placer les chiffres premiers, donc par exemple avec r = 5 et $\theta$ = 5, etc.. Il a alors fait une étrange découverte. Lorsqu'on observe les nombres premiers en dessous de 30 000, on obtient cela :  

 [^cp]: ![test alt >](images/polaire3.png){ align=right , width=26% }
 [comment]: <> NE PAS TOUCHER LA TAILLE AINSI QUE LA POSITION DE L'IMAGE !!!!!!!!! 
 Cela signifie étiqueter les points dans l'espace 2D, non pas avec
 les coordonnées xy habituelles mais plutot avec une distance par rapport à l'origine, appelée r pour le rayon, et l'angle $\theta$ en degrés puis reporté en radians par rapport à l'horizontale. Ainsi ici un r de 3 et un $\theta$ de 30.0° soit 0,52 radian.
 
![spiral nombre premier](https://i.stack.imgur.com/W3eQR.png)

!!! example "Différentes situations"
    === "Comparaison"
        Pour comprendre que ce ne sont que les nombres premiers qui font ça et non pas tout les nombres, même graphique mais avec les nombres premiers (en jaune) ainsi que les multiples de 3 (en vert) et de 7 (en rouge) superposées :

        ![spiral nb premier + multiple 3 7](https://i.stack.imgur.com/OtG8w.png)

    === "Portée de 200 000"
        De plus, encore plus fascinant, lorsqu'on augmente la portée, ici jusqu'à 200 000, les rayons des nombres premiers se regroupent en groupes de 3 ou 4 :  

        ![nb premier portée 200 000](https://i.stack.imgur.com/CosTo.png)

    === "Portée encore plus grande"
        Chose plus facile à observer quand on augmente encore plus la portée, ici 1 000 000 :

        ![nb premier portée 1 000 000](https://i.stack.imgur.com/8te4G.png)

    === "Petit bonus 1"
        Ici tout les nombres sont placés avec des coordonnées polaires, la portée est de 25 000 :

        ![nb portée 25 000](https://i.stack.imgur.com/uY1WW.png)
    
    === "Petit bonus 2"
        Tout les nombres y sont représentés, la portée est de 700 000 :

        ![nb portée 700 000](https://i.stack.imgur.com/Y9jIQ.png)

La question posé par **dwymark** est "Je sais qu'il y a quelque chose qui s'appelle le théorème du nombre premier - ces modèles y sont-ils liés ? Ces rayons sont-ils le même phénomène que les lignes diagonales trouvées dans les _[spirales d'Ulam](https://fr.wikipedia.org/wiki/Spirale_d%27Ulam)_ ?"

[comment]: <> (todo page spirales Ulam ?? envoyer vers la page)

La réponse proposée par **Greg Martin** est très axée mathématiques, n'ayant pas les capacitées pour la comprendre correctement, je vais utiliser la vidéo de _[3Blue1Brown](https://www.youtube.com/watch?v=EK32jo7i5LQ)_ afin d'y répondre.

## 3/ Réponse :



Pour comprend plus facilement, regardons en premier la disposition de tout les nombres dans un plan avec coordonées polaires, le chiffre 1, va avoir un r de 1 et un $\theta$ de 1, pour le chiffre 2, double de 1, on va donc doubler la distance du r et du $\theta$, ainsi le chiffre 3, qui est égal à 1 + 2, on va ajouter le r du 1 et du 2, et pareil pour son $\theta$, bref vous avez compris, il est normal qu'on obtienne une sorte de spirale a ce moment là.  

!!! tldr "Chiffre important"

    === " Spirales "
        Mais si on regarde le positionnement des nombres premiers, on pourrait juste penser qu'ils   sont positionnés au hasard, mais si on regarde d'une portée plus grande on peut clairement remarquer ^^20 spirales avec deux bras manquants^^.  
        ![spiral](images/spiral2.png)

    === " Rayons "
        En zoomant encore plus en arrière, on peut voir ^^280 rayons^^ qui s'organisent en ^^groupe de 4^^, avec quelque fois un rayon en moins.  
        ![spiral](images/spiral3.png)


L'utilisateur a directement relié le problème aux nombres premiers, et pour y répondre correctement nous allons séparer notre réponse en deux parties, donc en deux question.

### 3.1/ Que se passe-t-il lors du filtrage vers les nombres premiers ?

Même si les nombres premiers ne sont pas à l'origine de ces spirales, filtrer vers les nombres premiers nous conduit vers un théorème des nombres, [le théorème de Dirichlet](https://fr.wikipedia.org/wiki/Théorème_de_Dirichlet_(séries_de_Fourier)). 
Afin de donner le coup d'envoi, il faut regarder le début des ces spirales.  

!!! note "Tables et spirales"
    > En regardant de plus proche on peut remarquer 6 petites spirales :
    ![spiral zoom](images/spiral%20zoom.png)

    Regardons les plus en détails

    === "6k"
        Nous pouvons voir une premiere spirale, 
        ![spiral 6](images/multiple6.png)

    === "6k + 1"
        ![spiral 6 + 1](images/multiple61.png)

    === "6k + 2"





### 3.2/ D'où viennent ces spirales ?


