## ^^La génération aléatoire de Minecraft^®^^^

!!! info "Minecraft est un jeu mélangeant construction et aventure, créé en 2009 par Markus « [Notch](https://fr.wikipedia.org/wiki/Markus_Persson) » Persson, également fondateur de [Mojang Studios](https://fr.wikipedia.org/wiki/Mojang_Studios). Il permet à ses joueurs de manipuler un monde en trois dimensions, composé entièrement de blocs à détruire, placer et transformer. (Ah et minecraft est codé en java 🤮)"

<em>Maintenant abordons le coté technique du jeu</em>



## Le côté technique du jeu ⚙️

### **La génération des mondes**🗺️:

Avant de parler du coté algorithmique nous parlerons de comment fonctionne Minecraft du point de vue du joueur.

!!! note "Ce que voit le joueur :"
    
    Lors du démarage du jeu le joueur peut rejoindre le mode _singleplayeur_ ou _multiplayeur_. Nous nous intéresserons surtout au mode **singleplayeur**

    ![panneau](https://www.minecraft-france.fr/wp-content/uploads/2022/04/image-2-1536x814.png){width=1000}

    Le joueur va ensuite avoir un affichage comme ceci:
    
    ![choix monde](https://upload.fr-minecraft.net/images/frminecraft/fr-minecraft_JMHA_2021-04-15-04-27-19.png){width=1000}

    Il peut après créer un nouveau monde et aura un affichage comme ça:

    ![config monde](https://upload.fr-minecraft.net/images/frminecraft/fr-minecraft_CDNS_image-1.png){width=1000}

    On clique sur la barre "More World Options..." et on a cet affichage:

    ![config seed](https://images.wondershare.com/filmora/article-images/how-to-create-your-own-world-with-the-best-minecraft-seeds-step3.jpg){width=1000}
    
    On va s'interesser aux seed...

### C'est quoi une seed?🌱

!!! cite "Une seed(nom francais : graine) est un code utilisé dans le processus de génération de mondes dans Minecraft. Chaque monde a sa propre graine qui est utilisée pour garder une certaine consistance dans le terrain généré, puisque la génération est pseudo-aléatoire. Ainsi pour la même version du jeu, deux mondes ayant la même graine seront strictement identiques au moment de leur génération."

Pour mieux comprendre je vais donner un exemple

!!! example "monde confinguré avec une seed specifique"
    Je cherche par exemple sur internet si il existe une seed trouvée par les joueurs ou il y a un cratère au milieu d'une montagne:

    ![seed internet](https://minecraft.fr/wp-content/uploads/2021/12/cratere-sommet-montagne-seed-minecraft-1-18-1200x750.jpg){width=1000}

    Là par exemple j'en ai trouvé une je regarde donc la seed correspondante a cette generation et je la copie colle dans la configuration de mon monde dans mon jeu et j'obtiens pile le même monde.

Les seeds minecraft sont basées sur un algorithme : **le bruit de Perlin**

### Le Bruit de Perlin 🔊 

!!! info "Selon Wikipedia "Le bruit de Perlin a été développé par [Ken Perlin](https://fr.wikipedia.org/wiki/Ken_Perlin) en 1985. À cette époque, après avoir travaillé sur les effets spéciaux de Tron en 1981, il cherchait à éviter le look  "machinique". Il commença donc par mettre au point une fonction pseudo-aléatoire de bruit qui remplit les trois dimensions de l'espace"."

C'est une [texture procédurale](https://fr.wikipedia.org/wiki/Texture_proc%C3%A9durale) utilisée comme effet visuel qui augmente le réalisme de certaines textures.

Le bruit Perlin est le plus couramment utilisé à deux, trois, voire quatre dimensions, il peut être défini pour un nombre quelconque de dimensions. L'algorithme se décompose en **trois** étapes :

- Définition de la grille avec des vecteurs de gradient aléatoires.

- Calcul du produit scalaire entre le vecteur de gradient et le vecteur distance.
 
- Interpolation entre ces valeurs.
 
#### Définition de la grille avec des vecteurs de gradient aléatoires:

Définir une grille à _n_ dimensions. À chaque point de la grille (nœud), attribuer un vecteur de gradient aléatoire de longueur unitaire et de dimension _n_.

Pour une grille à deux dimensions, à chaque nœud sera assigné un vecteur aléatoire du cercle unité, et ainsi de suite pour les dimensions supérieures. Le cas unidimensionnel est une exception : le gradient est un scalaire aléatoire entre -1 et 1. 
![grille de vecteurs gradient](https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/PerlinNoiseGradientGrid.png/440px-PerlinNoiseGradientGrid.png){widht=1000}

_Une grille bidimensionnelle de vecteurs de gradient_

#### Produit scalaire:

C'est un point de l'espace à n-dimensions envoyé à la fonction de bruit, l'étape suivante dans l'algorithme consiste à déterminer dans quelle cellule de grille le point donné tombe. Pour chaque nœud-sommet de cette cellule, on calcule le vecteur distance entre le point et le nœud-sommet. Puis on calcule le [produit scalaire](https://fr.wikipedia.org/wiki/Produit_scalaire) entre le vecteur de gradient au nœud et le vecteur de distance.

Pour un point dans une grille bidimensionnelle, cela nécessitera le calcul de 4 vecteurs de distance et de 4 produits scalaires, tandis que dans les trois dimensions, 8 vecteurs de distance et 8 produits scalaires sont nécessaires.

![produit scalaire](https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/PerlinNoiseDotProducts.png/1280px-PerlinNoiseDotProducts.png){widht=1000}
_Le produit scalaire de chaque points avec sa valeur de gradient de nœud de grille la plus proche_

#### Interpolation :

La dernière étape est l'interpolation entre les produits scalaires calculés aux nœuds de la cellule contenant le point d'argument. Cela a pour conséquence que la fonction de bruit renvoie 0 lorsqu'elle est évaluée sur les nœuds de la grille eux-mêmes.

L'interpolation est effectuée en utilisant une fonction dont la dérivée première (et éventuellement la dérivée seconde) est nulle aux nœuds de la grille. Cela a pour effet que le gradient de la fonction de bruit résultante à chaque nœud de grille coïncide avec le vecteur de gradient aléatoire précalculé.

![interpolation](https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/PerlinNoiseInterpolated.png/1280px-PerlinNoiseInterpolated.png){widht=1000}

_Résultat de l'interpolation_


### Fonctionnement dans le jeu:

Minecraft va générer en fonction de la valeur de chaque cellule une hauteur de bloc et les biomes(ou region) qu'il y aura sur la seed.
D'ailleurs avec la nouvelle mise a jour il y a une utilisation du bruit de perlin pour ce qui est de la generation des souterrains différentes d'avant. La hauteur du monde accessible a augmenté donc la plupart des choses ont dut etre modifiée. La generation des mondes a dut etre modifiée car il y a eu egalement une amelioration des terrains qui devait avoir une generation plus logique et realiste. 

_Exemple de generation avant la mise a jour:_

![vielle generation](https://lh5.googleusercontent.com/-fR8Ml0hhimM/TYzfsyyhFvI/AAAAAAAAAGw/yrV-xMSMikU/s640/2409838883250561605_2.png){widht=1000}

!!! note "Constat"
    On voit ici des bugs quant à la logique de la génération. Il y a des iles qui volent... Un gros manque de realisme🤔.

Désormais on a plus de générations de monde logique tel que:
![nouvelle generation](https://www.breakflip.com/uploads/5f7c893429e35-montagne.png){widht=1000}

!!! note "Nouveau constat"
    On a des montagnes plus realiste sans coupure nette.

Voila un exemple de comment est généré un monde aléatoire dans les jeux, ou du moins dans Minecraft. Et comment fonctionne le bruit de Perlin.

Maintenant un petit quiz.

## Quiz🧠


Questions:

Question 1 :
    Qu'est ce que le bruit de Perlin?

- [ ] Une texture procedurale
- [ ] Un bruit d'animal
- [ ] Un plat
- [ ] Une formule mathématique

Question 2 :
    En combien d'etapes se décompose l'algorithme?

- [ ] 2
- [ ] 7 
- [ ] 3 
- [ ] 42

Question 3 :
    Quelle étape n'existe pas?

- [ ] Interpolation
- [ ] Produit scalaire
- [ ] La mise en place du bélinographe
- [ ] Définition de la grille avec des vecteurs de gradient aléatoires  
    

!!!done "Réponses:"
    
    Question 1 :
        Qu'est ce que le bruit de Perlin?

    - [x] Une texture procedurale
    - [ ] Un bruit d'animal
    - [ ] Un plat
    - [ ] Une formule mathématique

    Question 2 :
        En combien d'étapes se décompose l'algorithme?

    - [ ] 2
    - [ ] 7 
    - [x] 3 
    - [ ] 42

    Question 3 :
        Quelle étape n'existe pas?

    - [ ] Interpolation
    - [ ] Produit scalaire
    - [x] La mise en place du bélinographe (c'est un appareil qui permet la transmission à distance d'images fixes)
    - [ ] Définition de la grille avec des vecteurs de gradient aléatoires  


_Fin_