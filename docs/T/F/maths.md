# Pour écrire des Maths

??? info "Coming Soon"
    Dans un futur proche, en attendants voici un petit extrait
    
    ??? tip "Maths"
        f($x$) = $\dfrac{(x - 1) \times x}{x + \dfrac{10 + x}{18 + 4^{x}}}$