# Admonitions

!!! note "Admonitions par default"

    Une Admonitions c'est crit de la manière suivante :  
    - `!!!`  
    - Suivit d'une calification de type : `note`  
    - Si l'on veut changer le titre `note` par un autre titre : placer à la suite `" Nouveau titre"`  
    - Sinon a la suite de `note` placer `""` pour que la note soit sans titre
    - Puis le text qu'on souhaite écrire  

    ```markdown
    !!! note "Titre"
        
        Texte
    ```
    

## Les different type Admonitions

### Note réductible

??? note "Note réductible"

    Pour crée une note réductible, il faut changer les `!!!` par `???`  

    ```markdown
    ??? note "Titre"
        
        Texte
    ```  

    ???+ note "Note réductible déroulé"
        
        Dans le cas ou vous souhaité que l'Admonitions soit déjà déroulé
        il sufit de décrire `???+` à la place de `???`  

        ```markdown
        ???+ note "Titre"
    
            Texte
        ```

### Blocs en ligne

=== "Fin de ligne"
    
    !!! info inline end
        
        Placer deux blocs côte à côte est possible de cette manière.
        A la suite des `!!!` écrir `info inline end` ainsi vous placerer votre blocs à la fin.
    
    ```markdown
    !!! info inline end
        
        Lorem ipsum dolor sit amet, consectetur
        adipiscing elit. Nulla et euismod nulla.
        Curabitur feugiat, tortor non consequat
        finibus, justo purus auctor massa, nec
        semper lorem quam in massa.
    ```

=== "Au début"
    
    !!! info inline
    
        Placer deux blocs côte à côte est possible de cette manière.
        A la suite des `!!!` écrir `info inline end` ainsi vous placerer votre blocs à la fin.
        
    ```markdown
    !!! info inline
        
        Lorem ipsum dolor sit amet, consectetur
        adipiscing elit. Nulla et euismod nulla.
        Curabitur feugiat, tortor non consequat
        finibus, justo purus auctor massa, nec
        semper lorem quam in massa.
    ```

## Les Type d'Admonitions

!!! note "Note"
    
    ```markdown
    !!! note "Titre"
        
        Texte
    ```

!!! abstract "abstract, summary, tldr"
    
    ```markdown
    !!! abstract "Titre"
        
        Texte
    ```

!!! info "Info, todo"
    
    ```markdown
    !!! info "Titre"
        
        Texte
    ```

!!! tip "tip, hint,important"
    
    ```markdown
    !!! tip "Titre"
        
        Texte
    ```

!!! success "Success, check, done"
    
    ```markdown
    !!! success "Titre"
        
        Texte
    ```

!!! question "Question, help, faq"
    
    ```markdown
    !!! question "Titre"
        
        Texte
    ```

!!! warning "warning, caution,attention"
    
    ```markdown
    !!! warning "Titre"
        
        Texte
    ```

!!! failure "failure, fail,missing"
    
    ```markdown
    !!! failure "Titre"
        
        Texte
    ```

!!! danger "Danger, error"
    
    ```markdown
    !!! danger "Titre"
        
        Texte
    ```

!!! bug "Bug"
    
    ```markdown
    !!! bug "Titre"
        
        Texte
    ```

!!! example "Example"
    
    ```markdown
    !!! example "Titre"
        
        Texte
    ```

!!! quote "Quote, cite"
    
    ```markdown
    !!! quote "Titre"
        
        Texte
    ```