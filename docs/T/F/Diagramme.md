# Diagramme

!!! note "Organigramme"

``` mermaid
graph LR
  A[Start] --> B{Intersection};
  B -->|Yes or No| C[Station];
  C --> D[Insertion];
  D --> E;
  B ---->|Ligne Droite| E[+1 Tours];
```
!!! note "Diagramme de Séquance"

``` mermaid
sequenceDiagram
  Alice->>John: Sallut John, Comment tu vas?
  Note right of John: John Et Bob sont Amis
  Note right of Alice: Alice est la cousine de John
  John-->>Alice: Bien!
  John->>Bob: Tu vien se Week-end?
  Bob-->>John: Oui, bien sur
```

!!! note "DIagramme d'Etats"

``` mermaid
stateDiagram-v2
  state fork_state <<fork>>
    [*] --> State_One
    State_One --> fork_state
    fork_state --> State2
    fork_state --> State3

    state join_state <<join>>
    State2 --> join_state
    State3 --> join_state
    join_state --> State4
    State4 --> [*]
```

!!! Diagramme de Classe

``` mermaid
classDiagram
  Person <|-- Student
  Person <|-- Professor
  Person : +String name
  Person : +String phoneNumber
  Person : +String emailAddress
  Person: +purchaseParkingPass()
  Address "1" <-- "0..1" Person:lives at
  class Student{
    +int studentNumber
    +int averageMark
    +isEligibleToEnrol()
    +getSeminarsTaken()
  }
  class Professor{
    +int salary
  }
  class Address{
    +String street
    +String city
    +String state
    +int postalCode
    +String country
    -validate()
    +outputAsLabel()  
  }
```

!!! note "Diagramme entité-relation"

``` mermaid
erDiagram
  POSTE ||--o{ Lettre : places
  Lettre ||--|{ Expeditaire : ecrit
  POSTE }|..|{ Destinataire : delivre
```

