# Aceuille

## Base du Markdown

### Titre

Pour pouvoir écrire un titre en Markdown, il faut savoir qu'il existe plusieurs niveau de titre (4 et parfois pour les 
blocs de contenu 6)  

```markdown

# Aceuille

    Titre de niveau 1

## Base du Markdown

    Titre de niveau 2

### Titre

    Titre de niveau 3
```

### Blocs de code

!!! note "Blocs de code"

    Pour écrire un Blocs de code, il faut employer ` ``` ` suivis du type de code python(`py`), `mermaid`, `markdown`
    Ne pas oublier de fermé le blocs avec ` ``` `  

    ```markdown
    
    ``` py
    
    print("Hello World!)
    ```

### Onglets

=== "Onglet"
    
    ```mermaid
    graph LR
        A[10] --> B[5]
        A --> C[15]
    ```
=== "Onglet"
    
    > Il était une fois `Internet`

Pour créer un onglet il faut `===` suivit d'un titre pour l'onglet, puis le contenu et faire ceci pour chaque onglet que
l'on créer

### Info bulle

Pour créer une info bulle, il faut faire comme ça :

!!! note "Info"
    
    ``` markdown

    [Survolez moi]("Ceci est une info bulle")
    ```
    Ainsi on peut obtenir une [Info bulle]("Ceci est une info bulle"), pour informer le lecteur ignorent et ainsi ne pas 
    déranger les autres

### Liste

!!! note "Liste à puce"
    
    Pour crée une liste à puce, il faut placer un tirer:

    ```markdown

        - 1914-1918: `1er Guerre Mondiale`  
        - 1939-1945: `Second Guerre Mondiale`  
        - 2023:
    ```
    ![Future](images/Ptiz.gif)

### Liens et Image

=== "Lien"
    !!! note "Lien"
        Pour créer un lien, il faut :
        ``` markdown
        [Titre](lien)
        ```
        
        Exemple : Pour apprendre python il y a [France IOI](http://www.france-ioi.org/#:~:text=Chaque%20ann%C3%A9e%2C%20France%2Dioi%20s%C3%A9lectionne,de%20plus%20de%2080%20pays.)

=== "Image"
    !!! note "Image"
        Pour incrusté une image il faut:
        ``` markdown
        ![Titre](chemin de l'image)
        ```
        
        Exemple: Voici le [Logo Python](https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg)  
        ![Logo Python](images/1024px-Python-logo-notext.svg.png){ width="300" align=right }

### Citation

???+ note "Créer une citation"
    Placer un `>` en début de ligne puis écrire la citation

??? note "Citation Victor Hugo"
    > Mieux vaut une conscience tranquille qu'une destinée prospère. J'aime mieux un bon sommeil qu'un bon lit.

### Emphase

!!! danger "Attention Au abut"
    Une faible utilisation des emphases est conseillé
    Perssone ne veut lire un text comme ça  

    > Les _petits_ poissons, dans l'eau **nagent _nagent_ nagent _nagent_ nagent**

### Boutons
Les boutons permet d'exécuter une action tel que ouvrire un lien:

[Boutons](https://www.google.com/search?q=boutons&source=lmns&bih=695&biw=1366&rlz=1C1CHBF_frFR946FR946&hl=fr&sa=X&ved=2ahUKEwjHp8rC6-P3AhWCVPEDHZlVBVgQ_AUoAHoECAEQAA){ .md-button }
??? note "Obtenir un Boutons en Markdown"
    
    ```markdown
    [Titre](#){ .md-button }
    ```
    Remplacer `#` par un lien  
    Et mettre un titre

[Boutons](https://www.google.com/search?q=boutons&source=lmns&bih=695&biw=1366&rlz=1C1CHBF_frFR946FR946&hl=fr&sa=X&ved=2ahUKEwjHp8rC6-P3AhWCVPEDHZlVBVgQ_AUoAHoECAEQAA){ .md-button .md-button--primary }
??? note "Obtenir un Boutons en Markdown"
    ```markdown
    [Titre](#){ .md-button .md-button--primary }
    ```
    Remplacer `#` par un lien  
    Et mettre un titre

## Un Jour peut-être

[Un jour peut-être, se sera Vous l'auteur](https://squidfunk.github.io/mkdocs-material/){ .md-button }