# Comment trier une liste ?

## Introduction 

Savoir trier une liste est un problème omniprésent aujourd'hui.  
Différents algorithmes permettent de résoudre ce problème.
!!! info "Définition""
    Un algorithme c'est une suite d'instructions à faire pour atteindre un 
    objectif précis.
    !!! example "Exemple"
        On pourrait comparer un algorithme à une recette de cuisine.

Ces algorithmes sont plus ou moins efficaces, ils sont classés en fonction de 
leurs complexité.

!!! info "Définition"  
    !!! warning "Attention"
        Le terme de complexité est un peu trompeur, ce n'est pas :

        * de quelle façon l'algorithme est difficile à comprendre
        * de quelle façon l'algorithme est difficile à programmer
    Pour calculer la complexité on essaie de voir comment le nombre d'opérations élementaires va croire en fonction de la 
    taille du problème.

## Le tri à bulle

!!! info "Anecdote"
    Ce tri tient son nom car à la de la première passe la plus grosse valeur 
    sera à la fin de la liste.
    La plus grosse valeur remonte à la fin de la liste, comme une bulle qui 
    remonte à la surface.



### Fonctionnement 
Étape 1:
Regarder si l'élément à l'indice suivant est plus grand que l'élément à 
l'indice en place pour chaque indice de la liste :  

* s'il est plus grand, échanger les éléments
* sinon, ne rien faire.

Étape 2:
Si pendant le parcours il y a eu au moins un changement recommencer l'étape 1
Sinon la liste est triée

```mermaid
flowchart TD
  
  A([Début]) --> B[Mettre la variable changement à True.];
  B --> C[Mettre nombre éléments triés à 0];
  C --> D[Tant que chagement a la valeur True];
  D --> E[Mettre changement à False];
  E --> F[Pour tous les i de longueur de la liste - nombre d'éléments trié - 1];
  F --> G[Si liste à l'indice i est plus grand que liste à l'indice i + 1];
  G --> H[échanger la valeur à l'indice i avec celle à l'indice i + 1];
  H --> I[Mettre la variable changement à True];
  I --> J[Incrémenter le nombre d'éléments trié de 1];
  J ---> D;
  J --> L[La liste est triée];
  L --> M([Fin]);
```

### Complexité 

Le dernier élément, grâce à ce mécanisme, se trouve à la fin.
Donc pour une liste de n éléments qu'il faut trier au deuxième tour il ne faudra plus que tirer n - 1 éléments, au troisième tour n - 2 élément, donc la complexité c'est la somme des n premiers nombres.  

$$
n \left(\dfrac{0 + n}{2} \right) = \dfrac{n^2}{2}
$$

Les facteurs multiplicatifs ne sont pas pris en compte pour la complexité,
donc la complexité est quadratique. $O\left(n^2\right)$

### Programme en python

```python
--8<--- "docs/scripts/tri_a_bulle.py"
```

## Le tri fusion 


### Fonctionnement
Il faut couper la liste en deux, est trier recursivement les deux sous-listes.
Quand la liste est assez petite, il faut la trier "à la main".
Quand les deux sous-listes sont triées, il faut les fusionner.

``` Pseudo-Code
definir la fonction tri_fusion(liste):

si la longueur de la liste = 1:
    renvoier la liste

sinon:
    liste1 = premiere moitié de liste
    liste2 = deuxieme moitié de liste
    trier liste1 avec tri_fusion
    trier liste2 avec tri_fusion
    renvoier la fusion de liste1 et liste2
```

### Complexité 

Pour une liste de $n$ nombres il y a $\log(n, 2)$ coupages en deux possible.
Pour joindre les deux listes il faut $n$ opérations élémentaires.

Donc la complexité est logaritmique $O(n\log(n))$.

![Explication de nlog(n)](images/explication_nlogn.png)

### Programme en python

```python
--8<--- "docs/scripts/tri_fusion.py"
```