> ---
> ## Qu'est-ce que c'est l'ADN ?
> L'ADN est un *élément présent chez __tous__ les êtres vivants*. Qui définit chaques caractéristiques physique que nous possédont et sa structure en double hélice a été découverte le 18 Octobre 1962 par :

>    - **James Watson** (généticien/biochimiste)
>    - **Francis Crick** (biologiste) et
>    - **Maurice Wilkins** (physicien)

> Un prix **Nobel de médecine** leur a même été attribué.

> !!! info "Découverte possible grace aux travaux antérieurs d'une pionière de la biologie moléculaire : **Rosalind Franklin** "
> ---
> ## Une nouvelle forme de stockage ?
> Quand on parle de **stockage de données**, on parle bien sûr de nos classiques `clé USB` ou encore `disque dur`, très présents de nos jours évidement. Mais ici nous allons parler d'une **nouvelle forme** de stockage, d'une nouvelle méthode !
>![chronologie des formes de stockage de donnes](images/chrono_stockage_de_donnees.gif){width=600}  
>
> Comment le montre nôtre frise chronologique ci dessus, les méthodes ont changé et marqué leur époque respectives.  
> L'ADN présenté aujourd'hui ne fait évidement pas exeption à la rêgle .
> ---