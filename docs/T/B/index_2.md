## **Pourquoi et Comment**

!!! note "Cette rubrique vous expliquera pourquoi est-ce que l'ADN est devenu une possible forme de stockage de données informatique, ainsi que comment nous pourrons effectuer se rangement, qui a ce se jour est "abordable"."

=== "Pourquoi ?"
    !!! faq " **Est-ce que c'est de l'ADN d'être vivants ? Est-ce que l'on va modifier nôtre ADN ?**"
        !!! warning "Non, en aucun cas !"

        !!! info "L'ADN utilisé est ce que l'on appelle *synthétisé*, c'est à dire qu'il est *fabriqué*. Il n'appartient à aucun animal, aucune bestiole, etc... Aucun danger pour nos amis les animaux ! C'est génial non ?"

    ---
    
    !!! question "Si l'on veut utilisé de l'*ADN synthétisé*, c'est notamment pour sa *petite taille* !"

        L' ADN est un molécule macroscopique, c'est à dire visible à l'œil nu, celui-ci se trouve dans les chromosomes (X et Y par exemple), chez l'humain on décompte 46 chromosomes remplis d'informations génétiques ou plutôt formant le code génétique d'un individu à part entiere.

        En reprenant ce même principe dans l'ADN synthétisé, on peut alors stocker des données informatiques comme ce dernier.

        !!! info "L'ADN naturel contient donc aussi bien l'information génétique de la couleur des cheuveux que l'ADN synthétisé, qui lui pourrait en revanche sauvegarder une photo de famille en couleur."

        !!! warning "En 2018 le volume de données mondial était de 33 zettabytes, il est prévu que pour 2025 ce même volume atteigne *175 zettabytes*."

            !!! info "A titre de comparaison :"
                - l'ADN possède 10 milliards de fois la densité d'un CD
            
                - 1 pg (picogramme) d'ADN = 1 millionième de millionième de gramme, suffisant pour stocker un livre entier

                - 1 g (gramme) d'ADN = 215 petabytes de données

                - Plus solide que l'ADN normal/naturel, cela veut dire une bonne conservation pour une plus longue durée

                Du point de vue **informatique et écologique**, c'est une méthode révolutionnaire car celle-ci permettrait d'**enlever probablement la totalité des salles de serveur dans le monde**, qui sont une des plus grosses épines écologique.

=== "Comment ?"
    !!! info "Pour passer des données en binaire (0 et 1) aux informatiques génétique (A, C, G, T), comment fait-on ?"

        Nôtre information numérique / écrite en binaire doit être transcrise sous la forme de bases azotées (**A**dénine, **T**hymine, **G**uanine et **C**ytosine)

        Pour ce faire plusieurs méthodes sont envisageables (avec différents coût):

        === "2 bits pour 1 lettre"

            |génétique|binaire|
            |:---:|:---:|
            |A|00|
            |T|01|
            |G|10|
            |C|11|

            Cette méthode est **_simple_ mais peu _efficace_**.
        
            !!! warning "Pour une grande chaine de nucléotides, cela représenterait beaucoup de **données à stockées**."
        === "5 bits pour 2 lettres"

            |génétique|binaire|génétique|binaire|
            |:---:|:---:|:---:|:---:|
            |AA|00000|GG|01000|
            |AT|00001|GA|01001|
            |AG|00010|GT|01011|
            |AC|00011|GC|01100|
            |TT|00100|CC|01101|
            |TA|00101|CA|01110|
            |TG|00110|CT|01111|
            |TC|00111|CG|10000|

            Cette méthode est plus efficace que la précédente, en gardant une certaine simplicité. Néanmoins ce n'est pas la meilleure.
            
            !!! info "Normalement _les nucléotides sont par groupe de 3_ aussi appelés "codon". Il est donc plus efficace des traduire du génétique au binaire de la même façon."
        
        === "1 octet pour tout les codons"

            !!! warning "Avant tout !"
                Un "**codon**" c'est une **séquence de 3 nucléotides (A, T, G ou C)** sur un acide ribonucléique messager ( ou un ARN messager)  

                En plus clair, ça ressemble à ça :  
                
                |Codon d'initiation| Codon existant|Codon Stop|
                |:---:|:---:|:---:|
                |UGG ( Start / Leu )|ACU, ACC, ACA, ACG ( Thr )|UAG ( Stop / Pyl )|
                |CUG ( Start / Leu )|GGU, GGC, GGA, GGG ( Gly )|UGA ( Stop / Sec / Trp )|
                |AUG ( Start / Met )|UCU, UCC, UCA, UCG ( Ser )|UAA ( Stop )|

                !!! info "Le terme de **codon d'initiation/stop** prend son sens lors de la lecture du code génétique, ces codons indique le début ainsi que la fin de la lecture de l'information génétique."
            ---
            !!! question "Mais on connait près de 500 acides aminés ? Comment peut-on tous les retranscrire en binaire ?"
                !!! info "Aucun problème ! Avec 8bits soit 1 octet ont peux aller de 0 à 255 (soit 256 nombres)"
                    Il suffit seulement d'assigner un codon à chaque nombre (sous forme binaire).

                    Et on obtient une organisation des codons en binaire, similaire à celle-ci :

                    |Codon|binaire|Codon|binaire|
                    |:---:|:---:|:---:|:---:|
                    |UCC|00000000|ACC|00001000|
                    |UGG|00000001|ACA|00001001|
                    |CUG|00000010|ACG|00001010|
                    |AUG|00000011|GGU|00001011|
                    |UAG|00000100|GGC|00001100|
                    |UGA|00000101|GGA|00001101|
                    |UAA|00000110|GGG|00001110|
                    |ACU|00000111|UCU|00001111|

                    !!! info "C'est là plus efficace, car on peut traduire tout les codons existants sur une même tableau (sans changer de façon de les traduires) Néanmoins elle nécessite un base de données contenant chaque séquence en binaire de chaque codons."