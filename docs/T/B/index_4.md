!!! faq "L'ADN, abordable aujourd'hui ?"
    Et ben pour le moment non, malheureusement le coût est trop chère, les manipulations trop importantes et trops contraignantes pour les entreprises d'aujourd'hui

!!! faq "Et dans le futur ?"
    Dans le futur, probablement proche qui sait, l'ADN sera un secteur bien plus mis en avant.
    
    !!! info "Aujourd'hui, l'ADN n'est pas le Saint Graal de l'informatique, mais demain il le sera, peut être bien ..."
        ![homme heureux devant un ordi](https://c.tenor.com/pvGcwkC_VnAAAAAC/g%C3%A9nial-trop-bien.gif "homme heureux devant un ordi")