!!! faq "Où est le problème ?"
    !!! warning "Le problème est tout simple :"

        - Lire, écrite ou modifier des données informatiques :thumbsup:

        - Lire ou écrire des données génétique :triumph:

        - Réécrire des données génétique :scream:
        
        !!! important "<a href="https://fr.wikipedia.org/wiki/Stockage_de_donn%C3%A9es_num%C3%A9riques_sur_ADN#Lecture_de_l'information_:_le_s%C3%A9quen%C3%A7age_de_l'ADN" target="_blank">Lecture de l'information : le séquençage de l'ADN</a>"
            Actuellement la **technologie de séquençage de l'ADN** (la méthode pour lire l'ADN) la plus utilisé, est celle développée par [Illumina](https://fr.wikipedia.org/wiki/Illumina).

            Elle implique beaucoup de manœuvre, comme :
            <ul>
                <li> L'immobilisation d'un [monocaténaire](https://fr.wikipedia.org/wiki/Monocat%C3%A9naire) d'ADN sur un support solide</li>
                <li> [L'amplification en chaîne par polymérase](https://fr.wikipedia.org/wiki/R%C3%A9action_en_cha%C3%AEne_par_polym%C3%A9rase) des séquences</li>
                <li> Labellisation des [bases azotées](https://fr.wikipedia.org/wiki/Base_azot%C3%A9e) individuelles avec des bases azotées complémentaires... etc... etc...</li>
            </ul>
            :smile: Je vous passe les détails, mais il faut comprendre que la difficulté de lecture de l'ADN réside surtout dans son coût, à cause des multiples manipulations nécessaires.
            
        !!! important "<a href="https://fr.wikipedia.org/wiki/Stockage_de_donn%C3%A9es_num%C3%A9riques_sur_ADN#Lecture_de_l'information_:_le_s%C3%A9quen%C3%A7age_de_l'ADN" target="_blank">Ecriture de l'information : synthèse de l'ADN</a>"
            Quand on parle **d'écriture** ici, c'est un peu plus délicat.

            Le coût est encore aujourd'hui un peu trop important pour être rentable, même abordable sur le long terme.            