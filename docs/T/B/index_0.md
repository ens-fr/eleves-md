# Site NSI Terminale par Desparias

!!! faq "Ce site correspond à :"
    - La création manuelle d'un élève de terminale en NSI
    - Un site portant sur le sujet de l'ADN dans le monde informatique

!!! warning "Ce site ne fait pas :"
    - De pub, pour aucun organisme (sité ou non dans celui-ci)
    - D'établissement d'une véritée
    - D'imposition d'un point de vue

!!! info "Ce site fait :"
    - La présentation d'un sujet potentiellement interessant
    - La présentation d'un sujet portant sur l'avenir de notre société