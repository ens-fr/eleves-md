﻿# Chapitre : PROBABILITES ; T spé

Rappels première

!!! note "Exemple 1:"

    Un pays subit une épidémie due à un virus.
    Un test est mis au point pour savoir si une personne est infectée par ce virus.
    Les autorités décident de tester massivement la population : les résultats obtenus sont que le test est positif pour
    0,31 % de personnes de cet échantillon.

    Par ailleurs, une étude scientifique a établi que ce test n'est certes, pas parfait : 99,5 % des personnes inféctées
    sont positives et 99,7 % des personnes non inféctées sont négatives.
    Ce qui fera dire sur des plateaux télé que ce test est remarquablement fiable (d'habitude, certains tests sont
    fiables à plutôt 75-80 %)

   1. Deux questions intéressantes se posent :

       - Quelle est la probabilité pour une personne d'être infectée ?

       - Parmi les personnes positives, combien sont réellement infectées ?


   2. Les responsables du pays pensent tester massivement puis à mettre en quarantaine les personnes positives
      au test (en fait une personne testée positives doit s'isoler 8 jours, l'état se chargeant de prendre en gharge
      les coûts de ces “arrêts forcés de travail”).

      Un économiste est chargé d'essayer de déterminer le coût de cette disposition :

       - Il établit qu'une personne en “huitaine” coûtera en moyenne 750 € à l'état.

       - Par ailleurs, une personne infectée a un coût sanitaire moyen de 125 € (médecin, médicaments ,
       hospitalisation , …) car certaines personnes infectées tombent malades et nécessitent des soins, donc des
       dépenses (médecin, médicaments , hospitalisation , …) .

       - Cependant si une personne est testée positive et isolée, elle transmettra peu la maladie .
       Par contre une personne infectée et testée négative va propager le virus et infectera en moyenne 1,7
       personnes ce qui coûtera, estime l'économiste, 1650 € par personne “fausse négative” à l'état.

       Estimer le coût moyen par habitant de cette disposition.

       Que se passerait-il si on ne mettait finalement pas en place cette disposition ?

## Probabilités conditionnelles

!!! example "Définition :" 
    
    Soient A et B deux événements d'un univers Ω, tels que $P(B)\neq0$.
    On appelle probabilité de A sachant B , la probabilité : $P_{b}(A)\frac{P(A\cap B)}{P(B)}$.

!!! example "Arbre pondéré :"

    A et B deux événements d'un univers Ω, tels que $P(B)\neq0$.

    ![Arbre Pondéré](images/diagram1.png)

!!! note "Propriété:"

    - la probabilité d'un événement est le produit des probabilités pour arriver à cet événement dansl'arbre.

    - la probabilité d'un événement est la somme des probabilités des chemins qui mènent à cet événement.


!!! note "Propriété :"

    Formule des probabilités totales

    $P(A) = P(A\cap B)+P(A\cap \overline{B}) = P_{B}(A)\times P(B) + P_{\overline{B}}(A) \times P(\overline{B})$




- Remarque : on étend cela aux cas d'arbres avec plus que deux branches

### Indépendance de deux événements:

!!! example "Définition :"

    Soient A et B deux événements d'un univers Ω.

    On dit que A et B sont indépendants si : $P(A\cap B) = P(A) \times P(B)$


### Variables aléatoires.


!!! example "Définition :"
    Soit Ω l'ensemble des issues possibles d' une experience aléatoire. Ω est appelé l'univers.
    Définir une variable aléatoire X , c'est définir une fonction sur Ω à valeurs dans $\mathbb{R}$.

    Définir une variable aléatoire X sur une experience aléatoire, c'est associer à chaque événement
    élémentaire {ωi } un nombre xi ( « une mise » ).

    notation : p(X=xi) = pi est la probabilité de l'événement : « Obtenir une mise xi ».

    L'ensemble des couples ( xi ; pi ) est la loi de probabilité de la variable aléatoire X.


!!! example "Définition :"

    L'Espérance mathématique de la variable aléatoire X est le nombre :

    $E(X)=\sum_{i=1}^{n}p_{i}x_{i} = p_{1}x_{1} + p_{2}x_{2} + ... +p_{n}x_{n}$

!!! example "Définition :"

    • La variance de la variable aléatoire X est le nombre :  
    $V(X)=\sum_{i=1}^{n}p_{i}(x_{i} - E(X)) = E(X^2) - E(X)^2$

    • L'écart type de la variable aléatoire X est le nombre :  
    $\sigma (X) = \sqrt{V(X)}$




!!! note "Propriétés :"
    
    a et b deux réels :
    • linéarité de l'Espérance : $E(aX + b) = aE(X) + b$


    • $V(aX) = a^2V(X)$, et donc : $\sigma (aX) = a\sigma(X)$

!!! quote "Exemple 2 :"

    Un pays subit une épidémie due à un virus.
    Un test est mis au point pour savoir si une personne est infectée par ce virus.
    Les autorités décident de tester un échantillon de la population : les résultats obtenus sont que le test est positif
    pour 0,14 % de personne de cet échantillon.

    Par ailleurs, une étude scientifique a établi que ce test n'est pas parfait : 9,2 % des tests positifs sont des faux
    positifs et 27,5 % des tests négatifs sont de faux négatifs.

    3.  a. Un personne a subit un test négatif, quelle est la probabilité qu'elle soit quand même infectée ?

        b. Déterminer la probabilité pour une personne prise au hasard dans ce pays d'être infectée par ce virus.

        c. Sachant qu'une personne est infectée par le virus et qu'elle avait subit un test, quelle est la probabilité que ce test ait été positif ?

    Un économiste est chargé d'essayer de déterminer l'efficacité économique de ce test pour savoir si il faut le
    produire pour le généraliser à toute la population.
    Il établit que, sans le test, une personne infectée a un coût moyen de 195 € (médecin, médicaments ,
    hospitalisation , arrêt de travail, …).
    Cependant si une personne est testée positive, en mettant en place certains dispositifs avant l'arrivée des
    symptômes, ce coût moyen tombe à 52 € .
    Ce test a un coût unitaire de 15 € .

    4. Déterminer le coût moyen par personne (ainsi que l'écart-type) de cette épidémie, avec le test puis sans le test. Conclure.


## Succession d'épreuves indépendantes

!!! quote "Exemple :"
    Chez une espèce de souris, la mutation de chacun des trois gênes A , B , C conduit à une modification de la
    couleur du pelage.
    Ces mutations sont indépendantes car les gènes sont situés sur des chromosomes différents.
    Des scientifiques ont établi que :
       – 1,5 % des souris présentent une mutation du gêne A
       – 3,8 % des souris présentent une mutation du gêne B
       – 14,4 % des souris présentent une mutation du gêne C

    On choisit au hasard une souris de cette espèce.
       1. Déterminer le probabilité que cette souris présentent les 3 mutations.
       2. Déterminer le probabilité que cette souris présentent seulement la mutation A.
       3. Déterminer le probabilité que cette souris présentent la mutation B.

    !!! note "Définition et propriété :"

    	Lors d'une succession d'épreuves, lorsque l'issue de chaque épreuve ne dépend pas des épreuves précédentes, on
    	parle d'épreuves indépendantes.

    	Lors d'une succession de n épreuves indépendantes, la probabilité d'une issue $(x_{1},x_{2},x_{3},...,x_{n})$ est égale au
    	produit des probabilités des issues $x_{1},x_{2},x_{3},...,x_{n}$

## Epreuve de Bernoulli , loi de Bernoulli, loi binomiale

- Exercice : Lors de son trajet quotidien, M.Relue rencontre 7 feux tricolores indépendants les uns des autres.
    A chaque feu rencontré, la probabilité qu'il passe sans s'arrêter est de 0,45.
    1. Déterminer la probabilité que lors de son trajet quotidien, M.Relue ne s'arrête pas.
    2. Déterminer la probabilité que lors de son trajet quotidien, M.Relue s'arrête 7 fois.
    3. Déterminer la probabilité que lors de son trajet quotidien, M.Relue s'arrête exactement 2 fois.
    4. Déterminer la probabilité que lors de son trajet quotidien, M.Relue s'arrête exactement 3 fois.
    5. Déterminer la probabilité que lors de son trajet quotidien, M.Relue s'arrête exactement 4 fois.



!!! example "Définition :"

    On appelle **épreuve de Bernoulli** une épreuve ayant deux éventualités : Succès (ou 1) et Echec (ou 0)

    **Loi de Bernoulli :**  
    |    $k$   |   0   |  1  |
    | $P(X=k)$ | $1-p$ | $p$ |


!!! example "Définition :"

    On appelle **schéma de Bernoulli**, la répétition plusieurs fois (n fois) et de manière ***indépendante***,
    d'une épreuve de Bernoulli (une épreuve ayant deux éventualités).

    Soit X la variable aléatoire correspondant au nombre de succès à l'issue du schéma de Bernoulli.
    On appelle **loi binomiale** la loi de probabilité de la variable aléatoire X

    n et p (probabilité de succès) sont les paramètres de X.


!!! example "Définition :"

    Soit un schéma de Bernoulli .
    Soit l'arbre correspondant à ce schéma de Bernoulli.
    On appelle coefficient binomial, noté $\binom{n}{k}$
    le nombre de chemins de l'arbre réalisant k succès.
    ($0 \leqslant k \leqslant n$)

!!! note "Propriété :"

    Soit X une variable aléatoire suivant un loi binomiale de paramètres n et p.

    alors, $P(X=k)=\binom{n}{k}p^k(1-p)^{n-k}$, pour $0 \leqslant k \leqslant n$.

    $E(X) = np$
    $V(X) = np(1-p)$ et $\sigma(X) = \sqrt{np(1-p)}$

!!! note "Propriété :"

    Soit n un entier supérieur ou égal à 1 et $0 \leqslant k \leqslant n$.

    - $\binom{n}{0} = 1$
    - $\binom{n}{n} = 1$
    - $\binom{n}{k} = \binom{n}{n-k}$
    - $\binom{n}{k} + \binom{n}{k+1} = \binom{n+1}{k+1}$

!!! done "Triangle de Pascal :"

    |       | $k=0$ | $k=1$ | $k=2$ | $k=3$ | $k=4$ | $k=5$ | $k=6$ |
    | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- |
    | $n=0$ |   1   |       |       |       |       |       |       |
    | $n=1$ |   1   |   1   |       |       |       |       |       |
    | $n=2$ |   1   |   2   |   1   |       |       |       |       |
    | $n=3$ |   1   |   3   |   3   |   1   |       |       |       |
    | $n=4$ |   1   |   4   |   6   |   4   |   1   |       |       |
    | $n=5$ |   1   |   5   |  10   |  10   |   5   |   1   |       |
    | $n=6$ |   1   |   6   |  15   |  20   |  15   |   6   |   1   |

    etc...



    (On admet que $\binom{0}{0} = 1$)