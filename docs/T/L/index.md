# Géométrie dans l'espace

## 1. Vecteurs de l'espace

On étend à l'espace la notion de vecteurs avec les mêmes définitions, propriétés et notations.

### Propriété


!!! p ""
    - somme de vecteurs $\overrightarrow{u}+\overrightarrow{v}$
    - vecteur nul $\overrightarrow{0}$
    *[vecteur nul]: Ce vecteur n'a ni direction, ni sens
    - règle du parallélogramme
    - relation de Chasles $\overrightarrow{AB}+\overrightarrow{CB} = \overrightarrow{AB}$
    - vecteurs colinéaires :
        $\overrightarrow{u}$ et $\overrightarrow{v}$ sont colinéaires $\Leftrightarrow$ il existe un réel k tel que $\overrightarrow{u} = k \overrightarrow{v}$
    - produit d'un vecteur par un nombre réel

![Image title](images/carre_geo1.svg){ width="400", align=right }

???+ info "Exercice 1"
    - [ ] 1. citer des vecteurs colinéaires.

    - [ ] 2. placer le point $I$ tel que : $\overrightarrow{AB} = \overrightarrow{DH} + \frac 1 2 \overrightarrow{AB}$.

---

---

---

--------

???+ info inline end "Exercice 2"

    ![Image title](images/carre_geo1.svg)

    - [ ] 1. citer des points coplanaires et des points non coplanaires.

    - [ ] 2. $\overrightarrow{AD}, \overrightarrow{HG}$ et $\overrightarrow{BD}$ sont-ils coplanaires ?

    - [ ] 3. $\overrightarrow{AD}, \overrightarrow{AB}$ et $\overrightarrow{AE}$ sont-ils coplanaires ?




!!! d "Définition"

    Alignements : trois points de l'espace $A$, $B$ et $C$ sont **alignés** si et seulement si les vecteurs $\overrightarrow{AB}$ et $\overrightarrow{CD}$ sont colinéaires

    Parallélisme : deux droites de l'espace $(AB)$ et $(CD)$ sont **parallèles** si et seulement si les vecteurs $\overrightarrow{AB}$ et $\overrightarrow{CD}$ sont colinéaires

### Coplanarité

- quatre points de l'espace A, B, C et D sont **coplanaires** si et seulement si il existe un plan contenant ces quatre points.

- trois vecteurs $\overrightarrow{u}$, $\overrightarrow{v}$ et $\overrightarrow{w}$ sont **coplanaires** si et seulement si il existe un plan pouvant contenir des représentants de ces trois vecteurs.

---------

$\overrightarrow{u}$ est appelée **combinaison linéaire** des vecteurs $\overrightarrow{v}$, $\overrightarrow{w}$ et $\overrightarrow{t}$ si il existe des nombres réels $x$, $y$ et
$z$ tels que :

$$
\overrightarrow{u} = x\overrightarrow{v} + y\overrightarrow{w} + z\overrightarrow{t}
$$

!!! p "Propriété"

    trois vecteurs $\overrightarrow{u}$, $\overrightarrow{v}$ et $\overrightarrow{w}$ sont **coplanaire** si et seulement si $\overrightarrow{w}$ peut s'écrire comme combinaison linéaire de $\overrightarrow{u}$ et $\overrightarrow{v}$

    C'est à dire qu'il existe des nombres réels $x$ et $y$ tes que $\overrightarrow{w} = x\overrightarrow{u} + y\overrightarrow{v}$

## 2. Droites de l'espace

On appelle **vecteur directeur** d'une droite D, tout vecteur $\overrightarrow{u}$ non nul, colinéaire à un vecteur formé par deux points distincts de la droite D.

![droite](images/droite_1.svg)


> **Remarque** :
>
> - la direction d'un vecteur directeur de D définit la direction de la droite D.
> - Deux vecteurs directeurs d'une même droite sont colinéaires.



### Caractérisation vectorielle de droite

Soit A un point et $\overrightarrow{u}$ un vecteur **non nul** de l'espace.

L'ensemble des points M de l'espace tel que $\overrightarrow{AM} = k\overrightarrow{u}$ , $k\in\Bbb R$

$\overrightarrow{u}$ **est un vecteur directeur** de $d$ .

On dit que $(A; \overrightarrow{u})$ est **un repère** de cette droite


## 3. Plans de l'espace


### Caractérisation vectorielle de plan

Soit A un point et $\overrightarrow{u}$ et $\overrightarrow{v}$ deux vecteurs **non colinéaires** de l'espace.

L'ensemble des points M de l'espace tel que $\overrightarrow{AM} = x\overrightarrow{u} + y\overrightarrow{v}$ , $x\in\Bbb R$ et $y\in\Bbb R$ est le **plan** (ABC) où $\overrightarrow{AB} = \overrightarrow{u}$ et $\overrightarrow{AC} + \overrightarrow{v}$ 


On dit que $(A; \overrightarrow{u}, \overrightarrow{v})$ est **un repère** de ce plan.

On dit que la base $(\overrightarrow{u}, \overrightarrow{v})$ **dirige** le plan.



![plan](images/plan_1.svg)

!!! p "Propriétés"

    Deux plans dirigés par le même couple de vecteurs non colinéaires sont parallèles ou confondus

## 4. Bases et repères de l'espace

=== "Base de l'espace"

    Trois vecteurs $\overrightarrow{i}, \overrightarrow{j}$ et $\overrightarrow{k}$ non coplanaires de l'espace forment une base de l'espace.
    Pour tout vecteur $\overrightarrow{u}$ de l'espace, **il existe un unique** triplet $(x ; y ; z)$ de réels tels que $\overrightarrow{u} = x\overrightarrow{i} + y\overrightarrow{j} + z\overrightarrow{k}$

     $(x ; y ; z)$ sont les **coordonnées** de $\overrightarrow{u}$ dans $(\overrightarrow{i} ; \overrightarrow{j} ; \overrightarrow{k})$
     
=== "Repère de l'espace"

    Soient trois vecteurs $\overrightarrow{i}, \overrightarrow{j}$ et $\overrightarrow{k}$ non coplanaires de l'espace et $O$ un point fixe de l'espace.

    $(O ; \overrightarrow{i} ; \overrightarrow{j} ; \overrightarrow{k})$ est un **repère de l'espace**, c'est à dire que :
    Pour tout point $M$ de l'espace, **il existe un unique** triplet $(x ; y ; z)$ de réels tels que $\overrightarrow{OM} = x\overrightarrow{i} + y\overrightarrow{j} + z\overrightarrow{k}$
    $(x ; y ; z)$ sont les **coordonnées** de $M$ dans $(O ; \overrightarrow{i} ; \overrightarrow{j} ; \overrightarrow{k})$

    ![plan](images/droite_2.svg)

> **Remarque** :
>
> - De même que dans le plan, on parle de *repères orthogonaux, orthonormés*.
> - De même que dans le plan, on utilise ces coordonnées pour traduire **la colinéarité de deux vecteurs et l'alignement de trois points.**



### Propriétés

!!! p ""

    Le plan est muni d'un repère $(O ; \overrightarrow{i} ; \overrightarrow{j} ; \overrightarrow{k})$.

    Soient $A(x_A;y_A;z_A)$ et $B(x_B;y_B;z_B)$ deux points de $I$ le milieu de $[AB]$

    Alors $\overrightarrow{AB}  \begin{pmatrix} x_B - x_A \\ y_B - y_A \\ z_B - z_A \end{pmatrix}$ et $I  (\frac{x_B + x_A}{2}; \frac{y_B + y_A}{2}; \frac{z_B + z_A}{2})$

!!! p ""

    Le plan est muni d'un **repère orthonormé** $(O ; \overrightarrow{i} ; \overrightarrow{j} ; \overrightarrow{k})$.

    Soient $A(x_A;y_A;z_A)$ et $B(x_B;y_B;z_B)$ deux points

    Alors $AB = \sqrt{(x_B - x_A)^2 + (y_B - y_A)^2 + (z_B - z_A)^2}$

    ??? info "voici un script python"

        ```python

        from math import sqrt

        A = [1, 4, 3]
        B = [1, 5, 2]

        def longueur(A: list, B: list) -> float:
          x_a, y_a, z_a = A
          x_b, y_b, z_b = B
          return sqrt( (x_b - x_a)**2 + (z_b - z_a)**2 + (z_b - z_a)**2 )
    
        ```

        ```python

        print(longueur(A, B))

        > 1.4142135623730951

        ```

## 5. Orthogonalité dans l'espace

### Définitions


!!! d ""

    - Deux droites sont dites **orthogonales** lorsque leurs parallèles passant par un même point sont perpendiculaires.
      Exemple : 
          - $(AE)$ et $(AC)$ sont perpendiculaire (et donc orthogonales)
          - $(AE)$ et $(DC)$ sont orthogonales et non perpendiculaires
    - Deux vecteurs sont dits orthogonaux lorsque des droites dirigées par ces vecteurs sont orthogonales
    - Une droite est orthogonale à un plan lorsqu'elle est orthogonale à toutes les droites du plan.
      Exemple :
          - $(HD)$ est orthogonale au plan $(ABC)$
    
    !!! p ""

        une droite est orthogonale à toute droite d'un plan si et seulement si elle est orthogonale à deux droites sécantes de ce plan ( suffisant pour montrer qu'une droite est orthogonale à un plan)

!!! d ""

    Soit $P$ un plan de l'espace.

    On dit que $\overrightarrow{n}$ est un **vecteur normal** de $P$ si $\overrightarrow{n}$ est un vecteur directeur d'une droite orthogonal à $P$

Projections orthogonales :

=== "Le projeté orthogonal"

    **Le projeté orthogonal** d'un point $M$ sur une droite $d$ est le point d'intersection $H$ de $d$ avec le plan passant par $M$ et orthogonal à $d$.
    $H$ est alors le point de $d$ le plus proche de $M$.
    On dit que $MH$ est la **distance** de $M$ à $d$.

=== "La projection orthogonale"

    **La projection orthogonale** d'un point $M$ sur un plan $P$ est le point d'intersection $H$ de $P$ avec la droite passant par $M$ et orthogonal à $P$.
    $H$ est alors le point de $P$ le plus proche de $M$.
    On dit que $MH$ est la **distance** de $M$ à $P$.

## 6. Produit scalaire dans l'espace

!!! d "définition"

    Soient $\overrightarrow{u}$ et $\overrightarrow{v}$ deux vecteurs de l'espace.

    Soient $\overrightarrow{AB}$ et $\overrightarrow{AC}$ des représentants de $\overrightarrow{u}$ et $\overrightarrow{v}$ (on se ramène dans un plan).

    On définit le produit scalaire de $\overrightarrow{u}$ et $\overrightarrow{v}$ dans l'espace par : $\overrightarrow{u}.\overrightarrow{v} = \overrightarrow{AB}.\overrightarrow{AC}$

    En fait, *on se ramène dans le plan.*


### Propriétés

!!! p ""

    - $$ \overrightarrow{AB}.\overrightarrow{AC} = \overrightarrow{AB} \times \overrightarrow{AC} \times \cos(\widehat{BAC}) $$

    - avec le projeté orthogonal

    
    - $$\overrightarrow{u}.\overrightarrow{v} = \overrightarrow{v}.\overrightarrow{u}$$

    - $$(k\overrightarrow{u}).\overrightarrow{v} = \overrightarrow{u}.(k\overrightarrow{v}) = k(\overrightarrow{u}.\overrightarrow{v})$$

    - $$\overrightarrow{W}.(\overrightarrow{u} + \overrightarrow{v}) = \overrightarrow{w}.\overrightarrow{u} + \overrightarrow{w}.\overrightarrow{v}$$

    Formules de polarisation

    === "première"
        $\overrightarrow{u}.\overrightarrow{v} = \cfrac{1}{2}\left(\left\|\overrightarrow{u} + \overrightarrow{v}\right\|^2 - \left\|\overrightarrow{u}\right\|^2 - \left\|\overrightarrow{v}\right\|^2 \right)$
        
    === "deuxième"    
    
        $\overrightarrow{u}.\overrightarrow{v} = \cfrac{1}{2}\left(\left\|\overrightarrow{u}\right\|^2 + \left\|\overrightarrow{v}\right\|^2 - \left\|\overrightarrow{u} - \overrightarrow{v}\right\|^2 \right)$


=== "première"
    !!! p ""

        Soit deux vecteurs $\overrightarrow{u}$ et $\overrightarrow{v}$.

        $\overrightarrow{u}$ et $\overrightarrow{v}$ sont orthogonaux si et seulement si $\overrightarrow{u}.\overrightarrow{v} = 0$

=== "deuxième"

    !!! p ""

        Soit $d_1$ une droite de vecteur directeur $\overrightarrow{u}_1$
        Soit $d_2$ une droite de vecteur directeur $\overrightarrow{u}_2$

        $d_1$ et $d_2$ sont orthogonale si et seulement si $\overrightarrow{u}_1.\overrightarrow{u}_2 = 0$

!!! p ""

    Le plan est muni d'un repère $(O ; \overrightarrow{i} ; \overrightarrow{j} ; \overrightarrow{k})$.

    soit $\overrightarrow{u}  \begin{pmatrix} a \\ b \\ c \end{pmatrix}$ et $\overrightarrow{v}  \begin{pmatrix} a' \\ b' \\ c' \end{pmatrix}$ deux vecteurs de l'espace.

    [$\overrightarrow{u}.\overrightarrow{v} = aa' +bb' + cc'$](https://console.basthon.fr/?script=eJyVkEFOwzAQRfeRcoevbEggoK4rtRskTsA-cpMJWIpmImccpdyIc3AxDElplFBQ7Y395_vP8_RUKnlXeOxQN2I0tdx6TZNSxFXC_PFOMKg8-tEJn2RZftl7uMJbLr1xNN2K_k-em_nD_h-ga8zlyhxHX7uiGq2TylstutI0xjpKT6x-C_VtQzl-6Cclw_0eB5FmG0cIK0mS8fBIqoRauFQrDKUuXDuLivxwSunQCSvE6au8CJtQIYUj7sUSPH8HB2geIxcdlrRhnpuxUovDkOMIy3iz7fkbM_5sAv416W6HAbc4zvva9XywCy0fNrMkR-od49l5GkVqOlrXn0yQz2octc6yppfnPwfPPgGCe-tT){ .md-button }

## 7. Représentations paramétriques et équation cartésiennes

=== "Représentation paramétrique d'une droite"

    Soit $d$ la droite passant par $A(x_A;y_A;z_A)$ et de vecteur directeur $\overrightarrow{u}  \begin{pmatrix} a \\ b \\ c \end{pmatrix}$.

    $M(x;y;z)$ appartient à $d$ signifie qu'il existe un réel $t$ tel que : 

    $$\left\{\eqalign{x &= x_A + at\\y &= y_A + bt\\z &= z_A + ct}\right\}$$

    Ceci est **une représentation paramétrique** de la droite $d$.

=== "Représentation paramétrique d'un plan"

    Soit $P$ le plan passant par $A(x_A;y_A;z_A)$ et dirigé par les vecteurs $\overrightarrow{u}  \begin{pmatrix} a \\ b \\ c \end{pmatrix}$ et $\overrightarrow{v}  \begin{pmatrix} a' \\ b' \\ c' \end{pmatrix}$

    $M(x;y;z)$ appartient à $P$ signifie qu'il existe deux réels $t$ et $t'$ tels que :

    $$\left\{\eqalign{x &= x_A + at + a't'\\y &= y_A + bt + b't'\\z &= z_A + ct + c't'}\right\}$$

    Ceci est **une représentation paramétrique** du plan $P$.


### Propriétés

!!! p ""

    - Soit $P_1$ un plan de vecteur normal $\overrightarrow{n}_1$
    - Soit $P_2$ un plan de vecteur normal $\overrightarrow{n}_2$

    $P_1$ et $P_2$ sont orthogonaux si et seulement si $\overrightarrow{n}_1.\overrightarrow{n}_2 = 0$

!!! p ""

    Soit $P$ un plan de vecteur normale $\overrightarrow{n}  \begin{pmatrix} a \\ b \\ c \end{pmatrix}$ alors $P$ a pour **équation cartésienne** $ax + by + cz + d = 0$

    **Réciproquement** : L'ensemble des points qui vérifient : $ax + by + cz + d = 0$ est un plan de vecteur normal $\overrightarrow{n}  \begin{pmatrix} a \\ b \\ c \end{pmatrix}$.

## 8. Contenu additionnel

un tableau :

| Java le ~~meilleur~~ ^^pire^^ language de programmation | Python le ==best== |
|-----------|---------|
| une addition en java[^1]   |   |


--------

Graphe :

``` mermaid
graph LR
  A[Se lever] --> B{S'habiller};
  B -->| Femme | C[Maquiller];
  C --> D[Huiles essentiele];
  D --> E[Soin visage];
  E --> G;
  B ----> | Homme | F[WC];
  F ----> G[Partir travailler];
```

Un [lien](https://e-nsi.gitlab.io/pratique/) vers un site incroyable



``` mermaid
flowchart TD
  A[1] --> B([2]);
  B --> C([3]);
  C --> D([4]);
  D --> E[5];
  D --> A[1];
```




[^1]: sur de voir ca ? ok [...](https://perso.telecom-paristech.fr/hudry/coursJava/debutant/variables.html)


      


