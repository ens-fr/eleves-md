## Une fin tragique...
!!! fail "La mort d'Alan Turing 🪦"
    Mais les deux dernières années de sa vie précipitent une _fin tragique._ Une aventure avec un jeune homme le conduit au tribunal où il plaide coupable pour « pratiques indécentes réitérées en compagnie d’un autre homme ». Considérant qu’il a bien trop de travail pour aller en prison, il accepte l’alternative qu’on lui propose : la castration chimique. Ils peuvent bien faire ce qu’ils veulent de son corps pense-t-il, son esprit, son logiciel, restera intact… « Mais, sous l’effet des hormones, le voilà transformé en ce qu’il s’imagine être une presque femme. Et si la pensée et le corps entretenaient des liens plus profonds qu’il ne le croyait ? Et s’il s’était trompé ? », interroge Jean Lassègue, se figurant les dernières pensées du mathématicien. **Le 7 juin 1954**, Alan Turing est retrouvé raide et froid dans son lit. _La pomme au cyanure_🍎 a fait son œuvre.

??? info "Un hommage, 60 après les évènements !🎖️"
    
    Alan Turing a été gracié sur proposition du ministre de la Justice Chris Grayling qui a rendu hommage à un «esprit brillant». «À Bletchley Park, son génie a contribué à sauver des milliers des vies», a commenté le ministre. «Sa condamnation que nous considérerions aujourd'hui comme injuste et discriminatoire, est désormais annulée», a-t-il ajouté, l'homosexualité ayant cessé d'être illégale en Grande-Bretagne en 1967. «Alan Turing mérite que l'on se souvienne de lui pour sa contribution mythique à l'effort de guerre. Un pardon royal est digne de cet homme exceptionnel», a-t-il conclu. Seules quatre grâces de ce type ont été accordées depuis 1945.

    <figure markdown>
    ![Elizabeth II](Image/Partie_4/elizabeth.jpg){ width="300" }
    <figcaption>[La Reine Elizabeth II ](https://images.ladepeche.fr/api/v1/images/view/6260f02a3fd9cb6eb46894f4/large/image.jpg?v=2) </figcaption>
    </figure>
    

## Au-delà de la réalité...

!!! tldr "... il y a la fiction 🎥"
    Véritable pilier  de l'hsitoire de l'informatqiue, Alan Turing n'aura de cesse de se retrouver dans de nombreux médias tant son génie fut suprenant, en voici quelques uns: 
        
    ??? tip "Un hommage cinématographique! "Imitation Game"🍿"
        ![type:video](./videos/IG.mp4)

        <figure markdown>
        ![affiche de film ](Image/Partie_4/film.jpg){ width="300" }
        <figcaption>[Imitation Game ](https://www.allocine.fr/film/fichefilm-198371/dvd-blu-ray/?cproduct=291566) </figcaption>
        </figure>
        
    ??? tip "Une pièce aussi intrigante qu'Enigma !🎭 "
        <figure markdown>
        ![Affiche pièce de théâtre](Image/Partie_4/theatre.jpeg){ width="300" }
        <figcaption>[La Machine de Turing](https://www.billetreduc.com/270826/evt.htm) </figcaption>
        </figure>
    ??? tip "Et enfin, pour les plus littéraires📖"
        <figure markdown>
        ![livre biographie Alan Turing](Image/Partie_4/livre.jpg){ width="300" }
        <figcaption>[Turing, l'homme qui inventa l'informatique](https://www.dunod.com/sciences-techniques/alan-turing-homme-qui-inventa-informatique) </figcaption>
        </figure>

#### Avez vous bien retenu ?🧠

=== "Question 1"
    !!! faq "Question 1"
        Le test de Turing  est:

        - [ ] Une proposition de test d'intelligence artificielle
        - [ ] Une machine informatique
        - [ ] Une proposition de test de formule mathématique
    
        ??? done "Réponse"
            - ✅ Une proposition de test d'intelligence artificielle
            - ❌ Une machine informatique
            - ❌ Une proposition de test de formule mathématique
    
=== "Question 2"
    !!! faq "Question 2"

        Le créateur d'Enigma est :

        - [ ] Alan Turing
        - [ ] Arthur Scherbius
        - [ ] Tommy Flowers

        ??? done "Réponse"
            - ❌ Alan Turing
            - ✅ Arthur Scherbius
            - ❌ Tommy Flowers
        
=== "Question 3"
    
    !!! faq "Question 3"

        Le premier ordinateur programmable se nomme:

        - [ ] Colossus
        - [ ] Manchester Mark I
        - [ ] Enigma
    
        ??? done "Réponse"
            - ✅ Colossus
            - ❌ Manchester Mark I
            - ❌ Enigma

=== "Question 4"
    
    !!! faq "Question 4"
    
        La base d'opération des cryptanalystes d'Enigma fut :

        - [ ] La Maison Blanche
        - [ ] Bletchley Park
        - [ ] La  Royal Society

        ??? done "Réponse"
            - ❌ La Maison Blanche
            - ✅Bletchley Park
            - ❌ La  Royal Society

=== "Question 5"
    !!! faq "Question 5"

        Les travaux d'Alan Turing et de ses compagnons ont _rétreci_ la guerre de : 

        - [ ] 1 ans
        - [ ] 2 ans
        - [ ] 4 ans

        ??? done "Réponse"
            - ❌ 1 ans
            - ✅ 2 ans
            - ❌ 4 ans


    

       
#### Notes et réferences

!!! done "Les sources à l'origine de ce site 📝"

    Ce site a été réalisé à l'aide de nombreuses ressources. Voyez-le comme une sorte de synthèse. Voici les sites qui m'ont principalement aidé à sa réalisation:
    
    - [Wikipedia](https://fr.wikipedia.org/wiki/Alan_Turing)
    - [Interstices](https://interstices.info/turing-a-lassaut-denigma/)
    - [Le Journal, CNRS](https://lejournal.cnrs.fr/articles/alan-turing-genie-au-destin-brise)





