# Alan Turing, héros et précurseur de l'informatique

## La biographie d'un homme plus que talenteux

!!! tldr "Une enfance peu commune !👶"

        Né le 23 juin 1912 à Paddington, **Alan Mathison Turing** est un célèbre mathématicien anglais. Élève surdoué et précoce, son génie est vite repéré par ses proches et ses professeurs : il aurait appris à lire seul en trois semaines. Sa passion pour les études est même couverte par la presse, le jeune garçon de 13 ans ayant parcouru 90 km à vélo pour rejoindre son école un jour de grève générale. Passionné de sciences et de mathématiques, Alan Turing se décide à se lancer dans cette voie lorsqu’un de ses amis, Christopher Morcom, un brillant étudiant en sciences, meurt de la tuberculose bovine. Il veut ainsi lui rendre hommage.
        <figure markdown>
        ![Turing](Image/Partie_1/alanTuring.jpg){ width="500" }
        <figcaption>[Alan Turing](https://i0.wp.com/wallonica.org/wp-content/uploads/2020/05/alan-turing-600x338.jpg?resize=600%2C338&ssl=1)
        </figcaption>
        </figure>

        
     
        
        ??? fail "Une tragédie marquante 😢"
            Plus qu'un ami, Christopher Morcom a eu un véritable impact au sein de la vie d'Alan Turing, malgré sa mort précoce à seulement 13 ans...
            <figure markdown>
                ![Christopher](Image/Partie_1/christopher.jpg){ width="200" }
                <figcaption>[Christopher Morcom](https://images.findagrave.com/photos250/photos/2015/136/140865246_1431901638.jpg)
</figcaption>
            </figure>



## Un grand visionnaire ! 
!!! tip "Les débuts de l'informatique !💾"
    Les travaux qu’Alan Turing réalise à l’université, jusqu’en 1938, sont aujourd'hui devenus des références en matière de science informatique. Visionnaire, il est le créateur de la **Machine de Turing**, une expérience de pensée et de concepts de programmations qui prendront corps avec la création des ordinateurs, quelques années plus tard. Lors de la Seconde Guerre mondiale[^1], Alan Turing est engagé pour faire des recherches sur les cryptographies[^2] de la machine nazie Enigma, recherches qu’il mène avec succès. Après la guerre, le scientifique travaille sur les premiers modèles d’ordinateur appelés _Manchester Mark I[^3]_. 
    <figure markdown>
    ![machine_de_turing](Image/Partie_1/machine_de_turing.jpg){ width="500" }
    <figcaption>[Représentation de la machine de Turing](https://www.lemonde.fr/blog/binaire/files/2017/04/Turing-machine-general-1024x505.jpg)</figcaption>
    </figure>

!!! faq " Est-ce qu'une machine peut penser ? 🤖"
    
    Alan Turing soulève ensuite les passions en exposant sa théorie sur l’intelligence artificielle. Il publie _Can a machine think ?_[^4] Dans cet ouvrage, il présente sa philosophie de la machine intelligente. Il définit celle-ci par un test resté célèbre, le test de Turing, qui simule une conversation entre un homme et une machine. En 1951, il est admis à la Royal Society[^5] et l'année suivante, Alan Turing crée un programme de jeu d'échecs, mais aucune machine n'est assez puissante pour le lire !


                                                                     
??? info "Pour les plus curieux ! 📒"

    Le test de Turing est une proposition de test d’intelligence artificielle fondée sur la faculté d'une machine à imiter la conversation humaine. Décrit par Alan Turing en 1950 dans sa publication _Computing Machinery and Intelligence_, ce test consiste à mettre un humain en confrontation verbale à l’aveugle avec un ordinateur et un autre humain.

    Si la personne qui engage les conversations n’est pas capable de dire lequel de ses interlocuteurs est un ordinateur, on peut considérer que le logiciel de l’ordinateur a passé avec succès le test. Cela sous-entend que l’ordinateur et l’humain essaieront d’avoir une apparence sémantique humaine. 

    Pour conserver la simplicité et l’universalité du test, la conversation est limitée à des messages textuels entre les protagonistes. 
    
    <figure markdown>
    ![test](Image/Partie_1/test.png){ width= "200"}
    <figcaption>[Test de Turing](https://commons.wikimedia.org/wiki/File:Turing_Test_version_3.png) </figcaption>
    </figure>
    
    ??? check "Robot ou humain....?🤖/👨"
    
        <center>[Le test de Turing](https://www.conishiwa.org/zones/projets/ia/turing.htm){ .md-button }</center>


    
[^1]: [Pour en savoir plus sur cette guerre, révélatrice de  progrès technologiques](https://fr.wikipedia.org/wiki/Seconde_Guerre_mondiale)
[^2]:[Une technique d'écriture....spéciale](https://fr.wikipedia.org/wiki/Cryptographie)
[^3]: [L'_ancêtre_ de nos ordinateurs](https://fr.wikipedia.org/wiki/Manchester_Mark_I)
[^4]: [Un ouvrage passionnant](https://fr.wikipedia.org/wiki/Computing_Machinery_and_Intelligence)
[^5]: [Une école de renom](https://fr.wikipedia.org/wiki/Royal_Society)
