## Bletchley Park, le nid d'opérations mémorables


!!! info "Un manoir IMPRESSIONNANT🏘️"
    
     <figure markdown>
    <center>![Bletchley Park](Image/Partie_3/bletchley.jpg){ width="500" }</center>
    <figcaption>[Le manoir de Bletchley Park](https://fr.m.wikipedia.org/wiki/Fichier:Bletchley_Park_-_Draco2008.jpg) </figcaption>
    </figure>
    

    Situé près de Londres, ce manoir de style victorien et des hangars, appelés « huttes », construits dans son parc, abritèrent jusqu’à sept mille personnes décryptant chaque jour les messages ennemis interceptés.

!!! tip  "Un manoir certes, mais avant tout une base d'opérations...🚨"
    !!! example "Des métiers divers pour un objectif commun !"
        Au sein des **7000 personnes** présentes au sein de l'impressionnant manoir _Bletchley Park_, on retrouve:
    
        - des mathématiciens
        - des linguistes
        - des ingénieurs
        - des hauts-fonctionnaires
        - des cryptologues
        - ...
    
        La liste est longue, et pour cause, la résolution d'Enigma est tellement importante et complexe qu'elle nécessite la présence de différentes professions !
    
    !!! warning "Une équipe du tonnerre ! 💥"

        Parmi les résidents du manoir, quelques uns sortent du lot. ce sont des génies ayant grandement contribué à la résolution d'Enigma, mais avant out des amis proches de Turing, qui sans eux n'aurait peut-être pas eu le courage de mener ce "combat" contre Enigma jusqu'au bout...
        <center>
        
        | Prénom et Nom  | Photographie             | 
        | :-------------------:| :-------------------------------------: |
        | [Joan Clarke](https://static.wixstatic.com/media/6f6a03_afdc4153025f465bacc2ae692eaf6c4b~mv2.jpg/v1/fill/w_280,h_280,al_c,lg_1,q_90/6f6a03_afdc4153025f465bacc2ae692eaf6c4b~mv2.webp)     | <center>![Joan Clarke](Image/Partie_3/clarke.jpg){ width="100" }</center> |
        | [Conel Hugh O'Donel Alexander](https://en.wikipedia.org/wiki/File:Alexander_Conel.jpg)       |![Conel Hugh O'Donel Alexander](Image/Partie_3/alexander.jpg){ width="100" } |
        | [Stewart Menzies](https://en.wikipedia.org/wiki/File:Alexander_Conel.jpg)    | ![Stewart Menzies](Image/Partie_3/stewart.jpg){ width="100" }|
        | [Alastair Denniston](https://www.gchq.gov.uk/person/alastair-denniston)    | ![Alastair Denniston](Image/Partie_3/denniston.jpg){ width="100" } |      	
        | [John Cairncross](https://en.wikipedia.org/wiki/File:John_Cairncross.jpg)    | ![John Cairncross](Image/Partie_3/john.jpg){ width="100" }|      
        </center>

## Des opérations secrètes mais cruciales

!!! info "Une idée ingénieuse💡"

     <figure markdown>
    <center>![cycle de lettres](Image/Partie_3/wetter.jpg){ width="300" }</center>
    <figcaption> [Schéma d'un "cycle de lettres"](https://interstices.info/wp-content/uploads/jalios/ds-enigma/wetter.jpg)</figcaption>
    </figure>

    Ils utilisèrent le fait que certaines parties des messages étaient faciles à deviner. En particulier, les bulletins météo, transmis chaque matin à la même heure, contenaient tous le mot _Wetter_ (qui signifie "temps" en allemand).

    Alan Turing eut alors l’idée de rechercher des cycles de lettres. Pour qu’une suite de lettres forme un tel cycle, il faut que chaque lettre de la suite soit une version chiffrée de la lettre précédente, et que la version chiffrée de la dernière lettre corresponde à la première.

    Par exemple, « w », « e » et « t » peuvent former un cycle de trois lettres : 
        
    - « w » est transformé en « e »
    - « e » est transformé en « t »
    - « t » est transformé en « w »
    - ...




??? example "Exemple de cycle de lettres exploité par Alan Turing pour la cryptanalyse d’Enigma.🔃"
    Comme le tableau de connexions effectue les mêmes transpositions à la fin d’un chiffrement et au début du suivant, son effet s’annule complètement au sein d’un tel cycle. On dispose donc d’un motif caractéristique des rotors : il suffisait alors d’essayer toutes les possibilités pour les rotors pour trouver celle qui produisait le motif attendu. Pour tester le million de possibilités, Alan Turing fit construire des machines électromécaniques appelées _bombes_[^18], reproduisant les rotors d’Enigma et permettant d’essayer en parallèle jusqu’à **vingt mille configurations par seconde**. Une fois la position des rotors déterminée, il devenait possible de décrypter une partie du message (celle correspondant aux six lettres non affectées par le tableau de connexions) puis d’en déduire les transpositions.

    <figure markdown>
    ![exemple de cycles de lettres](Image/Partie_3/wetter2.jpg){ width="300" }
    <figcaption>[Exemple de cycle de lettres exploité](https://interstices.info/wp-content/uploads/jalios/ds-enigma/wetter2.jpg) <figcaption>
    </figure>
## Un manoir d'héros 
!!! quote inline end "Des héros de l'ombre🦸"

    > « Une fois qu’ils ont réussi à faire ça, les Britanniques ont réussi à couler les sous-marins allemands les uns après les autres. Cela a ouvert les voies maritimes. Ça a permis le débarquement de juin 1944, l’arrivée des troupes [alliées]. »

    De Jacques Marchand

!!! danger "Enigma, la première victime de beaucoup d'autres !💀"
    Les cryptanalystes de Bletchley Park ne se limitèrent pas à Enigma. Ils attaquèrent également:
        
    - **_Purple_**[^19], le système de chiffrement japonais
        
    - **_Lorenz_**[^20], utilisée à partir de 1941 par le commandement suprême des forces armées allemandes. 
        
    Le secret autour de l’opération _Ultra_ fut tellement bien gardé pendant trente ans que certains de ces mathématiciens, dont Alan Turing, furent parfois accusés de ne pas avoir contribué à l’effort de guerre. Ils auront néanmoins contribué à raccourcir la guerre de presque deux ans étant à l'origine de nombreuses opérations réussies comme le débarquement de Normandie[^21] de juin 1944.




??? info "Pour aller plus loin 🏃"

    !!! info " Et de nos jours alors ?🏙️"
    
        Depuis la seconde guerre mondiale, le champ d’application de la cryptographie a glissé de plus en plus du domaine militaire vers le domaine économique. Par exemple, la sécurité des données personnelles comme les données médicales est devenue un enjeu crucial.

        L’augmentation de puissance des ordinateurs rend naturellement plus rapides les attaques exhaustives sur les systèmes de chiffrement. Les chiffrements actuels doivent donc utiliser des clés de plus grande taille et être plus complexes. Pour mettre à l’épreuve leur sécurité, les chercheurs étudient différents types d’attaques qui exploitent des structures particulières des méthodes de chiffrement, tout comme Alan Turing avait exploité une régularité d’Enigma.

        Une avancée majeure, dans les années soixante-dix, a été la définition de la **cryptographie à clé publique**[^22], à partir du principe mathématique des fonctions à sens unique, faciles à calculer mais en pratique impossibles à inverser. La première réalisation concrète en a été le système _RSA_[^23], qui repose sur la difficulté de la factorisation de grands nombres entiers. D’autres cryptosystèmes sont construits à partir des courbes elliptiques.

        Et demain, peut-être, l’ordinateur quantique[^24] viendra-t-il _bouleverser ce paysage ?_

    !!! info  "Une passion peu commune: la morphogenèse🌱"
        
        Fleur de tournesol, pomme de pain, motifs de coquillages... La régularité mathématique de l’organisation des formes dans la nature passionne Alan Turing depuis son enfance. La **morphogenèse**, étude des mécanismes de croissance des formes biologiques à partir de la fécondation, devient même une obsession durant les quatre dernières années de sa vie. À l’époque, Watson et Crick[^25] sont sur le point de percer la structure de l’ADN. Mais Turing avait déjà perçu que la génétique ne suffirait pas pour tout décrire. Celle-ci fonctionne selon un code fixe, or il n’existe pas de déterminisme en biologie : les aléas imprévisibles de l’évolution des espèces en témoignent. Dans son article de 1952[^26], Turing est le premier à utiliser les équations différentielles, très complexes, de réaction-diffusion, dans le cadre de la morphogenèse. Sa démonstration est capitale dans l’histoire de la biologie du développement.

        <figure markdown>
            ![motifs illustrant la morphogenèse](Image/Partie_3/morphogenèse.png){ width="500" }
            <figcaption>
                [La morphogenèse sur le pelage d'un léopard](https://media4.obspm.fr/public/M2R/appliquettes/Turing/Images/guepardBIG.png)
            </figcaption>
        </figure>

[^19]: [L'Enigma japonais](https://fr.wikipedia.org/wiki/Code_97)
[^20]:[L'autre Enigma](https://fr.wikipedia.org/wiki/Machine_de_Lorenz)
[^21]: [ Une opréation qui aura changé l'histoire...](https://fr.wikipedia.org/wiki/D%C3%A9barquement_de_Normandie)
[^22]: [La crypptographie 2.0](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique)
[^23]:[Un système de chiffrement révolutionnaire](https://fr.wikipedia.org/wiki/Chiffrement_RSA)
[^24]:[Un ordinateur qui effectue des calculs à l'échelle....atomique](https://www.futura-sciences.com/sciences/definitions/physique-ordinateur-quantique-4348/)
[^25]: [Des scientifiques qui ont marqué l'histoire](https://www.futura-sciences.com/sante/photos/medecine-grands-noms-medecine-1456/medecine-james-watson-francis-crick-origine-structure-adn-10656/)
[^26]:[Pour les plus scientifiques](https://www.dna.caltech.edu/courses/cs191/paperscs191/turing.pdf)