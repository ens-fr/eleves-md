## La machine de Turing
!!! tip "Les débuts de l'algorithmique et du développement informatique 💡"
    
    Le 28 mai 1936, Alan Turing propose dans un article intitulé _On Computable Numbers, with an Application to the Entscheidungsproblem_[^6] un concept qui permet à une machine d'interpréter un code et donc d'effectuer des calculs de type différents. Ce qu'on appelle la "machine de Turing" n'est alors qu'un modèle théorique précurseur de l'informatique et non une réalisation complète. Cette application visionnaire sera la base du développement informatique et du fonctionnement par algorithme. Le premier ordinateur programmable ne sera construit qu'au cours de la Seconde Guerre mondiale et nommé Colossus. 
    
    ??? bug "Colossus 📟"
    
        Les principes de conception du Colossus, et son existence même, furent déclarés secret militaire, et  il fallut attendre 1975 pour que ce secret soit partiellement levé. À la fin de la guerre, huit des dix exemplaires du Colossus furent détruits ; les deux autres le furent en 1960. Grâce à des plans illégalement conservés, un exemplaire du Colossus a pu être reconstitué à l’initiative de Tony Sale et a fonctionné pour la première fois en 1996, en présence de Tommy Flowers. Il est visible au musée historique installé à _Bletchley Park_.
        <figure markdown>
        ![colossus](Image/Partie_2/colossus.jpg){ width="300" }
        <figcaption>[Colossus](https://commons.wikimedia.org/wiki/File:Colossus.jpg?uselang=fr)</figcaption>
        </figure>
        

    
    Mais à l'origine,le concept de machine de Turing, inventé avant l'ordinateur, était censé représenter une personne virtuelle exécutant une procédure bien définie, en changeant le contenu des cases d'un ruban infini, en choisissant ce contenu parmi un ensemble fini de symboles. D'autre part, à chaque étape de la procédure, la personne doit se placer dans un état particulier parmi un ensemble fini d'états. La procédure est formulée en termes d'étapes élémentaires du type : « si vous êtes dans l'état 42 et que le symbole contenu sur la case que vous regardez est « 0 », alors remplacer ce symbole par un « 1 », passer dans l'état 17, et regarder maintenant la case adjacente à droite ».
    <figure markdown>
    ![Article_turing](Image/Partie_2/computer.png){ width="300" }
    <figcaption>[Son article](https:/howtospeakmachine.com/2019/02/24/turing-machine-original-paper/)</figcaption>
    </figure>
    
    






## La résolution d'Enigma, cette machine <del>in</del>décryptable
!!! warning "La question de l'espionnage 🕵️"
    
    Dès l’Antiquité, généraux et souverains ont rivalisé d’ingéniosité pour imaginer des techniques d’espionnage toujours plus créatives, mais aussi des procédés sophistiqués pour protéger le secret de leurs communications. Les mathématiciens ont alors régulièrement été mis à contribution au sein des fameux _cabinets noirs_[^7] pour décrypter les messages chiffrés ennemis, œuvrant à l’apparition d’une science nouvelle, la **cryptanalyse**[^8]. Il n’est donc pas surprenant que, dès le début de la seconde guerre mondiale, les services de renseignement britanniques[^9] aient fait appel au jeune Alan Turing, brillant mathématicien, pour travailler au décryptage des messages allemands.
    <figure markdown>
        ![cabinet_noir(espionnage)](Image/Partie_2/cabinet noir.jpg){ width="300" }
        <figcaption>[Exemple de "cabinet noir"](https://cdn.radiofrance.fr/s3/cruiser-production/2017/03/53d7c0c7-4487-4b4d-b88b-e5cfd81f86ea/560x315_054_cor10134b.webp)</figcaption>
        </figure>
        


### Un message, plusieurs alphabets...
!!! info inline end "Soldats allemendands faisant usage d'Enigma 👨‍💻"

    <center>![enigma_machine](Image/Partie_2/soldats-enigma.jpg){width="200"}</center>
    
    Sur [cette photographie](https://flyingheritage.org/Explore/The-Collection/Germany/Enigma-Machine.aspx) datant de la seconde guerre mondiale, des soldats allemands utilisent une machine Enigma qui permettait d’envoyer des messages secrets. En réalité leurs messages étaient interceptés et décryptés par les alliés.


!!! danger "Un atout technologique révoltionnaire: Enigma 💣"
    Depuis les années vingt, l’armée allemande s’est équipée d’une machine à chiffrer révolutionnaire, la machine Enigma.
   
    Mais c'est quoi au final Enigma ? Inventée en 1919 et destinée, à l’origine, à une utilisation commerciale, la machine Enigma est utilisée par l’armée allemande durant la seconde guerre mondiale pour chiffrer les messages. Mais dès les années trente, les Polonais puis les Anglais se dotent de spécialistes capables de décrypter les messages secrets ennemis. Cette guerre du chiffre entraîna des perfectionnements de la machine. Ainsi une version plus complexe causa des problèmes aux services secrets britanniques qui, pendant plusieurs mois, eurent des difficultés pour déchiffrer les messages codés.

    <figure markdown>
    ![enigma_musée](Image/Partie_2/enigma1.jpg){width="300"}
    <figcaption>[Enigma](https://cdn.radiofrance.fr/s3/cruiser-production/2019/10/55e19765-cb96-4275-8e52-21bcdbc62f91/560x315_gettyimages-469050860.webp) </figcaption>
    </figure>
    

### Le chiffrement, la clé de la réussite d'Enigma !

??? faq "Le chiffrement... ?💽"
    En cybersécurité, le chiffrement désigne la conversion des données depuis un format lisible dans un format codé. Les données chiffrées ne peuvent être lues ou traitées qu’après leur déchiffrement.

    Le chiffrement est l’élément fondamental de la sécurité des données. C’est le moyen le plus simple et le plus efficace de s’assurer que les informations du système informatique ne peuvent être ni volées ni lues par quelqu’un qui souhaite les utiliser à des fins malveillantes.

    L'une des premières techniques de chiffrement a été  utilisée dans les textes religieux par les Hébreux qui connaissaint plusieurs procédés. Le plus connu appelé _Atbash_[^10] est une méthode de substitution alphabétique inversée.

    <figure markdown>
    ![méthode_chiffrement](Image/Partie_2/atbash.png){ width="500" }
    <figcaption>[La méthode "Atbash"](https://www.geocaching.com/geocache/GC866TG_mptm-14-ilettrisme?guid=108d24ec-6aab-4d03-b8d6-6a5f43681fe1)</figcaption>
    </figure>

!!! tip "Des méthodes infaillibles..🔒"
    
    Jusqu’à cette époque, la plupart des méthodes de chiffrement étaient des variantes de la **substitution alphabétique**. Une substitution alphabétique consiste à remplacer chaque lettre de l’alphabet par une autre, par exemple:

    - chaque « a » devient « k », 
    - chaque « b » devient « z », 
    - chaque « c » devient « u »… 
    
    Le nouvel alphabet utilisé (dans notre exemple « kzu… ») est le secret partagé par l’expéditeur et le destinataire. Cependant, depuis le IXe siècle, on sait décrypter un tel système en calculant la fréquence d’apparition dans le message chiffré de chaque lettre (puis de chaque groupe de deux ou trois lettres) et en la comparant à la fréquence des lettres dans la langue d’origine. Ainsi, pour un message initial en français, la lettre apparaissant le plus souvent dans le message chiffré correspond très certainement au « e ».
    
    <figure markdown>
    ![Substitution alphabétique](Image/Partie_2/sub.png){ width="300" }
    <figcaption>[Substitution alphabétique](https://commons.wikimedia.org/wiki/File:Caesar3.svg) </figcaption>
    </figure>
    
    
    


!!! bug "...enfin presque !🔓"
    Pour parer cette attaque, on utilisait des substitutions polyalphabétiques : les lettres du message étaient chiffrées avec des alphabets différents. Mais pour que le procédé reste utilisable, le nombre d’alphabets restait limité. Par exemple, on utilisait quatre alphabets secrets, le premier servait à chiffrer les lettres en positions 1, 5, 9… le deuxième les lettres en positions 2, 6, 10… L’attaque par analyse de fréquence ne s’applique alors plus directement. Toutefois, il suffit de regrouper les lettres du message chiffré obtenues avec le même alphabet et d’analyser les fréquences indépendamment dans chacun de ces groupes. L’attaque est simplement un peu plus difficile, car la longueur de texte disponible pour l’analyse est divisée par le nombre d’alphabets, mais ce dernier reste relativement petit. C’est ainsi que la plupart des chiffrements conçus jusqu’à la fin de la première guerre mondiale ont pu être attaqués.

### L'électromagnétique au service du secret

L’innovation majeure d’Enigma est d’utiliser un procédé électromécanique[^11] pour engendrer un nombre considérable d’alphabets; le nombre de lettres chiffrées avec le même alphabet devient alors tellement petit que les fréquences d’apparition ne sont plus du tout significatives.


!!! danger  "Arthur Scherbius, le créateur de cette machine "infernale" !⚡"
    
    À première vue, Enigma, avec son clavier, ressemble à une machine à écrire portative. Mais il s’agit d’une machine à chiffrer, c’est-à-dire d’une machine à coder des messages, conçue en 1919 par Arthur Scherbius[^12]. Différentes variantes ont été utilisées par l’armée allemande pendant la seconde guerre mondiale.
    
    <figure markdown>
    ![Arthur Scherbius](Image/Partie_2/arthur.jpg){ width="200" }
    <figcaption>[Arthur Scherbius](https://commons.wikimedia.org/wiki/File:Arthur_Scherbius_1.jpg) </figcaption>
    </figure>
    


=== "Machine à rotor unique"
    !!! info "Machine à rotor unique🔧"
        Lorsqu’on tape une lettre sur le clavier, c’est une autre lettre qui s’éclaire sur le tableau lumineux. Entre les deux, il y a tout un mécanisme, composé de rotors[^13] et de câbles électriques. Chaque fois qu’une lettre est chiffrée, cela entraîne la rotation d’un des rotors. La même lettre sera donc chiffrée différemment la prochaine fois. Ce système est rendu plus complexe encore par l’ajout d’un tableau de connexions[^14]. Pour une machine à trois rotors, il y a ainsi plusieurs centaines de milliards de milliards de possibilités🤕.

        Enigma se présente comme une machine à écrire, avec un clavier sur lequel on tape successivement les lettres du message de départ, non chiffré, et un tableau lumineux où s’allument les lettres chiffrées correspondantes. Il suffit de recopier les lettres éclairées pour obtenir le message chiffré.

        Les trois figures présentées ci-dessous représentent la machine en réduisant le nombre des 26 lettres de l’alphabet à 6 pour simplifier la lecture des schémas.

        L’alphabet de substitution est défini par le câblage électrique d’un rotor. Ainsi, sur la machine à 6 lettres de la figure ci-dessous:

        - « a » est transformé en « e » 
        - « b » en « d »
        - « c » en « f »
        - ...
        <figure markdown>
        ![machine avec un rotor](Image/Partie_2/1rotor.jpg){width="100}
        <figcaption> [Machine à rotor unique](https://interstices.info/turing-a-lassaut-denigma/)</figcaption>
        </figure>

   
    

=== "Machine à trois rotors"
    
    !!! info "Machine à trois rotors 🔨"
   
        Après avoir chiffré la première lettre, **l’alphabet de substitution est modifié** en faisant tourner le rotor d’une position. Dans le nouvel alphabet, « b » est maintenant transformé en « e ». En faisant tourner le rotor après le chiffrement de chaque lettre, Enigma ne revient au premier alphabet qu’après 26 lettres.

        Comme ce nombre d’alphabets est encore trop faible pour éviter les attaques par analyse de fréquence, Enigma utilise trois rotors en parallèle : à chaque fois que le premier rotor a fait un tour complet, on avance le second rotor d’une position. Le troisième rotor, lui, ne bouge que quand le deuxième a aussi fait un tour complet. Par ce procédé, on utilise donc $26^3 = 17 576$ alphabets différents comme :
    
        - l'alphabet `kzu`
        - l'alphabet `bkf`
        - l'alphabet `zru`
        - l'alphabet `yzt`
        - ...
        <figure markdown>
        ![machine avec trois rotors](Image/Partie_2/3rotors.jpg){ width="500" }
        <figcaption>[Machine à trois rotors](https://interstices.info/turing-a-lassaut-denigma/) </figcaption>
        </figure>
     
=== "Machine à trois rotors avec réflecteur et tableau de connexions" 
    
    !!! info  "Machine à trois rotors avec réflecteur et tableau de connexions🛠️"

        Pour utiliser une telle machine à chiffrer, une des difficultés pratiques vient du fait qu’il est indispensable de disposer de **deux machines différentes** : l’une pour chiffrer, l’autre qui correspond à la transformation inverse, pour déchiffrer. Pour pallier cet inconvénient, son concepteur, Arthur Scherbius, eut l’idée d’ajouter un réflecteur[^15], similaire à un rotor fixe. Son rôle est de transformer la lettre obtenue après passage dans les trois rotors puis de faire subir au résultat la même transformation dans le sens inverse. Le résultat du chiffrement d’une lettre correspond ainsi à un circuit entre une lettre du clavier et une lettre du panneau lumineux.
        <figure markdown>
        <center>![machine avec tableau de connexions](Image/Partie_2/rotors-tableau.jpg){ width="500" }</center>
        <figcaption> [Machine à trois rotors avec réflecteur et tableau de connexions](https://interstices.info/turing-a-lassaut-denigma/)</figcaption>
        </figure>

??? tip "Pour les plus déterminés 😎"
    === "Une machine virtuelle"
        <center>[Une machine "Enigma" virtuelle](https://cryptii.com/pipes/enigma-machine){ .md-button }</center>
    
    === "Enigma...avec Python"
        ```python
        
        --8<-- "docs/1/S/Enigma.py"
        
        ```



### Une machine à chiffrer et à déchiffrer

!!! done "Le hasard et la logique: deux clés importantes de cette résolution 🎲"
    Comme c’est ce même circuit qui est utilisé pour déchiffrer, on voit que si la lettre « b » est chiffrée en « e », alors pour une position identique des rotors, la lettre « e » sera chiffrée en « b ». Grâce à cette astuce, une seule machine suffit à chiffrer et à déchiffrer, car il s’agit exactement de la même transformation. Les rotors doivent avoir la même position de départ pour le chiffrement et pour le déchiffrement ; cette position fait partie du secret partagé par l’expéditeur et le destinataire.

    Avec l’étrange conception d’un hasard visiblement bien ordonné, l’armée allemande poussa cependant le raffinement[^16] jusqu’à choisir un réflecteur qui ne transformait aucune lettre en elle-même, assurant ainsi qu’une lettre ne pouvait jamais être son propre chiffré. Loin d’être une garantie de sécurité supplémentaire, cette propriété fut amplement exploitée par Alan Turing dans sa cryptanalyse !

!!! warning inline "Des possibilités de combinaisons à en perdre la tête !🤯 "

    Si l’on suppose que le câblage des rotors est connu de l’attaquant (ce qui fut le cas, car les plans de la machine furent communiqués aux Français par des espions dès le début des années trente), la sécurité d’Enigma repose sur un triple secret. La position de départ des trois rotors offre $26^3$, soit $17 576$ possibilités, le choix de ces trois rotors parmi cinq rotors différents multiplie ce nombre de clés possibles par $10$, enfin l’ordre des rotors le multiplie par factorielle de $3$, soit $3 ! = 1 × 2 × 3 = 6$. Le nombre de clés possibles était donc de $263 × 10 × 6$, ce qui fait plus d’un million de clés à essayer et qui n'est pas une mince à faire, même avec les machine que nous possédons de nos jours.

!!! example "Voici quelques clés cryptés de  de la machine "Enigma"....🗝️"
    | Nom de la clé      | Date d'identification                 | Date de cassage de la clé |Détails de la clé|
    | :-------------------:| :-------------------------------------: |:---------------------------:|:-----------------:|
    | `RED`       | septembre 1939 (jusqu'à la fin de la guerre) |6 janvier 1940             |Clé d'usage général.| 
    | `ONINON`       |mars 1941 (jusqu'à juillet 1941)   |8 mai 1941   |  Pour les communications liées aux faisceaux de navigation. Liée à Brown. | 
    | `LEEK`    | juin 1941 (jusqu'à la fin de la guerre) |   	31 juillet 1941   |  Une clé météo.               | 
     `SPARROW`    |   mars 1943 (jusqu'à mai 1943) |      6 avril 1943                     | Clé de son organisation d'interception radio en Méditerranée.                |

!!! danger "... et ce n'est rien comparé à leurs véritables nombres !⚠️"
        
    Pour augmenter encore ce nombre, Arthur Scherbius avait ajouté un tableau de connexions dont le rôle est de transposer 10 paires de lettres immédiatement en sortie du clavier et avant affichage sur le tableau lumineux. Le choix de ces dix couples faisait partie de la clé secrète, multipliant ainsi le nombre de possibilités par 1014, ce qui conduit à un nombre total de presque 1020 clés, c’est-à-dire des centaines de milliards de milliards. Les passer en revue de nos jours prendrait encore plusieurs mois, même avec un très grand nombre de nos ordinateurs.     Des cryptanalystes polonais eurent néanmoins le temps de transmettre leurs travaux aux services de renseignement britanniques, qui lancèrent dans le plus grand secret l’opération dite _Ultra_[^17] pour décrypter les messages d’Enigma. Ces travaux étaient menés par une équipe de 7000 personnes au sein du manoir de _Bletchey Park_, où Alan Turing se distingua.






[^6]: [Si vous êtes intéressé(e) par sa lecture...](https://www.cs.virginia.edu/~robins/Turing_Paper_1936.pdf)
[^7]: [Pour en savoir plus sur ces cabinets _spéciaux_...](https://fr.wikipedia.org/wiki/Cabinet_noir)
[^8]: [Une _autre_ technique spéciale](https://fr.wikipedia.org/wiki/Cryptanalyse)
[^9]:[Le _Special Operations Executive_](https://fr.wikipedia.org/wiki/Special_Operations_Executive)
[^10]: [L'une des premières méthodes de substitution alphabétique](https://fr.wikipedia.org/wiki/Atbash)
[^11]: [Pour en savoir plus sur ce procédé...](https://fr.wikipedia.org/wiki/%C3%89lectrom%C3%A9canique)
[^12]: [Un génie du côté des "méchants"](https://fr.wikipedia.org/wiki/Arthur_Scherbius)
[^13]: [Le rotor, clé de la résolution d'Enigma](https://fr.wikipedia.org/wiki/Rotor_(%C3%A9lectrotechnique))
[^14]: [L'ancêtre des programmes actuels](https://fr.wikipedia.org/wiki/Tableau_de_connexion)
[^15]: [Un composant portant bien son nom](https://fr.wikipedia.org/wiki/R%C3%A9flecteur_(cryptologie))
[^16]: [De la conception à l'implémentation pour un résultat frolant la perfection](https://fr.wikipedia.org/wiki/Raffinement#:~:text=En%20informatique%2C%20le%20raffinement%20consiste,mat%C3%A9rialis%C3%A9e%20par%20du%20pseudo%2Dcode.)
[^17]: [Une opération _ultra_ secrète](https://fr.wikipedia.org/wiki/Ultra_(nom_de_code))
[^18]: [Un instrument électromécanique qui aura fait des dégats](https://fr.wikipedia.org/wiki/Bombe_(%C3%A9lectrom%C3%A9canique))
