﻿
# _MarkDown_

!!! info "Source"
	 https://fr.wikipedia.org/wiki/Markdown

## En détails

!!! tldr "En résumé"

	:point_right: **Markdown** est un [langage de balisage léger](https://fr.wikipedia.org/wiki/Langage_de_balisage_l%C3%A9ger) créé en 2004 par [John Gruber](https://fr.wikipedia.org/wiki/John_Gruber) avec l'aide d'[Aaron Swartz](https://fr.wikipedia.org/wiki/Aaron_Swartz)[^ip]. Il a été créé dans le but d'offrir une [syntaxe](https://fr.wikipedia.org/wiki/Syntaxe) facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner l’impression d'avoir été balisé ou formaté par des instructions particulières.

  

	[^ip]: https://fr.wikipedia.org/wiki/Markdown#cite_note-1

  

	Un document balisé par Markdown peut être converti en [HTML](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language), en [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format) ou en d'autres formats. Bien que la syntaxe Markdown ait été influencée par plusieurs filtres de conversion de texte existants vers HTML — dont [Setext](https://fr.wikipedia.org/wiki/Setext), atx, [Textile](https://fr.wikipedia.org/wiki/Textile_(langage)), [reStructuredText](https://fr.wikipedia.org/wiki/ReStructuredText), Grutatext et EtText, la source d’inspiration principale du Markdown est le format du [courrier électronique](https://fr.wikipedia.org/wiki/Courrier_%C3%A9lectronique) en mode texte.

  

### Évolutions


!!! note "Notes"

	:exclamation: Depuis sa création originelle par John Gruber, Markdown n'a pas connu d'évolution notable de la part de ses auteurs. De plus, ce format n'a jamais été formellement standardisé.

  

	Un certain nombre de variantes ont donc été développées par d'autres, afin de pallier ce qui a été perçu comme des limitations inhérentes à une syntaxe très simplifiée.

  

!!! example "Exemple" 

	À titre d'exemples, on pourra citer [MultiMarkdown](https://fr.wikipedia.org/wiki/MultiMarkdown) et [GitHub Flavored Markdown](https://fr.wikipedia.org/wiki/GitHub_Flavored_Markdown). Ce dernier est utilisé pour les articles et la documentation sur [GitHub](https://fr.wikipedia.org/wiki/GitHub), mais a également été largement adopté sur plusieurs [éditeurs de texte](https://fr.wikipedia.org/wiki/%C3%89diteur_de_texte) supportant le format Markdown au niveau de la [coloration syntaxique](https://fr.wikipedia.org/wiki/Coloration_syntaxique) ou de la prévisualisation.

  

	Il existe également des greffons pour de nombreux logiciels, tels que « Markdown Here » pour [Firefox](https://fr.wikipedia.org/wiki/Firefox) et [Chrome](https://fr.wikipedia.org/wiki/Chrome_(logiciel)). Le système de gestion de contenu [WordPress](https://fr.wikipedia.org/wiki/WordPress) intègre nativement quelques éléments de ce langage depuis la version 4.3.

  

En mars 2016, deux [RFC](https://fr.wikipedia.org/wiki/Request_for_comments) ont été publiées dans un but de standardisation :

  

- RFC 776310, qui introduit le type [MIME](https://fr.wikipedia.org/wiki/Type_de_m%C3%A9dias) text/markdown à partir de la variante originale de Markdown.

- RFC 776411, qui répertorie des variantes MultiMarkdown, [GitHub Flavored Markdown (GFM)](https://fr.wikipedia.org/wiki/GitHub_Flavored_Markdown), [Pandoc](https://fr.wikipedia.org/wiki/Pandoc), CommonMark, Markdown Extra et autres.

  

L'initiative CommonMark, débutée en 2012, vise à pallier le manque de standardisation et les ambiguïtés du format en créant une spécification fortement définie du langage. Elle est désormais utilisée par, entre autres, Discourse, GitLab, Reddit, Qt, Stack Overflow et Stack Exchange.

  

### Mises en œuvre



  

:star2: Plusieurs [mises en œuvre](https://fr.wikipedia.org/wiki/Mise_en_%C5%93uvre) existent et ce dans différents [langages de programmation](https://fr.wikipedia.org/wiki/Langages_de_programmation) : en [Perl](https://fr.wikipedia.org/wiki/Perl_(langage)), en [PHP](https://fr.wikipedia.org/wiki/PHP), en [Ruby](https://fr.wikipedia.org/wiki/Ruby), en [Python](https://fr.wikipedia.org/wiki/Python_(langage)), en [Java](https://fr.wikipedia.org/wiki/Java_(langage)), en [C#](https://fr.wikipedia.org/wiki/C_sharp), en [Haskell](https://fr.wikipedia.org/wiki/Haskell), en [Gambas](https://fr.wikipedia.org/wiki/Gambas_(langage)), en [R](https://fr.wikipedia.org/wiki/R_(langage)) et même en [JavaScript](https://fr.wikipedia.org/wiki/JavaScript), notamment avec strapdown.js. Depuis la version 2.0 de [Swift](https://fr.wikipedia.org/wiki/Swift_(langage_d%27Apple)), il est aussi possible d'utiliser le Markdown dans ses playgrounds.

  

### Usages



  

:fire: Le langage de balisage léger Markdown est utilisé dans de nombreuses applications, que ce soit dans des logiciels/éditeurs de code, des logiciels d'écriture, des plateformes de code, ou encore dans des chaînes d'édition avec l'addition d'autres logiciels. Les domaines concernés peuvent être le développement informatique (Markdown est habituellement utilisé pour formater [le fichier README](https://fr.wikipedia.org/wiki/Readme) décrivant le code source d'un programme), la rédaction de documentation technique ou encore la publication académique.

  

C’est aussi le langage de balisage choisi par de nombreuses plateformes d’écriture collaborative, comme Authorea ou Manubot. Il existe plusieurs logiciels permettant de rédiger le Markdown en temps réel sur le web, comme CodiMD, Etherpad ou Firepad.

  

### Quelques exemples



:muscle: Voici quelques exemples de syntaxe Markdown. Les balises HTML équivalentes sont données. Cette liste n'est pas exhaustive.

  

### Formatage



  

:dizzy: En Markdown, l'[emphase](https://fr.wikipedia.org/wiki/Emphase_(typographie)) se manifeste indifféremment par les caractères `_` et `*` utilisés en paire, ceux-ci permettant une meilleure lisibilité du code s'ils sont utilisés correctement. Il y a trois niveaux d'emphase.

  

Pour mettre du texte en emphase (balise HTML ```<em>```), ce qui produit une mise en italique dans un navigateur courant :

  


`*quelques mots*`


  

ou :

  
  
`_quelques mots_`


  

Pour mettre du texte en grande emphase (balise HTML ```<strong>```), ce qui produit une mise en gras dans un navigateur courant, on utilisera des paires doubles (`__` ou `**`) :

  

`**plus important**`

  

ou :

`_**tout aussi très important**_`

  

Pour mettre du code dans le texte (balise HTML ```<code>```) :

  

(note : le caractère ` est un accent grave (en anglais backtick), par défaut AltGr + 7 sur les claviers [AZERTY](https://fr.wikipedia.org/wiki/AZERTY) français)

  

``Mon texte `code` fin de mon texte``

  

Ou sur plusieurs lignes avec [coloration syntaxique](https://fr.wikipedia.org/wiki/Coloration_syntaxique) selon le langage mis en préfixe :

  

(note : le caractère ` est un accent grave (en anglais backtick), par défaut AltGr + 7 sur les claviers [AZERTY](https://fr.wikipedia.org/wiki/AZERTY) français)

  

!!! example "Exemple de code python qui n'interfère par avec le code Markdown"
	``` python

	for w in range (A, Z):
       print (w , end = " ")
	
	```

	

  

Pour un paragraphe de code, mettre quatre espaces devant :

  

`Première ligne de code` truc normal à côté

`Deuxième ligne`

  

Comme dans les courriels, il est possible de faire des citations :

  

``` > Ce texte apparaîtra dans un élément HTML <blockquote>. ```

  

Pour faire un nouveau paragraphe (balise HTML ```<p>```), sauter deux lignes, c'est à dire laisser une ligne vide entre les deux paragraphes. Sauter une seule ligne dans le texte d'origine n'aura aucun effet à l'affichage (l'affichage sera en continu).

  

```

Premier paragraphe

  

Deuxième paragraphe

```

Pour faire un simple retour à la ligne, mettre deux espaces en fin de ligne (balise HTML ```<br>```).

  

### Listes


:dash: Sauter une ligne avant le début de la liste.

  

Pour créer une liste non ordonnée (balise HTML ```<ul>```) :

```markdown

* Pommes

* Poires

* Sous élément avec au moins quatre espaces devant.

```

  

Et une liste ordonnée (balise HTML ```<ol>```) :

```markdown

1. mon premier

2. mon deuxième

```

  

Et une liste en mode case à cocher
!!! note "Ce qu'il faut entrer comme code pour la création d'une case cochée et non cochée"
	```markdown
	- [ ] Case non cochée

	- [x] Case cochée
	```



!!! done "Rendu"
	- [ ] Case non cochée

	- [x] Case cochée

  

### Titres



  

:blue_heart: Les titres sont créés avec un certain nombre de`#`(croisillons) avant le titre, qui correspondent au niveau de titre souhaité (le HTML propose 6 niveaux de titres de ```<h1>``` à ```<h6>```)

  



# un titre de premier niveau

#### un titre de quatrième niveau



  

Pour les deux premiers niveaux de titre (```<h1>``` et ```<h2>```), il est également possible de souligner le titre avec des = ou des - (leur nombre réel importe peu, mais il doit être supérieur à 2).

  




### Tableaux



:anger: Pour créer des tableaux (balises HTML ```<tr>``` et ```<th>```)

  


!!! exemple "Voici ce qu'il faut entrer comme code pour la création d'un tableau"
	```
	| Titre 1 | Titre 2 | Titre 3 |
	| :------------ | :-------------: | -------------: |
	| Colonne | Colonne | Colonne |
	| Alignée à | Alignée au | Alignée à |
	| Gauche | Centre | Droite |
	```
	
	
	

  

!!! done "Rendu"
    
	|  Titre 1 | Titre 2 | Titre 3 |
	| :------------ | :-------------: | -------------: |
	|  Colonne | Colonne | Colonne |
	|  Alignée à | Alignée au | Alignée à |
	|  Gauche | Centre | Droite |

  

### Liens



:sparkles: Pour créer des liens (balise HTML ```<a>```) :

  

`[texte du lien](url_du_lien "texte pour le titre, facultatif")`

(Note : il n'y a pas d'espace entre le crochet fermant et la parenthèse ouvrante. Il faut écrire ..lien](url.. et non ..lien] (url.. )

### Images

  

:sparkling_heart: Pour afficher une image (balise HTML ```<img>```) :

  

`![Texte alternatif](url_de_l'image "texte pour le titre, facultatif")`

  

!!! info "Note"

	il n'y a pas d'espace entre le crochet fermant et la parenthèse ouvrante. Il faut écrire ..lien](url.. et non ..lien] (url.. )

  

### Annexes
:alien:

...

### Articles connexes



:cherry_blossom: :herb: :maple_leaf:

  

- [Asciidoc](https://fr.wikipedia.org/wiki/Asciidoc)

- [LaTeX](https://fr.wikipedia.org/wiki/LaTeX)

- [ReStructuredText](https://fr.wikipedia.org/wiki/ReStructuredText)

- [Textile (langage)](https://fr.wikipedia.org/wiki/Textile_(langage))

- [Wikicréole](https://fr.wikipedia.org/wiki/Wikicr%C3%A9ole)

  

:rose: :tulip: :sunflower:

  

### Lien externes



:dragon_face: :purple_heart: :blossom:

  

[Site officiel](https://daringfireball.net/projects/markdown/){ .md-button .md-button--primary }

  

### Réferences


:volcano: :poop: :fish_cake:

...
  

### Bonus


:mushroom: :cactus: :shell:

  

!!! tip "Astuces"

	Raccourcis pour copier un élément:

  

	++ctrl+c++

  

	Raccourcis pour coller un élément:

  

	++ctrl+v++

  

	Raccourcis pour changer de page rapidement:

  

	++alt+tab++

  

	Raccourcis pour supprimer et copier un élément:

  

	++ctrl+x++

!!! info "On peut aussi écrire des maths comme la formule pour la dérivée d'un quotient"

	$\left( \dfrac{u}{v}\right)' =\dfrac{(u'\times v)x - (u \times v')x}{v^2}$

	Ou même la formule de l'équation d'une tangente à une courbe :

	$T : y = f(a) + f'(a) \times (x-a)$ 

	Ou bien même quelques formules de puissances:

	$\dfrac {a^n} {a^m} = a^{n-m}$ , avec a$\neq$ 0

	$\left (a^n\right)^m = a^{n\times m}$
			
