# Les dérivés du Markdown

!!! info "Les problèmes autour du HTML"
    Le HTML le concurrent direct du Markdown dans le codage de sites web, est très méprisé par la plupart des doyens du codage car de une : c'est moche, de deux : c'est compliqué a comprendre, et de trois : a quoi bon s'y intéresser car les débouchés dans le milieu du développement de sites web sont très limités et sous payés. 

    Toutes les raisons vous amènent donc a choisir le Markdown qui est simple, rapide à apprendre et joli. Biensur sous linux et firefox en navigation privée, car windows c'est MAL.

!!! done "Donc"
    En conclusion choississez ----> _MARKDOWN_ (et linux)
