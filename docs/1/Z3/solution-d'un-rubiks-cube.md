# Comment résoudre un rubiks cube ?

![Rubiks cube](https://ruwix.com/pics/solution/17.svg)

Il existe de nombreuses approches sur la façon de résoudre le [Rubik's cube](https://fr.wikipedia.org/wiki/Rubik%27s_Cube) vers un moteur de recherche.Rubik's Cube. Toutes ces méthodes ont différents  niveaux de difficultés, pour les speedcubers ou les débutants, même pour résoudre le cube les yeux bandés. Les gens sont généralement coincés à résoudre le cube après avoir terminé la première, après quoi ils ont besoin d'aide. Dans l'article suivant, je vais vous montrer le moyen le plus simple de résoudre le cube en utilisant la méthode débutante.

Si vous avez besoin d'une aide vidéo:
[Vidéo solution rubiks cube](https://www.youtube.com/watch?v=Leml4U4D1r8){ .md-button }

## les 5 methodes principale pour le résoudre sont:

1. méthode Fridrich[^ip]
2. méthode Roux[^al]
3. méthode ZZ[^el]
4. méthode Metha
5. methode LBL (layer by layer)

### Les methodes les plus commune sont la méthode fridrich et roux, aujourd'hui je vais vous appendre la methode LBL car c'est la plus facile pour débuté.

La méthode présentée ici divise le cube en couches et vous pouvez résoudre chaque couche en appliquant un algorithme donné sans gâcher les pièces déjà en place. Vous pouvez trouver une page séparée pour chacune des sept étapes si la description sur cette page nécessite des explications et des exemples supplémentaires.

![Rubiks cube](Screenshot 2022-05-16 at 11.20.59.png)

## La solution étape par étape:

![Rubiks cube](Screenshot 2022-05-16 at 11.49.10.png)

### 1. Croix blanche

- Commençons par la face blanche. Tout d'abord, nous devons faire une croix blanche en faisant attention à la couleur des pièces centrales latérales. Vous pouvez essayer de le faire sans lire les instructions.
Utilisez cette étape pour vous familiariser avec le puzzle et voir jusqu'où vous pouvez aller sans aide. Cette étape est relativement intuitive car il n'y a pas de pièces résolues à surveiller. Pratiquez-vous simplement et n'abandonnez pas facilement. Essayez de déplacer les bords blancs à leur place sans gâcher ceux déjà fixés.


| Résoudre la croix | méthode | Rendu |
|---------|-------------|---|
| ✅  méthodes correcte:| ici la croix est aligné avec les centres du cubes | ![Rubiks cube croix résolu](Screenshot 2022-05-16 at 16.10.27.png)|
| ❌  Méthodes incorrecte:   | ici la croix n'est pas alignés avec les centres du cubes donc il est impossible de résoudre le cube en partant de la   |![Rubiks cube croix résolu](Screenshot 2022-05-16 at 16.10.37.png)|

### 2. Coins blanc

Dans cette étape, nous devons arranger les pièces d'angle blanches pour terminer la première face. Si vous êtes très persistant et que vous avez réussi à faire la croix blanche sans aide, alors vous pouvez essayer de faire celle-ci aussi. Si vous n'avez pas de patience, je vous donnerai quelques indices.

Tournez le calque inférieur de sorte que l'un des coins blancs se trouve directement sous l'endroit où il est censé aller sur le calque supérieur. Maintenant, faites l'un des trois algorithmes en fonction de l'orientation de la pièce, alias dans quelle direction l'autocollant blanc est orienté. Si le coin blanc est à sa place, mais s'est trompé, vous devez d'abord le sortir.

### Voici un exemple visuel:

![Rubiks cube croix résolu](9ae20bc015d49770ff428a7e6fc722b7.jpg)


### 3. La deuxième couche:

Jusqu'à présent, la procédure était assez simple, mais à partir de maintenant, nous devons utiliser des algorithmes. Nous pouvons oublier le visage blanc terminé, alors retournons le cube pour nous concentrer sur le côté non résolu.

Dans cette étape, nous complétons les deux premières couches (F2L). Il y a deux algorithmes symétriques que nous devons utiliser dans cette étape. Ils sont appelés algorithmes de droite et de gauche. Ces algorithmes insèrent la pièce de bord avant de la couche supérieure à la couche médiane sans gâcher la face blanche résolue.
Si aucune des pièces du calque supérieur n'est déjà alignée comme dans les images ci-dessous, tournez le calque supérieur jusqu'à ce que l'une des pièces de bord du calque supérieur corresponde à l'une des images ci-dessous. Suivez ensuite l'algorithme de correspondance pour cette orientation.
![Rubiks cube croix résolu](615c3a1f4a73a0385c993886276352b1.jpg)

### 4. La croix jaune:

ommencez à résoudre le dernier calque en faisant une croix jaune sur le dessus du cube. Peu importe que les pièces ne soient pas à leur dernière place, nous n'avons donc pas à faire attention aux couleurs des côtés.

Nous pouvons obtenir trois modèles possibles sur le dessus. Utilisez cet algorithme pour passer d'un état à l'autre :
F R U R' U' F'

Lorsque vous voyez un point, vous devez appliquer l'algorithme trois fois. Si vous avez une forme jaune "L", alors seulement deux fois, en tenant le cube dans vos mains comme on le voit sur l'image ci-dessous.
Dans le cas d'une ligne horizontale, il vous suffit d'exécuter la permutation une seule fois.
pour resoudre la croix jaune il faut apprendre un nouveau mouvement noter: Ff
  - pour faire ce mouvement on tourne la face avant comme "F" et nous tournons aussi la face du mileu dans la meme direction noté "f",
  ce qui nous donne "Ff"

=== "Ligne jaune"
    ![ligne jaune](Screenshot 2022-05-23 at 15.05.45.png)

    ici il faut appliquer la formule : F R U R' U' F'

    

=== "L jaune"
    ![L jaune](Screenshot 2022-05-23 at 15.05.57.png)

    ici il faut appliquer une nouvelle formule commencant par le nouveau mouvement : Ff
    nous avons donc: Ff R U R' U' Ff'



=== "Point jaune" 
    ![Point jaune](Screenshot 2022-05-23 at 15.06.04.png)
    ici il faut appliquer la premiere formule et ensuite la deuxième donc:
    F R U R' U' F' Ff R U R' U' Ff'

### si vous avez suivis les étapes correctement, vous devriez voir ca:

![cube avec croix jaune résolu](12.svg)

## 5. Bords jaune:

Après avoir fait la croix jaune sur le dessus du cube, vous devez placer les pièces de bord jaune à leur dernière place pour correspondre aux couleurs des pièces centrales latérales.


=== "Bords jaune alignés"
    ![Croix jaune complète avec bords alignés](13.svg)

    Si vous vous retrouver dans cette situation, il n'a rien a faire vous pouvez passer a l'étape suivante

    

=== "Bords au mauvais endroit"
    ![Bords au mauvais endroit](14.svg)

    ici il faut appliquer une nouvelle formule: R U R' U R U2 R' U
    appliquer plusieurs fois jusqu'à ce que les bords sont résolu


## 6. Coins jaunes a leurs place:
    
Seuls les derniers coins de calque ne sont pas résolus. Tout d'abord, nous devons les amener au bon endroit, alors ne vous inquiétez pas de l'orientation de cette étape.

Trouvez une pièce qui est déjà au bon endroit, déplacez-la dans le coin supérieur droit, puis appliquez l'algorithme suivant pour changer (cycler) les trois mauvaises pièces marquées sur l'image.
utiliser l'alghorithme:
  - U R U' L' U R' U' L

Faites ceci deux fois pour faire une rotation inverse des pièces. Si aucun des coins jaunes n'est au bon endroit, exécutez l'algorithme une fois pour obtenir un bon morceau.

![cycle des coins](15.svg)

## 7. Orientations des coins jaunes:

Toutes les pièces sont à leur bon endroit, il vous suffit d'orienter les coins jaunes pour terminer le puzzle. Cela s'est avéré être l'étape la plus confuse, alors lisez les instructions et suivez attentivement les étapes.
avant de commencer cette étape vous devriez voir ceci:

![coins pas orientés](16.svg)


Tenez le cube dans votre main avec un coin non résolu dans le coin avant droit en haut, puis faites l'algorithme ci-dessous deux ou quatre fois jusqu'à ce que cette pièce spécifique soit bien orientée :

  - R' D' R D

Il semblera que vous ayez gâché tout le cube, mais ne vous inquiétez pas, tout ira bien lorsque toutes les pièces d'angle seront orientées.

Tournez le calque supérieur uniquement pour déplacer une autre pièce jaune non résolue vers le coin supérieur avant droit du cube et faites à nouveau le même R' D' R D jusqu'à ce que cette pièce spécifique soit correcte. Faites attention à ne pas déplacer les deux couches inférieures entre les algorithmes et ne faites jamais pivoter le cube entier !

Vous êtes si près de la fin, alors soyez prudent car c'est l'étape de ce tutoriel où la plupart des gens se perdent. Si cette description n'a pas de sens.

Répétez cette opération jusqu'à ce que votre Cube soit résolu :)


![Rubiks cube finis!!](17.svg)





























[^ip] [Comment résoudre avec la méthode fridrich](https://www.francocube.com/deadalnix/fridrich)

[^al] [Comment résoudre avec la méthode ROUX](https://www.speedcubingtips.eu/methode-de-resolution-roux/)

[^el] [Comment résoudre avec la méthode ZZ](https://www.speedcubingtips.eu/methode-zz/)
