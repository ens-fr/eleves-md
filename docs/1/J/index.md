# Ingénieur

---

## Qu'est ce qu'un ingénieur ?

!!! info "Qu'est ce qu'un ingénieur ?"
    Un **ingénieur** est un [professionnel](https://fr.wikipedia.org/wiki/Profession) traitant de problèmes complexes d'[ingénierie](https://fr.wikipedia.org/wiki/Ing%C3%A9nierie), notamment en [concevant des produits](https://fr.wikipedia.org/wiki/Conception_de_produit), des processus si nécessaire avec des moyens [novateurs](https://fr.wikipedia.org/wiki/Innovation), et [dirigeant](https://fr.wikipedia.org/wiki/Directeur_de_projet) la réalisation et la mise en œuvre de l'ensemble : produits, systèmes ou services. Il ou elle crée, conçoit, innove dans plusieurs domaines tout en prenant en compte les facteurs sociaux, environnementaux et économiques propres au [développement durable](https://fr.wikipedia.org/wiki/D%C3%A9veloppement_durable). Il lui faut pour cela, non seulement des [connaissances techniques](https://fr.wikipedia.org/wiki/Connaissance_technique), mais aussi économiques, sociales, [environnementales](https://fr.wikipedia.org/wiki/Environnement) et humaines reposant sur une solide [culture](https://fr.wikipedia.org/wiki/Culture) scientifique et [générale](https://fr.wikipedia.org/wiki/Culture_g%C3%A9n%C3%A9rale).

    Ce terme – popularisé par la fonction de [Vauban](https://fr.wikipedia.org/wiki/S%C3%A9bastien_Le_Prestre_de_Vauban) au service de [Louis XIV](https://fr.wikipedia.org/wiki/Louis_XIV) – revêt cependant des significations diverses selon les époques et les secteurs d'activité. Le terme « ingénieur » et les fonctions qui y sont reliées se sont en effet beaucoup élargies.

    La technicité, l'autonomie requise et les coûts importants associés à certains équipements modernes amènent parfois le remplacement de techniciens ou de professionnels qualifiés par des ingénieurs.

Si tu veux en savoir plus ➡️ [Clique ici ! ](https://fr.wikipedia.org/wiki/Ing%C3%A9nieur){ .md-button } ⬅️

### Les différents types d'ingénieurs

1. 👨‍💻L'ingénieur en cybersecurite

![i](https://www.esic.fr/wp-content/uploads/2021/02/cropped-certification-expert-cybersecurite-ioc.jpg)

!!! faq "En quoi consiste le métier d'ingénieur en cybersécurité ?"
     ??? done "Réponse"
        L’ingénieur cybersécurité est un expert de la sécurité informatique qui analyse les failles de sécurité d’un système informatique et propose des solutions afin de le sécuriser.

2. ✈️L'ingénieur en aéronautique

![i](https://cdn.futura-sciences.com/buildsv6/images/mediumoriginal/0/1/1/0119c45346_50148024_ingenieur-calcul-aeronautique2.jpg)

!!! faq "En quoi consiste le métier d'ingénieur en aéronautique ?"
    ??? done "Réponse"
        L'ingénieur en aéronautique conçoit, teste, fabrique, entretient et commercialise des avions et des hélicoptères (civils ou militaires), mais aussi des lanceurs spatiaux, des satellites et des missiles.


3. ⚙️L'ingénieur en système embarqué

![i](https://www.3il-ingenieurs.fr/content/uploads/2020/06/systemes-embarques-4.jpg)

!!! faq "En quoi consiste le métier d'ingénieur en système embarqué ?"
    ??? done "Réponse"
        L’ingénieur en système embarqué est un ingénieur en électronique qui a pour mission principale de créer des systèmes électroniques complexes. Ce professionnel détermine également l’architecture des composants et produits destinés aux applications.

## Des entreprises accueillant des ingénieurs
!!! example "Exemples d'entreprises"
    !!! note "Présentation de l'entreprise Thalès"
        - Thalès 
        ![i](https://france3-regions.francetvinfo.fr/image/BRA10_umM1k6rCBr8TwJ0G9mi50/1200x900/regions/2020/10/04/5f79de1d569c2_maxnewsworldthree686925-5027960.jpg)

        Thales est un leader mondial des hautes technologies comptant plus de 81 000 collaborateurs présents sur tous les continents. Le Groupe investit dans les innovations du numérique et de la « deep tech »[^ip] – big data, intelligence artificielle, connectivité, cybersécurité et quantique – pour construire un avenir de confiance, essentiel au développement de nos sociétés, en plaçant l’humain au cœur des décisions.
        Thales propose des solutions, services et produits qui aident ses clients – entreprises, organisations, Etats – dans cinq grands marchés vitaux pour le fonctionnement de nos sociétés : identité et sécurité numériques, défense, aéronautique, espace, et transport. 
    
    !!! note "Présentation de l'entreprise Airbus"
        - Airbus

        ![i](https://www.challenges.fr/assets/img/2016/10/06/cover-r4x3w1000-5a1d2dd0eb3c6-000-cj21z-1.jpg)

        La société innove constamment pour fournir des solutions efficaces et technologiquement avancées dans l'aérospatial, la défense et les services connectés. Dans le domaine de l'aviation commerciale[^ipp], Airbus propose des avions de ligne modernes et économes en carburant ainsi que des services associés. Airbus est également un leader européen dans le domaine de la défense et de la sécurité, ainsi qu'un acteur mondial de premier plan dans le spatial. Dans le domaine des hélicoptères civils et militaires, Airbus fournit les solutions et les services les plus performants au monde.

        Airbus s'appuie sur son fort héritage européen pour devenir véritablement international - avec environ 180 sites et 12 000 fournisseurs directs dans le monde. La société possède des chaînes d'assemblage final d'avions et d'hélicoptères en Asie, en Europe et dans les Amériques, et son carnet de commandes a plus que sextuplé depuis 2000

    !!! note "Présentation de l'entreprise Naval Group"
        - Naval Group

        ![i](https://agence-api.ouest-france.fr/uploads/article/4d5a352dce0d4225486b0ed4b37fce07f439f1ef.jpg?v=13)

        Naval Group conçoit, réalise, modernise et maintient en service les navires des marines, forme leurs équipages et assure la conception et l'exploitation de leurs infrastructures navales.

## Citations concerant le métier d'ingénieur 

!!! cite "Citation 1"
      "Il faut se méfier des ingénieurs, ça commence par la machine à coudre, ça finit par la bombe atomique."
!!! cite "Citation 2"
     "Aucun ingénieur ne regarde une télécommande sans se demander comment faire pour la transformer en un pistolet hypodermique."

## Quelques langages de programmation qu'utilisent certains ingénieurs

=== "Java"
    ``` java linenums="1"
    public class Test {

    //Crée une nouvelle instance de Test
    public Test() {
    }

    //  args: les arguments de la ligne de commande
    public static void main(String[] args) {
        System.out.println("Allo !");
    }
    ```

=== "C"
    ```C linenums="1"
    int main(){
        int z;
        for(i = 0; z < 10; i++){
            printf("coucou !/n");
        }
        return 0;
    }
    ```
=== "Python"
    ```python linenums="1"
    for i in range (7):
        print("mon 1er site")
    ```

## Tableau de salaire brut par an de quelques ingenieurs

| Ingénieur | Salaire brut par an | Classement |
|:---------:|:-------:|:----------:|
| En cybersécurite | 62 500€ | ![1er](https://thumbs.dreamstime.com/b/er-vecteur-de-r%C3%A9compense-de-troph%C3%A9e-premier-accomplissement-d-de-placement-95968124.jpg) |
| En aéronautique | 60 000€ | ![i](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMKuHOh1freFRosBkIZjmcDXEjrtZyn5tdVbIHK9fw9YoUYLUWbe7Bc2-PXjboTw9SpZw&usqp=CAU) |
| En système embarqué | 33 908€ | ![i](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSftm9R2i6R_1oKlmSi1BBxK3LQEJ8LpNLTBjTEshVVoJJXPaO_-Ziq-94Im_43MsVxz9s&usqp=CAU) |

## Comment devenir ingenieur ? 
!!! tip "5 conseils pour devenir un ingénieur brillant !"
    - Travailler dur. Bien que cela puisse sembler évident, et c’est probablement le cas, il est impératif de travailler fort dans vos études.
    - Étudiez efficacement. ...
    - Gérez votre temps. ...
    - Réservez du temps pour vous amuser. ...
    - Rencontrer de nouvelles personnes/contacts. ..

!!! bug "Le Parcours"
    Il existe des parcours très différents pour acquérir le statut d’ingénieur. Mais pour obtenir son diplôme, il faut **au minimum avoir réalisé 5 années d’étude (un cycle préparatoire de 2 ans et un cycle ingénieur de 3 ans).** Plusieurs voies reviennent souvent : entrée en école d’ingénieur à un niveau post-bac, une entrée après un BAC+1, après une classe prépa ou après un BAC+2 ou plus. Il faut noter que seuls les établissements habilités par la CTI (Commission des Titres d’Ingénieurs) sont en mesure de délivrer un diplôme d’ingénieur. Le passage par une école d’ingénieur est la voie privilégiée, en France, il en existe plus de 200.

!!! warning "Difficulté de devenir ingénieur"
    Réussir ses études d’ingénieur suppose tout d’abord un goût prononcé pour les sciences et les technologies. Mais un vrai ==changement s’opère entre la terminale et la 1ère année en classe préparatoire intégrée==, tant au niveau du rythme qu’au niveau de l’organisation des études, des méthodes de travail ou encore de l’évaluation des résultats.

Pour plus d'infos ➡️ [Clique ici ! ](https://www.onisep.fr/Choisir-mes-etudes/Apres-le-bac/Principaux-domaines-d-etudes/Les-ecoles-d-ingenieurs/Les-differentes-voies-d-acces-en-ecole-d-ingenieurs){ .md-button .md-button--primary } ⬅️

## Mini Questionnaire 

1°/ Combien d'années d'étude faut-il pour devenir ingénieur ?

- [ ] 3 années
- [ ] 6 années
- [x] 5 années

2°/ Quels sont les langages présents sur le site ?

- [x] Python
- [x] Java
- [ ] Javascript
- [x] C

3°/ Quel est l'ingénieur qui a le salaire le plus élevé parmi ceux présents sur le site ?

- [x] Cybersécurité
- [ ] Aéronautique
- [ ] Système Embarqué

[^ip] : le terme désigne toujours les startups qui proposent des produits ou des services sur la base d'innovations de rupture..

[^ipp] : Un vol assurant le transport public de passagers et/ou de fret et de courrier, contre rémunération ou en vertu d'un contrat de location
