# Animation en stop motion [^1]

## Le stop motion

!!! faq "Le stop motion c’est quoi ?"
    C’est une animation image par image ou animation en volume.  
    C’est une technique d’animation qui permet de créer un mouvement à partir d’objets immobiles. Vous pouvez donc l’utiliser dans tous types de projets : votre film institutionnel, votre film corporate ou bien encore votre film d’entreprise . Le but de cet article est de vous donner un maximum d’éléments sur cette technique.

    Le stop motion consiste donc à prendre des photos, ou des images et les assembler, les unes après les autres, jusqu’à les animer pour en faire un **film en mouvement** . Il permet donc d’animer des personnages à raison de 24 images par seconde.

## Un peu d’histoire

!!! info "L’histoire de l’animation en stop motion est très riche :"

    -   En 1888, Louis Le Prince a breveté la conception de la toute première caméra cinématographique.
    -   En 1889, Friese-Greene a breveté une caméra cinématographique appelée caméra chronophotographique. Cette dernière prenait 10 images par seconde à l’aide d’un film celluloïd perforé.
    -   En 1891, un employé de Thomas Edison nommé William Kennedy Laurie Dickson conçoit et construit la caméra cinétique. Dotée d’un moteur électrique, elle se trouve être plus fiable que ses prédécesseurs.
    -   En 1894 et nous trouvons le Lumiere Domitor qui a été créé par Charles Moisson pour les Lumiere Brothers.
    -   Le tout premier film d’animation en stop-motion est The Humpty Dumpty Circus de 1898, créé par les réalisateurs et producteurs J. Stuart Blackton et Albert E. Smith. Le film a donné vie à des jouets en bois pour représenter des acrobates et des animaux en mouvement.
    -   Blackton a continué à travailler avec cette technique et à la développer, en utilisant un mélange d’action en direct et de stop-motion dans The Enchanted Drawing (1900)

    Voici une vidéo qui présente l’évolution du stop-motion :

    <iframe src="https://player.vimeo.com/video/180025799?h=b975a12a20" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

    Vous l’aurez compris, l’histoire du stop motion retrace également les prémisses du cinéma. Si vous êtes intéressé par le sujet historique du stop-motion et du cinéma en général, nous vous conseillons le Musée Lumière à Lyon.

## Pourquoi utiliser le stop motion ?

!!! faq "Pourquoi l'utiliser ?"

    - ### Innover
        Instaurer une nouvelle tendance, un nouveau souffle à votre site , vos réseaux sociaux. Grâce au stop motion vous pouvez animer tout type d’objet, les possibilités sont infinies !

    - ### Expliquer

        Le stop motion peut être utilisé en tant que vidéo explicative . Avec un brin d’imagination et l’aide d’une société de production audiovisuelle pour le côté technique. Réalisez une vidéo d’explication avec cette technique, c’est faire preuve de polyvalence et de créativité . Un rendu unique !

    - ### Communiquer sur l’image de marque

        En créant des animations simples mais créatives, l’ image de votre marque restera dans la mémoire des gens. Cette technique peut donc vous aider à créer une image positive . L’animation en stop motion reste dans l’esprit et motive les partages. Les vidéos en stop motion sont d’ailleurs très populaires sur les réseaux sociaux.

    - ### Clips

        Si vous êtes un artiste et que vous souhaitez réaliser un clip, pensez au stop motion, il vous permettra de créer des clips musicaux **vraiment imaginatifs**.  
        
        Les vidéos en stop motion peuvent être longues à réaliser, c’est pourquoi nous vous conseillons de vous rapprocher d’une société de production audiovisuelle. L’avantage premier c’est le **caractère unique et créatif** . Ainsi, que vous souhaitiez sensibiliser, communiquer, innover, le stop motion peut être la réponse.  

## Matériel

!!! faq "Quel est le matériel requis pour faire du stop motion ?"

    Il est indispensable de disposer d’un matériel que l’on peut fixer, car il est primordial de faire des **plans immobiles**. Cela vaut autant pour le modèle que pour tous les autres éléments du décor : éclairage, environnement, objets, second plan, etc. Pour une séquence de moins d’une minute, il faut plus d’une centaine d’images fixes.  
    Voici une liste exhaustive du matériel que vous devez avoir à votre disposition :

    - Un fond uni, si vous souhaitez ajouter des effets, munissez-vous d’un fond vert
    - Un appareil photo de type reflex de préférence
    - Un trépied pour une stabilisation parfaite
    - Un éclairage pour pouvoir focaliser la lumière sur certains éléments du décor

## Méthode

!!! faq "Quelle est la méthode à adopter pour faire du stop motion ?"

    Il faut capturer une première image, repositionner le ou les personnages et répéter l’opération. Pour obtenir un **mouvement fluide** , le ou les personnages doivent être repositionnés à une **distance constante** d’une image à l’autre. Pensez bien que plus un personnage est déplacé d’un incrément à l’autre, plus il semble se déplacer rapidement lors  
    de la lecture.  
    Pour lire vos séquences d’images, importez-les dans l’application de votre choix. Plus d’informations sur les logiciels dans le paragraphe suivant.

## Logiciel

!!! faq "Quel logiciel utiliser ?"

    Voici une liste exhaustive de logiciels que vous pouvez utiliser pour votre montage vidéo.

    - Appartenant à la suite Adobe : Premiere Pro et After Effect. Disponibles sur Mac et Windows, le premier est un logiciel de montage, le second permet d’ajouter des effets spéciaux.
    - Avidemux est un logiciel de montage vidéo gratuit qui s’adresse principalement aux débutants. Les fonctionnalités sont assez basiques (découpe, filtrage, etc.) mais suffisantes pour faire ses premiers pas de montage.
    - iMovie, disponible uniquement sur Mac et iOS, c’est un logiciel de montage vidéo hyper intuitif. ll permet de monter, modifier et partager des vidéos.

    [^1]: Source : [https://www.bproduction.fr/tout-savoir-sur-le-stop-motion/](https://www.bproduction.fr/tout-savoir-sur-le-stop-motion/)
