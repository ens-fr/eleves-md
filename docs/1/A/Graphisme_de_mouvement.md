# Graphise de mouvement / motion design [^1]

![Définition du motion design](images/motion-design-definition.jpg)

## Introduction

!!! info "Le motion design"
    Les **vidéos animées** sont partout. À force de voir cette technique audiovisuelle si captivante et dynamique, la curiosité vous pique : de quoi s’agit-il exactement ? Quel est son intérêt ? Comment est-ce réalisé ?

## Définiton

!!! faq "Le motion design, c’est quoi ?"

    Les termes “motion design” sont une contraction de _motion graphic design_, littéralement _conception graphique animée,_ ou plus simplement _graphisme en mouvement_.

    Le motion design consiste donc à faire passer un message avec une représentation visuelle (typographie, illustrations à la main ou vectorielles, photographie, couleurs, etc.) mise en mouvement.

    En plus de la créativité ou de la fibre artistique du graphiste, le motion designer dispose de la maîtrise technique des logiciels numériques.

## Différentes formes

!!! info "Les différentes formes de motion design"

    Les frontières couvertes par la discipline sont aujourd’hui assez floues. Il est moins aisé de différencier le motion design des films d’animation, des dessins animés, des effets spéciaux cinématographiques et de certaines animations 3D.

    Ce qui est certain, c’est que le motion design s’utilise pour réaliser une vidéo animée à partir d’éléments graphiques. Dans un cadre corporate, elle est d’une durée généralement courte.

    Plusieurs techniques et styles graphiques peuvent être employés pour produire des résultats radicalement différents :

    - Le motion 2D permet à la fois de créer des films ludiques à partir d’animations vectorielles ou des vidéos plus complexes. Ce style est particulièrement judicieux pour animer une infographie.
    - L’animation vidéo 3D est appréciée pour son réalisme.
    - La post-production à partir de prises de vue réelles, de typographies, ou de dessins apporte un rendu original.

## Origine

!!! faq "D’où vient cette discipline ?"

    L’origine du motion design ne fait pas l’unanimité parmi les motion designers. Néanmoins, la plupart reconnaissent que le graphiste publicitaire Saul Bass en fut l’un des pionniers avec ses génériques de films représentant des objets graphiques en mouvement. On peut citer par exemple Sueurs froides[^2] ou Psychose[^3] d’Alfred Hitchcock. À l’époque, ces animations étaient réalisées par découpage sur du carton, ou avec de l’encre ou de la peinture, ou encore dessinées image par image, à la main, sur pellicule.

    Cette approche narrative et artistique des séquences d’ouverture a révolutionné un format autrefois purement légal et informatif. Elle a par ailleurs été reprise quelques années plus tard par Maurice Binder, connu internationalement pour ses génériques de James Bond[^4].

## Et maintenant ?

!!! info "Le motion design aujourd’hui"

    Le motion design s’est professionnalisé depuis moins de dix ans, lorsque les premiers studios de création ont commencé à fleurir.

    Plus qu’un outil narratif et artistique, il sert désormais aussi à distraire, éduquer ou améliorer l’expérience utilisateur d’une interface. Complété avec des langages informatiques tels que l’HTML, le CSS et le JavaScript, le motion sort aussi du cadre de la vidéo en offrant des fonctionnalités interactives. Les effets générés par un like Facebook sont par exemple réalisés à l’aide de motion design.

    Les vidéos d’animation et les effets visuels se retrouvant aujourd’hui aussi bien sur internet, qu’à la télévision et sur les écrans urbains, difficile de passer à côté ! On les rencontre dans les publicités, les clips musicaux, les génériques du cinéma, les panneaux publicitaires LED, les émissions de télévision, les films institutionnels pour les entreprises, les interfaces graphiques des pages web et des applications, l’art numérique, etc. Le festival nantais Motion Motion[^5], dont Mars Vidéos est partenaire, propose par ailleurs de découvrir le graphisme animé chaque année, à travers des œuvres, conférences et rencontres.

## Avantages et exemples

!!! note "Avantages"

    Le motion design étant l’un des styles audiovisuels les plus dynamiques, il ajoute une touche de modernité à chacune de vos productions et ainsi à votre image de marque.

    Le public l’adore pour son aspect captivant, décalé et fluide. Cela fait de lui un excellent boost de vos taux de conversion ! De plus, sa capacité à présenter une idée ou un service de manière visuelle en une minute ou moins le rend totalement mobile-friendly. Le motion design deviendra l’outil de communication audiovisuelle préféré de vos cibles sur tous supports, notamment sur mobile.

    C’est d’ailleurs le choix qu’a fait Habitat 44 pour souhaiter ses vœux à ses locataires. Ils présentent en moins d’une minute leurs offres de logement dans un format vertical adapté aux réseaux sociaux sur smartphone.

    <iframe src="https://player.vimeo.com/video/383086715?h=8ec34ce292&title=0&byline=0" width="640" height="800" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

    Par ailleurs, une vidéo en motion design possède l’incroyable atout de pouvoir illustrer tout ce qui ne se prête pas à être filmé avec une caméra, soit parce que :

    - la thématique est abstraite ou complexe ;
    - l’image manquerait d’esthétisme ;
    - les éléments sont difficiles d’accès ;
    - ou tout simplement n’existent pas !

Voyons plus en détail comment cela se traduit avec différents exemples de vidéos motion design.

!!! info "Synthétiser un message complexe"

    Le motion design bénéficie d’un rendu fluide et dynamique grâce à des transitions nombreuses et visuellement optimisées ainsi qu’un rythme en accord avec la musique et les bruitages. Ajoutons à cela les explications d’une éventuelle voix-off, les vidéos animées deviennent parfaites pour vulgariser et synthétiser un sujet complexe tel que :

    - un long discours ;
    - des statistiques chiffrées ;
    - une procédure comportant de nombreuses étapes ;

    Cela fait de ce genre audiovisuel un excellent support pédagogique. La majorité des individus ayant une mémoire visuelle, les dessins et vecteurs animés facilitent l’apprentissage.

    Energy Formation a par exemple fait appel à notre agence pour synthétiser de manière visuelle leurs consignes de sécurité aux nouveaux apprenants :

    <iframe src="https://player.vimeo.com/video/482691805?h=5e524a3fe3&title=0&byline=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

!!! info "Illustrer un sujet sensible ou conceptuel"

    Les vidéos animées permettent de dédramatiser un contexte, traiter une problématique sensible (en santé par exemple) grâce à la neutralité des illustrations. Elles peuvent rendre un sujet, en apparence sérieux ou peu reluisant, ludique et captivant. L’idée étant d’accrocher votre audience dès les premières secondes grâce au rythme et aux effets visuels.

    Nous avons notamment produit pour plusieurs associations organisant le parrainage de proximité en Loire-Atlantique une vidéo en motion design pour moderniser l’image de cette action :

    <iframe src="https://player.vimeo.com/video/306995210?h=337c352e05&title=0&byline=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
    
    C’est par ailleurs un choix de style audiovisuel judicieux si votre objectif est de sensibiliser un jeune public.

    D’autre part, le motion peut donner vie à un sujet difficilement représentable matériellement : technologies numériques et virtuelles, domaines administratif et juridique, etc.

!!! info "Exprimer sa créativité sans limite"

    Le graphisme en mouvement vous autorise à créer un univers sur-mesure, 100% à votre image, sans contrainte physique ni géographique. En effet, nul besoin d’acteur ni de lieu de tournage !

    Alors, c’est l’occasion où jamais de voyager, de devenir cosmonaute, de donner vie aux dinosaures et aux contes de fée ! C’est d’ailleurs ce qu’a fait l’hébergeur en ligne Oxito à travers la production d’un film institutionnel _La légende de Marion d’Oxito_[^6].

    SCE environnement et aménagement a également utilisé le motion design pour exposer son dispositif de collecte et de traitement des macrodéchets dans les baies d’Abidjan. Un résultat efficace et pédagogique qui lui évite, en prime, le déplacement jusqu’en Côte d’Ivoire :

    <iframe width="640" height="360" src="https://www.youtube.com/embed/bBFMqnooHVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Création

!!! example "La réalisation d’une vidéo motion design"

    === "1. De l’idée au script"

        La toute première étape d’une réalisation de vidéo en motion design consiste à travailler votre stratégie marketing et communication avec l’un de nos chefs de projet audiovisuel qui vous sera dédié. Ensemble, vous définirez premièrement :

        - L’objectif prioritaire.
        - La cible.
        - Le message à faire passer.
        - Le moodboard pour définir le style visuel de votre projet audiovisuel.

        Ensuite, nous vous accompagnerons à la rédaction du scénario et de la voix-off en utilisant le storytelling. Le but étant qu’ils transmettent votre message de manière claire et efficace.

    === "2. Pré-production"

        À partir du discours de la voix-off et du style graphique retenu, nous vous proposons :

        - un storyboard sous forme de vignettes décrivant les différentes séquences de votre film ;
        - un choix de comédiens·ennes pour la voix-off ;
        - une première planche d’illustrations afin de nous assurer que le style vous convient.

        Cette dernière vous permet de vous représenter l’univers visuel autour de votre produit ou service. Si cet essai vous convient, nous concevons alors l’ensemble des visuels, soumis à une nouvelle validation de votre part.

    === "3. Montage vidéo par nos motion designers"

        Il est temps pour le motion designer de se lancer dans l’animation des séquences sous Aftereffects. Les visuels réalisés prennent vie de manière fluide et naturelle grâce à un montage millimétré. Eh oui ! Un personnage en mouvement se travaille par exemple du cycle de marche au mouvement des sourcils. Dix secondes de vidéo en motion peuvent représenter des heures de travail !

        De plus, l’ajout de sound design, ou habillage sonore, à travers des bruitages et une ambiance musicale, offre un résultat encore plus fascinant.

        La post-production achevée, une première version de la vidéo vous est envoyée pour vérification. Les modifications éventuelles terminées, nous vous livrons les fichiers définitifs en version haute définition et selon des formats adaptés pour vos canaux de communication.

[^1]: Source : [https://www.mars-videos.fr/2021/02/24/motion-design-definition/](https://www.mars-videos.fr/2021/02/24/motion-design-definition/)

[^2]: [Sueurs froides](https://www.youtube.com/watch?v=dC-_9Sf2Uaw)

[^3]: [Psychose](https://www.youtube.com/watch?v=Tek8QmKRODw&t=27s)

[^4]: [Génériques de James Bond](https://www.youtube.com/watch?v=PLhfMlsne5o&t=49s)

[^5]: [Motion Motion](http://motionmotion.fr/)

[^6]: [La légende de Marion d’Oxito](https://www.mars-videos.fr/realisations/oxito/)
