# Animation [^1]

<div style="width:480px"><iframe allow="fullscreen" frameBorder="0" height="270" src="https://giphy.com/embed/yV8Z9SMCnirHFn7mXk/video" width="480"></iframe></div>  

Que l'on joue à un jeu vidéo, que l'on regarde un dessin animé ou qu'on utilise une application dans laquelle il y a une interface, on est souvent spectateur d'animations. Des personnages du jeu, des éléments de décor, des boutons cliquables, une transition d'une page à l'autre, un logo qui prend vie. Tant d'utilisations de l'animation que c'est devenu banal d'en voir partout. Et pourtant...

!!! example "Il existe quatre grands types d'animation :"

    - [Animation 2D](http://127.0.0.1:8000/experience/Animation%202D/)
    - [Animation 3D](http://127.0.0.1:8000/experience/Animation%203D/)
    - [Animation en stop motion](http://127.0.0.1:8000/experience/Animation%20en%20stop%20motion/)
    - [Graphisme de mouvement](http://127.0.0.1:8000/experience/Graphisme%20de%20mouvement/)

## Introduction

!!! note "L'animation, au delà de l'esthétisme"

      Ce que les gens ignorent souvent, c'est son utilité. Certes dans un film ou un jeu ça paraît évident : sans actions, pas d'histoire. Mais pour un site web, à quoi bon mettre une transition animée d'une page à l'autre ?

    _Bah ça rend le truc joli et agréable quoi!_

    Exactement ! En général, on trouve que dès qu'il y a un peu d'animation, c'est plus "sympa". Ce que j'essaierai de vous expliquer dans cet article c'est ça :

    -   Que l'animation n'est pas qu'un petit détail qui fait que c'est plus joli
    -   Qu'elle implique directement le spectateur
    -   Qu'elle a sa place dans n'importe quel projet multimédia
    -   Qu'elle est vecteur d'informations

    Et en plus je vous donnerai quelques conseils pour que votre animation soit un peu mieux travaillée et plus logique (Web/interface/jeu vidéo/ film)

## Définition

!!! info "Définition de l'animation"

    Si vous demandez la définition à Google, il vous répondra ça :

    >Animation, nom féminin:
    >
    >- Caractère de ce qui est animé, plein de vie.
    >- Méthodes d'encadrement d'un groupe qui favorisent l'intégration et la participation de ses membres.
    >- Technique cinématographique permettant de donner l'impression du mouvement par une suite d'images fixes.

    On est d'accord que ce qui nous intéresse est la troisième définition. Mais je trouve que la première est la plus importante. Ce que google ne dit pas c'est que le mot Animation vient du latin Anima (_animae_.f) qui veut dire l'Âme, la vie, le souffle, la respiration.

    Pour faire court, l'animation c'est l'**art de donner vie à quelque chose**.

    Et cette définition est très importante. Si quelque chose prend vie, alors cette chose aura un caractère, et sa manière de bouger sera influencée par cette personnalité.

Et laissez moi vous donner la citation qui résume à quel point l'animation est importante :

!!! cite "_« Sans animation il n'y a pas de vie. Sans vie il n'y a pas de caractère. Sans caractère, pas d'émotion. Pas d'émotion, pas d'utilisateur satisfait. Pas d'utilisateur satisfait, pas de palais »_ -- **Moi, 2018**"

!!! example "Exemples d'animations"

    === "Films"
        Regardez bien ces deux animations. Elles racontent la même chose :

        ![animation exemple 1](images/txavery2.gif)
        
        ![animation exemple 2](images/snow.gif)

        Un personnage se croit seul et tranquille, et paf il se fait surprendre par un autre personnage. Mais regardez à quel point sans le son et sans contexte on peut tout comprendre: Tex Avery c'est super marrant, et Disney c'est super mignon.

        C'est ça qui est super avec l'animation. On comprend des choses et on ressent des choses, souvent inconsciemment !

    === "Jeux vidéos"

        Les animations suivantes viennent du jeu Overwatch[^2] où l'on incarne différents personnages qui ont chacun son histoire et sa personnalité.

        Faites attention à la manière dont chacun recharge son arme. L'animation donne beaucoup d'informations sur la personnalité : Tandis que l'une recharge délicatement en levant le petit doigt, un autre installe la mitraille d'un grand coup placide, alors qu'un dernier fait des gestes zen avec ses mains.

        ![overwatch animation](images/relodz.gif)

    === "Web"
        Un dernier exemple pour le web, où j'attire votre attention sur les transitions et la douceur des déplacements. On est définitivement sur un site vitrine, où l'utilisateur ne sera jamais gêné, où l'on explique les produits de façon limpide sans jamais brusquer. On est vraiment comme dans une boutique dans laquelle un vendeur nous vanterait paisiblement ses produits

        ![web animation exemple](images/shot.gif)

## Histoire

!!! info "Histoire de l'animation"

    !!! note "Une brève histoire de l'animation"
        Depuis le jeu d'ombres (ou, comme on l'appelle communément, la marionnette d'ombres) en 200 après J.-C., les humains ont trouvé de nombreux moyens créatifs de raconter des histoires.

        <iframe src="https://giphy.com/embed/r1jbtDXIAjq92" width="480" height="382" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

        Même si, de nos jours, l'imagerie générée par ordinateur (CGI) est utilisée pour créer n'importe quel type d'animation, les anciennes formes de narration, comme les marionnettes d'ombre, sont encore utilisées aujourd'hui.

    ??? faq "Qui a créé la première animation ?"
        De nombreuses personnes ont participé à l'histoire de l'animation et à la création des premières animations. Cependant, une seule personne est considérée comme le **"Père de l'animation".**

        Il s'agit du dessinateur français, Émile Eugène Jean Louis Courtet, dit Émile Cohl. Cohl est le créateur du premier film entièrement animé appelé _Fantasmagorie_. La première a eu lieu à Paris le 17 août 1908.

        <iframe width="560" height="315" src="https://www.youtube.com/embed/aEAObel8yIE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        Fantasmagorie est l'un des premiers exemples d'animation traditionnelle.

        Les historiens du cinéma considèrent qu'il s'agit du premier dessin animé, ce qui explique pourquoi Cohl est connu comme le père de l'animation.

        Le titre Fantasmagorie est inspiré de la **Phantasmagoria**, une forme de théâtre d'horreur du XIXe siècle qui utilisait des lanternes "magiques" pour projeter des images animées de squelettes, de fantômes, de démons, etc.

        Fantasmagorie, cette animation dessinée à la main ressemble à des dessins à la craie joués en séquence.

        Cependant, l'artiste a dessiné chaque image sur du papier blanc et l'a tirée sur un film négatif pour inverser les couleurs. C'est pourquoi cela ressemble à des dessins à la craie sur un tableau noir. Cohle a créé environ 700 dessins pour obtenir le résultat final.

        Depuis la création de Fantasmagorie, le premier film d'animation au monde, l'animation a parcouru un long chemin. Aujourd'hui, la plupart des animations sont créées à l'aide d'ordinateurs.

        L'animation par ordinateur est devenue populaire dès que les ordinateurs numériques se sont généralisés dans les années 1970.

## À quoi ça sert ?

!!! faq "À quoi sert concrètement une animation ?"

    Une animation, en plus de son aspect pragmatique doit absolument justifier le caractère de la chose animée.

    -   Un personnage qui est fatigué DOIT être animé comme quelqu'un qui est fatigué.
    -   Un site qui fait la promotion d'un film d'horreur DOIT avoir des animations angoissantes
    -   Une infographie animée DOIT être animée de manière claire pour que l'on comprenne chacune des informations

        === "Exemple 1" 
            
            Exemple où l'animation sert à expliquer quelque chose :
            
            ![explication animation 1](images/exemple1.gif)

        === "Exemple 2"

            Exemple où l'animation sert à donner une ambiance :

            ![explication animation 2](images/gif-ambiance-lumineuse.gif)

        === "Exemple 3"

            Exemple où l'animation sert à informer le spectateur que l'un des personnages est excité alors que l'autre est calme :

            ![explication animation 3](images/exemple3.gif)

Attention, c'est peut être le point le plus important de l'article alors n'arrêtez pas de lire :

## Questions à se poser

Si vous avez à animer quelque chose, posez vous trois questions:

!!! faq "Quel est le caractère de ce que je veux animer ?"

    Lourd, facile, stressé, joyeux, colérique, dynamique, moelleux, surprenant, grand, didactique, mélancolique…

    Le dictionnaire est bourré d'adjectifs qui peuvent vous aider, et si votre projet est bien préparé, vous saurez facilement trouver lequel est le plus adapté.

!!! faq "Qu'est-ce que le spectateur doit ressentir ?"

    Compassion, dégoût, GROSSE BARRE DE RIRE, apprentissage, être guidé, la facilité, étonnement, satisfaction, être perdu…

    Là c'est une tâche un peu plus compliquée parce que les gens sont tous différents et qu'ils vont pas tous avoir le même ressentiment face à vos animations. N'hésitez pas à vous demander ce qu'il va devoir penser:

    -   « Ah ouais ce type a l'air vraiment fatigué »
    -   “Oh, il l'avait pas vu alors qu'il était juste derrière lui HAHA”
    -   “Ah c'est bon j'ai trouvé le gros bouton rouge qui me permet d'annuler”
    -   “Rhalala mais c'est vachement intéressant ce truc”

!!! faq "Qu'est ce qu'il se passe si c'est mal animé ?"

    “Mal animé” ça veut dire que le sentiment exprimé par l'animation n'est pas passé chez le spectateur.

    === "Exemple d'une bataille "mal" animée"

        ![stickman animation](images/mal_anime.gif)

    === "Exemple d'une bataille mieux animée"

        ![stickman good animation](images/mieux_anime.gif)

    === "Exemple d'une bataille ARCHI bien animée"

        ![stickman very good animation](images/archi_bien_anime.gif)

    Un projet survit à de mauvaises animations. Encore une fois, c'est une histoire de ressenti et d'émotion inconsciente. C'est pas dramatique si on ne ressent rien, c'est tout simplement mieux si on ressent des choses.

    Par défaut une animation non travaillée est rigide et a un aspect robotique. Laissez moi vous donner quelques astuces pour que votre animation soit crédible.

## Les principes d'animation

!!! info "La règle d'or"

    Une animation doit suivre les **lois de la physique** !

    Gravitation, frottements, accélérations, poids…

    C'est tout simple: nous, les êtres humains, on est habitué à subir les lois de la physique, donc si on voit autre chose bouger de la même manière qu'on est habitué à bouger, c'est logique qu'on comprenne pourquoi c'est animé comme ça.

    Et si parfois on a l'impression que ce n'est pas régi par les lois de la physique, c'est que c'est très exagéré, comme de la caricature de mouvement.

    ??? example "Exemple de mouvement “caricaturé” :"

        ![respect mouvement animation](images/caricature.gif)

!!! tip "Plus de détails : les 12 principes d'animation"

    Je ne vais pas vous donner un cours d'animation, mais je vais vous donner les paramètres qui rentrent en compte pour avoir une animation cohérente. Ces principes sont principalement orientés pour l'animation traditionnelle de personnages, mais sont tout aussi valables pour le motion design de n'importe quelle interface animée. Ils sont utilisés chaque jours par les animateurs professionnels donc n'hésitez pas à en abuser dans vos projets.

    === "Compression et étirement"

        Étirez vos éléments et compressez les, ça donne du dynamisme dans les formes et une matière à vos objets

        ![compression et etirement](images/squash-and-stretch01.gif)

    === "Anticipation"

        C'est logique d'avoir une petite préparation à l'animation, ça donne une petite info sur l'animation à venir, et en plus ça ne surprends pas le spectateur parce qu'il est prévenu qu'une chose va vite arriver.

        ![Anticipation](images/anticipation.gif)

    === "Mise en scène"

        Comme au théâtre, il faut que chaque action soit claire et lisible.

        ![staging](images/mise_en_scene.gif)

    === "Toute l'action d'un coup / Partie par partie"

        ![Toute l'action d'un coup / Partie par partie](images/poseto-pose-strait-ahead.gif)

        Il s'agit là des deux manière principales d'animer. Soit on installe des poses clefs pour ensuite créer les poses intervalles, soit on y va image par image. Cette dernière se pratique pour les simulations de feu, eau, effets d'explosions ou tout autre animation où il n'y a pas besoin de fixer des poses clefs.

    === "Mouvement initial et Chevauchement"
        Continuité du mouvement initial et chevauchement de deux mouvements consécutif : 

        C'est ce qu'il manque souvent aux “mauvaises animations” Ces petites choses plus légères qui ont un retard sur le mouvement. Ne les oubliez pas.

        ![continuité animation](images/mouv_chevauchement.gif)

    === "Ralentissement"
        Ralentissement en début et en fin de mouvement :

        C'est le monde logique qui veut que les choses accélèrent et ralentissent lors d'un mouvement. Par pitié, évitez les choses linéaires, c'est d'un ennui !

        ![acceleration et freinage](images/ralentissement.gif)

    === "Trajectoire arquée"

        Rares sont les mouvements super droits. Préférez une courbe, même toute petite.

        ![trajectoires arquées](images/trajectoire.gif)

    === "Détails secondaires en mouvement"

        C'est pour donner plus de caractère à un personnage. L'action principale c'est “je mange un hamburger”. Mais c'est plus intéressant de lui faire lécher les babines avant non ?

        ![actions secondaires](images/secondary-action.gif)

    === "Cohérence physique / théâtrale"

        C'est un peu plus complexe à comprendre : Beaucoup d'images = lent, peu d'images = rapide. Si votre animation a un problème, essayez de rapprocher ou d'espacer les clés d'animation. Le timing c'est l'art de rendre les mouvements logique dans le temps.

        ![animation et cohérence physique](images/timing.gif)

    === "Éxagération"

        Les choses sont plus claires quand on exagère les mouvements. Même pour les animations réalistes, exagérez un peu vos poses pour que le spectateur n'ait pas de doute sur ce que vous voulez exprimer

        ![Animation et exagération](images/exageration.gif)

    === "Dessin en Volume"

        Le volume, c'est l'espace. L'espace fait vivre vos éléments, et nous les humains, on aime les trucs vivants. Mettez de la perspective.

        ![Dessin en volume et animation](images/dessin_volume.gif)

    === "Charisme"

        Et le dernier principe, c'est le charisme. Il est en lien avec l'exagération. C'est une histoire de forme. Simplifiez les et variez les pour que l'on puisse comprendre facilement à qui ou quoi on a à faire.

        ![charisme et animation](images/charisme.gif)

## Pour aller plus loin

!!! info "Pour aller plus loin dans l'animation"

    Pour aller plus loin dans cette histoire de principes d'animation, allez jeter un coup d'oeil [à cette vidéo](https://www.youtube.com/watch?v=uDqjIdI4bF4). Les gifs de cet article proviennent de là. Je ne connais pas de meilleure vidéo pour expliquer ces principes (en anglais).

    Remontez l'article, et regardez encore une fois les gifs. Essayez de retrouver l'application de ces principes ;)

    Maîtrisez ça et vous êtes directement embauchés chez Pixar.

Ok c'est compliqué à maîtriser, mais si vous avez ne serait-ce que les bases dans vos compétences, ce serait un gain gigantesque pour vos projets.

## Conclusion

!!! tldr "Conclusion"

    Ne négligez pas l'animation. Elle apporte tellement de choses qu'il serait vraiment trop bête de passer à côté.

[^1]: Source : [https://www.wanadev.fr/139-limportance-de-lanimation-dans-vos-projets/](https://www.wanadev.fr/139-limportance-de-lanimation-dans-vos-projets/)

[^2]: [Overwatch](https://playoverwatch.com/fr-fr/)
