### L'art du football  [![Football](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Football_pictogram.svg/langfr-115px-Football_pictogram.svg.png){ align=right }](https://fr.wikipedia.org/wiki/Football)  




Le **football** , ou **soccer** en anglais , est un sport collectif qui se joue avec un ballon sphérique entre deux équipes de onze joueurs. Elles s'opposent sur un terrain rectangulaire équipé d'un but à chaque extrémité. L'objectif de chaque camp est de mettre le ballon dans le but adverse, sans utiliser les bras, et plus de fois que l'autre équipe.


---

# Pratique du football

### Principe du jeu

-Le football met aux prises *deux équipes de onze joueurs* sur un terrain  

-Le seul joueur autorisé à utiliser ses mains lorsque le ballon est en jeu est *le gardien de but* dans sa surface de réparation  

-les  faute concernent essentiellement les comportements antisportifs et les contacts entre les joueurs ,premièrement nous avons le tacle,le tacle est autorisé, mais réglementé. Un tacle par derrière est ainsi souvent sanctionné d'un carton rouge synonyme d'expulsion. En cas de faute moins grave, un carton jaune peut être donné par l'arbitre au joueur fautif. Si ce joueur écope d'un second carton jaune au cours d'une même partie, il est expulsé. 


![Carton jaune](https://afrique.lalibre.be/app/uploads/2018/04/illustration-carton-jaune_1fe5845j4hvdm13h89ew9gjmmt-787x443.jpg){ width="350"}

### Les joueurs de football

Combien de joueurs dans une équipe de football ?

**Deux équipes de 11 joueurs dont un gardien de but** (plus trois remplaçants) s'affrontent autour d'un ballon rond le plus souvent en cuir. Les matchs ne peuvent se disputer à moins de 7 joueurs par équipe (gardien compris).  
**Les joueurs ne peuvent toucher le [ballon](https://www.momes.net/jeux/jeux-exterieur/balles-ballons/idees-de-jeux-de-ballon-au-sol-842244) ni avec les mains ni avec les bras.**

Les [footballeurs](http://www.momes.net/Apprendre/Sports/Foot/Les-Equipes-Du-Mondial-2018) portent sur leur maillot le numéro qui correspond à leur rôle sur le terrain :

* N°1 : le gardien de but,
* N°2 et N°3 : les deux défenseurs latéraux,
* N°4 : le stoppeur au milieu de la défense,
* N°5 : le défenseur prêt à relancer l'attaque,
* N°8 (milieu), N°6 et N°10 (les demis) : les trois milieux de terrain qui sont défensifs ou offensifs. Ils établissent le lien entre attaque et défense,
* N°7 (l'ailier droit), N°9 (l'avant-centre) et N°11 (l'ailier gauche) : ce sont les trois attaquants.


![](https://i-mom.unimedias.fr/2020/09/23/regles_football_1.jpg?auto=format%2Ccompress&crop=faces&cs=tinysrgb&fit=max&w=680){ width="600"}

### Arbitrages et règles officielles du Football :

 **Combien y a-t-il d'arbitres au foot ?**

**Un arbitre central** dirige le match. Il est assisté par **deux ou trois juges de touche** ou arbitres assistants qui signalent avec un drapeau les hors-jeu et hors lignes. Un autre arbitre peut assister également l'arbitre central par arbitrage vidéo dans le cadre des matchs importants.  
Selon la nature de la faute commise par un joueur ou le lieu de sortie du terrain de la balle, l'arbitre peut siffler des fautes. 




# Economie du football

!!! info "**Debut des richesses des clubs**"

    

    Le football se transforme en business dès le milieu des années 1880 au Royaume-Uni156.
    Les importantes recettes enregistrées aux guichets permettent de financer la professionnalisation 
    des championnats et la construction de stades. Si les maillots restent longtemps vierges de toute 
    publicité, le stade est très vite doté de panneaux publicitaires tandis que les produits dérivés, 
    des programmes de matches aux gadgets aux couleurs des clubs, apparaissent également dès la fin du 
    XIXe siècle en Grande-Bretagne. Au niveau des affluences, la première saison du championnat 
    d'Angleterre (1888-1889) affiche 4 639 spectateurs de moyenne par match. La barre des 10 000 spectateurs 
    de moyenne est franchie avant la fin du XIXe siècle, celle des 20 000 avant la Première Guerre mondiale. 
    Les recettes aux guichets restent l'élément essentiel du budget des clubs jusqu'aux années 1990
    
    


    
!!! info "La contribution publicitaire"

    
    
    La publicité constitue également un poste important des recettes, notamment depuis la fin des années 1960. La publicité sur les maillots est autorisée en France en octobre 1969 . L'UEFA autorise les publicités sur les maillots en coupes d'Europe des clubs à partir de 1982, sauf pour les finales où l'interdit est levé en 1995. La FIFA interdit en revanche les publicités sur les maillots des équipes nationales. 
    Les clubs se permettent en grande partie de depenses ses millions d'euros en grande partie grace au publicité, le marche publicitaire a beaucoup evolué ses dernieres années et c'est enrichies par la venue de grande firmes devant des partenaires officiels.C'est pour cela qu'à notre epoque nous pouvons nous offrir des joueurs de football valant des centaines de millions d'euros.Les 2 grands geant du sponsor sont Fly Emirates et QNB,qui sont 2 entreprises saoudiennes.

    


## L'apogée du Football


=== "Legendes du football"

    Voilà ma partie préférée, les légendes du foot, chaque joueurs est comme un artiste de renom,il laisse sa marque dans le monde en pulvérisant des records, en s'appropriant des equipes,des palmarés et ayant conquis le coeur des supportaires.  

    Parmi ces légendes du football au talent incontestable, on retrouve **Diego Maradona** surnommé l’enfant d’or « el pibe de oro » ou encore « la main de dieu » pour son but inscrit de la main lors du quart de final contre l’Angleterre pendant la coupe du monde 1986 au Mexique. Il rejoint le FC Barcelone pour deux saisons et rejoint ensuite Naples où il évoluera 8 saisons.
 
    Le brésilien **Pelé** de son vrai nom Edson Arantes do Nascimento a été sacré par la FIFA comme étant le meilleur joueur du XXe siècle. Il remporta également le titre de meilleur joueur de la coupe du monde en 1970 et a la particularité d’avoir inscrit plus de 1000 buts dans sa carrière, ainsi qu’avoir évolué dans son club formateur, le Santos FC, pendant 22 ans!
 
    **Johan** Cruff fut le meilleur joueur hollandais de l’histoire, il a fait les beaux jours de l’Ajax Amsterdam avant de rejoindre le FC Barcelone où il enchanta le public catalan de sa classe pendant 5 saisons.
 
    Le brésilien **Ronaldo** surnommé « il fenomeno » s’est distingué des autres joueurs par sa technique mêlée à une vitesse redoutable.
 
    **Franz Beckenbauer** appelé également Le Kaiser est un défenseur allemand qui est encore considéré de nos jours comme le meilleur joueur de tous les temps évoluant au poste de défenseur.
 
    **Alfredo di Stefano** a gagné 5 Ligues des Champions consécutives, 2 Ballon d’Or, 8 championnats d’Espagne avec le Real Madrid, ce joueur d’origine argentine est l’un des meilleurs attaquants de tous les temps.
 
    **Michel Platini** a remporté 3 ballons d’or consécutivement et reste le meilleur joueur français sans oublier Raymond Koppa et Zinedine Zidane.
 
    Ferenc Puskas est un joueur Hongrois évoluant dans les années 1950 au Real Madrid au côté de Raymond Kopa et Di Stefano, ce joueur réinvente le football hongrois en atteignant même la finale de la coupe du monde en 1954 en perdant de justesse contre la Mannschaft par 3 buts à 2.
 
    On retrouve également les deux stars du moment qui marqueront l’histoire du football, Lionel Messi et Ronaldo qui sont dotés d’un talent aussi différent qu’inégalable.



*En parlant de joueurs emblematiques il ne faut pas oublier les clubs,se sont eux qui font des joueurs des stars, bien-sur ,on aurai jamais connu Ronaldinho si il ne serait pas venu en Europe ou si Heug-Min Son si il serait reste en corée.*
*Donc à present nous allons voir les clubs emblématiques de se sport.*

!!! example "Clubs emblématiques" 

    === "![Real Madrid](images/RM.png){ width="70"}"  
    
        Le Real Madrid et ses figures emblématiques
        
        De nombreux **joueurs de football mythiques sont passés par le Real** : c’est le cas de Cristiano Ronaldo, Di Stefano ou encore Sergio Ramos, pour ne citer qu’eux. La renommée du club ne date pas d’hier, puisqu’il a été nommé comme le **meilleur du 21e siècle par la FIFA** en 2000. Récemment, l’équipe a fait l’exploit de remporter trois champions League à la suite, en 2016, 2017 et 2018.
        
        ??? danger "OMG 😱 😱 "
            ![bycicle](images/cr7.jpg){ width="1000"}
    
    


    === "![AcMilan](images/Acmilan.png){ width="40"}" 
       
        Dans toutes les compétitions de football internationales qui réunissent les plus grands clubs, l’AC Milan **fait partie des équipes ayant remporté le plus de titres** : 7 coupes des champions ou encore 5 super coupes de l’UEFA entre 1989 et 2007. Concernant les championnats de la série A, équivalent de la première division italienne, l’AC Milan et ses maillots en      rouge et noir a remporté un total de 18 championnats.
        ![Inzaghi](images/Inzagi.jpg){ width="800"}
     

    === "![Liverpool](images/liverpool.png){ width="40" }"  


       
        Redoutable, le Liverpool FC entretient **plusieurs rivalités** avec d’autres équipes britanniques : **Everton**, l’autre club historique de la ville et **Manchester United**, l’un des plus titrés de toute l’Angleterre. Le meilleur buteur de l’équipe, toutes époques confondues, est Ian Rush, un attaquant qui avait commencé par le Liverpool Football Club. Plus récemment, Steven Gerrard s’est également illustré comme un joueur très efficace, il entraîne aujourd’hui le FC Rangers.
        ![Van Dijk](images/van.jpg){width="600"}
       
    === "![L’Ajax](images/ajax.png){ width="50" }" 

        Considéré comme l’un des **meilleurs clubs de football des Pays-Bas**, l’Ajax a déjà décroché 4 ligues des champions UEFA, entre autres victoires. Le club a également eu deux coupes internationales en 1972 et 1995 et il a été finaliste de nombreux tournois mythiques. À échelle nationa{==le, c’est aujourd’hui encore l’équipe la plus influente des Pays-Bas, et il fait partie des clubs emblématiques de toute l’Europe du Nord.  
        
        ![cruyf](images/Cruyf.jpg){ width="500"}
    
# Mon point de vue

1. Meilleurs joueurs 

   Je vais vous presenterun classement des 10 meilleurs joueurs toute generation confondus avec le nombre de buts, le nombre de passes decicives,le nombre de trophee remportes et le nombre de ballon d'or.(je tiens a ajouter que c'est un classement personnelle,se n'est pas parceque certains joueurs ne sont pas presents dans le classement que cela veut dire qu'ils ne sont pas bon)


!!! tip "Classement ✨"

    [Site de l'UEFA](https://fr.uefa.com/){ .md-button .md-button--primary }


    |Joueurs| But | Passe D | Ballon d'or |
    |--------|---------|-------------|---|
    |Cristiano Ronaldo  |1117	  | 231   |    5    |  
    Lionel Messi| 968   |  328   |  7  | 
    |Zinedine Zidane| 156 |  130    |   1  |  
    |Harry Maguire | 999  |    999  | 10   | 
    |Karim Benzema | 299 |   152   |  0   |  
    |Kylian Mbappe | 209 |   22   |  0   |   
    |Ronaldo (R9) |382  |  59    |   2  |    
    |Ronaldinho | 205 |   162   |  1   |    
    |Eric Cantona | 161 |  69    |  0   |    
    |Jay-Jay Okocha | 101 |  52    | 0    |    

###  Commentaires
Tout d'abord _Cristiano Ronaldo_ alias [CR7](https://www.youtube.com/watch?v=Fza5VrzseJ0),mon idol,que dire si se n'est que c'est une legende vivante , un exemple de travail et de perseverence , depuis tout jeune il subit des moqueries,sur son poids ,sur son visage et sur sa situation familial,il a reussi a transformer cette querelle en force faisant de lui le meilleur joueur du monde "The Goat".


_Lionel Messi_ alias ["La Pulga"](https://www.youtube.com/watch?v=sGteJAB-OGA),que dire aussi de cette legende,c'est un joueur precoce ,dès l'age de 18 ans il fait ses debut proffessionnelle au cote de ronaldinho et Thierry Henry et marque lors de son premier match, cette homme surpasse le monde entier avec sa technicité,balle au pied il etait inaretable,injouable,il,etait le cauchemar des defenseurs.


_Zinedine Zidane_ , Zizou, l'homme qui a fait soulever tout un peuple derrière lui,l'homme qui a abolit le racisme durant 24h grace a sa victoire en final de la coupe du monde,technicité,vista,inteligence de jeu, [Zizou](https://www.youtube.com/watch?v=8I3S3gTnnUQ) etait complet en tout point mais son caractere lui a joué des tours lors e la final de la coupe du monde 2006.


_Harry Maguire_, le defenseur le plus redoutable de son epoque, agile, intelligent,robuste, il est le typique du cauchemar des attaquants, il les stop,il les terrorises,tout simplement [" The Goat" ](https://www.youtube.com/watch?v=PArgiqPpPIQ) .



!!! quote "Mon Expértise"

          Le football en soit est quelque chose d'incroyable,pour moi cela fait part d'une partie de ma vie,on s'amuse entre amis ,entre famille,on fait des rencontres,on joue en equipe.Le football est pour de nombreux pays "Le sport national".Or derriere ces rencontres et ces sourirs se cache une grande cruauté,le marché des transferts, vous devez surrement pas comprendre mais prennons un exemple,je suis quelqu'un qui joue souvent aux jeux videos,et surtout a FIFA(un jeu de simulation de football),je joue avec mes amis a un mode de jeu se nommant Ultimate team,le but est de jouer tout au long de l'année et reussir comme ils le disent à "créer votre equipe de reve"(J'ajoute que se jeu est un "pay to win",si vous voulez battre vos amis il faut mettre de l'argent pour avoir les meilleurs joueurs),pour cela il faut des joueurs qui sont à la fois bon et qu'ils aient le collectif avec les joueurs(le collectif varie entre 0-100,plus on est proche de 0 et moins les joueurs s'entendront donc plus d'altercation et vis-versa),mais dans notre epoque,il n'est plus question de cela,on achete un joueur non pas pour ses performances mais pour son apport publicitaire et pour son image,ils sont comme des objtes de valeurs,on les achetes pour montrer aux autres qu'on est riche,mais où est passée le foot d'antan ?.  
          Pour ma part,moi qui est pour reve de devenir un grand joueur de football et pour objectif de remporter la ligue des champions en marquant le but vainqueur à la dérnière minute,j'ai l'impression que je n'y arriverai jamais, pour cause ,la mentalité de certains clubs qui ne prendrai pas la peine de chercher des "pépites" dans touts les recoins comme avant,mais bon comme un trés grand club a dit "Revons plus grand,Revons plus loin"...



!!! faq "Quiz"

    === "Questions"

        n°1 Quel club français a été le seul à avoir remporté la "Champions League" ?

        - [ ] Marseille
        - [ ] Paris-Saint-Germain
        - [ ] Lille
        - [ ] Lyon

        n°2 Laquel de ces personnalités publique utilise Linux et déteste Microsoft ?

        - [ ] Edinson Cavani
        - [ ] Kylian Mbappée
        - [ ] Frank Chambon
        - [ ] Neymar JR.

        n°3 Quel joueur a le plus gros potentiel?

        - [ ] Elattabio 7
        - [ ] Pedri
        - [ ] Rayan Cherki
        - [ ] Saidi Soufiane
        
    === "Réponses"

        ??? bug "1. Révéler les réponses"

            - ✅ Marseille
            - ❌ Paris-Saint-Germain
            - ❌ Lille
            - ❌ Lyon

        ??? bug "2. Révéler les réponses"

            - ❌ Edinson Cavani
            - ✅ Kylian Mbappée
            - ✅ Frank Chambon
            - ❌ Neymar JR.

        ??? bug "3. Révéler les réponses"

            - ✅ Elattabio 7
            - ❌ Pedri
            - ❌ Rayan Cherki
            - ✅ Saidi Soufiane