# Top 10 des meilleures clubs

![Quels sont les clubs les plus titrés dans les différentes Coupes d'Europe ?](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/Real-Madrid-v-Club-Atletico-de-Madrid---UEFA-Champ-656275b6368530768d83b556b900fca1.jpg "Quels sont les clubs les plus titrés dans les différentes Coupes d'Europe ?")

Quels sont les clubs les plus titrés dans les différentes Coupes d'Europe ? 


**Depuis la création du football, de nombreuses compétitions européennes permettent aux meilleurs clubs de s'affronter, au terme de rencontres épiques. Mais quels sont ceux qui ont remporté le plus de trophées européens ? Dans ce top 10, il n'est pas surprenant de ne retrouver que du lourd.**

* * *

## 10\. Manchester United - 6 titres

En quête d'un retour au premier plan, [Manchester United](https://www.90min.com/fr/teams/manchester-united) reste tout de même l'un des clubs les plus emblématiques outre-Manche. Au cours de son histoire, les _Red Devils_ ont notamment remporté la [Ligue des Champions](https://www.90min.com/fr/categories/champions-league) à trois reprises (1968, 1999 et 2008), dont l'une des finales les plus incroyables en 1999, grâce au _Fergie Time_.  
  
Plus récemment, l'équipe mancunienne s'était offerte la première Ligue Europa de son histoire, en l'emportant face à l'Ajax Amsterdam (2-0), en 2016. Avant cela, Manchester United avait aussi soulevé le trophée de la Coupe d'Europe des vainqueurs de Coupe (1991), puis la Supercoupe de l'UEFA la même année.

## 9\. Inter Milan - 6 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagejpegbase649j4AAQSkZJRgABAQEBLAEsAAD4QAyRX-38a55220360995406048a1c55510dfe9.jpg)



Tout le monde se rappelle du fameux triplé de l'Inter Milan, en 2010. Sous les ordres d'un José Mourinho au sommet de son art, les _Nerazzurri_ remportaient alors la Ligue des Champions, ce qui est leur dernier titre européen, à l'heure actuelle. Pourtant, le club lombard dominait le football au début des années 1960. Avec un sacre en 1964 et en 1965, toujours en C1, l'Inter Milan s'offre le luxe d'être dans ce top 10.  
  
Le club a souvent très bien fonctionné, par période. Ce fut aussi le cas, dans les années 1990. Les _Nerazzurri_ ont tout de même gagné la Coupe de l'UEFA, à trois reprises (1991, 1994 et 1998). Comme Manchester United, l'équipe italienne cherche à revenir rapidement parmi le gratin européen.

## 8\. Atlético de Madrid - 7 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagejpegbase649j4AAQSkZJRgABAQEBLAEsAAD4SVSRX-c866721426c7916d1160f45812f48d4a.jpg)


Bien que l'Atlético de Madrid n'ait encore jamais remporté la Ligue des Champions, le club madrilène s'est installé dans ce top 10 ces dernières années. Hormis un succès en Coupe des Coupes en 1962, les _socios_ _colchoneros_ ont attendu de nombreuses années pour voir leur équipe tenir la dragée haute aux meilleures écuries du continent.  
  
Mais, ces derniers temps, et avec le talent de Diego Simeone, l'Atlético de Madrid est bien l'une des meilleures équipes d'Europe. En plus d'avoir remporté trois fois la Ligue Europa (2010, 2012 et 2018), le club madrilène a toujours battu le vainqueur de la Ligue des Champions, dans la foulée, en Supercoupe d'Europe (2010, 2012 et 2018).  
  
Ayant échoué à trois reprises en finale de la plus prestigieuse des compétitions européennes, l'Atlético de Madrid aurait pu encore grimper dans ce classement.

## 7\. Juventus - 8 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagejpegbase649j4AAQSkZJRgABAQEBLAEsAAD4QiZaH-c9b11deaad916b034bead90af2954fd7.jpg)



Revenue parmi les meilleurs clubs européens, ces dernières années, la [Juventus](https://www.90min.com/fr/teams/juve) n'arrive toutefois toujours pas à remporter la Ligue des Champions. Il faut remonter à 1996 pour assister au dernier sacre des _Bianconeri_ en C1. Le second titre du club piémontais, puisqu'il s'était aussi imposé, dans cette compétition, en 1985.  
  
Pour faire partie du gratin européen, la Juventus a mis son nom au palmarès de la Coupe de l'UEFA à trois reprises (1977, 1990 et 1993), mais aussi à la Coupe des Vainqueurs de Coupe (1984) et à la Supercoupe de l'UEFA (1984 et 1996). Avec l'arrivée de [Cristiano Ronaldo](https://www.90min.com/fr/players/cristiano-ronaldo), les _Bianconeri_ s'attendent à entrer dans le top 5 le plus rapidement possible.

## 6\. Ajax Amsterdam - 8 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagejpegbase649j4AAQSkZJRgABAQEBLAEsAAD4QiuaH-8af03eb0b895f86bf772d26c6daf6ef6.jpg)


Le joli parcours de l'Ajax Amsterdam en Ligue des Champions l'année passée, a rappelé de très bons souvenirs aux supporters hollandais. Il y a encore quelque temps, le club de la capitale des Pays-Bas était l'une des meilleures équipes européennes.  
  
Avec quatre succès en Ligue des Champions (1971,1972, 1973 et 1995), l'Ajax Amsterdam a marqué cette compétition. Le célèbre club hollandais, mené un temps par le légendaire Johan Cruyff, complète son palmarès européen avec une Coupe de l'UEFA (1992), une Coupe des Vainqueurs de Coupe (1987) et la Supercoupe de l'UEFA, à deux reprises (1973 et 1995).  
  
Le club hollandais se place aux portes du top 5 des clubs les plus titrés en Europe.  
  

## 5\. Bayern Munich - 9 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagejpegbase649j4AAQSkZJRgABAQEBLAEsAAD4SMIRX-0f2be9efd2c1ec548630cc878377aa4a.jpg)



Le Bayern Munich est souvent l'un des grands favoris de la Ligue des Champions. Toujours très dominateur sur le plan national, le club bavarois possède l'une des plus belles histoires européennes. L'équipe allemande a remporté pas moins de six fois la Ligue des Champions (1974, 1975, 1976, 2001, 2013 et 2020). En plus de ses sacres, il s'est incliné à cinq reprises en finale de la C1.  
  
Pour venir garnir encore un peu son palmarès, le Bayern Munich peut compter sur une Coupe de l'UEFA (1996), une Coupe des Vainqueurs de Coupe (1967) et une Supercoupe de l'UEFA (2013).

## 4\. Liverpool - 13 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagejpegbase649j4AAQSkZJRgABAQEBLAEsAAD4Q8FRX-d70094138c6321e40729bacdf3c72559.jpg)



On rentre désormais dans le très haut du panier. Aux portes du podium, Liverpool compte une très large avance sur son premier poursuivant. Les _Reds_ possèdent pas moins de 13 titres européens, depuis leur création, avec notamment six succès en Ligue des Champions (1977, 1978, 1981, 1984, 2005 et 2019), dont cette incroyable victoire face à l'AC Milan en 2005, après avoir été mené de trois buts à la pause.  
  
N'oublions pas ses trois victoires en Coupe de l'UEFA (1973, 1976 et 2001) et ses quatre sacres en Supercoupe de l'UEFA (1977, 2001, 2005 et 2019). De quoi se faire une place de taille, dans ce classement.

## 3\. FC Barcelone - 14 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagejpegbase649j4AAQSkZJRgABAQEBLAEsAAD4QgzaH-a321f77079f63a4b4f4a13921d482189.jpg)



Le [FC Barcelone](https://www.90min.com/fr/teams/barca) continue d'être l'un des meilleurs clubs européens, depuis de longues années déjà. Toujours emmenés par [Lionel Messi](https://www.90min.com/fr/players/lionel-messi), les _Blaugrana_ se sont fait remarquer en Ligue des Champions. Avec cinq succès en C1 (1992, 2006, 2009, 2011 et 2015), le club catalan s'est installé parmi les équipes les plus titrées de cette compétition.  
  
Pour devancer Liverpool et se faire une place sur le podium, le FC Barcelone s'est aussi offert la Coupe des Vainqueurs de Coupe, à quatre reprises (1979, 1982, 1989 et 1997) et cinq fois la Supercoupe de l'UEFA (1992, 1997, 2009, 2011 et 2015).

## 2\. AC Milan - 14 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/dataimagejpegbase649j4AAQSkZJRgABAQEBLAEsAAD4QhcaH-aa865c179268eca610b3e7df65fe0f5f.jpg)



Tout le monde l'attend ce retour de l'AC Milan sur le devant de la scène. Depuis de longues années, les _Rossoneri_ sont loin de leur gloire d'antan. Il est bon de rappeler que ce club est l'un des plus emblématiques de l'Histoire du football. Bien qu'il soit absent de la Ligue des Champions depuis 2014, le club italien est le deuxième le plus titré de la compétition, avec sept sacres (1963, 1969, 1989, 1990, 1994, 2003 et 2007).  
  
Les _Rossoneri_ ont aussi remporté la Coupe des Vainqueurs de Coupe, deux fois (1968 et 1973) et la Supercoupe de l'UEFA, à cinq reprises (1989, 1990, 1994, 2003 et 2007).

## 1\. Real Madrid - 19 titres

![](https://images2.minutemediacdn.com/image/upload/c_fill,w_720,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/Real-Madrid-Celebrate-After-Victory-In-The-Champio-78ec77128f1325620cb11f5c2dcd61b2.jpg)




Sans surprise, le [Real Madrid](https://www.90min.com/fr/teams/real-madrid) se retrouve à la première place de ce classement. Les _Merengue_ ont tout simplement le plus beau palmarès européen. Avec treize succès en Ligue des Champions (1956, 1957, 1958, 1959, 1960, 1966, 1998, 2000, 2002, 2014, 2016, 2017 et 2018), le club madrilène ne laisse que peu de place à la concurrence.  
  
Après s'être offert deux fois la Coupe de l'UEFA (1985 et 1986), le Real Madrid détient aussi la Supercoupe de l'UEFA, quatre fois dans son palmarès (2002, 2014, 2016 et 2017). De quoi se placer confortablement tout en haut de ce classement.