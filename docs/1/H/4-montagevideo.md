# Montage Video

### Introduction

En plus de la modélisation et de l’animation, Blender peut être utilisé pour éditer des vidéos. Il existe deux méthodes possibles pour cela, l’une étant le compositeur et l’autre, décrite dans ce chapitre, étant le séquenceur vidéo. Le séquenceur vidéo de Blender est un système de montage vidéo complet qui vous permet de combiner plusieurs canaux vidéo et d’y ajouter des effets. Vous pouvez utiliser ces effets pour créer des montages vidéo puissants, en particulier lorsque vous les combinez avec la puissance d’animation de Blender!

Pour utiliser le séquenceur vidéo, vous chargez plusieurs vidéo-clips et les mettez bout à bout (ou dans certains cas, vous les superposez), tout en insérant des fondus et des transitions pour lier la fin d’un clip au début d’un autre. Enfin vous pouvez ajouter l’audio et synchroniser avec la séquence vidéo.Pour utiliser le séquenceur vidéo, vous chargez plusieurs clips vidéo et les placez bout à bout (ou dans certains cas, les superposez), en insérant des fondus et des transitions pour lier la fin d’un clip au début d’un autre. Enfin, vous pouvez ajouter de l’audio et synchroniser le timing de la séquence vidéo pour qu’elle corresponde.7


![image](imagespage4/video-editing_introduction_screen-layout.png)

---

### Configurez votre projet

Le proverbe “Un bon départ est la moitié de la bataille”, s’applique certainement au montage vidéo. Configurer votre environnement de travail et votre projet en fonction de vos besoins est la clé du succès. Dans le montage de votre projet vidéo, vous devez distinguer:

- Paramètres et activités liés à l’environnement de travail:

    Ces réglages s’appliquent à tous vos projets et sont définis au “niveau Blender”; par exemple l’installation de modules complémentaires. En fait, ils peuvent également influencer vos projets de montage non-vidéo. La plupart de ces paramètres restent plus ou moins stables tout au long de vos projets et ne doivent probablement être définis qu’une seule fois.

- Paramètres et activités liés au projet:

    Ces paramètres varient d’un projet à l’autre et sont très spécifiques à votre projet; par exemple le format du support de sortie. Pour chaque nouveau projet, vous devez évaluer ces paramètres et ces activités.

Bien sûr, de nombreux paramètres et activités se déroulent aux deux niveaux. Par exemple, les proxys automatiques peuvent être activés globalement mais modifiés par projet ou même par bande. La disposition de l’espace de travail de montage vidéo est définie au niveau de Blender mais peut être modifiée par projet.

