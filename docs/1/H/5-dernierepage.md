Voila je vous ai appris quelque base pour faire vos propres animations

Si vous en voulez plus je vous laisse sur le site officiel du manuel de Blender : [Manuel Blender](<https://docs.blender.org/manual/fr/dev/index.html>)

Si vous voulez commencer votre premiere animation je vous laisse sur ces quelques chaine youtube spécialiste en animation blender :

| chaine Youtube | Lien |
|---------|-------------|
| :material-check: Polyfjord  | [Polyfjord](<https://www.youtube.com/channel/UC1MmrnDaPnNa55twYBiH4NA>) |
| :material-check: Motion Live Teach | [Motion Live Teach](<https://www.youtube.com/user/MrTheNinjaBoy07>) |
| :material-check: Mykol3D | [Mykol3D](<https://www.youtube.com/c/Mykol3D>) |
| :material-check: Benjamin Cerbai | [Benjamin Cerbai](<https://www.youtube.com/c/BenjaminCerbai/featured>) |



