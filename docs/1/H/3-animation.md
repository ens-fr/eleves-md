# Animation et Rigging

### Animation

### Introduction

Une animation consiste à déplacer un objet ou à changer sa forme avec le temps. Les objets peuvent être animés de plusieurs façons :

**Les déplacer** comme un objet entier
Modifier leur position, leur orientation ou leur taille avec le temps ;

**Les déformer**
En animant leurs sommets ou leurs points de contrôle ;

**Animation par héritage**
En faisant l’objet se déplacer suivant le mouvement d’un autre objet (par ex. son parent, hook, armature, etc…).

Dans ce chapitre, nous allons couvrir les deux premiers, mais les bases données ici sont réellement vitales pour comprendre aussi les chapitres suivants.

L’animation est typiquement accomplie avec l’utilisation de ==trames clé.==

### Couleurs d’état


![images](imagespage3/animation_introduction_state-colors.png)

Les propriétés possèdent différents couleurs et items de menu pour différents états.

| Couleur | Fonction |
|---------|-------------|
| Gris  | Pas animé |
| Jaune | Inséré dans une trame clé sur la trame courante |
| Green | Inséré dans une trame clé sur une trame différente |
| Orange | Modifié à partir de la valeur dans la trame clé |
| Mauve | Contrôlé par un pilote |

### Rigging

Rigging est un terme général pour ajouter des contrôles aux objets, typiquement à des fins d’animation.

Rigging implique souvent d’utiliser un ou plusieurs des éléments suivants :

**Armatures**
Ceci permet à des objets mesh d’avoir des joints flexibles et est souvent utilisé pour l’animation de squelette.

??? info "Armatures"

    Je dois absolument vous parlez des Armatures c'est la base de l'animation.

    ### Introduction Armatures

    Dans Blender, on peut considérer qu’une armature ressemble à celle d’un vrai squelette, et tout comme un vrai squelette, une armature peut être constituée de nombreux os. Ces os peuvent être déplacés et tout ce à quoi ils sont attachés ou associés se déplace et se déforme de la même manière.

    Une ==armature== est un type d’objet utilisé pour le rigging. Un rig comprend les contrôles et les ficelles qui bougent une marionnette. L’objet Armature emprunte beaucoup d’idées sur les squelettes du monde réel.

    ### Votre première armature

    De manière à voir ce que nous sommes en train de discuter, essayons d’ajouter l’armature par défaut dans Blender.

    (Notez que les détails d’édition d’armature sont expliqués dans la ==section d’édition d’armature.==)

    Ouvrez une scène par défaut, puis :

    1. Supprimez tous les objets dans la scène.

    2. Assurez-vous que le curseur est à l’origine du Monde avec ++"Maj-C"++.

    3. Pressez sur ++"Pavnum1"++ pour voir le Monde dans la Vue Front.

    4. Ajoutez un Single Bone (`os unique`) (`Add ‣ Armature`).

    5. Appuyez sur ++"PavnumSuppr"++. pour voir l’armature avec un zoom au maximum.

    ![images](imagespage3/animation_armatures_introduction_default.png)

    ### L’objet Armature

    - Comme vous pouvez le voir, une armature est comme n’importe quel autre type d’objet dans Blender :

    - Elle a une origine, une position, une rotation et un facteur d’échelle.

    - Elle a un bloc de données Object Data, qui peut être édité en Mode Édition.

    - Elle peut être liée à d’autres scènes, et les mêmes données d’armature peuvent être réutilisées sur plusieurs objets.

    - Toute l’animation que vous réalisez en Mode Objet ne fonctionne que sur l’objet entier, pas sur les os de l’armature (utilisez le Mode Pose pour faire cela).

    Comme les armatures sont conçues pour être posées, pour une scène statique ou une scène animée, elles ont un état spécifique, appelé ==position== de repos. C’est l'état” par défaut de l’armature, la position/rotation/échelle par défaut de ses os, telles que définies en Mode Édition.

    En Mode Édition, vous verrez toujours votre armature en position de repos, alors qu’en Mode Objet et en Mode Pose, vous avez habituellement la “pose” courante de l’armature (à moins que vous activiez le bouton Rest Position du panneau Armature).

    


**Contraintes**
Pour contrôler les types de déplacement qui font sens et qui ajoutent des fonctionnalités au rig.

**Modificateurs d’objet**
La déformation de maillage peut être tout à fait concernée, il y a plusieurs modificateurs qui contribuent à contrôler cela.

**Clés de forme**
Pour prendre en charge différentes formes de cible (telles que les expressions faciales) à contrôler.

**Pilotes**
Ainsi votre rig peut contrôler un grand nombre de valeurs à la fois, aussi bien que faire que certaines propriétés se mettent à jour automatiquement suivant des changements ailleurs.

Le rigging peut être aussi avancé que le réclame votre projet, les rigs définissant effectivement l’interface utilisateur à utiliser par l’animateur, sans avoir à être concerné par les mécanismes sous-jacents.

**Exemples**

- Une armature est souvent utilisée avec un modificateur pour déformer un maillage pour l’animation de personnage.

- Un rig de caméra peut être utilisé au lieu d’animer l’objet caméra directement pour simuler des rigs de caméra du monde réel (avec une perche de rallonge, montée sur un piédestal rotatif par exemple, des effets tels que camera jitter peuvent également être ajoutés).




