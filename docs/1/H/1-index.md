# Découverte de L'animation 3D

## Logiciel d'Animation

Ce Logiciel va vous permettre d'apprendre les bases de la 3D et de réussir à la fin de faire une petite animation.

**L'animation 3D** consiste à placer des objets et des personnages dans un espace 3D et à les manipuler pour créer l'illusion du mouvement. Les objets sont conçus à partir de modèles 3D assimilés dans un environnement numérique avec des outils de modélisation 3D.

!!! info "Logiciel d'animation"

    Ces logiciels sont très complet.
    
    === "Blender <img src="images/BlenderLogo.png" width="12" height="12" />"
 
        je vous conseille **Blender** car il possède une grande variété d'outils, ce qui le rend performant pour presque tous types de production de média. Les individus et les studios dans le monde entier l'utilisent pour des projets de loisirs, des publicités, et des films long métrage.
        Logiciel libre, gratuit et extrêmement puissant, Blender est une référence pour la modélisation, l'animation et le rendu en 3D chez les professionnels du jeu vidéo et du cinéma. Un outil incontournable pour les graphistes 3D

        Voici un petit aperçu de ce que blender est capable.

        cliquez sur l'image pour lancer la vidéo

        [![texte alternatif de l'image](images/videoSpring.jpg)](https://www.youtube.com/watch?v=R7TLwKwixZA)

        Vous pouvez le télécharger sur Steam, ce n'est pas une blague : [Télécharger-ici](https://store.steampowered.com/app/365670/Blender/?l=french){ .md-button }


    === "Maya <img src="images/MayaLogo.png" width="16" height="16" />"
        
        **Maya** est un logiciel 3D professionnel qui permet de créer des personnages réalistes et des effets dignes des meilleures superproductions.
        **Maya**, qui veut dire « illusion » en sanskrit, est un logiciel commercial réputé pour les images de synthèse, développé par la société Alias Systems Corporation. Il est fortement utilisé par l'industrie du cinéma, principalement sous Linux. Par contre il est payant.

        **Maya** offre la gamme d’outils d’animation qui vous permet de donner vie à vos données 3D pour animer des doubles numériques réalistes ou de sympathiques personnages de bande dessinée.

        - Donnez vie à des personnages plus vrais que nature avec les outils d’animation intégrés.
        - Concevez des objets et des scènes 3D grâce à des outils de modélisation intuitifs.
        - Créez des effets réalistes, des explosions jusqu’à la simulation de tissus.

        [Télécharger-ici](https://www.autodesk.fr/products/maya/overview?term=1-YEAR&tab=subscription){ .md-button }
        
Dans ce logiciel nous allons nous intéresser juste sur Blender <img src="images/BlenderLogo.png" width="50" height="50" />

    

!!! warning "CONFIGURATION REQUISE"

    === "Windows"

        | Minimun | Maximum |
        |:---------|:----------|
        | Système d'exploitation : **Windows 10** | Système d'exploitation : **Windows 10** |
        | Processeur : **Intel Core i3** | Processeur : **Intel Core i9** |
        | Mémoire vive : **2 GB de mémoire** | Mémoire vive : **32 GB de mémoire** |
        | Graphiques : **2GB RAM, OpenGL 4.3** | Graphiques : **8GB RAM** |
        | Espace disque : **500 MB d'espace disque disponible** |

    === "macOS"

        | Minimun | Maximum |
        |:---------|:----------|
        | Système d'exploitation : **OS X 10.13** | Système d'exploitation : **OS X 10.13** |
        | Processeur : **Intel Core i3** | Processeur : **Intel Core i9** |
        | Mémoire vive : **2 GB de mémoire** | Mémoire vive : **16 GB de mémoire** |
        | Graphiques : **2GB RAM, OpenGL 3.3** | Graphiques : **8GB RAM** |
        | Espace disque : **500 MB d'espace disque disponible** |
    
    === "SteamOS + Linux"

        | Minimun | Maximum 
        |:---------|:----------|
        | Système d'exploitation : **Ubuntu 16.04** | Système d'exploitation : **Ubuntu 19.04** |
        | Processeur : **Intel Core i3** | Processeur : **Intel Core i9** |
        | Mémoire vive : **2 GB de mémoire** | Mémoire vive : **32 GB de mémoire** |
        | Graphiques : **2GB RAM, OpenGL 4.3** | Graphiques : **8GB RAM** |
        | Espace disque : **500 MB d'espace disque disponible** |

## Les bases de Blender

### Les different outils

Vous arriver dans un premier temp sur l'interface de Blender.

Vous commençez avec juste un cube comme modèle de base.


???+ done "Différent mécanisme de Blender"

    === "Interface utilisateur de Blender"

        Vous arriver dans un premier temp sur l'interface de Blender

        Vous commençez avec juste un cube comme modèle de base

        Interface utilisateur de Blender, divisée en six zones.

        Les quatre plus grandes zones au centre de l’interface utilisateur sont appelées éditeurs. Chaque éditeur nous présente une façon spécifique de visualiser notre projet 3D. Il existe de nombreux types d’éditeurs, mais ces quatre sont ouverts dans l’espace de travail par défaut :

        ![image](images/interfaceBlendern.png)

        1. 3D Viewport: La fenêtre d’affichage 3D est l’endroit où nous passerons la plupart de notre temps. C’est notre fenêtre sur la scène 3D. Presque toute notre modélisation 3D est faite ici.

        2. Outliner : Le Outliner répertorie tous les objets du projet et nous aide à organiser notre scène.

        3. Propriétés : Le panneau Propriétés contain les paramètres de rendu et nous permet d’ajouter des modificateurs avancés, des contraintes, des particules, de la physique et des matériaux à nos modèles 3D.

        4. Chronologie : Le La chronologie est utile lorsque nous commençons à animer. Il garde une trace des options de lecture et des images clés.

        5. Barre supérieure: La barre supérieure se trouve tout en haut de l’interface utilisateur. Le logo Blender est visible en haut à gauche. En cliquant dessus, nous aurons la possibilité de rouvrir l’écran Splash. La barre supérieure inclut les options de menu typiques que vous trouverez dans la plupart des logiciels, telles que Fichier, Modifier, etc. La fonctionnalité la plus intéressante de la barre supérieure est les nouveaux préréglages de l’espace de travail, tels que la mise en page, la modélisation, la sculpture, etc. Ces onglets nous permettront de réorganiser rapidement l’interface utilisateur pour différents flux de travail.

        6. Barre d’état : Le La barre d’état se trouve tout en bas de l’interface utilisateur. Il comprend des rappels de raccourci clavier helpful, des options d’outil, un nombre de polygones et d’autres informations utiles sur le fichier actuel. Vérifiez souvent ici pour des rappels sur le fonctionnement des outils.

        [![texte alternatif de l'image](images/Blendertuto.png)](https://www.youtube.com/watch?v=N0fA3X2nDWk)




    === "Commandes de navigation 3D de base"

        La première chose que vous devrez apprendre dans n’importe quel logiciel 3D est de savoir comment naviguer dans la fenêtre d’affichage 3D.

        Dans Blender, l’axe X est utilisé pour la largeur, l’axe Y est utilisé pour la profondeur et l’axe Z est utilisé pour la hauteur. Toutes les applications 3D utilisent les mêmes couleurs pour ces axes ; rouge pour X, vert pour Y et bleu pour Z.
        
        !!! info "information importante"

            L’axe X est toujours utilisé pour la largeur dans les logiciels 3D. Cependant, certains logiciels tels que Unity et Maya inversent les deux autres axes de sorte que l’axe Y est utilisé pour la hauteur et l’axe Z est utilisé pour la profondeur.

        La fenêtre d’affichage 3D est l’endroit où vous passerez la majorité de votre temps dans n’importe quel logiciel 3D, et Blender ne fait pas exception. Nous aurons besoin d’une souris à trois boutons pour pouvoir naviguer correctement dans la fenêtre d’affichage 3D (appuyer sur la molette de défilement vers le bas agit comme un bouton central de la souris). Le bouton central de la souris (MMB) est utilisé pour trois commandes de navigation fondamentales :

        !!! info "information importante"

            - Rotation (parfois appelée Orbite) : cliquez et maintenez MMB enfoncé et faites glisser la souris pour faire pivoter la vue.
            
            - Zoom (parfois appelé Dolly): Scroll avec la molette de défilement pour zoomer et dézoomer. Si vous souhaitez plus de précision, vous pouvez maintenir la touche ++"Ctrl"++ enfoncée (abrégée en ++"Ctrl"++), puis cliquer et maintenir MMB enfoncé et faire glisser pour effectuer un zoom avant et arrière.

            - Panoramique (parfois appelé Diapositive ou Déplacer) : appuyez sur la touche ++"⇑ Maj."++, puis maintenez enfoncé le MMB et maintenez-le enfoncé pour faire défiler la vue.

        Si jamais vous oubliez ces contrôles, vous pouvez toujours regarder la barre d’état en bas de l’écran. Là, vous verrez des rappels de ces raccourcis clavier. Alternativement, dans Blender, il y a un nouveau gadget de navigation dans le coin supérieur droit de la fenêtre d’affichage 3D. Ce gadget est particulièrement utile si vous utilisez Blender avec une tablette de dessin ou tout autre appareil avec un stylet au lieu d’une souris:

        ![image](images/shemaXYZ.png)

        Le nouveau gadget de navigation, ainsi que quelques commandes de navigation utiles
        Naviguer dans l’espace 3D peut prendre un peu de temps pour s’y habituer, mais il est essentiel que vous pratiquiez ces contrôles. Le projet Every 3D vous obligera à utiliser en permanence une combinaison de rotation, de zoom et de panoramique.

        Tant que nous parlons de l’essentiel, mentionnons rapidement comment utiliser efficacement les raccourcis clavier de Blender. Vous devrez apprendre plusieurs raccourcis clavier importants, ou raccourcis clavier. Les raccourcis clavier de Blender ne fonctionnent correctement que si le curseur de votre souris survole la fenêtre appropriée lorsque vous appuyez dessus.

        La plupart des raccourcis clavier couverts dans ce livre concernent la fenêtre d’affichage 3D, ce qui signifie que vous devez vous assurer que votre souris survole la fenêtre d’affichage 3D lorsque vous appuyez sur une touche de raccourci. Sinon, les raccourcis clavier ne feront pas ce que vous attendez d’eux.

        Donc, vous connaissez maintenant les contrôles de navigation de base dans Blender. Vous devrez connaître ces contrôles avant de pouvoir suivre les projets de ce livre. En parlant de cela, nous allons jeter un coup d’œil rapide aux projets de ce livre!

    === "Raccourcis clavier les plus courants avec Blender"

        | A quoi ça sert | Raccourcis |
        |:---------|:----------|
        | Barre d'outils (afficher/masquer) | ++"T"++ |
        | Ajouter un objet | ++"⇑ Maj."++ + ++"A"++ |
        | Supprimer un objet | ++"X"++ (ou ++"Suppr"++) |
        | Déplacer | ++"G"++ |
        | Tourner |  ++"R"++ |
        | Modifier la taille |  ++"S"++ |
        | Déplacer sur l’axe X | ++"G"++ puis ++"X"++|
        | Idem pour Y ou Z | ++"G"++ puis ++"X"++ ou ++"Z"++ |
        | Idem pour tourner ou taille | ++"R"++ ou ++"S"++ puis ++"X"++, ++"Y"++ ou ++"Z"++ |
        | Agir sur tous les axes sauf X | ++"G"++, ++"R"++ ou ++"S"++ puis ++"⇑ Maj."++ + ++"X"++ |
        | Idem pour Y ou Z | ++"G"++, ++"R"++ ou ++"S"++ puis ++"⇑ Maj."++ + ++"Y"++ ou ++"Z"++ |
        | Dupliquer | ++"⇑ Maj."++ + ++"D"++ |
        | Mouvement précis | Maintenir ++"⇑ Maj."++ |
        | Mouvement progressif | Maintenir ++"Ctrl"++ |
        | Générer un rendu | ++"F12"++ |
        | Sélectionner | ++"Clic gauche"++ |
        | Tout sélectionner | ++"A"++ |
        | Tout Désélectionner | ++"Alt"++ + ++"A"++|
        | inverser sélection | ++"Ctrl"++ + ++"I"++|

    ---
        
    Si vous avez lue les parties, vous pouvez regardé cette vidéo qui va vous permettre de manipulé toutes ces informations.

    [![texte alternatif de l'image](images/videotuto.png)](https://www.youtube.com/watch?v=N0fA3X2nDWk)

Maintenant que vous avez apppris les bases nous allons essayé de faire cette structure mécanique.

### Principe de l'animation

Pour commencer une animatien en 3D, il faut connaitre les principes de l'animation.

??? done "Les 12 principes de l'animation"

    ??? info "Compression et étirement"
        Le principe compression et étirement, considéré comme le plus important des 12, est appliqué pour donner aux objets animés l’illusion de la gravité, du poids, de la masse et de la flexibilité. Par exemple, la forme d’une balle qui rebondit s’étend au contact du sol et s’étire lorsqu’elle se déplace à haute vitesse (en devenant aérodynamique). La balle semble ainsi capter la dynamique du mouvement de manière plus réaliste, et ce, même si on exagère ses caractéristiques. Il importe cependant que l’objet conserve le même volume tout au long de sa trajectoire.

    ??? info "Anticipation"
        En suivant ce principe, l’animateur amène le spectateur à prévoir l’action sur le point de se produire en lui donnant des indices (p. ex., un personnage plie les genoux avant de sauter). On peut appliquer ce principe de façon originale en faisant réagir le personnage à un élément extérieur pour déclencher l’anticipation d’une action imminente.

    ??? info "Mise en relief"
        La mise en relief permet d’attirer l’attention sur l’élément le plus important de l’animation, qu’il s’agisse d’un événement, d’une action imminente, du personnage ou de l’ambiance. Pour cela, il faut choisir le décor en conséquence, faire la mise au point sur un personnage en particulier ou effectuer un plan rapproché pour accentuer l’expression que vous voulez montrer. Des connaissances en composition sont utiles pour mettre ce principe en application.

    ??? info "Toute l’action d’un coup et partie par partie"
        Ce principe décrit les des deux processus que l’on peut utiliser en animation. Toute l’action d’un coup suppose d’animer l’action du début à la fin, image par image. L’animation partie par partie consiste à dessiner les éléments clés – images du début, du milieu et de la fin –, de les espacer puis de remplir les espaces (intervalles). Cette technique est idéale pour insister sur le tempo et la composition tandis que dessiner toute l’action d’un coup crée une illusion de fluidité et confère aux mouvements un aspect plus naturel et donc plus réaliste. Mais rien ne vous oblige à vous limiter à l’un de deux processus, puisque vous pouvez aussi combiner les deux dans une même scène.

    ??? info "Continuité du mouvement initial et chevauchement de deux mouvements consécutifs"
        Suivre les lois de la physique pour imprimer du réalisme à l’animation… Ce principe fait référence aux mouvements qui suivent une action; par exemple, un parachute qui se pose sur le sol une fois que le personnage a atterri ou des cheveux longs qui continuent de bouger lorsque le personnage arrête brusquement de courir. Essentiellement, ce principe met l’accent sur les éléments ou composantes secondaires qui ne bougent pas à la même vitesse ou dans la même direction que le sujet principal. Il faut consacrer quelques images à l’animation de ces éléments secondaires pour faire ressortir les effets de la gravité ou du mouvement.

    ??? info "Ralentissement en début et en fin de mouvement"
        On applique ce principe en ajoutant des images au début et à la fin d’un mouvement pour illustrer l’accélération ou la décélération. Cette illustration de l’accélération ou du ralentissement du mouvement aide à communiquer les effets de la gravité sur les objets. Par exemple, une balle qui rebondit ralentit alors qu’elle approche du sommet de son arc. Ce principe permet aussi d’exprimer le poids d’un objet : une balle qui roule et s’arrête graduellement semblera légère comme une balle de ping-pong alors qu’une balle qui roule à grande vitesse et s’immobilise brusquement semblera lourde.

    ??? info "Trajectoire arquée"
        Autre principe lié aux lois de la physique, la trajectoire arquée confère de la fluidité au mouvement (p. ex., une balle lancée dans les airs suit une trajectoire arquée en raison de la gravité). La trajectoire arquée joue un rôle important dans l’animation des personnages, puisque les membres bougent selon un arc en pivotant autour de leurs articulations. On parle de mouvement composite lorsqu’une action réunit un mouvement en ligne droite et un mouvement arqué, comme un bras tendu qui décrit un arc en bougeant de haut en bas.

    ??? info "Détails secondaires en mouvement"
        L’action secondaire confère plus de vie à une scène. Par exemple, la réaction naturelle d’un personnage à un événement, comme s’épousseter après une chute, serait considérée comme une action secondaire. Le personnage pourrait aussi exprimer une émotion avec son visage, trépigner, se gratter ou simplement soupirer. Cette action ne doit pas être importante au point de détourner l’attention du reste de la scène toutefois.

    ??? info "Cohérence physique et cohérence théâtrale"
        La cohérence est un élément clé des différents aspects du mouvement qui sont essentiels en art de l’animation. La position d’un objet d’une image à l’autre détermine la lenteur ou la rapidité du mouvement. Comme nous l’avons déjà mentionné, la vitesse de déplacement d’un objet peut exprimer son poids et sa masse (une enclume en chute libre voyage plus rapidement qu’un ballon de plage). On peut aussi ajouter plus d’images pour prendre le temps d’exprimer l’émotion d’un personnage ou d’une scène. De plus, la cohérence contribue à rendre un mouvement plus réaliste et crédible.

    ??? info "Exagération"
        En animation, l’exagération sert à donner du caractère à l’image et à mettre l’accent sur une ambiance, une émotion ou une action. Elle permet également d’éviter trop de réalisme — qui rendrait l’œuvre quelque peu fade — et d’ajouter une touche d’humour pour rendre les personnages plus sympathiques et accroître la capacité du spectateur à avoir de l’empathie pour ceux-ci. Par exemple, pour donner l’impression qu’un personnage porte le poids du monde sur ses épaules, on le dessine en le faisant littéralement plier sous l’effet d’un poids réel… ou inversement, on peut exagérer son sentiment de bonheur et de liberté en donnant l’illusion qu’il est léger comme une feuille.

    ??? info "Qualité du dessin"
        Un bon dessin doit refléter les volumes de l’anatomie humaine, les ombres, les reflets, la perspective et l’attention aux détails. On donne ainsi du poids, du relief et de la profondeur à l’animation.

    ??? info "Charisme"
        Il est important de doter votre personnage de charisme et de suffisamment de magnétisme pour intéresser le spectateur et susciter chez lui des émotions. Le personnage principal doit donc être attirant et captivant. Plus il est réaliste et plus il est facile d’appliquer ce principe. Si vous suivez tous les autres principes énumérés, vous n’aurez pas de mal à appliquer celui-ci.

Voici trois étape pour faire une bonne Animation:

``` mermaid
graph LR
  A[Rendu Animation] --> B{Modelisation};
  A[Rendu Animation] --> C{Animation};
  A[Rendu Animation] --> D{Montage Vidéo};
  B{Modelisation} --> E[Création du personnage];
  C{Animation} --> F[Animer le personnage];
  D{Montage Vidéo} --> G[Filmer le personnage en action];

```

Vous allez apprendre la base de ces trois étapes, je dois vous pervenir que j'utilise des termes assez compliqué mais il faut bien ça pour apprendre le plus possible.



