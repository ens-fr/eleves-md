# Modélisation

### Introduction
 
Maintenant que nous avons vue la base de ==Blender== nous pouvons passer à la premiére étape qui est la modélisation.

La création d’une scène 3D nécessite au moins trois composantes clés : modèles, matériaux et lumières. Dans cette partie, la première de celles-ci est couverte, à savoir la modélisation. La modélisation est simplement l’art et la science de créer une surface qui imite la forme d’un objet du monde réel ou témoigne de votre imagination pour créer des objets abstraits.

### Modes

Selon le type d’objet que vous essayez de modéliser, il y a différents types de modes de modélisation. Puisque les modes ne sont pas spécifiques à la modélisation, ils sont traités dans différentes parties du manuel.

La commutation entre les modes pendant la modélisation est courante. Certains outils peuvent être disponibles dans plus d’un mode alors que d’autres peuvent être réservés à un mode particulier.

### Modes édition

Le Mode Édition est le mode principal pour la modélisation. Le Mode Édition est utilisé pour l’édition des types d’objets suivants :

- Maillages

- Courbes

- Surfaces

- Metaballs

- Objets textes

Vous ne pouvez modifier que le maillage des objets que vous êtes en train d’éditer. Pour modifier d’autres objets vous devez quitter le Mode Édition, sélectionner un autre objet et entrer en Mode Édition, ou utiliser Édition multi-objets.

---

??? info "Maillages"

    ### Maillages

    La modélisation d’un maillage commence avec une forme de Primitive de maillage (exemple : circle, cube, cylinder…). À partir de là vous pourrez commencer à éditer pour créer une forme plus grande et plus complexe.

    ### Modes de modélisation

    La Vue 3D dispose de trois modes principaux qui permettent la création, l’édition et la manipulation des modèles de maillage. Chacun des trois modes dispose d’une variété d’outils. Certains outils se trouvent dans un ou plusieurs des modes.

    Modes utilisés pour la modélisation :

    - **Mode Objet**

        Prend en charge les opérations de base telles que la création d’objets, l’assemblage d’objets, la gestion des clés de forme, les couches UV/couleurs.

    - **Mode Édition**

        Utilisé pour la majorité des opérations d’édition de maillage.

    - **Mode Sculpture**

        Au lieu de traiter des éléments de maillage individuels, prend en charge la sculpture avec des pinceaux (non couverts dans ce chapitre).
    
    ### Structure

    Pour les maillages, tout est construit à partir de trois structures de base : vertices, edges et faces (sommets, arêtes, faces).

    ![image](imagespage2/blender-Face-Edge-Vertices.png)

    ### Vertices (Sommets)

    La partie la plus élémentaire d’un maillage est le sommet qui est un point ou une position unique dans l’espace 3D. Les sommets sont représentés dans la Vue 3D en mode Édition sous forme de petits points. Les sommets d’un objet sont enregistrés sous forme de tableau de coordonnées.

    ??? tip "Astuce"

        Ne confondez pas ==l’origine d’un objet avec un sommet==. Il peut sembler similaire, mais il est plus grand et ne peut pas être sélectionné.

        ![image](imagespage2/modeling_meshes_structure_cube-example.png)


    ### Edges (Arêtes)

    Une arête relie toujours deux sommets par une ligne droite. Les arêtes sont les ==fils== que vous voyez lorsque vous regardez un maillage en vue filaire. Ils sont généralement invisibles sur l’image rendue. Elles servent à construire des faces.

    ### Faces
    
    Les faces sont utilisées pour construire la surface réelle de l’objet. Elles sont ce que vous voyez quand vous faites le rendu du maillage. Si cette zone ne contient pas de face, elle sera simplement transparente ou inexistante dans l’image rendue.

    Une face est définie par la région bordée par trois (triangles), quatre (quadrangles) ou plus de sommets (n-gons), avec une arête sur chaque côté. Les faces sont souvent abrégées en tris, quads et n-gons.

    Les triangles sont toujours plats et donc faciles à calculer. En revanche, les quadrangles ==se déforment bien== et sont donc préférés pour l’animation et la modélisation avec subdivision.

    ### Normals (Normales)

    En géométrie, une normale est une direction ou une ligne perpendiculaire à quelque chose, généralement un triangle ou une surface, mais peut aussi être relative à une ligne, une tangente à un point sur une courbe ou un plan tangent à un point sur une surface.

    ![images](imagespage2/modeling_meshes_structure_viewport.png)

    Dans la figure ci-dessus, chaque ligne bleue représente la normale à une face du tore. Les lignes sont chacune perpendiculaire à la face sur laquelle elles reposent. La visualisation peut être activée, en Mode Édition, dans le panneau ==Mesh Display Viewport Overlays.==

    ### Topology (Topologie)

    Boucles (Loops):

    ![images](imagespage2/modeling_meshes_structure_edge-face-loops.png)

    Les boucles Edge et face sont des ensembles de faces ou d’arêtes qui forment des “boucles” continues comme le montre la figure ==Boucles d’arêtes et de faces.==

    Dans l’image ci-dessus, les boucles qui ne se terminent pas par des pôles sont cycliques (1 et 3). Elles commencent et se terminent au même sommet et divisent le modèle en deux partitions. Les boucles peuvent être un outil rapide et puissant pour travailler avec des régions spécifiques et continues d’un maillage et sont une condition préalable à l’animation organique de personnages. Pour une description détaillée de la façon de travailler avec des boucles dans Blender, voir: Edge Loop Selection.

    ??? info "Astuce"

        Notez que les boucles (2 et 4) ne font pas le tour de tout le modèle. Les boucles s’arrêtent à ce qu’on appelle des pôles, car il n’y a pas une façon unique de continuer une boucle à partir d’un pôle. Les pôles sont des sommets qui sont reliés à trois, cinq ou plusieurs arêtes. Par conséquent, les sommets connectés à exactement un, deux ou quatre bords ne sont pas des pôles.        

??? info "Courbes"

    ### Courbes

    Les Courbes et les ==Surfaces== sont des types d’objets particuliers de Blender. Elles sont exprimées par des fonctions mathématiques (interpolation) plutôt que par une interpolation linéaire entre une série de points.

    Blender offre à la fois les ==Bézier== et les **NURBS.** Les courbes de Bézier et les courbes NURBS et les surfaces sont toutes définies par un ensemble de “points de contrôle” (ou “sommets de contrôle”) qui définissent un “polygone de contrôle”.

    ![images](imagespage2/modeling_curves_introduction_logo.png)
    `Logo de Blender fait de courbes de Bézier.`

    Les courbes de Bézier et les NURBS sont nommées d’après leur définition mathématique, et le choix entre elles est souvent plus une affaire de la manière dont elles sont calculées en coulisse que de la manière dont elles apparaissent d’un point de vue du modéliste. Les courbes de Bézier sont généralement plus intuitives parce qu’elles commencent et se terminent aux points de contrôle que vous avez définis, mais les courbes NURBS sont plus efficientes pour les calculs par ordinateur dans les cas où il y a de nombreux torsions et virages dans une courbe.

    L’avantage majeur de l’utilisation des courbes à la place de maillages polygonaux est que les courbes sont définies par moins de données et ainsi peuvent produire des résultats avec moins de mémoire et d’espace d’enregistrement au moment de la modélisation. Cependant, cette approche procédurale des surfaces peut augmenter les exigences au moment du rendu.

    Certaines techniques de modélisation, telles que ==l'extrusion d’un profil le long d’un chemin==, sont possibles uniquement avec les courbes. D’autre part, dans l’utilisation des courbes, le contrôle au niveau des sommets est plus difficile et si un contrôle fin est nécessaire, ==l'édition de maillage== pourrait être une meilleure option pour la modélisation.

    Les courbes de Bézier sont les courbes les plus communément utilisées pour la conception de lettres et de logos.

    Elles sont aussi largement utilisées en animation, à la fois comme objets à déplacer (voir contraintes ci-dessous) et comme ==F-Curves== pour modifier les propriétés d’objets comme une fonction du temps.

??? info "Surfaces"

    ### Surfaces

    Les Courbes sont des objets 2D, et les surfaces sont leur extension 3D. Notez cependant que dans Blender vous n’avez que des surfaces NURBS, pas de surfaces Bézier (vous avez cependant le type nœud Bézier; voir ci-dessous) ou de surfaces polygonales (mais pour celles-ci, vous avez les maillages!). Bien que les courbes et les surfaces partagent le même type d’objet (aussi avec les objets texte…), ce n’est pas la même chose; par exemple, vous ne pouvez pas avoir dans le même objet à la fois des courbes et des surfaces.

    ![images](imagespage2/modeling_surfaces_introduction_nurbs-surface.png)

    Du fait que les surfaces sont en 2D, elles ont deux axes d’interpolation, U (comme pour les courbes) et V. Il est important de comprendre que vous pouvez contrôler les règles d’interpolation (knot, ordre, résolution) indépendamment pour chacune de ces deux dimensions (les champs U et V pour tous ces réglages, bien sûr).

    Il se peut que vous vous demandiez “mais la surface semble être en 3D, pourquoi est-elle seulement en 2D ?”. Pour être en 3D, l’objet a besoin d’avoir un ==volume==, et une surface, même quand elle est fermée, n’a pas de volume ; elle est infiniment fine. Si elle avait un volume la surface aurait une épaisseur (sa troisième dimension). Ainsi, c’est seulement un objet 2D, et a seulement deux dimensions d’interpolation ou axes ou coordonnées (si vous connaissez un peu les maths, pensez à la géométrie non euclidienne – eh bien, les surfaces sont simplement des plans 2D non euclidiens). Pour prendre un exemple plus près du “monde réel”, vous pouvez rouler une feuille de papier pour créer un cylindre ; eh bien, même si cela dessine un “volume”, la feuille elle-même restera un objet (presque…) 2D !

    En fait, les surfaces sont très semblables aux résultats que vous obtenez avec ==l'extrusion d’une courbe.==

    ### Visualisation

    Il n’y a presque pas de différence avec les courbes NURBS, sauf que la direction U est indiquée par des lignes de grille jaunes, et celle de V est matérialisée par des lignes de grille roses, comme vous pouvez le voir dans Fig. ==Surface NURBS en Mode Édition.==

    Vous pouvez **cacher et révéler** les points de contrôle tout comme avec les courbes.

    ### Conversion

    Comme il n’y a que des surfaces NURBS, il n’y a pas de conversion interne ici.

    Cependant, il existe une conversion ==externe==, de la surface au maillage, qui ne fonctionne qu’en Mode Objet. Elle transforme un objet de surface en un objet mesh, en utilisant les résolutions de surface dans les deux sens pour créer des faces, des arêtes et des sommets.

??? info "Metaball"    

    ### Metaball

    Les objets Metaball sont des surfaces implicites, ce qui signifie qu’ils ne sont pas définis explicitement par des sommets (comme le sont les maillages) ou par des points de contrôles (comme le sont les surfaces) : ils existent procéduralement. Les objets Meta sont littéralement des formules mathématiques qui sont calculées à la volée par Blender.

    Une caractéristique visuelle très distincte des metas est qu’elles ont des formes “arrondies” qui évoquent du mercure liquide ou de la glaise. De plus, quand deux objets meta se rapprochent, ils commencent à interagir l’un avec l’autre. Ils se ==mélangent ou fusionnent==, comme le font les gouttelettes d’eau, spécialement en apesanteur (ce qui, de cette manière, les rend très pratiques pour modéliser les flux d’eau quand vous ne voulez pas faire une simulation de fluide). Si, par la suite, ils s’éloignent les uns des autres, ils reprennent leur forme d’origine.

    Chacun d’entre eux est défini par sa propre structure mathématique sous-jacente, et vous pouvez à n’importe quel moment passer de l’un à l’autre en utilisant le panneau Active Element.

    Généralement les objets Meta sont utilisés pour des effets spéciaux ou comme base pour la modélisation. Par exemple, vous pouvez utiliser une collection de metas pour construire la forme initiale de votre modèle et la convertir ensuite en un maillage pour future une modélisation ou une sculpture. Les objets Meta sont aussi très efficients pour le lancer de rayons.

    ??? warning "Avertissement"

        Les noms des objets Meta sont très importants, car ils définissent des familles, et seuls les objets d’une même famille interagissent les uns avec les autres. Contrairement aux autres types d’objet, même l’édition (transformations) en Mode Objet affectera la géométrie générée dans les familles modifiées.

    ### Visualisation

    En Mode Objet, le maillage calculé est affiché, avec un **anneau de sélection** noir.

    ![images](imagespage2/modeling_metas_introduction_influence-selection.png)

    En Mode Édition (Fig. **Meta Ball en Mode Édition.**), une meta est affichée comme un maillage (soit en mode ombré soit en mode fil de fer noir, mais sans aucun sommet bien sûr), avec deux cercles colorés : un rouge pour la sélection (rose si sélectionné), et un vert pour un contrôle direct de la rigidité du meta (vert clair si actif). Notez que, à l’exception de la ==transformation Scale==, avoir le cercle vert surligné est équivalent à avoir le rouge surligné.

??? info "Objet textes" 

    ### Objet textes

    Les objets Text contiennent du texte, et partagent le même type d’objet que les courbes et les surfaces, étant donné que les fontes sont des données vectorielles (elles sont faites de courbes).

    Blender utilise un système de police (Font System) pour gérer la correspondance des codes de lettre à la géométrie les représentant dans la Vue 3D. Ce système de police a sa propre police intégrée, mais elle peut aussi utiliser des polices externes, y compris des polices PostScript Type 1, OpenType et TrueType. Et d’ailleurs, elle peut utiliser n’importe quel objet existant dans le fichier blend courant comme lettre.

    ![images](imagespage2/modeling_texts_introduction_examples.png)

    Les objets Text vous permettent de créer et de faire le rendu de texte 2D ou 3D, avec diverses options d’agencement avancées, telles que la justification et les cadres. Par défaut, les lettres sont simplement des surfaces pleines et plates, exactement comme n’importe quelle courbe 2D fermée. Mais, au même titre que les courbes, vous pouvez les extruder et leur appliquer des ==modificateurs== (par ex. les faire ==suivre une courbe==).

    Le texte dans Blender peut être présenté selon certaines manières relativement avancées, en définissant des colonnes et des blocs de texte, en utilisant différents alignements, etc.

    Ces fonctions sont d’un concept similaire à ce que vous pouvez trouver dans les logiciels DTP (comme ==Scribus==), bien qu’à un niveau très basique actuellement.

    ??? tip "Astuce"

        Vous pouvez convertir un objet Text, soit en courbe, soit directement en maillage,en utilisant ==Convert== en Mode Objet.

    ??? info "Note"

        Un maximum de 50.000 caractères est permis par objet Text. Cependant, soyez attentif que plus un texte possède de caractères, plus lente sera la réponse interactive de l’objet.
