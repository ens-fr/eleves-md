﻿# À la découverte de soi...

![Mont fuji](https://geo.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2FGEO.2Fvar.2Fgeo.2Fstorage.2Fimages.2Fmedia.2Fla-pagode-chureito-dans-le-sanctuaire-arakura-sangen-a-fujiyoshida-devant-le-mont-fuji.2F2049587-1-fre-FR.2Fla-pagode-chureito-dans-le-sanctuaire-arakura-sangen-a-fujiyoshida-devant-le-mont-fuji.2Ejpg/1150x647/background-color/ffffff/quality/70/japon-le-mont-fuji-montagne-sacree.jpg)


!!! note "🎀 Mme Boin Sylvie Sophrologue 🎀"
    Sophrologie et Thérapies Alternatives !

    - **enfants**
    - **ados**
    - **adultes**


!!! warning "Attention !"
    La sophrologie est une méthode qui permet de pratiquer en toute autonomie.
    
    Certaines difficultés peuvent nécessiter un suivi médical ou psychologique auxquels la pratique de la sophrologie ne saurait **en aucun cas se substituer**.
    Ces différents suivis peuvent s'effectuer conjointement.

    
## C'est quoi la sophrologie ?

    
Créée en **1960** par le neuropsychiatre [Alfonso Caycedo](https://alfonsocaycedo.com/biographie/), la sophrologie s'inspire des techniques de visualisation positive, de détente  musculaire et des différentes techniques de respiration.
    
Cet ensemble de techniques orientales et occidentales permet d'acquérir une meilleure connaissance de soi, de développer ses capacités et d'affronter les défis au quotidiens avec sérénité.
    
La sophrologie est une technique corporelle qui permet d'établir une meilleure relation entre le corps et le mental.
    
## Pour qui ?
 
Pour les femmes, les hommes et les enfants, **accessible à tous**, ses exercices sont simples et faciles à exécuter.

Elle s'adapte aux besoins et aux contraintes de tous les âges de **4ans à 99ans**. La pratique de la sophrologie ne se substitue à aucun traitement médical. Elle ne requiert aucune aptitude physique particulière, cependant en cas de doute, il est conseillé de demander un avis médical.

## Pourquoi ?
 
Elle aide à encrer **durablement** un équilibre physique, mental et émotionnel et les ressources nécessaires pour faire face aux différentes situations difficiles du quotidiens ou s'adapter aux changements.
    
C'est aussi un **outil pédagogique** qui permet une détente, un relâchement des tensions et une réactivation des ressources personnelles pour mieux se préparer à vivre la pression du quotidien.

!!! example "🦋 La sophrologie pour... 🦋"

    - ➡️ Confiance en soi
    - ➡️ Gestion du stress
    - ➡️ Phobie scolaire
    - ➡️ Preparation Mentale
    - ➡️ Examens (Brevet/Bac/oral...)
    - ➡️ Competions sportives
    - ➡️ Gestion des émotions
    - ➡️ Acouphènes
    - ➡️ Developpement Personnel
    - ➡️ Gestion du sommeil
    - ➡️ Lâcher prise
    - ➡️ Aide à la concentration
    - ➡️ Burn out
    - ➡️ Accompagnement d'un comportement pathologique



!!! tldr "📌 Mon mode de travail 📌"
     Les séances individuelles

    - Les **séances individuelles** de sophrologie permettent de personnaliser les techniques proposées en fonction de chacun. Au premier rendez-vous un **bilan est effectué afin de déterminer l'axe de travail**. À l'issu de cet entretien seront définis le nombre de séances en accord avec le sophronisant.

    - Une séance **dure une heure**, avec une sophronisation de base, état entre veille et sommeil, technique de respiration de détente musculaire et de visualisation positive.

    - la séance se déroule soit assis soit debout. Ces différents exercices sont suivis d'un temps de pause, d'intégration des phénomènes qui se manifestent en vous.

    - Une **désophronisation** et un partage sur les vécus et ressenti de chacun.

![diplôme quelconque](https://www.alain-giraud.com/medias/album/diplome-rncp-sophrologie-alain-giraud.jpg)

!!! faq "Les séances en groupe"

    - Les **séances en groupe** de Sophrologie sont proposées en fonction d'un axe de travail déterminé par le groupe.
    Sachant que les séances de groupe sont plus destinées aux entreprises, associations… **puisque je me déplace pour faire la séance**.

    - Une séance **dure une heure**,  avec une sophronisation de base, état entre veille et sommeil, technique de respiration de détente musculaire et de visualisation positive.

    - La séance se déroule soit assis soit debout. Ces différents exercices sont suivis d'un temps de pause, d'intégration des phénomènes qui se manifestent en vous.

    - Une **désophronisation** et un partage sur les vécus et ressenti de chacun.

![Citation Lao Tseu](https://medias.psychologies.com/storage/images/culture/les-phrases-de-sagesse/phrases-de-sagesse-bonheur/il-n-y-a-point-de-chemin-vers-le-bonheur-le-bonheur-est-le-chemin/2219193-1-fre-FR/Il-n-y-a-point-de-chemin-vers-le-bonheur-le-bonheur-est-le-chemin.jpg)

!!! tip "🔗 Mes partenariats 🔗"
     ㅤ
    >**E.D.F**

    >_Site :_ [ACCÉDER](https://www.edf.fr/centrale-nucleaire-tricastin/presentation)
    
    >_Tel : 04 75 50 37 10_


    >Résidence Alphonce Daudet

    >_Site :_ [ACCÉDER](https://annuaire.action-sociale.org/?p=foyer-residence-a-daudet-840006498&details=caracteristiques)  
    
    >_Tel : 04 90 30 17 64_


    >Lycée Lucie Aubrac  

    >_Site :_ [ACCÉDER](https://www.lyc-bollene.ac-aix-marseille.fr/spip/)

    >_Tel : 04 32 80 31 90_


    >Le Cavalier Fou  

    >_Site :_ [ACCÉDER](http://bollene-echecs.wifeo.com/)

    >_Tel : 09 83 62 13 87_

    >Collège Paul Eluard  

    >_Site :_ [ACCÉDER](https://www.clg-eluard-bollene.ac-aix-marseille.fr/spip/)

    >_Tel : 09 83 62 13 87_


    > Ecole municipale des sports Bollène  

    >_Site :_ [ACCÉDER](https://ville-bollene.fr/fr/)

    >_Tel : 06 16 99 85 25_  

    >_Lien vers article Facebook :_ [ici](https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2F157390190979267%2Fposts%2F4692570400794534%2F%3Fsfnsn%3Dscwspmo)

!!! done "💶 Tarifs 💶"

     Tarifs consultations :

    |               |Première consultation          |Autres consultations                       
    |----------------|-------------------------------|-----------------------------|
    |Tarifs          |60€                            |55€                          |
    |Temps           |1h30                           |1h                           |

    ```mermaid
    graph LR
    A[Paiement par] --> B(Chèque)
    A --> C(Espèces)
    ```

     Pour les entreprises :

     - Étude personnalisée  
     - Devis sur mesure

     Mutuelles : 

     Liste des mutuelles prenant en charge les séances de Sophrologie :  
     Celles-ci sont disponible sur le site de  [La Chambre Syndicale De La Sophrologie](https://www.chambre-syndicale-sophrologie.fr/mutuelle-sophrologie-sophrologue/).


![Logo chambre syndicale sophro](https://www.chambre-syndicale-sophrologie.fr/wp-content/uploads/2017/09/ChbreSyndicaleSOPHRO-logo2017_HD.png)

!!! danger "☎️ Contact ☎️"

    ![phot%20sylvie_edited.jpg](https://static.wixstatic.com/media/05eede_51e791c8a8f24029b6d89351f50885d7~mv2.jpg/v1/fill/w_317,h_387,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/phot%2520sylvie_edited.jpg)

    ![](https://static.wixstatic.com/media/035244_19984e4b06dc4b148048731e3dcf1011~mv2.png/v1/fill/w_26,h_26,al_c,usm_0.66_1.00_0.01,enc_auto/035244_19984e4b06dc4b148048731e3dcf1011~mv2.png) - Téléphone : 06 30 31 74 33

    ![](https://static.wixstatic.com/media/035244_e6b753ae08974e7785a153ccc10763b4~mv2.png/v1/fill/w_26,h_26,al_c,usm_0.66_1.00_0.01,enc_auto/035244_e6b753ae08974e7785a153ccc10763b4~mv2.png)        - Email : [sylviegonin@live.fr](mailto:sylviegonin@live.fr)

    ![](https://static.wixstatic.com/media/035244_ca183d1f593a4c41b9d034b9099ac2b5~mv2.png/v1/fill/w_26,h_26,al_c,usm_0.66_1.00_0.01,enc_auto/035244_ca183d1f593a4c41b9d034b9099ac2b5~mv2.png) - Adresse : 15 chemin de la source 84500 Bollène

    ![](https://static.wixstatic.com/media/035244_4e8624c8d90f44689d47ba6c14eeb42b~mv2.png/v1/fill/w_26,h_26,al_c,usm_0.66_1.00_0.01,enc_auto/035244_4e8624c8d90f44689d47ba6c14eeb42b~mv2.png) - Lien vers Google Maps :  [ici](https://www.google.com/maps/place/Mme+BOIN+Sylvie+SOPHROLOGUE+Certifi%C3%A9e+RNCP/@44.3059078,4.7536949,15z/data=!4m5!3m4!1s0x0:0x4a00e14e92310fa3!8m2!3d44.3058968!4d4.7536369)


 









