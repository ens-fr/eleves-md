# Muse

---
Plutôt fan de rock ? Alors Muse va vous plaire !

Sur mon site, vous saurez l'essentiel de Muse et à la fin, vous brûlerez d'envie de les écouter !

Je vous laisse découvrir pourquoi ce groupe de musique est celui que je préfère depuis maintenant 17 ans et qu'aucun n'a jamais détrôné sa place de numéro 1...🥇

## Muse...c'est qui ?

Groupe de rock britannique, Muse est un trio composé de [Matthew Bellamy](https://fr.wikipedia.org/wiki/Matthew_Bellamy) (chant, guitare, piano), [Christopher Wolstenholme](https://fr.wikipedia.org/wiki/Christopher_Wolstenholme) (basse, harmonica, chant, chœurs) et [Dominic Howard](https://fr.wikipedia.org/wiki/Dominic_Howard) (batterie, percussions).

Le trio : Dominic Howard à gauche, Christopher Wolstenholme au milieu et Matthew Bellamy à droite.

![trio](https://www.virginradio.fr/wp-content/uploads/virginradio/2022/03/media-1268.jpg){width=500}

---
### Pour en savoir plus...

|==**Informations générales**==|         |
|------------------|------------------|
| Pays d'origine | 🇬🇧 Royaume-Uni |
| Genre musical | Rock alternatif, rock progressif, space rock, new prog, electro rock, art rock, pop |
| Années actives | Depuis 1994 |
| Influences | Jeff Buckley, Rage Against the Machine, Deus, Jimi Hendrix, Smashing Pumpkins, Frederic Chopin, Sergueï Rachmaninov |
| Site officiel | [muse.mu](http://muse.mu/){ .md-button } |
| Membres | Matthew Bellamy (chanteur) ; Dominic Howard (bassiste) ; Christopher Wolstenholme (batteur) |
| Logo de Muse | ![logo](https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Muse_logo.svg/langfr-1280px-Muse_logo.svg.png){width=220}


!!! summary "Leur histoire (très) résumée"

     Les trois artistes se sont rencontrés dans les années 1990 au Teignmouth Community College, à Teignmouth (dans le Devon au Royaume-Uni). Ils appartiennent alors à des formations musicales différentes.   
     En 1992, âgés de treize à quatorze ans, ils créent le groupe Gothic Plague. Après sa création, le groupe change fréquemment de nom, comme Carnage Mayhem, Fixed Penalty, Rocket Baby Dolls, ou encore Young Blood (cet ordre est incertain, puisque Muse a donné des interviews contradictoires à ce sujet...🤷‍♀️ )


    Muse a réalisé neuf albums depuis ses débuts. 
    _Absolution_, leur 3ème album qui est sorti en 2003, provoque un succès planétaire et permet au groupe de commencer à percer aux USA et en France où il atteint la première place des ventes !👍
    Les chansons ayant le plus de succès dans cet album sont [Time is Running Out](https://www.youtube.com/watch?v=O2IuJPh6h_A), [Hysteria](https://www.youtube.com/watch?v=3dm_5qWWDV8) et [Stockholm Syndrome](https://www.youtube.com/watch?v=gXN9acC9edU).


!!! tip "Un peu d'écoute...🎵"

    === "Les plus vues sur Youtube"

         1. [Uprising](https://www.youtube.com/watch?v=w8KQmps-Sog) comptabilisant 234 M de vues.              
        2. [Starlight](https://www.youtube.com/watch?v=Pgum6OT_VH8) comptabilisant 159 M de vues.
        3. [Madness](https://www.youtube.com/watch?v=Ek0SgwWmF9w) comptabilisant 153 M de vues.

    === "Moins connues mais toujours géniales !"

        - [Unsustainable](https://www.youtube.com/watch?v=EF_xdvn52As)
        - [Citizen Erased](https://www.youtube.com/watch?v=KScEW5a_KGY)
        - [Isolated System](https://www.youtube.com/watch?v=VXPoJAyeF8k)


??? question "Le saviez-vous ?"

    Le choix du nom du groupe "Muse" fait référence à 2 interprétations :

    -  Dans la mythologie grecque et romaine, au nombre de 9, les muses sont les filles de Zeus et de Mnémosyne désignant la mémoire.

    - En littérature, les muses inspirent un écrivain ou un poète. Elles sont bien souvent les maîtresses des artistes...


!!! success "WOW !"

    - À l'occasion des Jeux olympiques de Londres de 2012, le groupe est invité à composer l'hymne officiel de l’événement (la chanson [Survival](https://www.youtube.com/watch?v=UcOUJM08bYk)).
    
    - Le groupe britannique a vendu près de **30 millions d'albums** dans le monde.
    
    - Ils atteignent **2,42 milliards de vues** sur YouTube en mars 2022. 
    
    - Régulièrement récompensés par des prix, ils remportent notamment le Grammy Award dans la catégorie de « Meilleur album rock » en 2011 et en 2016.

!!! note "Muse au cinéma...🎞️" 

    - En 2003, le réalisateur Alexandre Aja reprend le morceau [New Born](https://www.youtube.com/watch?v=qhduQhDqtb4) pour son film Haute Tension. Ce sera également le générique de fin.

    - En 2013, [Isolated System](https://www.youtube.com/watch?v=VXPoJAyeF8k) et [Follow Me](https://www.youtube.com/watch?v=5kH0OEJxUlE) sont deux chansons utilisées dans le film World War Z.

???+ tip "Envie de voir un concert de Muse ?"
     - Pour voir un ancien live de leur album _Drones_, c'est [ici](https://www.youtube.com/watch?v=Gii3EX0OamQ) !
     - Pour voir un ancien live de leur album _Simulation Theory_, c'est [ici](https://www.youtube.com/watch?v=E3zesEyUgIY&t=47s) !

!!! note "Anecdotes..."

    !!! example "Matthew et les guitares...🎸"

        Matt détient le record de guitares explosées pendant une même tournée ! 
        Il en a détruites 140 au cours d’Absolution Tour en 2004...
        Cette performance lui a valu d’entrer dans le Guinness Book des Records en 2010 🏆

        Sa passion pour la destruction de guitares ne date pas d’hier: sur la tournée accompagnant son deuxième album Origin of Symmetry (2001), Muse dépensait déjà 150 £ par concert en guitares anéanties. Le groupe avait même rebaptisé pour l’occasion cette tournée Origin of Chaos… 💥

        ![gif](http://www.pixbear.com/wp-content/uploads/2016/06/muse_smash_guitar.gif){width=350}

    !!! example "Matthew...BEEEEH 🐑"

        Le chanteur de Muse possède 160 moutons ! Et oui, Matthew fait parti d'un groupe de rock reconnu mondialement et est fermier à ses heures perdues...👨🏻‍🌾
    
    !!! example "Beurk le playback !"

        Les membres de Muse n’aiment pas le playback. Et lorsqu’une chaine de télévision en exige d’eux, ils tiennent à ne duper personne et préfèrent faire preuve d'humour :

        En 2009, sur une chaîne de télévision italienne, ils échangent carrément leurs places ! Ils exagèrent leurs mouvements et ne jouent même pas en rythme pour interpréter [Uprising](https://www.youtube.com/watch?v=w8KQmps-Sog).   
        La présentatrice s’adressera au batteur Dominic Howard, qui avait pris la place du chanteur, sans se rendre compte du désordre ! 😉

        Pour voir la bonne blague du groupe c'est [par là](https://www.youtube.com/watch?v=U7LT3FtPtbc) !

    !!! example "Dominic, un batteur particulier"

        Dominic, ou Dom pour les intimes, n'est pas un batteur ordinaire. En effet, sa batterie est agencée de manière inversée, ce qui fait qu’il joue avec la main gauche dominante ! ✌️


!!! tip "Et aujourd'hui ?"

    - Un nouvel album promis le 26 août 2022 avec 2 singles sortis cette année : [Won't Stand Down](https://www.youtube.com/watch?v=d55ELY17CFM) en janvier et [Compliance](https://www.youtube.com/watch?v=QP3zRBtgvJo) en mars !

    - Le 9ème album _Will of the People_  sera composé de 10 morceaux et une tournée mondiale aura peut-être lieu... Patience !

    ![album](https://resize1-rfm.lanmedia.fr/f/webp/rcrop/920,614/img/var/rfm/storage/images/news/muse-leur-prochain-album-will-of-the-people-sortira-le-26-aout-2022-25895/370354-1-fre-FR/Muse-Leur-prochain-album-Will-Of-The-People-sortira-le-26-aout-2022.jpg){width=350}

    La pochette du 9ème album de Muse, _Will of the People_



Maintenant que vous savez l'essentiel de Muse, je vous laisse avec une petite liste des chansons du groupe à écouter pour être prêt pour leurs prochains concerts 😄 :

- [ ] [New Born](https://www.youtube.com/watch?v=qhduQhDqtb4)
- [ ] [Undisclosed Desires](https://www.youtube.com/watch?v=R8OOWcsFj0U)
- [ ] [Dig Down](https://www.youtube.com/watch?v=b4ozdiGys5g)
- [ ] [Sunburn](https://www.youtube.com/watch?v=N9SZaOJEWXU)
- [ ] [Map of the Problematique](https://www.youtube.com/watch?v=Nw5AMCEiZms)
- [ ] [Pressure](https://www.youtube.com/watch?v=h2eKImKZviw)
- [ ] [Dead inside](https://www.youtube.com/watch?v=I5sJhSNUkwQ)
- [ ] [Supremacy](https://www.youtube.com/watch?v=avM_UsVo0IA)
- [ ] [Thought Contagion](https://www.youtube.com/watch?v=QQ_3S-IQm38)
- [ ] [Starlight](https://www.youtube.com/watch?v=Pgum6OT_VH8)
- [ ] [Supermassive Back Hole](https://www.youtube.com/watch?v=Xsp3_a-PMTw)
- [ ] [Unintended](https://www.youtube.com/watch?v=i9LOFXwPwC4)
- [ ] [Plug In Baby](https://www.youtube.com/watch?v=dbB-mICjkQM)
- [ ] [Algorithm](https://www.youtube.com/watch?v=X8f5RgwY8CI)
- [ ] [Animals](https://www.youtube.com/watch?v=tFG_5PBl2K8)
- [ ] [Bliss](https://www.youtube.com/watch?v=eMqsWc8muj8)
- [ ] [Unnatural Selection](https://www.youtube.com/watch?v=T23AY5gYhpE)

etc...

Bonne écoute ! 🎶

![gif](https://c.tenor.com/Cvu5s-KoATgAAAAC/muse.gif){width=400}



 















---

 


