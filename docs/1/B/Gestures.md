# Gestures

---

Gestures in Elden Ring, are a common feature among the Souls game. Gestures are similar to Emotes in other games, it's an action that can be picked from a list where you can choose so that your character expresses it. **Waving**, **Greeting**, **Sitting**, **Clapping**, etc, are the most commonly used Gestures. You can bind a Gesture to a Shortcut, but you can execute any gesture binded or unbinded from the Gestures menu. Some gestures may emit a sound or vocal expression.

## Gestures & Multiplayer

Gestures are most commonly used as means of communication between players in an Online game.  
During invasions some players like to greet their opponent before engaging into combat with them. Others like to taunt, but be careful - some players may attack you while you execute your gesture. You may consider being at a safe distance before you make your gesture.  
During coop, gestures may also be used to signal your allies. For example, pointing to a hidden chest or item for the host to collect.

## Gestures & Secrets

In some rare cases, Gestures may trigger certain events that will take you to new quests, items, places, and even new bosses or enemies. It's important to pay attention to details as some of them may hint of a gesture that may trigger some event. For example, if you find a statue posing as a gesture you recognize, you may consider trying to play the same gesture and wait for a minute. If an event triggers, there will be an indication such as a sound or a change in the environment. 


---

### Elden Ring Gestures

---

|My Lord|Nod In Thought|Finger Snap|Rapture|Warm Welcome|Extreme Repentance|
|:-:|:-:|:-:|:-:|:-:|:-:|
|![](imagesgestures/gesture_1_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_2_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_3_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_4_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_5_elden_ring_wiki_guide_150px.png)|![](imagesgestures/gesture_6_elden_ring_wiki_guide_150px.png){ width=100px }|
|Given by Boc The Seamster after giving him the Gold Sewing Needle at the East Capital Rampart Site of Grace in the Leyndell Royal Capital |Given by Sorceress Sellen when first met|Given by Merchant Kale when asked about the howling at Mistwood Ruins|Obtained by interacting with the two fingers besides Enia|Automatically received|Obtained by befriending Patches, then fighting him once more, letting him take you to low health then using the Grovel For Mercy gesture when he tells you to|

|Dejection|Erudition|Wave|Grovel for Mercy|Patches' Crouch|Outer Order|
|:-:|:-:|:-:|:-:|:-:|:-:|
|![](imagesgestures/gesture_7_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_8_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_9_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_10_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_11_elden_ring_wiki_guide_150px.png)|![](imagesgestures/gesture_12_elden_ring_wiki_guide_150px.png){ width=100px }|
|Automatically received|Give Thops the Academy Glintstone Key. (Used with Glintstone crowns to solve some puzzle towers)|Automatically received|Obtained after defeating Patches at the Murkwater Cave| |Given to the player when talking to Melina in the Minor Erdtree Church|

|Casual Greeting|Rallying Cry|Crossed Legs|Inner Order|Strength!|Heartening Cry|
|:-:|:-:|:-:|:-:|:-:|:-:|
|![](imagesgestures/gesture_13_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_14_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_15_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_16_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_17_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_18_elden_ring_wiki_guide_150px.png){ width=100px }|
|Obtained after summoning Great Horned Tragoth before the boss fight Magma Wyrm Makar or within Starscourge Radahn arena|Automatically received|Obtained by interacting with a corpse on a large piece of debris, South of Stormhill Shack.|Given to players when talking to D's twin brother at the Siofra Aqueduct.|Can be found in the cliff at the starting area, Past defeating Soldier of Godrick|Given to player when talking to Witch-Hunter Jerren at Redmane Castle.|

|Rest|Golden Order Totality|As You Wish|By My Sword|Sitting Sideways|The Ring|
|:-:|:-:|:-:|:-:|:-:|:-:|
|![](imagesgestures/gesture_19_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_20_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_21_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_22_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_23_elden_ring_wiki_guide_150px.png)|![](imagesgestures/gesture_24_elden_ring_wiki_guide_150px.png){ width=100px }|
|Automatically received|Given by Goldmask after telling him the solution to the riddle.|Given by Hyetta when given a Shabriri Grape at Church of Irith.|Found in the Fortified Manor in Leyndell Royal Capital|Given by Roderika in Stormhill Shack|Pre-ordering bonus for official release or after killing a boss with someone who received the gesture via pre-order (but not with someone who received the gesture from someone else).|

|Point Forwards|Hoslow's Oath|Dozing Cross-Legged|Bow|Point Upwards|Fire Spur Me|
|:-:|:-:|:-:|:-:|:-:|:-:|
|![](imagesgestures/gesture_25_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_26_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_27_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_28_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_29_elden_ring_wiki_guide_150px.png)|![](imagesgestures/gesture_30_elden_ring_wiki_guide_150px.png){ width=100px }|
|Automatically received|Obtained when invading Juno Hoslow for the first time at the Mountaintops of the Giants|Obtained by interacting with a corpse on a roof in Stormveil Castle near the Rampart Tower Site of Grace. Video Location|Automatically received|Automatically received|Found in a Fire Monk camp south of Church of Vows|

|Spread Out|Polite Bow|Point Downwards|Bravo!|Balled Up|My Thanks|
|:-:|:-:|:-:|:-:|:-:|:-:|
|![](imagesgestures/gesture_31_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_32_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_33_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_34_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_35_elden_ring_wiki_guide_150px.png)|![](imagesgestures/gesture_36_elden_ring_wiki_guide_150px.png){ width=100px }|
|Obtained by buying prawns from the blackguard at the Boilprawn Shack.|Received by speaking to Finger Maiden Thorelina inside Redmane Castle, before the boss fight with Starscourge Radahn|Automatically received|Obtained by interacting with the message beneath where White-Face Varre was originally, after defeating your first demigod and before talking to him at Rose Church|Obtained from Renalla after rebirthing for the first time.|Dropped at the end of the hallway in Volcano Manor after defeating Rykard|

|Beckon|Jump for Joy|What Do You Want?|Curtsy|Wait!|Reverential Bow|
|:-:|:-:|:-:|:-:|:-:|:-:|
|![](imagesgestures/gesture_37_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_38_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_39_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_40_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_41_elden_ring_wiki_guide_150px.png)|![](imagesgestures/reverential_bow_gestures_elden_ring_wiki_guide_160px.png){ width=100px }|
|Automatically received|Automatically received|Obtained when interacting with Ensha for the first time at Roundtable Hold.|Given by Roderika after becoming a spirit tuner, leveling a spirit to at least +4 and talking to her after.|Automatically received|Acquired by letting Alberich complete the gesture after he spawns in the roundtable.|

|Prayer|Calm Down!|Fancy Spin|Desperate Prayer|Triumphant Delight|
|:-:|:-:|:-:|:-:|:-:|
|![](imagesgestures/gesture_43_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_45_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_46_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_47_elden_ring_wiki_guide_150px.png){ width=100px }|![](imagesgestures/gesture_42_elden_ring_wiki_guide_150px.png){ width=100px }|
|Obtained when meeting Brother Corhyn for the first time at Roundtable Hold.|Obtained from returning to Patches in Murkwater Cave after being ensnared by his chest|Obtained from summoning the jellyfish spirit beside another spirit jellyfish within the Stargazer's Ruins|Obtained from Gowry after purchasing Pest Threads and exhausting dialogue.|Obtained as you help Iron Fist Alexander get unstuck from the ground in Stormhill|