# Elden Ring Wiki
---

!!! info "The real website"
    This site is inspired by The Elden Ring Wiki Fextralife [^1], so if you want to see the real website and interested by it, please go to the it, it's a very good wiki that I use too, with very detailled articles and guide, and an interractive  map, to help you in your quest of The Elden Ring. And with wiki of the other FromSoftware game's like DarkSouls 3, Demon Souls or Sekiro and other.  
    So please go check their work, it's fantastic.  
    Thank to you.


![logo wiki Elden Ring](images/elden-ring-wiki-logo.png)

Elden Ring Wiki Guide: Walkthrough, Multiplayer, Bosses, Secrets, Riding, Skills and more for Elden Ring!  
The new From Software game in collaboration with George. R.R. Martin!

---

![New](images/elden-ring-news.png)Patch 1.03: New Side Quest, Buy Simithing Stones, NPC Markers and big Nerfs: video here.

---

## Elden Ring Review

---

!!! question "When does Elden Ring Release?"
    Elden Ring was released for all platforms on February 25th of 2022. The original release date of January 21st was announced during the Summer Game Fest 2021, and the delay was revealed via Twitter on October 18th 2021

### Latest Elden Ring News 🆕

17th March 2022: Patch 1.03 (highlights)

### Elden Ring System Requirements 🖥️

#### Minimum Requirements

- CPU: Intel Core i5-8400 / AMD FX-Ryzen 3 3300X  
- RAM: 12 GB  
- OS: Windows 10  
- VIDEO CARD: Nvidia GeForce GTX 1060 3GB / AMD Radeon RX 580 4GB  
- Directx: Directx 12 (Feature Level 12.0)  
- Storage: 60GB  
- Sound Card: Windows-Compatible Audio Device  

#### Recommended Requirements

- CPU: Intel Core i7-8700K / AMD Ryzen 5 3600X  
- RAM: 16 GB  
- OS: Windows 11/10  
- VIDEO CARD: Nvidia GeForce GTX 1070 8GB / AMD Radeon RX Vega 56 8GB  
- Directx: Directx 12 (Feature Level 12.0)  
- Storage: 60GB  
- Sound Card: Windows-Compatible Audio Device  

!!! warning "graphics drivers"
    If your graphics drivers are not up to date, the game may not work.  
    Consider downloading graphics driver updates:  
    - Nvidia drivers [^2]  
    - AMD drivers [^3]  
    - Intel drivers [^4]  

---

## Elden Ring Getting Started Guide 🏁

---

???+ "Latest Elden Ring News"

    #### 17th March 2022: Patch 1.03 (highlights)

    - Adds new NPC Jar Bairn 
    - Adds questlines for several NPCs 
    - Adds Smithing Stones to early game merchants 
    - Adds NPC markers to the in-game map 
    - Fixes balancing for many weapons, ashes of war, spirit ashes and armor 
    - Fixes scaling of some weapons. 

    #### 10th February 2022: 
    Classes Revealed, Character Creation, New Gameplay, Game's HUB, Location and NPCs

    - All Classes and their Stats were revealed 
    - All starting Keepsakes, which act like Gifts of Dark Souls revealed. 
    - An entirely new map was revealed 
    - The game’s hub and many NPCs were revealed 
    - New Locations were revealed. 

    #### 4th November 2021: 
    Public Gameplay Revealed, Preorders Opened and Collector's Edition Annouced

    - Gameplay shows off Stats and Crafting  
    - Gameplay shows off the Maps  
    - The Valkyrie Boss has been named  
    - The Stormveil Castle Boss is named  
    - Melina is introduced  
    - Lots of Armor and Weapons are shown  

    #### 19th October 2021: 
    Game DELAYED, Closed Network Test announced

    - Closed Network Test is by invitation only  
    - Closed Network Test will only be on Consoles  
    - Players must apply via eldenring.bn-ent.net/en/  

    #### 28 August 2021: 
    Interviews and Gameplay demo confirm MANY mechanics, details on Crafting, PvP and Online, Legacy Dungeons, and a lot more.

    - Character Customization  
    - Attributes (Stats)  
    - Skills  
    - Spirit Summons (Buddies)  
    - Weapons Categories (Twinblades spotted!)  
    - Armor Sets  
    - Magic Types (Pyro, Sorc & more)  
    - Combat Changes  
    - Jumping Attacks (Bows & Magic too!)  
    - Stances (posture / guard system)  
    - Stamina only for Combat  
    - Stealth (Sleep Arrows!)  
    - NPCs Met & Roles in Story  
    - New Waifu (Melina)  
    - Pot Boy / Pot People  
    - Spirit Steed (Mount) Mechanics  
    - Mounted Combat  
    - Crafting: Gathering Resources  
    - Stam Recovery via Dried Meat  
    - Map Revealed  
    - Map Functions, Setting Markers, Overview  
    - Labyrinth spotted! Chalice Dungeons?  
    - Dungeons & Legacy Dungeons  
    - Return of Illusory Walls  
    - Jump to Explore Rooftops of Dungeons  
    - Multiplayer & Online  
    - Asynch Features & "Group" Passwords  
    - Co-op possibilities  
    - PvP ONLY For COOP  
    - Confirmed Gestures  

    #### 12 June 2021: 
    Interviews with Hidetaka Miyazaki confirm pvp, npc summons, crafting, over 100 Skills (weapon arts) and a lot more.

    #### 10 June 2021: 
    Announcement Trailer, Gameplay and Release date announced during Games Fest on June 10th 2021. Horseback riding, horseback combat and Multiplayer confirmed!

    #### May / June 2021: 
    Several journalists and known leakers claim the game will be shown one June 10th at the Summer Games Fest on June 10th 2021

    #### March 3rd 2021: 
    Leaked Elden Ring Trailer shows Gameplay

    Gameplay reveals via a leak of an internal Bandai Namco trailer have shown several aspects of the game, such as:

    - Confirmed Jump mechanic  
    - Confirmed Stealth Mechanic  
    - Confirmed Mounted Combat  
    - Weapon Categories: Swords, Spears / Polearms, Flails  
    - Magic: Magic Spear and Homing Soul Arrow  
    - Rolling / iframes  

    For a gameplay analysis [^5].

    #### January 2021: 
    Elden Ring unused Concept Art revealed by artist [^6].

---

## Elden Ring Gameplay

---

The **Elden Ring Wiki** covers Elden Ring, developed by FromSoftware and BANDAI NAMCO Entertainment Inc, is a fantasy action-RPG adventure set within a world created by Hidetaka Miyazaki -creator of the influential DARK SOULS video game series, and George R R Martin - author of The New York Times best-selling fantasy series, A Song of Ice and Fire. Danger and discovery lurk around every corner in FromSoftware's largest game to-date.

Hidetaka Miyazaki, President and Game Director of FromSoftware Inc known for directing critically-acclaimed games in beloved franchises including Armored Core, Dark Souls, Bloodborne and Sekiro Shadows Die Twice.

George R R Martin is the On  the New York Times bestselling author of many novels, including the acclaimed series A Song of Ice and One - A Game of Thrones, A Clash of Kings, A Storm of Swords, A Feast For Crows, and A Dance with Dragons. As a writer-producer, he has worked on The Twilight Zone, Beauty and the Beast, and various feature films and pilots that were never made.

---

## Elden Ring Reveal

---

- Elden Ring has been in production since the end of the Dark Souls III DLC.
This is in contrast to Sekiro, that begun production in 2015 after Bloodborne. 
- Elden Ring will release on PC, Xbox One and Playstation 4  
- Bandai Namco is publishing and has registered the Elden Ring IP  
- Elden Ring Release Date – From our observations  
The release window for this game is unlikely to match FromSoftware’s traditional March-April, and is more likely to fall into Dark Souls’ September-October release.  
- “Elden Ring is a third-person action RPG with a fantasy setting… gameplay is not so far from Dark Souls… Elden Ring belongs to the same genre.”  
- Plethora of weapons, magic and builds, and a focus on RPG elements. FromSoftware expects equipment variety to be larger than Souls in this regard.  
- No main fixed protagonist, players create the characteristics and personality of their character. This leads us to believe Character Creation is a given.  
- The World of Elden Ring is vast and interconnected. No longer a continuity of “levels”, players see vast landscapes in between the “dungeon-esque” areas.  
- The larger world scale allows for freedom, promotes exploration, and introduces new and exciting mechanics, such as Horse Riding and Mounted Combat.  
- Elden Ring is a “natural evolution of Dark Souls.” New mechanics needed by this larger world. Expect FromSoftware’s trademark challenging boss battles.  
- Towns won’t be full of villagers, merchants and the like. They will be ruins, dungeons and exploration hubs. As Souls there is no vast population of NPCs.  
- The NPCs the player finds are expected to be the most compelling yet in the studio’s work, thanks to Martin’s involvement in the Mythos.
George R. R. Martin has written the world’s mythos and Hidetaka Miyazaki has written the game story.  
- Story will be revealed through “fragments of environmental storytelling” as in previous Souls games.  
- Mythos and the present moment of the player are connected and exploration will lead players to understand why things are as they are.  
- There is no information at time of writing about multiplayer/online/invasions.  

=== "A new fantasy world"

    Journey through the Lands Between, a new fantasy world created by Hidetaka Miyazaki, creator of the infl uential DARK SOULS video game series, and George R. R. Martin, author of The New York Times best-selling fantasy series, A Song of Ice and Fire. Unravel the mysteries of the Elden Ring’s power. Encounter adversaries with profound backgrounds, characters with their own unique motivations for helping or hindering your progress, and fearsome creatures.

=== "World exploration in the Lands Between"

    ELDEN RING features vast fantastical landscapes and shadowy, complex dungeons that are connected seamlessly. Traverse the breathtaking world on foot or on horseback, alone or online with other players, and fully immerse yourself in the grassy plains, suffocating swamps, spiraling mountains, foreboding castles and other sites of grandeur on a scale never seen before in a FromSoftware title.

=== "Genre-defining gameplay"

    Create your character in FromSoftware’s refi ned action-RPG and define your playstyle by experimenting with a wide variety of weapons, magical abilities, and skills found throughout the world. Charge into battle, pick off enemies one-by-one using stealth, or even call upon allies for aid. Many options are at your disposal as you decide how to approach exploration and combat. A free upgrade to the PlayStation 5 version will be available for players who purchase ELDEN RING on PlayStation 4, and SmartDelivery will be supported for Xbox One and Xbox Series X|S.

|Elden Ring Wiki|  |
|---|--|
|PUBLISHER              | BANDAI NAMCO Entertainment America Inc. |
|DEVELOPER              | FromSoftware Inc. |
|PLAYERS                | 1 to 4 (from official FromSoftware site) |
|GENRE                  | Action RPG |
|PLATFORMS              | PLAYSTATION®4, XBOX ONE, PC, FREE UPGRADE: PLAYSTATION®5, XBOX SERIES X, XBOX SERIES S |
|RATING                 | RP – Rating Pending |
|LOCALIZATION VOICE OVER| English |
|SUBTITLE               | English, French, Italian, German, Spanish, Polish, Russian, Neutral Spanish, Brazilian Portuguese, Japanese, Thai, Traditional Chinese, Simplified Chinese, Korean | 

---

## What We Know So Far

---

### Elden Ring Story Overview

=== "The Lands Between"

    ![vue sur l'arbre sacré](images/lore-image1-elden-ring-wik-guide-300px.jpg){ align=right width=500px } 
    The Lands Between is the world of Elden Ring, and to those who live in The Lands Between were blessed by the Elden Ring and the "Erdtree". Those who were blessed by the Elden Ring are characterized by having a golden aura that can be seen in their eyes. However, at some point, some of those who were blessed, lost their grace, was exiled and labeled as the Tarnished.  As time passed, for unknown reasons, the Elden Ring was shattered across the lands and its pieces were picked up by many of the 6 demigods, where they inherited different powers and have physically changed and twisted due to being corrupted by the power.

    The Lands Between is broken into 6 distinct areas that is overseen by a demigod and features a central hub that connects all these 6 areas. According to an interview with Miyazaki, players will have total freedom as to the order they want to tackle for each area, and it is up to the player on how they want to approach it. Apart from that the hub will not be accessible at the beginning of the game, which means you will need to progress the game until you reach the hub area.

    According to Hidetaka Miyazaki: "The Lands Between is supposed to invoke this feeling of something that's very mysterious and very ethereal – and we hope that when players play the game, they're going to experience that."

=== "The Tarnished"

    ![fight](images/Elden-Ring-The-Tarnished-4k.png){ align=right width=550px }
    In the official reveal trailer that was presented in E3 2021, you'll see a softly spoken, hooded woman saying "The Tarnished, will soon return." The question is, who and what is The Tarnished? Simple, The Tarnished is you, the player, you are one of the Tarnished that have been exiled upon losing their blessing that was given by the Elden Ring and the "Erdtree", a tree that hosts source and life of The Lands Between. Very much like previous Souls games, you are the chosen undead or the ashen one. You are then called back to The Lands Between, as well as the other Tarnished after the destruction of the Elden Ring to recover the shards of the Elden Ring, to make them whole once again, and to become the lord of The Lands Between.

    According to Hidetaka Miyazaki in an interview: " I feel like one of the main themes of the game is how the player, the Tarnished, approaches or treats this new-found grace and this return to the land that they were once banished from, how they interpret this and the meaning. It's not just the player-character, of course, it's lots of characters in the game, who are all beckoned back and will have their own adventures and motives. We want the player to discover for themselves what that means and how they want to begin their adventure."

### Combat in Elden Ring

=== "Stealth"

    ![infiltratioon](images/elden_ring_artwork_elden_ring_wiki_guide_17_1920px.jpg){ width=500px align=right }
    Stealth mechanics seem that will be present and may be useful in combination with backstab attacks, also the game may feature a night and day mechanic that may affect the stealth mechanic as well. Stealth as many are aware of, will allow the player to crouch, sneak, and hide behind tall grass to be less detected by the enemy. Stealth will play an integral mechanic in combat since you can use it to your advantage to sneak behind an enemy to instantly kill them or at least deal a large amount of damage from using a stealth attack.

=== "New Mechanics"

    ![saut à cheval](images/elden_ring_artwork_elden_ring_wiki_guide_5_1920px.jpg){ width=500px align=right }
    Elden Ring will have a large scale in terms of the world and exploration, and to go hand-in-hand with this, some new mechanics are introduced such as jumping, yes players can jump now, as well as riding on horseback (Mounts). Hidetaka Miyazaki strongly suggests that you thoroughly search and explore every section of the world to find Skills that are hidden.

    There are a number of choices as well that players can approach their way of combat, some may be brave and choose to go on head-to-head, while others would rather have allies such as being able to summon the spirits of deceased enemies.

=== "Freedom in Character Build"

    ![fight](images/elden_ring_artwork_elden_ring_wiki_guide_4.jpg){ width=500px align=right }
    In the trailer that was revealed in E3 2021, we see a lot of unique Skills, magical abilities, and even having a horse leaping up a cliffisde, as well as attacking while riding your Mount. These are Skills that you pick up through the player's progression and exploration. There are some cases that a player can acquire a skill through purchasing it from merchants or learning them from an NPC, but during an interview with Hidetaka Miyazaki that was featured, he stresses out that in general, skils are hidden throughout the game's world, and it is the player's responsibiility to discover these through exploration.

    Elden Ring will have complete freedom when it comes to building up your character, you are basically free to combine your Skills with different weapons and equipment, and you'll learn magic as well. Skills are freely interchangeable between a large variety of weapons as per Hidetaka Miyazaki, and that there are around 100 Skills. With having less restrictions, players will have even more variations of Builds that is even richer than previous Souls games.

=== "Summoning the Dead"

    ![summoning](images/summon-combat-info-elden-ring-wiki-guide.jpg){ width=500px align=right }
    One of the combat aspects of Elden Ring is being able to summon Spirits that can help you in battle. These spirits are usually the fallen souls of the enmies that you defeat, and summoning them will have them assist you in combat. These are your offline summons, and it is quite different to have your friends or other playres summoned to your session which is available in Online co-op play. What's interesting with multiplayer is that you can even set a password on your calling marks, so that this way you can just have your friends join your session, if you're more comfortable with that rather than having a random drop in.

    Accorting to Miyazaki, "These spirit summons, we feel like – as well as there being a large variety to them – they're a nice collectable hidden element within the game world to discover and to equip as you go. They offer a lot of different strategic options, but also the player might find that they just like a certain enemy tagging along with them, a certain summon pleases them aesthetically. So there's a lot of strategic and a lot of personal touch to these summons. And we hope that players will also enjoy discovering their own progression elements as well."

=== "Day & Night Cycle"

    ![cavalier fantôme](images/elden_ring_artwork_elden_ring_wiki_guide_15_1920px.jpg){ width=500px align=right }
    Real-time changes are featured in Elden Ring in the form of the Day and Night cycle, as well as Dynamic Weather. These features also heavily affect gameplay, combat, and the world you are exploring. For example, in cases that you are romaing at night, both players and enemies will change in terms of aggression due to a decrease in visibility, and some special enemies that only appear at night will come out.

=== "Game Difficulty"

    ![chateau de Godrick salle à manger](images/elden_ring_artwork_elden_ring_wiki_guide_13_1920px.jpg){ width=500px align=right }
    There will only be ONE difficulty in Elden Ring. The decision of FromSoftware leaning towards this feature can benefit both new and veteran players of this genre. Elden Ring features a large scale when it comes to its world and exploration, so it does make sense that it really is up to you on how you want to tackle the game, it is highly encouraged that players carefully assess the situation that lies ahead and to find different ways to deal with it. This also provides a good camaraderie within the community since everyone can share their experiences and hopefully help one another.

=== "Covenants"

    Covenants or **Factions** are a confirmed feature of Elden Ring. **Elden Ring Covenants** are player-chosen allegiances or pledges undertaken to specific deities or groups of NPCs. Based on previous titles, Players can expect their covenant to affect their gameplay by changing their Online interactions, or by creating new situations were users can collect special items or defeat certain enemies to rank up and obtain special rewards. Rewards are usually Magic, Weapons and Armor pieces.

[^1]: [The Elden Ring Wiki Fextralife](https://eldenring.wiki.fextralife.com/Elden+Ring+Wiki)
[^2]: [Nvidia drivers](https://www.nvidia.fr/Download/index.aspx?lang=fr)
[^3]: [AMD drivers](https://www.amd.com/fr/support)
[^4]: [Intel drivers](https://www.intel.fr/content/www/fr/fr/support/products/80939/graphics.html)
[^5]: For a gameplay analysis see [this video](https://www.youtube.com/watch?v=3sMf753y_hE)
[^6]: Concept Art revealed by artist [here](https://fextralife.com/elden-ring-concept-art-revealed/)