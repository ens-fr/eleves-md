# Character Creation

---

**Character Creation** for Elden Ring allows players to fully shape and customize their character. When creating a character, you are able to define a wide variety of features, ranging from its voice and face structure, to gamestyle-defining characteristics like its Class.

Elden Ring features a Rebirth mechanic that allows you to Respec your character. You can unlock this function by defeating Rennala Queen of the Full Moon. Once defeated, you'll be able to trade Larval Tears for a Respec. Please visit the Rebirth Page for more information on how to respec your character.

---

## Character Creation in Elden Ring

---

In Elden Ring, you have the option to choose your character's Name, Gender, Age, Class, Keepsake and tweak its physical appearance. Additionally, you have the option to choose among 10 character presets under "Base Templates". In this page you'll find a comprehensive description of what these attributes do and how they work.

!!! question "What is the Best Class in Elden Ring ?"

    Regardless of the type of character you choose, any Class allows you to complete your journey through The Lands Between. You should pick your character class based on the playstyle that best suits you, as combat styles greatly vary since the early stages of the game and these differences intensify throughout the game. That said, some classes provide an easier start. The recommended classes for beginners are Vagabond due to the straightforward starting gear and good stats for a melee focused playstyle, and Samurai as it starts with a Longbow and a good amount of arrows, and an Uchigatana which is a very good bleed weapon. For those looking for a magic casting class, the Astrologer is a strong beginner option with its focus in sorcery type magic.

!!! question "Can I respec my Character in Elden Ring ?"

    Yes, you will be able to reallocate your character's stat points once you have the ability to do so. In addition to learning the skill, you will be required to turn in a rare item in exchange for reallocating points. You will be able to get the ability to respec your character during your playthrough. Please visit the Rebirth Page for more information on how to respec your character.

---

## Create Character Screen

---

### Base Attributes

![menu création personnage](images Characters/age_description_screen_elden_ring_wiki_700px.png){ align=right width=650px}
The first basic attribute to define in the main **Character Creation** window is ^^Name^^, followed by the "^^Body Type^^" option, in which the gender of your character is chosen. This is done by selecting between two options: "Type A" (Male) and "Type B" (Female).

Next you can select you character's apparent Age: Young, Mature or Aged. Here the game explains that this option "only affects appearance" and "has no bearing on ability".

At the main **Character Creation** window, you are also able to preview both the initial Stats for your character, as well as the appearance of the character itself. You can zoom in or out and rotate your character's model for further examination.

---

## Elden Ring Character Appearance

---

Besides _Body Type_ and _Age_, there are two other sections to further shape the character's appearance: "_Choose Base Template_" and "_Detailed Appearance_". The first one provides 10 different appearance presets to choose from. Even though each is displayed with a brief description, this is not a background for the character, but more of a description of represented type of character.

The second section accesses the "_Detailed Appearance_" sub-menu. Here, a wide variety of appearance-defining controls are available for players to tweak the aspect of their character. It also offers options to save and recall custom presets.

### Choose Base Template

"_Choose Base Template_" offers 10 different pre-set character appearances to set the starting point of your aesthetics customization process. These provide a wide range of varying traits such as hairstyle, skin color and body shape. Each template is displayed with a description of the intended type of descendance. However, note that this is not a background for the character itself, it doesn't reflect any narrative arc and won't affect gameplay.

#### Available templates

|Warrior|Truth-Seeker|Aristocrat|Loner|Northerner|
|-|-|-|-|-|
| ![Warriror](images Characters/warrior_base_templates_elden_ring_wiki_175px.png) | ![Truth-Seeker](images Characters/truth-seeker_base_templates_elden_ring_wiki_175px.png) |![Aristocrat](images Characters/aristocrat_2_base_templates_elden_ring_wiki_175px.png) |![Loner](images Characters/loner_base_templates_elden_ring_wiki_175px.png) | ![Northerner](images Characters/northerner_base_templates_elden_ring_wiki_175px.png) |
| The most common face among the Tarnished. After all, they were all warriors once. | The face of an austere pilgrim. There are many roads to truth. | A regal face found among those who claim noble blood in the Lands Between. | Face found among a proud and seclusive tribe of folk well-versed in ancient legends and heresies alike. | A face found among the hardy people of the unforgiving north. Some say they're descended from giants. |

|Seafarer|Reedlander|Draconian|Nightfolk|Numen|
|-|-|-|-|-|
| ![Seafarer](images Characters/seafarer_base_templates_elden_ring_wiki_175px.png) | ![Reedlander](images Characters/reedlander_base_templates_elden_ring_wiki_175px.png) | ![Draconian](images Characters/draconian_base_templates_elden_ring_wiki_175px.png) | ![Nightfolk](images Characters/nightfolk_base_templates_elden_ring_wiki_175px.png) | ![Numen](images Characters/numen_base_templates_elden_ring_wiki_175px.png) |
| The face of one who wanders the seas in search of their home in the Lands Between. | A face from the faraway, isolated Land of Reeds, where blood is a familiar sight. | The stony face of the people of the ancient dragons, among whom life is typically short. | The features of those known as Nightfolk. Few in number, they were said to bleed silver long ago. | The face of the Numen, supposed descendants of denizens of another world. Long-lived but seldom born. |



### Detailed Appearance

---

This section offers a variety of controls to tweak the aspect of the character. These options are divided in 5 groups: _Age_, _Voice_, _Alter Skin Color_, _Alter Face_ & _Hair_ and _Alter Body_.

It also offers options to save custom presets by selecting "_Save to Favorites_" and recall them using "_Load Favorite_".

=== " Alter Skin Color "
    
    ![SkinColor](images Characters/alter-skin-color_create-character_screen_2_elden_ring_wiki_600px.png){ align=right width=400px }
    This tab allows you to modify the skin color of your character, either from a "_Preset Colors_" palette or by fine-tuning it with the "_Adjust Color_" option, which gives an even deeper control on skin tone.

    The color of the skin is applied on top of the selected Base Template, and "_Temporary Colors_" can be stored.

    details on the adjust color menu and functionality.

=== " Alter Face & Hair " 
     
    ![SkinFace&Chair](images Characters/alter_face&hair_screen_screens_elden_ring_wiki_600px.jpg){ align=right width=400px }
    In the "_Alter Face & Hair_" menu, you will come across 11 more options to further manipulate specific parts of the character's face and hair. These are:

    - Adjust Face Template: Select among a variety of pre-set faces.  
    - Face Structure: Modify the shape of the face with dedicated controls.  
    - Hair: Modify the character's hairstyle.  
    - Eyebrows: Alter diverse properties of the eyebrows of the character.  
    - Facial Hair: Tweak parameters for the character's facial hair, i.e. beards or mustaches.  
    - Eyelashes: Controls for modifying different eyelashes parameters.  
    - Eyes: Modify various parameters for your character's eyes. Includes unlockable Eye Alteration setting.  
    - Skin Features: Fine-tune the skin of the character by adding and modifying skin details.  
    - Cosmetics: Choose among several different cosmetic add-ons.  
    - Tattoo/Mark/Eyepatch: Add a Tattoo, Mark or an Eyepatch to your character.  
    - Similar Face: Generate similar faces.  

=== " Alter Body "
    
    This tab allows you to modify the proportions of your character and some other cosmetic body-wide settings. List of settings: 

    - Head: Adjust size of head body part.
    - Abdomen: Adjust size of the lower body area.
    - Body Hair: Adjust abundance of body hair.
    - Musculature: Choose between Standard and Muscular options.
    - Chest: Adjust size of the upper body area.
    - Legs: Adjust leg thickness.
    - Body Hair Color: Adjust the color of Body Hair.
    - Burn Marks: Skin alteration akin to hollowing in the Dark Souls series that can be turned on and off. Option appears after meeting the Three Fingers.

---

## Elden Ring Keepsakes

---

**Keepsakes in Elden Ring** are an equivalent to the **Gifts** of **Dark Souls**. You can select one from a pool of nine (9) **Keepsakes** during character creation, beginning your journey in The Lands Between with a personalized item. 

Alternatively, you can start the game without a **Keepsake**. This option is only advised for experienced players looking for a harder challenge.

All **Keepsakes** are obtainable later in-game, meaning that you can safely choose to compliment your early game.

### Recommended Keepsakes

**Keepsakes** provide an opportunity for a personalized early game advantage. Below are our recommendations and reasonings, not necessarily in order, for the more popular options:

1. Golden Seed - Nothing beats having an extra flask! 
2. Stonesword Key x2 - Gain early access to areas gated by Imp Statues, or invest into saving Runes later. These can be found while exploring or purchased from Merchants, but start at a hefty 2000 Runes each.
3. Lands Between Rune - Consume for 3000 Runes. Allows for early Merchant purchases or quick level-ups.
4. Fanged Imp Ashes - After obtaining the Spirit Calling Bell shortly after the tutorial, this duo of fanged imps have the potential for high damage with attacks causing Blood Loss buildup.
5. Cracked Pot x3 - After being introduced to Crafting, players can store a greater amount of crafted Throwing Pots.

---

## Elden Ring Keepsakes

---

|Name|Type|Description|
|-|-|-|
|Crimson Amber Medallion ![CrimsonAmberMedallionTalisman](images Characters/crimson_amber_medallion_talisman_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Talisman|Increases maximum HP by 6%.|
|Lands Between Rune ![Rune](images Characters/lands_between_rune_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Consumable|Can be consumed to acquire 3000 Runes.|
|Golden Seed ![GoldenSeed](images Characters/golden_seed_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Upgrade Material|Can be used to upgrade your Flask, increases its uses.|
|Fanged Imp Ashes ![Ashes](images Characters/fanged_imp_ashes_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Spirit Ashes|Summons two fanged imp spirits.|
|Cracked Pot ![CrackedPot](images Characters/cracked_pot_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Key Item|Crafting item used to craft throwing pots.|
|Stonesword Key ![StoneswordKey](images Characters/stonesword_key_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Key Item|Can be used to break one imp statue seal.|
|Bewitching Branch ![BewitchingBranch](images Characters/bewitching_branch_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Consumable|Uses FP to charm pierced enemies.|
|Boiled Prawn ![BoiledPrawn](images Characters/boiled_prawn_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Consumable|Boosts physical damage negation for a certain duration.|
|Shabriri's Woe ![Shabriri'sWoe](images Characters/shabriris_woe_talisman_elden_ring_wiki_guide_200px.png){ width=75px align=right }|Talisman|Used to constantly attract enemies' aggression.|

---

## Choosing a Class in Elden Ring

---

When you begin the game, you'll be able to choose from one of 10 classes. The choice you make affects the equipment with which you begin your adventure. 

Each class has its own starting equipment and items.

**Classes** for Elden Ring are the starting archetypes available to the player. All Elden Ring Classes follow similar concept to that of the Dark Souls series, choosing a class determines the player's starting Stats and Equipment which can then be improved and upgraded later on as you progress throughout the game. This page contains a full list of all the classes in the game.

Players can select a Keepsake (like Gifts of Dark Souls) during Character Creation, which provide more customization options during early game.

---

## Elden Ring Classes Guide

---

=== "Hero"
    ![Hero](imagesClasses/hero_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |7|14|9|12|16|9|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |7|8|11|![battle axe](imageweaponclass/battle_axe_melee_armaments_weapons_elden_ring_wiki_guide_75px.png)![leather shield](imageweaponclass/leather_shield_elden_ring_wiki_guide_75px.png)|
=== "Bandit"
    ![Bandit](imagesClasses/bandit_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |5|10|11|10|9|13|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |9|8|14|![great knife](imageweaponclass/great_knife_elden_ring_wiki_guide_75px.png)![shortbow](imageweaponclass/shortbow_elden_ring_wiki_guide_75px.png)![buckler shields](imageweaponclass/buckler_shields_elden_ring_wiki_guide_75px.png)![bone arrows](imageweaponclass/bone_arrows_fletched_ammunition_elden_ring_wiki_guide_75px.png)
=== "Astrologer"
    ![Astrologer](imagesClasses/astrologer_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |6|9|15|9|8|12|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |16|7|9|![magic short sword](imageweaponclass/magic_short_sword-elden-ring-wiki-guide-200px.png){width=75px}![astrologers staff](imageweaponclass/astrologers_staff-elden-ring-wiki-guide-200px.png){width=75px}![scripture wooden shield](imageweaponclass/scripture_wooden_shield-elden-ring-wiki-guide-200px.png){width=75px}![glinstone pebble spells](imageweaponclass/glintstone_pebble_spells_elden_ring_wiki_guide_200px.png){width=75px}![glinstone arc spells](imageweaponclass/glintstone_arc_spells_elden_ring_wiki_guide_200px.png){width=75px}|
=== "Warrior"
    ![Warrior](imagesClasses/warrior_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |8|11|12|11|10|16|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |10|8|9|![scimitar](imageweaponclass/scimitar_weapons_elden_ring_wiki_guide_200px.png){width=75px}![scimitar](imageweaponclass/scimitar_weapons_elden_ring_wiki_guide_200px.png){width=75px}![riveted wooden shiels](imageweaponclass/riveted-wooden-shield_small-shield-elden-ring-wiki-guide-200.png){width=75px}|
=== "Prisoner"
    ![Prisoner](imagesClasses/prisoner_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |9|11|12|11|11|14|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |14|6|9|![glinstone staff](imageweaponclass/glintstone_staff_elden_ring_wiki_guide_75px.png)![estoc thrusting sword](imageweaponclass/estoc_thrusting-sword_weapons-elden-ring-wiki-guide-200.png){width=75px}![rift shield](imageweaponclass/rift-shield-elden-ring-wiki-guide-200.png){width=75px}![magic glintblade](imageweaponclass/magic_glintblade_elden_ring_wiki_guide_75px.png)|
=== "Confessor"
    ![Confessor](imagesClasses/confessor_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |10|10|13|10|12|12|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |9|14|9|![brooadsword](imageweaponclass/broadsword_elden_ring_wiki_guide_75px.png)![blue crest heater](imageweaponclass/blue-crest-heater-shield-elden-ring-wiki-guide-200.png){width=75px}![finger seal](imageweaponclass/finger-seal_armament-elden-ring-wiki-guide-200.png){width=75px}![urgent heal](imageweaponclass/urgent_heal_spells_elden_ring_wiki_guide_75px.png)![assassin's approach](imageweaponclass/assassins_approach_elden_ring_wiki_guide_75px.png)|
=== "Wretch"
    ![Wretch](imagesClasses/wretch_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |1|10|10|10|10|10|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |10|10|10|![club](imageweaponclass/club_weapons_elden_ring_wiki_guide_75px.png)|
=== "Vagabond"
    ![Vagabond](imagesClasses/vagabond_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |9|15|10|11|14|13|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |9|9|7|![longswoord](imageweaponclass/longsword_elden_ring_wiki_guide_75px.png)![halberd](imageweaponclass/halberd-weapon-starting-equipment-vagabond-elden-ring-wiki-guide-75-px.png)![heater shield](imageweaponclass/heater-sheild-starting-equipment-vagabond-elden-ring-wiki-guide-75-px.png)|
=== "Prophet"
    ![Prophet](imagesClasses/prophet_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |7|10|14|8|11|10|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |7|16|10|![short spear](imageweaponclass/short-spear__weapons-elden-ring-wiki-guide-200.png){width=75px}![finger seal](imageweaponclass/finger-seal_armament-elden-ring-wiki-guide-200.png){width=75px}![rickety shield](imageweaponclass/rickety_shield_shields_elden_ring_wiki_guide_75px.png)![heal](imageweaponclass/heal_spells_elden_ring_wiki_guide_75px.png)![catch flame](imageweaponclass/catch_flame_elden_ring_wiki_guide_75px.png)|
=== "Samurai"
    ![Samurai](imagesClasses/samurai_class_elden_ring_wiki_guide_200px.png){align=left}

    |Level|Vigor|Mind|Endurance|Strength|Dexterity|
    |-|-|-|-|-|-|
    |9|12|11|13|12|15|
    
    |Intelligence|Faith|Arcane|starter weapons|
    |-|-|-|-|
    |9|8|8|![uchigatana](imageweaponclass/uchigatana_elden_ring_wiki_guide_75px.png)![longbow](imageweaponclass/longbow__weapons-elden-ring-wiki-guide-200.png){width=75px}![red thorn roundshield](imageweaponclass/red_thorn_roundshield_shields_elden_ring_wiki_guide_75px.png)![arrow](imageweaponclass/bone_arrows_fletched_ammunition_elden_ring_wiki_guide_75px.png)![fire arrow](imageweaponclass/fire_arrow_arrow_-elden-ring-wiki-guide-200px.png){width=75px}|

!!! warning "evolution"
    Your character will evolve throughout the adventure, so don't pick a class if you think your equipment and levels won't change.

---

## Elden Ring Classes Comparison

---

This Class Comparison table helps players identify the relative strengths and weaknesses of the starting classes of Elden Ring.

| Class | Level | Vigor | Mind | Endurance | Strength | Dexterity | Intelligence | Faith | Arcane | Total |
|-|-|-|-|-|-|-|-|-|-|-|
| Hero | 7 | 14 | 9 | 12 | **16** | 9 | 7 | 8 | 11 | 86 |
| Bandit | 5 | 10 | 11 | 10 | 9 | 13 | 9 | 8 | **14** |	84 |
| Astrologer | 6 | 9 | **15** | 9 | 8 |	12 | **16** | 7 | 9 | 85 |
| Warrior | 8 | 11 |12 | 11 | 10 | **16** | 10 | 8 | 9 | 87 |
| Prisoner | 9 | 11 | 12 | 11 | 11 | 14 | 14 | 6 | 9 | 88 |
| Confessor | 10 | 10 | 13 | 10 | 12 | 12 | 9 | 14 | 9 | **89** |
| Wretch | **1** | 10 | 10 | 10 | 10 | 10 | 10 | 10 | 10 | 80 |
| Vagabond | 9 | **15** | 10 | 11 |	14 | 13 | 9 | 9 | 7 | 88 |
| Prophet | 7 | 10 | 14 | 8 | 11 | 10 | 7 | **16** | 10 | 86 |
| Samurai | 9 | 12 | 11 | **13** | 12 | 15 | 9 | 8 | 8 | 88 |
 