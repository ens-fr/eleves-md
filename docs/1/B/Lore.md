## Story Trailer Transcript

---
!!! warning "spoil"
    In this section you can be spoil about bosses

It happened an age ago. ![THE night when the rune of death was stolen](imageLore/Elden_Ring_Screenshot_CG.jpg){ width=380px align=left }
 
But when I recall, I see it true.  

On a night of wint'ry fog. 

The rune of death was stolen  

And the demigods began to fall, starting with Godwyn the Golden.  ![Godwyn](imageLore/Elden_Ring_Screenshot_CG (2).jpg){ width=450px align=right }

Queen Marika was driven to the brink. 

The Shattering ensued; a war that wrought only darkness.

The Elden Ring was broken, but by whom? And why?  
What could the Demigods ever hope to win by warring?  

![The fight between Malenia and Radahn](imageLore/Elden_Ring_Screenshot_CG%20(6).jpg){ width=600px align=left }

The conqueror of the stars, General Radahn.  
And the Blade of Miquella, Malenia the Severed. 

These two were the mightiest to remain, and locked horns in combat.  
But there would be no victor.

And so, we inhabit a fractured world.  
Awaiting the arrival of the Elden Lord.

Unless of course, thou shouldst take the crown?

---

![Melina](imageLore/ELDENRING_06_4K.jpg){ width=500px align=right }
The Tarnished will soon return.

Guided by Grace once lost.

The Golden Order is broken to its core.

Foul Tarnished... in search of the Elden Ring.

Emboldened by the flame of ambition.

Someone must extinguish thy flame.

![The elden ring](imageLore/Elden_Ring_Screenshot_CG (3).jpg){ width=500px align=left }

They will fight. And they will die. In an unending curse

For how else is a Champion, or a Lord, to be born?

... Well. A lowly Tarnished, playing as a Lord.

I command thee, knee!

Brandish the Elden Ring ... for all of us.

---

## Prologue

---

The Golden Order has been broken.![Elden Order](imageLore/elden-ring-newscreen06.jpeg){ width=650px align=right }

Rise, Tarnished, and be guided by grace to brandish the power of the Elden Ring and become an Elden Lord in the Lands Between.

In the Lands Between ruled by Queen Marika the Eternal, the Elden Ring, the source of the Erdtree, has been shattered.

Marika's offspring, demigods all, claimed the shards of the Elden Ring known as the Great Runes, and the mad taint of their newfound strength triggered a war: The Shattering. A war that meant abandonment by the Greater Will.

And now the guidance of grace will be brought to the Tarnished who were spurned by the grace of gold and exiled from the Lands Between. Ye dead who yet live, your grace long lost, follow the path to the Lands Between beyond the foggy sea to stand before the Elden Ring.

And become the ==Elden Lord==.

---

=== "Alternate Prologue"
    ![Fight with dragon](imageLore/elden-ring-newscreen01.jpeg){ width=600px align=right }
    Tarnished of the Lands Between
    The Golden Order has been shattered. Throughout the Lands Between, Demigods holding shards of the Elden Ring squabble and make war over the ruins of a perfect realm, now abandoned by the golden guidance of the Greater Will.

    As the echoes of this conflict thunder in the distance, an outcast arrives. Once, their ancestors called the Lands Between home, but the blessed light of grace was lost to their tribe long ago and they were expelled from the kingdom. They are the Tarnished, and they have returned to claim the Elden Lordship promised to them by legend.
    
    ![Nokron](imageLore/elden-ring-newscreen02.jpeg){ width=600px align=left }
    
    This is the world of ELDEN RING. As a Tarnished, the Lands Between await your exploration. You will ride through the vast fields, gallop over rolling hills, and leap to the top of rocky crags on your ephemeral steed, revealing a world teeming with life and danger.

    In the grand fields where your journey begins, mythic creatures prowl the veldts, ineffable horrors lurk in the bogs and marshes, and all manner of soldiers and itinerant warriors are waiting for those who wander unawares. Shy creatures nibble on sweet grasses or scuttle through the underbrush.

    Those few inhabitants who are not mad or hostile linger near the broken remnants of cities left behind by the Shattering. They may have answers for you, if you help them. Above them all, ensconced in vast legacies bristling with traps, secrets, and guardians, the Demigods – warped Lords who began as members of a royal and noble family – rule their domains with the unyielding power granted by shards of the Elden Ring.

=== "Extended Prologue"

    The guidance of lost graces – should you find them - will put you on a road to re-take these lands from the Demigods through might and magic, but you need not follow their path.

    The choice is yours. Do you crave power, or do you seek understanding? Decide for yourself, then build your character as you see fit.

    Try dozens of skills and find one to best suit your style. Practice stealth to avoid danger or catch enemies unaware. Use the environment, the weather, and the time of day to gain an advantage. Learn the art of combat , where you must read your enemy’s intentions and a well-timed dodge or parry could be the difference between life and death. Ride your steed into battle against mounted mercenaries and cut them from their horses. Master arcane spells from the masters that still linger among the ruins of the war. Summon familiar spirits to even the odds against you or call on your fellow Tarnished to fight at your side and share the burden as you explore. Or, delve into the complex, bloody history of the Shattering and discover the lost secrets of the Demigods and their kin. All these paths are possible, and more. Ultimately, your journey will be defined by the strength of your own ambition. The greater your goals, the greater the challenge will be. Should you choose to claim the Lands Between as your birthright then yes, you must fight.

    And yes, you may die.

    But you will return to fight again.

    For that is how a champion – or a Lord – is born.

---

## The Family tree

---

!!! question "understanding"
    If you don't understand certain things in the family tree, please refer to the boss lore in the Main Bosses section.

```mermaid
flowchart LR
    subgraph OuterGods
        direction LR
        ? 
        ??
        ???
    end
    
    subgraph Legends
        subgraph SamePerson
            direction LR
            Malekith
            Gurranq
        end
        Rennala
        ? & ?? --> SamePerson
        ?? & ??? --> TheSamePerson
        subgraph TheSamePerson
            direction TB
            Radagon
            Marika
        end
        Godfrey
        GO["Gideon Ofnir"]
    end

    subgraph Demigod
        direction LR
        subgraph Rennala'sAndRadgon'sChildren
            direction LR
            Rykard
            Radahn
            ????
            Renna
            Ranni
        end

        subgraph Twin
            direction LR
            Miquella 
            Malenia
        end
        Melina
        
        subgraph Marika'sAndGodfreyChildren
            direction LR
            GO["Gideon Ofnir"] -.Adopted.-> Nephili  
            subgraph TwinBrotherOmen
                direction LR
                Mohg
                Morgott
            end
            Nephili
            subgraph TwinBrother
                direction LR
                Godefroy
                Godrick
            end
            Godwyn
        end
    Marika & Godfrey --> Marika'sAndGodfreyChildren
    end

    Rennala & Radagon --> Rennala'sAndRadgon'sChildren
    TheSamePerson --> Twin
    Marika --> Melina



    subgraph Malenia'sDaughters
        direction LR
        Amy
        Mary
        Maureen
        Malireen
        Milicent 
    end
    Margit
    Malenia --> Malenia'sDaughters
    Morgott ==clone himself==> Margit


```

---

## The History 

---

```mermaid
flowchart TB
    subgraph Start
        direction TB
        A1["Greater will appoints Marika as Ruler"] --> A2[Marika removes Rune of Death from Elden Ring forcing it upon Maliketh] --> A3["Radagon marries Renella"] --> A4["Godfrey exiled  beginning the Long March as Horah Loux"] --> A5["Marika marries or fuses with Radagon"] --> A6["Ranni steals Hallowbrand, a fragment of Rune of Death, creating blades ables to slay demi-gods"]
        
        subgraph AtTheSameTime
            direction LR
         A7[Godwyn is murdered by the Black Knife Assassins] === A8[Ranni  dies at the same time by stripping her flesh] 
         end
        
        A6["Ranni steals Hallowbrand, a fragment of Rune of Death, creating blades ables to slay demi-gods"] --> AtTheSameTime
        AtTheSameTime --> A9["Marika, possibly enraged by her son's death shatters the Elden Ring"]
    end

    subgraph TheShattering
        direction TB
        F1["Mohg kidnaps Miquella and begans to worship the Formless Mother"] --> F2["Rykard agrees to be eaten by the Great Serpent"] --> G1["Malenia inflicts Scarlet Rot on Radhan, causing Radhan to go mad"] --> G2["Tarnished are summoned by the Greater Will to collect the Runes"] 
    end
    
    Start ==> TheShattering ==> ide3{Events of Game} ==> ide4{End}
```

## Outer Gods

The **Outer Gods** are mysterious beings mentioned throughout the lore of Elden Ring. They do not appear to manifest physical forms within the Lands Between and instead influence the events of the Lands Between in other ways, including through worship by denizens of the Lands Between and by powering incantations.

The Outer Gods are responsible for many of the events that happen both before and during Elden Ring's story, and have acted in both benevolent and malicious ways, and the various endings of the game (except, perhaps, the curse and dark moon endings) involve substituting one of the various outer gods for the Greater Will and beginning a subsequent cycle in which that god's power is incorporated into the Elden Ring.

!!! danger "There will be no images"
    Parce que tous ceci son soit des théory en ce qui concerne les Possible Outer Gods, ou alors FromSoftware n'a juste tout simplement pas créé d'artwork des Outer Gods.  
    Désolé pour le petit dérangement dans la lecture.


=== "List of Known Outer Gods"
    === "The Greater Will"

        The Outer God with the most influence in the current iteration of the Lands Between, and the creator of the Elden Beast, which later manifests the Elden Ring. Its main goal appears to be to maintain order within the Lands Between, although it appears to accomplish this by harvesting the life-essence of the dead of the Lands Between through the roots of the Erdtree. This Outer God uses the Two Fingers as vassals and is primarily associated with Erdtree, Golden Order, and Two Fingers Incantations. 

        This Outer God is mostly involved in the "Elden Lord" and "Age of Order" endings. (Although Corhyn comes to believe Goldmask was a madman who sought to betray the Golden Order, it is unclear this is actually correct. Corhyn frequently expresses confusion at Goldmask's wisdom and disbelief at, for instance, the revelation that Marika and Radagon are elements of the same whole, implying that he is unable to truly understand the nature of the Order or the forces that drive it. While Goldmask does seek to alter the Golden Order, the Mending Rune of Perfect Order indicates this is in order to make it more stable, a goal seemingly aligned with what we know of the Greater Will.)
    
    === "The Frenzied Flame"

        The Frenzied Flame is an Outer God who wants to end all life. According to the Frenzied Flame, life itself is the source of suffering and those who were born to suffer wish that they were never born. The Frenzied Flame and the Greater Will may once be a single entity known as the One Great. However, the Frenzied Flame viewed the Greater Will as having made a mistake when life was created. For this reason, the Frenzied Flame wants to undo all life and return everything to the One Great.

        This Outer God is associated with the Three Fingers and the Frenzied Flame Incantations. The Frenzied Flame can be subdued with Miquella's Needle before the player fully transforms into the Lord of Frenzied Flame.

        This Outer God is mostly involved in the "Lord of Frenzied Flame" ending. 

    === "The Formless Mother"

        An Outer God also known as the "mother of truth" who craves wounds. The Formless Mother employs Mohg as their primary vassal and is associated with Blood Incantations. When Mohg, Lord of Blood stood before her, deep underground, his accursed blood erupted with fire.

    === "The Sealed God of Scarlet Rot"

        An Outer God associated with the corrupting Scarlet Rot, which left Caelid filled with rot and sent Starscourge Radahn into an unending fit of madness after Malenia unleashed its power during their battle. It may be assumed that the God of Scarlet Rot is sealed away beneath the Lake of Rot, as the location's provided map description states "It is said that the divine essence of an outer god is sealed away in this land." Gowry and the Pests are associated with the Sealed God.

    === "The Dark Moon, the Stars and the Primeval Current"

        The Dark Moon, also known as the Full Moon and the Black Moon of Nokstella, is an entity who has wisdom, can bewitch and is feared by Ranni and the Snow Crone based on the Glintstone Icecrag spell. As one needs to be sentient to be said to have wisdom, the Dark Moon is likely an entity with its own consciousness and therefore qualifies as an outer god, a loose term used to describe any otherworldly sentient entity beyond the Lands Between.

        The ancient people of the Eternal City venerated the Dark Moon. The Dark Moon was also likely involved in the coming or the creation of a Lord of Night based on the Night Maiden Armor and the Mimic Tear Ashes. The Dark Moon is said to be "the guide of countless stars" based on the Moon of Nokstella item, and may be one of the key entities through which the primeval current of the void was channeled to the ancient astrologers and sorcerers of the Eternal Cities. 

        The primeval sorcerers were known to create "graven mass" - an abomination made of the heads of sorcerers amalgamated with glintstone - in order to transform into a seed that will become a "star". A graven mass appears to possess a collective consciousness and is star-like in appearance when viewed from afar. Whether the Dark Moon is a "graven mass" made by the ancient sorcerers of the Eternal City is unclear, however we do know that the Eternal City was experimenting with artificial life creation or modification (which is said to be a mockery of life or a crude imitation). The reason the primeval sorcerers want to become part of the Stars is obvious, as the Stars are said to command the fates of the people and even gods based on the Amber Starlight. This control over the fates was removed from the Stars with the advent of the Greater Will and the Golden Order.

        The Stars are said to have "residual life" based on Sellen. Therefore, the art of glintstone sorcery is essentially the art of using the life force contained within the Stars to power sorceries. Indeed, the Stars harbor sentient life such as Astel and Fallingstar Beast. 

        The Primeval Current is the life-bearing energy that flows through the stars and the abyss. This energy can harbor new life like Astel (a "natural-born"). In addition, this energy can be harnessed to transform a human sorcerer into a glintstone being (like what Azur and Lusat eventually become), or to ascend a group of human sorcerers into a graven mass to become, if successful, a new star with a collective consciousness.

        This power is involved in the "Age of Stars" ending.

=== "Possible Outer Gods"
    
    These gods are elaborated upon very little and have not been confirmed to be Outer Gods, but are associated with powers equivalent to that of the Outer Gods.

    === "The One-Eyed God of Fire"
        
        A god worshiped by the Fire Giants, granting them extremely powerful fire magic. It is associated with Fire Giant and Fire Monk Incantations. While there is a possibility that this god is the same as the Frenzied Flame, it is unlikely as the fire magic used by the Fire Giants and followers of the Frenzied Flame are different colors, and while Melina is happy to use the power of the Giant's Flame to burn the Erdtree, she turns against the player if the Frenzied Flame is embraced. However, this means the Giant's god is not associated with one of the current endings.

        It is also potentially significant that the visual motif found in the eye of the fire giant (one inner eye surrounded by eight smaller circles) is found in several other places in the Lands Between including on Godrick the Grafted's crown and on the crenellations at the top of the divine towers.
    
    === "The Blood Star"

        An enigmatic entity associated with Aberrant Sorceries, said to have been discovered by "the guilty" amidst eternal darkness, after their eyes were gouged out by thorns (described in Briars of Punishment). It is associated with red glintstone, which was said to have been discovered by those exiled to the north for their crimes. Mad Tongue Alberich wears a set of clothing that boosts the effects of its sorceries. The Blood Star is also associated with human sacrifices, as red glintstone is said to have been formed by their blood (Alberich's Set; Staff of the Guilty). Aberrant sorcery and the use of red glintstone is said to be a practice reviled at the academy, as it employs faith as well as intelligence (Briars of Sin).  Practitioners of aberrant sorcery, wielding Staffs of the Guilty, often accompany Fire Monks, and red glintstone is employed by the blasphemous practitioners of Mt Gelmir, who employ it in their own volcanic sorceries (Gelmir Glintstone Staff). The Staff of the Guilty also strongly resembles the large wooden stakes used to kill the Fire Giants.
        
        It is possible that this is another title for the Formless Mother, given shared themes of self-injury and sacrifice; however, Mohg's Sanguine Nobles do not appear to employ red glintstone or the tools necessary to create it, and their desire for luxury, power and love is at odds with the maddened, self-flagellating attitude of aberrant sorcerers. Also, despite the close relationship between flame and aberrant sorcery, users of red glintstone do not appear to employ bloodflame.
    
    === "Unnamed God of the Deathbirds"

        An Outer God related to the Deathbirds, and possibly embodying Destined Death itself. The description for the Twinbird Kite Shield says the "twinbird is said to be the envoy of an outer god." It is possibly associated with the Black Flame, the Godskin (including the Godskin Apostle Incantations), and a figure known as the "Gloam" or "Dusk" eyed queen who ruled the Godskin before being "defeated" by Maliketh.

        Several considerations point to Death being associated with an Outer God's power in some capacity: The Black Knife and Maliketh's Black Blade deal holy damage, and Black Blade is an Erdtree Incantation and deals holy damage, as do the Godslayer Incantaions. However, it is somewhat unclear whether death is an independent Outer God; an aspect of the Greater Will; or some different kind of power. The Death Sorceries and their references to the Death Birds suggests there is a sidereal dimension to death as well. Unlike the Magma Sorceries, which descend from Rennala through Rykard's work and are conceptually different than the power of the Giant's Flame incantations, the Death Sorceries are described as being the way death was handled in the Lands Between prior to the Erdtree and, presumably, the Golden Order. It may be that Death is simply a type of power (or rite) manifested by whatever regime is currently dominating the Lands Between.
    
    === "Unnamed Ancient God of Dragons"

        A god worshiped by Dragonlord Placidusax during his time as Elden Lord in an age before the Erdtree. Associated with Dragon Cult and Dragon Communion Incantations. It is unclear whether Placidusax was an Elden Lord in the same sense as, say, Godfrey, i.e., as a demigod embodying the power of a separate outer god or whether it merely indicates that the primal power of the dragons was equivalent to that of an Outer God while they reigned.
