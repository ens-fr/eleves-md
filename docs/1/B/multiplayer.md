# Multiplayer Coop & online

---

Multiplayer Coop and Online for Elden Ring contains information on the various features contained within online mode. Elden Ring Multiplayer contains various modes, from cooperative game styles in PvE, to competitive game modes in PvP and Invasions. Online players can make Gestures that may even include some vocal expression and every player at close range will hear them.

Guidelines, various mechanics and any other online information can be found on this page.

---

## Elden Ring Multiplayer Coop & Online Features

---

- 1 to 4 Players
- Cooperative PvE
- PvP
- See Summon Range Calculator
- See Recommended Level by Location
- See Effigies of the Martyr for Summoning Pool List.

!!! note "Invasions" 
    Note that the invasion mechanic is also present in the single-player campaign, even when offline. This means that you will still be invaded by Invader NPCs at specific times, just as you would by other players. These invasions, however, often bear a relation to the game's story.

---

## Elden Ring Multiplayer Guide

---

### Message Menu

The message menu allows you to write messages that players in other worlds can read at the same spot that you left them. Elden Ring allows you to change the format of the message and add Gestures to the message as well.

Players can rate other people's messages. When a message is rated, the HP of the player that left the message will be replenished.

### Multiplayer items 

Players are able to use Multiplayer Items to engage in the different types of multiplayer experiences. This can be done from either the Inventory or the Multiplayer Menu. Here are all the existing Multiplayer Items and their effects.

|Blue Cipher Ring|White Cipher Ring|Furlcalling Finger Remedy|Finger Severer|
|:-:|:-:|:-:|:-:|
|![](imagemulti/blue_cipher_ring_multiplayer_items_elden_ring_wiki_guide_200px.png){width=150px}|![](imagemulti/white_cipher_ring_multiplayer_items_elden_ring_wiki_guide_200px.png){width=150px}|![](imagemulti/furlcalling_finger_remedy_tools_consumables_elden_ring_wiki_guide_200px.png){width=150px}|![](imagemulti/finger_severer-elden-ring-wiki-guide.png){width=150px}|
|Puts you into a Ready state to answer, should someone in another world call for help via a White Cipher Ring. You will be summoned to their world as a Hunter and multiplayer will begin once you are summoned. Your goal will be to defeat invading players. Purchasable at Roundtable Hold.|Automatically requests the help of a Hunter from another world when invaded by a Bloody Finger. Can also be used after being invaded to summon a Hunter. Summoning rescuers may not always be possible. Purchasable at Roundtable Hold.|Reveals Summon Signs to invoke players from other worlds. Cooperative Summon Signs are shown in Gold, while competitive signs are Red. For either type of multiplayer the summoning player becomes Host of Fingers. Furlcalling Finger Remedy can be crafted using two Erdleaf Flowers.|Use as a Host of Fingers to select a summoned player and send them back to their world. Use when you have been summoned to another player's world to return to your own world.|

|Tarnished's Furled Finger|Duelist's Furled Finger|Tarnished's Wizened Finger|Bloody Finger|
|:-:|:-:|:-:|:-:|
|![](imagemulti/tarnisheds_furled_finger_multiplayer_items_elden_ring_wiki_guide_200px.png){width=150px}|![](imagemulti/duelists_furled_finger_multiplayer_items_elden_ring_wiki_guide_200px.png){width=150px}|![](imagemulti/tarnisheds_wizened_finger_multiplayer_items_elden_ring_wiki_guide_200px.png){width=150px}|![](imagemulti/bloody_finger_multiplayer_items_elden_ring_wiki_guide_200px.png){width=150px}|
|Creates a summon sign for cooperative multiplayer. Arrive as a cooperator (Furled Finger) with the objective of defeating the area boss of the world to which you were summoned.|Leaves a Red Summon Sign to play competitive multiplayer. This sign will appear in the world of other players so they may summon you as a Duelist Adversary. Your objective will be to defeat the Host of Fingers of the world to which you were summoned. Leaving a second Summon Sign will vanish the old one. Found on a corpse at Stormhill.| Use to write messages. Your messages will be conveyed to other worlds, allowing other players to read them.|Attempts to invade another player's world. If the invasion is successful, the competitive multiplayer will begin, with you as a Bloody Finger. The goal is to defeat the Host of Fingers of the other world. It is possible to re-invade the world you previously invaded. Obtained by completing White-Faced Varre's questline.|

|Taunter's Tongue|Recusant Finger|Small Golden Effigy|Small Red Effigy|
|:-:|:-:|:-:|:-:|
|![](imagemulti/taunters_tongue_multiplayer_items_elden_ring_wiki_guide_200px.png){width=150px}|![](imagemulti/recusant-finger-tools-elden-ring-wiki-guide.png){width=150px}|![](imagemulti/small_golden_effigy_tools_elden_ring_wiki_guide_200px.png){width=150px}|![](imagemulti/small_red_effigy_tools_elden_ring_wiki_guide_200px.png){width=150px}|
|Beckons Bloody Fingers to come invade your world. This allows you to be invaded even without a Furled Finger cooperator present, and reduces the amount of time before re-invasion is possible. It also allows for a second invader to join multiplayer. (With two invaders, the maximum number of cooperators becomes one.)|Attempts an invasion of another player's world. If successful, you will arrive as an invader (Recusant) with the objective of defeating the Host of Fingers of that world. It can be found on a table in one of the rooms of  Volcano Manor. |Send a cooperative summon sign to several nearby summoning pools (activated pools only). In cooperative multiplayer, your objective will be to defeat the area boss of the world to which you were summoned.|Sends competitive sign to several nearby summoning pools (activated pools only). This sign will appear in the world of other players so they may summon you as a Duelist Adversary. Your objective will be to defeat the Host of Fingers of the world to which you were summoned. Found on a corpse at Stormhill.|

|Festering Bloody Finger|Mohg's Great Rune|Phantom Great Rune|
|:-:|:-:|:-:|
|![](imagemulti/festering-bloody-finger_multiplayer-items-elden-ring-wiki-guide-200.png){width=150px}|![](imagemulti/mohgs-great-rune-key-item-elden-ring-wiki-guide.png){width=150px}|![](imagemulti/phantom_great_rune_elden_ring_wiki_guide.png){width=150px}|
|Attempts to invade another player's world. If the invasion is successful, the competitive multiplayer will begin, with you as a Bloody Finger. The goal is to defeat the Host of Fingers of the other world. This item is consumable.|A Great Rune of the shardbearer Mohg. Its blessing grants a blessing of blood to summoned phantoms, and imparts a Phantom Great Rune upon successful invasion. Mohg and Morgott are twin brothers, and their Great Runes are naturally similar. But Mohg's rune is soaked in accursed blood, from his devout love for the wretched mire that he was born into far below the earth.|Item for online play. Obtained after using the Mohg's Great Rune and invading another world. Consumed upon use. Enemies inside the invaded world receive a blessing of blood, which boosts their attack power when blood loss occurs nearby. Also, your HP recovers when blessed enemies defeat a player.|

!!! note "Multiplayer menu"
    From the Multiplayer menu, the Blue Cipher Ring, White Cipher Ring, Furlcalling Finger Remedy, Finger Severer, Tarnished's Furled Finger, Duelist's Furled Finger, Tarnished's Wizened Finger, Bloody Finger, Taunter's Tongue, Small Golden Effigy, Small Red Effigy and Festering Bloody Finger can also be used .

---

## PVP Rewards in Elden Ring

---

Defeating an opponent in PvP will reward the player with Runes. The amount of Runes is a fixed amount depending on the defeated opponent's Level; It is a percentage of that player's most-recent leveling cost (eg. a level 50 player drops some % of the cost of leveling up from level 49 to 50). However, the percentage awarded varies depending on the perspective of the player and the type of multiplayer being conducted.

=== "15% Rewards "
    - Received by hosts when an invader is defeated (who invaded via the Bloody Finger or Festering Bloody Finger) 

=== "5% Rewards " 
    - Received by co-op summons when an invader is defeated  
    - Received by hunters when an invader is defeated (summoned via White Cipher Ring / Blue Cipher Ring)  

=== "4% Rewards" 
    - Received by invaders when a host, co-op summon, or hunter is defeated  

=== "2% Rewards"
    - Received by hosts when a duelist is defeated (who was summoned via the Duelist's Furled Finger)  

=== "1% Rewards"

    - Received by co-op summons when a duelist is defeated  
    - Received by duelists when a host or co-op summon is defeated  
    - Received by invaders when another invader is defeated  
