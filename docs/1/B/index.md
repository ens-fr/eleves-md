# Main Bosses

---

!!! warning "spoil"
    If you don't want to be spoiled on the main story bosses, don't look at the following information on the whole page.

!!! info "info about the page"
    
    On this page you will find information about the main bosses and their lore, but you will not find information about how to beat them or the different techniques to beat them.

=== "Main Boss n°1"

    ##Godrick the Grafted
    
    ![Godrick the Grafted](imageboss/godrick_the_grafted.jpg){ align=left width=600px }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 20,000 Runes/ 66,000 Runes(NG+1)/ 80,000 Runes(NG+2)|
    |![Godrick's Great Rune](rune/godricks-great-rune-key-item-elden-ring-wiki-guide.png){ width=79px align=left } Godrick's Great Rune|
    |![Remembrance of the Grafted](remembrance/remembrance_of_grafted_elden_ring_wiki_guide_200px.png){ width=79px align=left } Remembrance of the Grafted|

    Godrick the Grafted is a Boss in Elden Ring. Godrick the Grafted is the ruler of Stormveil Castle, and the final boss of this area. You don't need to initially fight Godrick the Grafted to progress into the next area of Elden Ring, and can instead opt to face him at a later part of the game. He drops Remembrance of The Grafted when he is defeated.

    !!! cite "Godrick the Grafted" 
        Mighty Dragon, thou'rt a trueborn heir. Lend me thy strength, o kindred. Deliver me unto greater heights.  
        ...Well. A lowly Tarnished, playing as a lord. I command thee, kneel! I am the lord of all that is golden!

    Godrick the Grafted is a Legacy Dungeon Boss found in Stormveil Castle.

    - This boss is optional, as only two shardbearers must be defeated to gain access to Leyndell Castle.
    - This boss is a **Demi-God**
    - Closest Site of Grace: Secluded Cell
    - Multiplayer is allowed for this boss
    - Spirit ashes are allowed for this boss
    - NPC Summons: Nepheli Loux. Locate her at her first location and speak to her three times or exhaust her dialogue to have her available to summon

    Elden Ring Godrick the Grafted Lore :

    - Godrick was described as the weakest of the demigods, as said by Enia.

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 0||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 20||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 318||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 318|
    |Phy (Slash): 0||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 20||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 318||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 0||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 20||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 318||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 0||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 318|||

=== "Main Boss n°2"
    
    ## Rennala Queen of the Full Moon

    ![Rennala Queen of the Full Moon](imageboss/rennala_queen_of_the_full_moon_bosses_elden_ring_wiki_600px.jpg){ align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 40,000 Runes/ 120,000 Runes(NG+1)/ 132,000(NG+2)|
    |![Great Rune of the Unborn](rune/great-rune-of-the-unborn-key-item-elden-ring-wiki-guide.png){ width=78px align=left } Great Rune of the Unborn|
    |![Remembrance of the Full Moon Queen](remembrance/remembrance_of_the_full_moon_queen_elden_ring_wiki_guide_200px.png){ width=78px align=left } Remembrance of the Full Moon Queen|

    Rennala, Queen of the Full Moon is a Boss in Elden Ring. Though not a demigod, Rennala, Queen of the Full Moon is one of the Great Rune bosses that players may have to defeat in order to advance the story forward. She is found in the Academy of Raya Lucaria. 

    !!! cite "Melina"
        Rennala is the last Carian Queen, imprisoned in the Grand Library. Still distraught over Radagon’s departure, she obsesses over rebirth, and her scholars will protect her at all costs. But beware, should you witness the true power of the Full Moon Queen.

    Rennala, Queen of the Full Moon is a **Legend** Boss found in the Raya Lucaria Academy in Liurnia of the Lakes.

    - This boss is optional, as only two shardbearers must be defeated to gain access to Leyndell.
    - Closest Site of Grace: Debate Parlor (after shortcut is open)
    - Multiplayer is allowed for this boss
    - You can summon Spirit ashes for this boss

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): -10||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 80||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 327 (535)||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
    |Phy (Slash): -10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 20 (40)||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 327 (535)||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 0||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 20 (40)||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 327||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): -10||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 20 (40)||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 327 (535)|||
    
    _Values in brackets correspond to Phase 2._


=== "Main Boss n°3"

    ## Starscourge Radahn
    
    ![Starscourge Radahn](imageboss/radahn_enemies_elden_ring_wiki_600px.jpg){ align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 70,000 Runes/ 210,000 Runes(NG+1)|
    |![Radahn's Great Rune](rune/radahns-great-rune-key-item-elden-ring-wiki-guide.png){ width=90px align=left } Radahn's Great Rune|
    |![Remembrance of the Starscourge](remembrance/remembrance_of_the_starscourge_elden_ring_wiki_guide_200px.png){ width=90px align=left } Remembrance of the Starscourge|
    
    Starscourge Radahn is a Boss in Elden Ring. Starscourge Radahn is a massive and destructive demigod located in Caelid, where he can be accessed from Redmane Castle after activating the Radahn Festival by progressing far enough in Ranni's questline, or activating any site of grace in Altus Plateau. Defeating this Boss is mandatory for at least one ending of the game and to complete certain NPC questlines. However he does not necessarily have to be defeated to finish the game.

    !!! cite "Witch-Hunter Jerren"
        The famed Red Lion General has been reduced to a mindless beast, devouring corpses in the dunes. Uses gravity magic with extreme skill along with his brute strength, earning him the title of Starscourge.

    Starscourge Radahn is a **Demi-God** Boss found in Wailing Dunes

    - This is an optional Boss, depending on ending desired
        - only two shardbearers must be defeated to gain access to Leyndell Castle
        - required for at least one of the game's possible endings
    - Closest Site of Grace: Chamber Outside the Plaza
    - Multiplayer is allowed for this Boss
    - You can't summon Spirit Ashes for this Boss
    - This Boss can be stance broken, allowing for a critical hit
    - You can ride Torrent for this Boss

    NPC Summons: 

    - Blaidd
    - Iron Fist Alexander
    - Patches
    - Great Horned Tragoth
    - Bloody Finger Okina
    - Lionel the Lionhearted
    - Finger Maiden Therolina

    Starscourge Radahn Lore :  

    - Radahn is one of the 3 children of Radagon and Rennala (along with Ranni and Rykard), making him one of the 8 Demi-Gods of the Lands Between. This is especially obvious considering that he has his father’s red hair, and his mother’s fascination for the stars (leading to his mastery of gravity and nickname Conqueror of the Stars).  
    - He is one of the last Demi-Gods at war for control of the Great Runes after the shattering of the Elden Ring. His final duel with Malenia resulted in their mutual demise and the land of Caelid becoming plagued with scarlet rot (with which Malenia was afflicted at birth).  
    - He was accidentally nerfed, correcting his hitboxes, but reducing his damage output. The damage has been returned to normal, and his hitboxes are now fixed.  
    - According to the game files, his horse’s name is Leonard. He learned gravity magic in order not to impair his horse, now that he had grown too large to properly ride it.

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 10||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 20||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 335||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 548|
    |Phy (Slash): 10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 20||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 243||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 10||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 20||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 335||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 0||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 335|||

    !!! tip "The first is French and name Sardoche"

        Here is **Sardoche**, the first man (first world) and french to have defeated _Starscourge Radahn_, **with no hit, no magic and no summon**.

        <iframe width="794" height="447" src="https://www.youtube.com/embed/ytWuyJVeY5Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


=== "Main Boss n°4"

    ## Regal Ancestor Spirit

    ![Regal Ancestor Spirit](imageboss/regal_ancestor_2_bosses_elden_ring_wiki_600px.webp){ width=586px align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 24,000 Runes|
    |![Remembrance of the Regal Ancestor](remembrance/remembrance_of_the_regal_ancestor_elden_ring_wiki_guide_200px.png){ align=left } Remembrance of the Regal Ancestor|

    Regal Ancestor Spirit is a Boss in Elden Ring. Regal Ancestor Spirit is a large animated carcass of a deer, just like the Ancestor Spirit found in Siofra River. This boss, however, is found in Nokron, Eternal City and has access to more skills than its counterpart. This is an optional boss, as players don't need to defeat it in order to advance in Elden Ring. However, doing so yields useful Items and Runes.

    !!! cite "Melina"
        A spirit glowing in pale blue light, taking the shape of a huge deer. Moves with grace and attacks ferociously. Capable of draining the life of spirits and regaining heath.

    Regal Ancestor Spirit is a **Legend** Boss found in Nokron, Eternal City.

    - This is an optional Boss
    - Closest Site of Grace: Ancestral Woods
    - Multiplayer is allowed for this boss
    - You can summon Spirit ashes for this boss

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 0||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 20||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 1,169.6||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
    |Phy (Slash): -10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: -20||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 1,169.6||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 0||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 0||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 1,169.6||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 0||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: -20||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 1,169.6|||


=== "Main Boss n°5"

    ## Lichdragon Fortissax

    ![Lichdragon Fortissax](imageboss/fortissax_bosses_elden_ring_wiki_600px.jpg){ width=586px align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png)  90,000 Runes|
    |![Remembrance of the Lichdragon](remembrance/remembrance_of_the_lichdragon_elden_ring_wiki_guide_200px.png){ align=left } Remembrance of the Lichdragon|

    Lichdragon Fortissax is a Boss in Elden Ring. Lichdragon Fortissax is a massive four-winged black dragon wielding powerful lightning attacks, and is found in Deeproot Depths. This boss is optional, but is required for finishing Fia's questline and unlocking the "Age of Duskborn" ending.

    Lichdragon Fortissax is a **Legend** Boss found in Deeproot Depths.

    - Required for "Age of Duskborn" ending, optional for other endings
    - Closest Site of Grace: "Prince of Death's Throne"
    - Multiplayer is allowed for this boss
    - Spirit ashes can be summoned for this boss

    Lichdragon Fortissax Lore :

    - Fortissax is the sibling of Ancient Dragon Lansseax.
    - Fortissax and Godwyn were said to be good friends, as Godwyn had bested him in battle, which explains why Fortissax guards his corpse.


    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 35||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 40||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 552||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
    |Phy (Slash): 35||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 40||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 552||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 35||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 80||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 552||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 10||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 80||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 552|||


=== "Main Boss n°6"
    
    ## Morgott the Omen King

    ![Morgott the Omen King](imageboss/morgott_the_omen_king_bosses_elden_ring_wiki_600px.jpg){ align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png)  120,000 Runes/ 360,000 Runes(NG+1)/ 297,001 Runes(NG+2)|
    |![Morgott's Great Rune](rune/morgotts-great-rune-key-item-elden-ring-wiki-guide.png){ width=79px align=left } Morgott's Great Rune|
    |![Remembrance of the Omen King](remembrance/remembrance_of_the_omen_king_elden_ring_wiki_guide_200px.png){ width=79px align=left } Remembrance of the Omen King|

    Morgott, The Omen King is a Boss in Elden Ring. Morgott is the true identity of Margit, the Fell Omen and the self-proclaimed "Last of All Kings", found in Leyndell Royal Capital. This is **NOT** an optional boss as players **MUST** defeat it to advance.

    !!! cite "Morgott the Omen King"
        Have it writ upon thy meagre grave:  
        Felled by King Morgott! Last of all kings.

    Morgott, The Omen King is a **Demi-God** Boss found in Leyndell, Royal Capital.

    - Closest Site of Grace: Queen's Bedchamber
    - Multiplayer is allowed for this boss
    - Spirit Ashes are summonable for this boss fight
    
    NPC Summons: 

    - Melina. She can be summoned for this fight, her summon sign is at the top of the stairs on the right, in front of the fog gate. She wields the Blade of Calling and uses faith based Erdtree/healing incantations.
    - Dung Eater. Ensure Dung Eater is released during his questline in order to allow him to participate. 

    Morgott, The Omen King Lore :
    
    - Morgott is the twin brother of Mohg, Lord of Blood. He does not mention him as a traitor, likely due to the fact that they were both shunned and sent underground, and thus he lacks a seat at the Elden Throne.  
    - He is the son of Godfrey and Marika, but was shunned and imprisoned below, due to his Omen nature. He took on the moniker of Margit to hunt down enemies of the Erdtree, possibly due to not wanting others to learn of his appearance. This is most likely why he is referred as, “The Veiled Monarch.”  

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 0||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 0||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 337.7||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 552.6|
    |Phy (Slash): -10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 0||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 337.7||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 0||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 0||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 337.7||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 0||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 337.7|||

=== "Main Boss n°7"

    ## Rykard, Lord of Blasphemy

    ![Rykard, Lord of Blasphemy](imageboss/3952983-eldenringlordrykardguidethumb.jpg){ width=600px align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 100,000 Runes|
    |![Rykard's Great Rune](rune/rykards-great-rune-key-item-elden-ring-wiki-guide.png){ width=90px align=left }  Rykard's Great Rune|
    |![Rykard's Great Rune](remembrance/remembrance_of_the_blasphemous_elden_ring_wiki_guide_200px.png){ width=90px align=left }  Rykard's Great Rune |

    Rykard, Lord of Blasphemy is a Boss in Elden Ring. Rykard, Lord of Blasphemy is the true form of the God-Devouring Serpent, whose body has been taken over by Rykard, and is found in Mt. Gelmir. This is an optional boss as players don't need to defeat it to advance in Elden Ring.

    Rykard, Lord of Blasphemy is first fought in his First Phase as the God-Devouring Serpent. Unlike most boss phases, the God-Devouring Serpent has its own health bar which must be depleted before you can challenge the second phase. Note that dying to his second phase as Rykard will require you to defeat the serpent once again.

    !!! cite "Rykard, Lord of Blasphemy"
        Join the Serpent King, as family... Together, we will devour the very gods!
    
    Rykard, Lord of Blasphemy is a **Demigod** Boss found in Mt. Gelmir.

    - This boss is optional, as only two shardbearers must be defeated to gain access to Leyndell.
    - Closest Site of Grace: Audience Pathway
    - Multiplayer is allowed for this boss
    - Spirit ashes can be summoned for this boss

    Rykard, Lord of Blasphemy Lore :

    - He is the son of Rennala and Radagon, but chose to go against the Erdtree, and eventually gave himself to the serpent.

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 10||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 40||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 556,9||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 556,9|
    |Phy (Slash): 10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 80||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 556,9||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 35||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 20||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 556,9||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 10||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 247,5|||

=== "Main Boss n°8"
    
    ## Astel Naturalborn of the Void

    ![Astel Naturalborn of the Void](imageboss/astel.jpg){ width=480px align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 80,000 Runes/ ~250,000 Runes(NG+2)|
    |![Remembrance of the Naturalborn](remembrance/remembrance_of_the_naturalborn_elden_ring_wiki_guide_200px.png){ align=left } Remembrance of the Naturalborn|

    Astel, Naturalborn of the Void is a Boss in Elden Ring. Astel, Naturalborn of the Void is a malformed star which landed in the Lands Between long ago, and is found in Grand Cloister, Lake of Rot. This boss is optional, as players don't need to defeat it in order to advance in Elden Ring, but it is a required boss for Ranni the Witch's questline.

    !!! cite "Ranni the Witch"
        A creature born from the void, who assailed the Eternal City and stole their sky. This malformed star of ill-omen possess immense power over gravity and the stars. 
        
    Astel, Naturalborn of the Void is a **Legend** Boss found in Lake of Rot.

    - This boss is optional, but it is required for Ranni's questline
    - Closest Site of Grace: Lake of Rot Shoreside
    - Multiplayer is allowed for this boss
    - You can summon Spirit Ashes for this boss

    Astel, Naturalborn of the Void Lore :

    - The description of the Remembrance of the Naturalborn implies a connection between Astel and the Fallingstar Beast, both being creatures that fell from the stars.

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 10||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 40||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 552,6||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
    |Phy (Slash): 10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 40||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 552,6||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 10||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 40||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 552,6||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 10||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: Immune|||

!!! info "help"
    The absorption numbers are the % of your damage that gets blocked. For example, if an absorption is 60, 40% of that damage by that type will go through and 60% is absorbed. Bigger number = less damage. An absorption of 100 means no damage goes through, and a resistance of -100 mean the enemy takes 2x damage from that source. 0 means damage goes pretty much as is.

    The resistance numbers are the buildup amount to trigger it. 
    !!! example "resistance is 100"
        if a resistance is 100 you must deal 100 points of whatever aux buildup to trigger it. Note that these go down over time, and increase each time the effect procs.

=== "Main Boss n°9"

    ## Fire Giant

    ![Fire Giant](imageboss/fire_giant.jpg){ width=500px align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 135,000 Runes/ 360,000 Runes(NG+1)/ 396,000 Runes(NG+2)|
    |![Remembrance of the Fire Giant](remembrance/remembrance_of_the_fire_giant_elden_ring_wiki_guide_200px.png){ width=135px align=left } Remembrance of the Fire Giant|

    Fire Giant is a Boss in Elden Ring. Fire Giant, the last known survivor of the War against the Giants, is a massive humanoid creature with a face on its chest, and is found in Mountaintops of the Giants. This is **NOT** an optional boss as players **MUST** defeat it to advance to the Forge of the Giants.

    !!! cite "Melina"
        Upon realizing the flames of their forge would never die, Queen Marika marked him with a curse. "O trifling giant, mayest thou tend thy flame for eternity."

    Fire Giant is a **Legend** Boss found in Mountaintops of the Giants.

    - Closest Site of Grace: Foot of the Forge
    - Multiplayer is Allowed
    - Spirit ashes can be summoned for this boss
        
    NPC Summons:

    - Iron Fist Alexander. Can be summoned to help, though this disables Torrent.

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 0||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 0||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 565.7||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 565.7|
    |Phy (Slash): -10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 50||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 565.7||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 0||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 0||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 565.7||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 0||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 20||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 1,216.7|||

=== "Main Boss n°10"

    ## Mohg, Lord of Blood

    ![Mohg, Lord of Blood](imageboss/mohg.jpg){ width=500px align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 315,000 Runes/ 840,000 Runes(NG+1)|
    |![Mohg's Great Rune](rune/mohgs-great-rune-key-item-elden-ring-wiki-guide.png){ width=135px align=left } Mohg's Great Rune|
    |![Remembrance of the Blood Lord](remembrance/remembrance_of_the_blood_lord_item_elden_ring_wiki_guide_200px.png){ width=135px align=left } Remembrance of the Blood Lord|

    Mohg, Lord of Blood is a Boss in Elden Ring. Mohg, Lord of Blood is an extremely powerful Omen specializing in blood magic and is found in Mohgwyn Palace inside Siofra River. This is an optional boss as players don't need to defeat it to advance in Elden Ring.

    !!! cite "Mohg, Lord of Blood"
        Dearest Miquella. You must abide alone a while.  
        Welcome, honored guest. To the birthplace of our dynasty!

    Mohg, Lord of Blood is a **Demigod** boss found in Mohgwyn Palace.

    - This is an optional boss
    - Closest Site of Grace: Dynasty Mausoleum Midpoint
    - Multiplayer is allowed for this boss
    - Spirit Ashes are allowed for this boss
    - Killing him locks you out of getting War Surgeon set

    Mohg, Lord of Blood Lore :

    - While initially appearing as simply another Shardbearer, Mohg's true intention was to raise Miquella to godhood and become his consort, whom he had stolen from the Haligtree.  
    - Mohg is the twin brother of Morgott, the Omen King. Because they were both hidden underground, they lack seats at the Elden Throne, which may explain why Morgott does not deem him a traitor.  
    - Mohg gained his power via communion with an Outer God known as the Formless Mother.  

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 10||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 40||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 653,9||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 1406,5|
    |Phy (Slash): 10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 80||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 653,9||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 10||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 40||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 290,6||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 10||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 653,9|||

=== "Main Boss n°11"
        
    ## Malenia Blade of Miquella

    ![Malenia Blade of Miquella](imageboss/malenia%2C-blade-of-miquella.jpg){ width=500px align=left }  
    ![Malenia Blade of Miquella phase 2](imageboss/Malenia%20p2.jpg){ width=500px align=left }
        
    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 360,000 Runes/ 960,000 Runes(NG+1)/ 1,056,000 Runes(NG+2)|
    |![Malenia's Great Rune](rune/malenias-great-rune-key-item-elden-ring-wiki-guide.png){ align=left } Malenia's Great Rune|
    |![Remembrance of the Rot Goddess](remembrance/remembrance_of_the_rot_goddess_elden_ring_wiki_guide_200px.png){ align=left } Remembrance of the Rot Goddess|
        
    !!! warning "difficulty"
         Malenia is the hardest Boss of Elden Ring and considered as THE boss to beat to prove your level on the game and show that you are able to take the challenge. Beware, it may take you several days to complete this challenge if you are not a fan of difficulty or FromSoftware games.  
         Good luck.

    Malenia, Blade of Miquella is a Boss in Elden Ring. Born as a twin to Miquella, Malenia is known as the most powerful of the Empyreans. Wielding a prosthetic arm and leg, Malenia is located in Elphael, Brace of the Haligtree. This boss is optional, as players do not need to defeat it in order to advance in Elden Ring. Malenia is also the feature of the game's Collector's Edition, which includes a statue of her and a replica of her helmet.

    !!! cite "Malenia Blade of Miquella"
        ... Heed my words. I am Malenia. Blade of Miquella. And I have never known defeat.

    Malenia Blade of Miquella is a **Demi-God** boss found at the bottom of Elphael, Brace of the Haligtree.
        
    - This boss is optional.
    - Closest Site of Grace: Haligtree Roots
    - Multiplayer is allowed for this boss
    - Spirit Ashes can be summoned for this boss

    Elden Ring Malenia Lore :

    - Malenia first unleashed the Scarlet Rot in her battle with Radahn, destroying the region of Caelid in the process.  
    - She is an Empyrean like Ranni and her brother Miquella.  

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 10||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 20||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 1,481||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 688|
    |Phy (Slash): 10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 0||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 1,481||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 10||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 20||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 420||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 10||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 306|||

    !!! tip "Let Me Solo Her"
    
        **Let Me Solo Her** is a player who can be sommon at the enter of Malenia boss fight, and if you summon him, he will fight her alone.  
        he challenged himself to **beat Malenia 1000 times**, he did it, on stream. After doing so, he restarts the game in New Game + mode, does he have an idea in mind? Case to follow.  
        But for now **he definitely fits into the history of Elden Ring and FromSoftware** by being the hero of some players by helping them beat this (infamous) bump and achieving this feat of killing it 1000 times.  

        <iframe width="749" height="449" src="https://www.youtube.com/embed/Gkf9EGhzw_U" title="Let me solo her 1000th Malenias slain" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

=== "Main Boss n°12"
        
    ## Dragonlord Placidusax

    ![Dragonlord Placidusax](imageboss/dragonlord-placidusax-boss-elden-ring-wiki-guide.jpg){ width=300px align=left }

    |Drops|
    |:-:|
    |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 280,000 Runes/ 560,000 Runes(NG+1)|
    |![Remembrance of the Dragonlord](remembrance/remembrance_of_the_dragonlord_item_elden_ring_wiki_guide_200px.png){ width=170px align=left } Remembrance of the Dragonlord|

    Dragonlord Placidusax is a Boss in Elden Ring. Dragonlord Placidusax is a massive, two-headed dragon and is found in Crumbling Farum Azula. This is an optional boss, as players don't need to defeat it in order to advance in Elden Ring. However, doing so yields useful Items and Runes.

    !!! cite "Melina"
        A massive in size, two-headed dragon capable of immense acts of annihilation.

    Dragonlord Placidusax is a **Legend** Boss found in Crumbling Farum Azula

    - This is an optional Boss
    - Closest Site of Grace: Beside the Great Bridge
    - Multiplayer is allowed for this Boss
    - You can summon Spirit Ashes for this Boss

    Dragonlord Placidusax Lore :

    - According to the Old Lord’s Talisman, he is depicted as having four heads. He appears to have lost two of his heads in some sort of conflict, possibly against Godfrey or some other foe.  
    - Placidusax is said to be the previous Elden Lord before the appearance of the Erdtree. 

    | |Absorptions| | | |Resistances| |
    |:-:|:-:|:-:| |:-:|:-:|:-:|
    |Phy (Standard): 35||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 40||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 1,349.5||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
    |Phy (Slash): 35||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 40||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 1,349.5||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
    |Phy (Strike): 35||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 40||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 1,349.5||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
    |Phy (Pierce): 10||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 1,349.5|||

=== "Main Boss n°13"
        
    === "Beast Clergyman"
        
        ## Beast Clergyman
        
        ![Beast Clergyman](imageboss/beast-clergyman-4k.jpg){ width=600px align=left }

        |Drops|
        |:-:|
        |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 220,000 Runes/ 440,000 Runes(NG+1)/ 484,000 Runes(NG+2)|
        |![Remembrance of the Black Blade](remembrance/remembrance_of_the_black_blade_elden_ring_wiki_guide_200px.png){ width=185px align=left } Remembrance of the Black Blade|

        Beast Clergyman is a Boss in Elden Ring. He is the boss form of Gurranq Beast Clergyman and is found in Crumbling Farum Azula. This is **NOT** an optional boss as players **MUST** defeat it to advance in Elden Ring.

        Beast Clergyman is a **Legend** Boss found in Crumbling Faram Azula.

        - Closest Site of Grace: Bestial Sanctum
        - Multiplayer is allowed for this boss
        - Spirit ashes can be summoned for this fight

        Beast Clergyman Lore :

        - This boss is somewhat similar to the Guardian Ape from Sekiro, in the sense that he wildly swings multiple times at the player with large claws (as beasts do). His use of incantations and special abilities make him far different in terms of strategy, however.  

        | |Absorptions| | | |Resistances| |
        |:-:|:-:|:-:| |:-:|:-:|:-:|
        |Phy (Standard): 35||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 40||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 351||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
        |Phy (Slash): 35||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 40||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 351||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
        |Phy (Strike): 35||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 40||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 575||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
        |Phy (Pierce): 35||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 80||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 575|||

    === "Maliketh, the Black Blade"
        
        ## Maliketh, the Black Blade

        ![Maliketh](imageboss/maliketh-the-black-blade-4k.jpg){ width=600px align=left }

        |Drops|
        |:-:|
        |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 220,000 Runes/ 440,000 Runes(NG+1)/ 484,000 Runes(NG+2)|
        |![Remembrance of the Black Blade](remembrance/remembrance_of_the_black_blade_elden_ring_wiki_guide_200px.png){ width=185px align=left } Remembrance of the Black Blade|

        The Beast Clergyman reveals his true form and identity: Maliketh.  
        It's the phase 2 of Beast Clergyman, more difficult and more aggressive.

        !!! cite "Maliketh, the Black Blade"
            O, death... become my blade once more.
            
        Maliketh, The Black Blade is a **Legend** Boss found in Crumbling Farum Azula

        - Closest Site of Grace: Beside the Great Bridge
        - Multiplayer is allowed for this boss
        - Spirit ashes can be summoned for this boss

        Maliketh, The Black Blade Lore :

        - Maliketh was the half-brother and sworn Shadow of Queen Marika, to whom she entrusted the Rune of Death. Ranni the Witch infamously stole a fragment of the Rune of Death to forge the daggers used during the Night of the Black Knives, causing a series of events that resulted in the Shattering. After which, she gave the Blasphemous Claw to her brother Rykard, so that he may have the power to challenge Maliketh.  
        - The ensure that the Rune of Death was never stolen (again), Maliketh bound the blade that contained it to his own flesh, thus becoming eternally hungry for Deathroot in his clergyman disguise.  

        | |Absorptions| | | |Resistances| |
        |:-:|:-:|:-:| |:-:|:-:|:-:|
        |Phy (Standard): 35||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 40||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 351||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
        |Phy (Slash): 35||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 40||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 351||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
        |Phy (Strike): 35||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 40||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 575||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
        |Phy (Pierce): 35||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 80||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 575|||


=== "Main Boss n°14"

    === "Godfrey, First Elden Lord"

        ## Godfrey, First Elden Lord

        ![Godfrey, First Elden Lord](imageboss/godfrey-first-elden-lord-hoarah-loux.jpg){ width=500px align=left }

        |Drops|
        |:-:|
        |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 300,000 Runes/ 450,000 Runes(NG+1)/ 660,000 Runes(NG+2)|
        |![Remembrance of Hoarah Loux](remembrance/remembrance_of_hoarah_loux_elden_ring_wiki_guide_200px.png){ align=left } Remembrance of Hoarah Loux|

        Godfrey, First Elden Lord is a Boss in Elden Ring. This is the physical form of Godfrey, First Elden Lord who is found in Leyndell, Capital of Ash, an instance of the Royal Capital which is accessible only after making significant progress in the story. This is **NOT** an optional boss, as players **MUST** defeat it to advance in Elden Ring.

        !!! cite "Godfrey, First Elden Lord"
            To be granted audience once more. Upon my name as Godfrey, The first Elden Lord!
            
        Godfrey, First Elden Lord is a **Legend** boss found in Leyndell, Capital of Ash.  

        - Closest Site of Grace: Queen's Bedchamber
        - Multiplayer is allowed for this boss
         - You can summon spirits against this Boss

        NPC Summons:

        - Nepheli Loux
        - Shabriri

        Godfrey, First Elden Lord Lore :

        - At the beginning of the fight, Godfrey is seen cradling the shriveled corpse of his son Morgott, slowly dissipating into golden dust. His other sons are Mohg and Godwyn.  
        - Lore suggests that the powerful warrior Horoah Loux was chosen by Queen Marika as her consort (husband), making him the First Elden Lord. Due to this, he took the Beast Regent Serosh upon his back, taming his bloodthirsty and wild nature, and taking the name of Godfrey. 
        - After winning many wars for the Erdtree, he was robbed of his grace, and was banished from the Lands Between, becoming the first Tarnished. 

        | |Absorptions| | | |Resistances| |
        |:-:|:-:|:-:| |:-:|:-:|:-:|
        |Phy (Standard): 0||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 0||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 367.2||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 1292.6|
        |Phy (Slash): -10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 0||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 367.2||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
        |Phy (Strike): 0||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 0||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 601||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
        |Phy (Pierce): 0||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 601|||

        
    === "Hoarah Loux, Warrior"

        ## Hoarah Loux, Warrior

        ![Hoarah Loux](imageboss/hoarah_loux_bosses_elden_ring_wiki_600px.jpg){ align=left }

        |Drops|
        |:-:|
        |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 300,000 Runes/ 450,000 Runes(NG+1)/ 660,000 Runes(NG+2)|
        |![Remembrance of Hoarah Loux](remembrance/remembrance_of_hoarah_loux_elden_ring_wiki_guide_200px.png){ width=185px align=left } Remembrance of Hoarah Loux|

        After being brought down to about 40% health, Godfrey will drop his armor and weapons and enter his second phase where he is known as Hoarah Loux, Warrior.

        !!! cite "Hoarah Loux"
            I've given thee courtesy enough. Now I fight as Hoarah Loux. Warrior!
            
        Hoarah Loux, Warrior is a **Legend** Boss found in Leyndell, Ashen Capital. In this Bossfight, you'll also have to combat Godfrey, First Elden Lord.

        - Closest Site of Grace: Queen´s Bedchamber
        - Multiplayer is allowed for this boss
        - You can summon Spirit ashes for this boss

        NPC Summons:

        - Nepheli Loux
        - Shabriri

        | |Absorptions| | | |Resistances| |
        |:-:|:-:|:-:| |:-:|:-:|:-:|
        |Phy (Standard): 0||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 0||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 367.2||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: 1292.6|
        |Phy (Slash): -10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 0||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 367.2||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
        |Phy (Strike): 0||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 0||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: 601||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
        |Phy (Pierce): 0||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 40||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 601|||


=== "Main Boss n°15"
        
    === "Radagon of the Golden Order"
            
        ## Radagon of the Golden Order

        ![Radagon of the Golden Order](imageboss/radagon-of-the-golden-order-boss-elden-ring-wiki-guide.jpg){ width=400px align=left}

        |Drops|
        |:-:|
        |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 500,000 Runes/ 1,000,000 Runes(NG+1)/ 1,100,000 Runes(NG+2)|
        |![Elden Remembrance](remembrance/elden_remembrance_elden_ring_wiki_guide_200px.png){ width=180px align=left } Elden Remembrance|

        Radagon of the Golden Order is a Boss in Elden Ring. Radagon, the "other half" of Queen Marika, is a tall, fractured god wielding the hammer that shattered the Elden Ring, and is found in the Elden Throne. This boss is **NOT** optional, as it is the first half of the final battle of the game.

        !!! cite "Melina"
            Lord Radagon was a great champion, possessed of flowing red locks. He came to these lands at the head of a great golden host, when he met Lady Rennala in battle. He soon repented his territorial transgressions though, and became husband to the Carian Queen. However. When Godfrey, first Elden Lord, was hounded from the Lands Between, Radagon left Rennala to return to the Erdtree Capital, becoming Queen Marika's second husband and King Consort. Taking the title... of second Elden Lord.
            
        Radagon of the Golden Order is a **God** Boss found in the Elden Throne.

        - Closest Site of Grace: The Elden Throne
        - Multiplayer is allowed for this boss
        - Spirit ashes can be summoned for this boss

        Radagon of the Golden Order Lore :

        - Upon using the Law of Regression incantation on a statue in Leyndell Royal Capital, a message will appear saying "Radagon is Marika".  
        - The description for Marika's Hammer says that Marika shattered the Elden Ring, while Radagon attempted to fix it, suggesting that the two halves are in conflict with each other.  


        | |Absorptions| | | |Resistances| |
        |:-:|:-:|:-:| |:-:|:-:|:-:|
        |Phy (Standard): 35||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 20||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: 627.4||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
        |Phy (Slash): 35||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 0||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: 627.4||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
        |Phy (Strike): 10||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 20||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: Immune||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
        |Phy (Pierce): 35||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 80||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: 627.4|||


    === "Elden Beast"

        ## Elden Beast

        ![Elden Beast](imageboss/elden-beast-boss-elden-ring-wiki-guide.jpg){ width=520px align=left }

        |Drops|
        |:-:|
        |![Rune](rune/runes-currency-elden-ring-wiki-guide-18.png) 500,000 Runes/ 1,000,000 Runes(NG+1)/ 1,100,000 Runes(NG+2)|
        |![Elden Remembrance](remembrance/elden_remembrance_elden_ring_wiki_guide_200px.png){ align=left } Elden Remembrance|

        Elden Beast is a Boss in Elden Ring. Elden Beast is the true form of the Elden Ring, acting as the vassal of the Greater Will, and is found in the Elden Throne. This boss is **NOT** optional, as it is the final fight of the game, appearing immediately upon defeating Radagon of the Golden Order.

        !!! cite ""
            It is said that long ago, the Greater Will sent a golden star bearing a beast into the Lands Between, which would later become the Elden Ring.
            
        Elden Beast is a **God** Boss found in the Elden Throne.
            
        - Closest Site of Grace: Elden Throne
        - Multiplayer is allowed for this boss
        - Spirit Ashes can be summoned for this boss.

        Elden Beast Lore :

        - Elden Beast is the vassal of the Greater will, and **Outer God**.  

        | |Absorptions| | | |Resistances| |
        |:-:|:-:|:-:| |:-:|:-:|:-:|
        |Phy (Standard): 10||![magic](absorption/magic-upgrades-elden-ring-wiki-guide-30.png){ align=left } Magic: 40||![Poison](resistance/poison_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Poison: Immune||![Sleep](resistance/sleep_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Sleep: Immune|
        |Phy (Slash): 10||![fire](absorption/fire-upgrades-elden-ring-wiki-guide-30.png){ align=left } Fire: 40||![Scarlet Rot](resistance/scarlet_rot_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Scarlet Rot: Immune||![Madness](resistance/madness_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Madness: Immune|
        |Phy (Strike): 10||![lightning](absorption/lightning-upgrades-elden-ring-wiki-guide-30.png){ align=left } Lightning: 40||![Hemorrhage](resistance/hemmorage.png){ width=25px align=left } Hemorrhage: Immune||![Death Blight](resistance/blight_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Death Blight: Immune|
        |Phy (Pierce): 10||![holy](absorption/holy_upgrade_affinity_elden_ring_wiki_guide_60px.jpg){ width=30px align=left } Holy: 80||![Frostbite](resistance/frostbite_status_effect_elden_ring_wiki_guide_25px.png){ align=left } Frostbite: Immune|||
