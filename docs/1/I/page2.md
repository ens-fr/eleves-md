# Matériel d'escalade

## Corde et Sangle

???+ example "Examples"

    === "Corde"

        La corde est l'un des éléments essentiels dans la pratique de l'escalade. À l'origine, elle était constituée de chanvre2 dont la résistance et la fiabilité n'étaient pas toujours garanties. Avec la découverte du nylon et les progrès technologiques, les cordes ont beaucoup évolué dès la fin de la Seconde Guerre mondiale. De nos jours, les cordes d'escalade sont généralement constituées d'une âme de fibres tissées et d'une gaine extérieure de fibres de couleur tressées. L'âme fournit l'essentiel de la résistance en extension, alors que la gaine la protège en augmentant sa résistance à la friction, aux rayons UV et à l'humidité.

        ![corde d'escalade](images/Matériel/Corde Sangle/corde.jpg)

        es cordes utilisées pour l'escalade sont classées en deux catégories : les cordes dynamiques et les cordes statiques. Les cordes dynamiques sont élastiques et sont généralement utilisées comme systèmes d'assurage car l'élasticité réduit la force maximale subie par le grimpeur ainsi que les contraintes exercées sur l'équipement en cas de chute. Les cordes statiques ne sont pas élastiques et sont généralement utilisées pour des manœuvres mineures ou pour attacher l'équipement. Elles sont aussi utilisées pour le rappel, puisqu'elles réduisent les oscillations et facilitent la descente. De nombreux modèles de cordes dynamiques et statiques sont disponibles et adaptées aux besoins des grimpeurs et de leur pratique (longueur, diamètre, etc.).

        Dans les années 1970-1980, les grimpeurs disposaient de deux types de corde :

        - l'attache, d'une longueur n'excédant pas 40 mètres, destinée à l'école d'escalade ou aux grandes voies ne comportant pas de descente en rappel (descente sur sentier ou désescalade) ;

        - le rappel, d'une longueur n'excédant pas 80 mètres (2 × 40 mètres, bicolore afin de repérer facilement le milieu), permettant l'assurage en double sur deux brins (cordée de 2 grimpeurs), l'assurage en flèche (cordée de trois grimpeurs) et la réalisation de rappels.

        Les cordelettes sont des cordes de diamètre inférieur à 8 mm. Leur usage est multiple : confection de systèmes de sécurité, d'amarrages, d'étriers (ou « pédales » dans le jargon du grimpeur), de nœuds auto-bloquants, etc.
        
    === "Anneau de sangle"

        Une sangle est une corde plate, sans âme. Elle est un élément indispensable du matériel du grimpeur par sa polyvalence. Réalisées dans un matériau d'une très grande résistance (Dyneema), les sangles sont généralement nouées ou cousues pour former une boucle désignée sous le nom d'anneau de sangle. Les sangles ont de nombreux usages : extension des points d'assurage, confection de relais, de baudriers improvisés, transport de matériel, ou élément constituant une dégaine.

        ![Anneau de sangle](images/Matériel/Corde Sangle/anneau de sangle.jpg)

    === "Sac à corde"

        Le sac à corde est conçu pour transporter, stocker et protéger la corde d'escalade. Il peut être déplié au sol afin de protéger la corde et il est généralement muni de deux points d'attaches afin de repérer plus facilement ses extrémités

        ![sac à corde](images/Matériel/Corde Sangle/sac à corde.jpg)

## Matériel de connexion

???+ example "Examples"

    === "Mousqueton"
    
        Le mousqueton est un anneau de métal muni d'un système d'ouverture facile appelé doigt à ressort. Il est utilisé comme connecteur. Pour l'escalade sportive, la plupart des mousquetons sont fabriqués en alliage d'aluminium. Les mousquetons présentent de nombreuses formes, leur profil et leur type de doigt d'ouverture correspondent à des usages précis (assurage, progression, transport de matériel). Ils sont aussi connus sous le nom argotique de « mousquif ».

        ![mousqueton classiques](images/Matériel/connexion/mousqueton0.jpg)

        Les mousquetons sont classés en deux catégories : les mousquetons classiques, qui peuvent être ouverts par une simple pression sur le doigt du mousqueton, et les mousquetons de sécurité. Ces derniers sont munis d'un dispositif de fermeture à vis manuelle ou automatique qui empêche le doigt du mousqueton de s'ouvrir de façon intempestive lors de leur utilisation.

        ![mousqueton de sécurité](images/Matériel/connexion/mousqueton.jpg)

        Il existe tout un éventail de mousquetons avec différents types de doigt, dont les doigts à fil, les doigts courbés, ou les doigts droits, aux propriétés (résistance, etc.) adaptées à leur utilisation.

    === "Dégaine"
    
        La dégaine est utilisée par les grimpeurs pour relier la corde au point d'ancrage. Elle permet à la corde de coulisser avec une friction minimale et de limiter le tirage. Les dégaines sont généralement constituées de deux mousquetons classiques reliés par un court anneau de sangle cousue. Elles existent en différentes longueurs et certaines même avec une sangle extensible. Certaines sociétés peuvent réaliser un mousqueton avec une poulie à son extrémité pour réduire la friction de la corde sur le mousqueton, ce qui augmente néanmoins la force totale subie par la dégaine en cas de chute.

        ![dégaine](images/Matériel/connexion/dégaine.jpg)

    === "Baudrier"
    
        Un baudrier, ou harnais, permet de faire le lien entre le grimpeur et la corde. En cas de chute, il répartit le choc sur le corps et réduit efficacement les traumatismes. Les baudriers d'ancienne génération (large ceinture avec porte-matériel, harnais de torse ou harnais complet avec torse et cuissard) ont été progressivement remplacés par des cuissards avec encordement à la taille. 

        ![baudrier](images/Matériel/connexion/baudrier.jpg)

    === "Chaussons d'escalade"  

        Il s'agit de chaussures spécialement conçues pour l'escalade. Pour améliorer son adhérence sur un mur d'escalade ou sur un rocher, la semelle est recouverte d'une couche de gomme vulcanisée. Les chaussons doivent s'adapter à la morphologie du grimpeur, certains sont très resserrés autour du pied, d'autres possèdent un rembourrage en mousse sur le talon pour rendre les descentes et les rappels plus confortables. 

        ![Chaussons d'escalade](images/Matériel/connexion/chausson escalade.jpg)
    
## Matériel complémentaire

Avec l'amélioration du matériel, le grimpeur dispose d'accessoires complémentaires destinés à rendre la progression plus facile ou la descente plus confortable

???+ example "Examples"

    Il s'agit de dispositifs mécaniques utilisées pour l'assurage afin de garantir un contrôle rigoureux de la corde d'assurage. Leur finalité est de permettre le blocage de la corde avec un effort minimal. De nombreux dispositifs d'assurage existent sur le marché : certains, polyvalents, peuvent aussi être utilisés en tant que descendeur ou pour assurer du haut d'une voie, ou en relais intermédiaire. Leur mise en œuvre nécessite un apprentissage préalable qui n'exclut pas une vigilance de tous les instants, même chez le grimpeur expérimenté.

    ![Systèmes d'assurage non-autobloquants](images/Matériel/complémentaire/assurage0.jpg)

    Ces dispositifs se divisent en deux catégories : les appareils non-autobloquants, les plus anciens, et les appareils autobloquants, mécaniques, plus élaborés et qui facilitent les manipulations nécessaires à l'assurage

    ![Systèmes d'assurage autobloquants](images/Matériel/complémentaire/assurage1.jpg)

## Ancrage 
    
En escalade, un point d'ancrage ou simplement ancrage désigne tout dispositif permettant de relier le grimpeur à la paroi par l'intermédiaire de la corde, qu'il soit en cours de progression ou au relais. Il permet aussi de fixer une charge (portaledge, etc.).

Les ancrages peuvent être fixes, placés à demeure, ou temporaires.

???+ example "Examples"

    === "Bicoin"

        Les bicoins, aussi appelés câblés, sont fabriqués dans de nombreuses variétés. Dans leur forme la plus simple, ils sont juste constitués d'un petit bloc de métal auquel est attaché un anneau de cordelette ou un câble.

        Les bicoins sont utilisés en les bloquant dans des fissures du rocher.

        ![bicoin dans une fissure](images/Matériel/ancrage/bicoin.jpg)

    === "Coinceur hexagonal"
        
        Les Hexcentrics sont une marque de coinceurs en forme de prisme hexagonal concave avec des extrémités coniques. Ils sont généralement utilisés dans des fissures larges. Les Hexcentrics sont fabriqués par Black Diamonds dans de nombreuses tailles. Ces dernières années, des sociétés telles que Metolius ou Wild Country ont conçu des coinceurs similaires, avec des côtés incurvés, qui sont plus intuitifs à placer et ont une meilleure action en came.

        ![plusieurs coinceur hexagonal](images/Matériel/ancrage/coinceur_hexagonal.jpg)


    === "Coinceur mécanique"

        Aussi connu sous le nom de « coinceur à came » ou « friend », ce coinceur comporte deux, trois, ou quatre cames montées sur un axe commun ou deux axes adjacents, de manière que la traction sur l'axe force les cames à s'écarter. Une gâchette (ou une petite poignée) ramène les cames ensemble. Ils sont placés dans une fissure ou un trou du rocher en tirant sur cette gâchette pour permettre aux cames de s'insérer, puis en tirant sur une petite barre à l'extrémité de l'axe qui entraîne l'écartement des cames et leur coincement sur la surface du rocher. Les cames sont maintenues en place par de petits ressorts. Une fois le dispositif installé, la corde d'assurance peut enfin être reliée à l'extrémité du coinceur par un mousqueton.

        Les coinceurs mécaniques sont faciles et rapides à poser et s'adaptent à une plus grande variété de fissures que les bicoins. Ils sont en revanche beaucoup plus volumineux et leur prix reste élevé.

        ![Coinceur mécanique dans une fissure](images/Matériel/ancrage/coinceur_mécanique.jpg)

    === "Décoinceur"

        Le décoinceur est constitué d'une fine pièce de métal rigide d'une dizaine de centimètres de long et comportant un ou plusieurs crochets. Il permet de retirer les bicoins restés coincés dans les fissures du rocher, particulièrement lorsque le coinceur a supporté le poids d'un grimpeur. Il peut arriver qu'un coinceur correctement posé et verrouillé ne puisse être enlevé, même avec l'aide d'un décoinceur. 

        ![décoinceur](images/Matériel/ancrage/décoinceur.jpg)

## Équipement personnel

L'équipement, notamment l'habillement, répond à des exigences dictées par les particularités de l'escalade, sans exclure toutefois le phénomène de mode qui n'épargne pas les sports extrêmes ou à risques. Ainsi, dans les années 1980 et le début des années 1990, la mode était de porter des vêtements serrés et multicolores. La mode est maintenant plutôt aux vêtements amples. En escalade, la tendance va de la tenue minimaliste qui rappelle les années 1970 (short et chaussons) à celle qui répond au principe des trois couches, notamment pour les séances en altitude. Les accessoires et équipements n'échappent pas à la règle et évoluent au gré de l'inventivité et de l'ingéniosité de leurs concepteurs.

???+ example "Examples"

    === "Casque"

        Le casque est un élément de sécurité encore trop souvent négligé, bien qu'il ait sauvé de nombreux grimpeurs de blessures graves voire mortelles. Un casque est constitué de matériaux résistants pour protéger la tête et la boîte crânienne des impacts. Dans les secteurs d'escalade très fréquentées, ces impacts sont davantage la conséquence de chutes de pierres ou d'objets divers (matériel d'escalade) que par la chute d'un grimpeur percutant le rocher ou le sol.

        ![casque](images/Matériel/équipement perso/casque.jpg)

        En escalade, le port du casque est plus ou moins observé. Les raisons pour un grimpeur de ne pas porter de casque sont nombreuses et plus ou moins justifiées : poids, encombrement, voire gêne.

    === "Magnésie"

        La magnésie est une poudre constituée principalement de carbonate de magnésium, auquel on ajoute parfois du sulfate de magnésium qui agit comme agent séchant. Elle est utilisée par les gymnastes pour absorber la transpiration. Son usage s'est massivement répandu chez les grimpeurs qui l'utilisent pour améliorer l'adhérence des mains sur le rocher.

        ![magnésie](images/Matériel/équipement perso/magnésie.jpg)

         L'utilisation de la magnésie est controversée et même interdite sur certains sites à l'exemple de Albarracín. Elle serait nuisible au grès. Les traces de magnésie peuvent persister là où la pluie est insuffisante ou le rocher en dévers, et forment des dépôts disgracieux. Le brossage pour enlever ces dépôts, s'il est trop agressif, peut détériorer le rocher. 

    === "Sac à magnésie"

        Il s'agit d'un sac de la taille d'une main pour recevoir la magnésie. Il est habituellement clippé ou attaché sur le baudrier du grimpeur pour un emploi aisé durant l'escalade.

        ![sac à magnésie](images/Matériel/équipement perso/sac_magnésie.jpg)

        Pour faciliter la dispersion de la magnésie de manière homogène, une chaussette à magnésie ou une balle à magnésie est remplie de magnésie et ensuite placée dans le sac. Les chaussettes à magnésie sont des poches faites d'un matériau poreux qui permet à la magnésie de sortir en faible quantité quand la chaussette est remuée o

    === "Gants d'assurage"

        Bien qu'étant souvent rejetés par la plupart des grimpeurs qui prétendent que les gants d'assurage réduisent l'adhérence et le contrôle sur la corde les gants d'assurage sont une aide utile pour l'assurage durant de longues séances d'escalades, notamment lorsqu'ils doivent descendre un grimpeur. Les gants empêchent les brûlures provoquées par la corde et protègent des conséquences d'un lâcher de corde involontaire.

        ![Gants d'assurage](images/Matériel/équipement perso/gants_assurage.jpg)

        Les gants d'escalade sont réalisés soit en cuir soit en matière synthétique. Ils ont souvent des rembourrages sur la paume et les doigts pour protéger des brûlures. 

    === "Sac de hissage"

        Il s'agit d'un grand sac robuste qui permet de hisser le ravitaillement et le matériel d'escalade à l'occasion d'ascensions comportant un ou plusieurs bivouacs. Un sac à dos avec un anneau de hissage peut également être utilisé.

        ![sac de hissage](images/Matériel/équipement perso/sac_hissage.jpg)

    === "Porte matériel"

        Un porte-matériel est habituellement utilisé par les grimpeurs de grandes parois ou d'escalade artificielle quand le porte-matériel de leur baudrier ne peut recevoir la totalité du matériel. Il est porté autour de l'épaule. Il est constitué d'un anneau de sangle et, pour les plus évolués, d'un rembourrage et de deux sangles de chaque côté. 

        ![porte matériel](images/Matériel/équipement perso/porte_matériel.jpg)

## Matériel d'entraînement

Divers matériels sont employés pour l'entraînement à l'escalade.

???+ example "Examples"

    === "Poutre à doigts"

        Il s'agit d'un agrès principalement utilisé pour améliorer la force de préhension et pour pratiquer différentes techniques de préhension. Ces poutres, installées par exemple au-dessus d'une porte, consistent en une variété de trous et de réglettes de tailles différentes et qui permettent ainsi au grimpeur de se suspendre ou de pratiquer des tractions. 

        ![poutre à doigts](images/Matériel/entrainement/poutre_goigts.jpg)

    === "Matelas de bloc"

        Il s'agit d'un matelas dense utilisé pour amortir les chutes et recouvrir les volumes qui pourraient être dangereux en cas de chute. Il est généralement constitué d'une mousse dense de 5 à 15 centimètres recouverte d'une housse robuste. De nombreux modèles comportent des poignées ou sont conçus pour être repliés dans des dimensions raisonnables pour le transport. Le matelas de bloc est souvent appelé crash pad.

        ![Matelas de bloc](images/Matériel/entrainement/matelas.jpg)

??? info "Les sites utillisés" 
    - [Wikipédia](https://fr.wikipedia.org/wiki/Mat%C3%A9riel_d%27escalade)
    