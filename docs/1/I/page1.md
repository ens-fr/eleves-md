# L'ESCALADE

!!! info "L'escalade libre"
    Contrairement à l'escalade artificielle, l'escalade libre consiste à progresser sur une paroi sans s'aider de matériel mais en utilisant seulement ses capacités physiques. Il faut donc atteindre le sommet en n'utilisant que les prises du rocher.

    ---

    C'est la forme la plus courante d'escalade. Si un jour vous entendez le mot « escalade » sans davantage de précision, il s'agit sûrement d'escalade libre ! Mais derrière cette expression toute simple, vous découvrirez qu'il existe en réalité de nombreux types d'escalade.

## Les 4 principaux types d'escalade

### L'escalade sportive

!!! example "la voie"

    Commençons par les basiques ! La voie se pratique à l'extérieur mais aussi en salle. À moins d'être assuré par un enrouleur automatique comme en escalade de vitesse, il faut être en **binôme** : un grimpeur et un assureur. La paroi à gravir est toujours équipée d'éléments d'assurage et est à parcourir **d'un coup**. Une fois arrivé en haut, on atteint un relais : c'est un double anneau métallique qui permet d'attacher la corde en rappel et de redescendre en toute tranquillité.

    ---

    Cette discipline se dispute aux Jeux Olympiques sous le nom d'escalade de « **difficulté** ». Le grimpeur progresse sur un mur d'environ 15 mètres en s'assurant en tête. Son objectif ? Aller le plus haut possible !

!!! example "La grande voie"

    Difficilement praticable ailleurs qu'en extérieur, il s'agit d'une déclinaison de la voie dite classique. Ici, l'ascension est divisée en plusieurs étapes, appelées « **relais** ». Lorsque le grimpeur arrive au bout d'un relais, il peut se reposer avant d'attaquer le suivant. Il n'est pas rare qu'en grande voie l'ascension dure plusieurs jours. Les grimpeurs dorment alors sur des **portaledge**, qui sont une sorte de lit/tente vertical accrochables qui se déploient contre la paroi.

    ![portaledge](images/Matériel/complémentaire/portaledge.jpg)

    ---

    :warning: Si les parois ne sont pas équipées de points d'ancrage et que c'est aux grimpeurs d'y placer leurs propres protections (coinceurs), on parle d'escalade « traditionnelle », sinon, d'escalade « sportive ». 

### Le bloc

!!! example "Le bloc classique"

    Il s'agit de la forme la plus technique de l'escalade. L'effort est **court mais intense**. Le bloc se pratique en salle ou sur des rochers. Le grimpeur monte sans baudrier et est protégé par un tapis de réception _crashpad_ en cas de chute. N'étant pas assuré, on monte rarement au delà de 5 mètres. Ce type d'escalade se pratique seul.
    
    ---

    Le bloc représente la seconde discipline d'escalade aux Jeux Olympiques.

!!! example "Le psychobloc"

    Le psychobloc est une déclinaison du bloc qui se pratique en extérieur, sur des falaises, au dessus de l'eau. L'eau réduisant le risque de blessure grave en cas de chute, les grimpeurs montent généralement plus haut. La France, l'Espagne et la Thaïlande sont réputés pour leurs spots de psychobloc. 

### L'escalade en solo

!!! example "Le solo avec auto-assurage"
    
    L'escalade en solo avec auto-assurage se pratique seul. Le grimpeur progresse sur la voie en s'assurant lui-même. Cette manière de grimper fait appel à des techniques complexes d'assurage en tête ou bien sur corde tendue depuis le haut de la voie. Les grimpeurs solo utilisent du matériel d'assurage spécifique, comme des dispositifs mécaniques de blocage ou anti-chute, des absorbeurs de chocs, des cordes statiques, et bien d'autres.

!!! example "Le solo intégral (free solo)"

    L'escalade en solo intégral, est parmi tous les types d'escalade, de loin la plus **folle** et **dangereuse** des pratiques. Le grimpeur est seul, sans protection ni assurage. Il se retrouve seul face à la paroi frôlant la mort à chaque seconde. On ne pourra jamais comprendre ce qui pousse les grimpeurs à faire ceci, ainsi que le sentiment de liberté que cela peut procurer.

    ---

    Le géant de la discipline s'appelle Alex Honnold. Je vous recommande d'ailleurs l'excellent film[^ip] « Free solo » présentant le projet le plus fou de cet athlète.

    [^ip]: [Le trailer du film](https://www.youtube.com/watch?v=-KhzzfTEPGs) 

### L'escalade de vitesse

!!! example "Explication"

    En escalade de vitesse, l'objectif est simple : 2 grimpeurs s'affrontent pour gravir un mur de 15 mètres  le plus rapidement possible. Pour arrêter le chronomètre, on appuie sur un buzzer au sommet de la voie. La structure de la voie de vitesse a la particularité d'être toujours identique. Il s'agit des mêmes prises, au même endroit, avec la même inclinaison de mur.

    ---

    C'est la dernière discipline enregistrée à la Fédération Internationale d'Escalade. En 2016, le Comité International Olympique a accepté l'escalade de vitesse en tant que discipline aux Jeux Olympiques de Tokyo de 2020. Ce type d'escalade est très apprécié du public qui la trouve particulièrement spectaculaire. Les meilleurs temps pour gravir le mur sont aux alentours de 5.5 secondes !

??? info "Les sites utillisés"  
    - [CLIMB CAMP](https://climbcamp.fr/les-differents-types-descalade/amp/)
    