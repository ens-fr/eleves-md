# Adam Ondra à réussit l'exploit du DAWN WALL

## Biographie

!!! tldr "Adam Ondra"
    Adam Ondra[^ad], né le 5 février 1993 à Brno, est un grimpeur professionnel tchèque. Il commence l'escalade jeune et acquiert rapidement une réputation internationale notamment pour ses records et la difficulté de ses réalisations. Il est le plus jeune grimpeur à avoir réalisé une voie d'escalade avec une cotation de 8c+ et à avoir atteint le neuvième degré. Il a aussi le record de voies grimpées à vue dans la cotation 8c+ et est l'auteur de nombreuses premières ascensions de voies parmi les plus difficiles du monde, en particulier avec ses trois réalisations annoncées en 9b+, cotation crédibilisée par son expérience dans le 9b.
    
    [^ad]: [Pour en savoir plus sur Adam Ondra](https://fr.wikipedia.org/wiki/Adam_Ondra)

## La Voie 

!!! tldr "El Capitan"
    El Capitan[^az] est une formation rocheuse verticale de 900 m de haut située dans la vallée de Yosemite aux États-Unis. Le Dawn Wall est la plus grande voie et la plus dure au monde, tracée sur la paroi d'El Capitan, un "Big Wall" de près de 900m de haut. Le topo ? 32 longueurs sur un des granits les plus lisses au monde, et dont plus de la moitié sont situées dans le 8e degré. Le crux (passage clef de la voie) ? Les longueurs 14 et 15 constituent une traversée d'une difficulté côtée à 9a.

    [^az]: [Pour en savoir plus sur El Capitan](https://fr.wikipedia.org/wiki/El_Capitan#El_Capitan_et_l'escalade)

??? tip " _The Dawn Wall_"
    _The Dawn Wall_ est un documentaire austro-américain réalisé par Josh Lowell et Peter Mortimer sorti en 2017. Il traite de l'ascension en escalade libre d'une paroi rocheuse d'El Capitan, par Tommy Caldwell et Kevin Jorgeson. 

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/MbMxoKsJfRA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Dawn Wall Timeline

???+ tldr "Time Line"
    | date | trajet |  photo du parcours |
    |:-------:|:------------|---------------|
    | Jour 1| Départ nocturne pour Adam Ondra, qui quitte la terre ferme à 02h46 du matin. Le tchèque veut profiter de la fraîcheur de la nuit pour enchaîner le plus de longueur possible, avant l'arrivée du soleil et donc de la chaleur sur la paroi. Objectif réussi, à l'heure où des millions d'américains ouvrent les yeux, Adam est déjà au sommet de la longueur 9.  | ![parcours du jour 1](images/Dawn_Wall/j1.jpg) |
    | Jour 2   | Quelques heures de repos plus tard, Adam renfile les chaussons pour une nouvelle session de nuit. Au programme, les longueurs 10, 11, 12 et 13. Il bute plusieurs fois sur la longueur qu'il apprécie le moins (L10), qui impose un long Dülfer sur des pieds humides. Quelques minutes plus tard et après être tombé plusieurs fois dans la longueur 13 (8c), Adam peut souffler et se reposer avant les longueurs crux du « Dawn Wall ». | ![parcours du jour 2](images/Dawn_Wall/j2.jpg) |
    | Jour 3   | Repos | 
    | Jour 4   | Désastre mental en ce quatrième jour. Adam ne parvient pas à enchaîner la 14ème longueur (9a), qui ne lui posait pas tant de problèmes lors de ses séances de repérage. Trop nerveux et sous pression, le tchèque ne parvient pas à grimper proprement. Après 7 essais infructueux, où il tombe sur le premier pas de bloc, il parvient lors du 8ème run à passer ce crux avant de chuter sur le tout dernier mouvement de la voie. S'en est fini pou aujourd'hui. | ![parcours du jour 4](images/Dawn_Wall/j3.jpg) |
    | Jour 5   | C'est une nouvelle journée qui commence, qui s'avérera bien meilleure que la veille! En effet, Adam contrôle son mental. Il ne veut surtout pas retomber dans le même état d'esprit qu'hier. Et ça marche! Il enchaîne la longueur 14 au premier essai de la journée, avant de continuer sur L15, le deuxième 9a du « Dawn Wall », qu'il réalise rapidement, au 2ème essai. | ![parcours du jour 5](images/Dawn_Wall/j4.jpg) |
    | Jour 6   | Surmotivé, et après une nuit de repos, c'est une nouvelle grosse journée qui attend Adam. En effet, il enchaîne 6 longueurs de plus dans la journée, dont les longueurs 16, 17 et 18, 8b+. En arrivant au sommet de la longueur 22, Adam le sait: le plus gros est derrière lui. Même s'il est prêt à continuer, la pluie est annoncée sur le « Dawn Wall ». Place à un repos forcé. | ![parcours du jour 6](images/Dawn_Wall/j5.jpg) |
     Jour 7   | Repos | 
    | Jour 8   | Après avoir passé sa dernière nuit en pleine paroi, Adam s'élance pour ses derniers mètres d'escalade. Les premières longueurs sont mouillées, il faut donc redoubler de vigilance… Car comme il le dit si bien, « même les voies faciles sont difficiles ici! ». À midi, le temps se dégage et le soleil fait son apparition. Quoi de mieux pour saluer l'arrivée d'Adam au sommet. Il domine le « Dawn Wall » et ses 900 mètres d'escalade. Adam vient une nouvelle fois de rentrer dans la légende de l'escalade! | ![parcours du jour 8](images/Dawn_Wall/j6.jpg) |

## L'exploit 

Le 21 novembre, Adam Ondra a complété l'ascension des 32 longueurs du Dawn Wall, la grande voie la plus dure au monde, en un temps record.

![El Capitan](images/Dawn_Wall/El%20Capitan.png)

Une consécration pour le tchèque, qui, rappelons-le, venait pour la première fois de sa vie au Yosemite. Et on le sait, le style, là-bas est très atypique, comme le confirme le maître des lieux, Tommy Caldwell. En effet, on est bien loin des terrains de jeu habituel d'Adam: oubliez les grottes de Flatanger ou autres, où il faut se tordre dans tous les sens et user de son biceps pour enchaîner les voies ultra-physiques. Dans le _Dawn Wall_, les prises de pieds sont microscopiques, les lames de rasoir qui servent de prise de mains sont aiguisées à en cisailler la peau et il faut déployer tout son bagage technique pour enchaîner, pas à pas, toutes les longueurs.

![prise](images/Dawn_Wall/prise.png)

Voilà pourquoi cette ascension avait demandé à Tommy Caldwell et Kevin Jorgeson, les premiers à avoir libéré le _Dawn Wall_, plus de 7 ans de préparation et de travail, et près de 20 jours passés sur la paroi. À côté, les quelques jours de repérage par Adam avant son ascension complète paraissent négligeables et accroissent la performance de ce dernier.

![escalade durant la nuit](images/Dawn_Wall/Ondra.png)
![escalade durant la nuit](images/Dawn_Wall/nuit.png)

Le simple fait de dormir en portaledge ou même d'utiliser une technique de remontée sur corde efficace était quelque chose de nouveau pour Adam. Il partait donc de loin, de très loin, mais on le sait, le tchèque aime les nouveaux défis et n'est pas du genre à se laisser abattre! C'est une preuve que tout est possible. Adam s'est donné les moyens de réussir, et il l'a fait.

![portaledge](images/Dawn_Wall/portaledge1.png)
![portaledge](images/Dawn_Wall/portaledge2.png)

Une expérience qui restera à coup sûr gravée dans sa mémoire: de la vue du matin au réveil, perché à 800 mètres de haut, au plaisir du chocolat noir après la réussite des longueurs clés, en passant par cette sensation de peur, la nuit, éclairé par la lueur de sa lampe frontale, sur des pieds humides. Et cette joie au sommet, d'avoir réussi ce défi, enchaîner la grande voie la plus dure du Monde.

![la fin](images/Dawn_Wall/adam.png)

> **Ressenti** :
>
> - C'est une sensation extraordinaire 
> - C'est d'ailleurs une des meilleures sensations que j'ai ressenti en grimpant. Je pense que cette joie va durer, c'était une voie tellement longue et difficile. 

De Adam Ondra

??? info "Les sites utillisés"
    1. [Wikipédia, Adam Ondra](https://fr.wikipedia.org/wiki/Adam_Ondra)

    2. [Wikipédia, El Capitan](https://fr.wikipedia.org/wiki/El_Capitan)

    3. [L'ÉQUIPE](https://www.lequipe.fr/Adrenaline/Escalade/Actualites/Adam-ondra-au-sommet-du-dawn-wall-la-grande-voie-la-plus-dure-au-monde/751971)

    4. [PLANET GRIMPE](https://planetgrimpe.com/boum-adam-ondra-enchaine-32-longueurs-dawn-wall/?cn-reloaded=1)

