# Panda géant

## _Ailuropoda melanoleuca_

---

[![Aide Homonymie](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Logo_disambig.svg/20px-Logo_disambig.svg.png)](https://fr.wikipedia.org/wiki/Aide:Homonymie) _Pour les articles homonymes, voir [Panda](https://fr.wikipedia.org/wiki/Panda)._

---

Le **panda géant** (**_Ailuropoda melanoleuca_**) est une [espèce](https://fr.wikipedia.org/wiki/Esp%C3%A8ce) de [mammifères](https://fr.wikipedia.org/wiki/Mammif%C3%A8re), habituellement classée dans la [famille](https://fr.wikipedia.org/wiki/Famille_(biologie)) des [ursidés](https://fr.wikipedia.org/wiki/Ursidae) (Ursidae), [endémique](https://fr.wikipedia.org/wiki/End%C3%A9misme) de la [Chine](https://fr.wikipedia.org/wiki/Chine) centrale. Il fait partie de l'[ordre](https://fr.wikipedia.org/wiki/Ordre_(biologie)) des [Carnivores](https://fr.wikipedia.org/wiki/Carnivora), même si son [régime alimentaire](https://fr.wikipedia.org/wiki/R%C3%A9gime_alimentaire) est constitué à 99 % de végétaux, principalement du [bambou](https://fr.wikipedia.org/wiki/Bambou).  

Il ne vit que dans le centre de la Chine, dans des régions [montagneuses](https://fr.wikipedia.org/wiki/Montagne) recouvertes de forêts d'altitude, des provinces du [Sichuan](https://fr.wikipedia.org/wiki/Sichuan) et du [Gansu](https://fr.wikipedia.org/wiki/Gansu) (dans les régions traditionnelles de l'[Amdo](https://fr.wikipedia.org/wiki/Amdo) et du [Kham](https://fr.wikipedia.org/wiki/Kham) du [Tibet oriental](https://fr.wikipedia.org/wiki/Tibet)), ainsi qu'au [Shaanxi](https://fr.wikipedia.org/wiki/Shaanxi), entre 1 000 et 4 000 mètres.  

Jusqu'en 1901, le panda géant était connu par les anglophones sous le nom de « _parti-coloured bear_ ». Par la suite, il fut lié au [panda roux](https://fr.wikipedia.org/wiki/Ailurus_fulgens) (_Ailurus fulgens_), dont il possède des caractéristiques communes comme le « sixième doigt » ou « faux pouce », qu'il partage également avec _[Simocyon batalleri](https://fr.wikipedia.org/wiki/Simocyon_batalleri)_, l'ancêtre européen d'il y a 9 millions d'années du panda roux, ayant la taille d'un [puma](https://fr.wikipedia.org/wiki/Puma), retrouvé à Batallones-1, près de Madrid. _Ailurus fulgens_ appartient en réalité à une autre famille, les [Ailuridae](https://fr.wikipedia.org/wiki/Ailuridae), dans la superfamille des [Musteloidea](https://fr.wikipedia.org/wiki/Musteloidea), qui comprend également les [belettes](https://fr.wikipedia.org/wiki/Belette_d%27Europe), les [moufettes](https://fr.wikipedia.org/wiki/Mouffette) et les [procyonidés](https://fr.wikipedia.org/wiki/Procyonidae).  

|<mark>**_Ailuropoda melanoleuca_**</mark>|
|:--:|
|[![Photo Panda](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Grosser_Panda.JPG/290px-Grosser_Panda.JPG)](https://commons.wikimedia.org/wiki/File:Grosser_Panda.JPG?uselang=fr)|
|Panda géant à [Ocean Park Hong Kong](https://fr.wikipedia.org/wiki/Ocean_Park_Hong_Kong).|  

|[<mark>**Classification selon MSW**</mark>](https://fr.wikipedia.org/wiki/Mammal_Species_of_the_World)||
|---|---|
|[Règne](https://fr.wikipedia.org/wiki/R%C3%A8gne_(biologie))|[Animalia](https://fr.wikipedia.org/wiki/Animal)|
|[Embranchement](https://fr.wikipedia.org/wiki/Embranchement_(biologie))|[Chordata](https://fr.wikipedia.org/wiki/Chordata)|
|[Classe](https://fr.wikipedia.org/wiki/Classe_(biologie))|[Mammalia](https://fr.wikipedia.org/wiki/Mammif%C3%A8re)|
|[Ordre](https://fr.wikipedia.org/wiki/Ordre_(biologie))|[Carnivora](https://fr.wikipedia.org/wiki/Carnivora)|
|[Sous-ordre](https://fr.wikipedia.org/wiki/Sous-ordre)|[Caniformia](https://fr.wikipedia.org/wiki/Caniformia)|
|[Famille](https://fr.wikipedia.org/wiki/Famille_(biologie))|[Ursidae](https://fr.wikipedia.org/wiki/Ursidae)|
|[Genre](https://fr.wikipedia.org/wiki/Genre_(biologie))|[_Ailuropoda_](https://fr.wikipedia.org/wiki/Ailuropoda)|

|[<mark>**Espèce**</mark>](https://fr.wikipedia.org/wiki/Esp%C3%A8ce)|
|:--:|
|**_Ailuropoda melanoleuca_**<br/>([David](https://fr.wikipedia.org/wiki/Armand_David), [1869](https://fr.wikipedia.org/wiki/1869)) [Milne-Edw.](https://fr.wikipedia.org/wiki/Alphonse_Milne-Edwards)|

|[<mark>**Statut de conservation**</mark>](https://fr.wikipedia.org/wiki/Statut_de_conservation)[<mark>** UICN**</mark>](https://fr.wikipedia.org/wiki/Union_internationale_pour_la_conservation_de_la_nature)|
|:--:|
|![Image Statut conservation](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Status_iucn3.1_VU-fr.svg/244px-Status_iucn3.1_VU-fr.svg.png)<br/>**VU** C2a(i);D1 : **Vulnérable**|

|<mark>**Statut**</mark>[<marK>** CITES**</mark>](https://fr.wikipedia.org/wiki/Convention_sur_le_commerce_international_des_esp%C3%A8ces_de_faune_et_de_flore_sauvages_menac%C3%A9es_d%27extinction)|
|:--:|
|![Annexe 1](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Cites_I.svg/30px-Cites_I.svg.png) [Annexe I ](https://fr.wikipedia.org/wiki/Annexe_I_de_la_CITES), Rév. du 14/03/1984|

## Étymologie

---

Morphologiquement, son [nom normalisé](https://fr.wikipedia.org/wiki/Nom_normalis%C3%A9) [chinois](https://fr.wikipedia.org/wiki/Langues_chinoises) actuel, est « grand chat-ours » (大熊猫, _dà xióngmāo_). En [tibétain](https://fr.wikipedia.org/wiki/Tib%C3%A9tain#alphasyllabaire) son nom signifie ours-panaché ([tibétain](https://fr.wikipedia.org/wiki/Tib%C3%A9tain#alphasyllabaire) : དོམ་ཁྲ, [Wylie](https://fr.wikipedia.org/wiki/Translitt%C3%A9ration_Wylie) : _dom khra_).

Le nom de genre _Ailuropoda_ vient du grec [αἴλουρος](https://fr.wiktionary.org/wiki/%CE%B1%E1%BC%B4%CE%BB%CE%BF%CF%85%CF%81%CE%BF%CF%82) _ailouros_ qui signifie « chat » et de πους, ποδος _pous - podos_ qui signifie « pied », soit « à pied de chat ».

L’[épithète spécifique](https://fr.wikipedia.org/wiki/%C3%89pith%C3%A8te_sp%C3%A9cifique) _melanoleuca_ en latin scientifique vient du grec [μέλας](https://fr.wiktionary.org/wiki/%CE%BC%CE%AD%CE%BB%CE%B1%CF%82) _melas_ « noir » et [λευκός](https://fr.wiktionary.org/wiki/%CE%BB%CE%B5%CF%85%CE%BA%CF%8C%CF%82) _leukos_ « blanc ».

## Description

---

Le panda géant est volumineux et massif : il pèse de 80 à 125 kg, avec une moyenne de 105,5 kg ; il mesure de 1,50 à 1,80 mètre de longueur, avec une moyenne de 1,65 mètre. Comme chez la majorité des grands mammifères, les femelles sont généralement plus petites et moins massives.

Le panda est noir et blanc. Il est majoritairement constitué de blanc, avec les oreilles, les pattes et le contour des yeux noirs. Son pelage épais le protège du froid des régions de haute altitude où il vit.

Le panda possède six doigts dont un « faux pouce » opposable à ses cinq doigts. Phénomène de [convergence évolutive](https://fr.wikipedia.org/wiki/Convergence_%C3%A9volutive), il provient de la transformation d'un os du [poignet](https://fr.wikipedia.org/wiki/Poignet) modifié (l'[os sésamoïde](https://fr.wikipedia.org/wiki/Os_s%C3%A9samo%C3%AFde)). [Stephen Jay Gould](https://fr.wikipedia.org/wiki/Stephen_Jay_Gould) a utilisé cette particularité anatomique comme un exemple de « bricolage évolutif » dans son essai [_Le Pouce du panda_](https://fr.wikipedia.org/wiki/Le_Pouce_du_panda). Ce pouce est une adaptation liée à l'alimentation (il sert notamment à attraper les tiges de bambou dont il se nourrit en grande quantité) ou au déplacement.

Herbivore, il a de puissantes dents, pour broyer les bambous. Il possède 42 dents.

Son ouïe et son odorat sont très fins : il utilise surtout ces deux sens pour s'orienter et se repérer. Sa vue, en revanche, est plutôt médiocre : moins bonne que celle du chat ou de l'homme.

|[![Panda mâle du ZooParc de Beauval](https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Ailuropoda_melanoleuca_%28Panda_g%C3%A9ant%29_-_446.jpg/150px-Ailuropoda_melanoleuca_%28Panda_g%C3%A9ant%29_-_446.jpg)](https://commons.wikimedia.org/wiki/File:Ailuropoda_melanoleuca_(Panda_g%C3%A9ant)_-_446.jpg?uselang=fr)|
|:--:|
|Ailuropoda melanoleuca (Panda géant) mâle au [ZooParc de Beauval](https://fr.wikipedia.org/wiki/ZooParc_de_Beauval) à [Saint-Aignan-sur-Cher](https://fr.wikipedia.org/wiki/Saint-Aignan_(Loir-et-Cher)), France.|

### **Exception**

Le [Panda de Qinling](https://fr.wikipedia.org/wiki/Panda_de_Qinling) est une sous-espèce de panda résidant uniquement dans les [montagnes de Qinling](https://fr.wikipedia.org/wiki/Monts_Qinling) en [Chine](https://fr.wikipedia.org/wiki/Chine) a une altitude de 100 à 3 000 m. Il se distingue notamment par sa fourrure ventrale brune.

En 2019, un panda [albinos](https://fr.wikipedia.org/wiki/Albinisme) est photographié pour la première fois dans le sud-ouest de la Chine.

|![Photo panda géant](https://cdn.futura-sciences.com/buildsv6/images/wide1920/d/7/9/d79df23b89_50171178_panda-geant-pas-bete.jpg)|![Photo panda de Qinling](https://www.panda.fr/images/news2018/news_20180107r.jpg)|![Panda albinos](https://mobile-img.lpcdn.ca/lpca/924x/4c9db456/c72a4925-7fc1-11e9-9f79-0e7266730414.jpg)|
|:--:|:--:|:--:|
|Panda géant|Panda de Qinling|Panda albinos|

## Écologie et comportement

---

### **Mode de vie**

« Le panda géant est confiné à la [Chine](https://fr.wikipedia.org/wiki/Chine) du centre-sud. Actuellement, il se trouve dans certaines parties de six chaînes de montagnes isolées (Minshan, [Qinling](https://fr.wikipedia.org/wiki/Monts_Qinling), Qionglai, Liangshan, Daxiangling et Xiaoxiangling), dans les provinces du [Gansu](https://fr.wikipedia.org/wiki/Gansu), du [Shaanxi](https://fr.wikipedia.org/wiki/Shaanxi) et du [Sichuan](https://fr.wikipedia.org/wiki/Sichuan) (environ 75 % de la population habite la province du Sichuan). L'habitat du panda englobe environ 30 000 kilomètres carrés entre 102 et 108,3° de longitude est, et 28,2 à 34,1° de latitude nord ».

Il habite des forêts de [bambous](https://fr.wikipedia.org/wiki/Bambou), un habitat qui n'a cessé de régresser au bénéfice de l'agriculture, ne lui laissant aujourd'hui que des îlots dispersés et isolés les uns des autres.

### **Alimentation**

Le panda géant est habituellement représenté mangeant paisiblement du [bambou](https://fr.wikipedia.org/wiki/Bambou) plutôt que chassant, ce qui ajoute à son image de l'innocence. En effet, bien que [classé](https://fr.wikipedia.org/wiki/Taxonomie) parmi les [Carnivores](https://fr.wikipedia.org/wiki/Carnivora) (classe des Carnivora), cet animal se nourrit principalement de [végétaux](https://fr.wikipedia.org/wiki/V%C3%A9g%C3%A9tarisme). Même s'il a été rapporté qu'il mange à l'occasion des [œufs](https://fr.wikipedia.org/wiki/%C5%92uf_amniotique) et des [insectes](https://fr.wikipedia.org/wiki/Insecte), son régime alimentaire est constitué à 99 % de végétaux, quasi uniquement de [bambous](https://fr.wikipedia.org/wiki/Bambou) (jusqu'à 20 kg par jour), bien que cette plante soit peu digeste, mais peut inclure ponctuellement d'autres végétaux, et même un peu de viande (petits rongeurs, poissons...). Son origine de Carnivore explique d'ailleurs qu'il dispose d'un système digestif capable de digérer de la viande. Son [microbiote](https://fr.wikipedia.org/wiki/Microbiote_intestinal) serait plus proche de celui de ses homologues [carnassiers](https://fr.wikipedia.org/wiki/Carnivore_(r%C3%A9gime_alimentaire)) ou [omnivores](https://fr.wikipedia.org/wiki/Omnivore), que de celui des [herbivores](https://fr.wikipedia.org/wiki/Herbivore) stricts. Il possède peu des bactéries que l'on retrouve chez les herbivores, tels que les ruminants, qui décomposent la cellulose, composant principal du bambou. Les scientifiques pensent que les pandas ont commencé à manger du bambou à une époque lointaine où les autres sources de nourriture sont devenues rares, et auraient vécu sur cette niche alimentaire depuis quatre millions d'années.

Il mange en résumé :

- Végétaux (bambou à 99%)
- Oeufs
- Insectes
- Viandes (petits rongeurs, poissons...)

Son [faux pouce](https://fr.wikipedia.org/wiki/Os_s%C3%A9samo%C3%AFde) lui permet de cueillir et de tenir les tiges de bambou. Il passe près de 14 heures par jour à les mastiquer en raison de sa faible capacité à assimiler la [cellulose](https://fr.wikipedia.org/wiki/Cellulose) (privé de [cæcum](https://fr.wikipedia.org/wiki/C%C3%A6cum), comme n'importe quel ursidé, il ne peut en digérer que 17 %). Les pousses sont avalées tout entières, mais il ne garde que le cœur et rejette l'écorce. Le transit intestinal dure environ huit heures. Beaucoup de forêts de bambous chinoises sont aujourd'hui exploitées par l'homme ou ont été défrichées pour devenir des terres cultivables. C'est une des raisons de la forte régression de l'espèce, qui ne dispose plus de son aliment de base.

Le [génome](https://fr.wikipedia.org/wiki/G%C3%A9nome) du panda a été séquencé par une équipe chinoise en [2010](https://fr.wikipedia.org/wiki/2010) : ses 21 000 gènes contiennent notamment tous ceux codant les enzymes caractéristiques d'un régime carnivore (typique des ursidés) mais celui qui code le récepteur de la saveur de l'[umami](https://fr.wikipedia.org/wiki/Umami) est muté, ce qui pourrait rendre inactif ce récepteur sensible à la saveur des viandes, et ainsi expliquer en partie pourquoi le panda a un régime alimentaire essentiellement végétarien, alors qu'il est, du point de vue [phylogénétique](https://fr.wikipedia.org/wiki/Phylog%C3%A9nie), un carnivore. 

|[![Panda du Zoo National de Washington DC](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Giant_Panda_Tai_Shan.JPG/220px-Giant_Panda_Tai_Shan.JPG)](https://commons.wikimedia.org/wiki/File:Giant_Panda_Tai_Shan.JPG?uselang=fr)|[_Tai Shan_](https://fr.wikipedia.org/wiki/Tai_Shan_(panda)) au Zoo National de [Washington DC](https://fr.wikipedia.org/wiki/Washington_(district_de_Columbia)).|
|---|---|
|[![Panda du Zoo de San Diego mangeant du bambou](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Panda_San_Diego_Zoo.ogv/1920px--Panda_San_Diego_Zoo.ogv.jpg)](https://upload.wikimedia.org/wikipedia/commons/transcoded/c/cf/Panda_San_Diego_Zoo.ogv/Panda_San_Diego_Zoo.ogv.720p.webm)|Un Panda géant du [zoo de San Diego](https://fr.wikipedia.org/wiki/Zoo_de_San_Diego) mange du bambou.|

### **Reproduction**

Les pandas atteignent une maturité sexuelle entre 5 ans et demi et 6 ans. Ils ne peuvent se reproduire que quelques jours par an, ce qui rend leur reproduction difficile. La durée de la [gestation](https://fr.wikipedia.org/wiki/Gestation) est d'environ 112 à 163 jours (137,5 jours en moyenne). La mère peut donner naissance à un ou deux petits, rarement trois, avec une moyenne de 1,7 petit par portée. Cependant celle-ci ne s'occupe que d'un seul petit et les autres meurent rapidement, peut-être parce que l'énergie nécessaire pour en élever plus est trop élevée, mais le débat n'est pas clos sur cette question. En ce qui concerne les animaux en captivité, afin d'éviter cette perte, des chercheurs américains font actuellement des études sur le fait d'alterner les petits, ainsi la mère s'occupe des deux petits sans s'en rendre compte. À sa naissance, le petit pèse à peine entre 85 et 140 grammes (110 grammes en moyenne), est rose, aveugle et sans fourrure, totalement dépendant de sa mère ou d'un incubateur. La différence de poids entre un petit à la naissance et sa mère est dans un rapport d'un sur 800 ou 900 (un sur 20 pour l'homme), le plus élevé des mammifères placentaires, surpassé seulement par les [marsupiaux](https://fr.wikipedia.org/wiki/Marsupialia). Après environ 46 semaines, le petit est totalement sevré, et il peut se débrouiller seul à environ 18 mois.

Ayant une [fécondité](https://fr.wikipedia.org/wiki/F%C3%A9condit%C3%A9) naturellement faible, ils ont aussi beaucoup de difficultés à se reproduire en [captivité](https://fr.wikipedia.org/wiki/Captivit%C3%A9_(animal)). Le mâle, avec sa nourriture à portée de main, prend l'habitude de ne pas faire d'efforts, même pour se reproduire. Des problèmes psychologiques renforcent ce phénomène. Au Centre de recherche sur la reproduction des pandas géants à [Chengdu](https://fr.wikipedia.org/wiki/Chengdu) (Chine), seulement 10 % d'entre eux s'accouplent, et seulement 30 % des femelles accouplées font des petits. Afin de sauvegarder cette espèce menacée, les zoos et les centres d'élevage ont souvent recours à l'[insémination artificielle](https://fr.wikipedia.org/wiki/Ins%C3%A9mination_artificielle). Les premiers succès de cette technique ont été obtenus au zoo de [Pékin](https://fr.wikipedia.org/wiki/P%C3%A9kin) dès 1978. 

|[![Bébé panda dans un incubateur](https://upload.wikimedia.org/wikipedia/commons/9/96/Chengdu-pandas-d18.jpg)](https://commons.wikimedia.org/wiki/File:Chengdu-pandas-d18.jpg?uselang=fr)|
|:--:|
|Un bébé panda dans un incubateur.|

### **Longévité**

L'[espérance de vie](https://fr.wikipedia.org/wiki/Esp%C3%A9rance_de_vie) en captivité est de 20-25 ans, le record de [longévité](https://fr.wikipedia.org/wiki/Long%C3%A9vit%C3%A9) étant détenu par Jia Jia, une femelle hébergée à l'[Ocean Park Hong Kong](https://fr.wikipedia.org/wiki/Ocean_Park_Hong_Kong), qui est morte à 38 ans le 16 octobre 2016 (l'équivalent de 100 années humaines). Dans la nature, la longévité de l'animal est mal connue, mais serait d'une quinzaine d'années.

### **Pathologie**

Une femelle panda est morte en 2014 dans un zoo en Chine après des symptômes [gastroentériques](https://fr.wikipedia.org/wiki/Gastro-ent%C3%A9rite) et [respiratoires](https://fr.wikipedia.org/wiki/Maladie_respiratoire). La cause de la mort a été attribuée à une infection par [Toxoplasma gondii](https://fr.wikipedia.org/wiki/Toxoplasma_gondii), agent de la [toxoplasmose](https://fr.wikipedia.org/wiki/Toxoplasmose), une maladie qui peut affecter la plupart des animaux à sang chaud et les humains.

## Habitat et répartition

---

Leur habitat est réduit à six régions dispersées en Chine, dans des forêts de [montagnes](https://fr.wikipedia.org/wiki/Montagne) situées de 1 800 à 3 500 m d’altitude.

Il y a, en 2014, 26 réserves qui hébergent environ 60 % des 1 000 à 3 000 pandas survivants.

Au sein de ces parcs protégés comme en pleine nature, les animaux sont éparpillés en minuscules groupes ne circulant pas librement d’une montagne à l’autre en raison des vallées occupées par l’homme, ce qui ne favorise pas la reproduction. 

|[![Répartition géographique du panda](https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Mapa_distribuicao_Ailuropoda_melanoleuca.png/220px-Mapa_distribuicao_Ailuropoda_melanoleuca.png)](https://commons.wikimedia.org/wiki/File:Mapa_distribuicao_Ailuropoda_melanoleuca.png?uselang=fr)|
|:--:|
|Répartition du panda géant en Asie.|

## Menaces et production

---

Cette espèce, très menacée, figure sur la liste des espèces de l'[annexe I de la CITES](https://fr.wikipedia.org/wiki/Annexe_I_de_la_CITES) ([Convention sur le commerce international des espèces de faune et de flore sauvages menacées d'extinction](https://fr.wikipedia.org/wiki/Convention_sur_le_commerce_international_des_esp%C3%A8ces_de_faune_et_de_flore_sauvages_menac%C3%A9es_d%27extinction), dite convention de Washington).

En [Chine](https://fr.wikipedia.org/wiki/Chine), tuer un panda a longtemps été passible de peine de mort, mais cette peine a été remplacée par de la prison en 2010.

Une ambitieuse politique de protection a permis à l’espèce de se développer. En 2016, celle-ci est passée du statut « en danger » à « vulnérable » sur la liste rouge de l’[Union internationale pour la conservation de la nature](https://fr.wikipedia.org/wiki/Union_internationale_pour_la_conservation_de_la_nature). Les autorités chinoises ont commencé dès les années 1960 à créer des réserves afin de protéger les pandas.

Actuellement, il y a environ 2 000 pandas vivant à l'état sauvage, principalement dans les provinces de Sichuan et Shaanxi en Chine, et près de 600 pandas vivant en captivité.

[![Pandas qui jouent](https://previews.123rf.com/images/xiebiyun/xiebiyun1311/xiebiyun131100140/23426581-deux-grands-pandas-qui-jouent-ensemble-%C3%A0-chengdu-province-du-sichuan-en-chine.jpg)](https://upload.wikimedia.org/wikipedia/commons/5/5b/Pandas_playing_640x480.ogv)

---

---

### **Source :**

|[![Page Wikipédia sur le Panda géant](https://logo-marque.com/wp-content/uploads/2020/09/Wikipedia-Embleme.png)](https://fr.wikipedia.org/wiki/Panda_g%C3%A9ant)|
|:--:|
|Page Wikipédia sur le Panda géant|