Mondstadt (« Cité lunaire ») est la première des sept nations de Teyvat. C'est également dans cette région que débute l'aventure du Voyageur après avoir perdu sa/son jumeau. C'est dans la cité-état qu’on rend un culte à Barbatos, l'Archon Anémo.

Depuis sa création, Mondstadt célèbre chaque année le festival appelé Ludi Harspatum. Il s'agit de 15 jours de fête en l'honneur de Barbatos.

Le Prologue se passe dans cette région. Dans l'Aperçu de l'histoire : Les Traces, Dainsleif dit de Mondstadt : 

*« Le dragon qui pendant mille ans a défendu la cité de la liberté a enfin fait face à son dilemme. 
Qu'est-ce que la liberté, lorsqu'elle n'est que l'ordre d'un dieu ? »*

« La Cité de la liberté », « le Paradis des bergers », « la Couronne du Nord »... Telle est appelée Mondstadt depuis un certain temps par les bardes et les peintres.
Ici, ils conservent tous une passion pour la poésie, la musique, le bon vin et les fêtes... On peut même dire que ces choses rythment leur vie.

| Régions | Descriptions | Images |
|---------|-------------|---|
| Domaine de l'aurore | De nombreux vignobles aux raisins de bonne qualité situé au sud-ouest de Mondstadt. Tous les ans, une partie de la production de vin est distribuée dans la cité de Mondstadt et l'autre partie est vendue dans toutes les régions du continent.| ![domaine de l'aurore](https://static.wikia.nocookie.net/genshinimpact/images/4/45/Panorama_Domaine_de_l%27Aurore.jpg/revision/latest/scale-to-width-down/1000?cb=20210216152600&path-prefix=fr) |
| Dauclaire   | Un petit village calme situé au sud de Mondstadt, séparé de la cité par le Lac de Cidre. La plupart de ses habitants vivent de la chasse et fournissent des viandes de qualité à tous les grands restaurants de Mondstadt.|![Dauclaire](https://static.wikia.nocookie.net/genshinimpact/images/9/94/Panorama_Deauclaire.jpg/revision/latest/scale-to-width-down/180?cb=20210216152557&path-prefix=fr) |
| Territoire des loups | Les chasseurs s'aventurent difficilement dans cette forêt profonde où un inquiétant silence règne. Une meute de loups dangereux s'y est installée. | ![Territoire des Loups](https://static.wikia.nocookie.net/genshinimpact/images/b/b4/Territoire_des_loups.png/revision/latest/scale-to-width-down/180?cb=20210225145652&path-prefix=fr) |
| Antre de Stormterror | Celui qui habitait autrefois dans cette tour solitaire n'est plus. Les vents qui entourent la ville abandonnée murmurent les histoires les plus anciennes, dont nul mortel ne se souvient : ils parlent de leur ancien maître, l'esprit du vent sans nom., et leur chant fait vaciller la flèche de la tour... | ![Antre de Stormeterror](https://static.wikia.nocookie.net/genshinimpact/images/b/b9/Panorama_Antre_de_Stormterror.jpg/revision/latest/scale-to-width-down/180?cb=20210216152452&path-prefix=fr) |
| Montagnes de Brillecouronne | Ensemble de montagnes. Un joli paysage a aller découvrir en s'aventurant dans Teyvat. | ![Montagnes de Brillecouronne](https://static.wikia.nocookie.net/genshinimpact/images/a/ab/Canyon_de_Brillecouronne.png/revision/latest/scale-to-width-down/180?cb=20210303172702&path-prefix=fr) |