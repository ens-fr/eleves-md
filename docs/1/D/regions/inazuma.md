Inazuma (Japonais: 稲妻 Inazuma; Chinois: 稻妻 Dàoqī) est l'une des sept nations de Teyvat. C'est la cité-état qui vénère Baal, l'Archon Électro, qui est également le chef de son corps dirigeant, le Bakufu d'Inazuma.

Dans l'Aperçu de l'histoire : Les Traces, Dainsleif dit d'Inazuma : « Sur les terres recluses de l'immortel shogunat règne le Bakufu, et la Shogun Raiden. Mais que voient les mortels de l'éternité après laquelle court leur déesse... ? »





## Narukami

| Régions | Descriptions | Images |
|---------|-------------|---|
| Île d'Amakane | Une île de festival située au nord-ouest d'Inazuma. | ![Île d'Amakane](https://static.wikia.nocookie.net/gensin-impact/images/d/da/Amakane_Island.png/revision/latest/scale-to-width-down/180?cb=20210723062236) |
| Araumi | Comprend une grande zone souterraine. |![Araumi](https://static.wikia.nocookie.net/gensin-impact/images/f/ff/Araumi.png/revision/latest/scale-to-width-down/180?cb=20220213021036) |
| Plaine de Byakko |Séparant le village de Konda et Inazuma, on peut y trouver de nombreux renards sauvages. | ![Plaine de Byakko](https://static.wikia.nocookie.net/gensin-impact/images/d/d1/Byakko_Plain.png/revision/latest/scale-to-width-down/180?cb=20210805072450) |
| Forêt de Chinju | Une forêt tranquille et mystérieuse située au pied du Mont Yougou. On y trouve des statues de Tanuki partout, des portes torii dispersées dans la forêt et des sanctuaires abandonnés cachés dans les profondeurs... Comme une rivière qui coule, ils racontent les légendes qui serpentent dans un passé oublié. La rumeur dit qu'en se promenant dans la forêt de Chinju, on peut rencontrer des êtres mystérieux qui aiment faire des farces... |![Forêt de Chinju](https://static.wikia.nocookie.net/gensin-impact/images/d/df/Viewpoint_The_Sacred_Forest_in_the_Moonlight.png/revision/latest/scale-to-width-down/180?cb=20210723005710) |
| Île de Jinren | Un groupe d'îles brisées à l'extrémité nord d'Inazuma. | ![Île de Jinren](https://static.wikia.nocookie.net/gensin-impact/images/a/ac/Jinren_Island.png/revision/latest?cb=20210805071911) |
| Ville d'Inazuma | Le quartier le plus animé et le plus prospère d'Inazuma, où vit la majorité de la population d'Inazuma. De Hanamizaka aux rues de la ville, vous pouvez suivre le sentier pour visiter les boutiques traditionnelles locales et goûter aux spécialités d'Inazuma. La Commission Tenryou est également située dans la ville. Elle régit les questions relatives à l'ordre et à la sécurité publics, ainsi que les affaires militaires du Shogunat. Au sommet de la ville d'Inazuma se trouve le Tenshukaku, où le Shogun Raiden réside et règne sur Inazuma. |![Ville d'Inazuma](https://static.wikia.nocookie.net/gensin-impact/images/4/43/Inazuma_City.png/revision/latest/scale-to-width-down/180?cb=20210720003854) |
| Mont Yougou | Une montagne sur l'île de Narukami, où se trouve le Grand sanctuaire de Narukami et le Sakura sacré. | ![Mont Yougou](https://static.wikia.nocookie.net/gensin-impact/images/b/be/Mt_Yougou.png/revision/latest/scale-to-width-down/180?cb=20210723060759) |
| Grand sanctuaire de Narukami | Le plus grand sanctuaire d'Inazuma, situé au sommet du Mont Yougou. Il est dédié à Son Excellence, le tout-puissant Narukami Ogosho. Ceux qui le souhaitent peuvent monter au sommet de la montagne, une marche après l'autre. Ceux qui ont obtenu les faveurs de l'Electro peuvent utiliser les sphères de tonnerre pour s'envoler et atteindre le sanctuaire entouré de l'immense Sakura sacré. Le Sakura Sacré est en constante floraison, symbolisant l'éternité de Narukami. Après avoir franchi les nombreuses portes torii, trouvez la jeune fille du sanctuaire pour tirer un billet de bonne aventure et demander la bénédiction de Narukami. |![Grand sanctuaire de Narukami](https://static.wikia.nocookie.net/gensin-impact/images/8/84/Grand_Narukami_Shrine.png/revision/latest/scale-to-width-down/180?cb=20210726210846) |
| Ritou | Pour explorer correctement le royaume du Shogun en cette période de décret Sakoku, vous devez d'abord passer le poste de contrôle de Ritou. Ritou est sous la juridiction de la Commission Kanjou et les gens ont besoin de divers documents pour obtenir le passage. Cependant, pour obtenir le permis de voyage nécessaire, vous devez suivre une procédure très compliquée et avoir les bonnes relations. Sans le permis de voyage, vous ne pouvez vous arrêter que brièvement à Ritou. Vous verrez d'autres étrangers qui sont également coincés là, regardant ensemble le paysage désolé du village de pêcheurs aux érables rouges et aux tuiles vertes. | ![Ritou](https://static.wikia.nocookie.net/gensin-impact/images/0/07/Ritou.png/revision/latest/scale-to-width-down/180?cb=20210709174550) |
| Village de Konda  | Village étape entre Ritou et la cité d'Inazuma. Depuis le Décret de confinement, plus grand monde ne s'y arrête.|![Village de Konda](https://static.wikia.nocookie.net/genshinimpact/images/f/f4/Village_de_Konda.png/revision/latest/scale-to-width-down/180?cb=20210908145347&path-prefix=fr) |
| Domaine Kamisato | La résidence du clan Kamisato qui supervise la Commission Yashiro. |![Domaine Kamisato](https://static.wikia.nocookie.net/gensin-impact/images/4/41/Kamisato_Estate.png/revision/latest/scale-to-width-down/180?cb=20210805071101) |

### Infos

L'île de Narukami a été vue pour la première fois dans le programme spécial de la version 1.5.
L'art conceptuel de Tenshukaku a été montré.

Les images conceptuelles du Grand sanctuaire de Narukami et des Profondeurs du Mont Yougou ont été montrées dans le programme spécial de la version 1.6.

Déverrouiller la statue des Sept sur l'île de Narukami illuminera la carte de la mer entre Inazuma et le continent de Teyvat, qui n'appartient à aucun pays.

### Étymologie

Narukami (japonais : 鳴る神 " dieu rugissant ") peut faire référence à Raijin, le dieu de la foudre, du tonnerre et des orages dans la mythologie japonaise.

Il peut également faire référence à l'autre titre du Shogun Raiden, "Narukami Ogosho".

## Kannazuka

| Régions | Descriptions | Images |
|---------|-------------|---|
| Tatarasuna | La légende dit que Kannazuka signifie "la colline où aucun dieu ne réside". Jusqu'à présent, Kannazuka est un territoire disputé entre le Shogunat et Sangonomiya. Kannazuka a un terrain montagneux avec de hautes falaises abruptes. Au milieu de ces affleurements rocheux se trouve le four Mikage, la plus grande fonderie d'Inazuma. C'est là que l'on fabrique l'acier de jade, un matériau rare qui est utilisé à Inazuma pour forger des épées.| ![Tatarasuna](https://static.wikia.nocookie.net/gensin-impact/images/c/ca/Viewpoint_Tatara_Islands%27_Vantage_Point.png/revision/latest/scale-to-width-down/180?cb=20210723005252) |
| Campement Kujou | Le camp militaire du Shogunat à Kannazuka. Il y aura toujours un conflit irréconciliable entre le peuple de Narukami qui croit au Shogun Raiden et le peuple de Sangonomiya qui croit au Watatsumi Omikami. Le conflit entre les deux parties n'a fait qu'empirer depuis l'entrée en vigueur du décret sur la chasse à la vision. Pour réprimer la rébellion de Sangonomiya, la Commission Tenryou a stationné l'armée du Shogun ici. Par rapport aux forces de Sangonomiya, l'armée du Shogun a plus de troupes, est mieux entraînée et est bien équipée. La victoire semble assurée pour ces dernières.|![Campement Kujou](https://static.wikia.nocookie.net/gensin-impact/images/3/34/Viewpoint_Front_Line%2C_Kannazuka.png/revision/latest/scale-to-width-down/180?cb=20210723005343) |

### Infos

Kannazuka a été vu pour la première fois dans le programme spécial de la version 1.5.
Des images conceptuelles de Tatarasuna et de Mikage Furnace ont été montrées.

Bien que Kannazuka soit "la colline où aucun dieu ne réside", elle semble abriter une entité puissante sous la terre, scellée par les Sakuras du Tonnerre.

### Ethymologie

Kannazuka est dérivé du japonais Kannadzuka (japonais : 神かん無な塚づか), qui peut signifier "la butte où les dieux sont absents".

## Yashiori

| Régions | Descriptions | Images |
|---------|-------------|---|
| Fort Fujitou | Un camp militaire précédemment occupé par l’armée Watatsumi.| ![Fort Fujitou](https://static.wikia.nocookie.net/gensin-impact/images/a/a1/Fort_Fujitou.png/revision/latest/scale-to-width-down/180?cb=20210710064710) |
| Fort Mumei | Une zone en ruine sur le côté est de l’île de Yashiori.|![Fort Mumei](https://static.wikia.nocookie.net/gensin-impact/images/6/6a/Fort_Mumei.png/revision/latest/scale-to-width-down/180?cb=20210805035758) |
| Village Higi |Un village abandonné.|![Village Higi](https://static.wikia.nocookie.net/gensin-impact/images/d/d6/Higi_Village.png/revision/latest/scale-to-width-down/180?cb=20220213015354) |
| Mine de Jakotsu | La zone la plus méridionale de l’île de Yashiori|![Mine de Jakotsu](https://static.wikia.nocookie.net/gensin-impact/images/e/ef/Jakotsu_Mine.png/revision/latest/scale-to-width-down/180?cb=20210723115639) |
| La tête du serpent | Du serpent géant vaincu par l'Archonte de l'électricité avec le Musou no Hitotachi, il ne reste aujourd'hui que les restes blanchis. Bien que son crâne regarde toujours vers le ciel avec un air de défi, le dieu est déjà mort, et le nom de " Watatsumi Omikami " est peu à peu oublié. Même si le serpent géant est mort depuis longtemps, on peut encore clairement sentir les traces de sa puissance. Sous l'influence de Tatarigami, l'île Yashiori a été désertée par ses habitants. Ce que l'on peut voir sur l'île n'est qu'une pluie incessante et des tempêtes qui tentent d'effacer les vestiges du passé.|![La tête du serpent](https://static.wikia.nocookie.net/gensin-impact/images/1/18/Viewpoint_Overlooking_Serpent_Head.png/revision/latest/scale-to-width-down/180?cb=20210723005530) |
| Gorges de Musoujin | Cette étroite et majestueuse vallée de rift traverse la partie orientale de l'île Yashiori. Cette vue extraordinaire n'est pas une formation naturelle, mais plutôt le résultat du combat au cours duquel l'Archon Électrique a tué le serpent géant. La vallée est nommée d'après le Musou no Hitotachi, un témoignage de l'habileté de l'Archon Électrique à manier l'épée. À ce jour, les restes du serpent géant sont encore éparpillés sur l'île de Yashiori. Les Tatarigami qui ont émergé des restes de ce dieu semblent s'éveiller...|![Gorges de Musoujin](https://static.wikia.nocookie.net/gensin-impact/images/b/be/Viewpoint_Rift_Valley%2C_Yashiori_Island.png/revision/latest/scale-to-width-down/180?cb=20210723005935) |
| Plage de Nazuchi  | Située entre l'île Yashiori et Kannazuka, la plage de Nazuchi est un lieu ravagé par la guerre. Sur la côte peu profonde, à part les ibis violets qui cherchent un endroit pour se reposer, on trouve des drapeaux déchirés et des flèches brisées des batailles précédentes, ainsi que des mâts brisés et des ponts fracassés des épaves de bateaux. En raison de la guerre, les pirates et les ronin infestent l'endroit à la recherche de butin. Même le rare Dendrobium fleurit dans cet endroit...|![Plage de Nazuchi](https://static.wikia.nocookie.net/gensin-impact/images/9/93/Viewpoint_Tidal_Flat_Amidst_the_Flames_of_War.png/revision/latest/scale-to-width-down/180?cb=20210723005616) |

### Lore

Après que Makoto et sa sœur jumelle Ei eurent réuni Inazuma sous leur domination, aucun des dieux restants ne chercha à revendiquer un territoire qui n'était pas le sien, de peur d'être éradiqué par l'Archonte. Orobashi no Mikoto, connu sous le nom de Watatsumi Omikami, était l'un de ces dieux. Cependant, pour des raisons inconnues, il entra soudainement en conflit avec le Shogunat. Orobashi et Ei s'affrontèrent sur l'île Yashiori, où elle le tua à l'aide du Musou no Hitotachi et divisa l'île en deux, créant ainsi les gorges de Musoujin. La haine persistante d'Orobashi se transforma en Tatarigami, qu'elle supprima à l'aide de ses protections. Les habitants de l'île Watatsumi acceptèrent ensuite à contrecœur la domination de Baal, bien qu'ils continuent à vénérer Orobashi, même aujourd'hui.

Avec les sorciers, le clan Kitain, descendant de Kitain Bunsou, a pris la relève des gardiens Yashiori qui ont supprimé le Tatarigami. Cependant, la lignée Kitain s'est éteinte pour des raisons inconnues.

Avec la mise en œuvre du décret Sakoku et du décret Vision Hunt, les tensions entre le Shogunat et le peuple de Watatsumi se transforment en guerre, la Résistance Sangonomiya se formant pour résister ouvertement au règne du Shogun. Un groupe radical de la Résistance, à l'insu de son chef Sangonomiya Kokomi, s'est déguisé en troupes du Shogunat pour s'approcher des protections de Baal et les détruire. Ils pensaient que cela restaurerait la "dignité" d'Orobashi, mais cela n'a servi qu'à libérer le Tatarigami sur l'île. On ne sait pas encore s'ils étaient conscients de ce qui allait se passer.

Sans les gardes pour les tenir à distance, les Tatarigami ont commencé à faire des ravages sur l'île. Les habitants du village de Higi ont commencé à succomber à des maladies mystérieuses et à la folie, laissant le village abandonné au moment où le voyageur explore la région. Les troupes du Shogunat stationnées dans la région ont également été durement touchées, ce qui a poussé le général Kujou Masahito à envoyer un rapport à son père, le commissaire de Tenryou Kujou Takayuki, sur la perturbation causée par le Tatarigami. Takayuki, furieux de ce qu'il considérait comme l'incompétence de Masahito, envoya une équipe d'enquêteurs pour découvrir la "vérité". Sur la base des conclusions de cette équipe d'enquête, Takayuki allait envisager de rétrograder Masahito et d'affecter Kujou Sara à la place.

Au fil des textes perdus trouvés sur l'île Yashiori, l'équipe d'enquêteurs a découvert que Masahito ne mentait pas et n'exagérait pas les effets du Tatarigami. Ils ont commencé à succomber à l'influence du Tatarigami et le groupe entier est maintenant présumé mort ou disparu.

## Seirai

| Régions | Descriptions | Images |
|---------|-------------|---|
| Pic d'Amakumo | Il y a longtemps, Kanna Kapatcir a été tué ici, tandis que le pouvoir de sa haine et de ses regrets persistants était supprimé par des murailles maintenues par le sanctuaire d'Asase. Au cours de la rébellion de Seirai, il y a environ 500 ans, Asase Hibiki a levé les protections, provoquant l'apparition de la manifestation du tonnerre et dévastant la région avec une énorme tempête électrique.| ![Pic d'Amakumo](https://static.wikia.nocookie.net/gensin-impact/images/e/e5/Amakumo_Peak.png/revision/latest/scale-to-width-down/180?cb=20210902003454) |
| Sanctuaire d'Asase |Le sanctuaire a été créé pour supprimer le pouvoir persistant de Kanna Kapatcir, également connu sous le nom d'oiseau-tonnerre.|![Sanctuaire d'Asase](https://static.wikia.nocookie.net/gensin-impact/images/4/44/Asase_Shrine.png/revision/latest/scale-to-width-down/180?cb=20210901055302) |
| Fort Hiraumi | Situé dans la partie nord-est de l'île de Seirai.|![Fort Hiraumi](https://static.wikia.nocookie.net/gensin-impact/images/6/66/Fort_Hiraumi.png/revision/latest/scale-to-width-down/180?cb=20210902004059) |
| Village de Koseki | Situé au nord-ouest du pic Amakumo.|![Village de Koseki](https://static.wikia.nocookie.net/gensin-impact/images/4/45/Koseki_Village.png/revision/latest/scale-to-width-down/180?cb=20210901044510) |
| Seiraimaru | La zone doit son nom au "Seiraimaru", le navire amiral de la flotte d'Ako Domeki, qui a été détruit ici.|![Seiraimaru](https://static.wikia.nocookie.net/gensin-impact/images/1/17/Seiraimaru.png/revision/latest/scale-to-width-down/180?cb=20210901044514) |

### Étymologie

En japonais, seirai (japonais : 清せい籟らい) est un terme qui décrit le "son pur et rafraîchissant du vent soufflant dans les arbres."

Il convient toutefois de noter que le nom japonais de l'île comporte " Seirai " écrit exclusivement en katakana ; de la même manière que les différentes zones de l'île de Tsurumi sont nommées.
En supposant que Seirai est un nom d'origine Ainu comme divers endroits de Tsurumi, le nom peut alternativement être décomposé en mots Ainu セィ sey, signifiant "coquillage", et ラィ ray, signifiant "mort".

## Watatsumi

| Régions | Descriptions | Images |
|---------|-------------|---|
| Sanctuaire Sangonomiya | Un sanctuaire situé au centre de l'île Watatsumi.| ![Sanctuaire Sangonomiya](https://static.wikia.nocookie.net/gensin-impact/images/f/fd/Watatsumi_Island_Sangonomiya_Shrine.png/revision/latest/scale-to-width-down/180?cb=20210820155707) |
| Piscine Suigetsu  | Situé dans la partie nord-est de l'île Watatsumi.|![Piscine Suigetsu](https://static.wikia.nocookie.net/gensin-impact/images/9/9c/Suigetsu_Pool.png/revision/latest/scale-to-width-down/180?cb=20210901044520) |
| Village de Bourou | Un petit village situé dans la partie sud de l'île Watatsumi. Il servait de camp où les soldats étaient formés pendant la lutte de l'armée Watatsumi contre le Shogunat.| ![Village de Bourou](https://static.wikia.nocookie.net/gensin-impact/images/1/11/Bourou_Village.png/revision/latest/scale-to-width-down/180?cb=20210901044517) |
| Sanctuaire de Mouun | Un sanctuaire abandonné dans la partie ouest de l'île Watatsumi.|![Sanctuaire de Mouun](https://static.wikia.nocookie.net/gensin-impact/images/2/2d/Mouun_Shrine.png/revision/latest/scale-to-width-down/180?cb=20210901055428) |

### Étymologie

Dans la mythologie japonaise, Watatsumi (japonais : 海神わたつみ), ou parfois Wadatsumi, est le nom de la divinité tutélaire (kami) de la mer, souvent assimilée au dieu dragon Ryuujin. Dans certains cas, le terme peut être utilisé pour désigner la mer elle-même.

Le sanctuaire Sangonomiya pourrait être inspiré de Ryuuguu-jou, le palais sous-marin de Ryuujin dans le folklore japonais. L'un des autres noms de Ryuuguu est "Palais Watatsumi" (japonais : 海わたつみ宮のみや Watatsumi no Miya).

### Lore

L'île de Watatsumi tire son nom de l'expression " le dieu des océans " de l'ancienne langue de Watatsumi. Elle a été créée par Orobashi après la guerre des Archontes. Les coraux géants trouvés sur l'île étaient des branches de corail qui s'étaient détachées du corps d'Orobashi. Ces coraux ont ensuite été utilisés comme échelle par le peuple d'Enkanomiya pour remonter à la surface Le peuple d'Enkanomiya est devenu l'ancêtre du peuple de Watatsumi. Sous la direction du clan Sangonomiya, ils vénèrent l'Omikami de Watatsumi (japonais : 海祇大御神) au lieu du Shogun Raiden.

Le sanctuaire Sangonomiya est situé au centre de cette île, entouré d'un couvert de montagnes et de chutes d'eau. La distance qui les sépare de la capitale a permis aux résidents de cette île de développer une culture et des coutumes uniques, et de ce fait, de nombreux Inazumains en dehors de cette île sont bien conscients des mauvaises relations qu'ils entretiennent avec le Shogun et son gouvernement - une guerre brutale a eu lieu entre les deux il y a longtemps, mais elle s'est finalement terminée avec le clan Sangonomiya qui a recherché la paix.

En conséquence, le décret sur la chasse aux visions a suscité une hostilité sans précédent entre le clan Sangonomiya et les partisans du Shogun Raiden, en particulier la Commission Tenryou, et l'armée de Watatsumi s'est soulevée pour s'opposer au décret.


## Tsurumi

| Régions | Descriptions | Images |
|---------|-------------|---|
| Plaines d'Autake | ... | ![Plaines d'Autake](https://static.wikia.nocookie.net/gensin-impact/images/c/cb/Autake_Plains.png/revision/latest/scale-to-width-down/185?cb=20211016235958) |
| Sanctuaire de Chirai | ... |![Sanctuaire de Chirai](https://static.wikia.nocookie.net/gensin-impact/images/4/41/Chirai_Shrine.png/revision/latest/scale-to-width-down/185?cb=20211017000214) |
| Site cérémoniel Moshiri | ... | ![Site cérémoniel Moshiri](https://static.wikia.nocookie.net/gensin-impact/images/4/43/Moshiri_Ceremonial_Site.png/revision/latest/scale-to-width-down/185?cb=20211019122853) |
| Mont Kanna | ... |![Mont Kanna](https://static.wikia.nocookie.net/gensin-impact/images/7/79/Mt._Kanna.png/revision/latest/scale-to-width-down/185?cb=20211016064054) |
| Plage Oina | ... | ![Plage Oina](https://static.wikia.nocookie.net/gensin-impact/images/0/0f/Oina_Beach.png/revision/latest/scale-to-width-down/185?cb=20211016065110) |
| Pic de Shirikoro | ... |![Pic de Shirikoro](https://static.wikia.nocookie.net/gensin-impact/images/d/d9/Shirikoro_Peak.png/revision/latest/scale-to-width-down/185?cb=20211015173319) |
| Haut-fond de Wakukau | ... |![Haut-fond de Wakukau](https://static.wikia.nocookie.net/gensin-impact/images/0/0e/Wakukau_Shoal.png/revision/latest/scale-to-width-down/185?cb=20211017000111) |

### Infos

Tsurumi s'inspire de la culture des Ainus, la tribu indigène et les natifs des îles Hokkaido, Sakhaline et Kouriles. Les lieux, les personnages et d'autres éléments de Tsurumi sont nommés d'après des mots de la langue ainu, représentée dans le jeu comme la langue du script Ishine.

Il est à noter que la version japonaise du jeu n'utilise pas les petits caractères kana de la langue ainu pour écrire correctement les mots ainu et opte plutôt pour des kana de taille normale ; comme ピリカ チカッポ カパッチリカムイ Pirika cikappo kapatciri-kamui au lieu de ピㇼカ チカッポ カパッチㇼカムィ Pirka cikappo kapatcir-kamuy. (Phrase ainu tirée de la description d'Inazuma : Eagleplume.)

La grue (japonais : 鶴 tsuru) - ou plus précisément, la grue à couronne rouge (japonais : 丹頂鶴 tanchou-dzuru/タンチョウ tanchou) - est connue comme un symbole de longévité dans les cultures d'Asie de l'Est. Dans le folklore japonais, on dit qu'il vit mille ans et qu'il accorde des faveurs en échange d'actes de sacrifice.

Cela pourrait faire allusion aux illusions persistantes des habitants de l'île de Tsurumi, bien que la tribu d'origine ait péri après avoir subi la colère de Kanna Kapatcir suite au sacrifice de Ruu il y a des milliers d'années. Cette idée de longévité peut également s'étendre au regret persistant de l'oiseau-tonnerre, qui a donné naissance aux manifestations du tonnerre du pic Amakumo et du mont Kanna.
L'ancienne civilisation qui habitait l'île de Tsurumi (avant le peuple de Ruu) est probablement liée à Khaenri'ah et Celestia, car comme l'île de Seirai, l'île abrite une énorme ruine souterraine qui contient des Gardiens de la Ruine. Plus particulièrement, des peintures murales de Celestia sont représentées sur certains de ses murs. Selon Sumida, quelque chose serait tombé de Celestia sur l'île, ce qui, selon Kama, aurait manipulé les lignes ley de l'île.

Par conséquent, les "personnes" présentes sur l'île pendant la série Through the Mist ne sont que des fantômes qui revivent le passé. Cependant, l'un d'entre eux, Ruu, peut entendre et voir tout ce qui l'entoure et a développé une conscience de soi. Les esprits des défunts peuvent également être trouvés sur l'île.

Cependant, l'incident de Tsurumi se serait produit il y a des milliers d'années, ce qui serait antérieur à l'incident de Khaenri'ah.

Les coffres remarquables ne peuvent être trouvés que sur l'île de Tsurumi.

L'île de Tsurumi est actuellement la seule zone non-instantanée du jeu qui présente des frayages d'ennemis variés en raison de son effet de boucle temporelle, qui se répète tous les trois jours. Seules quelques fractions ne sont pas affectées par cette boucle.