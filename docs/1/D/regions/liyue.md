Liyue (Chinois : 璃月, Líyuè) est l'une des sept nations de Teyvat. On y adore Morax, l'Archon Géo.

Les Quêtes d'Archon du Chapitre 1 : Adieu à l'Exuvia, se passe dans cette région.

Dans l'Aperçu de l'histoire : Les Traces, Dainsleif dit de Liyue : « Sous le regard horrifié de tous, le Dieu des contrats a été assassiné. À l'instant ultime, il signera le contrat qui mettra fin à tous les contrats. »

Une riche cité portuaire situé à l'est de Teyvat.
Liyue possède une topographie variée, constituée de montagnes, de forêts de pierres, de vastes plaines et de plages qui demeurent toutes aussi belles au fil des saisons. Combien de trésors anciens gracieusement cachés par l'Archon Géo attendent-ils d'être découverts dans ce magnifique paysage ? 

## Histoire
---
Liyue est la plus ancienne des sept nations de Teyvat, établie il y a 3 700 ans lorsque le Souverain de la Roche signa un contrat avec les Adeptes pour protéger le Port de Liyue.

Bien avant la création du Port de Liyue, lorsque de nombreux dieux parcouraient les terres de Teyvat et avant que les Sept ne prennent leurs positions, de nombreuses civilisations existaient sur les terres qui allaient devenir Liyue. À cette époque, Morax régnait aux côtés de la Déesse de la Poussière, Guizhong, sur les terres connues aujourd'hui sous le nom de Plaines de Guili. Cette civilisation prospéra sous leur règne commun et la protection des Adeptes.

Cependant, lorsque la Guerre des Archons éclata, ce peuple connu sa fin au cours d'une bataille féroce qui se solda par la mort de Guizhong, tandis que la région connue aujourd'hui sous le nom de Marais de Dihua, passa d'un îlot fertile en un marécage. Morax emmena son peuple au sud des Mont Tianheng et établit le Port de Liyue, où il se trouve encore aujourd'hui. Afin de protéger cette nouvelle ville, Morax conclut un contrat avec les Adeptes et fit la guerre aux autres dieux. Entre l'assujettissement de dieux comme Ortell, et la mort d'autres, comme Havria, Morax finit par devenir le seul vainqueur de Liyue et prit sa place parmi les Sept en tant qu'Archon Géo.

Néanmoins, Liyue subit les ravages de nombreux êtres maléfiques. Les corps des dieux tués durant la guerre répandirent une haine qui prit la forme d'esprits démoniques, de pestes, de monstres et de mutations qui commencèrent à infester les terres. Pour les combattre, le Souverain de la Roche convoqua cinq Yakshas. Il est décrit que chaque Yaksha représentait un certain élément de Teyvat. Malheureusement, trois d'entre eux tombèrent sous le joug de la corruption et de la peur après des années d'effusion de sang. Certains se retournèrent les uns contre les autres, et d'autres s'abandonnèrent aux ténèbres. Seul l'un d'entre eux subsista, tandis qu'un autre disparut sans laisser de trace.

Une fois par an, le Souverain de la roche se manifeste au peuple de Liyue lors du rituel de la descente, au cours duquel il fait don à la ville de ses prophéties offrant ses conseils sur le développement économique à suivre pour l'année qui arrive. Les affaires courantes sont gérées par le Liyue Qixing, les Huit Corporations et les Millelithes.

| Régions | Descriptions | Images |
|---------|-------------|---|
| Port de Liyue |La construction du port a contribué au développement du commerce maritime à Liyue. Le volume de marchandises importées et exportées dans le plus grand port commercial de Teyvat dépasse de loin celui de ports ordinaires.Tous les ans, les milliers de lanternes célestes s'élèvent dans le ciel étoilé pendant le festival des lanternes que vous ne devriez pas rater lors de votre visite de Liyue.| ![Port de Liyue](https://static.wikia.nocookie.net/genshinimpact/images/2/2e/Port_de_Liyue.png/revision/latest/scale-to-width-down/180?cb=20210321142500&path-prefix=fr) |
| Monts Tianheng  | Une chaîne de montagnes située à l'ouest de Liyue qui constitue une barrière de défense naturelle contre des ennemis. Des murs, des remparts et des fortifications, dont vous pouvez aujourd'hui admirer les ruines, ont été construits pendant les nombreuses guerres dans lesquelles Liyue a combattu. On raconte aussi que les premières opérations minières des Liyuéens eurent lieu ici.|![Monts Tianheng](https://static.wikia.nocookie.net/genshinimpact/images/0/0d/Monts_Tianheng.jpg/revision/latest/scale-to-width-down/180?cb=20210321142617&path-prefix=fr) |
| Marais Dihua | La faune et la flore variée qui s'y trouve constituent un écosystème riche ainsi qu'un paysage admiré par de nombreux voyageurs. Il est nécessaire de passer par le Marais Dihua sur le chemin terrestre de Mondstadt à Liyue, alors les convois de marchandise s'arrêtent souvent ici pour prendre une pause. Au loin, on peut distinguer l'Auberge Wangshu qui se dresse au fond. Ce bâtiment célèbre est un point de repère pour les commerçants itinérants. | ![Marais Dihua](https://static.wikia.nocookie.net/genshinimpact/images/3/39/Marais_Dihua.jpg/revision/latest/scale-to-width-down/180?cb=20210321142707&path-prefix=fr) |
| Auberge Wangshu | Une auberge symbolique construite sur un pilier de pierre géant dans le Marais Dihua. Très prisée par les commerçants itinérants qui y prennent une pause pendant leur voyage, l'auberge est devenue un lieu où ils peuvent faire leurs affaires. Elle offre également une excellente vue sur le Mont Qingce et Jueyunjian s'il fait beau. Une rumeur qui court dit que l'auberge a été construite pour accomplir un devoir mystérieux qui se transmet depuis de génération en génération, mais personne n'a jamais pu vérifier sa véracité. | ![Auberge Wangshu](https://static.wikia.nocookie.net/genshinimpact/images/2/2b/Auberge_Wangshu.jpg/revision/latest/scale-to-width-down/180?cb=20210321142758&path-prefix=fr) |
| Village de Qingce | Un village situé au nord de Liyue, caché entre des collines et des forêts de bambous. Des champs en terrasses en forme de demi-lune s'étendent telles des rangées d'écailles sur la vallée enveloppée par la brume. Lorsque le vent se lève, les cultures ondulent doucement comme des vagues colorées.Dans ce village mystérieux isolé du monde, les habitants vivent une vie tranquille mais épanouissante. | ![Village de Qingce](https://static.wikia.nocookie.net/genshinimpact/images/6/63/Village_de_Qingce.jpg/revision/latest/scale-to-width-down/180?cb=20210321142838&path-prefix=fr) |
| Jueyunjian | Des nuages de fumée s'élèvent souvent des montagnes désertes qui entourent Liyue. De beaux paysages se cachent dans ces montagnes à la topographie complexe et aux pistes dangereuses où les Adeptes, gardiens de Liyue, vivent en ermite. Des personnes qui souhaitent obtenir leur bénédiction font parfois des pélerinages jusqu'ici. Cependant, étant donnée les dangers potentiels pour des étrangers, il vous sera vivement conseillé d'observer Jueyunjian de loin, depuis l'Auberge Wangshu. | ![Jueyunjian](https://static.wikia.nocookie.net/genshinimpact/images/e/eb/Jueyunjian.jpg/revision/latest/scale-to-width-down/180?cb=20210321142923&path-prefix=fr) |

## Anecdotes
---
Liyue est inspirée de la Chine
En chinois, Liyue signifie « Lapis lazuli », « Pierre de lune précieuse » ou «  Lune de verre (ou vitrée) ». En plus de celles-ci, Mihoyo a aussi proposé « Lune de Jade » comme traduction possible en utilisant le radical de 玉 (jade) dans le Li (璃) de « Liyue ».

Le Li (璃) de « Liyue » peut se décomposer en 王 (wáng), qui signifie « roi » et en 离 (lí), qui signifie « départ », il s’agit du même li que celui que l’on trouve dans le nom « Zhongli » en chinois simplifié. Le yue (月) de Liyue peut aussi signifier « mois », ainsi, le nom « Liyue » pourrait aussi avoir ce second sens : « Le mois du départ du roi », qui présente une similitude frappante avec une traduction possible du nom « Zhongli » (« L’heure du départ »).

On dit de Liyue qu’il s’agit d’un centre économique (comme c’est le cas de nombreuses villes portuaires en Chine).

Liyue est la plus prospère des Sept Nations.

Dans « Wangsheng », Zhongli affirme que Liyue est riche d’une histoire de 3 700 ans. Ganyu confirme aussi que Liyue fut fondée il y a 3 700 ans.