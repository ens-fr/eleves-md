Le Gouffre est une zone située à l'ouest de Lisha, Liyue, et constitue la principale source de minerais de la nation. Récemment, une série d'accidents mystérieux a entraîné la fermeture de toute la mine, mettant ses travailleurs au chômage pour le moment.

Esther remarque que le climat du Gouffre est complètement différent de celui du reste de Liyue, bien qu'elle ne donne pas de détails sur cela.

## La Surface

| Régions | Descriptions | Images |
|---------|-------------|---|
| Falaise de Cinnabar |Indisponible | ![Falaise de Cinnabar](images/cinnabar_cliff.png){ width=200 } |
| Vale Fuao | Indisponible |![Vale Fuao](images/fuao_vale.png) {width=200} |
| Pic de Glaze | Indisponible | ![Pic de Glaze](images/glaze_peak.png) {width=200} |
| Vallée de Lumberpick | Indisponible | ![Vallée de Lumberpick](images/lumberpick_valley.png) {width=200} |
| La faille du gouffre | Indisponible | ![La faille du gouffre](images/the_chasm_maw.png) {width=200} |
| La surface | Indisponible | ![La surface](images/the_surface.png) {width=200} |
| Gorges de Tiangong | Indisponible | ![Gorges de Tiangong](images/tiangong_gorge.png) {width=200} |

## Mines Souterraines

| Régions | Descriptions | Images |
|---------|-------------|---|
| Tunnel principal Ad-Hoc |Indisponible | ![Tunnel principal Ad-Hoc](images/main_tunnel.png) {width=200} |
| Ruines sans nom | Indisponible |![Ruines sans nom](images/nameless_ruins.png) {width=200} |
| Halls de pierre | Indisponible | ![Halls de pierre](images/stony_halls.png) {width=200} |
| Le gouffre : Principale zone minière | Indisponible | ![Le gouffre : Principale zone minière](images/the_chasm_main_mining.png) {width=200} |
| Le gouffre incandescent | Indisponible | ![Le gouffre incandescent](images/the_glowing_narrows.png) {width=200} |
| La grotte du serpent | Indisponible | ![La grotte du serpent](images/the_serpents_cave.png) {width=200} |
| Voie d'eau souterraine | Indisponible | ![Voie d'eau souterraine](images/underground_waterway.png) {width=200} |

### Lore

???+ "Lore"

    Selon la légende, le gouffre a été _formé par la chute d'une étoile il y a environ 6000 ans_.  

    Cette météorite avait un "tempérament orgueilleux et agité" et, pendant la guerre des Archontes, les conflits constants l'ont fait bondir dans les cieux, laissant derrière elle l'immense étendue qu'était le Gouffre.  

    Les ruines de Dunyu ont été créées par un fragment de cette même météorite.  
    On dit aussi que pendant le cataclysme qui a tué les lunes, le "char solaire" s'est écrasé ici, puis a été réparé et est retourné dans le ciel, mais il n'est pas certain que l'étoile tombée et le char solaire tombé soient les mêmes événements ou des événements distincts.  

    En raison de conditions géologiques particulières, peut-être dues à la météorite, le sol et la végétation de la région ont une couleur particulière, et des plantes fluorescentes uniques ont poussé à l'intérieur.  

    Les cristaux de sable glacé sont une spécialité de la région, utilisée dans la création de porcelaine.  

    > *Les légendes racontent également que c'est dans cette région que Morax et Azhdaha se sont affrontés en combat, et plus récemment, lors du cataclysme d'il y a 500 ans, un "météore de fer" qui a marqué le début de l'invasion des abysses*.

    Avant la création du Gouffre, la région semblait être celle d'un océan en raison des divers fossiles marins trouvés dans les mines. Selon le Khédive, il pense que ce qui a créé le Gouffre a été fait en un instant en raison de la parfaite conservation des fossiles.

    Pendant ou après la guerre des Archontes, le peuple de Liyue est venu dans la région pour exploiter les mines. Azhdaha, qui résidait à l'intérieur, a beaucoup souffert lorsque les humains ont perturbé les lignes de Ley à l'intérieur. Morax, Mountain Shaper, Moon Carver et un troisième adepte inconnu ont combattu l'être élémentaire fou, se frayant un chemin jusqu'aux plus profondes profondeurs du gouffre, ce qui a également entraîné la création de Dragonfall. Après la fermeture d'Azhdaha, les humains sont retournés dans le Gouffre pour exploiter les mines.

    À Liyue, le Gouffre et les Ruines de Dunyu sont deux endroits connus qui ont été fortement touchés par le cataclysme. Une " météorite de fer " a touché le gouffre et des monstres en sont sortis. En réponse, Morax a envoyé le Millelith pour défendre et évacuer les mineurs. Un yaksha sans nom - qui avait autrefois abandonné son devoir de défendre Liyue - est revenu se battre aux côtés des Millelith. Les yaksha et les Millelith qui ont choisi de mener l'avant-garde ont finalement péri dans la bataille. Plus précisément, on dit que le Yaksha a "succombé aux ténèbres", et le Fatui Anton nous dit que la bataille d'il y a 500 ans se déroule toujours sous nos pieds, donc il n'est pas certain que le Yaksha soit vraiment mort. On ignore ce qui s'est passé dans les Ruines de Dunyu, mais ses habitants ont décidé d'abandonner la ville et de se rendre au port de Liyue ; aucun des réfugiés ni des adeptes n'a parlé de ce qui s'est passé là-bas.

    Ces derniers temps, une mystérieuse série d'accidents a conduit à la fermeture complète du Gouffre malgré sa richesse. Avant l'expulsion générale de Liyue, les Fatui, avec l'aide de Signora, ont découvert que des suintements noirs avaient commencé à infester les mines. Les Fatui ont proposé de s'occuper de la suie mais ont échoué car leur dispositif a explosé et aucune autre nouvelle n'était attendue après que Childe ait lâché Osial sur Liyue. Finalement, les Liyue Qixing ont commencé à rouvrir lentement la mine, en limitant d'abord l'accès à la surface. Par conséquent, les collectionneurs de trésors ont commencé à se faufiler dans les mines pour s'enrichir.

    Il est révélé plus tard, lors de la descente du Voyageur dans le Gouffre, que les mystérieux tremblements de terre et le suintement noir étaient le résultat de la corruption par l'Ordre du Gouffre d'un clou similaire au clou de Skyfrost de Dragonspine.  
    _Après avoir purifié la corruption, les tremblements de terre se sont atténués et la brume noire a depuis commencé à se retirer_.  
    Cela a également permis aux Qixing de commencer à rouvrir la mine tout en s'assurant que les mineurs ne s'éloignent pas trop des zones les plus dangereuses.