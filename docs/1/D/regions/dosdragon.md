| Régions | Descriptions | Images |
|---------|-------------|---|
| Cité ensevelie - Palais antique | Une ancienne cité qui repose en silence sous la glace éternelle. Cherchez des indices et enquêtez sur le secret d'un royaume désormais détruit : Snow-Tombed Starsilver. | ![Cité ensevelie - Palais antique](images/dragonspine_entombed.png) {width=200} |
| Entombed City - Périphérie | La périphérie d'une ville ancienne. |![Entombed City - Périphérie](images/dragonspine_entembed_city.png) {width=200} |
| Clou de Skyfrost | Le Clou de Skyfrost est la zone du pic de Dosdragon. Débloquer l'accès à cette zone fait partie de la quête mondiale "Dans les montagnes". Si vous tentez d'accéder à cette zone en escaladant Dragonspine, Paimon vous dira qu'il fait bien trop froid avant que le joueur ne soit obligé de se téléporter. | ![Clou de Skyfrost](images/dragonspine_skyfrost_nail.png) {width=200} |
| Chemin enneigé | Le chemin qui mène à la montagne n'est pas aussi raide que prévu, mais il est recouvert de glace tout le long, alors il faut quand même faire attention ! Il fait si froid ici que même l'herbe ne brûle pas. On a l'impression qu'à chaque pas, nous cherchons la prochaine source de chaleur pour nous réchauffer, et si nous ne le faisons pas, nous ne ferons certainement pas long feu ici. | ![Chemin enneigé](images/dragonspine_snow_covered_path.png) {width=200} |
| Grotte de la lueur des étoiles | Une grande caverne qui s'étend dans les profondeurs de Dosdragon. Une végétation lumineuse particulière pousse dans toute la caverne, éclairant même les passages les plus profonds et les plus sombres de sa lueur glaciale. | ![Grotte de la lueur des étoiles](images/dragonspine_starglow_cavern.png) {width=200} |
| Vallée de Wyrmrest | Le lieu de repos final du dragon noir. La force vitale exsudant du noyau du dragon a teinté en rouge écarlate la neige argentée de la vallée escarpée. Il y a une grotte rougeoyante intéressante. | ![Vallée de Wyrmrest](images/dragonspine_wyrmrest_valley.png) {width=200} |

### Lore

???+ "Lore"

    Voici Dragonspine, une énorme montagne qui abrite les restes du dragon venimeux Durin, tombé ici après la bataille contre Dvalin. Peu d'aventuriers osent tenter de conquérir cet environnement impitoyable marqué par la neige profonde, le froid intense, le sang empoisonné et les monstres. Mais cela ne dissuadera pas le voyageur des étoiles de se rendre ici....

    _Albedo soupçonne l'épée Festering Desire d'être liée au dragon Durin, et de tirer son pouvoir du venin du dragon_.

    >Avant Durin

    >Bien avant la chute de Durin, il existait sur la montagne une civilisation appelée Sal Vindagnyr. La plupart de ses vestiges archéologiques sont concentrés dans la Cité ensevelie - Ancien Palais. Il y a également des documents contenus dans les sculptures anciennes, dans les descriptions de l'arme Snow-Tombed Starsilver, ainsi que du catalyseur Frostbearer, de la boîte de la princesse, de la boîte du prêtre et de la boîte du scribe, ainsi que des fresques dans la salle murale où se trouve le Snow-Tombed Starsilver.

    Ensemble, ils racontent l'histoire d'un royaume, d'une princesse douée du pouvoir de prédire par le biais de visions et de la peinture, et de la tragédie ou de la chute de ce royaume. La chute est provoquée par le clou de Skyfrost qui tombe et détruit l'arbre de la ligne ley d'argent sur la montagne - ce qui provoque le changement de climat de verdoyant à gelé. Les artefacts trouvés à Dragonspine racontent l'histoire de la princesse et du peuple de cette époque, ainsi que l'histoire d'un étranger à qui l'on a confié l'arme d'argent des étoiles - lorsque l'étranger revient de sa quête, il trouve la ville morte et abandonnée, et laisse l'arme dans la chambre secrète.

    Les messages décodés _("Record of Serial No.")_ trouvés sur des gardes de ruine brisés dans la région peuvent également être liés, mais aussi à la **chute de Khaenri'ah**, d'où proviennent les gardes de ruine.

### Infos

???+ "Infos"

    Une grande partie de l'histoire, de la culture et de l'histoire de Dosdragon a été délibérément laissée ambiguë pour permettre aux joueurs de développer leurs propres interprétations de ce qui s'est passé. Dans _"Snow-Covered Path" - Behind the Scenes of Dragonspine (3:50 - 4:37)_, le concepteur de niveaux en monde ouvert Xiao Ao déclare : 
    >"Pour enrichir l'expérience de l'histoire de Dosdragon, nous prenions les thèmes centraux de Dosdragon - à savoir le Clou de Skyfrost et la civilisation révolue - et les reprenions sous de nombreux angles. Au final, le joueur développe sa propre interprétation de ce qui s'est passé."  

    _La région de Dosdragon est largement inspirée des Alpes suisses_.