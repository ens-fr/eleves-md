
!!! info inline end "Identité"
    Kokomi est la prêtresse divine et l'autorité suprême en poste sur l'Île de Watatsumi.
    Elle est une excellente stratège expérimentée dans l'art de guerre qui possède un superbe sens des affaires militaires.
    Elle est également douée pour la gestion des affaires internes, la diplomatie et dans bien d'autres domaines.
    Cependant, cette mystérieuse dirigeante possède une face cachée... 

![Kokomi](https://images2.alphacoders.com/117/thumbbig-1172302.webp)


## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de lazurite varunada x1 - Perle de corail x3 - Coquille spectrale x3 |
| 40 | - Moras 40 000 - Fragment de lazurite varunada x3  - Rosée du rejet x2 - Perle de corail x10 - Coquille spectrale x15 |
| 50 | - Moras 60 000 - Fragment de lazurite varunada x6  - Rosée du rejet x4 - Perle de corail x20 - Cœur spectral x12 |
| 60 | - Moras 80 000 - Morceau de lazurite varunada x3  - Rosée du rejet x8 - Perle de corail x30 - Cœur spectral x18 |
| 70 | - Moras 100 000 - Morceau de lazurite varunada x6 - Rosée du rejet x12 - Perle de corail x45 - Noyau spectral x12 |
| 80 | - Moras 120 000 - Pierre de lazurite varunada x6 - Rosée du rejet x20 - Perle de corail x60 - Noyau spectral x24 |

## Aptitudes
---

|![aptitude 1](imagees/aptitude/aptitude_1_kokomi.png) Forme de l'eau | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 3 attaques chargées qui prennent la forme de poissons pour infliger des DGT Hydro. |
| Attaque chargée : | Consomme de l'endurance pour infliger des DGT Hydro de zone après un court délai. |
| Attaque descendante : | Plonge depuis les airs en concentrant ses pouvoirs Hydro, infligeant des DGT aux ennemis en chemin et des DGT Hydro de zone à l'impact. |

|![aptitude 2](imagees/aptitude/aptitude_2_kokomi.png) Serment de Kurage | Aptitude de combat |
|---------|-------------|
| | Kokomi cré une méduse fantôme à partir d'eau pour soigner ses alliès. Utiliser cette compétence applique l'état à Kokomi. |
| Méduse fantôme | Inflige des DGT Hydro aux ennemis proches et restaure les PV des personnages déployés à proximité à intervalles réguliers. Les soins sont fonction des PV max de Kokomi. Kokomi a besoin de récupérer des forces réfulièrement pour maintenir son efficacité de travail. |

|![aptitude 3](imagees/aptitude/aptitude_3_kokomi.png) Ascension de la néréide | Aptitude de combat |
|---------|-------------|
| | La puissance de Watatsumi se manifeste, infligeant des DGT Hydro aux ennemis proches, avant d'entourer Kokomi d'une parure cérémoniale faite des eaux vives de Sangonomiya. |
| Parure cérémoniale | *Les DGT des attaques normales et chargées de Kokomi et ceux de la méduse fatôme créée par sa compètence élémentaire augmentent en fonction des PV max de Kokomi ; Lorsque ses attaques normales et chargées touchent des ennemis, Kokomi restaure les PV de tous les personnages de l'équipe à proximité, la quantité de PV restaurés dépendant des PV max de Kokomi ; Augmente la RÉS à l'interruption de Kokomi, et lui permet de se déplacer à la surface de l'eau. Ces effets prennent fin lorsque Kokomi quitte le champ de bataille. Une lumiére claire se fond dans les robes autour de Son Altesse Sangonomiya Kokomi, reflétant sa forme magnifique. |

|![aptitude 4](imagees/aptitude/aptitude_4_kokomi.png) Coffret de Tamanooya | Aptitude passive |
|---------|-------------|
| | Si la méduse fantôme de Kokomi est sur le champ de bataille lorsqu'elle utilise Ascension de la néréide, sa durée est réinitialisée. |

|![aptitude 5](imagees/aptitude/aptitude_5_kokomi.png) Chant de nacre | Aptitude passive |
|---------|-------------|
| | Lorsque Kokomi revêt la prure cérémoniale créée par Ascension de la néréide, les bonus de DGT d'attaque normale et chargée en fonction de ses PV max bénéficient d'une augmentation supplémentaire basée sur 15 % de son bonus de soins. |

|![aptitude 6](imagees/aptitude/aptitude_6_kokomi.png) Princesse de Watatsumi | Aptitude passive |
|---------|-------------|
| | Réduit la consommation d'endurance de vos personnages dans l'équipe de 20 % lors de la nage. Ne peut pas être cumulé avec d'autres aptitudes passives aux effets identiques. |

|![aptitude 7](imagees/aptitude/aptitude_7_kokomi.png) Stratégie sans faille | Aptitude passive |
|---------|-------------|
| | Kokomi obtient un bonus de soins de 25 %, mais son taux CRIT diminue de 100 %. |

## Constellations
---

|![constellation 1](imagees/constel/constelation_1_kokomi.png) Au bord de l'eau | Constellation Niv.1 |
|---------|-------------|
| | Lorsque Kokomi revêt la parur cérémoniale créée par Ascension de la néréide, la dernière attaque normale de son combo libère un poisson qui inflige des DGT Hydro équivalant à 30 % de ses PV max. Ces DGT ne sont pas considéré comme des DGT d'attaque normale |

|![constellation 2](imagees/constel/constelation_2_kokomi.png) Nuages comme des vagues ondulantes | Constellation Niv.2 |
|---------|-------------|
| | Lorsque Kokomi soigne des personnages dont les PV sont inférieurs ou égaux à 50 %, elle bénéficie d'un bonus de soin en fonction de la méthode utilisée : Méduse fantôme de Serment de Kurage : augmentation équivalant à 4,5 % des PV de Kokomi. Attaque normale ou chargée sous l'effet d'Ascension de la néréide : augmentation équivalant à 0,6 % des PV max de Kokomi. |

|![constellation 3](imagees/constel/constelation_3_kokomi.png) La lune, navire des mers | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Ascension de la néréide +3. |
| | Niveau max : 15 |

|![constellation 4](imagees/constel/constelation_4_kokomi.png) La lune surplombe les eaux | Constellation Niv.4 |
|---------|-------------|
| | Lorsque Kokomi revêt la prure cérémoniale créée par Ascension de la néréide, la VIT d'attaque de ses attaques normale augmente de 10 %, et toute attaque normale touchant un ennemi lui permet de restaurer son énergie élémentaire de 0,8 pt. Cet effet peut être déclenché une fois toutes les 0,2 s. |

|![constellation 5](imagees/constel/constelation_5_kokomi.png) Tous les ruisseaux coulent vers la mer | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Serment de Kurage +3. |
| | Niveau max : 15 |

|![constellation 6](imagees/constel/constelation_6_kokomi.png) Sango Isshin | Constellation Niv.6 |
|---------|-------------|
| | Lorsque kokomi revet la parure cérémoniale créée par Ascension de la néréide, elle bénéficie d'un bonus de DGT Hydro de 40 % pendant 4 s après avoir effectueé un soin sur un personnage dont les PV sont supérieurs à 80 % grâce à son attaque normale ou chargée |