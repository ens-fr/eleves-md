
!!! info inline end "Identité"
    Deuxième fils d'une riche famille à la tête de la Guilde des Marchands de Feiyun, l'aimable Xingqiu fait dès son plus jeune âge montre d'une assiduité prononcée dans les études.
    Mais ne vous y trompez pas : Xingqiu a aussi un côté beaucoup plus téméraire.

![Xingqiu](https://images.alphacoders.com/118/thumbbig-1181520.webp)

## Histoire
---

=== "Histoire 1"

    Pour Xingqiu, un « moment de solitude » peut signifier toutes sortes de choses.
    Parfois, cela peut vouloir dire visiter la Librairie Wanwen pour y parcourir les derniers romans disponibles, ou passer un moment au Salon de thé Heyu à goûter les dernières boissons proposées par Yunjin.
    D’autres fois, cela signifie qu’il est parti « combattre pour la justice »…
    Vaincre les bandits de grand chemin, chasser les monstres, ou aider quelque gamin des alentours à récupérer son cerf-volant coincé dans un arbre : autant de responsabilités qui entrent dans cette catégorie.
    Inspiré par ses lectures – par exemple l’histoire classique du roi se grimant pour se mêler au peuple -, Xingqiu n’hésite pas à mettre à profit la réputation de la Guilde des marchands de Feiyun pour venir à bout des problèmes que la force ne peut résoudre.

=== "Histoire 2"

    Le frère de Xingqiu vint un jour le chercher, mais sa chambre était déserte. À son retour, Xingqiu tomba sur ce dernier dans le couloir.
    « Ta chambre était dans un tel état, je t’ai aidé à la ranger. Écoute, tu es comme moi responsable de la réputation de la Guilde des marchands de Feiyun ; tu devrais être source d’inspiration. Si tu n’es pas capable de nettoyer ta chambre, comment veux-tu un jour nettoyer le monde ? Comme l’a dit autrefois un Adepte… »
    Le sermon dura plus d’une demi-heure avant que Xingqiu puisse retourner à ses affaires ; mais son frère termina d’une étrange manière :
    « Je ne pénètrerai plus dans ta chambre sans autorisation, et je m’assurera que le personnel fasse de même. »
    Ne comprenant pas ce qu’il voulait dire, Xingqiu resta sans réponse. Mais en s’éloignant, son frère maugréa pour lui-même :
    « Tous ces livres sous son lit… Je préfère ne pas savoir. La puberté, sans doute… »
    Xingqiu ne compris pas vraiment pourquoi par la suite personne ne lui posa de questions sur les romans d’arts martiaux cachés sous son lit (pour lesquels il avait depuis longtemps préparé une réponse toute prête), pas plus qu’il ne comprit pourquoi plus personne ne mit les pieds dans sa chambre.

=== "Histoire 3"

    ...


## Elevation
---

| Niveau |A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de lazurite varunada x1 - Fleur de soie x3 - Masque endommagé x3 |
| 40 | - Moras 40 000 - Fragment de lazurite varunada x3 - Cœur d'eau pure x2 - Fleur de soie x10 - Masque endommagé x15 |
| 50 | - Moras 60 000 - Fragment de lazurite varunada x6 - Cœur d'eau pure x4 - Fleur de soie x20 - Masque sale x12 |
| 60 | - Moras 80 000 - Morceau de lazurite varunada x3 - Cœur d'eau pure x8 - Fleur de soie x30 - Masque sale x18 |
| 70 | - Moras 100 000 - Morceau de lazurite varunada x6 - Cœur d'eau pure x12 - Fleur de soie x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre de lazurite varunada x6 - Cœur d'eau pure x20 - Fleur de soie x60 - Masque sinistre x24 |

## Aptitudes
---

|![aptitude 1](imagees/aptitude/aptitude_1_xingqiu.png) Style Guhua | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Consomme de l'endurance pour déclencher deux coups d'épée rapides. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imagees/aptitude/aptitude_2_xingqiu.png) Épée Guhua : Pluie battante | Aptitude de combat |
|---------|-------------|
| | Lance une succession de 2 coups d'épée qui infligent des DGT Hydro aux ennemis en face. Dans le même temps, le personnage déployé s'entoure du nombre maximum d'épées de pluie. Les épées de pluie ont les propriétés suivantes : Les épées se brisent lorsque Xingqiu subit des DGT et ce faisant réduisent les DGT subis. Les actions de Xingqiu sont plus difficiles à interrompre. 20% du bonus de DGT Hydro de Xingqiu est Jusqu'à 24% de réduction de DGT supplémentaire peut être obtenue de cette manière. Xingqiu commence avec 3 épées de pluie maximum. Applique l'état Trempé à Xingqiu lors du lancement. « Le dragon est facile à vaincre, mais la goutte de pluie est insaisissable. » |

|![aptitude 3](imagees/aptitude/aptitude_3_xingqiu.png) Épée Guhua : Pluie et arc-en-ciel | Aptitude de combat |
|---------|-------------|
| | Xingqiu fait appel à un déluge d'épées et combat avec une lame arc-en-ciel ; il s'entoure de plus du nombre maximum d'épées de pluie. |
| Lame arc-en-ciel | Les attaques normales infligées par votre personnage déployé libèrent un déluge La Voie des Lames de Pluie est l'un des trois secrets jalousement gardés par le Clan Guhua. |

|![aptitude 4](imagees/aptitude/aptitude_4_xingqiu.png) Clé d'invocation d'eau | Aptitude passive |
|---------|-------------|
| | Lorsque l'épée de rideau pluvial est brisée par des attaques ou se termine, elle restaure les PV du personnage déployé basés sur 6% des PV max de Xingqiu. |

|![aptitude 5](imagees/aptitude/aptitude_5_xingqiu.png) Lames et gouttes de pluie | Aptitude passive |
|---------|-------------|
| | Confère un bonus de DGT Hydro de 20% à Xingqiu. |

|![aptitude 6](imagees/aptitude/aptitude_6_xingqiu.png) Illumination | Aptitude passive |
|---------|-------------|
| | Vous avez 25% de chance de récupérer des matériaux de synthèse lors de la synthèse de matériaux d'aptitude. |

## Constellations
---

|![constellation 1](imagees/constel/constelation_1_xingqiu.png) Le parfum est demeuré | Constellation Niv.1 |
|---------|-------------|
| | Augmente le nombre max d'épées de pluie de 1. |

|![constellation 2](imagees/constel/constelation_2_xingqiu.png) Arc-en-ciel sur le ciel d'azur | Constellation Niv.2 |
|---------|-------------|
| | Prolonge la durée d'Épée Guhua : Pluie et arc-en-ciel de 3 s. De plus, les épées de pluie réduisent la RÉS Hydro des ennemis qu'elles touchent de 15% pendant 4 s. |

|![constellation 3](imagees/constel/constelation_3_xingqiu.png) Tisseuse de vers | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Épée Guhua : Pluie et arc-en-ciel +3. |
| | Niveau max : 15 |

|![constellation 4](imagees/constel/constelation_4_xingqiu.png) Conquérant du dragon | Constellation Niv.4 |
|---------|-------------|
| | Épée Guhua : Pluie et arc-en-ciel inflige 50% de DGT supplémentaires lorsque Épée Guhua : Pluie battante est active. |

|![constellation 5](imagees/constel/constelation_5_xingqiu.png) Embrassement de pluie | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Épée Guhua : Pluie battante +3. |
| | Niveau max : 15 |

|![constellation 6](imagees/constel/constelation_6_xingqiu.png) Rassemblement d'articles | Constellation Niv.6 |
|---------|-------------|
| | Les DGT du déluge d'épées d'Épée Guhua : Pluie et arc-en-ciel augmentent considérablement tous les 2 pluies d'épées ; Xingqiu récupère de plus 3 pts d'énergie élémentaire. |