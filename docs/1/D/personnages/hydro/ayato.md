!!! info inline end "Identité"
    Kamisato Ayato (Japonais : 神里綾人) est un personnage Hydro dans Genshin Impact.

    L'actuel chef du Clan Kamisato et, de ce fait, de la Commission culturelle. Il atteint toujours ses objectifs de manière réfléchie. Toutefois, très peu de gens connaissent son véritable « objectif ».


![Ayato](https://images4.alphacoders.com/122/thumbbig-1221820.webp)

## Histoire
---

A venir

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de lazurite varunada x1 - Fleur de cerisier x3 - Garde-main ancien x3 |
| 40 | - Moras 40 000 - Fragment de lazurite varunada x3 - Rosée du rejet x2 - Fleur de cerisier x10 - Garde-main ancien x15 |
| 50 | - Moras 60 000 - Fragment de lazurite varunada x6 - Rosée du rejet x4 - Fleur de cerisier x20 - Garde-main kageuchi x12 |
| 60 | - Moras 80 000 - Morceau de lazurite varunada x3 - Rosée du rejet x8 - Fleur de cerisier x30 - Garde-main kageuchi x18 |
| 70 | - Moras 100 000 - Morceau de lazurite varunada x6 - Rosée du rejet x12 - Fleur de cerisier x45 - Garde-main célèbre x12 |
| 80 | - Moras 120 000 - Pierre de lazurite varunada x6 - Rosée du rejet x20 - Fleur de cerisier x60 - Garde-main célèbre x24 |

## Aptitudes
---

A venir

## Constellations
---

A venir