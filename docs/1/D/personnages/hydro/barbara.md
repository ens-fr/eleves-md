
!!! info inline end "Identité"
    À la fois diaconesse de l'Église de Favonius et star de la chanson à Mondstadt, Barbara est une jeune fille à la vie bien remplie.
    Pour les habitants de Mondstadt qui sont habitués aux bardes, le concept d'idole qu'elle incarne est encore nouveau. Cependant, le moins qu'on puisse dire, c'est que Barbara est adulée par tout le monde dans le coin.

![Barbara](https://imagees5.alphacoders.com/111/thumbbig-1114238.webp)

## Histoire 
---

=== "Histoire 1"

    Il n'existe pas un habitant de Mondstadt qui n'apprécie pas Barbara.
    Les choses n'en furent pas toujours ainsi. À ses débuts, ses chansons étaient quelque peu étranges aux oreilles des Mondstadtois habitués seulement aux ballades populaires.
    Mais restant fidèle à sa réputation de Cité de la Liberté, Mondstadt et ses habitants finirent par joindre une nouvelle tradition aux anciennes.
    Les gens acceptèrent ses chansons, qui détinrent sur eux : bientôt ils furent nombreux à fredonner ses airs dans la ville.
    "Sieur Albert, par pitié, arrêtez-vous ! Vous chantez complètement faux !"

=== "Histoire 2"

    Barbara éprouvent des sentiments compliqués face à son succès. L'idole se doit d'être aimée de tous, et de ce point de vue on peut dire qu'elle a rencontré un franc succès.
    Aucun doute qu'elle a fait le bon choix.
    Cependant, une idole se doit aussi de répandre la joie et d'alléger la fatigue du peuple... Avait-elle réussi cette partie ? Elle a chanté pour Gloria l'aveugle, l'encourageant à garder espoir qu'un jour son amant reviendrait ; elle a aussi chanté pour Anna, lui promettant que sa maladie serait un jour guérie.
    Cependant, leurs sourires ne restent jamais longtemps sur leurs lèvres une fois les chansons terminées ; Barbara a fini par devenir quelque peu désabusée.

=== "Histoire 3"

    Depuis sa plus jeune enfance, Barbara affiche un caractère ouvert et optimiste. De nature maladroite, elle rate souvent ce qu'elle entreprend, mais ne s'avoue jamais vaincue.
    Sa sœur a toujours été, par opposition, la définition même de "succès".
    La surpasser ne serait-ce qu'une seule fois était le rêve de la jeune Barbara ?
    Mais rien n'y fit ; maîtrise de l'épée, notes à l'école, sa sœur l'emportait dans tous les domaines.
    Barbara en conçut quelque déception, bien qu'étant d'un naturel optimiste.
    "Le travail est la meilleure des magies ; mais si ça ne marchait pas tout le temps ?"

=== "Histoire 4"

    Barbara n'abandonna jamais.
    Même son père, le cardinal Seamus, fut étonné par sa détermination et sa résilience. Lorsque Barbara subit les aléas des attaques de la vie, elle ne s'autorise que trente secondes de laissez-aller, avant de se forcer à retrouver un état d'esprit positif.
    "Puisque je ne sais pas manier l'épée, alors je supporterai ceux qui le peuvent !"
    Sous les conseils de son père, la jeune fille devint guérisseuse.
    Ses bonnes dispositions sont toujours appréciées des blessés même les plus graves.
    Et ainsi, son désir de reconnaissance se mua progressivement en un désir d'aider les autres.

=== "Histoire 5"

    "Merci." Il s'agit de là du mot le plus souvent prononcé à l'encontre de Barbara.
    Quand elle se sent d'humeur maussade, il y a toujours une main qui cherche à prendre la sienne.
    "Merci d'être à mes côtés, je me sens déjà mieux."
    À ses yeux, il n'est plus grande récompense que de voir les sourires revenir sur les visages des gens.
    C'est ainsi que lorsque, fatiguée par sa journée, elle se masse ses chevilles enflées, ou qu'elle boit un thé bien chaud pour apaiser sa gorge, Barbara se remémore tous ceux qui ont fait preuve à son égard de générosité.
    "C'est grâce aux encouragements de tous que je continue !"
    Peut-être que les sourires qu'elle reçoit sont la preuve que ses chansons sont capables elles aussi de soigner les maux ?
    Son ambition de dépasser sa sœur et de devenir la personne la plus populaire de Mondstadt n'a cependant point disparu ; elle est juste enfouie profond en son cœur.
    "Si je m'améliore, peut-être pourrai-je un jour être de quelque utilité à Jean."
    Voilà en quoi la guérisseuse croit.
    "D'accord, Barbara... Vas-y !"

## Elevation
---

| Niveau |A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de lazurite varunada x1 - Champignon anémophile x3 - Parchemin divinatoire x3 |
| 40 | - Moras 40 000 - Fragment de lazurite varunada x3 - Cœur d'eau pure x2 - Champignon anémophile x10 - Parchemin divinatoire x15 |
| 50 | - Moras 60 000 - Fragment de lazurite varunada x6 - Cœur d'eau pure x4 - Champignon anémophile x20 - Parchemin sigillé x12 |
| 60 | - Moras 80 000 - Morceau de lazurite varunada x3 - Cœur d'eau pure x8 - Champignon anémophile x30 - Parchemin sigillé x18 |
| 70 | - Moras 100 000 - Morceau de lazurite varunada x6 - Cœur d'eau pure x12 - Champignon anémophile x45 - Parchemin maudit x12 |
| 80 | - Moras 120 000 - Pierre de lazurite varunada x6 - Cœur d'eau pure x20 - Champignon anémophile x60 - Parchemin maudit x24 |

## Aptitudes
---

|![aptitude 1](imagees/aptitude/aptitude_1_barbara.png) Murmure de l'eau | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 attaques chargées d'eau qui infligent des DGT Hydro. |
| Attaque chargée : | Consomme de l'endurance pour infliger des DGT Hydro de zone après un court délai. |
| Attaque descendante : | Plonge depuis les airs en concentrant ses pouvoirs Hydro, infligeant des DGT aux ennemis en chemin et des DGT Hydro de zone à l'impact. |

|![aptitude 2](imagees/aptitude/aptitude_1_barbara.png) C'est parti pour le show ♪ | Aptitude de combat |
|---------|-------------|
| Invoque des goutelettes dansantes qui forment un cercle de mélodie infligeant des DGT Hydro et trempant les ennemis proches. |
| Cercle de mélodie | Les attaques normales de Barbara restaurent les PV de vos personnages dans l'équipe ainsi que de tous les alliés proches lorsqu'elles touchent. La quantité de PV restaurés est proportionnelle aux PV max de Barbara. L'effet des soins est multiplié par 4 avec les attaques chargées. Régénère les PV de votre personnage déployé sur la durée. Barbara trempe les personnages et les ennemis avec qui elle entre en contact. La musique de Barbara est une magie capable de soigner chacun. |

|![aptitude 3](imagees/aptitude/aptitude_3_barbara.png) Miracle brillant ♪ | Aptitude de combat |
|---------|-------------|
| | Restaure considérablement les PV des alliés et de vos personnages dans l'équipe ; la quantité de PV restaurés est proportionnelle aux PV max de Barbara. Soigner le cœur de chacun, telle est la force du chant de Barbara. |

|![aptitude 4](imagees/aptitude/aptitude_4_barbara.png) Splendide saison | Aptitude passive |
|---------|-------------|
| | Les personnages situés dans le cercle de mélodie de C'est parti pour le show ♪ consomment 12 % d'énergie élémentaire en moins. |

|![aptitude 5](imagees/aptitude/aptitude_5_barbara.png) Bis repetita | Aptitude passive |
|---------|-------------|
| | Chaque orbe ou particule élémentaire obtenu(e) par votre personnage déployé prolonge le cercle de mélodie de C'est parti pour le show ♪ de 1 s. Durée max : 5 s. |

|![aptitude 6](imagees/aptitude/aptitude_6_barbara.png) De tout mon cœur ♪ | Aptitude passive |
|---------|-------------|
| | 12 % de chance d'obtenir un plat supplémentaire lorsque vous cuisinez un plat de soins parfait. |

## Constellations
---

|![constellation 1](imagees/constel/constelation_1_barbara.png) Joyeuse ballade | Constellation Niv.1 |
|---------|-------------|
| | Barbara récupère 1 pt d'énergie élémentaire toutes les 10 s. |

|![constellation 2](imagees/constel/constelation_2_barbara.png) Éclat de vitalité | Constellation Niv.2 |
|---------|-------------|
| | Réduit le TdR de C'est parti pour le show ♪ de 15 %. Confère un bonus de 15 % DGT Hydro à votre personnage déployé tant que la compétence est active. |

|![constellation 3](imagees/constel/constelation_3_barbara.png) Étoile du lendemain | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Miracle brillant ♪ +3. |
| | Niveau max : 15 |

|![constellation 4](imagees/constel/constelation_4_barbara.png) L'effort est la meilleure des magies | Constellation Niv.4 |
|---------|-------------|
| | À chaque ennemi touché par une attaque chargée, Barbara récupère 1 pt d'énergie élémentaire. 5 pts d'énergie maximum peuvent être régénérés après chaque attaque chargée. |

|![constellation 5](imagees/constel/constelation_5_barbara.png) Lien innocent | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence C'est parti pour le show ♪ +3. |
| | Niveau max : 15 |

|![constellation 6](imagees/constel/constelation_6_barbara.png) Toujours le meilleur pour vous | Constellation Niv.6 |
|---------|-------------|
| | Si Barbara est en réserve, et qu'un de vos personnages dans l'équipe est vaincu : Barbara le réanime immédiatement. Les PV de ce personnage sont restaurés à 100 %. Cet effet peut être déclenché une fois toutes les 15 min. |