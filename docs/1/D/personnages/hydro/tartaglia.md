!!! info inline end "Identité"  
    Tartaglia, également connu sous son alias "Jeune Sire" (Childe en anglais), est un personnage jouable Hydro dans Genshin Impact.

    Tartaglia est un combattant imprévisible venant des terres enneigées de Snezhnaya.
    Rien ne sert d'essayer de deviner ses intentions ou ses objectifs. Mieux vaut juste se souvenir d'une chose : sous un aspect parfois encore puéril se cache la volonté indomptable d'un guerrier. 


![Tartaglia](https://images8.alphacoders.com/117/thumbbig-1170451.webp)

## Histoire
---

=== "Histoire 1"

    Plus jeune Exécuteur de l'histoire des Fatui, Tartaglia est un électron libre, que rien ni personne ne restreint.

    Cette indépendance n'est pas exactement appréciée au sein de l'organisation, et le style de Tartaglia s'accorde mal à celui de ses collègues Exécuteurs. Mais lorsqu'il travaille seul, Tartaglia fait montre d'une prudence à toute épreuve et d'un fort sens des responsabilités.

    Tartaglia est un homme fier, et tient pour cette raison toujours sa parole - peu importe la promesse faite, hors de question d'y revenir.

    Chevaucher seul pour détruire un antre de vouivres, pénétrer dans quelque donjon particulièrement dangereux pour en ressortir indemne, et même en une occasion renverser seul un régime aristocratique...

    Tartaglia est fidèle à la parole donnée, et le fait en plus avec élégance.

    En tant qu'avant-garde des Exécuteurs, Tartaglia apparait souvent là où les ennemis de Snezhnaya montrent quelque faiblesse, prêt à la mettre à profit et à porter le premier coup avant que les choses ne dégénèrent complètement.

=== "Histoire 2"

    On raconte à Snezhnaya que le jeune Tartaglia connut le champ de bataille dès l'âge de quatorze ans.

    Fait étrange, Tartaglia semble avoir depuis toujours eu une connaissance innée des différentes façons d'ôter la vie à ses adversaires.

    Plus effrayante encore est sa passion inégalée pour le combat. Les batailles s'annonçant particulièrement ardues l'attirent plus qu'autre chose, et les ennemis les plus terrifiants ne sont à ses yeux qu'un attrait de plus.

    L'attitude cavalière et confiante de Tartaglia est le résultat de milliers d'affrontement et d'une expérience en combat que peu peuvent se vanter de posséder. Mais cet excès de confiance en soi et cette soif de combat ont souvent pour conséquence d'amener davantages d'ennuis que nécessaire.

    C'est d'ailleurs pour cette raison que les autres Exécuteurs des Fatui font tout pour que les missions qu'on lui confie soient aussi loin de Snezhnaya que possible.

    D'une manière incompréhensible aux yeux de beaucoup, Tartaglia semble être en permanence au cœur de la tempête. Son expérience aussi impressionnante qu'inhabituelle a fait de lui quelqu'un aimant faire montre de ses talents, et appréciant qu'on l'en félicite.

    Contrairement aux autres Exécuteurs qui préfèrent garder profil bas, Tartaglia aime à assister à des représentations publiques - et n'hésite pas à prendre à l'occasion le devant de la scène.

=== "Histoire 3"

    La pêche sur glace est l'un des passe-temps de Tartaglia depuis son enfance.

    A cette époque, il n'était connu ni sous le nom de Tartaglia, ni de « Jeune Sire », comme l'appellent les Fatui, mais sous le nom d'Ajax, nommé par son père d'après l'histoire du héros. Lui et son père creusaient des trous dans des lacs gelés, avant de s'asseoir et de s'occuper de leurs lignes de pêche.

    Ce n'était pas une tâche facile, et cela prenait parfois toute la matinée.

    Mais qu'il s'agisse de ciseler la glace épaisse ou les longues attentes entre les prises, il était toujours accompagné des interminables récits d'aventures de son père.

    C'était ses aventures de jeunesse et elles ne tardèrent pas à devenir le futur auquel Tartaglia aspirait secrètement pour lui-même.

    C'est pourquoi, il y prêtait une attention particulière, se mettait dans la peau du protagoniste, s'y plongeait pleinement alors qu'ils attendaient qu'un poisson morde. Même après avoir quitté le foyer familial, Ajax, plus tard connu sous le nom de « Jeune Sire » Tartaglia, garderait ce passe-temps.

    Mais ce n'était plus accompagné des histoires d'autrefois, mais plutôt l'occasion de s'entrainer, de perfectionner son endurance et de réfléchir aux techniques de combat.

    Après de telles périodes de méditation, il ne se souciait plus guère s'il avait attrapé un poisson ou non.

=== "Histoire 4"

    Contrairement à ce que tout le monde pense, Tartaglia n'est pas né avec une épée à la main.

    Mais il ne parle jamais à personne de cette rencontre critique et fatidique.

    A l'âge de 14 ans, il décida de fuir la monotonie de sa vie et quitta sa maison avec rien d'autre qu'une épée courte et un sac de pain à la main.

    Le pauvre garçon finit par se perdre dans la forêt. Alors qu'il essayait d'échapper à une meute de loups et à un ours géant, il trébucha et tomba dans une faille sans fond.

    C'est là qu'il fut témoin des possibilités infinies d'un monde ancien. Il y rencontra un mystérieux épéiste. Ou plutôt un épéiste de ce monde des ténèbres fut attiré par l'ambition brûlante du cour du jeune garçon.

    Des ténèbres dans lesquelles le Jeune Sire ne devait plus jamais plonger.

    Durant trois mois, l'épéiste appris à Tartaglia à se déplacer dans les Abysses et, plus important encore, il attisa sa capacité à déclencher des conflits sans fin en utilisant sa prédisposition au chaos. Personne ne sait ce qui s'est réellement passé et Ajax n'en parle jamais à personne.

    Lorsque sa mère et ses sœurs inquiètes le trouvèrent finalement dans la forêt, seulement 3 jours s'étaient écoulés dans ce monde. C'est ainsi que la première aventure d'Ajax se termina, avec son épée courte maintenant rouillée à la main.

    Cela marquerait la fin de sa jeunesse et le début de ses jours de guerrier.

=== "Histoire 5"

    Quand il rentra chez lui, le jeune homme n'était plus le même.

    Il n'était plus effrayé ou indécis, mais était devenu frivole et confiant. Il agissait comme si le monde tournait autour de lui et comme si les guerres existaient par et pour lui.

    Les conflits amènent souvent des changements et ce changement fantasque et imprévisible attirait Ajax comme un kaléidoscope hypnotique.

    Aux yeux de son père, ce troisième fils dont il était si inquiet avait changé pour le pire, causant beaucoup de ravages injustifiés dans leur paisible ville balnéaire. Ou peut-être serait-il plus juste de dire qu'Ajax était devenu une personne gênante, car partout où il allait, il y avait des combats et des disputes dont il se réjouissait.

    Jusqu'au jour où, après qu'une bagarre énorme eut été pacifiée non sans mal en évitant de justesse des victimes, son père n'eut d'autre choix que de remettre son fils bien-aimé aux mains des Fatui.

    Il espérait que l'entrainement militaire strict des Fatui aiguise son tempérament, mais Ajax finit par vaincre tous les soldats et les faire fuir.

    Ce fut une grande déception pour son père, mais cela attira également l'attention de Pulcinella, le 5ème des Onze Exécuteurs. Impressionné par la force d'Ajax et intrigué par la façon dont il devenait invariablement le centre des disputes, Pulcinella plaça Ajax chez les Fatui sous prétexte de « lui infliger une punition », le forçant à commencer au bas de l'échelle en assumant le devoir de servir l'Archon Cryo.

    Sa soif de victoire était rassasiée en combattant avec les Fatui et son égo toujours en plein essor se gorgeait de l'exaltation de vaincre des ennemis puissants. Au final, Ajax fut choisi pour faire partie des Onze des Fatui sous le nom de « Jeune Sire » Tartaglia et devenir l'une des personnes les plus puissantes de Snezhnaya.

    Mais devenir Tartaglia n'était pas son but ultime. Pour quelqu'un qui cherchait à conquérir le monde, ce n'était qu'un petit pas dans une longue marche.

## Elevation
---

| Niveau |A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de lazurite varunada x1 - Conque d'étoile x3 - Insigne de nouvelle recrue x3 |
| 40 | -  Moras 40 000 - Fragment de lazurite varunada x3 - Cœur d'eau pure x2 - Conque d'étoile x10 - Insigne de nouvelle recrue x15 |
| 50 | -  Moras 60 000 - Fragment de lazurite varunada x6 - Cœur d'eau pure x4 - Conque d'étoile x20 - Insigne de sergent x12 |
| 60 | -  Moras 80 000 - Morceau de lazurite varunada x3 - Cœur d'eau pure x8 - Conque d'étoile x30 - Insigne de sergent x18 |
| 70 | -  Moras 100 000 - Morceau de lazurite varunada x6 - Cœur d'eau pure x12 - Conque d'étoile x45 - Insigne d'officier x12 |
| 80 | -  Moras 120 000 - Pierre de lazurite varunada x6 - Cœur d'eau pure x20 - Conque d'étoile x60 - Insigne d'officier x24 |

## Aptitudes
---

|![aptitude 1](imagees/aptitude/aptitude_1_tartaglia.png) Taille du torrent | Aptitude de combat |
|---------|-------------|
| Contre-courant | Les ennemis affectés par cet effet subissent l'un des types de DGT Hydro de zone suivants lorsque Tartaglia attaque ; les DGT infligés par ces effets comptent comme les DGT d'une attaque normale. Contre-courant : Flash : Les tirs visés complètement chargés touchant les ennemis affectés par Contre-courant infligent des DGT de zone consécutifs. Contre-courant : Flas peut être déclenché une fois toutes les 0,7 s. Contre-courant : Éclat : Vaincre un ennemi affecté par Contre-courant provoque une onde qui inflige l'effet Contre-courant aux ennemis proches. |
| Attaque normale : | Enchaîne jusqu'à 6 tirs consécutifs. |
| Attaque chargée : | Effectue un tir visé plus précis et infligeant davantage de DGT. Lors de la visée, la flèche se charge en élément Hydro, infligeant des DGT Hydro quand elle est complètement chargée et qu'elle touche ; elle inflige également aux ennemis touchés l'effet Contre-courant. |
| Attaque descendante : | Tire une pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de zone à l'impact. Impossible d'utiliser l'attaque descendante lors de l'activation de Posture du démon : Marée déchaînée (état de mêlée). |

|![aptitude 2](imagees/aptitude/aptitude_2_tartaglia.png) Posture du démon : Marée déchaînée | Aptitude de combat |
|---------|-------------|
| | Tartaglia invoque un arsenal d'armes exploitant la puissance de l'eau pure et infligeant des DGT Hydro aux ennemis proches ; Tartaglia entre en état de mêlée. En état de mêlée, les attaques normales et chargées de Tartaglia sont converties en DGT Hydro ne pouvant pas être enchantés, et changent : Contre-courant : Entaille Les attaques de mêlée appliquent à nouveau l'effet Contre-courant lorsqu'elles touchent un ennemi affecté par Contre-courant. Ceci inflige des DGT Hydro de zone. Les DGT infligés par Contre-courant : Entaille comptent comme des DGT de compétence élémentaire. Contre-courant : Entaille peut être déclenché une fois toutes les 1,5 s. La compétence prend fin après 30 s, ou lorsque vous appuyez dessus à nouveau. Tartaglia repasse alors en état de distance ; le TdR de la compétence est réinitialisé, et dure plus longtemps si l'état de mêlée a perduré. L'état de mêlée ne peut exéder 30 s. Au bout de cette durée, Tartaglia repasse automatiquement à l'état de distance. C'est pour venir à bout de ses difficultés avec cette arme que Tartaglia a choisi l'arc comme arme de prédilection. Mais… C'est sur le champ de bataille que sa vraie puissance se déchaîne. |
| Attaque normale : | Enchaîne jusqu'à 6 entailles Hydro consécutives. |
| Attaque chargée : | Exécute une frappe croisée rapide et inflige des DGT Hydro ; cette attaque consomme de l'endurance. |

|![aptitude 3](imagees/aptitude/aptitude_3_tartaglia.png) Ravage : Oblitération | Aptitude de combat |
|---------|-------------|
| | Tartaglia effectue différentes attaques selon l'état dans lequel il se trouve : État à distance : Éclair de balle maléfique Tire rapidement devant lui une flèche gorgée de magie Hydro, qui inflige des DGT Hydro de zone ainsi que l'effet Contre-courant aux ennemis en face. Restaure une partie de l'énergie élémentaire à la fin de l'attaque. État de mêlée : Lumière d'élimination Exécute une large entaille Contre-courant : Torrent Lorsqu'elle touche un ennemi affecté par Contre-courant, Lumière d'élimination consomme cet effet et inflige des DGT Hydro de zone. Les DGT infligés par Contre-courant : Torrent comptent comme les DGT d'un déchaînement élémentaire. L'eau qui coule n'est pas une vision courante dans son pays de neige ; peut-être que seul celui capable de saisir l'eau qui fuit entre les doigts est capable de comprendre comme elle est précieuse. |

|![aptitude 4](imagees/aptitude/aptitude_4_tartaglia.png) Fin infinie | Aptitude passive |
|---------|-------------|
| | L'effet Contre-courant est prolongé de 8 s. |

|![aptitude 5](imagees/aptitude/aptitude_5_tartaglia.png) Épée hydromorphe | Aptitude passive |
|---------|-------------|
| | Les ennemis subissant des coups CRIT lors d'attaques normales et chargées sont aussi affectés par l'effet Contre-courant tant que l'état de mêlée de Posture du démon : Marée déchaînée est actif. |

|![aptitude 6](imagees/aptitude/aptitude_6_tartaglia.png) Maîtrise de l'armement | Aptitude passive |
|---------|-------------|
| | Augmente le niveau des attaques normales des personnages de votre équipe de 1. |

## Constellations
---

|![constellation 1](imagees/constel/constelation_1_tartaglia.png) Posture du démon : Vague au calme | Constellation Niv.1 |
|---------|-------------|
| | Réduit le TdR de Posture du démon : Marée déchaînée de 20%. |

|![constellation 2](imagees/constel/constelation_2_tartaglia.png) Posture du démon : Courant souterrain | Constellation Niv.2 |
|---------|-------------|
| | Vaincre un ennemi affecté par Contre-courant restaure 4 pts d'énergie élémentaire à Tartaglia. |

|![constellation 3](imagees/constel/constelation_3_tartaglia.png) Chaos de l'Abîme : Vortex d'agitation | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Posture du démon : Marée déchaînée +3. |
| | Niveau max : 15 |

|![constellation 4](imagees/constel/constelation_4_tartaglia.png) Chaos de l'Abîme : Fleur de l'eau condensée | Constellation Niv.4 |
|---------|-------------|
| | Lorsque Tartaglia est dans l'état de mêlée de Posture du démon : Marée déchaînée, un Contre-courant : Entaille est déclenché contre les ennemis affectés par l'effet Contre-courant toutes les 4 s ; sinon, il déclenche un Contre-courant : Flash. Les Contre-courant : Entaille et Contre-courant : Flash déclenchés par cette constellation ne sont pas influencés par les intervalles de déclenchement de ces deux effets Contre-courant, et n'affectent pas leur propre intervalle de déclenchement. |

|![constellation 5](imagees/constel/constelation_5_tartaglia.png) Ravage : Lame sans forme | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Ravage : Oblitération +3. |
| | Niveau max : 15 |

|![constellation 6](imagees/constel/constelation_6_tartaglia.png) Ravage : Annihilation | Constellation Niv.6 |
|---------|-------------|
| | En état de mêlée, l'activation de Ravage : Oblitération supprime le TdR de Posture du démon : Marée déchaînée. Cet effet se produit après le retour à l'état de distance. |