
!!! info inline end "Identité"
    Jeune astromancienne mystérieuse, Mona se surnomme elle-même « la grande astromancienne Mona », et fait preuve d'une force singulière et d'une érudition hors-norme qui ne désavouent pas cette appellation.
    Bien que vivant une vie frugale et devant souvent faire face à des fins de mois difficiles, Mona refuse de faire payer ses prédictions... Sa bourse reste donc vide la plupart du temps.

![Mona](https://images.alphacoders.com/115/thumbbig-1155149.webp)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de lazurite varunada x1 - Champignon anémophile x3 - Nectar de Fleur mensongère x3 |
| 40 | - Moras 40 000 - Fragment de lazurite varunada x3 - Cœur d'eau pure x2 - Champignon anémophile x10 - Nectar de Fleur mensongère x15 |
| 50 | - Moras 60 000 - Fragment de lazurite varunada x6 - Cœur d'eau pure x4 - Champignon anémophile x20 - Nectar miroitant x12 |
| 60 | - Moras 80 000 - Morceau de lazurite varunada x3 - Cœur d'eau pure x8 - Champignon anémophile x30 - Nectar miroitant x18 |
| 70 | - Moras 100 000 - Morceau de lazurite varunada x6 - Cœur d'eau pure x12 - Champignon anémophile x45 - Nectar élémentaire x12 |
| 80 | - Moras 120 000 - Pierre de lazurite varunada x6 - Cœur d'eau pure x20 - Champignon anémophile x60 - Nectar élémentaire x24 |

## Aptitudes
---

|![aptitude 1](imagees/aptitude/aptitude_1_mona.png) Ondes du destin | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 attaques chargées d'eau qui infligent des DGT Hydro. |
| Attaque chargée : | Après un court délai, inflige des DGT Hydro de zone ; cette attaque consomme de l'endurance. |
| Attaque descendante : | Plonge depuis les airs en concentrant ses pouvoirs Hydro, infligeant des DGT aux ennemis sur la route et des DGT Hydro de zone à l'impact. |

|![aptitude 2](imagees/aptitude/aptitude_2_mona.png) Mirage aqueux | Aptitude de combat |
| | Mona invoque un mirage du destin créé à partir de projections aqueuses. |
| Mirage | Le mirage a les effets suivants : Nargue les ennemis proches et attire leur attention ; Inflige des DGT Hydro continus aux ennemis proches ; Lorsque le mirage prend fin, il explose et inflige des DGT Hydro de zone. |
| Appui long | Mona utilise la puissance de l'eau pour se propulser rapidement en arrière et invoque un mirage du destin. Il ne peut exister qu'un seul mirage à la fois. Les astromanciens les plus doués ont toujours rêvé de pouvoir créer un sosie d'eux-mêmes pour pouvoir faire face au danger ; mais Mona est sans doute la première à avoir réalisé ce tour de main. |

|![aptitude 3](imagees/aptitude/aptitude_3_mona.png) Vicissitude | Aptitude de combat |
|---------|-------------|
| Remplace le sprint | Mona se transforme en un courant d'eau, consommant de l'endurance pour se déplacer aussi vite qu'un torrent. Dans l'état Vicissitude, Mona peut se déplacer rapidement sur l'eau. À la fin de l'état, Mona trempe les ennemis proches lorsqu'elle réapparaît. Voici une technique que Mona est capable d'effectuer, mais pas son maître. Ce n'est pas comme si cette dernière souhaitait apprendre quoi que ce soit au sujet de l’œil divin Hydro… |

|![aptitude 4](imagees/aptitude/aptitude_4_mona.png) Voie divine | Aptitude de combat |
|---------|-------------|
| | Mona invoque des vagues chatoyantes pour créer un reflet du ciel étoilé, qui inflige l'état Bulle nocturne aux ennemis dans une large zone d'effet. |
| Bulle nocturne | Emprisonne les ennemis, et les trempe. Les ennemis les plus faibles sont immobilisés. Lorsqu'un ennemi emprisonné subit des DGT, il subit également les effets suivants : Un présage est appliqué à l'ennemi, qui subit alors une augmentation de DGT, y compris de l’attaque l'ayant provoqué ; L'effet Bulle nocturne disparaît, et inflige des DGT Hydro supplémentaires |
| Présage | Le présage augmente les DGT subis par l'ennemi tant qu'il est actif. C'est lorsque le regard des dieux dessina les étoiles que la notion de « destin » naquit. |

|![aptitude 5](imagees/aptitude/aptitude_5_mona.png) Leurre du fatum | Aptitude passive |
|---------|-------------|
| | Entrer dans l'état Vicissitude invoque automatiquement un mirage après 2 s en cas de présence d'ennemis proches. Le mirage ainsi invoqué dure 2 s, et ses DGT d'explosion sont équivalents à 50% des DGT de Mirage aqueux. |

|![aptitude 6](imagees/aptitude/aptitude_6_mona.png) Que le destin décide | Aptitude passive |
|---------|-------------|
| | Confère à Mona un bonus de DGT Hydro équivalent à 20% de sa recharge d'énergie. |

|![aptitude 7](imagees/aptitude/aptitude_4_mona.png) Principes d'astromancie | Aptitude passive |
|---------|-------------|
| | Lorsque vous fabriquez un matériau d'élévation d'arme, il y a 25% de chance qu'une partie des matières premières vous soit rendue. |

## Constellations
---

|![constellation 1](imagees/constel/constelation_1_mona.png) Prophétie immersive | Constellation Niv.1 |
|---------|-------------|
| | Les effets des réactions liées à l'élément Hydro sont renforcés pendant 8 s lorsque l'attaque de vos personnages dans l'équipe touche un ennemi marqué par un présage : Les DGT d'Électrocution, d'Évaporation, de Dispersion Hydro augmentent de 15% ; La durée de Gel augmente de 15%. |

|![constellation 2](imagees/constel/constelation_2_mona.png) Chaîne lunaire | Constellation Niv.2 |
|---------|-------------|
| | Les attaques normales ont 20% de chance d'être suivies par une attaque chargée. Cet effet peut être déclenché une fois toutes les 5 s. |

|![constellation 3](imagees/constel/constelation_3_mona.png) Révolution éternelle | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Voie divine +3. |
| | Niveau max : 15 |

|![constellation 4](imagees/constel/constelation_4_mona.png) Prophétie de la fin | Constellation Niv.4 |
|---------|-------------|
| | Augmente le taux CRIT de 15% lorsque vos personnages dans l'équipe attaquent un ennemi marqué par un présage. |

|![constellation 5](imagees/constel/constelation_5_mona.png) Un tour du destin | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Mirage aqueux +3.
| | Niveau max : 15 |

|![constellation 6](imagees/constel/constelation_6_mona.png) Rhétorique de l'adversité | Constellation Niv.6 |
|---------|-------------|
| | Dans l'état Vicissitude, Mona obtient un bonus de 60% DGT pour sa prochaine attaque chargée à chaque seconde passée à se déplacer. Cet effet est cumulable 3 fois max (bonus de 180%) et dure 8 s. |