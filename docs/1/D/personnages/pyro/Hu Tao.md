!!! info inline end "Identité"
    Hu Tao — 77e directrice du Funérarium Wangsheng, en charge de tout ce qui a trait aux funérailles.
    Elle s'occupe des obsèques de chacun avec zèle, et s'assure que l'équilibre entre le monde des morts et des vivants soient maintenu.
    À cela s'ajoute des... "talents" de poète ; plusieurs de ses vers de mirliton passent de bouche en bouche à Liyue. 

    Difficile, lorsqu'on se fie à son apparence de jeune fille joyeuse et un peu bizarre, d'imaginer qu'il s'agit là d'une personne dont la renommée n'est plus à faire : Hu Tao. |

![Hu Tao](https://images5.alphacoders.com/113/thumbbig-1133597.webp)

## Histoire

=== "Histoire 1"

    Les cérémonies funéraires permettent aux mortels de quitter ce monde avec dignité.

    Et on dit que le Funérarium Wangsheng de Liyue peint les derniers traits sur les rouleaux de la vie des gens de la manière la plus respectueuse.

    Les funérailles traditionnelles comprennent de multiples étapes telles que la tenue d'une veillée, l'inhumation, l'installation d'une plaque commémorative... Toutes soumises à des règles strictes.

    Quels que soient leur statut social et leur niveau de richesse, tous ceux qui partent méritent une cérémonie qui leur ferait honneur. C'est la philosophie du service client du Funérarium Wangsheng.

    On pourrait penser qu'une organisation aussi réputée ne devrait être dirigée que par une personne d'un savoir et d'une sagacité extrêmes.

    Pourtant, le lourd manteau du 77e directeur est tombé sur les épaules d'une jeune femme comme Hu Tao.

    Elle a une bonne réputation à Liyue. Chaque fois que quelqu'un mentionne Hu Tao, ses voisins ont du mal à naviguer dans la conversation.

    Bien qu'elle soit largement louée pour son esprit et sa perspicacité, ses notions excentriques ne sont pas aussi bienvenues, car elle laisse souvent libre cours à son imagination.

    À trois ans, elle lisait des volumes de textes classiques tout en faisant le poirier. À six ans, elle interrompait les cours et s'endormait dans des cercueils. À huit ans, elle a commencé à vivre au salon et à apprendre l'étiquette des cérémonies funéraires...

    On n'utiliserait jamais le mot "mesuré" pour décrire son comportement.

    Au cours de son adolescence, Hu Tao a été chargée de diriger une cérémonie funéraire pour la première fois.

    Le Funérarium et les consultants du salon anticipaient ses débuts le ventre noué comme s'ils étaient suspendus au-dessus des sommets du Jueyun Karst.

=== "Histoire 2"

    Heureusement, Hu Tao accorde la plus haute importance aux opérations du salon et cherche toujours à développer son activité.

    "Au Funérarium Wangsheng, les vivants nous donnent des Mora pour voir les morts sur leur chemin. Nous avons une responsabilité envers les deux parties, nous devons donc nous assurer que les deux parties sont heureuses."

    En ce qui concerne les règles du salon, Hu Tao les connaît toutes comme sa poche.

    Chaque jour, lorsque le salon ferme ses portes, Hu Tao invite des consultants de différents horizons à transmettre leurs enseignements aux pompes funèbres.

    "La tradition funéraire est comme la science. Elle ne peut pas être exécutée sur la base d'impressions et d'habitudes personnelles."

    De tous les conférenciers, Zhongli est le plus vénéré. En tant que tel, ses leçons se sont avérées les plus efficaces pour cultiver la compétence des pompes funèbres du Funérarium Wangsheng.

    Bien qu'il soit souvent taquiné par Hu Tao en raison de son style démodé, il est aussi la personne en qui elle a le plus confiance.

    En dehors de cela, Hu Tao demande toujours à ses pompes funèbres de respecter les souhaits de leurs clients et de ne pas insister sur une forme particulière d'enterrement.

    "Certains clients ne veulent rien de plus qu'une cérémonie paisible, certains optent pour un style d'événement plus vivant. Il y a aussi des clients plus riches qui demanderaient un arrangement funéraire des plus pompeux selon les besoins des clients."

    Depuis que Hu Tao a pris le relais, les opérations du salon ont été solides et fiables, avec des cérémonies menées avec tellement de tact que bon nombre de personnes superstitieuses à Liyue ont changé leur attitude envers les funérailles.

    Même ainsi, Hu Tao a toujours tendance à s'égarer, disparaissant dès que ses pompes funèbres sont occupées à écouter des conférences.

    Les passe-temps et les intérêts de cette jeune réalisatrice sont tout aussi excentriques que sa personnalité, et pourtant il est difficile de dire avec certitude si ces activités sont de nature purement ludique.

    L'ombre de Hu Tao peut être vue sur les quais éclairés par la lune ou aux points de vue les plus hauts et les plus précaires des montagnes, où elle est susceptible d'admirer le paysage et de façonner ses pensées en une belle poésie.

    Elle aime se promener librement la nuit, à la recherche d'inspiration. Quand cela la frappe, peu importe où elle se trouve, elle ne peut s'empêcher de composer un poème sur le champ.

    S'ils sont particulièrement chanceux, les marchands itinérants qui prennent du répit dans la forêt de pierre de Huaguang peuvent apercevoir une mystérieuse fille s'amusant dans la solitude.

    Hu Tao peut jouer à un jeu de cartes à quatre joueurs sans personne pendant des heures.

    Cela dit, les joies de s'engager dans de telles activités restent un mystère pour tous, sauf pour Hu Tao elle-même.

=== "Histoire 3"

    Le Bureau des affaires civiles est gardé par deux lions de pierre réalistes, symbole de pouvoir et d'autorité.

    Cependant, Hu Tao, qui passait un jour devant le bâtiment du ministère, les a vus sous un jour différent. Au début, elle a examiné les lions avec un regard pensif, mais assez vite, son visage pensif a fait place à un large sourire, alors qu'elle les giflait sur les pattes avant.

    À partir de ce moment, Hu Tao a souvent visité et caressé les statues de pierre. Non seulement elle leur parlerait, mais leur donnerait également des noms d'animaux – Moustaches pour celui de gauche et Minet pour celui de droite.

    À l'occasion, elle apportait même un seau d'eau et une grosse brosse pour les baigner, chaque mouvement si prudent et délibéré qu'on pourrait croire qu'ils étaient ses véritables animaux de compagnie.

    En l'occurrence, un autre félin, un chat calicot vivant et respirant, a pu être trouvé en train de savourer les spécialités locales devant le kiosque Xinyue, non loin du bâtiment du ministère. Un jour, des citadins sont venus jouer avec le favori du restaurant, offrant un clin d'œil intéressant aux particularités de Hu Tao. Rencontré avec les regards confus des spectateurs, Hu Tao a montré une confiance inébranlable.

    "Bien sûr, ton chat est mignon, mais on peut dire la même chose de Moustaches et Minet. Leur fourrure est peut-être plus dure, mais ils sont toujours aussi moelleux ! Tout animal qui apporte de la joie aux gens peut devenir un animal de compagnie. Et quand il s'agit de allures majestueuses, votre petite boule de poils ne peut tout simplement pas se mesurer à mes lions !"

    Autant dire que cette explication a été accueillie avec encore plus de consternation par les spectateurs.

    Pourtant, le groupe de personnes le plus choqué et le plus souvent par les bouffonneries de Hu Tao était les gardes du ministère. Des pas doux pouvaient souvent être entendus devant le bâtiment vers minuit. Au départ, les gardes soupçonnaient qu'il s'agissait d'un voleur qui se préparait à voler des fonctionnaires. À leur surprise incessante, il s'avérerait qu'il s'agissait simplement d'une jeune femme jouant avec les lions de pierre.

    Les gardes ont été confrontés à une énigme encore plus grande lorsque, une fois que tout le monde s'est habitué à contrecœur à sa présence, Hu Tao a interrompu ses visites.

    Cela signifiait que la responsabilité du nettoyage des statues retomba sur les épaules des gardes.

    Par la suite, ils ont décidé de garder la garde des lions pendant plusieurs jours, anticipant le retour de Hu Tao. Une fois qu'ils ont enfin eu l'occasion de lui demander pourquoi elle ne voulait plus venir, ils ont reçu la plus absurde des réponses.

    "Moustaches et Minet sont des adultes maintenant et peuvent prendre soin d'eux-mêmes ! Excusez-moi, je suis en retard pour mon tête-à-tête avec une Statue des Sept concernant le sens de la vie !"

=== "Histoire 4"

    Peu de temps après sa première rencontre avec le petit zombie, Hu Tao a décidé qu'en tant que véritable amie autoproclamée, elle devrait accorder à Qiqi la paix éternelle.

    Hu Tao continuerait à kidnapper Qiqi plusieurs fois après de nombreuses délibérations dans chaque cas, y compris le calcul du moment le plus propice pour la cérémonie, dans le but de suivre la procédure standard de crémation, après quoi elle placerait Qiqi dans une tombe à la périphérie de la ville.

    En fait, elle aurait déjà réussi sans les interventions opportunes de Baizhu.

    Chaque fois que le propriétaire du Cottage Bubu réussissait à les rattraper, Hu Tao avait déjà fait emballer Qiqi dans un sac, avec seulement sa tête qui sortait, regardant avec une confusion totale alors que Hu Tao creusait vigoureusement un trou pour le bûcher.

    Par la suite, Hu Tao a envoyé à Qiqi une lettre d'excuses, dans laquelle elle a exprimé son profond regret de ne pas avoir pu faire reposer Qiqi assez rapidement.

    Aux yeux de Hu Tao, Qiqi est décédée il y a longtemps, mais a été piégée dans ce monde et ne peut pas se libérer de sa souffrance éternelle.

    Baizhu, pour sa part, était devenu de plus en plus implacable dans sa poursuite de la vie éternelle après avoir rencontré Qiqi. Cet acte de défi contre le cycle de la vie et de la mort était inacceptable pour Hu Tao.

    Elle voulait enterrer Qiqi non seulement par considération pour son amie mais aussi pour rétablir l'ordre naturel.

    Mais Qiqi ne pouvait pas être plus en désaccord, car elle avait peur de la mort et n'aimait pas Hu Tao pour ses tentatives.

    Parce que la lutte entre Hu Tao et elle durait depuis si longtemps, Qiqi a connu une percée. Elle a commencé à se souvenir des endroits où elle pourrait se cacher pour éviter d'être capturée par Hu Tao.

    C'est peut-être grâce à ces efforts de survie désespérés que Hu Tao a décidé de faire quelque chose d'inhabituel et de creuser plus profondément dans le passé de Qiqi.

    L'histoire de son accident et le mystère de l'adepte... Cette série de coïncidences fit hésiter le cœur de Hu Tao.

    Puisque la volonté de vivre de Qiqi est si forte, elle ne devrait pas être forcée de passer à autre chose. Étant donné les circonstances, elle ne pouvait être traitée que comme une rare exception qui échappe aux lois naturelles.

    Depuis, l'attitude de Hu Tao envers Qiqi a radicalement changé, au point qu'elle la chouchoute.

    Malheureusement, le mal est fait et Hu Tao est devenu la bête noire de Qiqi. Le petit zombie aura peut-être besoin de plusieurs années de plus pour abandonner ses griefs.

=== "Histoire 5"

    En fait, Hu Tao n'est pas surtout connue pour son rôle de directrice, mais pour son autre grande réalisation : la création de poésie.

    Elle se surnomme la "marchande de vers des ruelles les plus sombres", et des vers libres coulent de sa bouche dès qu'elle a du temps libre pour sortir dans la rue.

    Le "Hilitune" est l'œuvre la plus célèbre de Hu Tao, très appréciée non seulement par les habitants du port, mais également sur les lèvres des enfants jusqu'au village de Qingce.

    Les amateurs et les critiques ont été grandement surpris par l'originalité simple mais profonde du "Hilitune" et de sa création, et se sont rendus en masse à Libraire Wanwen pour rechercher les œuvres de ce grand poète. Malheureusement, les anthologies de Hu Tao, intitulées respectivement "Baguettes de violon" et "Des vies communes", n'ont pas encore été publiées.

    Xingqiu, toujours immergé dans les livres, voulait également rencontrer cette personne étrange, et a donc choisi une date propice pour visiter, apportant un cadeau avec lui.

    Les deux se sont immédiatement entendus, et ils ont échangé des pointeurs et de la poésie impromptue dans la salle principale de Wangsheng — et face à la poésie du penchant artistique traditionnel de Xingqiu, Hu Tao pouvait toujours revenir en nature avec des vers étranges et merveilleux.

    Et il y avait un sens dans le chaos, en effet, un rythme bizarre que n'importe qui pouvait percevoir, et c'était aussi accrocheur.

    C'est ainsi qu'elle "battit le vieux maître avec des poings non entraînés", laissant Xingqiu à court de mots.

    Enfin, le combat poétique s'est terminé à l'amiable, et ils sont amis depuis, lisant et composant de la poésie ensemble quand ils ont le temps de se rencontrer.

    Au fil du temps, Chongyun a également été appelé à servir d'arbitre, leurs rires remplissant les rues.

    Les poèmes nés de ces séances seraient également enregistrés par les passants.

    Si jamais vous entendez une paire de couplets assortis, l'un strict et l'autre ludique, vous en avez probablement entendu un créé conjointement par Hu Tao et Xingqiu.

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'agate agnidus x1 - Fleur de soie x3 - Nectar de Fleur mensongère x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Jade juvénile x2 - Fleur de soie x10 - Nectar de Fleur mensongère x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Jade juvénile x4 - Fleur de soie x20 - Nectar miroitant x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Jade juvénile x8 - Fleur de soie x30 - Nectar miroitant x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Jade juvénile x12 - Fleur de soie x45 - Nectar élémentaire x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Jade juvénile x20 - Fleur de soie x60 - Nectar élémentaire x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_hutao.png) Lance secrète de Wangsheng | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 6 coups de lance. |
| Attaque chargée : | Charge en ligne droite et inflige des DGT aux ennemis sur sa route ; cette attaque consomme de l'endurance. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis en chemin et des DGT de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_hutao.png) Chaperon de la renaissance  | Aptitude de combat |
|---------|-------------|
| | Seule la flamme éternelle peut purifier le mal de ce monde. Hu Tao consomme un certain montant de PV pour repousser les ennemis proches, et entre dans l'état Paramita Papilio. |
| Paramita Papilio | *L'ATQ de Hu Tao augmente proportionnellement à ses PV max au moment où elle entre dans cet état. Le bonus ainsi conféré ne peut pas dépasser 400 % de son ATQ de base. ; Les DGT d'attaque sont convertit en DGT Pyro, et ne peuvent pas être enchantés. ; Les attaques chargées infligent l'effet Bourgeon de prune ; Augmente la RÉS à l'interruption de Hu Tao. |
| Bourgeon de Prune | Les ennemis affectés par Bourgeon de prune subissent des DGT Pyro toutes les 4 s, qui sont considérés comme étant des DGT de compétence élémentaire. La même cible ne peut être affectée que par un seul Bourgeon de prune à la fois, et la durée de cet effet ne peut être rafraîchie que par Hu Tao. L'État de Paramita Papilio se termine à la fin de sa durée, lorsque Hu Tao quitte le champ de  bataille ou lorsqu'elle est vaincue. La technique de la lance secrète est basée sur plusieurs principes, dont le premier est : "la lance ouvre les portes de l'au-delà et les papillons forment un pont entre les deux mondes." |

|![aptitude 3](images/aptitude/aptitude_3_hutao.png) Apaisement divin  | Aptitude de combat |
|---------|-------------|
| | Faisant appel aux âmes ardentes, Hu Tao inflige des DGT Pyro dans une large zone. Hu Tao récupère à chaque ennemi touché (5 ennemis max) une quantité de PV proportionnelle à ses PV max. Les DGT infligés et les PV récupérés sont plus importants si les PV de Hu Tao sont inférieurs ou égaux à 50 % lorsque la compétence touche. Le Funérarium Wangsheng ne voit pas d'un bon œil les âmes refusant de quitter ce monde. La crémation est pour Hu Tao le meilleur moyen d'avoir l'esprit tranquille - mais plus elle est anxieuse et plus son feu est puissant |

|![aptitude 4](images/aptitude/aptitude_4_hutao.png) Chrysalide | Aptitude passive |
|---------|-------------|
| | Le taux CRIT de tous les personnages de l'équipe (à l'exception de Hu Tao) augmente de 12 % durant 8 s lorsque l'état Paramita Papilio conféré par Chaperon de la renaissance prend fin. |

|![aptitude 5](images/aptitude/aptitude_5_hutao.png) Sang bouillant | Aptitude passive |
|---------|-------------|
| | Hu Tao obtient un bonus de DGT Pyro de 33 % lorsque ces PV sont inférieurs ou égaux à 50 %. |

|![aptitude 6](images/aptitude/aptitude_6_hutao.png) Plus il y en a, mieux c'est | Aptitude passive |
|---------|-------------|
| | 18 % de chance d'obtenir un plat suspect supplémentaire du même type lorsque vous cuisinez un plat parfait. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_hutao.png) Bouquet écarlate | Constellation Niv.1 |
|---------|-------------|
| | Les attaques chargées de Hu Tao ne consomment pas d'endurance tant qu'elle se trouve dans l'état Paramita Papilio conféré par Chaperon de la renaissance. |

|![constellation 2](images/constel/constelation_2_hutao.png) Sombres auspices | Constellation Niv.2 |
|---------|-------------|
| | Bourgeon de prune inflige un bonus de DGT équivalant à 10 % des PV max de Hu Tao lorsqu'elle bénéficie de l'effet. Apaisement divin applique de plus l'effet Bourgeon de prune lorsqu'il touche. |

|![constellation 3](images/constel/constelation_3_hutao.png) Rituel du sang | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Chaperon de la renaissance +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_hutao.png) Jardin du repos éternel | Constellation Niv.4 |
|---------|-------------|
| | Lorsqu'un ennemi affecté par Bourgeon de prune est vaincu, le taux CRIT de tous les personnages de l'équipe (à l'exception de Hu Tao) augmente de 12 % pendant 15 s. |

|![constellation 5](images/constel/constelation_5_hutao.png) Encens floral | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Apaisement divin +3. Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_hutao.png) L'envol du papillon | Constellation Niv.6 |
|---------|-------------|
| | Lorsque les PV de Hu Tao passent sous le seuil des 25 % ou lorsqu'elle subit assez de DGT pour être vaincue : Hu Tao n'est pas vaincu cette fois-ci. Pendant les 10 s qui suivent, ses RÉS élémentaires et physique augmentent de 200 %, son taux CRIT augmente de 100 %, et sa RÉS à l'interruption augmente considérablement. Cet effet se déclenche automatiquement lorsqu'il ne reste qu'1 PV à Hu Tao. Cet effet peut être déclenché une fois toutes les 60 s. |