
!!! info inline end "Identité"
    Yanfei — une conseillère juridique mi-femme, mi-Adepte inégalée.
    Oscillant entre respecter la loi et savoir s'adapter aux circonstances, Yanfei a trouvé son propre point d'équilibre.
    Son statut unique de conseillère juridique ainsi que ses méthodes particulières lui permettent de protéger aux mieux l'harmonie de Liyue.

    On raconte que Yanfei, la conseillère juridique la plus connue du Port de Liyue, a en mémoire plus de dix mille articles du code juridique, et peut ainsi s'appuyer dessus à tout moment en fonction de la situation pour arriver à ses fins de manière appropriée.

![Yanfei](https://images8.alphacoders.com/116/thumbbig-1161797.webp)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 |  - Moras 20 000 - Éclat d'agate agnidus x1 - Jade noctiluque x3 - Insigne du Pilleur x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - ade juvénile x2 - Jade noctiluque x10 - Insigne du Pilleur x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - ade juvénile x4- Jade noctiluque x20 - Insigne de corbeau en argent x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - ade juvénile x8- Jade noctiluque x30 - Insigne de corbeau en argent x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - ade juvénile x12 - Jade noctiluque x45 - Insigne de corbeau en or x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - ade juvénile x20 - Jade noctiluque x60 - Insigne de corbeau en or x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_yanfei.png) Sceau embrasé | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Tire des boules de feu qui infligent jusqu'à 3 vagues de DGT Pyro. Lorsqu'une attaque normale touche un ennemi, Yanfei obtient un sceau écarlate. Yanfei peut détenir un maximum de 3 sceaux écarlates, et chaque fois que cet effet se déclenche, la durée des sceaux en sa possession est réinitialisée. Chaque sceau écarlate diminue la consommation d'endurance de Yanfei, et disparaît lorsque Yanfei quitte le champ de bataille. |
| Attaque chargée : | Consomme de l'endurance pour infliger des DGT Pyro de zone après un court délai. La portée et l'intensité de ces DGT Pyro de zone augmentent avec le nombre de sceaux écarlates consonmmeés. |
| Attaque descendante : | Yanfei concentre sonénergie Pyro et plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT Pyro de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_yanfei.png) Pacte des flammes | Aptitude de combat |
|---------|-------------|
| | Yanfei en appelle aux flammes dévastatrices, infligeant des DGT Pyro de zone. Lorsque des ennemis sont touchés, Yanfei gagne le nombre maximum de sceaux écarlates. En plus de leur valeur juridique, les sceaux de Yanfei présentent des conditions d'utilisation très personnalisées. Après tout, ce n'est pas comme si le Bureau des affaires civiles pouvait changer les lois de la nature. |

|![aptitude 3](images/aptitude/aptitude_3_yanfei.png) Application des peines | Aptitude de combat |
|---------|-------------|
| | Invoque une explosion de flammes intense qui jaillit vers les ennemis proches, ingligeant des DGT Pyro de zone et confèrent à Yanfei le nombre maximum de sceaux écarlates, tout en appliquant l'effet Brillance. |
| Brillance | Confère les effets suiants : Confère à Yanfei un sceau écarlate à intervalles règuliers.  Augmente les DGT infligés par ses attaques chargées. Les effets de Brillance se dissipent lorsque Yanfei quitte le champ de bataille ou qu'elle est vaincue. Veuillez trouver ici les termes de cette question de vie ou de mort, et par làj'entends ma vie et votre mort ! |

|![aptitude 4](images/aptitude/aptitude_4_yanfei.png) Dispositions supplémentaires | Aptitude passive |
|---------|-------------|
| | Lorsque Yanfei consome un sceau écarlate en utilisant une attaque chargée, chaque sceau écarlate augmente le bonus de DGT Pyro de Yanfei de 5 % pour 6 s. Utiliser une attaque chargée avant la fin de la durée d'un effet dissiple l'effet précédent. |

|![aptitude 5](images/aptitude/aptitude_5_yanfei.png) Œil de la justice | Aptitude passive |
|---------|-------------|
| | Quand les attaques chargées de Yanfei infligent des coups CRIT aux ennemis, elle inflige des DGT Pyro de zone supplémentaires éqiovalant à 80 % de son ATQ. Ces DGT comptent comme des DGT d'ttaque chargées. |

|![aptitude 6](images/aptitude/aptitude_6_yanfei.png) Connaissance du terrain | Aptitude passive |
|---------|-------------|
| | Affiche l'emplacement des produits de Liyue sure la mini-carte. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_yanfei.png) Plaidoyer convaincant | Constellation Niv.1 |
|---------|-------------|
| | Lorsque Yanfei utilise une attaqye chargée, chaque sceau écarlate existant réduit sa consommation d'endurance de 10 % supplémentaires et augmente la RÉs à l'interruption lors de son déclenchement. |

|![constellation 2](images/constel/constelation_2_yanfei.png) Verdict final | Constellation Niv.2 |
|---------|-------------|
| | Le taux CRIT des attaques chargées de Yanfei augmente de 20% lorsqu'elles touchent des ennemis dont les PV sont inférieurs à 50 %. |

|![constellation 3](images/constel/constelation_3_yanfei.png) Mise sous scellés | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Pacte des flammes +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_yanfei.png) Droit commun | Constellation Niv.4 |
|---------|-------------|
| | Application des peines confère les effets suivants : Génère un bouclier qui absorbe une quantité de DGT équivalant à 45 % des PV max et dure 15 s. Le bouclier est efficace à 250 % contre les DGT Pyro. |

|![constellation 5](images/constel/constelation_5_yanfei.png) Déclaration sous serment | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Application des peines +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_yanfei.png) Clause complémentaire | Constellation Niv.6 |
|---------|-------------|
| | Augmente le nombre max de sceaux écarlates de 1. |