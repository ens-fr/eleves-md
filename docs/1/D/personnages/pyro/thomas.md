!!! info inline end "Identité"
    Employé de maison du Clan Kamisato qui gère la Commission culturelle. Thomas est aussi un « négociateur » bien connu à Inazuma.
    Affable et sociable, il est comme un poisson dans l'eau peu importe où il se trouve.
    Au premier abord, il semble débonnaire, mais est en réalité très responsable. Il possède une facette extrêmement sérieuse, que ce soit dans le cadre de son travail ou dans les relations humaines. 

![Thomas](https://th.bing.com/th/id/OIP.geVpA61oEpztEH2UmWpmTAHaEh?w=268&h=180&c=7&r=0&o=5&pid=1.7)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'agate agnidus x1 - Champignon fluorescent x3 - Insigne du Pilleur x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Perle brûlante x2 - Champignon fluorescent x10 - Insigne du Pilleur x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Perle brûlante x4 - Champignon fluorescent x20 - Insigne de corbeau en argent x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Perle brûlante x8 - Champignon fluorescent x30 - Insigne de corbeau en argent x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Perle brûlante x12 - Champignon fluorescent x45 - Insigne de corbeau en or x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Perle brûlante x20 - Champignon fluorescent x60 - Insigne de corbeau en or x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_thoma.png) Lance célère | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 coups de lance. |
| Attaque chargée : | Consomme de l'endurance pour charger en ligne droite et infliger des DGT aux ennemis sur la route. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_thoma.png) Bénédiction flamboyante | Aptitude de combat |
|---------|-------------|
| | Thomas utilise la pointe de sa lance comme point d'apui pour canaliser les flammes et effectuer un coup de pied qui inflige des DGT Pyro de zone et libère une barrière défensive brûlante. Au moment où elle se déclenche, la barrière applique l'élément Pyro à Thomas. L'absorption des DGt de la barrière est fonction des PV max de Thomas. La barrière brûlante possède les caractéristiques suivantes : Elle absorbe les DGT Pyro avec 250 % d'éfficacité L'absorption de DGT restant d'une barrière brûlant existante se cumulera si une autre barrière brûlante est obtenue en même temps, tandis que sa durée sera actualisée. L'absorption maximale de DGT de la barrière ne dépassera pas un certain pourcentage des PV max de Thomas. Pour un invité venu de loin, seule la passion la plus ardente permet de maîtriser les arts si précis de la lance d'Inazuma. Les prouesses de Thoas, en effet, ne se limitent pas à être « le disciple le plus rapide » ou « un lancier hautement qualifié », il a également imprégné sa technique de sa volonté de protéger les autres et de rupousser les ennemis. |

|![aptitude 3](images/aptitude/aptitude_3_thoma.png) O-yoroi écarlate  | Aptitude de combat |
|---------|-------------|
| | Thomas fait tournoyer sa lance, tranchant ses ennemis avec des DGT Pyro de zone et se transforment en un o-yoroi embrasé. |
| O-yoroi embrasé | L'attaque normale d'un personnage déployé affecté par un o-yoroi embrasé déclenchera un effrondrement ardent qui inflige des DGT Pyro de zone et crée une barrière brûlante. Un effronfrement ardent peut être déclenché une fois toutes les secondes. Une barrière brûlante créé de cette manière est identique à celle créée grâce à la compétence élémentaire « Bénédiction flamboyante » de Thomas, à l'exception de la quantité de DGT qu'elle peut absorber. Elle absorbe les DGT Pyro avec 250 % d'efficacité. L'absorption de DGT restante d'une barrière brûlante existante se cumulera si une autre barrière brûlante est obtenue en même temps, tandis que sa durée sera actualisée. L'absorption maximale de DGT de la barrière ne dépassera pas un certain pourcentage des PV max de Thomas. Si Thomas est vaincu, les effets de l'o-yoroi embrasé se dissiperont. |

|![aptitude 4](images/aptitude/aptitude_4_thoma.png) Superposition d'armures | Aptitude passive |
|---------|-------------|
| | Lorsque le personnage déployé obtient ou actualise une barrière brulante, la force de son bouclier augmente de 5% pendant 6 s. Cet effet peut être déclenché une fois toutes les 0,3 s et cumulé jusqu'à 5 fois. |

|![aptitude 5](images/aptitude/aptitude_5_thoma.png) Assaut enflammé | Aptitude passive |
|---------|-------------|
| | Les DGT infligés par l'effrondrement ardent d'O-yoroi écarlate augmentent d'une valeur équivalant à 2,2 % des PV max de Thomas. |

|![aptitude 6](images/aptitude/aptitude_6_thoma.png) Passion pêche | Aptitude passive |
|---------|-------------|
| | Lorsque la voyageu(r/se) réussit une pêche à Inazuma, l'aide de Thomas accorde 20 % de chances d'obtenir le double de prises. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_thoma.png) Devoir de camarade | Constellation Niv.1 |
|---------|-------------|
| | Lorsqu'un personnage protégé par la barrière brûlante de Thomas subit une attaque (Thomas excepté, les TdR de Bénédiction flamboyante et d'O-yoroi écarlate de Thomas diminuent de 3 s. Cet effet peut être déclenché une fois toutes les 20 s. |
        
|![constellation 2](images/constel/constelation_2_thoma.png) Compétences de subordonné | Constellation Niv.2 |
|---------|-------------|
| | Prolonge la durée d'O-yoroi écarlate de 3 s. |

|![constellation 3](images/constel/constelation_3_thoma.png) Résolution à toute épreuve | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Bénédiction flamboyante +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_thoma.png) Planification à long terme | Constellation Niv.4 |
|---------|-------------|
| | Après avoir utilisé O-yoroi écarlate, Thomas récupère 15 pts d'énergie élémentaire. |

|![constellation 5](images/constel/constelation_5_thoma.png) Feu incontrôlable | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude O-yoroi écarlate +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_thoma.png) Cœur brûlant | Constellation Niv.6 |
|---------|-------------|
| | Lorsqu'une barrière brûlante est obtenue ou actualisée, les DGT des attaques normales, chargées et plongeantes de tous les personnages de l'équipe augmentent de 15 % pendant 6 s. |