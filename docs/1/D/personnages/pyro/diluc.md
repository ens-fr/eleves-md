!!! info inline end "Identité"
    Étant l'homme le plus riche de Mondstadt, Diluc se présente toujours comme un jeune homme élégant et charmant, à l'image publique parfaite.
    Sous ce masque se cache un guerrier à la volonté de fer forgée dans les flemmes qui ne recule devant rien pour protéger Mondstadt. Ne comptez pas sur lui pour ménager ses adversaires, il sera sans pitié jusqu'à la fin.

![Diluc](https://images.alphacoders.com/112/thumbbig-1123761.webp)

## Histoire
---

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'agate agnidus x1 - Herbe à lampe x3 - Insigne de nouvelle recrue x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Graine de feu x2 - Herbe à lampe x10 - Insigne de nouvelle recrue x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Graine de feu x4 - Herbe à lampe x20 - Insigne de sergent x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Graine de feu x8 - Herbe à lampe x30 - Insigne de sergent x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Graine de feu x12 - Herbe à lampe x45 - Insigne d'officier x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Graine de feu x20 - Herbe à lampe x60 - Insigne d'officier x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_diluc.png) Épée trempée | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 coups. |
| Attaque chargée : | Exécute une succession d'entailles aussi rapides que tranchantes ; cette attaque consomme de l'endurance sur la durée. L'attaque chargée se termine sur un coup puissant. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_diluc.png) Assaut brûlant | Aptitude de combat |
|---------|-------------|
| | Diluc fait tournoyer sa lourde épée vers l'avant, infligeant des DGT Pyro. Cette attaque peut être enchaînée 3 fois de suite. Elle entre en TdR si l'enchaînement n'est pas assez rapide. Chacune de ces attaques représente les valeurs chères à Diluc : justice, courage et la responsabilité. |

|![aptitude 3](images/aptitude/aptitude_3_diluc.png) Aurore | Aptitude de combat |
|---------|-------------|
| | Diluc érupte en une explosion de flammes qui repoussent les ennemis et leur infligent des DGT Pyro. Les flammes convergent ensuite sur son arme, pour renaître en un phénix qui inflige d'importants DGT Pyro et repousse les ennemis sur sa route. Le phénix explose lorsqu'il atteint sa destination et inflige d'importants DGT Pyro de zone. Les flammes confèrent à l'arme de Diluc un enchantement Pyro. Celui qui apporte la lumière doit marcher dans les ténèbres. Les flammes au loin ne sont que le prélude de l'aube à venir. |

|![aptitude 4](images/aptitude/aptitude_4_diluc.png) Impitoyable | Aptitude passive |
|---------|-------------|
| | Les attaques chargées de Diluc consomment 50% d'endurance en moins, et durent 3 s de plus. |

|![aptitude 5](images/aptitude/aptitude_5_diluc.png) Bénédiction du phénix | Aptitude passive |
|---------|-------------|
| | Prolonge l'enchantement Pyro d'Aurore de 4 s. Diluc inflige 20% de DGT Pyro supplémentaires pendant la durée de cet effet. |

|![aptitude 6](images/aptitude/aptitude_6_diluc.png) Légende de l'aurore | Aptitude passive |
|---------|-------------|
| | Lorsque vous forgez une épée à deux mains, 15% du minerai utilisé vous sera rendu. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_diluc.png) Conviction | Constellation Niv.1 |
|---------|-------------|
| | Diluc inflige 15% de DGT supplémentaires aux ennemis ayant plus de 50% de leurs PV. |


|![constellation 2](images/constel/constelation_2_diluc.png) Chaleur des cendres | Constellation Niv.2 |
|---------|-------------|
| | L'ATQ de Diluc augmente de 10% et sa VIT d'attaque de 5% pendant 10 s lorsqu'il subit des DGT. 3 cumuls max. Cet effet peut être déclenché une fois toutes les 1,5 s. |

|![constellation 3](images/constel/constelation_3_diluc.png) Feu et acier | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Assaut brûlant +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_diluc.png) Flamme courante | Constellation Niv.4 |
|---------|-------------|
| | Exécuter Assaut brûlant en rythme augmente considérablement les DGT infligés. L'assut brûlant suivant un premier Assaut brûlant inflige 40% de DGT supplémentaires pendant 2 s lorsqu'il est exécuté dans les 2 s. |

|![constellation 5](images/constel/constelation_5_diluc.png) Phénix, Messager de l'Aube | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Aurore +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_diluc.png) L'épée des flammes brise-nuit | Constellation Niv.6 |
|---------|-------------|
| | Confère aux 2 attaques normales suivant dans les 6 s le lancement d'Assaut brûlant un bonus de 30% de DGT et de VIT d'attaque. De plus, Assaut brûlant n'interrompt plus les enchaînements d'attaques normales. |