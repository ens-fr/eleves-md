
!!! info inline end "Identité"
    Une jeune fille pleine de vie et l'unique éclaireuse de l'Ordre de Favonius. Avec trois titres consécutifs de championne de planement aérien de Mondstadt, cette discipline n'a plus de secrets pour elle. En tant qu'étoile montante de l'Ordre de Favonius, Amber brille toujours en première ligne !

![Amber](https://images3.alphacoders.com/111/thumbbig-1115222.webp)

## Histoire

=== "Histoire 1"

    Depuis son plus jeune âge, Amber déborde d'une énergie et d'un optimisme résolus.
    Son enthousiasme brûle à travers chaque journée sans faillir, et elle affronte avec bravoure toutes les épreuves sur son chemin.
    Il arrive cependant parfois que son excès d'énergie soit la cause de problème , à son corps défendant.
    Enfant, Amber était une vraie petite terreur. Ses exploits incluent notamment faire tomber sur la tête de son grand-père un nid d'oiseau en essayant d'en attraper les œufs, ainsi que la mise à feu des feuilles de Dendroblobs qui se mirent à courir dans tout les sens. Ces bêtises lui valurent au fil du temps d'acquérir une certaine notoriété auprès des chasseurs du coin.
    Amber arrivait tout le temps à quitter la scène de ses crimes, et nul même le plus aguerri des chevaliers n'arrivait à la poursuivre. C'était invariablement à son grand-père qu'échoyait le rôle de limiter les dégâts.
    L'amour silencieux et inconditionnel de ce dernier était telle une brise chaleureuse qui l'entourait avec douceur. Elle était consciente de cet amour empli de responsabilité, et l'acceptait tacitement.
    Mais ce n'est que lorsque son grand-père quitta les chevaliers de l'Ordre de Favonius qu'elle comprit vraiment l'étendue des responsabilités qu'il avait supportées durant toutes ces années.

=== "Histoire 2"

    Le grand-père d'Amber était autrefois le chef d'un groupe de mercenaires. Venant du Port de Liyue, il était chargé d'escorter les caravanes marchandes traversant Teyvat. Un jour qu'il effectuait une escorte de routine, le convoi fut victime de l'attaque d'un monstre horrible. Il fut le seul rescapé, restant en vie grâce à l'intervention d'un médecin de l'Ordre de Favonius.
    Cherchant à repayer cette faveur, et trop honteux pour rentrer chez lui, il joignit l'Ordre. Avant peu, il y fondait les Éclaireurs, supervisant personnellement leur entraînement, et les dirigeant lors de missions.
    Il trouva l'amour à Mondstadt, et y fonda une famille. Bien des années plus tard naissait Amber. Durant la journée, l'enfant se cachait derrière la fenêtre pour observer discrètement l'entraînement des Éclaireurs, pour la nuit venue se glisser subrepticement dans la cour pratiquer les mouvements qu'elle avait passé la journée à mémoriser.
    Ayant remarqué son enthousiasme, son grand-père décida de lui apprendre ce qu'il savait.
    « En m'acceptant, Mondstadt est devenue mon foyer, et j'ai fait le choix de protéger la cité. Peut-être hériteras-tu un jour de cette responsabilité ? Ah ! Qui sait ce que le futur renferme ? »
    Parlant ainsi, il lui tapotait la tête de sa paume caleuse, tandis que la jeune fille hochait la tête avec énergie.

=== "Histoire 3"

    Il y a de cela quatre ans eut lieu un événement qui changea profondément Amber.
    Ce jour-là, laissant derrière lui son insigne et son arme de service à l'Ordre, son grand-père disparut sans dire un mot à personne.
    Il avait été le pilier de la division des Éclaireurs, et sans lui à leur tête, ils perdirent rapidement de leur professionnalisme.
    Après plusieurs missions s'étant soldées par un échec, la division avait perdu tout de son ancienne aura, et ne subsista bientôt qu'en nom au sein de l'organisation de l'Ordre. Les plus expérimentés de ses membres furent transférés vers d'autres départements, ou démissionnèrent, au point que les quelques Éclaireurs restants ne furent même plus en mesure d'assurer leurs patrouilles quotidiennes.
    Plus grave encore, certains virent le départ du grand-père comme une trahison ; ceci ajouta de l'huile sur le feu, rendant la remise sur pied de la division encore plus difficile.
    Amber, qui venait de rejoindre les Éclaireurs, fut le témoin de tous ces événements. Ce fut la première fois de sa vie qu'elle ressentit une perte et une déception de cet ordre. Une nouvelle détermination l'habita, et elle laissa derrière elle son ancienne insouciance : elle grandirait, et deviendrait le plus vite possible une vraie Éclaireuse.
    Elle n'avait pas de plan précis, et manquait de maturité ; mais de courage et de détermination, elle ne manquait point. Elle voulait faire honneur au nom de son grand-père qu'elle souhaitait retrouver.
    Plus important, elle voulait reprendre le rôle tenu par lui, et devenir une loyale protectrice de son pays natal.

=== "Histoire 4"

    La vie au sein de l'Ordre ne fut pas facile au début.
    Les chevaliers expérimentés mirent un point d'honneur à soutenir la fille encore si jeune et marquée par la perte de son grand-père...
    Mais être la cible de toutes ces attentions était pour Amber un rappel constant qu'elle n'était pas encore prête à être autonome.
    En conséquence, elle demanda à assumer davantage de responsabilités, montrant à tous qu'elle était capable et déterminée.
    Vint enfin le jour où, durant un combat particulièrement ardu, les chevaliers prirent note de sa bravoure et de sa débrouillardise.
    C'est à ce moment qu'ils réalisèrent que la jeune fille qu'ils connaissaient avait grandi.
    Sa nouvelle maturité se doubla d'une nouvelle assurance. Face aux critiques et aux compliments, face au silence et aux provocations, sa réponse reste aujourd'hui toujours la même.
    « Je n'ai pas l'expérience des chevaliers confirmés ; mais le jour viendra où je serai la meilleure parmi les Éclaireurs ! »
    Amber n'est pas du genre à tourner autour du pot, et parle sans détour, de manière honnête et directe.
    Elle croit fermement qu'elle ne décevra pas son grand-père.

=== "Histoire 5"

    Amber est aujourd'hui toujours habitée par la même passion. Maintenant adulte, elle fait honneur aux enseignements de son grand-père, déployant son planeur et veillant sur la Cité de la Liberté de l’œil acéré de l'aigle, rusée et agile comme le lièvre.
    Le « Chevalier Carmin » est de nos jours connu dans tout Mondstadt. Le peuple est ravi de voir que la petite fille espiègle d'autrefois est en passe de devenir une protectrice sur qui compter.
    « N'ayez crainte, l’Éclaireuse, Amber est à vos côtés ! »
    Sa confiance en soi est inébranlable.
    « Je suis Amber, la seule et unique, la meilleure des Éclaireurs ! »

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'agate agnidus x1 - Herbe à lampe x3 - Pointe de flèche robuste x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Graine de feu x2 - Herbe à lampe x10 - Pointe de flèche robuste x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Graine de feu x4 - Herbe à lampe x20 - Pointe de flèche aiguisée x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Graine de feu x8 - Herbe à lampe x30 - Pointe de flèche aiguisée x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Graine de feu x12 - Herbe à lampe x45 - Pointe de flèche usée x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Graine de feu x20 - Herbe à lampe x60 - Pointe de flèche usée x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_amber.png) Archerie revisitée | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaine jusqu'à 5 tirs consécutifs. |
| Attaque chargée : | Effectue un tir visé plus précis infligeant davantage de DGT.  Lors de la visée, la flèche se charge en élément Pyro, infligeant de considérables DGT Pyro quand elle est complètement chargée. |
| Attaque descendante : | Tire une pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_amber.png) Poupée explosive | Aptitude de combat |
|---------|-------------|
| | Le fidèle Baron Lapinou est déployé sur le terrain. |
| Baron Lapinou | * Le baron nargue les ennemis proches et attire leur attention. Ses PV sont proportionnels aux PV max d'Amber. Le baron explose à la fin ou s'il est détruit entre temps, et inflige des DGT Pyro de zone. |
| Appui long | Ajuste la direction du jet. Ajuste la portée du jet. La lignée de Baron Lapinou est impressionnante, mais Amber a décidé d'arrêter de compter à partir du 893e Baron Lapinou. |

|![aptitude 3](images/aptitude/aptitude_3_amber.png) Pluie de flèches | Aptitude de combat |
|---------|-------------|
| | Tire une pluie de flèches qui inflige des DGT Pyro continus. Amber est une fille chaleureuse et pleine de passion. |

|![aptitude 4](images/aptitude/aptitude_4_amber.png) Dans le mille ! | Aptitude passive |
|---------|-------------|
| | Augmente le taux CRIT de Pluie de flèches de 10 % et la zone d'effet de 30 %. |

|![aptitude 5](images/aptitude/aptitude_5_amber.png) Tir précis | Aptitude passive |
|---------|-------------|
| | L'ATQ augmente de 15 % pendant 10 s lorsque le tir visé touche un point faible. |

|![aptitude 6](images/aptitude/aptitude_6_amber.png) Championne de planage | Aptitude passive |
|---------|-------------|
| | Réduit la consommation d'endurance de vos personnages dans l'équipe de 20 % lors du planage. Ne peut pas être cumulé avec d'autres aptitudes passives ayant les mêmes effets. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_amber.png) Une flèche suffit ! | Constellation Niv.1 |
|---------|-------------|
| | Tire 2 flèches consécutives à chaque tir visé. La seconde flèche inflige 20 % des DGT de la première. |

|![constellation 2](images/constel/constelation_2_amber.png) Tout est prêt | Constellation Niv.2 |
|---------|-------------|
| | Une toute nouvelle modification du Baron Lapinou ! Un tir visé à pleine charge aux pieds du Baron Lapinou, et ce dernier explose directement... Ce type d'explosion inflige 200 % de DGT supplémentaires. |

|![constellation 3](images/constel/constelation_3_amber.png) Ça brûle ! | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Pluie de flèches +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_amber.png) Ce n'est pas n'importe quelle poupée… | Constellation Niv.4 |
|---------|-------------|
| | Réduit le TdR de Poupée explosive de 20 % et permet de l'utiliser 1 fois supplémentaire. |

|![constellation 5](images/constel/constelation_5_amber.png) Baron Lapinou ! | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Poupée explosive +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_amber.png) Feu sauvage | Constellation Niv.6 |
|---------|-------------|
| | Pluie de flèches augmente la VIT de déplacement de tous les personnages de l'équipe de 15 % et leur ATQ de 15 % pendant 10 s. |