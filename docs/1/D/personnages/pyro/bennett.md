!!! info inline end "Identité"
    Les membres de la Guilde des Aventuriers de Mondstadt âgés de moins de 20 ans sont rares, mais Bennett n'a jamais eu de chance.
    Il est le seul membre actif de la Bande de Benny, l'équipe d'aventuriers qu'il a fondée. Celle-ci était à deux doigts de la dissolution à cause de ses amis, partis « en congé » à la suite de malheureux incidents.
    Cependant, à la demande sincère de Bennett, Catherine a gardé son équipe dans le registre de la Guilde, tout en lui cachant que tous les autres membres l'avaient officiellement quitté pour ne pas lui briser le cœur. Les membres de la Guilde des Aventuriers de Mondstadt âgés de moins de 20 ans sont rares, mais Bennett n'a jamais eu de chance.

![Bennett](https://images2.alphacoders.com/118/thumbbig-1181523.webp)

## Histoire 

=== "Histoire 1"

    Jack fut la première victime de la légendaire déveine de la bande à Benny.
    Alors qu'il se trouvait à quelques pas seulement du trésor recherché, une turbulence Géo comme il n'en advient qu'une fois par millénaire se fit soudain sentir, et en l'espace de quelques secondes le sol tremblant s'ouvrit, révélant à tous un abysse sans fond. "Si proche, et pourtant si loin...". Jack ne souffrit que de contusions mineures, mais cet épisode marqua la fin de sa carrière d'aventurier : il ne fut plus jamais le même homme, comme s'il avait eu le cœur brisé.
    Un autre membre de l'équipe, Royce, entendit quelqu'un crier "Héhooo !", avant de perdre conscience dans une immense explosion. La criminelle se cachant derrière la catastrophe n'était autre que Klee ; elle fut enfermée en punition pendant 7 jours dans sa cellule pour avoir fait exploser une grotte qui s'avéra malheureusement être une cave secrète explorée avec précaution par le groupe d'aventuriers.
    Heckler souffrit lui pendant une semaine de diarrhée aiguë après avoir rejoint la bande à Benny ; bien que cela s'avéra être dû à une intoxication, personne ne put lui faire démordre qu'il s'agissait là d'une autre manifestation de la poisse à Bennett.
    "Ils vont revenir ! Ils sont juste partis en vacances, hahaha... Je vous en prie ne supprimez pas mon équipe de la Guilde !" Face aux sincères supplications de Bennett, Catherine finit par soupirer ; elle n'eût pas le cœur de lui avouer que ses partenaires s'étaient déjà tous carapatés.

=== "Histoire 2"

    Autrefois existait un lieu empli de dangers e de désespoir. Un vieil explorateur vint à le parcourir.
    Les flammes dansantes lui brûlaient la peau, et le tonnerre retentissant dans l’air le transperçait de son grondement strident, tandis qu’il avançait en proie à des vents à déchirer l’âme.
    Parvenant enfin au bout de son infernal périple, quelle ne fut pas sa surprise lorsqu’il y trouva… un nourrisson.
    Le vieil aventurier, qui était convaincu d’être le premier être vivant à fouler ce lieu de perdition, ne put s’expliquer cette surprenante apparition.
    « Ce bébé… Ce bébé a été abandonné par le monde. »
    Nullement déçu de n’avoir pas trouvé quelque arme sacrée ou artefact précieux, le vieil homme décida que le nourrisson lui tiendrait lieu de trésor.
    « Nul doute que cette aventure a sa propre signification. » Et ainsi pensant, il prit avec soin le petit être chétif contre sa poitrine.
    Ce faisant, il ignorait qu’il allait à l’encontre de la volonté du monde.

=== "Histoire 3"

    Le vieil aventurier succomba à ses blessures avant de pouvoir raconter son histoire, ne laissant derrière lui que le mystérieux bébé rescapé.

    Il parvint seulement à murmurer ses ultimes mots avant de rendre son dernier souffle : " volonté "," aventure ", et " trésor final ". La branche Mondstadtoise de la Guilde des aventuriers comptait alors parmi ses membres plusieurs hommes d'un âge avancé n'ayant ni femme, ni enfants ; ceux-ci prirent le nourrisson sous leur aile, le traitant comme leur propre fils, et l'appelant Bennett. Le petit Bennett qui avait l'esprit éveillé les appelait tous " papa ".

    " Papa ton dentier est encore tombé dans la soupe ! " ; "Papa pourquoi continuer à porter ce vieux pourpoint en cuir ? Où sont les sous-vêtements que je t'ai achetés ? " ; "Ne reste pas trop près de moi sous l'orage papa, ou tu risques de te faire frapper par la foudre ! " De nos jours, Bennett passe le plus clair de son temps à s'occuper de ses aïeux qui n'ont pas de famille sur qui s'appuyer.

    " Tous ses vieux ont certainement trouvé un sacré trésor, pas de doutes là-dessus ! ", a coutume de dire en riant Cyrus, l'actuel chef de la Guilde, en donnant une bonne claque sur le dos de Bennett. Il a beau être malchanceux, au moins peut-il rendre la vie de ce qui lui sont chères plus agréable - c'est du moins ainsi que Bennett voit les choses. Ce dernier continue de croire que sa malchance finira par le guider vers son propre trésor.

=== "Histoire 4"

    Oz, le corbeau de Fischl, dit de Bennett qu'il s'agit " du jeune homme le plus tenace de l'univers ".

    Ceci n'est pas sans fondement : en effet, les cicatrices qui le retrouvent témoignent d'une vie d'épreuves. Attaques de monstres, effondrements de ruines, chutes de précipices ... Bennett le malchanceux à dû apprendre à la dure à se sortir de tous type de situations. Barbara la Diaconesse fut un jour surprise de voir avec qu'elle facilité Bennett remet en place les articulations disloquées.

    Un autre don résultant de toutes les épreuves qu'il a subit est son " talent unique " en combat. " Regardez ses mouvements... Cela ne devrait-il pas lui faire mal ? ", fit un jour le Grand Maître Varka, intrigué au plus haut point. Ce n'est pas que Bennett soit insensible à la douleur ; il s'y est juste habitué, comme on s'habitue à une mauvaise odeur ou à une lumière trop puissante. C'est ainsi que son style unique de combat - foncer droit devant au mépris du danger - est devenu en quelque sorte sa signature.

=== "Histoire 5"

    Qu'est-ce que la mort ? Ayant passé sa vie entière entre la vie et la mort, Bennett ne peut s'empêcher de se poser la question.

    Il sait que le vieil aventurier qui l'a sauvé a laissé après sa mort nombre d'histoire se transmettre d'une génération à l'autre. Il sait aussi qu'on n'entends nuls pleurs aux funérailles des vieux explorateurs n'ayant pas de famille, mais juste au son des verres d'amis de longues dates trinquant à leur départ. Il sait également qu'aux yeux des aventuriers de Mondstadt, " une bonne mort " est apportée par une vie dédiée à la poursuite des trésors et des secrets de ce monde ; l'âme de l'aventurier méritant est alors importait par l'Archon Anémo.

    Bennett était autrefois terrifié par l'idée-même de la mort. Mais après avoir longtemps réfléchi au sujet, il sent maintenant que la mort est une conclusion heureuse pour tout aventurier. Non que la fortune lui sourie en aucune manière de toute façon... " Allez direction le prochain trésor ! " ; et Bennett abandonne alors le fil de ses pensées.

## Elevation
---
| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'agate agnidus x1 - Chrysanthème à aubes x3 - Insigne du Pilleur x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Graine de feu x2 - Chrysanthème à aubes x10 - Insigne du Pilleur x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Graine de feu x4 - Chrysanthème à aubes x20 - Insigne de corbeau en argent x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Graine de feu x8 - Chrysanthème à aubes x30 - Insigne de corbeau en argent x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Graine de feu x12 - Chrysanthème à aubes x45 - Insigne de corbeau en or x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Graine de feu x20 - Chrysanthème à aubes x60 - Insigne de corbeau en or x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_bennett.png) Épée chanceuse | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Consomme de l'endurance pour déclencher deux coups d'épée rapides. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_bennett.png) Surpassion | Aptitude de combat |
|---------|-------------|
| | Bennett concentre l'élément Pyro et sa passion pour l'aventure sur son épée. Différentes choses se produisent selon le niveau de passion atteint… |
| Appui simple | Bennett exécute un coup rapide de sa lame enflammée, qui inflige des DGT Pyro aux ennemis en face. |
| Appui long | Bennett effecture une charge qui donne des résultats différents selon le niveau de charge. Niveau 1 : Bennett exécute une seconde attaque, qui inflige des DGT Pyro et projette les ennemis en l'air. Niveau 2 : Une troisième attaque complète l'enchaînement ; celle-ci inflige des DGT Pyro. L'attaque se termine par une explosion, qui projette simultanément Bennett et les ennemis dans les airs. Bennett ne subit pas de DGT lorsqu'il est projeté. Une attaque Pyro emplie de la passion et des espoirs de Bennett, mais passion débordante signifie surcharge, et qui dit surcharge dit explosion. |

|![aptitude 3](images/aptitude/aptitude_3_bennett.png) Merveilleux voyage | Aptitude de combat |
|---------|-------------|
| Champ vertueux | Les personnages se trouvant dans le champ et ayant moins de 70% de PV sont soignés d'une quantité de PV proportionnelle aux PV max de Bennett. Les personnages se trouvant dans le champ et ayant plus de 70% de PV obtiennent un bonus d'ATQ proportionnel à l'ATQ de base de Bennett. Le champ vertueux applique l'élément Pyro à tous les personnages qui s'y trouvent. Malgré tous les dangers qui ne manquent jamais de venir ponctuer chaque périple de la Bande à Benny, Bennett, son unique membre, préfère continuer à dire qu'il n'y a rien de tel qu'un peu d'inattendu pour pimenter une aventure, bien sûr. |

|![aptitude 4](images/aptitude/aptitude_4_bennett.png) Renouveau de la passion | Aptitude passive |
|---------|-------------|
| | Réduit le TdR de Surpassion de 20%. |

|![aptitude 5](images/aptitude/aptitude_5_bennett.png) Enthousiasme intrépide | Aptitude passive |
|---------|-------------|
| | Dans le champ de Merveilleux voyage, Surpassion bénéficie des effets suivants : TdR réduit de 50%. ne projette pas Bennett en l'air en Niveau 2 de charge. |

|![aptitude 6](images/aptitude/aptitude_6_bennett.png) R.A.S. | Aptitude passive |
|---------|-------------|
| | Réduit la durée d'une expédition de 25% lorsqu'elle se déroule à Mondstadt. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_bennett.png) Perspective de voyage | Constellation Niv.1 |
|---------|-------------|
| | Le bonus d'ATQ conféré par Merveilleux voyage n'est plus soumis à la restriction de PV, et augmente numériquement l'ATQ de base de Bennett de 20%. |

|![constellation 2](images/constel/constelation_2_bennett.png) Brise-désespoir | Constellation Niv.2 |
|---------|-------------|
| | La recharge d'énergie de Bennett augmente de 30% lorsque ses PV tombent en dessous de 70%. |

|![constellation 3](images/constel/constelation_3_bennett.png) Passion ardente | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Surpassion +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_bennett.png) Enthousiasme éternel | Constellation Niv.4 |
|---------|-------------|
| | Une attaque supplémentaire est permise lorsque le deuxième coup de Surpassion en 1e charge est une attaque normale. Celle-ci inflige 135% des DGT du deuxième coup. |

|![constellation 5](images/constel/constelation_5_bennett.png) Ouverture d'âme | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Merveilleux voyage +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_bennett.png) Feu et courage | Constellation Niv.6 |
|---------|-------------|
| | Le champ de Merveilleux voyage confère à tous les personnages de l'équipe déployés et maniant une épée à une ou deux mains ou une arme d'hast qui s'y trouvent un bonus de DGT Pyro de 15%, ainsi qu'un enchantement Pyro. |