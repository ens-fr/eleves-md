!!! info inline end "Identité"
    Pyrotechnicienne aux talents extraordinaires et propriétaire de la boutique « Feux de Naganohara », Yoimiya est connue sous le nom de « Reine du festival d'été ».
    C’est une jeune fille aussi chaleureuse que le feu : son âme d'enfant et sa passion pour son métier en font une personnalité rayonnante.

    « La beauté des feux d'artifice trouve son éternité dans l'engouement des gens. »

![Yoimiya](https://images5.alphacoders.com/117/thumbbig-1174049.webp)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'agate agnidus x1 - Herbe à sanglots x3 - Parchemin divinatoire x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Perle brûlante x2 - Herbe à sanglots x10 - Parchemin divinatoire x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Perle brûlante x4 - Herbe à sanglots x20 - Parchemin sigillé x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Perle brûlante x8 - Herbe à sanglots x30 - Parchemin sigillé x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Perle brûlante x12 - Herbe à sanglots x45 - Parchemin maudit x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Perle brûlante x20 - Herbe à sanglots x60 - Parchemin maudit x24 |


## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_yoimiya.png) Flambée pyrotechnique | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Yoimiya peut enchaîner jusqu'à cinq tirs consécutifs. |
| Attaque chargée : | Effectue un tir visé plus précis avec un bonus de DGT. En visant, des flammes viennent se concentrer sur le carreau de la flèche avant d'être tirées lors de l'attaque. L'effet varie selon le nombre de charges : 1 charge : Tire une flèche enflammée qui inflige des DGT Pyro. 2 charges : Génère jusqu'à 3 flèches chercheuses en fonction du temps de charge, qui sont libérées lors du tir visé. Les flèches suivent les ennemis proches, et infligent lorsqu'elles touchent des DGT Pyro.
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_yoimiya.png) Danse du feu « Niwabi » | Aptitude de combat |
|---------|-------------|
| | Yoimiya brandit un cierge magique, s'entourant d'un champ de salpêtres. |
| Niwabi Enshou | Pendant la durée de cette aptitude, les flèches tirées lors de l'attaque normale de Yoimiya sont des flèches enflammées, infligeant davantage de DGT, et dont les DGT sont convertis en DGT Pyro. Durant cette période, la deuxième charge de son attaque normale « Flambée pyrotechnique » ne génère pas de flèche chercheuses. L'effet s'arrête lorsque Yoimiya quitte le champ de bataille. Le « Niwabi » est une sorte de feux d'artifice à base d'encens inventé par Yoimiya, qui ne requiert aucune formation ou préparation préalable, ce qui le rend particulièrement populaire. Après pusieurs incidents, la Commission administrative a interdit l'usage du « Niwabi » dans l'enseinte de la ville, mais il arrive encore que de discrètes étincelles illuminent la nuit. Vues des hauteurs, on dirait de petites étoiles filantes. |

|![aptitude 3](images/aptitude/aptitude_3_yoimiya.png) Saxifrage Ryuukin | Aptitude de combat |
|---------|-------------|
| | Yoimiya s'élance dans les airs avec sa création originale, le « Saxifrage Ryuukin » et tire des roquettes flamboyantes pleines de surprises, qui infligent des DGT Pyro de zone, et marquent un ennemi d'une « Flamme Ryuukin ». |
| Flamme Ryuukin | Les attaques normales, chargées et plongeantes, les compétences élémentaires et les déchaînements élémentaires de tous les personnages de l'équipe (hormis Yoimiya) génèrent une explosion lorsqu'elles touchent un ennemi marqué d'une Flamme Ryuukin, infligeant des DGT Pyro de zone. Lorsqu'un ennemi marqué d'une Flamme Ryuukin est vaincu avant la fin de l'effet, ce dernier est transféré à un ennemi proche qui hérite de la durée restante. Il est possible de générer ainsi une explosion toutes les 2 s max. Quand Yoimiya est vaincue, l'effet des Flammes Ryuukin générées par aptitudes prend fin. Les feux d'artifice sont les étoiles créées par l'homme, comètes detinées aux cieux. Et même si elle n'est qu'éphémère, la lumière qu'ils génèrent peut demeurer longtemps dans le cœur des hommes. |

|![aptitude 4](images/aptitude/aptitude_4_yoimiya.png) Tour de passe-passe | Aptitude passive |
|---------|-------------|
| | Tant que Danse du feu « Niwabi » est active, les attaques normales de Yoimiya augmentent son bonus de DGT Pyro de 2 %. Cet effet dure 3 s et peut être cumulé jusqu'à 10 fois. |

|![aptitude 5](images/aptitude/aptitude_5_yoimiya.png) Aube d'une nuit d'été | Aptitude passive |
|---------|-------------|
| | Lorsque Yoimiya libère Saxifrage Ryuukin, l'ATQ de tous les autres personnages de l'èuipe (hormis Yoimiya) augmente de 10 % pendant 15 s. De plud, chaque cumul de l'aptitude passive « Tour de passe-passe » que Yoimiya possède lorsqu'elle exécute Saxifrage d'or augmente ce bonus d'ATQ de 1 %. |

|![aptitude 6](images/aptitude/aptitude_6_yoimiya.png) Rentabilisation maximum | Aptitude passive |
|---------|-------------|
| | Lors de la fabrication de décorations, d'aménagements ou de décorations de paysage, une partie des matériaux est récupérée. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_yoimiya.png) Agate Ryuukin | Constellation Niv.1 |
|---------|-------------|
| | Prolonge la durée de la FLamme Ryuukin générée par Saxifrage Ryuukin de 4 s. De plus, lorsqu'un ennemi affecté par Flamme Ryuukin est vaincu pendant la durée de l'effet, l'ATQ de Yoimiya augmente de 20 % pendant 20 s. |

|![constellation 2](images/constel/constelation_1_yoimiya.png) Procession de feux de joie | Constellation Niv.2 |
|---------|-------------|
| | Lorsque les DGT Pyro infligés par Yoimiya résultent en un coup CRIT, cette derinére obtient pendant 6 s un bonus de DGT Pyro de 25 %. Cet effet s'applique également lorsque Yoimiya fait partie de l'équip mais n'est pas déployée. |

|![constellation 3](images/constel/constelation_3_yoimiya.png) Flammes ingénieuses | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Danse du feu « Niwabi » +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_yoimiya.png) Pro de la pyrotechnie | Constellation Niv.4 |
|---------|-------------|
| | Réduit le Tdr de Danse du feu « Niwabi » de 1,2 s lorsque Flamme Ryuukin de Yoimiya déclenche une explosion. |

|![constellation 5](images/constel/constelation_5_yoimiya.png) Nuit d'un festival d'été | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Saxifrage Ryuukin +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_yoimiya.png) Pluie de météores de Nagonahara | Constellation Niv.6 |
|---------|-------------|
| | Tant que Danse du feu « Niwabi » est active, les attaques normales de Yoimiya ont 50 % de chances de libèrer une flèche enflammée suplémentaire qui inflige 60 % des DGT d'origine. Ces DGT sont considérés comme des DGT d'attaque normale. |