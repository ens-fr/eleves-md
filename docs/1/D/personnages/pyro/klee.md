
!!! info inline end "Identité"
    Chevalier de l'étincelle de l'Ordre de Favonius ! Elle entre toujours en scène d'une manière explosive !
    Puis disparait silencieusement sous le regard sévère de Jean, la grande maîtresse intérimaire de l'Ordre de Favonius.
    Bien qu'elle ait tout le loisir d'élaborer de nouveaux explosifs pendant ses périodes d'isolement, Klee préfèrerait ne pas être détenue.

![Klee](https://images6.alphacoders.com/111/thumbbig-1115869.webp)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'agate agnidus x1 - Champignon anémophile x3 - Parchemin divinatoire x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Graine de feu x2 - Champignon anémophile x10 - Parchemin divinatoire x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Graine de feu x4 - Champignon anémophile x20 - Parchemin sigillé x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Graine de feu x8 - Champignon anémophile x30 - Parchemin sigillé x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Graine de feu x12 - Champignon anémophile x45 - Parchemin maudit x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Graine de feu x20 - Champignon anémophile x60 - Parchemin maudit x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_klee.png) Boum ! Boum ! | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Une bombe qui explose au contact, ça c'est un bel objet ! Klee jette jusqu'à 3 bombes consécutives, qui infligent des DGT Pyro de zone. |
| Attaque chargée : | Après un court délai, bombarde les ennemis et leur inflige des DGT Pyro de zone ; cette attaque consomme de l'endurance. |
| Attaque descendante : | Plonge depuis lers airs en concentrant ses pouvoirs Pyro, infligeant des DGT aux ennemis sur la route et des DGT Pyro de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_klee.png) Bombe rebondissante | Aptitude de combat |
|---------|-------------|
| | Boiing, boiing ! Klee lance une bombe qui pète de joie ! La bombe rebondit 3 fois et explose à chaque rebond, infligeant des DGT Pyro de zone. Après le 3e rebond, la bombe se divise en plusieurs mines. Les mines explosent après un court délai ou au contact de l'ennemi, infligeant des DGT Pyro de zone. Klee commence avec 2 charges. La Bombe rebondissante est l'amie fidèle de Klee… Elle veut toujours la présenter à tout le monde. |

|![aptitude 3](images/aptitude/aptitude_3_klee.png) Fleur bombardier | Aptitude de combat |
|---------|-------------|
| | Klee s'enflamme ! Invoque une Fleur bombardier qui harcèle les ennemis, infligeant des DGT Pyro de zone. Les membres de l'Ordre de Favonius sont convaincus qu'une bonne étoile veille sur Klee ; sinon, comment expliquer qu'elle ne se blesse jamais ? |

|![aptitude 4](images/aptitude/aptitude_4_klee.png) Cadeau explosif | Aptitude passive |
|---------|-------------|
| | Les attaques normales ainsi que Bombe rebondissante ont 50% de chance de conférer à Klee une étincelle explosive lorsqu'elles infligent des DGT. Cette dernière est consommée lors de la prochaine attaque chargée, qui inflige 50% de DGT supplémentaires sans consommer d'endurance. |

|![aptitude 5](images/aptitude/aptitude_5_klee.png) Étincelle infinie | Aptitude passive |
|---------|-------------|
| | Lorsque Klee inflige des DGT CRIT avec des attaques chargées, l'énergie élémentaire de tous les membres de l'équipe est restaurée de 2 pts. |

|![aptitude 6](images/aptitude/aptitude_6_klee.png) Des trésors partout ! | Aptitude passive |
|---------|-------------|
| | Affiche l'emplacement des produits de Mondstadt sur la mini-carte. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_klee.png) Bombardement continu | Constellation Niv.1 |
|---------|-------------|
| | Les attaques et aptitudes ont une chance de générer des étincelles qui bombardent les ennemis, infligeant 120% des DGT de Fleur bombardier. |

|![constellation 2](images/constel/constelation_2_klee.png) Éclats de bombe | Constellation Niv.2 |
|---------|-------------|
| | Les mines de Bombe rebondissante réduisent la DÉF des ennemis de 23% pendant 10 s. |

|![constellation 3](images/constel/constelation_3_klee.png) Réglage spécial de Klee | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Bombe rebondissante +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_klee.png) Tout est prêt | Constellation Niv.4 |
| | Une explosion se produit lorsque Klee quitte le champ de bataille tant que Fleur bombardier est active. Cette explosion inflige 555% des l'ATQ en DGT Pyro de zone. |

|![constellation 5](images/constel/constelation_5_klee.png) Bombardement d'étoiles | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Fleur bombardier +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_klee.png) À tout feu | Constellation Niv.6 |
|---------|-------------|
| | Klee restaure 3 pts d'énergie élémentaire à tous les membres de l'équipe (à l'exception de Klee) toutes les 3 s tant que Fleur bombardier est active. L'activation de Fleur bombardier confère un bonus de DGT Pyro de 10% pendant 25 s à tous les membres de l'équipe. |