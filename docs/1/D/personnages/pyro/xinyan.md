!!! info inline end "Identité"
    À Liyue, le rock 'n' roll est une nouvelle forme d'art et Xinyan en est la pionnière.
    Sa musique et ses chansons passionnées sont un pied de nez aux préjugés et au conformisme.
    Si vous en avez l'occasion, ne manquez pas l'un de ses concerts. 

    Suivez les flammes dans la nuit de Liyue, laissez-vous aller aux rythmes endiablés et rejoignez cette performance qui vous fera tout oublier...

![Xinyan](https://images7.alphacoders.com/121/thumbbig-1210585.webp)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | -  Moras 20 000 - Éclat d'agate agnidus x1 - Muguet bleu x3 - Insigne du Pilleur x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Graine de feu x2- Muguet bleu x10 - Insigne du Pilleur x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Graine de feu x4 - Muguet bleu x20 - Insigne de corbeau en argent x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Graine de feu x8 - Muguet bleu x30 - Insigne de corbeau en argent x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Graine de feu x12 - Muguet bleu x45 - Insigne de corbeau en or x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Graine de feu x20 - Muguet bleu x60 - Insigne de corbeau en or x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_xinyan.png) Danse du feu | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 coups d'épée. |
| Attaque chargée : | Exécute une succession d'attaques tournoyantes ; cette attaque consomme de l'endurance sur la durée. L'attaque chargée se termine sur un coup puissant. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_xinyan.png) Jeu fervent | Aptitude de combat |
|---------|-------------|
| | Xinyan brandit sa guitare, et inflige aux ennemis proches des DGT Pyro, et génère un bouclier grâce au soutien de l'audience. Le bouclier absorbe une quantité de PV proportionnelle à la DÉF de Xinyan et aux ennemis touchés : 0 à 1 ennemi touché : Bouclier Niv. 1 2 ennemis touchés : Bouclier Niv. 2 3 ennemis touchés ou plus : Bouclier Niv. 3, qui inflige en plus des DGT Pyro continus aux ennemis proches. Le bouclier a les effets suivants : Applique au moment de sa création l'élément Pyro à Xinyan ; Le bouclier est efficace à 250% contre les DGT Pyro. Xinyan n'insiste jamais assez sur le fait que tous ces effets sont le pinacle de l'art de la scène, et qu'ils constituent un phénomène d'ordre physique n'ayant bien sûr rien à voir avec son œil divin. |

|![aptitude 3](images/aptitude/aptitude_3_xinyan.png) Riff rebelle | Aptitude de combat |
|---------|-------------|
| | Grattant son instrument avec passion, Xinyan libère une vague d'ondes qui repoussent les ennemis, et infligent aux ennemis proches des DGT physiques, au plus grand plaisir du public. L'ambiance se réchauffe tellement que des explosions ont lieu, infligeant aux ennemis proches des DGT Pyro. Xinyan avait autrefois pour habitude de terminer toutes ses représentations de cette manière. Ce n'est pas à cause du danger qu'elle s'est arrêtée, mais parce qu'elle n'avait pas de final encore plus épique à proposer dans le cas d'un nouveau rappel. |

|![aptitude 4](images/aptitude/aptitude_4_xinyan.png) « Le spectacle doit continuer, même sans audience… » | Aptitude passive |
|---------|-------------|
| | Réduit le nombre d'ennemis à toucher pour déclencher chaque niveau de bouclier de Jeu fervent : 1 ennemi touché : Bouclier Niv. 2 ; 2 ennemis touchés ou plus : Bouclier Niv. 3. |

|![aptitude 5](images/aptitude/aptitude_5_xinyan.png) « … Ça c'est du rock ! » | Aptitude passive |
|---------|-------------|
| | Les DGT physiques infligés par les personnages sous la protection du bouclier généré par Jeu fervent augmentent de 15%. |

|![aptitude 6](images/aptitude/aptitude_6_xinyan.png) Recette pour un tube | Aptitude passive |
|---------|-------------|
| | 12% de chance d'obtenir un plat supplémentaire lorsque vous cuisinez un plat parfait qui renforce la DÉF. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_xinyan.png) Accélération fatale | Constellation Niv.1 |
|---------|-------------|
| | La VIT des attaques normales et chargées de Xinyan augmente de 12% pendant 5 s lorsque Xinyan  inflige un coup CRIT. Cet effet ne peut être déclenché qu'une fois toutes les 5 s. |

|![constellation 2](images/constel/constelation_2_xinyan.png) Ouverture impromptue | Constellation Niv.2 |
|---------|-------------|
| | Le taux CRIT des DGT physiques de Riff rebelle augmente de 100%. Un bouclier Niv. 3 est de plus généré à l'activation. |

|![constellation 3](images/constel/constelation_3_xinyan.png) Double corde | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Jeu fervent +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_xinyan.png) Rythme irrésistible | Constellation Niv.4
|---------|-------------|
| | La RÉS physique des ennemis touchés par Xinyan au début de Jeu fervent est réduite de 15% pendant 2 s. |

|![constellation 5](images/constel/constelation_5_xinyan.png) Rappel fervent | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Riff rebelle +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_xinyan.png) Rock infernal | Constellation Niv.6 |
|---------|-------------|
| | Réduit le coût d'endurance des attaques chargées de Xinyan de 30%. Xinyan obtient de plus un bonus d'ATQ équivalent à 50% de sa DÉF lorsqu'elle exécute une attaque chargée. |