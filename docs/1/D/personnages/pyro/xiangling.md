!!! info inline end "Identité"
    La nouvelle toque du Restaurant Wanmin, qui travaille aussi en salle quand elle n'est pas aux fourneaux. Sa maîtrise de l'art culinaire se voit notamment dans ses spécialités aussi relevées que savoureuses.
    Douée malgré son jeune âge, Xiangling jouit d'une excellente réputation auprès des amateurs de bonne chère de la Falaise Chihu.
    Si par chance elle vous invite un jour à venir tester une nouvelle recette, allez-y les yeux fermés. Satisfaction garantie. 

![Xiangling](https://images5.alphacoders.com/110/thumbbig-1109802.webp)

## Histoire
---

=== "Histoire 1"

    Xiangling est responsable de la destruction d'innombrables woks. Certains furent entièrement brûlés par des Pyroblobs ou finirent complètement craquelés après avoir été au contact de fleurs brumeuses, d'autres devinrent juste méconnaissables après des explosions qui... arrivèrent.
    Son père dut réfléchir par trois fois avant de décider de ne pas la bannir des cuisines.
    L'imagination débordante de Xiangling et sa témérité sont autant de traits qui la poussent à constamment essayer de nouvelles recettes. Peut-être n'est-ce pas si mal après tout, même si pour cela il faut sacrifier wok après wok...
    Toujours est-il que les dépenses mensuelles du Restaurant Wanmin incluent toujours un réapprovisionnement en woks.

=== "Histoire 2"

    « Il y a de nombreux secrets à une cuisine bien faite ; mais le plus important reste la passion. Lorsque Xiangling décida d'apprendre la cuisine, son père lui confia son propre livre personnel de recettes, l'oeuvre d'une vie ; cette phrase était inscrite sur la page de titre.
    Venant de n'importe qui d'autre cette citation aurait paru aux yeux de Xiangling un cliché éculé : mais venant de son père, elle la chérit et l'appliqua du mieux qu'elle put. À ses yeux, c'est en restant fidèle à ce principe que son père avait réussi à assurer sa position dans la guerre éternelle opposant les styles culinaires de Li et de Yue.
    Le « conflit » entre les deux écoles battait alors son plein, et nombre d'enseignes en ressentaient la pression ; le Restaurant Wanmin ne faisait pas exception.
    « La bonne cuisine c'est de la bonne cuisine, point final ! »
    Le monopole sur les ingrédients de luxe tels que matsutake et crabe faisait enrager Xiangling ; sa façon de canaliser sa colère fut de prendre en main les cuisines.
    L'heure était venue pour elle de mettre à profit le savoir de son père, et de faire évoluer le restaurant. « Je ferai des plats délicieux, et peu importe les ingrédients ! »

=== "Histoire 3"

    L'existence de Xiangling est une calamité pour les Brutocolinus du Karst Jueyun. Ils se réveillèrent ainsi un jour pour découvrir que leurs gourdins avaient disparu.
    Le sort de la flore du Marais Dihua n'est guère plus enviable. Ces efforts payèrent, puisque Xiangling créa de nouvelles recettes telles que son poisson grillé sur gourdin ou encore son riz collant façon queue de cheval.
    Celui qui répète les recettes de ses prédécesseurs ne crée jamais de nouveau plat, après tout. Xiangling avait compris très vite qu'il lui fallait s'éloigner des chemins battus pour trouver sa propre voie.
    Sa recette de volaille à la sauce du chef fit appel à toute son ingéniosité ; mais son père, goûteur officiel, en paya le prix, puisqu'il ne put absorber que du gruau pendant les deux jours qui suivirent.
    « Ainsi donc la fleur de Qingxin combinée à la menthe semble donner la diarrhée... Intéressant. »
    Et ainsi Xiangling ajoutait ses découvertes dans son journal. Assez étrangement, elle n'était jamais victime de ses propres expériences, bien qu'elle aussi mange de ses plats ; elle en concevait quelque culpabilité. Peut-être était-ce dû à son jeune âge et à sa bonne santé, à moins qu'elle n'ait réussi avec le temps à développer une résistance à certaines toxines... Qui sait ?

=== "Histoire 4"

    Il n'est pas rare qu'un client lui demande l'origine de l'étrange créature qui l'accompagne ; l'histoire qu'elle leur raconte est la suivante : Un jour qu'elle était partie cueillir du muguet bleu, la pluie se mit à tomber dru. Elle n'avait pas parcouru la moitié da chemin, que déjà la fatigue et la faim se faisaient sentir. Elle trouva une grotte et y pénétra ; à l'intérieur était dressé une sorte d'autel. Elle s'assit à côté, et sortit deux pains au maïs.
    Elle engloutit le premier, et posa le second sur l'autel, comptant le manger plus tard ; puis elle s'endormit.
    À son réveil, le pain de maïs restant avait disparu ; à sa place se tenait une étrange créature qui la dévisageait.
    « Tu as aimé ? »
    La créature hocha la tête.
    « Tu en veux d'autre ? », fit Xiangling en sortant de la viande séchée de son sac.
    À nouveau, la créature hocha la tête.
    Et c'est ainsi que Xiangling se fit un nouvel ami. Peut-être la créature était-elle attirée par sa cuisine ; toujours est-il qu'elle la suivait partout, et en devint inséparable.
    Xiangling lui donna le nom de son snack préféré lorsqu'elle était enfant :« Gooba ».

=== "Histoire 5"

    Les talents de cuisinère de Xiangling s'améliorèrent radicalement une fois qu'elle eut goûté à tous les plats disponibles à Liyue. Ses spécialités épicées lui valurent même les compliments de son père, un chef ayant plus de deux décennies de métier.
    Combinant de généreuses quantités de piments à toute une gamme d'herbes et de fruits pour plus d'arômes, elle mit au point une saveur unique et complexe. Elle créa également le plat qui allait devenir la spécialité du Restaurant Wanmin : la marmite de perche à dos noir, qui allie la richesse, les couleurs et les arômes puissants de la cuisine Li aux ingrédients à forte valeur nutritionnelle et aux saveurs fraîches de l'école Yue.
    Certains se sont essayés à copier ses recettes ; mais nul n'a à ce jour réussi à reproduire leur goût unique.
    « Faites macérer dans le miel pendant une nuit les brins de muguet bleu que vous aurez cueillis fraîs ; faites ensuite sécher, puis réduisez en poudre. Saupoudrez sur n'importe quel plat pour en relever la saveur. »
    Ne vous y trompez pas : cette recette secrète coûta à Xiangling nombre de maux de ventre, d'égratignures à aller chercher les ingrédients nécessaires, et de woks sacrifiés.

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'agate agnidus x1 - Piment de Jueyun x3 - Bave de Blob x3 |
| 40 | - Moras 40 000 - Fragment d'agate agnidus x3 - Graine de feu x2 - Piment de Jueyun x10 - Bave de Blob x15 |
| 50 | - Moras 60 000 - Fragment d'agate agnidus x6 - Graine de feu x4 - Piment de Jueyun x20 - Mucus de Blob x12 |
| 60 | - Moras 80 000 - Morceau d'agate agnidus x3 - Graine de feu x8 - Piment de Jueyun x30 - Mucus de Blob x18 |
| 70 | - Moras 100 000 - Morceau d'agate agnidus x6 - Graine de feu x12 - Piment de Jueyun x45 - Essence de Blob x12 |
| 80 | - Moras 120 000 - Pierre d'agate agnidus x6 - Graine de feu x20 - Piment de Jueyun x60 - Essence de Blob x24 |

## Aptitudes
---

|![aptitude 1](images/aptitude/aptitude_1_xiangling.png) Cuisine-fu | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups de lance. |
| Attaque chargée : | Charge en ligne droite et inflige des DGT aux ennemis sur sa route ; cette attaque consomme de l'endurance. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](images/aptitude/aptitude_2_xiangling.png) Attaque Gooba | Aptitude de combat |
|---------|-------------|
| | Xiangling invoque le panda Gooba. Tant que Gooba est actif, il crache du feu sur les ennemis, leur infligeant des DGT pyro de zone continus. Gooba raffole des produits épicés ; mais même ça c'est un peu trop pour lui. |

|![aptitude 3](images/aptitude/aptitude_3_xiangling.png) Pyrotation | Aptitude de combat |
| | Démontrant à nouveau sa maîtrise du feu et des armes, Xiangling libère une tornade de feu qui tournoie autour d'elle. La tornade suit le personnage tant qu'elle est active, infligeant des DGT Pyro aux ennemis sur sa route. On n'a jamais assez d'explosifs. JAMAIS. |

|![aptitude 4](images/aptitude/aptitude_4_xiangling.png) Feux croisés | Aptitude passive |
|---------|-------------|
| | Augmente la portée des jets de feu de Gooba de 20 %. |

|![aptitude 5](images/aptitude/aptitude_5_xiangling.png) Attention, ça pique | Aptitude passive |
|---------|-------------|
| | À la fin d'Attaque Gooba, le gentil petit Gooba laisse quelques piments sur sa position avant de disparaître. Ces piments confèrent un bonus de 10 % d'ATQ pendant 10 s. |

|![aptitude 6](images/aptitude/aptitude_6_xiangling.png) Cheffe cuisinière | Aptitude passive |
|---------|-------------|
| | 12 % de chance d'obtenir un plat supplémentaire lorsque vous cuisinez un plat parfait qui renforce l'ATQ. |

## Constellations
---

|![constellation 1](images/constel/constelation_1_xiangling.png) Croustillant à l'extérieur, tendre à l'intérieur | Constellation Niv.1 |
|---------|-------------|
| | La RÉS Pyro des ennemis touchés par Gooba est réduite de 15% pendant 6 s. |

|![constellation 2](images/constel/constelation_2_xiangling.png) L'huile sur le feu | Constellation Niv.2 |
|---------|-------------|
| | Le dernier coup d'attaque normale inflige pendant 2 s un effet d'implosion aux ennemis. Au bout de ce délai, ils explosent, infligeant des DGT Pyro de zone équivalent à 75% de l'ATQ. |

|![constellation 3](images/constel/constelation_3_xiangling.png) Sauté profond | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Pyrotation +3. |
| | Niveau max : 15 |

|![constellation 4](images/constel/constelation_4_xiangling.png) Douce cuisine | Constellation Niv.4 |
|---------|-------------|
            Prolonge la durée de Pyrotation de 40 %.

|![constellation 5](images/constel/constelation_5_xiangling.png) Colère de Gooba | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Attaque Gooba +3. |
| | Niveau max : 15 |

|![constellation 6](images/constel/constelation_6_xiangling.png) Pyrotation condensée | Constellation Niv.6 |
|---------|-------------|
| | Les DGT Pyro de tous les personnages de l'équipe augmentent de 15% tant que Pyrotation est active. |