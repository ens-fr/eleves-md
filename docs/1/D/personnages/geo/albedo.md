
!!! info inline end "Identité"
    Un alchimiste vivant à Mondstadt, travaillant au service de l'Ordre de Favonius.
    « Génie », « Kreideprinz », « Enquêteur en chef »… Les titres et les éloges lui importent peu : à ses yeux, seules comptent ses recherches.
    Albedo n'a que faire de l'accumulation de richesse et des relations sociales. Son unique objectif est la poursuite du savoir, et la compréhension des connaissances les plus obscures et les plus profondes laissées par les générations d'érudits l'ayant précédé.

![Albedo](https://images3.alphacoders.com/112/thumbbig-1122526.webp)

## Histoire 
---

=== "Histoire 1"

    Albedo occupe une position importante au sein de l’Ordre de Favonius. Cependant, on le voit rarement se montrer en public.
    Ce n’est point là la manifestation de quelque excentricité propre à un esprit érudit. Au contraire, l’attitude d’Albedo envers tout un chacun est aussi ouverte qu’affable.
    Mais à ses yeux, les relations humaines requièrent un certain investissement émotionnel, sans aucune garantie de fiabilité dans le temps. Aussi préfère-t-il maintenir une distance amicale certes, mais surtout appropriée, entre lui et ceux qui l’entourent.
    Albedo est introuvable à Mondstatd lorsqu’il ferme son laboratoire ou qu’il part collecter quelque matériau.
    Mais n’allez pas croire que ces manières cachent une personnalité froide ou misanthrope.
    Ainsi, il n’est pas rare de le voir faire montre d’une joie et d’un entrain véritables lorsqu’il dirige ses deux assistants, Timaeus et Sucrose.
    Mais quand il voit Lisa, une personne si douée se contenter d’un poste de librairie… il ne peut s’empêcher d’éprouver quelques tristes regrets.

=== "Histoire 2"

     « C’est grâce à la recommandation de son maître qu’Albedo a pu s’établir à Mondstadt. »
     Cette dernière affirmation est la vérité même, bien que Mondstadt accueille chaleureusement toute personne, avec ou sans recommandation.
     En réalité, il serait peut-être plus approprié d’affirmer que la vie d’Albedo à Mondsatdt serait plus simple sans avoir été présenté.
     En effet, c’est à Alice, la célèbre auteure de voyage et vieille connaissance de son maître Rhinedottir, que cette dernière adressa sa lettre de recommandation.
     La lecture de la missive plongea Alice dans une profonde réflexion.
     « Je lis ici qu’il faut que je vous trouve un laboratoire… Ca ne court pas vraiment les rues. Hum… Ah, je sais ! »
     Et c’est ainsi qu’Albedo fut présenté à l’Ordre de Favonius, qu’il rejoignit.
     Les tâches qu’on lui confie ne sont guère difficiles, et ne requièrent qu’une infime portion de ses talents.
     Albedo pensait à tort pouvoir mettre à profit le reste de son temps libre pour se consacrer à ses expériences et ses artéfacts. Mais cela s’avéra plus difficile que prévu.
     Alice a une fille, Klee.
     … C’est ça, CETTE Klee.
     « À partir d’aujourd’hui on est une famille. Tu peux la considérer comme ta propre sœur ! »
     Et depuis ce jour, Albedo dépense une grande partie de son énergie à garder un œil sur la jeune et turbulente Klee.

=== "Histoire 3"

     Albedo ne garde aucun souvenir de sa propre famille. Aussi loin qu'il puisse se rappeler, le jeune alchimiste se voit aux côtés de son maître, explorer les recoins de ce monde.
     Qu'il s'agisse des membres de l'Ordre de Favonius, de dame Alice ou bien de ce voyageur venu d'un autre monde, nombre de personnes de l'entourage d'Albedo occupent une place particulière à ses yeux.Cependant, aucune ne peut rivaliser avec celle qu'il accorde à son maître, Rhinedottir.
     Cette dernière est sa seule et unique vraie famille.
     Rhinedottir est d'un naturel froid et sévère. C'est elle qui éleva Albedo lorsqu'il était enfant, et qui lui enseigna les secrets de l'alchimie.
     « L'univers est tel les noirs traînées habitant le ciel de la vérité ; la terre est le souvenir des vies et du temps passés. Tu es telle la craie, et la terre noire, la Khemia, est la source de l'alchimie, et de toute vie. Et ceci... »
     Albedo revoit encore son maitre exécuter une technique complexe, faisant apparaître d'une gigantesque cuve de culture un être colossal alors qu'autour mille fragments se brisaient au sol.
     « ... C'est la vie. »

=== "Histoire 4"

     Albedo menait autrefois une existence aussi simple qu'heureuse.
     Aucun soucis, juste le long cours tranquille du temps, répondre aux attentes et enseignements de son maître suffisant à donner à sa vie toute sa signification.
     Mais un jour, Rhinedottir découvrit au fond de quelque ténébreux recoin de ce monde une relique répondant au nom de laruo; Cœur de Naberius ».
     À compter de ce jour fatidique, la femme disparut. Elle ne laissa derrière elle qu'une simple note, une lettre de recommandation, ainsi qu'un ouvrage ancien.
     La note était ainsi rédigée : « Abedo, rends-toi à Mondstadt, présente cette lettre de recommandation à mon amie Alice, et termine ton ultime enseignement. »
     Quant à l'ancien livre, il s'agissait du précieux « Livre des Rites » chérie par la femme.
     Albedo reçut aussi à l'occasion un cadeau bien particulier à se yeux : la reconnaissance écrite de son statut d'alchimiste, ainsi que son surnom, « Kreideprinz », le « Prince de la Craie. ».
     Chacun des enseignements qu'il reçut de son maître par le passé avait été incroyablement difficile.
     Albedo avait toujours ajouté foi aux menaces de cette dernière lorsqu’elle lui disait : « Échoue, et je t'abandonne. »
     Mais la tâche qui cette fois-ci l'attendait dépassait en difficulté tout ce qu'il avait pu affronter auparavant...
     Serait-ce donc là l'ultime défi qui se tiendrait entre leurs retrouvailles futures ?
     « Voici l'ultime réalisation que j'attends de ta part : montre-moi la vérité et le sens de ce monde. »

=== "Histoire 5"

     L'alchimie pratiquée par Albedo n'a rien en commun avec celle pratiquée à travers les Sept Nations de Teyvat.
     L'héritage qui lui a été transmis est originaire d'un autre royaume - Khaenri'ah. Khaenri'ah est un monde souterrain que seules très peu d'espèces d'animaux habitent. C'est pour cette raison qu'Albedo a poussé à l'extrême l'étude e la branche « créatrice » de l'alchimie - la « Khemia », l'art de la création de la vie.
     Enfant, Albedo apprit la vérité suivante en parcourant les écrits de son maître :
     La craie est la forme évoluée de la Terre noire. Ceci lui avait d'ailleurs été plus tard confirmé par Rhinedottir.
     « La craie est la forme la plus pure de la terre, et l'origine de la vie. »
     Albedo fait aujourd'hui preuve d'une maîtrise autrement plus poussée de l'alchimie, et a acquis un savoir immense.
     « La craie naît de la Khemia. »
     Le jeune alchimiste a compris toutes les vérités sous-jacentes à cette simple affirmation.
     Toutes ces arcanes qu'aucune langue ne peut expliquer sont à jamais à ses yeux liées au souvenir précieux qu'il garde de son maître.
     Bien qu'il ne s'agisse pas de sa mère biologique, il n'empêche que pour Albedo, c'est ) elle et elle seul qu'il doit la vie.
     « J'espère que je ne me fais pas des idées mais quand les parents parlent du « sens de la vie » à leurs enfants, ils parlent de trouver le bonheur, c'est ça ?... »
     Ainsi pense à l'occasion le jeune alchimiste.L'alchimie pratiquée par Albedo n'a rien en commun avec celle pratiquée à travers les Sept Nations de Teyvat.
     le et elle seul qu'il doit la vie.

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de topaze prithiva x1 - Cécilia x3 - Parchemin divinatoire x3 |
| 40 | - Moras 40 000 - Fragment de topaze prithiva x3 - Pilier de basalte x2 - Cécilia x10 - Parchemin divinatoire x15 |
| 50 | - Moras 60 000 - Fragment de topaze prithiva x6 - Pilier de basalte x4 - Cécilia x20 - Parchemin sigillé x12 |
| 60 | - Moras 80 000 - Morceau de topaze prithiva x3 - Pilier de basalte x8 - Cécilia x30 - Parchemin sigillé x18 |
| 70 | - Moras 100 000 - Morceau de topaze prithiva x6 - Pilier de basalte x12 - Cécilia x45 - Parchemin maudit x12 |
| 80 | - Moras 120 000 - Pierre de topaze prithiva x6 - Pilier de basalte x20 - Cécilia x60 - Parchemin maudit x24 |

## Aptitudes
---

|![aptitude 1](imageeee/aptitude/aptitude_1_albedo.png) Escrime de Favonius - Blanche | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Consomme de l'endurance pour déclencher deux coups d'épée rapides. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageeee/aptitude/aptitude_2_albedo.png) Genèse : Aura solaire | Aptitude de combat |
|---------|-------------|
| | Génère une Aura solaire, qui inflige des DGT Géo de zone. |
| Aura solaire | Un Germe éphémère est créé lorsque les ennemis se trouvant dans la zone subissent des dégâts, infligeant des DGT Géo de zone proportionnels à la DÉF d'Albedo. Un Germe éphémère peut être généré toutes les 2 s. Lorsqu'un personnage rejoint le centre de l'Aura solaire, l'énergie Géo présente se concentre pour générer un podium ascendant lui permettant de dominer le champ de bataille. Il ne peut exister qu'un seul podium à la fois. L'Aura solaire est une construction Géo ; il ne peut exister qu'une seule Aura solaire créée par Albedo à la fois. |
| Appui long | Ajuste la zone où la compétence sera lancée. Cet étrange dispositif rocheux est semblable à l'art même de l'alchimie : né de la terre et aspirant au ciel. |

|![aptitude 3](imageeee/aptitude/aptitude_3_albedo.png) Transformation : Reflux Géo | Aptitude de combat |
|---------|-------------|
| | L'énergie Géo contenue dans le sol explose en émergeant sous les ordres d'Albedo, infligeant des DGT Géo de zone. Lorsqu'une Aura solaire générée par Albedo est présente sur le champ de bataille, 7 Germes fatals sont générés, qui fleurissent en explosant, infligeant des DGT Géo de zone. Les DGT d'explosion et les DGT des Germes fatals ne génèrent pas de Germes éphémères. Sous les commandes du Kreideprinz, les forces habitant le sol émergent en une marée implacable. |

|![aptitude 4](imageeee/aptitude/aptitude_4_albedo.png) Menace calcaire | Aptitude passive |
|---------|-------------|
| | Les Germes éphémères générés par Genèse : Aura solaire infligent 25% de DGT supplémentaires aux ennemis dont les PV sont inférieurs à 50%. |

|![aptitude 5](imageeee/aptitude/aptitude_5_albedo.png) Sagesse en bouteille | Aptitude passive |
|---------|-------------|
| | L'activation de Transformation : Reflux Géo confère à tous les personnages de l'équipe à proximité un bonus de 125 pts de maîtrise élémentaire pendant 10 s. |

|![aptitude 6](imageeee/aptitude/aptitude_6_albedo.png) Découverte de génie | Aptitude passive |
|---------|-------------|
| | 10% de chance d'obtenir le double de produits lors de la synthèse de matériaux d'élévation d'arme. |

## Constellations
---

|![constellation 1](imageeee/constel/constelation_1_albedo.png) Fleur d'Éden | Constellation Niv.1 |
|---------|-------------|
| | Albedo récupère 1,2 pts d'énergie élémentaire lorsqu'un Germe éphémère généré par Genèse : Aura solaire éclot. |

|![constellation 2](imageeee/constel/constelation_2_albedo.png) Éon de l'avènement | Constellation Niv.2 |
|---------|-------------|
| | Genèse : Aura solaire confère pendant 30 s l'effet Jugement fatal à Albedo lorsque des Germes éphémères sont générés : Tous les cumuls de Jugement dernier sont consommés lors du lancement de Transformation : Reflux  Géo, conférant aux Germes éphémères et aux coups CRIT de cette aptitude un bonus de DGT proportionnel au nombre de cumuls. Chaque cumul de Jugement dernier confère un bonus de DGT équivalent à 30% de la DÉF d'Albedo. Cet effet peut être cumulé 4 fois maximum.

|![constellation 3](imageeee/constel/constelation_3_albedo.png) Fleur solaire | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Genèse : Aura solaire +3. |
| | Niveau max : 15 |

|![constellation 4](imageeee/constel/constelation_4_albedo.png) Chute divine | Constellation Niv.4 |
|---------|-------------|
| | Les personnages déployés se trouvant dans la zone de l'Aura solaire infligent 30% de DGT d'attaque descendante supplémentaires.

|![constellation 5](imageeee/constel/constelation_5_albedo.png) Flux antique | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Transformation : Reflux Géo +3. |
| | Niveau max : 15 |

|![constellation 6](imageeee/constel/constelation_6_albedo.png) Pureté de la terre | Constellation Niv.6 |
|---------|-------------|
| | Les personnages déployés se trouvant dans la zone de l'Aura solaire infligent 17% de DGT supplémentaires lorsqu'ils sont sous la protection d'un bouclier généré par Cristallisation. |