
!!! info inline end "Identité"
    Zhongli (Chinois :钟离 Zhōnglí) est un personnage Géo jouable dans Genshin Impact.

    Consultant mystérieux invité par le Funérarium Wangsheng, ce beau jeune homme aux manières élégantes possède des connaissances qui vont bien au-delà de celles des gens ordinaires.
    Bien que personne ne sache d'où il est originaire, Zhongli est coutumier de l'étiquette et des règles de la bienséance. Il effectue toutes sortes de cérémonies pour le compte du Funérarium Wangsheng. 

![Zhongli]()https://images8.alphacoders.com/114/thumbbig-1144793.webp

## Histoire
---

=== "Histoire 1"

    À Liyue, si une personne accorde une grande attention aux détails et a des critères insurmontables par lesquels elle juge certaines choses, alors elle est appelée « particulière ».

    En vérité, tout le monde est « particulier » à propos de quelque chose. Certains détestent la nourriture épicée, d'autres ne mangent pas de poisson, et certains veulent que leur tofu soit servi sucré ...

    Mais Zhongli, il est particulier pour tout.

    Il doit assister aux opéras des artistes les plus célèbres, prendre les oiseaux les plus luxuriants pour   se promener, et il doit se rendre personnellement dans la cuisine pour informer les cuisiniers du ratio de pétoncles et de poisson nécessaire pour faire l'œuf de pleine lune le plus authentique.

    Zhongli a une expertise dans toutes sortes de domaines : de la mode aux nécessités quotidiennes, du vin et des collations, des thés et des épices, de la flore et de la faune, et il débattra même activement des affaires de commerce, de politique et des relations entre les Sept Nations.

    Mais il ne vous renseigne que sur des anecdotes inutiles, car il aime aussi partager des choses sans importance.

=== "Histoire 2"

    Lorsque vous achetez quelque chose à Liyue, il faut toujours négocier.

    C'est la règle. Quelle que soit la valeur que le propriétaire donne à son produit, ou son histoire, les prix sont toujours flexibles. Considérez la moitié du prix indiqué comme étant un bon point de départ pour les négociations.

    Mais lorsque Zhongli achète quelque chose (ou plutôt demande à quelqu'un de l'acheter pour lui), il ne regarde jamais à la dépense. Tant que l'objet lui plaît, Zhongli est prêt à payer le prix fort.

    Cependant, il oublie toujours son porte-monnaie...

    Il a des amis qui l'aident pour les petites dépenses quotidiennes, et pour les grosses factures, il trouve toujours le moyen de retarder le paiement.

    Pour tous ces marchands qui se sont enrichis sur son dos, Zhongli est un homme curieux car en réalité, il connaît très bien la valeur de l'argent, et il comprend que c'est quelque chose de difficile à gagner. Cependant, il semble ne pas concevoir que le terme de « pauvreté » puisse s'appliquer à lui.

    En d'autres termes, Zhong Li ne peut pas s'imaginer sans argent.

    Comment a-t-il fait pour survivre sans mourir de faim ?

=== "Histoire 3"

    Mourir de faim ? Inconcevable pour Zhongli.

    Perdre ou gagner de l'argent n'a guère d'importance pour lui. Son cœur est dévoué aux Sept Nations.

    Et pour ce qui est des finances... Zhongli en est l'incarnation. Et oui ! Il est le Souverain de la Roche,

    l'Archon Géo, le dirigeant de Liyue, alias Morax. C'est son nom que porte la monnaie qui circule en Teyvat.

    Lorsque la nuit tombe, et que Liyue, grouillante de vie, s'endort, parfois l'Archon monte au sommet de la montagne et contemple la ville qu'il a créée de ses mains.

    Les habitants de Liyue se réfèrent au Souverain de la Roche sous diverses noms.

    Lorsqu'il écrit le code des lois de Liyue, le peuple le baptisa Dieu des contrats.

    Lorsqu'il frappa le premier mora, devenu la base du commerce, les marchands le nommèrent Dieu des affaires.

    Étant le plus ancien des Sept Archons, Zhongli a été le témoin de milliers d'années d'histoire, c'est pourquoi les historiens l'ont proclamé Dieu de l'histoire.

    Il y a des milliers d'années, lorsque les premiers colons de Liyue ont allumé leur premier feu sur un poële en pierre et cuisiné leur propre nourriture, ils ont commencé à l'appeler Dieu des fourneaux.

    Les étrangers l'appellent Morax, mais les habitants de Liyue préfèrent l'appeler le Souverain de la Roche.

    En outre, les enfants et les amateurs de théâtre apprécient particulièrement son titre de Dieu du combat pour avoir vaincu les dieux maléfiques sur les champs de bataille.

    Les délices que le Souverain de la Roche aurait découverts en se perdant dans les rues, les plaques gravées de son écriture, un opéra célèbre dans lequel il aurait lui-même joué le rôle d'un guerrier...

    De nombreuses histoires et contes de Liyue sont, lorsqu'ils sont étudiés de près, en rapport avec lui.

    Et les habitants de Liyue sont incroyablement fiers que leur Archon vive ainsi parmi eux.

=== "Histoire 4"

    En tant que fondateur du Port de Liyue, Morax accorde une importance particulière à la notion de « contrat ».

    Du simple accord commercial entre marchands à l'ancien code de lois qu'il a lui-même rédigé, il n'y a pas un aspect de la vie de la ville qui ne soit régi par des contrats.

    Pour les commerçants, ils sont la garantie d'une transaction équitable et sûre, que ce soit pour les délais, les factures, les destinations d'expédition... Ce n'est qu'en étant encadré de façon stricte que le commerce peut être florissant, ce qui est la pierre angulaire du Port de Liyue.

    C'est pourquoi les contrevenants à ces lois sont systématiquement punis, non seulement pour faire respecter les décisions divines de Morax, mais aussi pour permettre à Liyue de maintenir sa prospérité.

    Pendant des millénaires, les Sept Étoiles se sont consacrées de génération en génération à interpréter les lois et à créer toutes sortes de clauses supplémentaires pour empêcher toute échappatoire juridique. S'il y a quelque chose qui n'est pas écrit, il est considéré comme « autorisé par la loi » jusqu'à ce que les Sept Étoiles apportent l'amendement correspondant.

    Dans ce jeu du chat et de la souris, le livre regroupant ces amendements a fini par atteindre l'imposante épaisseur de 279 pages.

    La personne actuellement responsable de la maintenance de ce livre, la Megrez Ningguang, est appelée le « Tailleur de Liyue », pour sa rapidité et sa précision.

    Cependant, aussi complexes les lois humaines puissent être, il y en a une qui se tient au-dessus de toutes les autres aux yeux du Souverain de la Roche.

    « Celui qui renie sa parole subira le châtiment de la Roche. »

=== "Histoire 5"

    En tant qu'aîné des Sept, le Souverain de la Roche a vécu longtemps.

    Il se souvient encore de l'époque où la guerre des Archons prit fin et où les sept derniers Archons prirent chacun leurs trônes respectifs en tant que « dieux ». Bien qu'ils étaient parfois en désaccord et résidaient à des milliers de kilomètres les uns des autres, ils avaient tous le devoir de guider les humains.

    Au fil du temps, de nombreux titres des Sept changèrent de mains. Parmi les Sept Archons originaux, il n'en reste désormais que deux : le Souverain de la Roche et l'Archon Anémo.

    L'Archon Anémo, Barbatos, est le deuxième plus ancien Archon.

    Il y a deux mille ans, lorsque Barbatos arriva à Liyue, le Souverain de la Roche pensa qu'il rencontrait une terrible crise, nécessitant son aide.

    Ainsi, lorsque Barbatos descendit dans une rafale de vent, l'Archon Géo s'était déjà préparé à recevoir cette divinité voisine et à lui prêter l'aide qu'il pouvait.

    Cependant, l'Archon Anémo lui tendit une bouteille de vin.

    « Voici du vin de Mondstadt. On y goûte ? »

    Abandonner son poste pour apporter une simple bouteille de vin, quelle audace !

    Pourtant, l'Archon Anémo ne cessa de revenir, d'explorer le Port de Liyue, et de poser toutes sortes de questions étranges. Les questions de l'Archon Anémo affluaient comme son vin.

    Dès lors, les Sept organisèrent régulièrement des rassemblements à Liyue.

    Le Souverain de la Roche se souvient encore du goût du vin qu'il y goûtait.

    Le monde a beaucoup changé depuis, et tout ce qui était autrefois familier s'est évanoui dans la mémoire.

    Les sept sièges ont changé encore et encore, jusqu'à ce que cinq des Sept ne viennent plus à la table.

    Les jeunes Archons oubliaient leur devoir.

    Même la roche la plus dure s'érode après trois mille ans.

    Barbatos lui-même finit par ne plus se rendre à Liyue.

    Un jour, le Souverain de la Roche se promenait dans le Port de Liyue et entendit un marchand dire à l'un de ses subordonnés :

    « Vous avez terminé toutes vos tâches, vous pouvez vous reposer. »

    Morax resta perdu dans ses pensées au milieu de la foule pendant un moment.

    « Et moi ? Ai-je accompli... mon devoir ? »

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de topaze prithiva x1 - Cœur de lapis x3 - Bave de Blob x3
| 40 | - Moras 40 000 - Fragment de topaze prithiva x3 - Pilier de basalte x2 - Cœur de lapis x10 - Bave de Blob x15 |
| 50 | - Moras 60 000 - Fragment de topaze prithiva x6 - Pilier de basalte x4 - Cœur de lapis x20 - Mucus de Blob x12 |
| 60 | - Moras 80 000 - Morceau de topaze prithiva x3 - Pilier de basalte x8 - Cœur de lapis x30 - Mucus de Blob x18
| 70 | - Moras 100 000 - Morceau de topaze prithiva x6 - Pilier de basalte x12 - Cœur de lapis x45 - Essence de Blob x12 |
| 80 | - Moras 120 000 - Pierre de topaze prithiva x6 - Pilier de basalte x20 - Cœur de lapis x60 - Essence de Blob x24 |

## Aptitudes
---

|![aptitude 1](imageeee/aptitude/aptitude_1_zhongli.png) Pluie de pierres | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 6 coups de lance. |
| Attaque chargée : | Charge vers l'avant, faisant pleuvoir des lances de roche sur sa route. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageeee/aptitude/aptitude_2_zhongli.png) Dominus Lapidis | Aptitude de combat |
|---------|-------------|
| | Le pouvoir Géo est présent dans les montagnes, sur la terre comme dans les profondeurs, et jusque dans les fissures rocailleuses. Mais rares sont ceux capables de maîtriser une telle puissance. |
| Appui simple | Zhongli matérialise une Dorsale rocheuse qui inflige des DGT Géo de zone. La Dorsale entre en résonance avec les autres constructions Géo présentes. L'amas rocheux libère l'écho de la pierre originelle de façon intermittente, infligeant des DGT Géo aux ennemis à proximité. La Dorsale rocheuse est une construction Géo qui bloque les attaques et peut être escaladée. Il ne peut exister qu'une seule Dorsale à la fois. |
| Appui long | Zhongli fait exploser l'élément Géo contenu dans les environs, entraînant les effets suivants : Un bouclier de jade capable d'absorber une quantité de DGT proportionnelle aux PV max de Zhongli est généré. Le bouclier est efficace à 250% contre les DGT Géo. Inflige des DGT Géo de zone ; Absorbe l'élément Géo des cibles proches affectées par l'élément Géo (jusqu'à 2 cibles max). Cet effet n'inflige pas de DGT. Un vrai maître de la roche doit savoir sculpter le jade et l'or. |

|![aptitude 3](imageeee/aptitude/aptitude_3_zhongli.png) Chute de météore | Aptitude de combat |
|---------|-------------|
| | Zhongli fait tomber des cieux un gigantesque météore, qui inflige d'importants DGT Géo à l'impact et pétrifie les ennemis. |
| Pétrification | Les ennemis pétrifiés sont immobilisés. Que le météore qui transperce la voûte illumine les ténèbres de ce monde. |

|![aptitude 4](imageeee/aptitude/aptitude_4_zhongli.png) Ondes résonnantes | Aptitude passive |
|---------|-------------|
| | Chaque attaque touchant le bouclier de jade génère les effets suivants : Augmente la force du bouclier de 5% ; Cet effet peut être cumulé 5 fois maximum et dure jusqu'à la fin du bouclier. |

|![aptitude 5](imageeee/aptitude/aptitude_5_zhongli.png) Domination de la Terre | Aptitude passive |
|---------|-------------|
| | Chute de météore inflige un bonus de DGT équivalent à 33% des PV max de Zhongli. |

|![aptitude 6](imageeee/aptitude/aptitude_6_zhongli.png) Cristal du destin | Aptitude passive |
|---------|-------------|
| | Lorsque vous forgez une arme d'hast, 15% du minerai utilisé vous sera rendu. |

## Constellations
---

|![constellation 1](imageeee/constel/constelation_1_zhongli.png) Roche, le squelette de la terre | Constellation Niv.1 |
|---------|-------------|
| | 2 Dorsales de Dominus Lapidis peuvent exister en même temps. |

|![constellation 2](imageeee/constel/constelation_2_zhongli.png) Pierre, le berceau du jade | Constellation Niv.2 |
|---------|-------------|
| | Les personnages déployés à proximité sont protégés par un bouclier de jade lors de Chute de météore. |

|![constellation 3](imageeee/constel/constelation_3_zhongli.png) Jade, brillant à travers les ténèbres | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Dominus Lapidis +3. |
| | Niveau max : 15

|![constellation 4](imageeee/constel/constelation_4_zhongli.png) Topaze, incassable et intrépide | Constellation Niv.4 |
|---------|-------------|
| | Augmente la zone d'effet de Chute de météore de 20%. Prolonge de plus la durée de la pétrification de 2 s. |

|![constellation 6](imageeee/constel/constelation_5_zhongli.png) Lazulite, le héraut de l'ordre divin | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Chute de météore +3. |
| | Niveau max : 15 |

|![constellation 6](imageeee/constel/constelation_6_zhongli.png) Or, la générosité du dominateur | Constellation Niv.6 |
|---------|-------------|
| | 40% des DGT subis par le bouclier de jade sont convertis en PV pour le personnage déployé. La quantité de PV restaurés en une fois ne peut pas dépasser 8% des PV max du personnage. |