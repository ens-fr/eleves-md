!!! info inline end "Identité"
    Général de Watatsumi, il a gagné le respect de ses troupes tout en restant humble.
    Ses subordonnés lui accordent toute leur confiance. C'est une personne avec laquelle il est possible de partager ce qu'on a sur le cœur sans honte.

    Un général courageux et loyal de Watatsumi. Il possède un instinct animal de combat inné et une volonté de fer. Même dans les situations les plus désespérées, il parvient à trouver son chemin vers la victoire avec habileté.

![Gorou](https://images6.alphacoders.com/117/thumbbig-1179636.webp)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de topaze prithiva x1 - Perle de corail x3 - Coquille spectrale x3 |
| 40 | - Moras 40 000 - Fragment de topaze prithiva x3 - Cœur perpétuel x2 - Perle de corail x10 - Coquille spectrale x15 |
| 50 | - Moras 60 000 - Fragment de topaze prithiva x6 - Cœur perpétuel x4 - Perle de corail x20 - Cœur spectral x12 |
| 60 | - Moras 80 000 - Morceau de topaze prithiva x3 - Cœur perpétuel x8 - Perle de corail x30 - Cœur spectral x18 |
| 70 | - Moras 100 000 - Morceau de topaze prithiva x6 - Cœur perpétuel x12 - Perle de corail x45 - Noyau spectral x12 |
| 80 | - Moras 120 000 - Pierre de topaze prithiva x6 - Cœur perpétuel x20 - Perle de corail x60 - Noyau spectral x24 |

## Aptitudes
---

|![aptitude 1](imageeee/aptitude/aptitude_1_gorou.png) Empenne aux crocs acérés | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaine jusqu'à 4 tirs consécutifs. |
| Attaque chargée : | Effectue un tir visé plus précis infligeant davantage de DGT. Lors de la visée, la flèche se charge de cristauz de pierre, infligeant de DGT Géo quand elle est complètement chargée.
| Attaque descendante : | Tire une pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de zone à l'impact. |

|![aptitude 2](imageeee/aptitude/aptitude_2_gorou.png) Défense intégrale d'Inazuka | Aptitude de combat |
|---------|-------------|
| | Inflige des DGT Géo de zone et erigé une bannière de générale |
| Bannière de générale | Accorde les effets suivants aux personnages dans la zone d'effet en fonction de la quantité de personnages de type Géo dans l'équipe au moment d'utilisation : Avoir un personnage Géo accorde un effet de pied ferme : bonus de DÉF Avoir deux personnages Géo accorde un effet d'imprenabilité : effet précedent + bonus de RÉS à l'interruption. Avoir trois personnages Géo accorde un effet de fracas : effets précedent + bonus de DGT Géo Gorou ne peut générer qu'une bannière de générale à la fois, et les personnages ne peut pas bénéficier des effets qu d'une une seule bannière de générale à la fois. Lorsque le personnage quitte la zone, les effets continuant pendant 2 s. |
| Appui long | Ajuste la zone où la compétence sera lancée « Adoptez cette formation et nous serons en sécurité, je vous le garantis ! » En tant que général et chef des troupes, le dernière chose que Gorou veut, c'est de voir ses soldats blessés. |

|![aptitude 3](imageeee/aptitude/aptitude_3_gorou.png) Crocs bestiaux: Vers la victoire | Aptitude de combat |
|---------|-------------|
| | Affichant sa bravoure de général, Gorou inflige des DGT Géo de zone et crée un champ de prestige de général pour enhardir ses compagnons. |
| Prestige de général | Les caractéristiques suivantes s'appliquent: Ce champ possède des effets identiques à ceux de la bannière de général créée par le compétenxe élémentaire «Défense intégrale d'Inazuka», accordant des bonus aux personnages déployédans la zone d'effet en fonction du nombre de personnages de type Géo dans l'équipe. Le champ se déplace en suivant le personnage déployé. Déclenche un effrondement cristallin toutes les 1,5 s qui inflige des DGT Géo de zone à un ennemi dans la zone d'effet. Attire 1 fragment élémentaire créé par une réaction de Cristallisation dans la zon d'effet vers l'emplacement du personnages déployé toutes les 1,5 s. Si une bannière de géneral créée par Gorou est présente lors d'utilisation, elle sera détruite. De plus, si Gorou utilise sa compétence élémentaire «Défense intégrale d'Inazuka» alors qu'un prestige de général est actif, il nee crée pas de bannière de général. Si Gorou est vaincu, les effets du prestige de général sont annulés. « Le terrain est à notre avantage ! ». Gorou peut s'appropier la topographie du champ de bataille pour faire pencher labalance en sa faveur. Cette technique lui a permis de traverser de nombreux combats contre une opposition en apparence supérieure. |

|![aptitude 4](imageeee/aptitude/aptitude_4_gorou.png) Nonchalance du vent et de la pluie | Aptitude passive |
|---------|-------------|
| | Pendant 12 s après Crocs bestiaux : Vers la victoire, la DÉF de tous les personnages de l'équipe à proximité augmente de 25%. |

|![aptitude 5](imageeee/aptitude/aptitude_5_gorou.png) Faveur rendue | Aptitude passive |
|---------|-------------|
| | Les DGT des attaques suivantes de Gorou augmentent en fonction de sa DÉF : Les DGT de compétence de Défense intégrale d'Inazuka augmentent à hauteur de 156% a sa DÉF. Les DGt de compétence et les DGT de l'effrondement cristallin de Crocs bestiaux: Vers la victoire augmentent à hauteur de 15,6 % de sa DÉF. |

|![aptitude 6](imageeee/aptitude/aptitude_6_gorou.png) Chien renifleur | Aptitude passive |
|---------|-------------|
| | Affiche l'emplacement des produits d'Inazuma sur la mini carte. |

## Constellations
---

|![constellation 1](imageeee/constel/constelation_1_gorou.png) Vivacité canine : Célérité du vent | Constellation Niv.1 |
|---------|-------------|
| | Lorsqu'un personnage déployé (autre que Gorou) dans la zone d'effet de la bannière de général ou du champ de prestige de général créés par Gorou inflige des DGT Géo à un ennemi, le TdR de Défense intégrale d'Inazuka de Gorou diminue de 2 s. Cet effet peut être déclenché une fois toutes les 10 s.

|![constellation 2](imageeee/constel/constelation_1_gorou.png) Repos canin : Aplomb du temps | Constellation Niv.2 |
|---------|-------------|
| | Lorsqu'un personnage déployé à proximité obtient un fragment élémentaire généré par une réaction de Cristallisation alors qu'un prestige de général est actif, la durée de prestige de général de Gorou augmente de 1 s. Cet effet peut être déclenché une fois toutes les 0,1 s, et sa durée peut être prolongée de 3 s max de cette manière. |

|![constellation 3](imageeee/constel/constelation_3_gorou.png) Griffe canine : Frénésie du feu | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Défense intégrale d'Inazuka +3.|
| | Niveau max : 15 |

|![constellation 4](imageeee/constel/constelation_4_gorou.png) Léchouille canine : Bonté de l'eau | Constellation Niv.4 |
|---------|-------------|
| | Lorsque le prestige de général est en état d'imprenabilité ou de fracas, il restaure également les PV du personnage déployé dans la zone d'effet toutes les 1,5 s d'une valeur équivalent à 50 % de la DÉF de Gorou. |

|![constellation 5](imageeee/constel/constelation_5_gorou.png) Frappe canine : Élan du tonnerre | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Crocs bestiaux: Vers la victoire +3. |
| | Niveau max : 15 |

|![constellation 6](imageeee/constel/constelation_6_gorou.png) Vaillance canine : Fidélité de la terre | Constellation Niv.6 |
|---------|-------------|
| | Pendant 12 s après Défense intégrale d'Inazuka ou Crocs bestiaux: Vers la victoire, les DGT CRIT des DGT Géo de tous les personnages de l'équipe à proximité augmentent en fonction du niveau du champ au moment de l'utilisation de la compétence : Pied ferme: +10 %. Imprenabilité: +20 %. Fracas: +40 %. Cet effet ne se cumule pas, et la dernière augmentation dêclenchée sera la référence pour l'effet. |