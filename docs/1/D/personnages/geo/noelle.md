!!! info inline end "Identité"
    Comme beaucoup d'autres jeunes à Mondstadt, Noëlle rêve de revêtir un jour la fameuse armure des Chevaliers de l'Ordre.
    Bien qu'elle ne soit pas encore un vrai chevalier, Noëlle travaille comme servante à l'Ordre de Favonius, et ne rate pas une occasion d'y apprendre tous les aspects de la chevalerie.
    Croyant fermement que rien n'est impossible à celui qui s'investit dans son travail, Noëlle est convaincue qu'un jour viendra où elle pourra à son tour porter l'armure de l'Ordre, symbole de gloire.

    Les gens de Mondstadt voient en Noëlle la servante une vraie merveille : elle est partout, et s'occupe de tout.

    Ses prouesses lui valent le surnom de « Perle des Servantes ».

![Noelle](https://images6.alphacoders.com/111/thumbbig-1118086.webp)

## Histoire 
---

=== "Histoire 1"

    Aux yeux de nombre de personnes, Noëlle est une perle. Ces dernières disent toutes d'elle la même chose : " Elle est partout. " Mettons par exemple qu'un enfant veuille une assiette lors d'un banquet, mais qu'il soit incapable de se servir dans l'armoire sans faire tomber toute la vaisselle... " Noëlle !!! "

    Il suffit de crier son nom pour que l'omniprésente servante apparaisse de nulle part pour l'aider à prendre une assiette, tout en lui rappelant de ne pas boire de boisson glacée après avoir mangé de la viande grillée ; puis elle en profite pour donner un coup de chiffon sur la vaisselle, et referme l'armoire. Cette capacité à être toujours là quand on a besoin d'elle en étonne plus d'un, et nombreux sont ceux qui y voient une sorte de miracle.

    Le travail d'une servante n'a un réalité rien de miraculeux ; Noëlle applique juste à la lettre un ensemble de règles : " Rien en trop et rien en moins ; chaque chose à sa place, et pas de superflu. " Noëlle ne se considère nullement comme quelqu'un de spéciale, estimant juste qu'elle a parfois tendance à trop réfléchir.

=== "Histoire 2"

     Indisponible

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de topaze prithiva x1 - Tombaie x3 - Masque endommagé x3 |
| 40 | - Moras 40 000 - Fragment de topaze prithiva x3 - Pilier de basalte x2 - Tombaie x10 - Masque endommagé x15 |
| 50 | - Moras 60 000 - Fragment de topaze prithiva x6 - Pilier de basalte x4 - Tombaie x20 - Masque sale x12 |
| 60 | - Moras 80 000 - Morceau de topaze prithiva x3 - Pilier de basalte x8 - Tombaie x30 - Masque sale x18 |
| 70 | - Moras 100 000 - Morceau de topaze prithiva x6 - Pilier de basalte x12 - Tombaie x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre de topaze prithiva x6 - Pilier de basalte x20 - Tombaie x60 - Masque sinistre x24 |

## Aptitudes
---

|![aptitude 1](imageeee/aptitude/aptitude_1_noelle.png) Escrime de Favonius - Servante | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 coup d'épée. |
| Attaque chargée : | Exécute une succession d'attaques tournoyantes ; cette attaque consomme de l'endurance sur la durée. L'attaque chargée se termine sur un coup puissant. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageeee/aptitude/aptitude_2_noelle.png) Armure de cœur | Aptitude de combat |
|---------|-------------|
| | Invoque une armure de roche qui inflige des DGT Géo aux ennemis proches et génère un bouclier absorbant une quantité de DGT proportionnelle à la DÉF de Noëlle. L'armure de roche a les propriétés suivantes : Les attaques normales et chargées de Noëlle ont une chance de restaurer les PV de tous les personnages de l'équipe lorsqu'elles touchent un ennemi. L'armure est efficace à 250% contre les DGT Géo. Les soins reçus sont proportionnels à la DÉF de Noëlle. Noëlle est à la fois une servante fidèle et un chevalier vaillant. |

|![aptitude 3](imageeee/aptitude/aptitude_3_noelle.png) Grand ménage | Aptitude de combat |
|---------|-------------|
| | Concentrant le pouvoir de la roche sur son arme, Noëlle exécute une puissante attaque circulaire qui inflige des DGT Géo aux ennemis proches. Noëlle bénéficie ensuite des effets suivants : Portée d'attaque augmentée. DGT d'attaque convertis en DGT Géo ne pouvant pas être enchantés. Bonus d'ATQ proportionnel à la DÉF de Noëlle. Parfois, la poussière n'est pas la seule chose qu'il faut balayer… |

|![aptitude 4](imageeee/aptitude/aptitude_4_noelle.png) Dévotion ultime | Aptitude passive |
|---------|-------------|
| | Lorsque Noëlle est en réserve, l'effet suivant se déclenche automatiquement si les PV du personnage déployé tombent en dessous de 30% : Confère un bouclier au personnage déployé pouvant absorber des DGT équivalent à 400% de la DÉF de Noëlle pendant 20 s. Le bouclier est efficace à 250% contre les DGT Géo. Le bouclier peut être déclenché une fois toutes les 60 s. |

|![aptitude 5](imageeee/aptitude/aptitude_5_noelle.png) Clair et net | Aptitude passive |
|---------|-------------|
| | Le TdR d'Armure de cœur est réduit de 1 s tous les 4 coups d'attaques normales et chargées. Toucher simultanément plusieurs ennemis compte comme un seul coup. |

|![aptitude 6](imageeee/aptitude/aptitude_6_noelle.png) À votre service | Aptitude passive |
|---------|-------------|
| | 12% de chance d'obtenir un plat supplémentaire lorsque vous cuisinez un plat qui renforce la DÉF. |

## Constellations
---

|![constellation 1](imageeee/constel/constelation_1_noelle.png) J'assure vos arrières | Constellation Niv.1 |
|---------|-------------|
| | L'effet de restauration des PV d'Armure de cœur a 100% de chances d'être déclenché lorsque Grand ménage et Armure de cœur sont actifs en même temps. |

|![constellation 2](imageeee/constel/constelation_2_noelle.png) Servante de tourbillon | Constellation Niv.2 |
|---------|-------------|
| | Les attaques chargées de Noëlle consomment 20% d'endurance en moins et infligent 15% de DGT supplémentaires. |

|![constellation 3](imageeee/constel/constelation_3_noelle.png) La servante ne sera jamais blessée | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Armure de cœur +3. |
| | Niveau max : 15 |

|![constellation 4](imageeee/constel/constelation_4_noelle.png) Je vais nettoyer après | Constellation Niv.4 |
|---------|-------------|
| | Armure de cœur inflige une quantité de DGT Géo équivalent à 400% de l'ATQ aux ennemis proches lorsqu'elle prend fin ou que l'armure est détruite. |

|![constellation 5](imageeee/constel/constelation_5_noelle.png) Maîtresse de ménage | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Grand ménage +3. |
| | Niveau max : 15 |

|![constellation 6](imageeee/constel/constelation_6_noelle.png) Aucune poussière | Constellation Niv.6 |
|---------|-------------|
| | Grand ménage augmente la DÉF de Noëlle de 50%. De plus, chaque ennemi vaincu pendant que la compétence est active prolonge sa durée d'1 s (10 s max). |