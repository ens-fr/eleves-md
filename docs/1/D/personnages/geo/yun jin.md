!!! info inline end "Identité"
    Yun Jin (Chinois: 云堇 Yún Jǐn) est un personnage Géo jouable dans Genshin Impact.

    L'imprésario actuel de la troupe d'opéra Yun-Han, une talentueuse chanteuse d'opéra et une dramaturge très célèbre à Liyue. Son style unique, doux et élégant, lui ressemble beaucoup. 

![Yun Jin](https://images8.alphacoders.com/120/thumbbig-1200177.webp)

## Histoire
---

...

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de topaze prithiva x1 - Lys verni x3 - Masque endommagé x3 |
| 40 | - Moras 40 000 - Fragment de topaze prithiva x3 - Régalia de faille x2 - Lys verni x10 - Masque endommagé x15 |
| 50 | - Moras 60 000 - Fragment de topaze prithiva x6 - Régalia de faille x4 - Lys verni x20 - Masque sale x12 |
| 60 | - Moras 80 000 - Morceau de topaze prithiva x3 - Régalia de faille x8 - Lys verni x30 - Masque sale x18 |
| 70 | - Moras 100 000 - Morceau de topaze prithiva x6 - Régalia de faille x12 - Lys verni x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre de topaze prithiva x6 - Régalia de faille x20 - Lys verni x60 - Masque sinistre x24 |

## Aptitudes
---

|![aptitude 1](imageeee/aptitude/aptitude_1_yunjin.png) Caresse des nuages | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups de lance. |
| Attaque chargée : | Consomme de l'endurance pour charger en ligne droite et inflige des DGT aux ennemis sur la route. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageeee/aptitude/aptitude_2_yunjin.png) Épanouissement ouvert | Aptitude de combat |
|---------|-------------|
| | Bien que maître Yun ne fasse que jouer la comédie lors de ses combats sure scène, ses talents avec une lance sont bien réels. |
| Appui simple | Brandit sa lance pour caresser les nuages, infligeant des DGT Géo. |
| Appui long | Adopte la posture de l'épanouissement ouvert pour se charger, formant ainsi un bouclier est fonction des PV max de Yun Jin, it est efficace à 150 % contre les DGT élémentaires et physiques. Le bouclier dure jusqu'à ce qu'elle finisse de libérer sa compétence élémentaire. Lorsque vous relâchez la compétence, que la durée prend fin ou que le bouclier est détruit, Yun Jiin libère l'énergie stockée sous forme d'attaque infligeant des DGT Géo. En fonction du temps passé à charger, une attaque de 1e ou 2e charge sera déclenchée. Pour un unique moment glorieux sur scène, une décennie de préparaation en coulisse... |

|![aptitude 3](imageeee/aptitude/aptitude_3_yunjin.png) Bannière de rupture des falaises | Aptitude de combat |
|---------|-------------|
| | Inflige des DGT Géo de zone et accorde une formation de bannière de nuagerie à tous les personnages de l'équipe à proximité. Formation de bannière de nuagerie Lorsque le personnage inflige des DGT d'attaque normale à un ennemi, les DGT infligés augmentent en fonction de la DEF actuelle de Yun Jin Les effets de la formation de bannière de nuagerie disparaissent à la fin de sa durée ou après avoir activé un certain nombre de cumuls. Lorsqu'une attaque normale touche plusieurs ennemis en même temps, les cumuls sont consommés selon le nombre d'ennemis touchés. Les cumuls de formation de banniére de nuagerie sont calculés indépendamment pour tous les personnages de l'équipe. « Alors que les tambours résonnent et que les épées et les hallebardes s'entrechoquent, la doyenne se transforme en un puissant général. » |

|![aptitude 4](imageeee/aptitude/aptitude_4_yunjin.png) Fidèle à soi-même | Aptitude passive |
|---------|-------------|
| | Lorsque Yun Jin utilise Épanouissement ouvert au moment exact où elle est attaquée, elle déclenche sa 2e charge. |

|![aptitude 5](imageeee/aptitude/aptitude_5_yunjin.png) Rejet des conventions | Aptitude passive |
|---------|-------------|
| | Lorsque l'équipe comprend des personnages de 1/2/3/4 types élémentaires, le bonus de DGT d'attaque normale accordé par une formation de bannière de nuagerie augmente davantage d'une valeur équivalant à 2,5 %/5 %/7,5 %/11,5 % de la DÉF de Yun Jin. |

|![aptitude 6](imageeee/aptitude/aptitude_6_yunjin.png) Nutrition optimisée | Aptitude passive |
|---------|-------------|
|  | 12 % de chances d'obtenir un plat supplémentaire lorsque vous cuisinez un plat parfait qui a des effets sur l'exploration. |

## Constellations
---

|![constellation 1](imageeee/constel/constelation_1_yunjin.png) Galop vaudevillesque | Constellation Niv.1 |
|---------|-------------|
| | Réduit le TdR d'Épanouissement ouvert de 18% |

|![constellation 2](imageeee/constel/constelation_2_yunjin.png) Myriade d'accessoires | Constellation Niv.2 |
|---------|-------------|
| | Aprés avoir utilisé Bannière de rupture des falaises, les DGT des attaques normales de tous les personnages de l'équipe á proximité augmentent de 15 % pendant 12 s. |

|![constellation 3](imageeee/constel/constelation_3_yunjin.png) Drapeau maritime | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Bannière de rupture des falaises +3. |
| | Niveau max : 15 |

|![constellation 4](imageeee/constel/constelation_4_yunjin.png) Le tranchant d'une fleur | Constellation Niv.4 |
|---------|-------------|
| | Lorsque Yun Jin déclenche une réaction de Cristallisation, sa DÊF augmente de 20 % pendant 12 s. |

|![constellation 5](imageeee/constel/constelation_5_yunjin.png) Célébrité à travers les terres | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Épanouissement ouvert +3. |
| | Niveau max : 15 |

|![constellation 6](imageeee/constel/constelation_6_yunjin.png) Harmonie esthétique | Constellation Niv.6 |
|---------|-------------|
| | Lorsqu'un personnage est affecté par une formation de bannière de nuagerie, la VIT d'attaque de ses attaques norales augmente de 12 %. |