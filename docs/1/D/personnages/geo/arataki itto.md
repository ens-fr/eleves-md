
!!! info inline end "Identité"
    Arataki Itto (Japonais: 荒あら瀧たき一いっ斗と Arataki Itto) est un personnage Géo jouable dans Genshin Impact.

    Descendant d'Oni, c'est un homme intrépide au cœur vaillant et à l'esprit ardent.
    Il est aussi rapide que le vent et puissant que l'éclair. 

![Arataki Itto](https://images8.alphacoders.com/119/thumbbig-1198725.webp)

## Histoire
---

...

## Elevation
---


| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de topaze prithiva x1 - Scarabuto x3 - Bave de Blob x3 |
| 40 | - Moras 40 000 - Fragment de topaze prithiva x3 - Régalia de faille x2 - Scarabuto x10 - Bave de Blob x15 |
| 50 | - Moras 60 000 - Fragment de topaze prithiva x6 - Régalia de faille x4 - Scarabuto x20 - Mucus de Blob x12 |
| 60 | - Moras 80 000 - Morceau de topaze prithiva x3 - Régalia de faille x8 - Scarabuto x30 - Mucus de Blob x18 |
| 70 | - Moras 100 000 - Morceau de topaze prithiva x6 - Régalia de faille x12 - Scarabuto x45 - Essence de Blob x12 |
| 80 | - Moras 120 000 - Pierre de topaze prithiva x6 - Régalia de faille x20 - Scarabuto x60 - Essence de Blob x24 |

## Aptitudes
---

|![aptitude 1](imageeee/aptitude/aptitude_1_itto.png) Légende de la baston | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 coups d'épée. Lorsque son 2e et 4e coup touchent un ennemi, Itto gagne respectivement 1 et 2 cumuls de Mégaforce superlative. Itto peut détenir un maximum de 5 cumuls de Mégaforce superlative, et chaque fois que cet effet se déclenche, la durée des cumuls en sa possession est réinitialisée. De plus, l'enchaînement d'attaques normales d'Itto ne sera pas réinitialisé pendant une courte période après Zetsugi anti-démon : Catapultage d'akaushi ou l'initiation d'un sprint.
| Attaque chargée : | Lorsque vous maintenez appuyé pour lancer une attaque chargée tout en possédant des cumuls de Mégaforce superlative, vous pouvez effectuer des kesa giri d'Arataki sans consommer d'endurance. Chaque kesa giri consommera à la place un cumul de Mégaforce superlative. Lorsque le dernier cumul de Mégaforce est consommé, Itto libère une attaque finale très puissante. Si aucun cumul de Mégaforce superlative n'est disponible, Itto consommera de l'endurance pour effectuer une simple entaille de Saichimonji. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageeee/aptitude/aptitude_2_itto.png) Zetsugi anti-démon : Catapultage d'akaushi | Aptitude de combat |
|---------|-------------|
| | Projette Ushi, le jeune taureau akaushi et membre auxiliaire du gang Arataki, pour infliger des DGT Géo ! Lorsqu'Ushi touche un ennemi, il accorde à Arataki Itto 1 cumul de Mégaforce superlative. Ushi reste ensuite sur le terrain et apporte son aide des façons suivantes : Il nargue les ennemis proches et attire leur attention. Il hérite d'un pourcentage des PV max d'Arataki Itto. Lorsqu'il subit des DGT, il accorde à Arataki Itto 1 cumul de Mégaforce superlative. Un cumul peut être obtenu une fois toutes les 2 s de cette manière. Si ses PV sont épuisés ou si la durée arrive à terme, il s'enfuit tout en accordant à Arataki Itto 1 cumul de Mégaforce superlative. |
| Appui long | Ajuste la direction du jet.  Ushi est considéré comme une construction Géo, et Arataki Itto ne peut générer qu'un seul Ushi à la fois. Le nom de cette technique est dérivé d'« Onibudo », un roman écrit par Junkichi. Cependant, l'appeler « Zetsugi anti-oni » constituerait probablement une violation de droits d'auteur… Sans parler du fait qu'Itto lui-même est un Oni… |

|![aptitude 3](imageeee/aptitude/aptitude_3_itto.png) Roi oni maléfique : Avènement d'Itto | Aptitude de combat |
|---------|-------------|
| | Il est temps de montrer la puissance du gang Arataki ! Itto laisse émerger son Roi oni courroucé intérieur pendant un certain temps, utilisant son kanabo de Roi oni au combat. Les caractéristiques suivantes s'appliquent : Les attaques normales, chargées et plongeantes sont imprégnées de DGT Géo ne pouvant pas être enchantés. La VIT d'attaque des attaques normales d'Itto augmente, et son ATQ sera augmentée en fonction de sa DÉF. Lorsque le 1er ou 3e coup de son attaque normale touche un ennemi, Arataki Itto obtient 1 cumul de Mégaforce superlative. Les RÉS élémentaires et physique d'Arataki Itto diminuent de 20 %. Les effets du Roi oni courroucé prennent fin lorsqu'Arataki Itto quitte le champ de bataille. C'est la meilleure super technique ultime d'Itto… Enfin, d'après lui… Il est inutile pour Arataki Itto d'imiter qui que ce soit d'autre pour devenir plus fort, car il est sans conteste l'Oni le plus fort et le plus terrible de tout le pays. En gros, Itto ne prend exemple que sur lui-même ! Shouta trouve que c'est super cool. |

|![aptitude 4](imageeee/aptitude/aptitude_4_itto.png) Arataki au top | Aptitude passive |
|---------|-------------|
| | Lorsqu'Arataki Itto effectue une série de kesa giri d'Arataki, il obtient les effets suivants : Chaque frappe augmente la VIT d'attaque de la prochaine de 10 %. La VIT d'attaque peut être augmentée de 30 % max de cette manière. Sa RÉS à l'interruption augmente. Ces effets prennent fin lorsqu'Arataki Itto arrête d'utiliser cette série d'attaques. |

|![aptitude 5](imageeee/aptitude/aptitude_5_itto.png) Lignée de l'Oni cramoisi | Aptitude passive |
|---------|-------------|
| | Les DGT infligés par les kesi giri d'Arataki augmentent d'une valeur équivalent à 35 % de la DÉF d'Arataki Itto. |

|![aptitude 6](imageeee/aptitude/aptitude_6_itto.png) Castor au rapport | Aptitude passive |
|---------|-------------|
| | Lorsqu'un personnage de l'équipe utilise des attaques pour obtenir du bois d'un arbre, il a 25 % de chances d'obtenir un morceau de bois supplémentaire. |

## Constellations
---

|![constellation 1](imageeee/constel/constelation_1_itto.png) Du calme et on écoute ! | Constellation Niv.1 |
|---------|-------------|
| | Après avoir utilisé Roi oni maléfique : Avènement d'Itto, Arataki Itto gagne 2 cumuls de Mégaforce superlative. Après 1 s, Itto gagne 1 cumul de Mégaforce superlative toutes les 0,5 s pendant 1,5 s. |

|![constellation 2](imageeee/constel/constelation_2_itto.png) Baston générale ! | Constellation Niv.2 |
|---------|-------------|
| | Après avoir utilisé Roi oni maléfique : Avènement d'Itto, chaque personnage de l'équipe de type Géo permet de diminuer le TdR de cette aptitude de 1,5 s et de restaurer l'énergie élémentaire d'Arataki Itto de 6 pts. Le TdR peut être diminué d'un maximum de 4,5 s et l'énergie élémentaire peut être restaurée d'un maximum de 18 pts de cette manière. |

|![constellation 3](imageeee/constel/constelation_3_itto.png) Le taureau par les cornes ! | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Zetsugi anti-démon : Catapultage d'akaushi +3. |
| | Niveau max : 15 |

|![constellation 4](imageeee/constel/constelation_4_itto.png) Au pain sec et à l'eau ! | Constellation Niv.4 |
|---------|-------------|
| | Lorsque l'état de Roi oni courroucé accordé par Roi oni maléfique : Avènement d'Itto prend fin, la DÉF et l'ATQ de tous les personnages de l'équipe à proximité augmentent de 20 % pendant 10 s. |

|![constellation 5](imageeee/constel/constelation_5_itto.png) Une décennie de renommée à Hanamizaka | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Roi oni maléfique : Avènement d'Itto +3. |
| | Niveau max : 15 |

|![constellation 6](imageeee/constel/constelation_6_itto.png) Arataki Itto, présent ! | Constellation Niv.6 |
|---------|-------------|
| | Les DGT CRIT des attaques chargées d'Artaki Itto augmentent de 70 %. De plus, lorsqu'il utilise une série de kesa giri d'Arataki, il a 50 % de chances de ne pas consommer de Mégaforce superlative. |