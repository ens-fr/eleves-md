!!! info inline end "Identité"
     Ningguang est l'une des Sept Étoiles de Liyue, au sein desquelles elle possède le titre de Megrez. Elle est dotée d'une grande éloquence et d'une perspicacité sans pareille. Grâce à ces qualités, Ningguang a su s'imposer comme la meilleure des commerçantes de Liyue.
     Riche commerçante dont l'influence bouleverse le monde des affaires, grande-sœur que les enfants du coin adorent, personnalité mondaine au banquet de la Terrasse Yujing et amatrice de pâtisseries, ses identités multiples en font la personne la plus énigmatique de tout Liyue.

![Ningguang](https://images8.alphacoders.com/121/thumbbig-1210586.webp)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de topaze prithiva x1 - Lys verni x3 - Insigne de nouvelle recrue x3 |
| 40 | - Moras 40 000 - Fragment de topaze prithiva x3 - Pilier de basalte x2 - Lys verni x10 - Insigne de nouvelle recrue x15 |
| 50 | - Moras 60 000 - Fragment de topaze prithiva x6 - Pilier de basalte x4 - Lys verni x20 - Insigne de sergent x12 |
| 60 | - Moras 80 000 - Morceau de topaze prithiva x3 - Pilier de basalte x8 - Lys verni x30 - Insigne de sergent x18 |
| 70 | - Moras 100 000 - Morceau de topaze prithiva x6 - Pilier de basalte x12 - Lys verni x45 - Insigne d'officier x12 |
| 80 | - Moras 120 000 - Pierre de topaze prithiva x6 - Pilier de basalte x20 - Lys verni x60 - Insigne d'officier x24 |

## Aptitudes
---

|![aptitude 1](imageeee/aptitude/aptitude_1_ningguang.png) Géo-dispersion | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Tire des gemmes qui infligent des DGT Géo. Ningguang obtient un Jade d'étoile à chaque fois qu'une gemme touche un ennemi. |
| Attaque chargée : | Tire une gemme géante qui inflige des DGT Géo. Si Ningguang a obtenu des Jades d'étoile, l'attaque chargée les tire tous au même moment, infligeant des DGT supplémentaires. |
| Attaque descendante : | Plonge depuis les airs en concentrant ses pouvoirs Géo, infligeant des DGT aux ennemis sur la route et des DGT Géo de zone à l'impact. |

|![aptitude 2](imageeee/aptitude/aptitude_2_ningguang.png) Paravent d'astrolabe | Aptitude de combat |
|---------|-------------|
| | Ningguang puise dans son opulence pour créer un paravent d'or et d'obsidienne infligeant des DGT Géo de zone. |
| Paravent d'astrolabe | Bloque les projectiles ennemis. Les PV du paravent sont proportionnels aux PV max de Ningguang. Le paravent d'astrolabe est une construction Géo qui peut bloquer certaines attaques mais ne peut pas être escaladée. Il ne peut exister qu'un seul paravent à la fois. Les étoiles sont dispersées dans le ciel nocturne, les pions sont prêts sur l'échiquier divin. Alors, le paravent d'astrolabe commence à s'étendre. |

|![aptitude 3](imageeee/aptitude/aptitude_3_ningguang.png) Éclatement des étoiles | Aptitude de combat |
|---------|-------------|
| | Ningguang fait tournoyer plusieurs jades précieux à tête chercheuse autour d'elle, qu'elle relâche tous en même temps, infligeant ainsi d'importants DGT Géo. Si le paravent d'astrolabe est actif lorsque cette attaque est exécutée, il l'accompagne d'un tir de jade supplémentaire. Éparses sont les ténèbres quand les étoiles sont éclatées. |

|![aptitude 4](imageeee/aptitude/aptitude_4_ningguang.png) Plan de reprise | Aptitude passive |
|---------|-------------|
| | L'attaque chargée de Ningguang ne consomme pas d'endurance tant que celle-ci possède des Jades d'étoile. |

|![aptitude 5](imageeee/aptitude/aptitude_5_ningguang.png) Réserve stratégique | Aptitude passive |
|---------|-------------|
| | Confère pendant 10 s un bonus de DGT Géo de 12% aux personnages qui traversent les Paravent d'astrolabe. |

|![aptitude 6](imageeee/aptitude/aptitude_6_ningguang.png) Corne d'abondance | Aptitude passive |
|---------|-------------|
| | Affiche l'emplacement des filons de minerai (fer, fer blanc, cristal, cristal magique et argétoile) sur la mini-carte. |

## Constellations
---

|![constellation 1](imageeee/constel/constelation_1_ningguang.png) Fragments perforants | Constellation Niv.1 |
|---------|-------------|
| | Les attaques normales qui touchent infligent des DGT de zone. |
        
|![constellation 2](imageeee/constel/constelation_2_ningguang.png) Effet de choc | Constellation Niv.2 |
|---------|-------------|
| | Le TdR de Paravent d'astrolabe est annulé lorsque ce dernier est brisé. Cet effet peut être déclenché une fois toutes les 6 s. |

|![constellation 3](imageeee/constel/constelation_3_ningguang.png) Alignement des étoiles | Constellation Niv.3 |
|---------|-------------|
| |  Niveau de compétence Éclatement des étoiles +3. |
| | Niveau max : 15 |

|![constellation 4](imageeee/constel/constelation_4_ningguang.png) Exquis soit le Jade, éclatant de toutes parts | Constellation Niv.4 |
|---------|-------------|
| | Paravent d'astrolabe agmente la RÉS élémentaire des personnages à proximité de 10%. |

|![constellation 5](imageeee/constel/constelation_5_ningguang.png) Invincible soit le Paravent d'astrolabe | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Paravent d'astrolabe +3. |
| | Niveau max : 15 |

|![constellation 6](imageeee/constel/constelation_6_ningguang.png) Grandeur soit les Sept Étoiles | Constellation Niv.6 |
|---------|-------------|
| | L'activation d'Éclatement des étoiles confère 7 Jades d'étoile à Ningguang. |