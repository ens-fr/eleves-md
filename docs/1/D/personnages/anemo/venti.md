!!! info inline end "Identité"
        Un barde inconnu qui chante des vieux poèmes oubliés certaines fois, et des poèmes que personne n'avait jamais entendus auparavant d'autres fois.
        Venti aime les pommes et les lieux animés. Cependant, il a horreur du fromage et des choses gluantes.
        Ses pouvoirs Anémo se manifestent souvent sous la forme de plumes, car il apprécie beaucoup les objets légers et flottants.

![venti](https://images8.alphacoders.com/112/thumbbig-1121700.webp)

## Histoire

=== "Histoire 1"

    - À son arrivée à Mondstadt, ses revenus étaient loin d'égaler ceux des bardes déjà établis ; mais à chaque fois que quelques moras se retrouvaient dans son chapeau posé au sol, il ne fallait pas attendre longtemps pour qu'il fasse le tour des tavernes de la cité.
    Malheureusement pour lui, ses traits enfantins le faisaient passer aux yeux des tenanciers pour un mineur, et l'achat d'alcool se révéla impossible.
    - La première fois qu'on refusa de le servir, on put l'entendre grommeler : "Je suis sûr que cette règle idiote n'existait pas la dernière fois que j'étais dans le coin..." Une fois qu'il réalisa qu'aucune taverne en ville ne lui servirait autre chose que des boissons non alcoolisées, il se trouva dans l'obligation de modifier quelque peu son approche.
    - Sa nouvelle stratégie consiste à boire en jouant ; autrement dit, lorsque les badauds appréciaient sa musique, il leur demandait paiement non pas en moras, mais en alcool acheté dans quelque taverne proche.
    - Cette technique unique lui permit enfin de se sentir comme un poisson dans l'eau.
    Seul problème, son allergie aux chats. Lorsqu'un chat l'approchait, il ne pouvait se retenir d'éternuer. Pour peu qu'il ait à ce moment une coupe à la bouche... Vous imaginez le résultat.
    - Aussi Venti s'est-il établi une règle d'or : aucun chat ne doit être présent quand il se produit. Ce qui est malheureusement plus facile à dire qu'à faire, les chats locaux semblant avoir un faible à son égard.

=== "Histoire 2"

        Un gigantesque chêne se trouve en plein centre de Ventlevé. On prétend qu'il germa mille ans plus tôt lorsque Vennessa monta au ciel.
        Ces derniers mois, les gens se reposant à son pied ont pu entendre par moments un jeune homme chanter la gloire de Barbatos, l'Archon Anémo.
        Contrairement aux autres dieux continuant à veiller sur d'autres territoires, Barbatos a depuis longtemps quitté Mondstadt ; ne subsiste comme trace de son passage que la Statue des Sept - et même là, la ressemblance n'est que très vague. En revanche, ses actions ont été archivées, chantées et transmises par les bardes.
        Mais les ballades de Venti diffèrent quelque peu des canons classique, et incluent des aventures étranges  ou des actions cadrant mal les histoires habituelles - comme par exemple le vol du sceptre de l'Archon Cryo pour le remplacer par le gourdin d'un Brutocollinus...
        Les adorateurs de Barbatos en prennent facilement ombrage ; mais confronté à leurs critiques, Venti n'a pas l'air d'afficher une once de remords.
        _"Comment pourriez-vous savoir si c'est faux ?"_
        Il n'a pas tort : même la plus dévote des nonnes serait bien incapable de connaître tous les détails de la vie de Barbatos, il y a de cela plus de mille ans.
        Seul Venti sait ce qu'il en ait, et le cache derrière un sourire énigmatique.
        Pour une simple raison : les exploits inorthodoxes qu'il chante sont inventés de toutes pièces.
        Mais après tout, qui ne sent pas porté par une inspiration débordante après quelques verre ?

=== "Histoire 3"

        Il y a de cela environ 2 600 ans, la Guerre des Archons faisait rage, et le mode n'était pas encore sous la domination des Sept.
        En ces temps-là, la Cité de Mondstadt était prisonnière, maintenue isolée du reste du monde par des vents furieux, qui s'érigeait en une barrière impénétrable même pour le plus téméraire des oiseaux.
        L'incessant assaut des bourrasques avait fini par éroder pierres et sol, faisant souffler dans les ruelles une poussière qui jamais ne retombait.
        Décabarian, le Seigneur des Rafales, aussi appelé "Dieu des tempêtes", dominait le peuple du haut de sa tour, prenant grand plaisir à voir les Mondstadtois soumis entièrement à sa puissance.
        À cette époque, Venti n'était alors qu'une brise parmi les brises, un simple souffle parcourant les terres du nord.
        Lui qui un jour deviendrait Barbatos, le puissant Archon Anémo, n'était à l'origine qu'un faible et agile esprit élémentaire, une brise apportant une touche d'espoir où elle le pouvait.
        Venti vint alors à faire la connaissance dans l'antique Mondsatdt d'un jeune garçon ; ce dernier jouait de la lyre, et rêvait d'écrire un poème comme nul n'en avait jamais lu.
        _"Ah, comme j'aimerais voir l'oiseau voler."_
        De ciel azur toujours il rêvait ; mais les murs d'orage couvraient l'horizon, et sa voix se perdait au milieu des vents hurlants.
        _"Ami, ne me suivras-tu pas ?"_

=== "Histoire 4"

        L'esprit élémentaire qu'était alors Venti récupéra pour le jeune garçon né dans la cité, qui jamais de sa vie n'avait vu d'oiseau, des plumes de faucon.
        Mais peu après éclata à Mondstadt la révolution du peuple demandant sa liberté. Les plumes restèrent avec Venti, l'accompagnant à travers la fumée des canons, pour finalement être témoin à ses côtés de la mort du despote.
        Le tyran, qui avait offert à ses loyaux ministres une ville à l'abri es rigueurs de l'hiver, crut jusqu'à la fin que ceux-ci l'adoraient...
        Quant à Venti, une fois la victoire obtenue, il se dépêcha d'aller retrouver le jeune garçon pour lui remettre les plumes qu'il portait toujours sur lui.
        Hélas, ce dernier n'était plus, ayant succombé à son combat pour la poésie, les cieux d'azur et les oiseaux qu'il rêvait de voir un jour...
        La divinité vaincue, un autre dieu devait naître et prendre sa place, et Venti, investi d'une puissance qu'il pouvait à présent sentir en tout son être, devint Barbatos, l'Archon Anémo.
        La première chose qu'il fit avec ce nouveau pouvoir fut de se donner l'apparence du jeune garçon qui avait péri.
        En effet, seule une forme humaine pourrait lui permettre de jouer la lyre à la manière de ce dernier...
        Et, pinçant les cordes de l'instrument, il souffla sur les glaciers, et sépara les pics montagneux.
        _"Que Mondstadt soit désormais la cité de la liberté, un royaume sans roi. Avec le temps, je sais qu'elle deviendra un hâvre de paix, une capitale où il fera bon vivre."_
        Et c'est ainsi que Mondstadt, renaissant de ses cendres, devint avec le temps la cité que l'on connaît aujourd'hui.

=== "Hsitoire 5"

        Bénie soit Mondstadt, la cité de la liberté ;
        Laissez-moi aujourd'hui vous chanter sa beauté.
        Merci à la caresse du vent de ponan,
        Merci aux fleurs amenées par le printemps.
        Pinsons, canards, lapins, sangliers :
        La faune abonde dans notre cité !
        Le lion d'été les terres parcourt ;
        Qui osera défier sa bravoure ?
        Le jus de la treille coule, noyant les peines,
        Un vrai délice - tous en conviennent.
        Et semblables à l'ivrogne affalé,
        Ici s'étendent monts et vallées.
        Mais peu importe au vent de l'est :
        Ce dernier vole tel l'oiseau leste !
        Mouvant les branches chargées de fruits,
        Son souffle apporte récoltes fournies.
        Le vent du nord dans les forêts,
        Observe les loups, en meute partis.
        Nul ne les voit, et il le sait :
        Car de l'hiver l'homme se méfie.
        Et ainsi donc passent les saisons,
        Au rythme des vents et des chansons.
        Gloire à Mondstadt, gloire à l'Archon !
        Et gloire à moi, le barde fécond,
        Car sans mon aide, pas de chanson !

## Elevation
---
| Niveaux | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de turquoise vayuda x1 - Cécilia x3 - Bave de Blob x3 |
| 40 | - Moras 40 000 - Fragment de turquoise vayuda x3 - Graine d'ouragan x2 - Cécilia x10 - Bave de Blob x15 |
| 50 | - Moras 60 000 - Fragment de turquoise vayuda x6 - Graine d'ouragan x4 - Cécilia x20 - Mucus de Blob x12 |
| 60 | - Moras 80 000 - Morceau de turquoise vayuda x3 - Graine d'ouragan x8 - Cécilia x30 - Mucus de Blob x18 |
| 70 | - Moras 100 000 - Morceau de turquoise vayuda x6 - Graine d'ouragan x12 - Cécilia x45 - Essence de Blob x12 |
| 80 | - Moras 120 000 - Pierre de turquoise vayuda x6 - Graine d'ouragan x20 |

## Aptitudes
---

|![aptitude 1](image/aptitude/aptitude_1_venti.png) Archerie divine | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 6 tirs consécutifs.|
| Attaque chargée : | Effectue un tir visé plus précis infligeant davantage de DGT. |
| | Lors de la visée, la flèche se charge en élément Anémo, infligeant de considérables DGT Anémo quand elle est complètement chargée. |
| Attaque descendante :  | Tire une pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de zone à l'impact. |

|![aptitude 2](image/aptitude/aptitude_2_venti.png) Sonnet des Vents célestes | Aptitude de combat |
|---------|-------------|
| | Le vent porteur de chants et de mélodies vient faire voler les êtres foulant cette terre. |
| Appui simple | Venti invoque une bourrasque sur la position de l'ennemi, qui inflige des DGT Anémo de zone et projette les ennemis en l'air. |
| Appui long | Venti invoque une bourrasque plus puissante sur sa position, qui inflige des DGT Anémo de zone et projette les ennemis proches en l'air. L'appui long permet à Venti de voler dans les airs. Les ennemis projetés en l'air retombent lentement au sol. Aux temps anciens où dieux et hommes se partageaient la terre, nombreux furent les chants qui atteignirent les cieux. Ce sonnet possède une mélodie oubliée depuis longtemps. |

|![aptitude 3](image/aptitude/aptitude_3_venti.png) Ode au Vent | Aptitude de combat |
|---------|-------------|
| | Venti tire une flèche porteuse de la puissance de mille vents et de l'Œil de la tempête. Ce dernier attire les ennemis en son centre, et inflige des DGT Anémo continus. |
| Absorption élémentaire | L'Œil de la tempête absorbe les éléments Hydro, Pyro, Cryo et Électro avec lesquels il entre en contact ; ceux-ci lui confèrent leurs attributs, et infligent un bonus de DGT de l'élément correspondant. |

|![aptitude 4](image/aptitude/aptitude_4_venti.png) Embrasse des vents | Aptitude passive |
|---------|-------------|
| | L'appui long de Sonnet des Vents célestes génère une bourrasque ascendante qui dure 20 s. |

|![aptitude 5](image/aptitude/aptitude_5_venti.png) Œil de la tempête | Aptitude passive |
|---------|-------------|
| | Venti récupère 15 pts d'énergie élémentaire à la fin d'Ode au Vent. En cas d'absorption élémentaire, les personnages de l'élément correspondant récupèrent également 15 pts d'énergie élémentaire. |

|![aptitude 6](image/aptitude/aptitude_6_venti.png) Chevaucheur du vent | Aptitude passive |
|---------|-------------|
| | Réduit la consommation d'endurance de vos personnages dans l'équipe de 20% lors du planage. Ne peut être cumulé avec d'autres aptitudes passives ayant les mêmes effets. |

## Constellations
---

|![constellation 1](image/constel/constelation_1_venti.png) Vent violent fractionné | Constellation Niv.1 |
|---------|-------------|
| | Le tir visé tire 2 flèches supplémentaires, chacune infligeant 33% des DGT de la flèche d'origine. |

|![constellation 2](image/constel/constelation_2_venti.png) Bise de réminiscence | Constellation Niv.2 |
|---------|-------------|
| | Sonnet des Vents célestes réduit la RÉS Anémo et la RÉS physique des ennemis de 12% pendant 10 s. La RÉS Anémo et la RÉS physique des ennemis projetés en l'air par Sonnet des Vents célestes sont réduites de 12% tant qu'ils sont en l'air. |

|![constellation 3](image/constel/constelation_3_venti.png) L'Ode aux mille vents | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Ode au Vent +3. |
| | Niveau max : 15 |

|![constellation 4](image/constel/constelation_4_venti.png) Vent glacial de liberté | Constellation Niv.4 |
|---------|-------------|
| | Venti inflige 25% DGT Anémo supplémentaires pendant 10 s lorsqu'il obtient un orbe ou une particule élémentaire. |

|![constellation 5](image/constel/constelation_5_venti.png) Concerto céleste | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Sonnet des Vents célestes +3. |
| | Niveau max : 15 |

|![constellation 6](image/constel/constelation_6_venti.png) Tempête de résistance | Constellation Niv.6 |
|---------|-------------|
| | La RÉS Anémo des ennemis touchés par Ode au Vent est réduite de 20%. En cas d'absorption élémentaire, leur RÉS à l'élément correspondant est également réduite de 20%. |

## Skin