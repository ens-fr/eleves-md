!!! info inline end "Identité"
     Xiao (Chinois : 魈 Xiāo, "Démon de la Montagne") est un personnage jouable Anémo dans Genshin Impact.

     Un des puissants Adeptes éclairés de Liyue, aussi connu sous le nom de « Yaksha gardien ».
     Bien qu'il ait l'apparence d'un jeune garçon, on retrouve de nombreuses légendes à son sujet dans des ouvrages littéraires vieux de plusieurs milliers d'années.
     Son plat préféré est le tofu aux amandes de l'Auberge Wangshu.
     Il apprécie particulièrement ce mets parce qu'il a le goût d'un beau rêve qu'il a jadis dévoré.

![xiao](https://images7.alphacoders.com/114/thumbbig-1144794.webp)

=== "Histoire 1"

    Contre qui lutte vraiment Xiao ?

=== "Histoire 2"

     ?

=== "Histoire 3"

     En remerciement de la générosité de l'Archon Géo, Xiao s'engagea à porter la responsabilité de protéger Liyue pour les milles années à venir. Les années de servitude sous le joug du dieu maléfique ont vidé Xiao de toute trace d'innocence et de bonté ; il est devenu une machine à tuer hantée par le poids de ses péchés. Combattre est la seule chose qu'il puisse offrir au service des hommes. Mais que peuvent faire les hommes en retour à son égard ? Nul n'a jamais eu l'audace de poser cette question ; même sa silhouette distante dégage suffisamment de puissance pour faire détaler n'importe qui saint d'esprit.Cela dit... Il existe peut-être une chose que pourrait faire celui qui tiendrait à montrer son appréciation. Un agent secret au service des Sept Étoiles lui fournit de l'aide, sous le couvert de l'Auberge Wangshu. Xiao s'y rend s'y rend de temps à autre pour déguster un bol de tofu aux amandes ; et l'expression qu'il prend alors ne trompe guère : il adore ça. Non pas qu'il est préférence pour le goût sucré ; c'est la consistance qu'il trouve irrésistible - elle lui rappelle celle des rêves qu'il dévorait autrefois.

=== "Histoire 4"

     Contre quoi exactement Xiao se débat-il ? Les dirigeants de Liyue savent tout de son combat contre les manifestations corrompues, vestiges des Archons défaits. Mais celui qui poserait la question directement à Xiao obtiendrait une réponse quelque peu différente. Xiao fut autrefois asservi par une déité maléfique, et souffrit sans mesure sous son joug, jusqu'au jour où il fut enfin libéré grâce à l'intervention du Souverain de la Roche. Ses pouvoirs magiques en faisaient autrefois l'un des adeptes les plus puissants, et soumettre démons et monstres à sa volonté est à ses yeux un parcours de santé. Mais la puissance de l'esprit de vengeance habitant les Archons déchus furent tels qu'ils donnèrent naissances à une multitudes de créatures maléfiques., que Xiao massacra sans relâche ; or le fiel finit par corrompre son âme. La seule façon d'éradiquer une telle haine est d'accepter la dette karmique que cela entraîne ; mais l'amplitude du mauvais karma accumulé par Xiao au fil des ans était telle qu'elle le dévorait de l'intérieur. Et pourtant, il ne ressent aucune haine. Plus de deux milles années d'existence ont finit par ne laisser de ce mauvais karma qu'un souvenir. Nulle rancœur ne peut perdurer aussi longtemps ; nulle dette n'est qi grande qu'elle ne puisse être repayée sur autant d'années. Xiao a toujours été, et demeurera à jamais, seul. Et le combat qu'il mène est avant tout contre lui-même.

=== "Histoire 5"

     Contre quoi lutte vraiment Xiao ? Le voyageur comprend que Xiao combat les ténèbres qui menacent Liyue depuis des millénaires et se bat pour défendre Liyue. Mais qui le défendra, lui ? Un jour, Xiao avait utilisé toutes ses forces lors d'un combat qui dura du crépuscule à l'aube, émergeant victorieux d'un cheveu. Les champs de roseau avaient été tranchés dans la violente bataille. Récupérant sa lance d'où il l'avait plantée, Xiao se mit en route pour le chemin du retour. Nul ne savait sa destination, il quitté juste le champs de bataille, voilà tout. Épuisé depuis longtemps, Xiao sentit la colère divine prendre possession de son corps. Une haine sans fin se mit à envahir ses sens, le faisant vaciller dans les champs de roseaux. Mais c'est aussi à cet instant que la douleur quitta son corps sans sommation. Ce n'était pas le fait de Xiao, le son d'une flûte venait de le délivrer. Le son claire et mélodieux de la flûte avait traversé les montagnes et les rivières, emporté par le vent jusqu'à l'endroit où Xiao gisait. Et il continua de résonner jusqu'à ce que les premières lueurs de l'aube amenèrent avec elles le vol des oiseaux apeurés au loin. Le son protégea Xiao, domptant ses sens enflammés et lui permettant pour un instant de retrouver quelque paix. Qui jouait cette musique ? Xiao était curieux de l'apprendre mais ne s'en enquit pas. Il avait déjà une idée de la réponse. La dernière personne à avoir pu l'aider n'était autre que l'un des Sept. Cette personne ne pouvait donc être que...

## Elevation
---
| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de turquoise vayuda x1 - Qingxin x3  - Bave de Blob x3 |
| 40 | - Moras 40 000 - Fragment de turquoise vayuda x3 - Jade juvénile x2 - Qingxin x10 - Bave de Blob x15 |
| 50 | - Moras 60 000 - Fragment de turquoise vayuda x6 - Jade juvénile x4 - Qingxin x20 - Mucus de Blob x12 |
| 60 | - Moras 80 000  - Morceau de turquoise vayuda x3 - Jade juvénile x8 - Qingxin x30 - Mucus de Blob x18 |
| 70 | - Moras 100 000 - Morceau de turquoise vayuda x6 - Jade juvénile x12 - Qingxin x45 - Essence de Blob x12 |
| 80 | - Moras 120 000 - Pierre de turquoise vayuda x6 - Jade juvénile x20 - Qigxin x 60 |

## Aptitudes
---

|![aptitude 1](image/aptitude/aptitude_1_xiao.png) Coup de tourbillon | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 6 coups de lance. |
| Attaque chargée : | Consomme de l'endurance pour exécuter une attaque ascendante. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis en chemin et des DGT de zone à l'impact. Xiao ne suvit pas de DGT lorsqu'il exécute une attaque descendante. |

|![aptitude 2](image/aptitude/aptitude_2_xiao.png) Cycle du vent lemniscatique | Aptitude de combat |
|---------|-------------|
| | Xiao se précipite vers l'avant, infligeant des DGT Anémo aux ennemis sur sa route. Fonctionne aussi dans les airs. Xiao commence avec 2 charges. La légende d'un lointain pays raconte qu'un jour, le monde périra entre deux roues à aubes. La maîtrise totale de la lance permet à Xiao d'écraser ses ennemis, à l'image des roues légendaires écrasant le monde. |

|![aptitude 3](image/aptitude/aptitude_3_xiao.png) Fléau du mal | Aptitude de combat |
|---------|-------------|
| | Xiao arbore le Masque du Yaksha, qui, il y a bien longtemps, fit trembler dieux et démons. |
| Masque du Yaksha | Améliore considérablement la capacité de saut de Xiao. Augmente la portée et les DGT d'attaque. Les DGT d'attaque sont convertis en DGT Anémo ne pouvant pas être enchantés. Cet effet consomme des PV de façon continue. L'état prend fin lorsque Xiao est vaincu ou quitte le champ de bataille. En revêtant le Masque du Yaksha, Xiao prend la forme terrifiante du Gardien Yaksha Dompteur de démon. Mais un tel pouvoir a un prix ; aussi l'appelle-t-on : le Fléau du mal. |

|![aptitude 4](image/aptitude/aptitude_4_xiao.png) Conquérant du Mal : Dompteur de Démons | Aptitude passive |
|---------|-------------|
| | Alors que sous les effets de Bane of All Evil, tous les DMG traités par Xiao augmentent de 5%. DMG augmente de 5% supplémentaires pour chaque 3s la capacité persiste. Le bonus DMG maximum est de 25%. |

|![aptitude 5](image/aptitude/aptitude_5_xiao.png) Dissolution d'éon : Chute des cieux | Aptitude passive |
|---------|-------------|
| | Cycle du vent lemniscatique inflige 15% de DGT en plus lorsqu'il est relancé dans les 7 s. Cet effet dure 7 s et peut être cumulé 3 fois au maximum. Le délai est réinitialisé à chaque nouveau cumul. |

|![aptitude 6](image/aptitude/aptitude_6_xiao.png) Transcendance : Contrôleur de gravité | Aptitude passive |
|---------|-------------|
| | Réduit la consommation d'endurance des personnages de l'équipe de 20% lors de l'escalade. Ne peut pas être cumulé avec d'autres aptitudes passives ayant les mêmes effets. |

## Constellations
---

|![constellation 1](image/constel/constelation_1_xiao.png) Dissolution d'éon : Destructeur de mondes | Constellation Niv.1 |
|---------|-------------|
| | Confère une charge supplémentaire pour Cycle du vent lemniscatique. |

|![constellation 2](image/constel/constelation_2_xiao.png) Annihilation d'éon : Fleur du kaléidoscope | Constellation Niv.2 |
|---------|-------------|
| | La recharge d'énergie de Xiao augmente de 25% lorsqu'il fait partie de l'équipe mais n'est pas déployé. |

|![constellation 3](image/constel/constelation_3_xiao.png) Conquérant du Mal : La Déité de la Colère | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Cycle du vent lemniscatique +3 |
| | Niveau max : 15 |

|![constellation 4](image/constel/constelation_4_xiao.png) Transcendance : Extinction de la souffrance | Constellation Niv.4 |
|---------|-------------|
| | Xiao bénéficie d'un bonus de DÉF de 100% quand ses PV tombent en dessous de 50%. |

|![constellation 5](image/constel/constelation_5_xiao.png) Évolution d'éon : Origine de l'ignorance | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Fléau du mal +3. |
| | Niveau max : 15 |

|![constellation 6](image/constel/constelation_6_xiao.png) Dompteur de démons : Gardien Yaksha | Constellation Niv.6 |
|---------|-------------|
| | Lorsque Fléau du mal est actif, les attaques descendantes touchant au moins 2 ennemis confèrent immédiatement une charge de Cycle du vent lemniscatique supplémentaires, qui peut être activée dans la seconde qui suit, même s'il n'est pas entièrement rechargé. |

## Skin