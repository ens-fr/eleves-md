
!!! info inline end "Identité"
      _Entièrement dévouée à sa fonction de grande maîtresse intérimaire de l'Ordre de Favonius, Jean veille sans relâche sur Mondstadt. Bien qu'elle n'était pas la plus douée de ses pairs, cette jeune femme est devenue le pilier de l'Ordre grâce à son travail acharné.
      Elle protègera Mondstadt coûte que coûte face à toutes les menaces, notamment celle que représente Stormterror._

      _Elle est le Chevalier au Pissenlit, loyal et rigoureux, et la Grande Maîtresse suppléante de l'Ordre de Favonius._

![jean](https://images4.alphacoders.com/121/thumbbig-1210594.webp)

### Histoire

=== "Histoire 1"


     - Le clan de Gunnhildr constitue la plus ancienne famille de chevalier de Mondstadt, qui l'aurai protégé _"avant même que ne soit fini le premier poème épique "_, disent les légendes. Mais un tel héritage peut être un lourd poids à supporter. Jean fut élever par sa mère pour devenir le successeur au trône du clan.

      - Elle fut formée dans tout les domaines que se doit de maitriser un chevalier, tels que l'étiquette, la conduite et la discipline, en passant par les bonnes connaissances de l'histoire et des ballades ou encore la maitrise de l'épée et le maintien d'une forme physique optimale. Ce n'est qu'ainsi qu'elle peut se montrer à la hauteur de la devise familiale :" Pour Mondstadt, toujours ".

      - Un bon mot circulait il y a quelques années dans les tavernes de la cité : un membre des Gunnhildr apprend la devise du clan avant de savoir dire _" maman "_.

    - Un jour qu'elle était jeune enfant, Jean, absorbée par son bouquin, " La Brise de la Forêt ", prit une pause pour regarder les autres enfants de son âge jouer avec leurs moulins à vent de l'autre côté de la fenêtre. C'est à ce moment qu'elle sût quelle signification la devise du clan avait à ses yeux.

    - Aujourd'hui encore, lorsqu'elle relève la tête de ses piles de papiers, elle peut voir la nouvelle génération s'amusant de la même manière. La Grande Maîtresse suppléante ne regrette aucune des années passées au service de ce motto. _" La vérité importe seule ; il faut toujours faire de son mieux pour la vérité, toujours. "_

=== "Histoire 2"

      _"Jean est toujours là pour aider " " Si vous avez un problème aller voir Jean. "_

      - A Mondstadt, chevalier comme civils savent qu'ils peuvent compter sur Jean. Qu'il s'agisse d'une altercation dans une ruelle ou d'une dispute de couple, Jean est toujours disposée à aider. Bien que cela ne relève pas toujours de ces responsabilités, à ceux qui lui posent la question, Jean répond toujours : _" Aider la personne dans le besoin tous type de besoin, c'est une raison suffisante pour un chevalier. "_ Elle fait passer son rôle de chevalier avant son rôle de Grande Maîtresse ; à ses yeux aider les autres est au centre de la chevalerie.

      - Les effectifs sous sont ordres sont soumis aux mêmes exigences. Lisa la bibliothécaire lui a un jour suggéré de faire une pause et de " prendre un thé, comme une demoiselle. _" Mais Jean fait passer sont devoir de chevalier avant tout le reste, demoiselle ou pas. "_ On peut compter sur Jean. " Telle est la réputation de Jean à Mondstadt. Son seul regret est qu'il n'y est pas d'avantage de temps dans une journée ; elle ne peut pas aider tout les gens dans le besoin, dut-elle pour cela faire une croix sur son sommeil. Le dévouements et l'engagement requis pour devenir une telle personne sont inimaginables.

=== "Histoire 3"

      - On n'oublie souvent que Jean n'est que Grande Maîtresse suppléante, et qu'elle répond aux ordres du Grand Maître. Cela ne la dérange pas outre-mesure ; sont rôle et son grade au sein de l'Ordre de Favonius n'ont aucune espèce d'influence sur son éthique du travail.

      - Elle doit sa passion, son honnêteté et sa méticulosité à deux choses. D'abord, à son éducation et à son entraînement, qui lui instillèrent l'esprit chevaleresque dès son plus jeune âge. Ensuite aux enseignements de Varka, Chevalier de Borée et Grand Maître de l'Ordre de Favonius, actuellement en mission. Varka le libre penseur a eu sur le développement de Jean une forte et bénéfique influence. _" Grand Maître, prenez votre rôle au sérieux ; Mondstadt dépend de vous. "_ _"Tu es mon commandant en second, m'assister fait partie de tes responsabilités ; de cette manière, je peux me concentrer sur des choses de la plus hautes importance, n'est tu pas d'accord ? "_

      - Varka est le chevalier des frontières et des conquêtes, tandis que Jean est le chevalier de la protection et de la liberté. Jean n'éprouve aucun ressentiment à l'égard de Varka : après tout ; nul doute que la conduite de ce dernier obéit à ses propres raisons. Mais elle doit souvent finir ce que lui commence, et c'est alors à elle de faire les bons choix.

      - Il y a six mois, Varka a lancé une nouvelle expédition de Mondstadt. Une expédition, du Varka tout craché._" Je te laisse en charge; après tout, tu fais déjà mon travail depuis des années. "_ _"Je m'occupe de tout Grand Maître."_ Après avoir regardé l'escouade partir Jean s'est assis près de la fenêtre, absorbée par ses pensées. _" Mondstadt sera encore plus prospère, paisible et chaleureuse à votre retour. "_

=== "Histoire 4"

      - C'est à l'endroit actuel de l'arbre de Ventlevé que le premier Chevalier au Pissenlit arriva au bout de son périple. Il s'agissait de Venessa, la fondatrice de l'Ordre de Favonius, et celle à qui l'on doit le renouveau de Mondstadt. Faisant ses adieux à la cité qu'elle avait juré de protéger, elle laissa derrière elle sa légende ainsi qu'un unique arbrisseau.

      - L'arbrisseau, bercé depuis des siècles par les milles vents, finit par devenir l'imposant arbre d'aujourd'hui. Jean obtint le titre de _" Chevalier au Pissenlit "_ à l'âge de quinze ans. Appelé aussi _" Chevalier du Croc de Lion "_, ce titre est transmis aux meilleurs des chevaliers de génération en génération. Le jour où lui fut remit le titre, Jean se retira des célébrations qui suivirent et marchant tel l'enfant marchant sur les pas de son héros, elle arriva à l'arbre devant lequel elle se tint. Le titre de Chevalier au Pissenlit symbolise l'héritage de résistance et de dévouement laissé par Venessa ; perdue dans ces pensées, Jean se demandait comment elle avait pu être jugée digne d'un tel honneur. Plus d'un siècle avait passé depuis la restauration de Mondstadt, aurait-elle ce qu'il fallait en elle pour continuer à protéger cette ancienne terre de liberté ?

      - Sous son apparence mature se cachait le cœur une jeune fille qui était encore peu préparée à la tâche qui l'attendait. Le souffle du vent venait de très loin, et elle le sentit emplir son esprit, la libérant de ses doutes, et la laissant pleine d'une farouche détermination. _" Pour Mondstadt, toujours. "_ Devenir un guerrier aussi bon et courageux que Venessa ; combattre pour son peuple et la liberté... Jean a pris l'habitude lorsqu'elle se sent en proie aux doutes et qu'elle se sent flancher de venir rendre visite à l'arbre, qui la console à chaque fois, emportant sa fatigue, et lui permettant de continuer la mission qu'elle s'est assignée. Le grand arbre de Ventlevé marqua la fin du périple du premier Chevalier du Croc de Lion. Aujourd'hui, il symbolise le départ du périple de Jean.

=== "Histoire 5"

      - La Grande Maîtresse Jean a un secret. Le clan Gunnhildr est une famille de chevalier à la longue histoire.  
      C'est à sa mère Frederica que Jean doit son sang de noble lignée qui coule dans ses veines. Le père de la jeune femme était lui le célèbre aventurier Seammus Page.  
      Ce dernier abandonna la personne qu'il était à Mondstadt, pour recommencer une nouvelle vie et rejoindre l'Eglise de Favonius, au sein de laquelle il se hissa jusqu'à atteindre de hautes responsabilités, et être nommé _" Cardinal de l'Aurore "_.  
      Le destin sépara les deux amants, qui suivirent chacun une voix différentes, et Jean dut dire très jeune adieu à son père, qui partit avec sa sœur Barbara.  
      Cette dernière suivit la voix de son père, et entra dans les ordres, pour devenir une diaconesse aimée de tous à Mondstadt.  
      Jean a toujours espéré pouvoir se rapprocher de sa sœur, mais les mots lui manquent lorsqu'elle fait fasse au regard fuyant de celle-ci... Peut-être que cette gène est l'expression confuse que ce sentiment est partagé.

     - Jean possède un autre secret, à ses yeux beaucoup plus embarrassant.  
     Elle est versée dans les classiques historiques, elle qui porte le titre de _" Chevalier au Pissenlit "_, elle qui est devenue la Grande Maîtresse suppléante de l'Ordre, respectée de tous.  
     Elle est aussi une lectrice inconditionnelle de romans à l'eau de rose.  
     Cet intérêt n'est pas dû à une adolescence éclipsée l'entraînement et le devoir, et il ne faut pas non plus en chercher la cause dans le mariage écourté de ses parents.  
     Jean trouve simplement inspirante l'attraction mutuelle décrite dans de telles histoires, et en son for intérieur aspire à ressentir les émotions subtiles et fragiles qu'elle y lit.  
     Mais... _" Si seulement moi aussi je pouvais... "_ Ainsi soupire Jean, assise à la nuit avancée dans son bureau, en refermant " La Mélancolie de Véra ", qu'elle vient de finir de nouveau  
     _" Si j'ai le temps je devrais pouvoir me rendre au cap de la promesse avant le lever du jour... Ca ne devrait pas poser de problèmes après tout, non ? "_ Et baissant la tête, elle se fond dans la contemplation des étoiles brillant par la fenêtre dans un ciel noir comme le jais.

## Elevation
---
| Niveau | A récolter |
|---------|-------------|
| 20 | - Moras 20 000  - Éclat de turquoise vayudax1  - Graine de pissenlit x3  - Masque endommagé x3 |
| 40 | - Moras 40 000 - Fragment de turquoise vayuda.png - Fragment de turquoise vayuda x3 - Graine d'ouragan x2 - Graine de pissenlit x10 - Masque endommagé x15 |
 50 | - Moras 60 000 - Icône Fragment de turquoise vayuda.png Fragment de turquoise vayuda x6 - Graine d'ouragan x4 - Graine de pissenlit x20 - Masque sale x12 |
| 60 | - Moras 80 000 - Morceau de turquoise vayuda x3 - Graine d'ouragan x8 -Graine de pissenlit x30 - Masque sale x18 |
|70 |  - Moras 100 000 - Morceau de turquoise vayuda x6 - Graine d'ouragan x12 - Graine de pissenlit x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre de turquoise vayuda x6 - Graine d'ouragan x20 - Graine de pissenlit x60 - Masque sinistre x24 |

## Aptitudes
---
|![aptitude 1](image/aptitude/aptitude_1_jean.png) Escrime de Favonius | Aptitude de combat |
|-------------|----------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Libère une puissante attaque ascendante portée par les vents ; cette attaque consomme de l'endurance. Les ennemis projetés en l'air retombent lentement au sol. |
|Attaque descendante : |Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](image/aptitude/aptitude_2_jean.png) Épée de tourbillon | Aptitude de combat |
|-------------|----------|
| | Concentrant l'énergie des vents dans son épée, Jean libère une tornade qui projette les ennemis dans la direction ciblée, infligeant d'importants DGT Anémo. |
| Appui long | Les ennemis et les objets proches sont attirés par la tornade ; cette attaque consomme de l'endurance sur la durée.  La tornade peut être dirigée.  Jean ne peut pas se déplacer tant que l'attaque dure.  Idéale pour protéger ses alliés, cette technique accompagne Jean depuis longtemps. |

|![aptitude 3](image/aptitude/aptitude_3_jean.png) Brise de pissenlit | Aptitude de combat |
|-------------|----------|
| | Jean invoque la protection des vents ; un champ de pissenlits tournoyant repousse les ennemis proches, tout en leur infligeant des DGT Anémo. Dans le même temps, tous les personnages de l'équipe sont soignés considérablement ; le nombre de PV restaurés est proportionnel à l'ATQ de Jean. |
| Champ de pissenlits | Le champ de pissenlits restaure en continu les PV des personnages dans la zone. Le champ leur applique également l'élément Anémo. Les ennemis qui rentrent ou sortent de la zone subissent des DGT Anémo. Semblable aux pissenlits bercés par le vent, la magnanimité de Jean n'a que l'horizon pour limite. |

|![aptitude 4](image/aptitude/aptitude_4_jean.png)Charge critique | Aptitude passive |
|-------------|----------|
| | Les attaques normales de Jean ont 50% de chance de restaurer une quantité de PV équivalent à 15% de l'ATQ de Jean à tous les personnages de l'équipe lorsqu'elles touchent. |

|![aptitude 5](image/aptitude/aptitude_5_jean.png) Que le vent vous guide | Aptitude passive |
|-------------|----------|
| | Brise de pissenlit restaure 20% d'énergie élémentaire lorsqu'elle prend fin. |

|![aptitude 6](image/aptitude/aptitude_6_jean.png) Vent directeur | Aptitude passive |
|-------------|----------|
| | 12% de chance d'obtenir un plat supplémentaire lorsque vous cuisinez un plat de soins parfait. |

## Constellations
---
|![constellation 1](image/constel/constelation_1_jean.png) Tourmente tranchante | Constellation Niv.1 |
|-------------|----------|
| | La VIT d'attraction et les DGT infligés par Épée de tourbillon augmentent de 40% lorsque la compétence reste activée plus d'une seconde. |

|![constellation 2](image/constel/constelation_2_jean.png) Égide du peuple | Constellation Niv.2 |
|-------------|----------|
| | La VIT de déplacement et d'attaque de tous les personnages de l'équipe augmente de 15% pendant 15 s lorsque Jean obtient un orbe ou une particule élémentaire. |

|![constellation 3](image/constel/constelation_3_jean.png) Lorsque le vent d'ouest souffle | Constellation Niv.3 |
|-------------|----------|
| | Niveau de compétence Brise de pissenlit +3. |
| | Niveau max : 15 |

|![constellation 4](image/constel/constelation_4_jean.png) Territoire des pissenlits | Constellation Niv.4 |
|-------------|----------|
| La RÉS Anémo des ennemis à l'intérieur du champ de Brise de pissenlit est réduite de 40%. |

|![constellation 5](image/constel/constelation_5_jean.png) Le moment du vent violent | Constellation Niv.5 |
|-------------|----------|
| | Niveau de compétence Épée de tourbillon +3. |
| | Niveau max : 15 |

|![constellation 6](image/constel/constelation_6_jean.png) Croc de Lion, le protecteur de Mondstadt | Constellation Niv.6 |
|-------------|----------|
| | Les DGT subis par Jean lorsqu'elle se trouve dans le champ de Brise de pissenlitsont réduits de 35%.
| | Cet effet disparaît dans les 10 s ou après avoir subi 3 attaques une fois que Jean a quitté le champ. |

## Skin

![Skin de Jean](image/skin_jean.png)