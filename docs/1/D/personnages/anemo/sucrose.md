!!! info inline end "Identité"
    Faisant preuve d'une curiosité sans limite envers tout ce que l'univers peut offrir, Sucrose est l'assistante d'Albedo, et travaille pour l'Ordre de Favonius, se concentrant plus particulièrement sur la bio-alchimie.
    Elle espère que l'alchimie lui permettra de modifier de manière positive la vie sur terre, et de rendre ce monde encore plus varié qu'il ne l'est aujourd'hui.
    Bien sûr, ses expériences résultent de temps en temps en des découvertes assez dérangeantes. Mais dans l'ensemble, on doit à ses recherches en la matière des résultats remarquables.

![sucrose](https://images5.alphacoders.com/112/thumbbig-1120737.webp)

## Histoire

=== "Histoire"

    - On doit à Sucrose de remarquables résultats dans le domaine de la bio-alchimie.

    - Ses recherches... Ou plutôt, comme elle aime à le répéter, sa _« quête de vérité »_ l'amène parfois à obtenir d'étranges et mystérieuses créations.

    - Un type de pissenlit contenant six fois plus de graines (à tel point que lorsque souffle le vent, les gens n'ont d'autre choix que de fermer portes et fenêtres en tout hâte), des baies à crochets capables de casser la brique, ou encore des pommes crépusculaires ressemblant à s'y méprendre à des potirons...

    - Autant de découvertes qui laissent Sucrose perplexe, mais font avancer ses recherches à grands pas.

    - Mais il faut bien l'avouer, trouver des noms pour chacune des bizarreries qui sortent de son laboratoire reste peut-être la tâche la plus ardue. Pour Sucrose, ceci est encore plus douloureux que de voir les résultats de ses expériences cueillis par des passants peu scrupuleux, ou de voir ses ingrédients contaminés par des mains sales.

    - Prenons l'exemple de cette fleur sucrante, au nectar dense comme de la glue. Il aura fallu à Sucrose un... certain temps pour finalement trouver un nom qu'elle juge à la hauteur (par « un certain temps », comprenez plus longtemps encore que le temps requis pour polliniser tout un champ de manière artificielle) :

    - _« Voyons voir... Voici la fleur du 28ème essai... euh, la fleur de la phase 28... Hum... Il me faut un nom plus scientifique... »_

    - _« Je sais, on va l'appeler... Fleur sucrante version spéciale de la 28ème édition ! »_

## Elevation

| Niveau | A récolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de turquoise vayuda x1 -Chrysanthème à aubes x3 -Nectar de Fleur mensongère x3 |
| 40 | - Moras 40 000 - Fragment de turquoise vayuda x3 - Graine d'ouragan x2 - Chrysanthème à aubes x10 - Nectar de Fleur mensongère x15 |
| 50 | - Moras 60 000 - Fragment de turquoise vayuda x6 - Graine d'ouragan x4 - Chrysanthème à aubes x20 - Nectar miroitant x12 |
| 60 | - Moras 80 000 - Morceau de turquoise vayuda x3 - Graine d'ouragan x8 - Chrysanthème à aubes x30 - Nectar miroitant x18 |
| 70 | - Moras 100 000 - Morceau de turquoise vayuda x6 - Graine d'ouragan x12 - Chrysanthème à aubes x45 - Nectar élémentaire x12 |
| 80 | - Moras 120 000 - Pierre de turquoise vayuda x6 - Graine d'ouragan x20 |

## Aptitudes
---

|![aptitude 1](image/aptitude/aptitude_1_sucrose.png) Esprit du vent | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 attaques chargées de vent qui infligent des DGT Anémo. |
| Attaque chargée : | Après un court délai, inflige des DGT Anémo de zone ; cette attaque consomme de l'endurance. |
| Attaque descendante : | Plonge depuis les airs en concentrant ses pouvoirs Anémo, infligeant des DGT aux ennemis sur la route et des DGT Anémo de zone à l'impact. |

|![aptitude 2](image/aptitude/aptitude_2_sucrose.png) Esprit du vent : Sujet 6308 | Aptitude de combat |
|---------|-------------|
| | Sucrose invoque un Esprit du vent de taille réduite, qui attire les ennemis et les objets proches à lui, projetant les ennemis dans les airs et leur infligeant des DGT Anémo. C'est un peu embarrassant, mais il faut bien reconnaître que malgré leur courte durée de vie, les Esprits créés via ce procédé sont particulièrement utiles pour l'aventure. Comme quoi, même les expériences ratées peuvent servir. |

|![aptitude 3](image/aptitude/aptitude_3_sucrose.png) Esprit du vent : Isomorphe 75 Type II | Aptitude de combat |
|---------|-------------|
| | Sucrose jette une concoction instable qui libère un Esprit du vent de grande taille. L'Esprit attire les ennemis proches à lui, projetant les ennemis dans les airs et leurs infligeant des DGT Anémo. |
| Absorption élémentaire | L'Esprit du vent absorbe les éléments Hydro, Pyro, Cryo et Électro avec lesquels il entre en contact ; ceux-ci lui confèrent leurs attributs, et infligent un bonus de DGT de l'élément correspondant. Un seul élément peut être absorbé à la fois. Sucrose aime à donner des noms à ses Hypostases Anémo instables ; leur lignée est encore plus longue que celle du Baron Lapinou 893e du nom. |

|![aptitude 4](image/aptitude/aptitude_4_sucrose.png) Permutation de catalyste | Aptitude passive |
|---------|-------------|
| | Tous les personnages de l'équipe du type élémentaire concerné (à l'exception de Sucrose) obtiennent un bonus de 50 pts de maîtrise élémentaire pendant 8 s lorsque Sucrose déclenche une Dispersion. |

|![aptitude 5](image/aptitude/aptitude_5_sucrose.png) Mollis Favonius | Aptitude passive |
|---------|-------------|
| | Lorsque Esprit du vent : Sujet 6308 ou Esprit du vent : Isomorphe 75 Type II touche un ennemi, tous les personnages (à l'exception de Sucrose) bénéficient pendant 8 s d'un bonus de maîtrise élémentaire équivalent à 20% de la maîtrise élémentaire de Sucrose. |

|![aptitude 6](image/aptitude/aptitude_6_sucrose.png) Fragile découverte | Aptitude passive |
|---------|-------------|
| | Sucrose a 10% de chance d'obtenir le double de produits lors de la synthèse de matériaux d'amélioration de personnage ou d'arme. |

## Constellations
---

|![constellation 1](image/constel/constelation_1_sucrose.png) Champ du néant | Constellation Niv.1 |
|---------|-------------|
| | Confère une charge supplémentaire à Esprit du vent : Sujet 6308. |

|![constellation 2](image/constel/constelation_2_sucrose.png) Bett : Forme éthérée | Constellation Niv.2 |
|---------|-------------|
| | Prolonge la durée d'Esprit du vent : Isomorphe 75 Type II de 2 s. |

|![constellation 3](image/constel/constelation_3_sucrose.png) Zéro défaut | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Esprit du vent : Sujet 6308 +3. |
| | Niveau max : 15 |

|![constellation 4](image/constel/constelation_4_sucrose.png) Alchémania | Constellation Niv.4 |
|---------|-------------|
| | Réduit le TdR d'Esprit du vent : Sujet 6308 de 1 à 7 s toutes les 7 attaques normales et chargées que Sucrose inflige. |

|![constellation 5](image/constel/constelation_5_sucrose.png) Flasque standard | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Esprit du vent : Isomorphe 75 Type II +3. |
| | Niveau max : 15 |

|![constellation 6](image/constel/constelation_6_sucrose.png) Théorie de l'entropie | Constellation Niv.6 |
|---------|-------------|
| | En cas d'absorption élémentaire, Esprit du vent : Isomorphe 75 Type II confère à tous les personnages de l'équipe un bonus de DGT élémentaires de 20% tant que la compétence est active. |

## Skin