
!!! info inline end "Identité"
     "Samouraï errant né à Inazuma, au caractère humble et agréable."
     Son apparence jeune et débonnaire cache un cœur abritant de nombreuses blessures. On pourrait le croire insouciant, mais Kazuha obéit à ses propres principes.

![kazuha](https://images.alphacoders.com/115/thumbbig-1150213.webp)

### Histoire

=== "Histoire"


     - Son apparence jeune et débonnaire cache un cœur abritant de nombreuses blessures enfouies. On pourrait le croire insouciant, mais Kazuha obéit à ses propres principes.
     - Errant à travers le monde, il marche de jour, pour dormir le soir avec la terre et le ciel. Jamais il ne se plaint, et les richesses ne l'intéressent pas. La seule chose qu'il recherche est la paix intérieure en toute chose. Passant souvent ses nuits à la belle étoile, il ne s'inquiète jamais de se faire surprendre par les intempéries. On dirait presque que vents et pluies préfèrent faire un détour plutôt que de le déranger.
     - Demandez-lui comment l'expliquer, et Kazuha vous répondra en toute franchise qu'il sait parler le langage du vent et des nuages. Il est rare que les gens fassent attention au temps qu'il fait ; rien de plus normal donc qu'ils ne puissent voir les signes quand ils sont là. Kazuha, pour lequel tout cela n'a plus de secrets, n'a qu'à écouter les variations dans le chant du vent et goûter à l'humidité de l'air pour savoir que la pluie et la neige arrivent et qu'il faut trouver un autre chemin.
     - Nombreuses sont les tempêtes auxquelles Kazuha a dû échapper au cours de sa vie passée à errer sous les étoiles. Mais la tempête qui le guette aujourd'hui est un ouragan comme il n'en a jamais affronté ; et cette fois, il ne le contournera pas. Aux croisements du destin l'attendent la pluie et le tonnerre – enfin.
     _« — Au loin tonne l'éclair ; je peux le sentir. La tempête arrive, mais je ne bougerai pas. »_

## Elevation
---
| Niveau | A récolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de turquoise vayuda x1 - Ganoderma marin x3 - Insigne du Pilleur x3 |
| 40 | - Moras 40 000 - Fragment de turquoise vayuda x3 - Mécanisme oni x2 - Ganoderma marin x10 - Insigne du Pilleur x15 |
| 50 | - Moras 60 000 - Fragment de turquoise vayuda x6 - Mécanisme oni x4 - Ganoderma marin x20 - Insigne de corbeau en argent x12 |
| 60 | - Moras 80 000 - Morceau de turquoise vayuda x3 - Mécanisme oni x8 - Ganoderma marin x30 - Insigne de corbeau en argent x18 |
| 70 | - Moras 100 000 - Morceau de turquoise vayuda x6 - Mécanisme oni x12 - Ganoderma marin x45 - Insigne de corbeau en or x12 |
| 80 | - Moras 120 000 - Pierre de turquoise vayuda x6 - Mécanisme oni x20 |

## Aptitudes
---
|![aptitude 1](image/aptitude/aptitude_1_kazuha.png)Escrime de Garyuu | Aptitude de combat |
|-------------|----------|
| Attaque plongeante : | Contrôle du chaos Lorsqu'une attaque plongeante est déclenchée en utilisant les effets de la compétence élémentaire « Véhémence divine », ses DGT sont convertis en DGT Anémo et un petit tunnel venteux est créé à l'atterrissage à l'aide d'une technique d'épée secrète pour attirer les objets et les ennemis proches. |
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Consomme de l’endurance pour déclencher deux coups d’épée rapides. |
| Attaque descendante : | Plonge dans les airs pour frapper le sol, infligeant des DGT aux ennemis en chemin et des DGT de zone à l'impact. Si une attaque plongeante est effectué pendant « Véhémence divine », elle se transforme en « Attaque plongeante : Contrôle du chaos ». |

|![aptitude 2](image/aptitude/aptitude_2_kazuha.png) Véhémence divine | Aptitude de combat |
|-------------|----------|
| | |Libère une technique secrète aussi violente qu’une bourrasque déchaînée qui attire les ennemis et les objets vers la position actuelle de Kazuha, avant de projeter les ennemis situés dans la zone, leur infligeant des DGT Anémo et d’élever Kazuha sur un courant de vent impétueux. Si Kazuha déclenche une attaque plongeante depuis les airs dans un délai de 10 s après la compétence élémentaire « Véhémence divine », il provoque une attaque plongeante particulièrement puissante connue sous le nom de Contrôle du chaos. |
| Appui simple | Permet d’utiliser l’aptitude dans les airs. |
| Appui long | Améliore Charge l’aptitude avant de libérer davantage de DGT Anémo sur une zone plus large que lors d’un appui simple. |
| Attaque plongeante : Contrôle du chaos | Lorsqu’une attaque plongeante est déclenchée en utilisant les effets de la compétence élémentaire « Véhémence divine », ses DGT sont convertis en DGT Anémo et un petit tunnel venteux est créé à l’atterrissage à l’aide d’une technique d’épée secrète pour attirer les objets et les ennemis proches. Les dégâts infligés par Contrôle du chaos sont considérés comme étant des DGT d’attaque plongeante. Les vents dans les érables pleurent, pour les dieux et les hommes du passé, comme des montagnes rouges d’automne. |

|![aptitude 3](image/aptitude/aptitude_3_kazuha.png) Coupure de mille feuilles | Aptitude de combat | 
|-------------|----------|
| Déclenche la technique ultime de Garyuu, une coupure unique semblable à une tempête des DGT Anémo de zone. Le trajet de la lame laissera derrière lui un champ appelé « Vent d'automne » qui infligera des DGT Anémo de façon intermittente aux ennemis à l'intérieur.
| Absorption élémentaire | Le Vent d'automne absorbe les éléments Hydro, Pyro, Cryo et Électro avec lesquels il entre en contact ; ceux-ci lui confèrent leurs attributs, et infligent un bonus de DGT de l'élément correspondant. _« Je vois passer l'éternité en un instant, les automnes de plusieurs années d'une seule feuille ; ainsi je trancherai cet érable en un éclair, et je poserai la question des âges... »_ |

|![aptitude 4](image/aptitude/aptitude_4_kazuha.png) Lame d'érosion | Aptitude passive |
|-------------|----------|
| | Si Véhémence divine entre en contact avec l'élément Hydro, Pyro, Cryo ou électro lors de son utilisation, l'attaque plongeante « Contrôle du chaos » déclenchée après cette Véhémence divine bénéficie d'une absorption élémentaire, ajoutant ensuite des DGT de l'élément correspondant pour 200 % de l'attaque, dégâts considérés comme étant des DGT d'attaque plongeante. Un seul élément peut être absorbé à chaque utilisation de Véhémence divine |

|![aptitude 5](image/aptitude/aptitude_5_kazuha.png) Haïku de la brise | Aptitude passive |
|-------------|----------|
| | Pour chaque point de maîtrise élémentaire de Kazuha, ce dernier accorde à tous les personnages de l'équipe un bonus de 0,04 % de DGT de l'élément correspondant pendant 8 s lorsqu'il déclenche une Dispersion. Il est possible de cumuler les bonus de DGT de différents éléments grâce à cette méthode. |

|![aptitude 6](image/aptitude/aptitude_6_kazuha.png)Vent d'aube | Aptitude passive |
|-------------|----------|
| | Réduit la consommation d'endurance des personnages de l'équipe de 20 % lors du sprint. Ne peut être cumulé avec d'autres attitudes passives aux effets identiques. |

## Constellations
---
|![constellation 1](image/constel/constelation_1_kazuha.png) Montagnes écarlates | Constellation Niv.1 |
|-------------|----------|
| | Réduit le TdR de Véhémence divine de 10 %. Coupure de mille feuillesréinitialise le TdR de Véhémence divine lorsqu'elle est activée. |

|![constellation 2](image/constel/constelation_2_kazuha.png) Zanshin des montagnes cruelles | Constellation Niv.2 |
|-------------|----------|
| | Le champ de Vent d'automne créé par Coupure de mille feuilles a les effets suivants : Tant qu'il est actif, la maîtrise élémentaire de Kazuha est augmentée de 200 pts. La maîtrise élémentaire des personnages déployés dans le champ est augmentée de 200 pts. Les effets d'augmentation de maîtrise élémentaire de cette constellation ne se cumulent pas. |

|![constellation 3](image/constel/constelation_3_kazuha.png) Mystère d'érable | Constellation Niv.3 |
|-------------|----------|
| | Niveau d’aptitude Véhémence divine +3. |
| | Niveau max : 15 |

|![constellation 4](image/constel/constelation_4_kazuha.png) Illusion du néant | Constellation Niv.4 |
|-------------|----------|
| | Quand l'énergie élémentaire de Kazuha tombe en dessous de 45 pts, il obtient les effets suivants :
Cliquer simplement ou longuement pour utiliser Véhémence divine restaure l'énergie élémentaire de Kazuha de 3 ou 4 pts, respectivement.
Planer restaure l'énergie élémentaire de Kazuha de 2 pts toutes les secondes. | 

|![constellation 5](image/constel/constelation_5_kazuha.png) Collection ancestrale | Constellation Niv.5 |
|-------------|----------|
| | Niveau d’aptitude Coupure de mille feuilles +3. |
| | Niveau max : 15 |

|![constellation 6](image/constel/constelation_6_kazuha.png) Pétales écarlates | Constellation Niv.6 |
|-------------|----------|
| | Kazuha bénéficie d'un enchantement Anémo pendant 5 s après avoir utilisé Véhémence divine ou Coupure de mille feuilles. De plus, chaque point de maîtrise élémentaire de Kazuha augmente les DGT infligés par son attaque normale, son attaque chargée et son attaque plongeante de 0,2 %. |

## Skin