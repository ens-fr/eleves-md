!!! info inline end "Identité"
    Sayu est une ninja de l'organisation secrète Shuumatsuban, dont le plus grand désir est de dormir à satiété et de grandir.
    Désireuse de passer le plus de temps possible à faire la sieste, Sayu est devenue experte des techniques de l'art ninja de la fuite, du camouflage et de la transformation pour cette même raison.
    L'usage qu'elle en fait pourrait bien vous surprendre. 

![sayu](image/sayu.png){width="500"}

=== "Histoire"

    - Une ninja rattachée au Shiyuumatsu-Ban. Elle est petite et extrêmement agile.

    - La croissance de Sayu s'est arrêtée il y a longtemps, si longtemps qu'elle croit avoir été piégée dans un rêve sans fin.

    _« Quand est-ce que je grandirai enfin ? Est-ce parce que je ne dors pas assez ? »_

    - Elle se réveille souvent en s'interrogeant ainsi, avant de se pelotonner pour se rendormir.

    - Sayu pense que dormir peut lui permettre de récupérer de l'énergie pour grandir. Par conséquent, elle est toujours à la recherche de méthodes lui permettant de perfectionner son art ninja tout en dépensant le moins d'énergie.

    - La fuite, le camouflage, la transformation... Sayu maîtrise toutes ces techniques à la perfection et peut même prendre l'apparence d'un buisson ou d'un seau d'eau.

    - C'est pourquoi le Shiyuumatsu-Ban a toujours beaucoup de mal à lui mettre la main dessus lorsqu'il a une mission à lui confier.

    - Évidemment, cela ne compte pas comme de la paresse aux yeux de la petite ninja : en dormant dans le but de grandir, elle se consacre à une tâche beaucoup plus importante.

## Elevation

| Niveau | A récolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Moelle cristalline x3 - Nectar de Fleur mensongère x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Mécanisme oni x2 - Moelle cristalline x10 - Nectar de Fleur mensongère x15 |
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Mécanisme oni x4 - Moelle cristalline x20 - Nectar miroitant x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Mécanisme oni x8 - Moelle cristalline x30 - Nectar miroitant x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Mécanisme oni x12 - Moelle cristalline x45 - Nectar élémentaire x12 |
| 80 | -  Moras 120 000 - Pierre de jade shivada x6 - Mécanisme oni x20 |

## Aptitudes
---

|![aptitude 3](image/aptitude/aptitude_3_sayu.png) Lame ninja du Shuumatsuban | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 coups d'épée. |
| Attaque chargée : | Exécute une succession d'attaques tournoyantes contre les ennemis proches ; cette attaque consomme de l'endurance de façon continue. L'attaque chargée se termine sur un coup puissant. |
| Attaque descendante : | Plonge dans les airs pour frapper le sol, infligeant des DGT aux ennemis en chemin et des DGT de zone à l'impact. |

|![aptitude 3](image/aptitude/aptitude_3_sayu.png) École Yoohoo : Course Fuuin | Aptitude de combat |
|---------|-------------|
| | La technique spéciale des ninjas de l'École Yoohoo ! Se recroqueville sous la forme d'un tourbillon Fuufuu, qui inflige des DGT Anémo aux ennemis qu'elle percute à grande vitesse, pour terminer sur un coup de pied Fuufuu qui inflige des DGT Anémo de zone. |
| Appui simple | Se met sous la forme d'un tourbillon Fuufuu et roule vers l'avant sur une courte distance avant d'exécuter son coup de pied Fuufuu. |
| Appui Long |Roule continuellement en tourbillon Fuufuu, augmentent la RÉS à l'interruption de Sayu. Sayu peut contrôler la direction dans laquelle elle roule, et utiliser à nouveau la compétence permet de sortir de cet état d'exécuter un coup de pied Fuufuu plus puissant. L'apppui long de cette compétence peut séclencher une absorption élémentaire. |
| Absorption élémentaire | Si sayu entre en contact avec les éléments Hydro, Pyro, Cryo, et Électro lorsqu'elle est en tourbillon Fuufuu, elle obtient les attributs élémentaires correspondants, et le tourbillon Fuufuu ainsi que le coup de pied Fuufuu infligent des DGt supplementaires de l'élément absorbé. L'absorption élémentaire ne peut se produire qu'une fois pendant la durée de la compétence. |

|![aptitude 3](image/aptitude/aptitude_3_sayu.png) École Yoohoo : Bourrasque du mujina | Aptitude de combat |
|---------|-------------|
| | Une autre technique spéciale des ninjas d'École Yoohoo, qui fait appel à une invocation pour venir en aide à Sayu. Inflige des DGT Anémo aux ennemis proches, et soigne tous les personnages de l'équipe proches d'une quantité de PV proportionelle à l'ATQ de Sayu. Iinvoque également un Daruma Muji-muji. |
| Daruma Muji-muji | À intervalles réguliers, le Daruma Muji-muji agit de l'une des manières suivantes en fonction de la situation : Lorsque les PV des personnages proches sont supérieurs à 70 %, le Darum MUji-muji attaque un ennemi proche, à qui il inflige des DGT Anémo ; Lorqu'un personnages déployé dont les PV cont intérieurs ou égaux à 70 % se trouve àproximité, le Daruma Muji-muji soigne le personnage déployé proche aayant le plus faible pourcentage de PV d'une quantité de PC proportionnelle à l'ATQ de Sayu. Si aucun ennemi ne se trouve à proximité et que les personnages ont tous plus de 70% de leurs PV, le Daruma Muji-muji soigne aussi les personnages. Ceux qui ne comprennent rien au jujitsu adorent voir Sayu fair étalage de cette technique, et certains ont même essayé de l'apprendre auprès d'elle. Mais Sayu veut dormir et n'a aucune envie d'être dérangée. |

|![aptitude 3](image/aptitude/aptitude_3_sayu.png) Quelqu'un de plus efficace | Aptitude passive |
|---------|-------------|
| | Lorsque Sayu déclenche une Dispersion, elle soigne les personnages de l'équie ainsi que les personnage alliés proches à hauteur de 300 PV. Sayu récupère de plus 1,2 PV supplémentaires pour chacun de ses pts de maîtrise élémentaires. Cet effet peut être déclenché une fois toutes les 2 s. |

|![aptitude 3](image/aptitude/aptitude_3_sayu.png) Pas de boulot aujourd'hui ! | Aptitude passive |
|---------|-------------|
| | Confère au Darum Muji-muji invoqué pendant École Yoohoo : Bourrasque du mujina les effets suivants : Lorqu'il soigne un personnage, les personnages proches de ce dernier sont soignés à hauteur de 20 % des PV récupérés par le personnage : La zone de DGT infligés augmente lorsqu'il attaque un ennemi. |

|![aptitude 3](image/aptitude/aptitude_3_sayu.png) École Yoohoo : Art du silence | Aptitude passive |
|---------|-------------|
| | Quand Sayu fait partie de l'équipe, vos personnages f'effraient pas certains animaux, tels que le papillon cristallin. Pour savoir quels sont les animaux concernés, consultez l'onglet « Divers » dans la section « Faune » du Bestiaire des Archives. |


## Constellations
---

|![constellation 1](image/constel/constelation_1_sayu.png) Technique multitâche | Constellation Niv.1 |
|---------|-------------|
| | Le Daruma Muji-muji invoqué pae École Yoohoo : Bourrasque du mujina ignore les limites de PV et peut attaquer les ennemis proches et soigner les personnages en même temps. |

|![constellation 2](image/constel/constelation_2_sayu.png) Hors de mon chemin ! | Constellation Niv.2 |
|---------|-------------|
| | École Yoohoo : Course Fuuin obtient les effets suivants : Les DGT Infligeés par le coup de pied Fuufuu après un appui simple augmentent de 3,3 %; Les DGT infligés par le coup de pied Fuufuu après un appui long augmentent de 3,3% toutes les 0,5s passées en tourbillon FUufuu, jusqu'à un max de 66 %. |

|![constellation 3](image/constel/constelation_3_sayu.png) Bah, le clone s'en occupe... | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude École Yoohoo : Bourrasque du mujina +3. |
| | Niveau max : 15 |

|![constellation 4](image/constel/constelation_4_sayu.png) Paresse revue et améliorée | Constellation Niv.4 |
|---------|-------------|
| | Sayu récupère 1,2pts d'énergie élémentaire lorsqu'elle déclenche une Dispersion sur le champ de bataille. Cet effet peut être déclenché une fois toutes les 2 s. |

|![constellation 5](image/constel/constelation_5_sayu.png) La vitesse avant tout | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Yoohoo Art : Fuuin Dash +3. |
| | Niveau max est 15. |



|![constellation 6](image/constel/constelation_6_sayu.png) C'est l'heure d'un bon dodo | Constellation Niv.6 |
|---------|-------------|
| | Les attaques et les soins du Daruma Muji-muji iinvoqué par Sayu lors d'École Yoohoo : Bourrasque du mujina augmentent avec le niv. de maîtrise élémentaire de Sayu. Shaque pt de maîtrise élémentaire de Sayu confère les effets suivants : Augmente les DGT infligés par les attaques du Daruma Muji-muji de 0,2% de l'ATQ jusqu'à 400% de l'ATQ max : Augmente de 3 les PV soignés par le Daruma Muji-muji, jusqu'à 6 000 PV max. |

## Skin