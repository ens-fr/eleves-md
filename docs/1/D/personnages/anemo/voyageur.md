![voyageur](https://images2.alphacoders.com/114/thumbbig-1141140.webp)


=== "Histoire 1"

    Aether et Lumine allaient quitter Teyvat après avoir été témoins de la destruction de Khaenri'ah, quand soudain une déesse inconnue leur a bloqué le chemin. Un combat s'ensuivit et l'un des jumeaux fut consumé par des cubes rouges de la déesse inconnue et l'autre piégé dans un sceau qui leur fit perdre leurs pouvoirs. Après avoir passé un temps inconnu scellé (le Voyageur suppose que cela faisait "des années"), ils se sont finalement réveillés. Échoué seul sur Teyvat pendant un certain temps. Un jour, le Voyageur repêcha accidentellement Paimon dans un lac, la sauvant de la noyade.

    Paimon restera avec le Voyageur et leur apprendra la langue Teyvat : à sa grande surprise, il ne leur a fallu que deux mois pour devenir un locuteur. Après avoir appris comment ils se sont retrouvés bloqués sur Teyvat au début du jeu, Paimon suggère qu'ils entrent en contact avec les Sept, afin d'aider le Voyageur à rechercher leur jumeau perdu. Le voyage du Voyageur commence à Mondstadt, où ils participent à l'incident Stormterror, orchestré par l'Ordre de l'Abîme et où le barde Venti est le vaisseau de Barbatos, l'un des Sept. Ils rencontrent également le Fatui et l'éxécutrice Signora, où ils apprennent la Gnosis de Venti et que l'un des Sept, la Tsarine, semble avoir des motifs inconnus avant de suggérer qu'ils se rendent à Liyue pour rencontrer Morax.

    À Liyue, le voyageur et Paimon tentent de rencontrer Morax, mais il meurt inexplicablement et ils sont par la suite suspectés d'être étrangers. Ils parviennent à s'échapper mais sont obligés de demander l'aide d'un autre prédicateur, Tartaglia, qui suggère qu'ils rencontrent l'adepte pour résoudre le problème et les présente à Zhongli, un consultant du salon funéraire Wangsheng qui peut les amener à l'Exuvia de Morax. L'aide de Tartaglia se révèle plus tard être une ruse alors qu'il brise l'ancien dieu Osial des liens de Morax, mais avec l'aide des Sept étoiles de Liyue et des adeptes, ils repoussent l'ancien dieu. Ce n'est qu'après avoir résolu le problème qu'ils apprennent que Zhongli est le vaisseau mortel de Morax et qu'il se retirait de son poste. Ils sont incapables d'empêcher les Fatui de mettre la main sur le Gnosis de Morax. Alors que les Fatui partent, Zhongli dit au Voyageur de se rendre à Inazuma.

    En attendant l'occasion de le faire, ils sont rappelés à Mondstadt où ils rencontrent Dainsleif, qui souhaite arrêter l'Ordre de l'Abîme. Pendant ce temps, ils arrêtent un complot dans lequel l'Ordre tentait de "mécaniser" Osial pour faire leur demande . Ils rencontrent également leur jumeau, mais sont choqués d'apprendre qu'ils dirigent l'Ordre. Ils tentent de convaincre leur jumeau de partir, mais le jumeau refuse jusqu'à ce que leur « guerre » avec le divin soit terminée. Le jumeau leur demande de continuer à voyager à Teyvat afin qu'ils puissent découvrir la vérité de première main avant de partir, avec Dainsleif à leur poursuite. Le Voyageur tente de le suivre mais est exclu.

    Finalement, le voyageur parvient à arriver à Inazuma où il est témoin du chaos du pays en raison du décret de saisie et refuse d'aider jusqu'à ce qu'il soit convaincu par Kamisato Ayaka. Ils rejoignent bientôt un groupe de résistance basé sur l'île de Watatsumi après avoir rencontré le shogun et ont failli être tués par elle dans le processus. Au cours de leur voyage, ils rencontrent Yae Miko, qui révèle que le Shogun est une marionnette et qu'Ei, l'archonte qu'ils recherchent, réside à l'intérieur. Ils apprennent bientôt que le chaos à Inazuma a été causé par la Fatui et affrontent Signora, la battant en duel devant le trône et assistent à sa mort aux mains de la Shogun. La Shogun essaie alors de les abattre alors qu'ils quittent Tenshukaku, mais l'arrivée opportune de la résistance permet au Voyageur de confronter Raiden Ei une fois de plus et de la vaincre avec les aspirations des yeux divins saisies, l'incitant à abroger le Décret de saisie. Le chaos étant résolu, Yae les remercie pour leur aide et leur conseille de se rendre à Sumeru.

=== "Histoire 2"

        indisponible

=== "Histoire 3"

     indisponible

=== "Histoire 4"

     indisponible

=== "Histoire 5"

     indisponible

## Elevation

| Niveau | A recolter |
|---------|-------------|
| 20 |  - Moras 20 000 - Éclat de topaze prithiva x1 - Chrysanthème à aubes x3 - Masque endommagé x3 |
| 40 | - Moras 40 000 - Fragment de topaze prithiva x3 - Éclat de diamant scintillant x2 - Chrysanthème à aubes x10 - Masque endommagé x15 |
| 50 | - Moras 60 000 - Fragment de topaze prithiva x6 - Éclat de diamant scintillant x4 - hrysanthème à aubes x20 - Masque sale x12 |
| 60 |- Moras 80 000 - Morceau de topaze prithiva x3 - Éclat de diamant scintillant x8 - Chrysanthème à aubes x30 - Masque sale x18 |
| 70 | - Moras 100 000 - Morceau de topaze prithiva x6 - Éclat de diamant scintillant x12 - Chrysanthème à aubes x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre de topaze prithiva x6 - Éclat de diamant scintillant x20 - Chrysanthème à aubes x60 |

## Aptitudes
---

|![aptitude 1](image/aptitude/aptitude_1_voyageur.png) Vents de la discorde | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Consomme de l'endurance pour déclencher deux coups d'épée rapides. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](image/aptitude/aptitude_2_voyageur.png) Vortex destructeur | Aptitude de combat |
|---------|-------------|
| | Un vortex de vent, tournant et tranchant, se forme dans la paume de votre main, infligeant des DGT Anémo continus aux ennemis en face. Le vortex finit par exploser à la fin de la compétence ; l'explosion couvre une zone plus large, et inflige des DGT Anémo plus importants. |
| Appui long | Augmente les DGT et la zone d'effet. |
| Absorption élémentaire | Le vortex absorbe les éléments Hydro, Pyro, Cryo et Électro avec lesquels il entre en contact ; ceux-ci lui confèrent un bonus de DGT de l'élément correspondant. Une seule réaction d'absorption élémentaire peut avoir lieu par utilisation. Vous avez discuté de la maîtrise de vos nouveaux pouvoirs Anémo avec Paimon. |

|![aptitude 3](image/aptitude/aptitude_3_voyageur.png) Rafale de vent | Aptitude de combat |
|---------|-------------|
| | Vous invoquez une tornade qui attire les objets et les ennemis en avançant, et qui inflige des DGT Anémo continus. |
| Absorption élémentaire | La tornade absorbe les éléments Hydro, Pyro, Cryo et Électro avec lesquels elle entre en contact ; ceux-ci lui confèrent leurs attributs, et infligent un bonus de DGT de l'élément correspondant. Un seul élément peut être absorbé à la fois. Vous avez étudié le battement des ailes du papillon avec Paimon. |

|![aptitude 4](image/aptitude/aptitude_4_voyageur.png) Vent du ciel déchiré | Aptitude passive |
|---------|-------------|
| | Le dernier coup d'attaque normale libère une lame de vent, qui inflige une quantité de DGT Anémo équivalent à 60 % de l'ATQ aux ennemis sur sa route. |

|![aptitude 3](image/aptitude/aptitude_5_voyageur.png) Vent de guérison | Aptitude passive |
|---------|-------------|
| | Vortex destructeur restaure 2 % des PV par seconde pendant 5 s lorsqu'elle élimine un ennemi. Cet effet peut être déclenché une fois toutes les 5 s. |

## Constellations
---

|![constellation 1](image/constel/constelation_1_voyageur.png) Vortex déchaîné | Constellation Niv.1 |
|---------|-------------|
| | Vortex destructeur attire les ennemis et les objets dans un rayon de 5 mètres autour du personnage. |

|![constellation 2](image/constel/constelation_2_voyageur.png) Tornade de la rébellion | Constellation Niv.2 |
|---------|-------------|
| | Augmente la recharge d'énergie de 16 %. |

|![constellation 3](image/constel/constelation_3_voyageur.png) Rafale de balayage | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Rafale de vent +3. |
| | Niveau max : 15 |

|![constellation 4](image/constel/constelation_4_voyageur.png) Prévenance de brise | Constellation Niv.4 |
|---------|-------------|
| | Les DGT subis sont réduits de 10 % tant que Vortex destructeur est actif. |

|![constellation 5](image/constel/constelation_5_voyageur.png) Vortex stellaire | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Vortex destructeur +3. |
| | Niveau max : 15 |

|![constellation 6](image/constel/constelation_6_voyageur.png) Vents enlacés | Constellation Niv.6 |
|---------|-------------|
| | La RÉS Anémo des ennemis touchés par Rafale de vent est réduite de 20 %. En cas d'absorption élémentaire, leur RÉS à l'élément correspondant est également réduite de 20 %. |

## Skin