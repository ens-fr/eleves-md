
!!! info inline end "Identité"
     Kaeya est le fidèle bras droit de Jean au sein de l'Ordre de Favonius. Il n'y a rien qu'il ne puisse pas faire !
     Ce charmant chevalier est très apprécié de la population locale, mais il n'en demeure pas moins mystérieux.
     Kaeya est le capitaine de cavalerie des chevaliers de Favonius. Il est tenu en haute estime par les habitants de Mondstadt, même avec toutes ses excentricités et ses secrets.


![Kaeya](https://images4.alphacoders.com/114/thumbbig-1142348.webp)

## Histoire 

=== "Histoire 1"

    Ce qui est plus intéressant, c'est que vous aurez plus de chance la nuit de tomber sur Kaeya à la taverne qu'au quartier général de l'Ordre.

    On le voit souvent assis au bar, discutant avec les autres amateurs d'alcool tout en sirotant un "mort de l'après-midi", le fameux cocktail mondstatois.

    Les vieux et les piliers de bar de la cité lui font un excellent acceuil, allant jusqu'à le nommer "le meilleur candidat au poste de gendre".

    Il peut paraître surprenant qu'un homme espiègle et buveur tel que Kaeya soit capitaine de cavalerie de l'Ordre de Favonius.

    Ce qui est plus intéressant, c'est que vous aurez plus de chances la nuit de tomber sur Kaeya à la taverne qu'au quartier général de l'Ordre.

    Chasseurs comme bandits font partie de ses compagnons de beuverie; même le plus circonspect finit victime de ses beaux discours, et se retrouve à lui parler autour d'un verre.

    Et selon ce qu'ils confient à Kaeya, la nuit de ses compagnons de table peut connaître des fins radicalement différentes, allant de simple soirée entre clients du bar, à un cauchemar échappant entièrement à leur contrôle.

    "Tout le monde connait un secret, mais peu de gens savent quoi en faire", aime à répéter Kaeya avec son sourire narquois.

=== "Histoire 2"


     "La justice n'est pas un principe absolu, mais plutôt le point d'équilibre entre force et stratégie. Quand à comment elle est appliquée... Mieux vaut ne pas trop se poser de questions" Ainsi parla un jour Kaeya au Grand Maître.

     Tant que toutes les pièces se mettent en place à la fin de la manière qu'il le souhaite, Kaeya ne se soucie guère des moyens pour arriver à ses fins.

     L'attitude de ce cavaliers définit son caractère.

     D'aucuns pourraient ajouter qu'elle ressemble en tous points au fameux cocktail apprécié par l'homme : sauvage et explosive. Mais elle ne fait pas toujours l'unanimité.

     Ainsi par exemple, Kaeya déclencha un jour le gardien d'une ruine afin que le chef d'une bande de bandits l'affronte en face-à-face; cependant, ainsi faisant, il mit plusieurs de ses propres hommes en danger.

     Dans ces moments-là, même Jean, qui place toute sa confiance en Kaeya, secoue la tête en signe de désaccord. Mais cela ne dérange pas le moins du monde Kaeya, évidement. Au contraire même : il affectionne de mettre les gens dans des position délicates. Il prend autant de plaisir à voir la lueur d'hésitation dans les yeux de ses camarades juste avant l'assaut, que de lire la terreur dans celle de ses ennemis.

=== "Histoire 3"

     Le brassage d'alcool a depuis longtemps prospéré à Mondstadt, participant de la richesse de la cité; cette richesse attire tous types d'individus peu recommandables.

     Ces ombres que l'on aperçoit dans les ruelles sombres viennent d'horizons variés, et pour des raisons qui leur sont propres.

     Kaeya s'assure que Monsdstadt est protégée de tels bandits et monstres avec son épée autant qu'avec son esprit.

     Un jeune chevalier ayant dédié des années à l'étude des différentes menaces dans et autour de Mondstadt avait autrefois tenu la conclusion suivante :

     Les saisons où le cocktail mort de l'après-midi est indisponible, le niveau de menace et le nombre d'incidents enregistrés dans et en dehors de la cité montrent une baisse significative; jusqu'à ce que le cocktail soit de nouveau disponible...

     Ayant montré le résultat de ses recherches à Kaeya dans l'espoir d'obtenir du capitaine de la cavalerie quelques conseils, le chevalier n'obtint pour toute réponse qu'un simple "Intéressant..." accompagné dans sourire en coin.

=== "Histoire 4"

     De manière générale, Kaeya est quelqu'un facile d'accès; la seule chose dont il ne parle jamais est son passé.

     Quand bien même le grand Maître lui demande quelque renseignement sur sa vie passée, il trouve toujours le moyen de ne dournir que des réponses vagues.

     _"Il y a de cela dix ans environ, mon père m'amena un après-midi de fin d'été au Domaine de l'Aurore."_

    _ "Je me souviens encore de ses mots : "Je vais acheter une bouteille de jus de raisin pour la route"; mais il n'est jamais réapparu."_

     _"Je doute que si maître Crépus n'avait pris soin de moi cette nuit j'aurais réussi à passer la tempête qui s'abattit cette nuit-là"_

     Derrière ce qui paraît comme un récit détaché se cache un mensonge savamment construit. Kaeya n'a jamais révélé à quiconque ce qui était réellement arrivé en ce jour fatidique.

     _"C'est ta chance; tu es notre seul espoir."_

     Son père biologique lui avait alors serré les épaules, le regard portant au loin, en direction de leur pays natal, Khaebri'ah.

     Jamais Kaeya n'oublia ce regard, dans le quel l'espoir se disputait à la haine.

=== "Histoire 5"

     Nombreux sont les habitants de Mondstadt qui se souviennent des deux jeunes hommes les plus en vue il y a quelques années.

     Diluc était un épéiste élégant, affichant en permanence sur son visage plein d'assurance un sourire sympathique.

     Kaeya, aux traits étrangers, était son ami, son soutien et sa voix de la raison, s'assurant que Diluc vienne à bout de toutes les épreuves sur son chemin.

     Les deux amis étaient semblables à deux jumeaux, n'ayant point besoin de parler pour savoir ce que pensait l'autre, et protégeant Mondstadt de jour comme de nuit.

     ...Jusqu'à ce jour fatidique où le convoi qu'escortait Diluc fut attaqué en pleine forêt par un monstre énorme, jour à jamais gravé dans la mémoire de Kaeya. Ce fut la seule fois où il manqua à son devoir.

     Lorsqu'il arriva sur la scène, le combat était déjà terminé.

     Leur "père" avait vaincu la bête en faisant appel à quelque pouvoir occulte, mais avait perdu la vie.

     Kaeya et Diluc furent profondément choqués par l'événement, et perdirent le sang-froid légendaire du chevalier.

     _"Ainsi donc même quelqu'un comme maître Crepus se soumet face à un tel pouvoir maléfique..."_

     Tandis que cette dangereuse pensée germait en son esprit, Kaeya continuait d'afficher son sourire en coin.

     _"Ce monde est vraiment...fascinant"_

     Leur "père" gisait devant eux dans une mare de sang; et c'est cette nuit que les chemins des deux hommes se séparèrent.

## Elevation

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Lys calla x3 - Insigne du Pilleur x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Duramen de glace x2 - Lys calla x10 - Insigne du Pilleur x15 |
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Duramen de glace x4 - Lys calla x20 - Insigne de corbeau en argent x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3  - Duramen de glace x8  - Lys calla x30 - Insigne de corbeau en argent x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Duramen de glace x12 - Lys calla x45 - Insigne de corbeau en or x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Duramen de glace x20 - Lys calla x60 - Insigne de corbeau en or x24 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_kaeya.png) Passe d'armes | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Consomme de l'endurance pour déclencher deux coups d'épée rapides. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_kaeya.png) Attaque givrée | Aptitude de combat |
|---------|-------------|
| | Libère un blizzard gelant qui inflige des DGT Cryo aux ennemis en face. Qui serait capable de faire fondre la glace qui se cache derrière ce masque aimable…' |

|![aptitude 3](imagee/aptitude/aptitude_3_kaeya.png) Valse glaciale | Aptitude de combat |
|---------|-------------|
| | Kaeya condense les particules dans l'air pour invoquer 3 stalactites tournoyantes. Les stalactites suivent le personnage, et infligent des DGT Cryo aux ennemis sur leur route. Ses dehors souriants, son ton agréable, et la glace qu'ils cachent : voici les armes de Kaeya. |

|![aptitude 4](imagee/aptitude/aptitude_4_kaeya.png) Épée de la cruauté | Aptitude passive |
|---------|-------------|
| | Chaque fois qu'Attaque givrée touche un ennemi, Kaeya récupère une quantité de PV équivalent à 15 % de son ATQ. |

|![aptitude 5](imagee/aptitude/aptitude_5_kaeya.png) Cœur de l'Abîme glacial | Aptitude passive |
|---------|-------------|
| | Les ennemis gelés par Attaque givrée font tomber d'avantage de particules élémentaires. Jusqu'à 2 particules supplémentaires peuvent ainsi être obtenues par Attaque givrée. |

|![aptitude 6](imagee/aptitude/aptitude_6_kaeya.png) Force cachée | Aptitude passive |
|---------|-------------|
| | Réduit la consommation d'endurance de vos personnages dans l'équipe de 20 % lors du sprint. Ne peut pas être cumulé avec d'autres aptitudes passives ayant les mêmes effets. |

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_kaeya.png) Lignée de l'excellence | Constellation Niv.1 |
|---------|-------------|
| | Le taux de CRIT des attaques normales et chargées de Kaeya contre les ennemis affectés par Cryo augmente de 15 %. |

|![constellation 2](imagee/constel/constelation_2_kaeya.png) Danse givrée éternelle | Constellation Niv.2
|---------|-------------|
| | Chaque ennemi éliminé pendant la durée de Valse glaciale prolonge la durée de la compétence de 2,5 s.Cumul max : 15 s. |

|![constellation 3](imagee/constel/constelation_3_kaeya.png) Ébats glacials | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Attaque givrée +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_kaeya.png) Baiser glacé | Constellation Niv.4 |
|---------|-------------|
| | Kaeya génère un bouclier lorsque ses PV sont inférieurs à 20 %. Le bouclier absorbe une quantité de DGT équivalent à 30 % des PV max et dure 20 s. Ce bouclier peut absorber jusqu'à 250 % de DGT Cryo. Cet effet peut être déclenché une fois toutes les 60 s. |

|![constellation 5](imagee/constel/constelation_5_kaeya.png) Étreinte glaciale | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Valse glaciale +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_kaeya.png) Tourbillon glacial | Constellation Niv.6 |
|---------|-------------|
| | Génère une stalactite supplémentaire pendant Valse glaciale et restaure 15 pts d'énergie élémentaire. |

## Skin