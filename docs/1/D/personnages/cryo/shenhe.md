
!!! info inline end "Identité"
    Shenhe (Chinois: 申鹤 Shēnhè) est un personnage Cryo jouable dans Genshin Impact.

    Bien qu'elle soit née parmi les mortels, elle est devenue la disciple d'une Adepte. Elle a ainsi grandi dans les montagnes, bien loin du Port de Liyue, son âme maintenue par des cordes rouges, cultivant son corps et son esprit.
    Malgré son tempérament élégant d'Adepte, sa personne est enveloppée de mystères.
    Elle est également la tante de Chongyun.

![Shenhe](https://images.alphacoders.com/120/thumbbig-1200196.webp)

## Histoire
---

...

## Elevation
---
| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Qingxin x3 - Nectar de Fleur mensongère x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Fausse nageoire de l'héritier du dragon x2 - Qingxin x10 - Nectar de Fleur mensongère x15
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Fausse nageoire de l'héritier du dragon x4 - Qingxin x20 - Nectar miroitant x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Fausse nageoire de l'héritier du dragon x8 - Qingxin x30 - Nectar miroitant x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Fausse nageoire de l'héritier du dragon x12 - Qingxin x45 - Nectar élémentaire x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Fausse nageoire de l'héritier du dragon x20 - Qingxin x60 - Nectar élémentaire x24 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_shenhe.png) Percée des étoiles | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups de lance. |
| Attaque chargée : | Consomme de l'endurance pour charger en ligne droite et infliger des DGT aux ennemis sur la route. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_shenhe.png) Invocation d'esprit printanier | Aptitude de combat |
|---------|-------------|
| | La rosée givrée et argentée exorcisera tous les démons. Accord un effet de pennage glacé à tous les membre de l'équipe à proximité et inflige des DGT Cryo de différentes manières selon que la capacité a été activé2 avec un appui simple ou long. |
| Appui simple | Se précipite vers l'avant avec un esprit talismanique, infligeant des DGT Cryo aux ennemis en chemin. |
| Appui long | Ordonne à l'esprit talismanique d'infliger des DGT Cryo de zone.
| Pennage glacé | Lorsqu'une attaque normale, chargée ou plongeante, un compétence ou un dechaînement élémentaire inflige des DGT Cryo à un ennemi, les DGT infligés augmentent en fonction de l'ATQQ actuelle de Shenhe. Les effets du pennage glacé disparaissent à la fin de sa durée ou après avoir activé un certain nombre de cumuls. En utilisant un pennage glacé avec un appui long à la place d'un appui simple, sa durée et  le nombre de cumuls générés augmentent. Lorsqu'une attaque de DGT Cryo touche pusieurs ennemis en meme temps, les cumuls sont consommés selon le nombre d'ennemis touchés. Les cumuls de pennage glacé sont calculés indépendamment pour tous les personnages de l'équipe. _« Le pouvoir des esprits n'est pas un mensonge. Même accablée pr les ordres, la demoiselle divine continue ! »_ |

|![aptitude 3](imagee/aptitude/aptitude_3_shenhe.png) Délivrance de la demoiselle divine | Aptitude de combat |
|---------|-------------|
| | Libère la puissance de l'espirit talismanique, lui permettant de se déplacer librement dans ce plan d'existence pour infliger des SGT Cryo de zon. L'espirit talismanique crée alors un champ qui diminue les RÉS Cryo et physique des ennemis, et inflige des DGT Cryo de façon continue aux ennemis à l'intérieur. _«  La voie des Adeptes n'est pas pour ceux pris dans le mystère et l'imagination. Ce n'est qu'en observant les changements des cieux et de la terre. Ce n'est qu'en observant les changements des cieux et de la terre, et en comprenant les lois de cause à effet dans ce royaume, que l'on peut saisir son essence. »_ |

|![aptitude 4](imagee/aptitude/aptitude_4_shenhe.png) Étreinte divine | Aptitude passive |
|---------|-------------|
| | Lorsque le personnage déployé est dans le champ générée par Délivrance de la demoiselle divine, bon bonus de DGT Cryo augente de 15%. |

|![aptitude 5](imagee/aptitude/aptitude_5_shenhe.png) Sceau de communion spirituelle | Aptitude passive |
|---------|-------------|
| | Lorsque Shenhe utilise Invocation d'esprit printanier, tous les personnages de l'équipe à prozimité bénéficient des effets suivants : |
| Appui simple : | les DGT infligés par les compétences et les déchaînements élémentaires augmentent de 15 % pendant 10 s. |
| Appui long : | les DGT infligés par les attaques normales, chargées et plongeantes augmentent de 15 % pendant 15 s. |

|![aptitude 6](imagee/aptitude/aptitude_6_shenhe.png) Arrivée opportune | Aptitude passive |
|---------|-------------|
| | Augmente les récompenses d'une expédition de 20 h à Liyue de 25 %. |

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_shenhe.png) Clarté du cœur | Constellation Niv.1 |
|---------|-------------|
| | Confère une charge supplémentaire pour Invocation d'esprit printanier. |

|![constellation 2](imagee/constel/constelation_2_shenhe.png) Esprit centré | Constellation Niv.2 |
|---------|-------------|
| | Prolonge la durée de Délivrance de la demoiselle divine de 6 s. De plus, les DGT CRIT des DGT Cryo du personnage déployé dans le champ augmentent de 15 %. |

|![constellation 3](imagee/constel/constelation_3_shenhe.png) Isolement | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Invocation d'esprit printanier +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_shenhe.png) Clairvoyance | Constellation Niv.4 |
|---------|-------------|
| | Lorsqu'un personnage affectépar le pennage glacé de Shenhe déclenche l'effet de bonus de DGT de la flèche, Shenhe obtient 1 cumul de Mantra de givre : Lorsque Shnhe utilise Invocation d'esprit printanier, tous les cumuls de Mantra de givre sont consommés pour augmenter les DGT infligés par cette invocation d'esprit printanier de 5% pour chaque cumul consomme. Cet effet peut être cumulé 50 fois et dure 60 s. |

|![constellation 5](imagee/constel/constelation_5_shenhe.png) Divinisation | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Délivrance de la demoiselle divine +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_shenhe.png) Abandon mystique | Constellation Niv.6 |
|---------|-------------|
| | Lorsqu'un personnage déclenche les effets d'un pennage glacé avec les DGT d'une attaque normale ou charge, aucun cumul n'est consommé. |

## Skin