!!! info inline end "Identité"
    Exorciste basé à Liyue, et exerçant ses activités de par le monde. Héritier de l'une des plus célèbres maisons de l'art, il a un talent dévastateur qu'il ne doit pas à l'enseignement de ses maîtres, mais à l'impressionnante quantité d'énergie en permanence au bord de l'implosion qui l'habite.

![Chongyun](https://images4.alphacoders.com/112/thumbbig-1127299.webp)

## Histoire

=== "Histoire 1"

    Au début de la carrière de Chongyun courait à Liyue une rumeur au sujet d'un mauvais esprit rôdant dans le port. A l'origine de ces bruits se trouvait une femme d'une riche famille, au statut suffisament élevé pour pouvoir s'adresser directement au Sept Etoiles. Chaque nuit, cette dernière pouvait entendre d'étranges bruits ; mais à chaque fois qu'elle essayait de s'en rapprocher, les bruits semblaient soudain venir d'une direction opposée. Elle finit par en perdre le sommeil et sombrer dans un état d'angoisse constante. Une femme de ses moyens pouvait bien sûr faire appel aux meilleurs des exorcistes ; mais aucun d'eux ne parvint à trouver la source du problème. Plus inquiétant encore, les bruits s'amplifièrent. Alors qu'elle ne savait plus à quel saint se vouer, Chongyun frappa à sa porte. " Toutes mes excuses ; il faisait trop chaud ces derniers jours, et je n'ai appris que tout récemment votre problème. Laissez-moi donc m'en occuper. " Et ainsi parlant, Chongyun s'assit tout simplement au milieu de la résidence et resta là un instant, sans mot dire. Le soir-même, aucun bruit ne se fit entendre ; la femme put enfin dormir tout son soûl.Le lendemain, elle se rendit chez Chongyun, comptant le couvrir d'or et de bijoux ; mais celui-ci, toujours aussi imperturbable, n'accepta que les quelques centaines de moras qu'il demandait pour ce type de service. Cet épisode suffit à propulser Chongyun sur le devant de la scène, et sa réputation le précède aujourd'hui sur tout le continent - la réputation d'un personnage sérieux et au visage de glace.

## Elevation

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Cœur de lapis x3 - Masque endommagé x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Duramen de glace x2 - Cœur de lapis x10 - Masque endommagé x15 |
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Duramen de glace x4 - Cœur de lapis x20 - Masque sale x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Duramen de glace x8 - Cœur de lapis x30 - Masque sale x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Duramen de glace x12 - Cœur de lapis x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Duramen de glace x20 - Cœur de lapis x60 - Masque sinistre x24 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_chongyun.png) Fléau des démons | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 coups d'épée. |
| Attaque chargée : | Exécute une succession d'attaque tournoyantes ; cette attaque consomme de l'endurance sur la durée. L'attaque chargée se termine sur un coup puissant. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_chongyun.png) Lame de l'esprit : Givre superposé | Aptitude de combat |
|---------|-------------|
| | Chongyun abat violemment sa longue épée sur le sol, provoquant une explosion de glace qui inflige des DGT Cryo aux ennemis en face dans une zone circulaire. Après un court délai, les courants d'air froids de l'explosion se condensent en un champ de givre. Les DGT des attaques normales et chargées des personnages qui s'y trouvent et qui manient l'épée longue, à une main, ou une arme d'hast obtiennent un enchantement Cryo. « Lorsque les cloches sonnent, tout est calme et silencieux. Les eaux des lacs et des rivières ne s'écoulent plus, et les nuages sont couverts de givre épais. Les portes de l'autel s'ouvrent, tous les maux sont éliminés. » |

|![aptitude 3](imagee/aptitude/aptitude_3_chongyun.png) Lame de l'esprit : Chute d'étoiles | Aptitude de combat |
|---------|-------------|
| | D'un geste rapide, Chongyun invoque en l'air 3 lames éthérées qui foncent au sol après un court délai. L'impact inflige des DGT Cryo de zone et projette les ennemis en l'air. « Ô lame du ciel et de la terre qui gèle tous les démons du monde… Entends mon appel ! » |

|![aptitude 4](imagee/aptitude/aptitude_4_chongyun.png) Respiration stable | Aptitude passive |
|---------|-------------|
| | Dans le champs de Lame de l'esprit : Givre superposé, la VIT d'attaque normale des personnages maniant une épée à une ou deux mains ou une arme d'hast augmente de 8%. |

|![aptitude 5](imagee/aptitude/aptitude_5_chongyun.png) Lame du chasseur de glace | Aptitude passive |
|---------|-------------|
| | Une lame apparaît à la fin du champ de Lame de l'esprit : Givre superposé ; elle inflige des DGT Cryo de zone équivalent à 100% des DGT de Lame de l'esprit : Givre superposé aux ennemis proches. La RÉS Cryo des ennemis touchés est réduite de 10% pendant 8 s. |

|![aptitude 6](imagee/aptitude/aptitude_6_chongyun.png) Périple alyzéen | Aptitude passive |
|---------|-------------|
| | Réduit la durée d'une expédition de 25% lorsqu'elle se déroule à Liyue. |

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_chongyun.png) Glace déchaînée | Constellation Niv.1 |
|---------|-------------|
| | Le dernier coup d'attaque normale libère 3 lames de glace vers l'avant ; chacune inflige une quantité de DGT Cryo équivalent à 50% de l'ATQ de Chongyun. |

|![constellation 2](imagee/constel/constelation_2_chongyun.png) Révolution astrale | Constellation Niv.2 |
|---------|-------------|
| | Lancer une compétence ou un déchaînement élémentaires dans le champ de Lame de l'esprit : Givre superposé réduit leur TdR de 15%. |

|![constellation 3](imagee/constel/constelation_3_chongyun.png) Étincellement des nuages | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Lame de l'esprit : Chute d'étoiles +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_chongyun.png) Cieux givrés | Constellation Niv.4 |
|---------|-------------|
| | Chongyun récupère 1 pt d'énergie à chaque fois qu'il touche un ennemi affecté par Cryo. Cet effet peut être déclenché toutes les 2 s. |

|![constellation 5](imagee/constel/constelation_5_chongyun.png) La Voie | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Lame de l'esprit : Givre superposé +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_chongyun.png) Rassemblement des Quatre lames | Constellation Niv.6 |
|---------|-------------|
| | Lame de l'esprit : Chute d'étoiles inflige 15% de DGT supplémentaires aux ennemis dont les PV sont inférieurs à ceux de Chongyun. De plus, une lame supplémentaire est générée à l'activation de la compétence. |

## Skin