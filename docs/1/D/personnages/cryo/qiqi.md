!!! info inline end "Identité"
     Qiqi est une jeune apprentie du Cottage Bubu, en charge notamment de la récolte des herbes médicinales.
     C'est également un zombie au teint blafard et aux mots comptés.
     En raison de sa mémoire défaillante, Qiqi porte toujours sur elle un carnet. Il s'agit là d'une aide indispensable lui permettant de mener une vie à peu près normale.

![Qiqi](https://images4.alphacoders.com/117/thumbbig-1173274.webp)

## Histoire

=== "Histoire 1"

     De manière générale, les zombies ont un corps froid et rigide, ce qui explique leur déplacement lent et saccadé.

     Afin de remédier à cet inconvénient, Qiqi suit un entraînement régulier et soutenu alliant étirements et gymnastique.

     Hu tao, directrice de la 77e génération du Funérarium Wangsheng, ne voit point l’utilité de toutes ces simagrées, et ne manque pas lorsqu’elle croise Qiqi de lui dire _« Tu serais mieux si tu me laissais t’enterrer une bonne fois pour toutes tu sais. »_

     Bien évidemment, Qiqi n’a aucune envie de finir sous terre, et continue donc ses exercices à la nuit tombée, loin de tout regard.

     Habitant un corps qui refuse de mourir, Qiqi possède en revanche une mémoire bien inférieure à celle de tout mortel.

     À moins qu’elle ne fasse preuve d’un effort surhumain, aucun visage ne demeure présent dans son esprit plus de trois jours. Cela n’est cependant pas une mauvaise chose. En effet, pouvoir oublier un événement malheureux peut être considéré par beaucoup comme une bénédiction.

     Quant à la personne dont Qiqi doit à tout prix se souvenir…

     Cette tête à claques, qu’elle voit tous les jours… Bah. Qiqi ne s’inquiète pas outre-mesure de sa mémoire défaillante. Peu importe, après tout.

=== "Histoire 2"

     Qiqi a beau avoir les dimensions d’une enfant, elle n’en demeure pas moins un zombie à la puissance surprenante. Sa petite taille n’est point un désavantage, et lui permet au contraire de se mouvoir avec rapidité.

     En combat, Qiqi s’affranchit du self-control dont elle doit faire preuve tous les jours et devient un redoutable zombie à la vitesse de déplacement et à la force accrues.

     _« Tu es mort »_

     Une fois qu’elle est venue, de manière particulièrement stoïque, à bout de ses ennemis, elle reprend contrôle de ses muscles, et retourne à son état habituel.

     Il lui arriva un jour d’être menacée par des Pilleurs de trésors alors qu’elle était partie chercher quelques herbes médicinales.

     Une petite fille toute seule dans la campagne fait une proie facile à première vue, non ?

     Nul doute que ses assaillants qui, quelques secondes plus tard, se retrouvaient tous volants dans les airs, se firent cette même remarque en attaquant.

     Comme quoi, il faut toujours éviter de juger les gens d’après leur apparence – les gens, ou les zombies.

=== "Histoire 3"

     Les zombies ne peuvent pas bouger sans ordre de la personne qui a procédé à leur réveil. Mais Qiqi n’a jamais été réveillée, au sens strict du terme, et fait partir d’une infime minorité de zombies capable de se donner leurs propres ordres.

     Les instructions de type « Attaque cet ennemi » ne posent pas problème ; mais celles comme « Va au Karst Jueyun, et escalade les parois rocheuses pour aller cueillir des herbes médicinales », plus complexes, peuvent se solder par un échec, sans pour autant que Qiqi soit en mesure d’abandonner. On la verra ainsi s’acharner à grimper, peu importe le nombre de fois qu’elle aura échoué.

     Annuler un de ses verra ordres est très simple : entourez-la de vos bras par-derrière, puis dites-lui « C’est toi que j’aime le plus », ou toute phrase approchant, et l’ordre sera annulé.

     Malheureusement, ces mots, lorsque prononcés par M. Baishu, manquent quelque peu de sincérité, et ont un effet souvent limité.

     Qui sait, peut-être qu’un jour, quelqu’un comprenant que de tels mots ne peuvent être dits à la légère viendra, et pourra alors les prononcer doucement à son oreille…

     Quelle sera alors la réaction de Qiqi ?

=== "Histoire 4"

     Cette histoire a depuis longtemps été oubliée de tous. Une jeune fille répondant au nom de « Qiqi », partie un jour cueillir quelque herbe médicinale, se perdit et pénétra dans le domaine des Adeptes. S’étant blessée à la jambe lors d’une chute, l’enfant se réfugia dans une grotte pour se reposer. Toute occupée à bander ses plaies, elle pouvait cependant entendre des bruits n’appartenant pas à la dimension des mortels. Mais elle n’aurait jamais pu s’attendre à ce qui advint ensuite.

     Un bruit terrible éclata, et elle se retrouva à jamais prisonnière entre la vie et la mort.

     Adeptes comme démons, le bien comme le mal avaient compris qu’elle n0létait qu’une simple passante innocente, victime malheureusement de feux croisés. Peut-être que ces circonstances expliquent l’octroi de l’œil divin qu’elle reçut alors qu’elle allait mourir ; peut-être fut-il dû à quelque volonté supérieure…

     Mais toujours est-il que cet épisode mis fin à la bataille opposant Adeptes et démons. Les Adeptes, attristés de voir cette enfant injustement frappée, insufflèrent chacun en elle une part de la force les habitant, lui permettant de ressusciter dans le monde des mortels.

     Ceci s’avéra très efficace, voire peut-être trop efficace. Qiqi, à peine réveillée et incapable de maîtriser l’immense énergie qu’accueillait à présent son corps ressuscité, entra dans un état de folie furieuse…

     Et Taille-Monts n’eut d’autre alternative pour ramener la paix que de l’emprisonner dans de l’ambre.

=== "Histoire 5"

     Plusieurs centaines d’années passèrent, jusqu’au jour où quelqu’un, l’ayant découverte toujours prisonnière dans son ambre, la ramena au Funérarium Wangsheng afin de lui offrir des funérailles convenables.

     Les chemins de montagne étaient difficiles, et le sceau emprisonnant l’enfant avait perde de son efficacité au fil des ans ; l’ambre contenant la jeune fille se fissura.

     Se réveillant au milieu de la nuit, Qiqi finit de détruire l’ambre, puis s’enfuit sous le couvert de l’obscurité.

     Obéissant à ses anciennes habitudes, elle se dirigea vers les collines où elle avait l’habitude de cueillir des herbes.

     Par chance, elle tomba en chemin sur Baishu, le gérant du Cottage Bubu, qui la prit sous son aile. Baishu, s’il était un docteur de renom, n’était guère un homme de courage ou au caractère trempé.

     Cela ne l’empêcha pas de prendre soin de Qiqi, ne se souciant pas du fait que sa mémoire défaillante ne lui permettait même pas de trier les herbes médicinales de la pharmacie. Il semblerait d’ailleurs que la tolérance dont fait preuve Baishu à l’égard de Qiqi ne soit pas étrangère à quelque aspiration personnelle.

     Bien que possédant un caractère quelque peu rigide, Qiqi n’en est pas moins perspicace, et a compris cette dernière vérité ; mais elle ne s’y attarde pas pour autant.

     Peut-être est-ce dû au fait qu’elle est restée si longtemps seule ou qu’elle a vu trop de choses… toujours est-il qu’elle chérit l’attention qu’il lui porte, quand bien même cette dernière cacherait quelque motif intéressé.

## Elevation

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Muguet bleu x3 - Parchemin divinatoire x3
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Duramen de glace x2 - Muguet bleu x10 - Parchemin divinatoire x15 |
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Duramen de glace x4 - Muguet bleu x20 - Parchemin sigillé x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Duramen de glace x8 - Muguet bleu x30 - Parchemin sigillé x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Duramen de glace x12 - Muguet bleu x45 - Parchemin maudit x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Duramen de glace x20 - Muguet bleu x60 - Parchemin maudit x24 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_qiqi.png) Escrime ancienne des nuages | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Consomme de l'endurance pour déclencher deux coups d'épée rapides. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_qiqi.png) Arcane d'Adepte : Héraut de givre | Aptitude de combat |
|---------|-------------|
| | Utilise un talisman formé de glace et de sang pour invoquer un Héraut de givre, infligeant des DGT Cryo aux ennemis à proximité. |
| Héraut de givre | Les attaques normales et chargées de Qiqi restaurent les PV de vos personnages dans l'équipe ainsi que des alliés proches lorsqu'elles touchent. La quantité de PV restaurée est proportionnelle à l'ATQ de Qiqi. Régénère les PV de votre personnage déployé sur la durée. Le héraut restaure périodiquement les PV de Qiqi. Le héraut se déplace en suivant Qiqi et inflige des DGT Cryo aux ennemis sur sa route. _« C'est parfait pour prévenir la pourriture de la chair… C'est ça, je parle bien des fameuses herbes de M. Baishu ! »_

|![aptitude 3](imagee/aptitude/aptitude_3_qiqi.png) Arcane d'Adepte : Talisman sacré | Aptitude de combat |
|---------|-------------|
| | Qiqi libère le pouvoir d'Adepte scellé en elle et marque les ennemis d'un talisman qui leur inflige des DGT Cryo. |
| Talisman | Les personnages récupèrent des PV équivalents aux DGT subis par les ennemis marqués. _« Je suis l'envoyée de la Providence, le symbole de la renaissance. »_ — Qiqi est bien incapable de se souvenir de cette phrase. |

|![aptitude 4](imagee/aptitude/aptitude_4_qiqi.png) Survie en milieu hostile | Aptitude passive |
|---------|-------------|
| Arcane d'Adepte : | Héraut de givre confère un bonus de 20% de soins pendant 8 s lorsque le personnage déclenche une réaction élémentaire. |

|![aptitude 5](imagee/aptitude/aptitude_5_qiqi.png) Ouverture des arcanes | Aptitude passive |
|---------|-------------|
| | Les attaques normales et chargées de Qiqi ont 50% de chance de marquer l'ennemi touché d'un talisman pendant 6 s. Cet effet peut se déclencher une fois toutes les 30 s. |

|![aptitude 6](imagee/aptitude/aptitude_6_qiqi.png) souvenir d'une vie lointaine | Aptitude passive |
|---------|-------------|
| | Affiche l'emplacement des produits de Liyue sur la mini-carte. |

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_qiqi.png) Ascèse du givre | Constellation Niv.1 |
|---------|-------------|
| | Qiqi récupère 2 pts d'énergie élémentaire à chaque fois que son Héraut de givre touche un ennemi marqué par un talisman. |

|![constellation 2](imagee/constel/constelation_2_qiqi.png) Gelé jusqu'à l'os | Constellation Niv.2 |
|---------|-------------|
| | Les attaques normales et chargées de Qiqi infligent 15% de DGT supplémentaires contre les ennemis affactés par Cryo. |

|![constellation 3](imagee/constel/constelation_3_qiqi.png) Louange vers le ciel | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Arcane d'Adepte : Talisman sacré +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_qiqi.png) Répression divine | Constellation Niv.4 |
|---------|-------------|
| | L'ATQ des cibles marquées par un talisman est réduite de 20%. |

|![constellation 5](imagee/constel/constelation_5_qiqi.png) Éclosion du lotus rouge | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Arcane d'Adepte : Héraut de givre +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_qiqi.png) Symbole de la renaissance | Constellation Niv.6 |
|---------|-------------|
| | L'activation d'Arcane d'Adepte : Talisman sacré réanime tous les personnages de l'équipe à proximité et restaure 50% de leurs PV. Cet effet peut être déclenché une fois toutes les 15 min. |

## Skin