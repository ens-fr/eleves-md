
!!! info inline end "Identité"
    Rosalia, une sœur de l'Église de Favonius de la ville de Mondstadt.
    Une étrange religieuse qui n'en a que l'apparence. Ses paroles sont acérées et son comportement froid.
    Imprévisible, elle part souvent sans prévenir personne et semble agir pour le bien d'une mission dont personne n'a connaissance...
    Au premier abord, rien ne semble la différencier de ses collègues. Cependant, vous vous rendrez vite compte que son comportement n'a rien à voir avec celui d'une religieuse.

![Rosalia](https://images2.alphacoders.com/115/thumbbig-1152996.webp)

## Histoire
---

...

## Elevation

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000  - Éclat de jade shivada x1 - Tombaie x3 - Insigne de nouvelle recrue x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Duramen de glace x2 - Tombaie x10 - Insigne de nouvelle recrue x15|
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Duramen de glace x4 - Tombaie x20 - Insigne de sergent x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Duramen de glace x8 - Tombaie x30 - Insigne de sergent x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Duramen de glace x12 - Tombaie x45 - Insigne d'officier x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Duramen de glace x20 - Tombaie x60 - Insigne d'officier x24 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_rosalia.png) Lance expiatoire | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups de lance. |
| Attaque chargée : | Consomme de l'endurance pour charger en ligne droite et infliger des DGT aux ennemis sur la route. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_rosalia.png) Confession des péchés | Aptitude de combat |
|---------|-------------|
| | Rosalia de déplace rapidement derrière sa cible qu'elle transperce et tranche de sa lance, infligeant des DGT Cryo. Ce mouvement ne fonctionne pas avec les ennemis trop grands. Rosalia ne pardonne pas les péchés de ceux qui. se confessent à elle. Au contraire, plus le péché est grand, plus sa punition glaciale est implacable. |

|![aptitude 3](imagee/aptitude/aptitude_3_rosalia.png) Sacrement mortel | Aptitude de combat |
|---------|-------------|
| | Rosalia officie pour une priére pas comme les autres. Aprés avoir exécuté, une attaque circulaire balayant les ennemis à proximité, Rosalia invoque une lance de glace qui frappe le sol. Les deux actions infligent des DGT Cryo. Tant que la lance de glace est active, elle émet de faćon intermittente des ondes de frois, qui infligent des DGT Cryo aux ennemis à proximité. Un procés est bien trop long, autant en arriver directement à la sentence est donner les derniers sacrements. Rosalia ne croyant pas à l'Archon Anémo, c'est d'autant plus accommodant. |

|![aptitude 4](imagee/aptitude/aptitude_4_rosalia.png) Confession forcée | Aptitude passive |
|---------|-------------|
| | Le taux CRIT de Rosalia augmente de 12 % pendant 5 s lorsqu'elle frappe un ennemi de dos avec Confession des péchés. |

|![aptitude 5](imagee/aptitude/aptitude_5_rosalia.png) Samaritain de l'ombre | Aptitude passive |
|---------|-------------|
| | Exécuter Sacrement mortel confére à tous les personnages de l'équipe proches (hormis Rosalia) un bonus de taux CRIT équivalant à 15 % du taux CRIT de Rosalia pendant 10 s. Le bonus de taux CriT ainsi obtenu ne peut pas exéfer 15 %. |

|![aptitude 6](imagee/aptitude/aptitude_6_rosalia.png) Balade nocturne | Aptitude passive |
|---------|-------------|
| | À nuit (18:00 - 6:00), augmente le VIT de l'équipe de 10%. Ne prend pas effet dans les domaines, les domaines de Trounce ou les abysses en spirale. Non cumulable avec les talents passifs qui fournissent les mêmes effets.|

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_rosalia.png) Guide du pécheur | Constellation Niv.1 |
|---------|-------------|
| | Lorsque les attaques de Rosalia inglent un coup CRIT, sa VIT d'attaque et les DGT des attaques normales augmentent de 10 % pendant 4 s. |

|![constellation 2](imagee/constel/constelation_1_rosalia.png) Terre sacrée | Constellation Niv.2 |
|---------|-------------|
| | Prolonge la durée de la lance de glace de Sacrement mortel de 4 s. |

|![constellation 3](imagee/constel/constelation_3_rosalia.png) Rite de confession | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Confession des péchés +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_rosalia.png) Souffrance bénie | Constellation Niv.4 |
|---------|-------------|
| | Rosalia récupère 5 pts d'énergie élémentaire lorsque Confession des péchés inflige un coup CRIT. Cet effet ne peut être déclenché qu'une fois par Confession des péchés. |

|![constellation 5](imagee/constel/constelation_5_rosalia.png) Prière agonisante | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Sacrement mortel]] +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_rosalia.png) Châtiment divin | Constellation Niv.6 |
|---------|-------------|
| | Les attaques de Sacrement mortel réduisent la RÉS physique des ennemis de 20 % endant 10 s. |

## Skin

