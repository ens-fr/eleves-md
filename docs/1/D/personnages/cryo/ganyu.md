
!!! info inline end "Identité"
    La secrétaire des Sept Étoiles. Dans ses veines coule du sang de Qilin, la créature mythique.
    De nature gracieuse et calme, les Qilin ont souvent un caractère doux et ne voient aucun problème à affronter les tâches les plus fastidieuses.
    Ganyu croit fermement que le travail qu'elle effectue honore le contrat qu'elle a passé avec le Souverain de la Roche, et assure le bien-être d'autant de Liyuéens que possible.

![Ganyu](https://images4.alphacoders.com/113/thumbbig-1130246.webp)

## Histoire

=== "Histoire"

    ...

## Elevation
---
| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Qingxin x3 - Nectar de Fleur mensongère x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Duramen de glace x2 - Qingxin x10 - Nectar de Fleur mensongère x15 |
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Duramen de glace x4 - Qingxin x20 - Nectar miroitant x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Duramen de glace x8 - Qingxin x30 - Nectar miroitant x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Duramen de glace x12 - Qingxin x45 - Nectar élémentaire x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Duramen de glace x20 - Qingxin x60 - Nectar élémentaire x24 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_ganyu.png) Archerie Liutian | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 6 tirs consécutifs. |
| Attaque chargée : | Effectue un tir visé plus précis et infligeant davantage de DGT. Lors du tir visé, la puissance du froid vient se concentrer sur la pointe de la flèche. Les effets varient selon le nombre de charges : 1 charge : Tire une flèche glaciale qui inflige des DGT Cryo.  2 charges : Tire une flèche de givre qui inflige des DGT Cryo. Le givre se répand lorsqu'il touche un ennemi, infligeant des DGT Cryo de zone. |
| Attaque descendante : | Tire une pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de    zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_ganyu.png) Trace de Qilin | Aptitude de combat |
|---------|-------------|
| | Ganyu dépose un lotus de glace qui inflige des DGT Cryo de zone puis part vite en retraite. |
| Lotus de glace | Le lotus nargue les ennemis et concentre leurs attaques. Ses PV sont proportionnels aux PV max de Ganyu ; Le lotus explose à la fin ou s'il est détruit entre temps, et inflige des DGT Cryo de zone. « Vous m'avez aperçue ? Ça doit être une erreur, je suis restée tard au travail. » |

|![aptitude 3](imagee/aptitude/aptitude_3_ganyu.png) Baptême céleste | Aptitude de combat |
|---------|-------------|
| | Condensant les particules présentes dans l'air, Ganyu invoque une perle de glace magique qui exorcise les démons. Tant qu'elle est active, la perle libère des éclats de glace en continu qui infligent des DGT Cryo de zone aux ennemis dans la zone. Ganyu sait bien sûr comment invoquer une bruine rafraîchissante ; mais quand il s'agit de combattre, la bruine se change en tempête de neige. |

|![aptitude 4](imagee/aptitude/aptitude_4_ganyu.png) Cœur indivisible | Aptitude passive |
|---------|-------------|
| | Tirer une Flèche de givre confère pendant 5 s aux Flèches de givres suivantes ainsi qu'aux zones de givre ainsi créées un bonus de 20% de taux CRIT. |

|![aptitude 5](imagee/aptitude/aptitude_5_ganyu.png) Harmonie du ciel et de la terre | Aptitude passive |
|---------|-------------|
| | Les personnages déployés se trouvant dans la zone de Baptême céleste obtiennent un bonus de 20% de DGT Cryo supplémentaires. |

|![aptitude 6](imagee/aptitude/aptitude_6_ganyu.png) Arc de réserve | Aptitude passive |
|---------|-------------|
| | Lorsque vous forgez un arc, 15% du minerai utilisé vous sera rendu. |

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_ganyu.png) Buveur de rosée | Constellation Niv.1 |
|---------|-------------|
| | La RÉS Cryo des ennemis touchés par le coup CRIT d'une Flèche de givre chargée 2 fois ou par la zone de givre est réduite de 15% pendant 6 s. De plus, toucher ainsi l'ennemi d'un coup CRIT grâce à une chargée restaure 2 pts d'énergie élémentaire à Ganyu. Cet effet ne peut être déclenché qu'une fois par flèche chargée. |

|![constellation 2](imagee/constel/constelation_2_ganyu.png) Auspices | Constellation Niv.2 |
|---------|-------------|
| | Confère une charge supplémentaire à Trace de Qilin. |

|![constellation 3](imagee/constel/constelation_3_ganyu.png) Mouvement des nuages | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Baptême céleste +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_ganyu.png) Chasse vers l'ouest | Constellation Niv.4 |
|---------|-------------|
| | Baptême céleste inflige plus de DGT aux ennemis dans la zone d'effet ; cet effet se cumule avec le temps. Bonus de DGT initial : 5%. Ce bonus augmente de 5% toutes les 3 s (cumul max : 25%). L'effet perdure pendant 3 s après avoir quitté la zone. |

|![constellation 5](imagee/constel/constelation_5_ganyu.png) Miséricorde | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Trace de Qilin +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_ganyu.png) Clémence | Constellation Niv.6 |
|---------|-------------|
| | La première Flèche de givre libérée dans les 30 s suivant l'activation de Trace de Qilin ne requiert pas de charge. |

## Skin