
!!! info inline end "Identité"
    Reine du cocktail à la Queue de Chat, star de l'industrie du vin de Mondstadt, barmaid du renouveau.
    Tout alcool préparé par les soins de cette jeune fille originaire de Deauclaire, peu importent ses efforts, est toujours un breuvage digne de figurer sur les meilleures cartes.
    Mais s'agit-il pour elle qui rêve de détruire l'industrie du vin de Mondstadt d'une bénédiction, ou d'une malédiction ?

![Diona](https://images6.alphacoders.com/111/thumbbig-1113674.webp)

## Histoire

=== "Histoire"


    ...

## Elevation

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Lys calla x3 - Pointe de flèche robuste x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Duramen de glace x2 - Lys calla x10 - Pointe de flèche robuste x15 |
| 50 | - Moras 60 000 -Fragment de jade shivada x6 - Duramen de glace x4 - Lys calla x20 - Pointe de flèche aiguisée x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Duramen de glace x8 - Lys calla x30 - Pointe de flèche aiguisée x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Duramen de glace x12 - Lys calla x45 - Pointe de flèche usée x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Duramen de glace x20 - Lys calla x60 - Pointe de flèche usée x24 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_diona.png) Archerie de chasse | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 tirs consécutifs. |
| Attaque chargée : | Effectue un tir visé plus précis infligeant davantage de DGT. Lors de la visée, la flèche se charge en élément Cryo, infligeant de considérables DGT Cryo quand elle est complètement chargée. |
| Attaque descendante : | Tire une pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_diona.png) Griffes Cryo | Aptitude de combat |
|---------|-------------|
| | Libère des griffes de chat Cryo qui infligent des DGT Cryo, et génèrent un bouclier lorsqu'elles touchent. Le bouclier peut absorber une quantité de DGT proportionnelle aux PV max de Diona, et sa durée dépend du nombre de cibles touchées. |
| Appui simple | Exécute rapidement deux attaques consécutives de Griffes Cryo. |
| Appui long | Bat rapidement en retraite, et libère 5 Griffes Cryo. Le bouclier généré lors d'un appui long obtient un bonus de 75% d'absorption des DGT. Le bouclier est efficace à 250% contre les DGT Cryo, et applique l'élément Cryo au personnage déployé pendant un court instant lorsque le bouclier est généré. En théorie, la glace versée dans un breuvage est sans danger pour celui qui le boit. Ceci était exact jusqu'à ce que Diona utilise son œil divin pour créer une glace capable de geler jusqu'au cerveau… 5 étoiles ! |

|![aptitude 3](imagee/aptitude/aptitude_3_diona.png) Cuvée spéciale | Aptitude de combat |
|---------|-------------|
| | Diona lance un cocktail gelé de sa fabrication, qui inflige des DGT Cryo de zone, et génère un champ de vapeur d'alcool. |
| Champ de vapeur d'alcool | Inflige aux ennemis présents dans la zone des DGT Cryo continus ; Restaure les PV des personnages présents dans la zone. Une recette que même Diona juge inhumaine. L'avant-goût terrifiant est mieux laissé aux ennemis ; nous ne parlerons pas de l'arrière-goût particulièrement frais qui en découle. |

|![aptitude 4](imagee/aptitude/aptitude_4_diona.png) Menu secret de La Queue de Chat | Aptitude passive |
|---------|-------------|
| | Augmente de 10% la VIT de déplacement des personnages sous la protection du bouclier généré par la Griffes Cryo. Leur coût d'endurance est de plus réduit de 10%. |

|![aptitude 5](imagee/aptitude/aptitude_5_diona.png) Joyeuse ébriété | Aptitude passive |
|---------|-------------|
| | Les ennemis entrant dans le champ de vapeur d'alcool de Cuvée spéciale voient leur ATQ baisse de 10% pendant 15 s. |

|![aptitude 6](imagee/aptitude/aptitude_6_diona.png) Apéritif offert | Aptitude passive |
|---------|-------------|
| | 12% de chance d'obtenir un plat supplémentaire lorsque vous cuisinez un plat de soins parfait. |

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_diona.png) Cocktail maison | Constellation Niv.1 |
|---------|-------------|
| | Diona récupère 15 pts d'énergie élémentaire à la fin de Cuvée spéciale. |

|![constellation 2](imagee/constel/constelation_2_diona.png) On the rocks | Constellation Niv.2 |
|---------|-------------|
| | Les DGT infligés par Griffes Cryo augmentent de 15%, et le bouclier absorbe 15% de DGT en plus. De plus, la compétence confère pendant 5 s lorsqu'elle touche un bouclier aux autres personnages déployés à proximité, dont le pouvoir absorbant correspond à la moitié de celui de Griffes Cryo. |

|![constellation 3](imagee/constel/constelation_3_diona.png) Un dernier pour la route | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Cuvée spéciale +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_diona.png) Mixologie | Constellation Niv.4 |
|---------|-------------|
| | Le temps de recharge du tir visé de Diona est réduit de 60% lorsqu'elle se trouve dans le champ de vapeur d'alcool de Cuvée spéciale. |

|![constellation 5](imagee/constel/constelation_5_diona.png) Un double avec glaçons | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Griffes Cryo +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_diona.png) Dernière tournée | Constellation Niv.6 |
|---------|-------------|
| | Les personnages se trouvant dans le champ d'alcool de Cuvée spéciale bénéficient des effets suivants selon les PV qu'il leur reste : Bonus de 30% de soins reçus si les PV sont à 50% ou en dessous ; Maîtrise élémentaire augmentée de 200 si les PV sont au-dessus de 50%. |

## Skin