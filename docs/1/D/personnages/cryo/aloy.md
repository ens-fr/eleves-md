!!! info inline end "Identité"
    Une chasseuse agile originaire de la Tribu Nora. L'arc à la main, elle est toujours prête à protéger les innocents.
    « Tout ce que je fais est au service de la vie, pas de la mort ! C'est pourquoi je suis ici avec vous. »

![Aloy](imagee/aloy.jpg)

## Histoire

=== "Histoire"


    Paria depuis sa naissance, Aloy a grandi dans de dangereuses montagnes sauvages près de la tribu qui l'a excommuniée. Élevée par un chasseur expert, elle s'est entraînée à chasser avec une grâce féline et une précision mortelle... Mais il était incapable de lui enseigner les choses qu'elle désirait le plus ardemment apprendre. Elle brûlait surtout de connaître les circonstances de sa naissance : qui étaient ses parents et pourquoi elle avait été rejetée par la tribu.

    Sa quête de réponses l'a entraînée dans un monde plus vaste et plus dangereux qu'elle n'aurait pu l'imaginer. Elle rencontra de nouvelles tribus étranges et puissantes, d'anciennes ruines pleines de mystères et de dangereux ennemis, humains et machines.

    Elle découvrit finalement que ses origines et son destin étaient profondément liés au destin du monde lui-même, et elle dut mener une bataille épique pour le sauver des forces maléfiques d'une intelligence artificielle du passé.

    Elle pensait alors que ce serait la fin de son voyage. Mais il y a toujours un chapitre de plus dans le récit d'une aventurière... Elle est maintenant venue en Teyvat à la recherche de nouveaux défis.

    Dans ce nouveau monde, Aloy est prête pour la chasse !

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Moelle cristalline x3 - Coquille spectrale x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Fleur cristalline x2 - Moelle cristalline x10 - Coquille spectrale x15 |
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Fleur cristalline x4 - Moelle cristalline x20 - Cœur spectral x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Fleur cristalline x8 - Moelle cristalline x30 - Cœur spectral x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Fleur cristalline x12 - Moelle cristalline x45 - Noyau spectral x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Fleur cristalline x20 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_aloy.png) Tir Rapide | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 tirs consécutifs. |
| Attaque chargée : | Effectue un tirs visé plus precis infligeant davantage de DGT. Lors de la visée, la flèche se charge en élément Cryo, infligeant d'importants DGT Cryo, quand elle est complétement chargée. |
| Attaque descendante : | Tire un pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_aloy.png) Frozen Wilds | Aptitude de combat |
|---------|-------------|
| Aloy lance une bombe à gel qui explose à l'impact dans la direction ciblée, infligeant des DGT Cryo. Après avoir explosé, la bombe à gel se divise en de nombreuses petites bombed glacées qui explosent au contact des ennemis ou après un court délai, infligeant des DGT Cryo. Lorsqu'une bombe à gel ou une petite bombe glacée touche un ennemi, l'ATQ de ce dernier est réduite et Aloy obtient 1 charge de bobine. Une charge de bobine peut être obtenue toutes les 0,1 s max. |

|![aptitude 3](imagee/aptitude/aptitude_3_aloy.png) Prophéties de l'aube | Aptitude de combat |
|---------|-------------|
| | Aloy lance un batterie Cryo dans la direction ciblée, puis la fait exploser avec une flèche, infligeant des DGT Cryo de zone Que l'Aube zéro fasse référence à l'espoir ou à l'extinction n'a plus d'importance. Aloy a déja sauvé son monde, et celui-ci a ses propres héros pour s'inquiéter de son sort. |

|![aptitude 4](imagee/aptitude/aptitude_4_aloy.png) Surcharge offensive | Aptitude passive |
|---------|-------------|
| | Lorsqu'Aloy est sous l'effet d'une bobine de Frozen Wilds, son ATQ augmente de 16 %, tandis que celle des personnages de l'équipe à proximité augmente de 8 %. Cet effet dure 10 s. |

|![aptitude 5](imagee/aptitude/aptitude_5_aloy.png) Frappe puissante | Aptitude passive |
|---------|-------------|
| | Losqu'Aloy est dans l'état de Glace précipitée accordé par Frozen Wilds, son bonus de DGT Cryo augmente de 3,5 % toutes les secondes. Son bonus de DGT Cryo peut augmenter de cette maniére jusqu'à 35 %. |

|![aptitude 6](imagee/aptitude/aptitude_6_aloy.png) Doucement mais sûrement | Aptitude passive |
|---------|-------------|
| | Quand Aloy fait partie de l'équipe, vos personnages n'effraient pas les petits animaux en s'approchent, et pouvant ainsi obtenir de la volaille, de la viande crue et de la viande congelée. |

## Constellations
---

**Aucune (pour le moment)**

## Skin
