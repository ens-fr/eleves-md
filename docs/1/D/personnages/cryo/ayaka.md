
!!! info inline end "Identité"
    Fille du clan Kamisato de la Commission culturelle. Digne et élégante, mais aussi sage et forte.
    Ayaka traite tout le monde avec respect et gentilllesse, ce qui lui vaut la révérence des habitants d'Inazuma. Ces derniers la surnomment « Shirasagi Himegimi.

![Ayaka](https://images4.alphacoders.com/116/thumbbig-1160941.webp)

## Histoire

=== "Histoire"

    Ayaka est la fille du clan Kamisato de la Commission culturelle, louée pour sa beauté et sa noblesse de caractère.

    Elle partage les responsabilités de son clan avec Ayato, son frère. Celui-ci en est le chef et s'occupe principalement des affaires politiques, tandis qu'elle gère les activités internes et externes du clan.

    Ayaka est une jeune demoiselle gentille et polie, dotée d'un grand cœur. Étant proche de ses concitoyens, elle s'enquiert souvent personnellement de leurs problèmes, et par sa nature consciencieuse, s'efforce toujours de les résoudre de la meilleure manière possible. Touchés par sa bonté, ceux-ci lui donnèrent le surnom de « Shirasagi Himegimi ». Tous ses voisins laissent également transparaître une certaine admiration dans leur voix lorsqu'ils parlent d'elle.

    L'excellente éducation qu'Ayaka a reçue fait qu'elle possède un cœur aussi pur que le cristal. Lorsqu'elle projette des cristaux de glace en hiver, ceux-ci prennent des couleurs éblouissantes, à l'image de son âme. Bien qu'elle montre souvent son côté délicat et réservé, Ayaka possède une facette plus chaleureuse et attachante que peu de gens ont le privilège de voir.

    Ceux qui aspirent à se tenir au sommet doivent avoir la force d'atteindre les nuages ! Ainsi, Ayaka n'hésite pas à se lier d'amitié avec des personnes de talent. Elle considère ses amis comme des cadeaux du destin venus illuminer sa vie, comme la lumière qui se reflète sur l'épée ou la lueur de la rosée sur le givre.

## Elevation

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Fleur de cerisier x3 - Garde-main ancien x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Cœur perpétuel x2 - Fleur de cerisier x10 - Garde-main ancien x15 |
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Cœur perpétuel x4 - Fleur de cerisier x20 - Garde-main kageuchi x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Cœur perpétuel x8 - Fleur de cerisier x30 - Garde-main kageuchi x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Cœur perpétuel x12 - Fleur de cerisier x45 - Garde-main célèbre x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Cœur perpétuel x20 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_ayaka.png) École Kamisato : Fleur de glace | Aptitude de combat |
|---------|-------------|
| | Ayaka invoque une fleur glacée qui projette les ennemis proches, leur infligeant des DGT Cryo de zone. « Les nuages enveloppent la lune, le désir descend tel un brouillard, emplissant le cœur du voyageur. » |

|![aptitude 2](imagee/aptitude/aptitude_2_ayaka.png) École Kamisato : Sentier de givre | Aptitude de combat |
|---------|-------------|
| Remplace le sprint | Ayaka consomme de l'endurance pour se déplacer rapidement au milieu d'un brouillard de glace. Sous Sentier de givre, Ayaka peut se déplacer rapidement sur l'eau. À la fin de l'état, elle réapparaît ; deux choses se produisent alors simultanément : Elle libère une énergie glacée qui applique aux ennemis proches l'élément Cryo. Le froid se concentre sur son épée : les DGT qu'inflige cette dernière sont momentanément convertis en DGT Cryo. « Volent les ailes mouillées / Le héron blanc marche sur l'eau / D'une vision parfaite ». |

|![aptitude 3](imagee/aptitude/aptitude_3_ayaka.png) École Kamisato : Givre mortel | Aptitude de combat |
|---------|-------------|
| | Ayaka adopte une posture magnifique pour libérer un tourbillon givré qui avance sans s'arrêter 
| Tourbillon givré | * Le tourbillon givré lac-re de façon continue les ennemis qui se trouvent sur sa route, infligeant des DGT Cryo. Le tourbillon finit par exploser, infligeant des DGT Cryo de zone. Le blizzard dissimule les pensées du héron, au cœur rempli de pitié. |

|![aptitude 4](imagee/aptitude/aptitude_4_ayaka.png) Sanctification de l'impur | Aptitude passive |
|---------|-------------|
| | Confère aux attaques normales et chargées d'Ayaka suivant dans les 6 s le lancement d'École Kamisato : Fleur de glace un bonus de 30 % de DGT. |

|![aptitude 5](imagee/aptitude/aptitude_5_ayaka.png) Bénédiction de Kanten Senmyou | Aptitude passive |
|---------|-------------|
| | L'énergie glacée libérée à la fin du Sentier de givre d' École Kamisato : Sentier de givre confère à Ayaka les effets suivants lorsqu'elle touche l'ennemi : Régénération de 10 pts d'endurance ; Icône Cryo.png Bonus de DGT Cryo de 18 % pendant 10 s. |

|![aptitude 6](imagee/aptitude/aptitude_6_ayaka.png) Fruits de Shinsa | Aptitude passive |
|---------|-------------|
| | 10 % de chances d'obtenir le double de produits lors de la synthèse de matériaux d'élévation d'arme. |

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_ayaka.png) Cerise givrée | Constellation Niv.1 |
|---------|-------------|
| | Les attaques normales et chargées d'Ayaka ont 50 % de chances de réduire le TdR d'École Kamisato : Fleur de glace de 0,3 s lorsqu'elles infligent des DGT Cryo aux ennemis. Cet effet peut être déclencher une fois toutes les 0,1 s. |

|![constellation 2](imagee/constel/constelation_2_ayaka.png) Lame de blizzard Seki | Constellation Niv.2 |
|---------|-------------|
| | Deux petits tourbillons givrés supplémentaires sont libérés à l'activation d'École Kamisato : Givre mortel ; chacun inflige 20 % des DGT du tourbillon d'origine. |

|![constellation 3](imagee/constel/constelation_3_ayaka.png) Confettis de fleuraison givrée | Constellation Niv.3 |
|---------|-------------|
| | Niveau d’aptitude École Kamisato : Givre mortel +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_ayaka.png) Retour prééminent | Constellation Niv.4 |
|---------|-------------|
| | La DÉF des ennemis subissant les DGT infligés par le tuorbillon givré d'École Kamisato : Givre mortel est réduite de 30 % pendant 6 s. |

|![constellation 5](imagee/constel/constelation_5_ayaka.png) Lune entre les nuages | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence École Kamisato : Fleur de glace +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_ayaka.png) Danse de la lune | Constellation Niv.6 |
|---------|-------------|
| | Kamisato Ayaka s'illumine d'une gracieuse gelée toutes les 10 s, augmentant les DGT infligés par son attaque chargée de 298%. Cet effet s'annule 0,5 s après que l'attaque chargée d'Ayaka touche un ennemi, puis le chronomètre de cette capacité démarre. |

## Skin