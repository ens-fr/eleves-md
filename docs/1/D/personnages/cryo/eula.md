
!!! info inline end "Identité"
     Eula est la descendante rebelle de la vieille aristocratie qui vit sur le champ de bataille en tant que chevalier. Sa façon unique d'aborder le monde lui a permis de passer outre les préjugés à son encontre, la poussant à couper tout lien avec sa famille pour devenir le « Chevalier aux Embruns ». Elle pourchasse les ennemis de Mondsatdt dans les contrées sauvages dans le but d'assouvir son unique « vengeance ».

![Eula](imagee/eula.png)

## Histoire

=== "Histoire 1"

    Eula est le descendante de l'infâme Clan Lawrance, est c'est un héritage empoisonné que lui a laissé la vieille noblesse - mais elle est également une spécialiste de l'escrime comme on en voit peu à Mondstadt.

    Le sang du passé corrompu et la virtuosité au combat ont ensemble forgé cette femme.
    Elle attaque quand on s'y attend le moins, et qui sait ce qu'elle emporte avec elle... Aux yeux des citoyens de Mondstadt, la capitaine de l'unité de reconnaissance est aussi agréable que la mer qui couve avant la tempête.

    L'attitude d'Eula à cet égard consiste en un mélange de fière indifférence et de flegme. Qu'un malheureux ose la remettre en question, et elle se contente d'un simple : « Comment osez-vous. Je n'oublierai pas cet affront. »

    Obsédée par le ressentiment permanent, elle passe une bonne partie de l'année loin de la ville. En une rare occasion où elle venait à rentrer à Mondstadt, on la vit se diriger d'un pas pressé vers le quartier général de l'Ordre de Favonius, sa lourde épée à la main.

    À sa vue, la paralysie saisit les deux pauvres garde fraîchement débarqués ayant le malheur d'être de faction ce jour-l), qui, sagement, préférèrent maintenir une distance prudente à l'affronter de face, et ne purent que la regarder passer en trombe en direction du bureau de la Grande Maîtresse Jean.
    Mais le plus étrange, c'est que plusieurs heures passèrent ensuite sans qu'aucun bruit ne s'échappe du bureau.

    Les deux gades firent part de leurs légitimes inquiétudes à Kaeya, le capitaine de cavalerie des chevaliers de l'Ordre de Favonius qui passait justement par là en compagnie de Lisa, la bibliothécaire de l'Ordre.

    « - Ah, la capitaine de l'unité de reconnaissance est de retour ? Quand vous l'avez vue, est-ce qu'elle marchait à grands pas en direction du bureau de Jean avec sa grande épée à la main comme si elle allait tuer quelqu'un ?

    - Sacrebleu, je dois me dépêcher, comment osent-elles prendre le thé sans moi ? »

    Et sans perdre un instant, Lisa se mit à foncer vers le bureau.

    Kaeya, ramenant les deux sauveurs de Mondstadt à leur porte, leur fit en riant :
    « - Vous avez confiance dans le jugement de la Grande Maîtresse suppléante, si je ne m'abuse ? Aux yeux des chevaliers de Favonius, seul compte ce que vous savez faire, peu importe d'où vous venez. Et c'est pour cette raison qu'elle trouve toujours du temps à consacrer à la capitaine de l'unité de reconnaissance pour se perfectionner dans l'art de l'épée. Une façon comme une autre de faire honneur à la réputation des chevaliers, et de montrer ce qu'il est possible de faire table rase du passé. Une sage décision, si vous voulez mon avis.
    - Oh, hum... Je vois...

    - Alors ça veut dire ça « prendre le thé »... »

    Ce genre de scène se produit presque tous les mois. Mais quand bien même, on peut se demander... Faire table rase du passé, ce mystérieux entraînement dans l'art de l'épée, et pas un bruit ? Étrange...

=== "Histoire 2"


     Eula n'a pas exactement l'apparence qu'on associe généralement aux personnes dangereuses. Entourée de préjugés de toute part, elle donne parfois une impression de faiblesse en contraste.

     Au début, les magasins refusaient de la servir, les restaurants la trompaient souvent sur l'addition, et les personnes vivant dans la zone sous sa responsabilité étaient allergiques à là moindre idée de coopération. Les obstacles peuplent le travail d'Eula.

     En cas de conflit, Eula répond du tac au tac sans se départir de son sérieux, pour finir par dire qu'elle prend bonne note de l'affront qui lui est fait, et qu'elle repassera réclamer son dû un jour prochain. Ces paroles ont l'effet d'une malédiction et viennent clore la conversation avant qu'elle ne s'envenime.

     De manière assez intéressante, les gens redoublent de vigilance lorsqu'Eula est dans les parages, elle qui observe pourtant la loi à la lettre et n'a jamais fait le moindre mal aux citoyens de Mondstadt. Elle peut paraître distante, mais elle est aussi droite dans ses paroles que dans ses manières.

     Les gens finissent par réaliser qu'il est difficile de reprocher quoi que ce soit à sa conduite, et par ne plus avoir aussi peur, voyant en son discours de vengeance l'avertissement de ne pas franchir la ligne.

     Eula, qui pourrait bien être un agent infiltré chez les chevaliers... Eula, une taupe travaillant au service de l'aristocratie... Les nouveaux venus dans l'Ordre de Favonius ont bien du mal à se faire une idée de la personne.

     Une fraîche recrue chargée de lui apporter les ordres de Jean obtient toujours la même réponse nonchalante : « Alors, besoin de l'aide de la part de la fille de criminels ? Pas encore à la hauteur, je vois. »

     Mais malgré l'antagonisme exprimé par ses mots, elle s'acquitte toujours des missions qui lui sont confiées à la perfection, et même le porteur d'ordre est bien forcé d'admettre qu'avec de tels talents, prendre en l'espace de quelques années la tête de l'unité de reconnaissance n'a rien de surprenant.

     Tout le monde sait qu'Eula est le froid et distant Chevalier aux Embruns, le rejeton de la noblesse honnie, une personne dont il vaut mieux se méfier... Mais en est-il vraiment ainsi ?
     Sur le chemin du retour, la jeune recrue ne peut alors s'empêcher de se remémorer chaque mot, chaque mouvement de la capitaine de l'unité de reconnaissance.

     Cette expression de bonté et de résilience dans ses yeux, quand elle regardait ailleurs...
     Quelqu'un d'aussi sérieux, on doit bien pouvoir compter dessus, non... ?

=== "Histoire 3"


     Malgré tout, Mondstadt demeure la riante cité de la liberté qu'elle a toujours été, et même le rejeton d'une famille de criminels haïe de tous peut y trouver des amis.

     Eula et la population peuvent compter sur la loyale Éclaireuse Amber pour arrondir les angles. Lorsqu'Amber, que tout le monde apprécie, se trouve dans les environs, les prix des commerçants reviennent à la normale, et on en voit même qui vont jusqu'à discuter avec elle. Dans ces occasions, Eula peut alors se montrer charmante.

     C'est justement pour cette raison qu'Amber, qui a le cœur gros comme une montagne, aime à l'accompagner, et n'hésite pas à l'aider le cas échéant, lui rapportant elle-même ce dont elle a besoin.

     C'est Amber aussi qui se charge d'informer la populace des fréquents et lointains exploits de la capitaine de l'unité de reconnaissance, et il n'est pas rare qu'une surprise mêlée d'une teinte d'admiration gagne alors le public.

     En fin de semaine, aux petites heures du matin, Amber, debout sur une caisse en bois, déclame à la populace depuis sa tribune improvisée les derniers succès d'Eula :
     « Au cours des jours qui viennent de s'écouler, le capitaine de l'unité de reconnaissance des chevaliers de l'Ordre a sauvé d'une mort certaine une femme au Port de Dornman, et, après enquête et grâce au témoignage de la victime, a exposé et démantelé un réseau de membres de l'Ordre de l'Abîme opérant dans la région. Les jours de la victime, qui n'est autre qu'une célèbre juriste de Liyue, ne sont pas en danger. Le Bureau des communication de Liyue a exprimé sa gratitude à l'égard de notre capitaine dans un message adressé aux chevaliers de l'Ordre... » Peut-être est-ce l'acharnement dont fait preuve Amber à réparer les préjudices du passé, peut-être est-ce tout simplement que le poids de tous les succès accomplis par Eula en tant que chevalier de l'Ordre a fini par étouffer le feu de la défiance ancestrale qui brûlait à Mondstadt... Force est de reconnaître que sa vie a connu certains changements ces dernières années. L'agressivité de la population à son égard s'est progressivement éteinte, et sa réputation n'est plus à faire auprès des chevaliers de l'Ordre, ses compagnons d'armes.

     Mais personne ne lui est aussi fidèle que son unité de reconnaissance, qui la suit sans hésiter et lui procure un soutien indéfectible, veillant à ses côtés sur Mondstadt et ses environs, et éradiquant à sa suite toute menace à la tranquillité du lieu.

     Il va sans dire que nul n'est plus heureux qu'Amber d'avoir participé au changement, et que nul n'a dépensé autant d'énergie pour y parvenir. Après tout, les deux femmes se connaissaient bien avant qu'Eula rejoigne les chevaliers de l'Ordre, et Eula a de tout temps fait confiance à la petite-fille du père fondateur des Éclaireurs, que ce dernier chérissait et dont il fit sa propre apprentie.

=== "Histoire 4"

     La cuisine est l'un des talents d'Eula contrastant quelque peu avec sa morgue habituelle. Il faut bien l'admettre, elle sait préparer un bon repas.

     L'unité de reconnaissance a pour habitude de nos jours de s'appeler, non sans fierté, « le rata préféré des chevaliers ». Ses membres ont toujours dans leur barda une tourte en forme de lune, et ce type de ration est un plaisir pour le palais, comme peuvent d'ailleurs en témoigner tous ceux qui ont eu la chance d'y goûter.

     La jeune cuisinière affectée à l'unité expliqua un jour s'être inspirée des tourtes faites par la capitaine pour créer ces rations, en augmentant le temps de cuisson pour une meilleure élasticité de la pâte en remplaçant les ingrédients de la recette originale ayant du mal à se conserver. Ceci lui permit non seulement d'en accroître considérablement la durée de vie, mais aussi d'en réduire le coût de manière significative.

     Cependant, quand l'unité en déguste une, nul ne peut s'empêcher de tomber dans une profonde rêverie : vous imaginez, si c'est ça quand c'est des rations, ce que doivent valoir les tourtes de lune quand c'est ma capitaine qui les fait... ?

     La réponse à cette question se cache dans les anciens ouvrages de la bibliothèque. Le déclin prononcé de la famille Lawrence n'empêche pas ceux qui la composent d'avoir continué à rêver de reprendre un jour le pouvoir et de dominer à nouveau la cité. Et pour que tout se passe à la perfection et que rien ne soit laissé au hasard, ce jour dut-il arriver, leurs enfants doivent subir une éducation stricte d'une cruauté confinant à la pathologie. Les soi-disant « obligations aristocratiques » requièrent une perfection dans tous les domaines - maintien, étiquette, instruction, mais également l'art de la table ou encore les tâches ménagères. La famille Lawrence croit fermement que « Mondstadt post-libération manque autant de sens moral que de manières. Lorsqu'enfin le clan reprendra ce qui lui est dû, trouver des serviteurs répondant au niveau d'excellence requis pourrait alors s'avérer difficile. Prudence est donc de mise, nous ne pouvons permettre que la fange de la plèbe nous salisse. »

     L'apprentissage des arts culinaires se fait dans la famille Lawrence de la plus stricte des manières. Oublier une demi-cuillerée de farine dans la pâte, un soupçon de sel dans l'assaisonnement, ou avoir le malheur de sortir le plat du four quelques secondes trop tard... Autant d'erreurs pouvant se solder par un terrible sermon, quand ce n'est pas un châtiment

     Aux yeux d'Eula, toute l'envie que suscite sa maîtrise aux fourneaux n'est qu'une autre conséquence inutile d'une éducation archaïque.

     C'est probablement pourquoi, le seul genre de personne qu'elle autorise à goûter à ses recettes maison est quelqu'un qu'elle... respecte ? ... Non, plutôt quelqu'un qu'elle tolère, quelqu'un qui « n'arrête malgré tout pas de me suivre partout où je vais et de m'importuner, ne faisant apparemment aucun cas de mes avertissements, et qui a franchi la ligne à de multiples reprises ».

=== "Histoire 5"

     L'étiquette mis à part, l'art est pour l'aristocratie « la seconde âme », d'une importance incomparable à ses yeux.

     La « Danse du sacrifice » était autrefois un rituel destiné à faire montre de la noblesse des illustres familles, et considéré comme la forme cristallisée de l'âme, la gemme sur le sceptre du dirigeant.

     Les gens de Mondstadt racontent que les plus illustres des clans créèrent ensemble cette danse, et ce bien longtemps avant que le règne sanglant de l'aristocratie ne débute.

     Son troisième acte, qui représente la famille Lawrence et s'intitule « Lueur vacillante », est d'une importance capitale. La personne qui la danse devait être de la plus haute noblesse, et on choisissait souvent pour l'exécuter la fille aînée de la famille.

     La famille Lawrence recrutait les meilleures professeurs de danse pour s'assurer que chaque pas soit exécuté à la perfection, et chaque pointe de pied ensanglantée n'était qu'un nouvel apprêt à la gloire du clan - chaque danseuse devait en être fière.

     L'antique cérémonie perdura trop longtemps, et a fini par être abandonnée et interdite il y a de cela bien des années. Et pourtant, le Clan Lawrence a continué à maintenir la tradition. Sans le majestueux banquet et la scène imposante allant autrefois avec l’évènement, la Danse du sacrifice a avec le temps perdu bien de son décorum, et le degré d'excellence exigé de chaque position a dû être revu à la baisse, les bons professeurs se faisant rares. En désespoir de cause, le Clan Lawrence dut se résoudre à la considérer comme un art secondaire et à en faire un cours optionnel.

     Les années ont fini par débarrasser la danse de toute connotation négative, et de n'en garder que les pas et les chorégraphies les plus élégants. Le cours de danse fut pour Eula, première-née de la famille Lawrence de cette génération, la seule échappatoire aux règles aussi stricte qu’étouffantes de la plupart des autres cours durant son enfance.

     La femme a semble-t-il définitivement coupé les ponts avec cet art de nos jours, et les gens n’imaginent pas que le Chevalier aux Embruns connaisse quoi que ce soit à la danse.

     Et pourtant, mélodies indescriptibles et beauté du rythme découlent de chacun de ses mouvements lorsqu'elle manie son épée.

     Lorsque son arme quitte son fourreau, Eula virevolte avec l'élégance d'une danseuse sous la lune, aussi belle qu'elle est insaisissable.

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat de jade shivada x1 - Graine de pissenlit x3 - Masque endommagé x3 |
| 40 | - Moras 40 000 - Fragment de jade shivada x3 - Fleur cristalline x2 - Graine de pissenlit x10 - Masque endommagé x15 |
| 50 | - Moras 60 000 - Fragment de jade shivada x6 - Fleur cristalline x4 - Graine de pissenlit x20 - Masque sale x12 |
| 60 | - Moras 80 000 - Morceau de jade shivada x3 - Fleur cristalline x8 - Graine de pissenlit x30 - Masque sale x18 |
| 70 | - Moras 100 000 - Morceau de jade shivada x6 - Fleur cristalline x12 - Graine de pissenlit x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre de jade shivada x6 - Fleur cristalline x20 - Graine de pissenlit x60 - Masque sinistre x24 |

## Aptitudes
---

|![aptitude 1](imagee/aptitude/aptitude_1_eula.png) Escrime de Favonius - Aristocratie | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Exécute une succession d'entailles aussi rapides que tranchantes ; cette attaque consomme de l'endurance de façon continue. L'attaque chargée se termine sur un coup puissant. |
| Attaque descendante : | Plonge dans les airs pour frapper le sol, infligeant des DGT aux ennemis en chemin et des DGT de zone à l'impact. |

|![aptitude 2](imagee/aptitude/aptitude_2_eula.png) Vortex des mers glacées | Aptitude de combat |
|---------|-------------|
| | Givre coupant et lame dansante. |
| Appui simple | Eula exécute une frappe rapide qui inflige des DGT Cryo. Si elle touche un ennemi, Eula gagne une charge de Cœur de glace (jusqu'à 2 charges max). Eula peut gagner une charge toutes les 0,3 s. |
| Cœur de glace | Améliore la RÉS à l'interruption et la DÉF d'Eula. |
| Appui long | Eula consomme toutes les charges de Cœur de glace pour faire tournoyer son épée devant elle et infliger des DGT Cryo de zone aux ennemis en face. Si des charges de Cœur de glace sont consommées, la RÉS physique et la RÉS Cryo des ennemis proches diminuent. Chaque charge de Cœur de glace est convertit en épée vortex de glace, qui inflige des DGT Cryo aux ennemis proches. Ainsi roula l'écume des vagues, engloutissant dans son sillage la couronne de la royauté. Et chaque embrun de cette marée glaciale étincela de sa lumineuse clarté |

|![aptitude 3](imagee/aptitude/aptitude_3_eula.png) Lame de fond | Aptitude de combat |
|---------|-------------|
| | Eula brandit son épée avec fureur, infligeant des DGT Cryo aux ennemis proches, et créé une épée de lumière qui la suit pendant 7 s max. Tant qu'elle est active, l'épée de lumière augmente la RÉS à l'interruption d'Eula. Lorsqu'Eula inflige des DGT à des ennemis avec son attaque normale, sa compétence ou son déchaînement élémentaire, elle accumule les charges d'énergie pour l'épée de lumière. Une charge d'énergie peut être cumulée toutes les 0,1 s. Lorsque la durée de l'épée de lumière est écoulée, elle explose violemment, infligeant des DGT physiques aux ennemis proches. Ces DGT augmentent en fonction du nombre de charges d'énergie accumulées. L'épée explose immédiatement lorsqu'Eula quitte le champ de bataille. Le naufrage est facile. Et pourtant, Eula insiste pour figer cette marée. |

|![aptitude 4](imagee/aptitude/aptitude_4_eula.png) Maelström de glace | Aptitude passive |
|---------|-------------|
| | Lorsque vous consommez 2 charges de Cœur de glace lors de l’appui long de Vortex des mers glacées, une brève épée de lumière est créée. Cette dernière explose immédiatement, infligeant des dégâts physiques équivalant à 50% des dégâts de base de Lame de fond. |

|![aptitude 5](imagee/aptitude/aptitude_5_eula.png) Esprit belligérant | Aptitude passive |
|---------|-------------|
| | Le temps de recharge de Vortex des mers glacées est réinitialisé lors de l’utilisation de Lame de fond, et Eula obtient une charge de Cœur de glace. |

|![aptitude 6](imagee/aptitude/aptitude_6_eula.png) Gratification aristocratique | Aptitude passive |
|---------|-------------|
| | 10% de chances d’obtenir le double de produits lors de la synthèse de matériaux d’aptitudes de personnage. |

## Constellations
---

|![constellation 1](imagee/constel/constelation_1_eula.png) Illusion des marées | Constellation Niv.1 |
|---------|-------------|
| | À chaque fois qu’une charge de Cœur de glace de Vortex des mers glacées est consommée, Eula gagne un bonus de DGT physiques de 30 % pendant 6 s. Chaque charge de Cœur de glace consommée prolonge la durée de l’effet de 6 s, jusqu’à un maximum de 18 s. |

|![constellation 2](imagee/constel/constelation_2_eula.png) Fille d'écume de mer | Constellation Niv.2 |
|---------|-------------|
| | Diminue le TdR de l’appui long de Vortex des mers glacées pour correspondre à celui de l’appui simple. |

|![constellation 3](imagee/constel/constelation_3_eula.png) Le sang des Lawrence | Constellation Niv.3 |
|---------|-------------|
| | Niveau d’aptitude Lame de fond +3. |
| | Niveau max : 15 |

|![constellation 4](imagee/constel/constelation_4_eula.png) Émergence de la plèbe | Constellation Niv.4 |
|---------|-------------|
| | Augmente de 25 % les DGT infligés par l’épée de lumière aux ennemis ayant moins de 50 % de PV. |

|![constellation 5](imagee/constel/constelation_5_eula.png) Honneur de la chevalerie | Constellation Niv.5 |
|---------|-------------|
| | Niveau d’aptitude Vortex des mers glacées +3. |
| | Niveau max : 15 |

|![constellation 6](imagee/constel/constelation_6_eula.png) Obligations aristocratiques | Constellation Niv.6 |
|---------|-------------|
| | Confère immédiatement 5 charges d’énergie à l’épée de lumière créée par Lame de fond. De plus, lorsque vous utilisez des attaques normales, la compétence ou le déchainement élémentaire pour cumuler des charges, vous avez 50 % de chances de gagner une charge supplémentaire. |
