!!! info inline end "Identité"
     À Mondstadt, il y a une rumeur qui court sur un jeune garçon. Certains disent que c'est un enfant abandonné que les loups ont adopté quand il était bébé, d'autres croient qu'il est l'incarnation d'un loup…
     Aussi vifs que l'éclair et dotés de griffes acérées, les loups vivent en harmonie avec la nature sauvage.
     À l'heure actuelle, Razor court et chasse certainement dans la forêt avec les loups à l'aide de son instinct et de son agilité surprenante.

![Razor](https://images7.alphacoders.com/111/thumbbig-1117662.webp)

## Histoire 


=== "Histoire 1"

    Razor vit nuit et jour parmi les loups. Chacun des membres de la meute est aussi proche du garçon qu'un loup puisse l'être. Eux n'ont aucun secret pour Razor, qui sait lequel hurle le plus fort, lequel court le plus vite, lequel est le meilleur aux embuscades.

    Razor est capable de ressentir l'émotion du vent, de repérer des odeurs à des lieues de distance, et connait les applications pratiques de nombreux types d'herbes; mais il est une chose qu'il ne peut se rappeler : le visage de ses parents. Quel type de personnes étaient-ils ? Il est incapable de s'en souvenir, aussi dur qu'il essaie.

    Sa mémoire semble ne commencer qu'après sa vie avec les loups, lorsque ceux-ci l'adoptèrent comme lupical, un terme qui, quelqu'un lui avait par le passé expliqué, signifie "famille" pour les loups.

=== "Histoire 2"

      De minuscules bestioles volent dans le ciel en chantant; les nuages duveteux s'allongent à la manière d'une queue de loup. Le monde de Razor est un monde simple.

      Quand le soleil est de sortie, il faut aller chasser, ou cueillir des fruits; les jours de pluie, on se cache dans un arbre creux pour dormir au chaud avec un camarade sur un lit de feuilles et d'herbes.

      Manger autant que possible, et boire beaucoup. Se rafraichir au lac quand il fait trop chaud, et grignoter des baies quand il fait soif. Rien de compliquer.

      Razor est conscient qu'il est différent des autres membres de la meute; mais cela ne semble pas le déranger le moins du monde.

=== "Histoire 3"

      Un homme de grande taille vint un jour dans les montagnes, troublant la vie paisible de Razor. Razor ne le connaissait pas, sachant juste qu'un humain avait pénétré sur son territoire. À la vue de son air troublé, l'homme lui fit un sourire aimable.

      "Pourquoi ne pas rentrer avec moi à Mondstadt ?"_ Parlant ainsi, l'homme avait tendu une main amicale. Ni les loups ni Razor ne comprirent le geste, et, protectrice, la meute vient se placer entre Razor et l'homme.

      Se cachant parmi eux, Razor observa à travers leurs pelages le corps de l'homme, le comparant au sien. Razor était conscient qu'il n'était pas le plus intelligent des êtres, et le doute habita alors son esprit. "Suis-je un homme ou un loup ?"

=== "Histoire 4"
     ...

=== "Histoire 5"
     ...

## Elevation

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'améthyste vajrada x1 - Baie à crochet x3 - Masque endommagé x3 |
| 40 | - Moras 40 000 - Fragment d'améthyste vajrada x3 - Prisme d'éclair x2 - Baie à crochet x10 - Masque endommagé x15 |
| 50 | - Moras 60 000 - Fragment d'améthyste vajrada x6 - Prisme d'éclair x4 - Baie à crochet x20 - Masque sale x12 |
| 60 | - Moras 80 000 - Morceau d'améthyste vajrada x3 - Prisme d'éclair x8 - Baie à crochet x30 - Masque sale x18 |
| 70 | - Moras 100 000 - Morceau d'améthyste vajrada x6 - Prisme d'éclair x12 - Baie à crochet x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre d'améthyste vajrada x6 - Prisme d'éclair x20 - Baie à crochet x60 - Masque sinistre x24 |

## Aptitudes
---

|![aptitude 1](imageee/aptitude/aptitude_1_keqing.png) Croc d'acier | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 coups d'épée. |
| Attaque chargée : | Exécute une succession d'attaques tournoyantes ; cette attaque consomme de l'endurance sur la durée. L'attaque chargée se termine sur un coup puissant. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageee/aptitude/aptitude_2_keqing.png) Griffe et tonnerre | Aptitude de combat |
|---------|-------------|
| | Razor met à profit les enseignements de son maître et de son lupical pour chasser sa proie. |
| Appui simple | D'un coup de griffe du loup du tonnerre, il inflige des DGT Électro aux ennemis en face. Razor obtient un sceau Électro lorsque cette attaque touche un ennemi ; chaque sceau lui confère un bonus de recharge d'énergie. Razor peut cumuler jusqu'à 3 sceaux Électro ; chaque nouveau sceau réinitialise leur durée. |
| Appui long | Amassant la puissance du tonnerre, Razor libère une explosion électrique, infligeant d'importants DGT Électro aux ennemis proches, et supprimant tous les sceaux Électro. Chaque sceau ainsi supprimé est converti en énergie élémentaire. Parfois un malheureux peut apercevoir à la lueur de l'éclair le regard de prédateur de Razor. |

|![aptitude 3](imageee/aptitude/aptitude_3_keqing.png) Croc éclair | Aptitude de combat |
|---------|-------------|
| | Le loup de tonnerre qui sommeille se réveille et vient à la rescousse, infligeant des DGT Électro aux ennemis proches ; ceci supprime également tous les sceaux Électro, qui sont convertis en énergie élémentaire. Le loup de tonnerre combat aux côtés de Razor tant qu'il est actif. Le loup de tonnerre disparaît lorsque Razor est vaincu ou quitte le champ de bataille. Le temps restant est converti en énergie élémentaire (10 pts max) lorsque Razor quitte le champ de bataille. Le loup s'est réveillé. L'heure de la chasse a sonné. |

|![aptitude 4](imageee/aptitude/aptitude_4_keqing.png) Éveil | Aptitude passive |
|---------|-------------|
| Réduit le TdR de Griffe et tonnerre de 18 %. Croc éclair réinitialise le TdR de Griffe et tonnerre lorsqu'il est activé. |

|![aptitude 5](imageee/aptitude/aptitude_5_keqing.png) Famine | Aptitude passive |
|---------|-------------|
| | Razor bénéficie d'un bonus de recharge d'énergie de 30 % lorsque son énergie élémentaire est inférieure à 50 %. |

|![aptitude 6](imageee/aptitude/aptitude_6_keqing.png) Vélocité lupine | Aptitude passive |
|---------|-------------|
| | Réduit la consommation d'endurance de vos personnages dans l'équipe de 20 % lors du sprint. Ne peut pas être cumulé avec d'autres aptitudes ayant les mêmes effets. |


## Constellations
---

|![constellation 1](imageee/constel/constelation_1_keqing.png) Nature du loup | Constellation Niv.1  |
|---------|-------------|
| | Razor inflige 10 % de DGT supplémentaires pendant 8 s lorsqu'il obtient un orbe ou une particule élémentaire. |

|![constellation 2](imageee/constel/constelation_2_keqing.png) Répression | Constellation Niv.2 |
|---------|-------------|
| | Le taux de CRIT de Razor augmente de 10 % quand il attaque des ennemis ayant moins de 30 % de PV. |

|![constellation 3](imageee/constel/constelation_3_keqing.png) Âme de loup | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Croc éclair +3. |
| | Niveau max : 15 |

|![constellation 4](imageee/constel/constelation_4_keqing.png) Morsure | Constellation Niv.4 |
|---------|-------------|
| | L'appui simple de Griffe et tonnerre réduit la DÉF des cibles de 15 % pendant 7 s. |

|![constellation 5](imageee/constel/constelation_5_keqing.png) Griffes tranchantes | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Griffe et tonnerre +3. |
| | Niveau max : 15 |

|![constellation 6](imageee/constel/constelation_6_keqing.png) Lupus Fulguris | Constellation Niv.6 |
|---------|-------------|
| | Toutes les 10 s, l'arme de Razor se charge d'énergie électrique pour infliger lors de la prochaine attaque normale des DGT Électro équivalent à 100 % de l'ATQ. Chaque ennemi touché confère à Razor un sceau électro de Griffe et tonnerre tant que Croc éclair n'est pas actif.|