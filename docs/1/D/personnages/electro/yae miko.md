
!!! info inline end "Identité"
    Yae Miko, (Japonais: 八重神子 Yae Miko) également connu sous le nom de Guuji Yae (Japonais: 宮ぐう司じ Guuji, "grand prêtre") ou la Guuji, est un personnage Électro dans Genshin Impact.

    Grande prêtresse du Sanctuaire de Narukami et descendante de la lignée des Kitsunes, famille et amie de l'Éternité, mais aussi intimidante responsable éditoriale de la Chambre Yae, une maison d'édition de romans légers.
    Ce qui réside au plus profond du cœur de l’énigmatique guuji, avec ses nombreuses identités, pourrait ne jamais être pleinement compris par les mortels.

![Yae Miko](https://images4.alphacoders.com/120/thumbbig-1204945.webp)


## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'améthyste vajrada x1 - Ganoderma marin x3 - Garde-main ancien x3 |
| 40 | - Moras 40 000 - Fragment d'améthyste vajrada x3 - Fausse nageoire de l'héritier du dragon x2 - Ganoderma marin x10 - Garde-main ancien x15 |
| 50 | - Moras 60 000 - Fragment d'améthyste vajrada x6 - Fausse nageoire de l'héritier du dragon x4 - Ganoderma marin x20 - Garde-main kageuchi x12 |
| 60 | - Moras 80 000 - Morceau d'améthyste vajrada x3 - Fausse nageoire de l'héritier du dragon x8 - Ganoderma marin x30 - Garde-main kageuchi x18 |
| 70 | - Moras 100 000 - Morceau d'améthyste vajrada x6 - Fausse nageoire de l'héritier du dragon x12 - Ganoderma marin x45 - Garde-main célèbre x12
| 80 | - Moras 120 000 - Pierre d'améthyste vajrada x6 - Fausse nageoire de l'héritier du dragon x20 - Ganoderma marin x60 - Garde-main célèbre x24 |

## Aptitudes
---

|![aptitude 1](imageee/aptitude/aptitude_1_miko.png) Vulpes mangeur de péchés | Aptitude de combat |
|---------|-------------|
| Attaque normale | Invoque des esprits Kitsunes, enchaînant jusqu'à 3 attaques qui infligent des DGt électro. |
| Attaque chargée | Consomme de l'endurance pour infliger des DGT électro de zone après un court délai. |
| Attaque plongeante | Plonge depuis les airs en concentrant la puissance du tonnerre, infligeant des DGT aux ennemis en chemin et des DGT électro de zone à l'impact |

|![aptitude 2](imageee/aptitude/aptitude_2_miko.png) Sakura dévastateur | Aptitude de combat |
|---------|-------------|
| | Pour Yae, les tâches ennuyeuses qui peuvent être réglées en chassant les esprits n'ont pas besoin d'être accomplies personnellement. Se déplace rapidement, en laissant derrière elle un sakura dévastateur |
| Sakura dévastateur | Les caractéristiques suivantes s'appliquent : Frappe un ennemi proche d'un éclair de façon intermittente, infligeant des DGT électro. Lorsqu'il y a d'autres sakura dévastateurs à proximité, leur niveau augmente, améliorant les DGT qu'ils infligent. Cette capacité peut être utilisée 3 fois. Un maximum de 3 sakuras dévastateurs peut exister en même temps. Le niveau initial d'un sakura est 1, et le niveau le plus élevé qu'un sakura peut atteindre est 3. Si un sakura dévastateur est créé trop près d'un autre sakura, l'ancien sakura est détruit. _« Les esprits Kitsunes scellés dans les branches sont tous des yakan qui aspirent à chasser les humains. Les yakan sont deux grades plus bas que nous, les Kitsunes célestes, et donc, je vois à peine le problème si je les... mets à mes ordres. Néanmoins, cela ne signifie pas que vous, les humains, devriez copier notre système de grade. Que nenni... Et quant à qui d'entre nous, Kitsunes ou humains, est le plus haut placé dans l'ordre hiérarchique... Héhé...»_ |

|![aptitude 3](imageee/aptitude/aptitude_3_miko.png) Incarnation de tenko | Aptitude de combat |
|---------|-------------|
| | La légende du "Kitsunetsuki", ou la possession par un Kitsune, est l'un des contes populaires d'Inazuma. Parmis eux, celui du Kitsune céleste est considéré comme spécial, accablant les ennemis du Sanctuaire Narukami sous la forme d'un éclair qui s'abat, infligeant des DGT électro de zone. En utilisant cette compétence, Yae Miko descelle les sakura dévastateurs à proximité pour détruire leurs formes extérieures et les transformer en éclairs de tenko descendant du ciel, infligeant des DGT électro de zone. Chaque sakura dévastateur détruit de cette manière déclenche un éclair. Yae n'utilise sa quintessence de Kitsune céleste que dans ces moments-là, brandissant elle-même la foudre. Tous ceux qui ont vu les queues de la Guuji ont été réduits en cendres, alors rappelez_vous que dame yYae n'a pas de queue ! |

|![aptitude 4](imageee/aptitude/aptitude_4_miko.png) Ombre sacrée du sanctuaire | Aptitude passive |
|---------|-------------|
| | Lors de l'utilisation de Technique secrète ! Incarnation de tenko, le TDR d'évocation des yakan : Sakura dévastateur se réinitialise une fois pour chaque sakura dévastateur détruit. |

|![aptitude 5](imageee/aptitude/aptitude_5_miko.png) Bénediction éveillée | Aptitude passive |
|---------|-------------|
| | Chaque point de maitrise élèmentaire que Yae Miko possède augmente les DGT des sakura dévastateurs de 0.15%. |

|![aptitude 6](imageee/aptitude/aptitude_6_miko.png) Méditation d'une yako | Aptitude passive |
|---------|-------------|
| | 25% de chances de synthétiser un maétriau d'aptitude aléatoire supplémentaire (même rareté et région que les matériaux consommés). |

## Constellations
---

|![constellation 1](imageee/constel/constelation_1_miko.png) Offrande des yakan | Constellation N°1 |
|---------|-------------|
| | Lorsque Technique secrète : Incarnation de tenko déclenche un éclair de tenko, Yae Miko récupère 8 pts d'énergie élémentaire. |

|![constellation 2](imageee/constel/constelation_2_miko.png) Glapissement sous la lune | Constellation N°2 |
|---------|-------------|
| | Les sakura dévastateurs commencent au Niv.2 et peuvent monter jusqu'au Niv.4, et leur portée d'attaque augmente de 60%. |
        
|![constellation 3](imageee/constel/constelation_3_miko.png) Les sept glamours | Constellation N°3 |
|---------|-------------|
| | Niveau d'aptitude évocation des yakan : sakura dévastateur +3. |
| | Niveau max : 15 |

|![Constellation 4](imageee/constel/constelation_4_miko.png) Canalisation de sakura | Constellation N°4 |
|---------|-------------|
| | Lorsque l'éclair d'un sakura dévastateur touche un ennemi, tous les membres de l'équipe à proximité obtiennent un bonus de DGT éclectro de 20% pendant 5s. |

|![Constellation 5](imageee/constel/constelation_5_miko.png) Moquerie implacable | Constellation N°5 |
|---------|-------------|
| | Niveau d'aptitude Technique secrète : Incarnation de tenko +3.
| | Niveau max : 15

|![Constellation 6](imageee/constel/constelation_6_miko.png) Art tabou : Daisesshou | Constellation N°6 |
|---------|-------------|
| | Les attaques des sakuras dévastateurs ignorent 60% de la DEF de l'ennemi. |