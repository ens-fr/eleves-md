!!! info inline end "Identité"
    « L'Alioth, l'une des Sept Étoiles de Liyue. L'Alioth critique à mots couverts l'idée que Liyue suive à la lettre la parole du Seigneur de la Roche ; mais ce dernier apprécie justement ce genre de personnes.
    Keqing croit fermement en l'idée que l'humanité devrait être responsable de son propre destin, et qu'elle en est capable. Et elle n'hésite pas à travailler deux fois plus que n'importe qui pour prouver qu'elle a raison. »

![Keqing](https://images2.alphacoders.com/111/thumbbig-1113087.webp)


## Histoire
---

=== "Histoire 1"
    Keqing est issue d'une illustre famille de Liyue, et connaît mieux que quiconque l'influence engendrée par le Souverain de la Roche sur les habitants de la cité.
    De manière presque cyclique, les prévisions émises par le Souverain de la Roche lors du rite de la descente bouleversant toutes les quelques années la vie économique de la ville et ont de profondes répercussions. Leur annonce est célébrée par certaines familles, et plongent d'autres dans la consternation. Les plus démunis espèrent qu'elles leur offriront une chance de faire fortune, tandis que les plus nantis craignent de voir la leur en subir les conséquences.
    Plutôt que d'analyser le climat économique avec soin, les marchands sont plus enclins à investir en se basant sur leur instinct. Ceci s'avère en général être une bien mauvaise stratégie. En effet, le Souverain de la Roche est un dieu puissant, et il est plus sage de lui faire confiance sur ces sujets. Cependant, Keqing a ses propres doutes : qu'adviendra-t-il le jour où l'Archon Géo ne participera plus à la vie économique de la cité ? Que deviendront les gens de Liyue ?
    L'actuelle prospérité de la ville est semblable à ses yeux à quelque magnifique château de sable sur la plage : l'humanité n'a pas son mot à dire sur le mouvement de la marée... Mais les opinions de Keqing sur le sujet ne sont pas soutenues par la majorité. Du point de vue de la plupart des gens, la vie n'est qu'un bref moment insignifiant au regard de la longue histoire de la cité, et il n'y a nul besoin de s'empoisonner l'existence à imaginer des problèmes aussi abstraits. Aux yeux de Keqing, cette perspective est aussi lâche qu'indolente, et dénie toute valeur à l'existence humaine.
    "Si la vie des hommes a si peu de valeur, alors pourquoi attendons-nous tant de soutien de la part des dieux ?", rétorque-t-elle souvent.
    Refusant toute compromission, elle observe à travers le prisme de son scepticisme tout ce qui l'entoure - de la bonté du Souverain de la Roche à l'insouciance de ceux qui l'entourent, en passant par le fonctionnement de la société.
    Les affaires des hommes devraient être laissés à ces derniers ; ils sont bien capables de gérer leur destin eux-mêmes.
    Keqing fit un jour un discours remarqué à l'occasion justement d'un rite de la descente.
    "L'Archon Géo a veillé sur Liyue pendant plus d'un millénaire. Mais qu'en sera-t-il du prochain millénaire ? Et des dix suivants ? Combien de temps pourra durer ce statu quo ?"
    À ces mots, le Souverain de la Roche éclata d'un rire de bon aloi. Seul lui sait pourquoi.

=== "Histoire 2"
     Certains - ils sont peu nombreux - sont capables de suivre les idées avant-gardistes de Keqing, mais personne n'est capable de suivre son rythme de travail.
     Peut-être dans un espoir inconscient de devenir un modèle pour les gens de Liyue, Keqing s'astreint à travailler plus que tous, et est incapable d'accepter tout ce qui se rapproche de près ou de loin à de l'indolence ou de l'inefficacité.
     Les capacités de l'homme, comme sa longévité, sont bien inférieures à celles des dieux et céder à l'apathie et l'hésitation ne lui permettra jamais de devenir un jour maître de son destin.
     C'est cette idée à laquelle elle croit dur comme fer qui explique sans aucun doute qu'elle soit incapable de synchroniser son rythme de travail avec celui de ses collègues. Keqing ne prend pas de repos avant d'avoir fini la tâche à laquelle elle s'est attelée... Un travail requérant deux semaines sera pour elle l'occasion de faire tout son possible pour réduire autant que faire se peut cette durée - et elle aura terminé au bout de deux jours.
     Il faut aussi noter qu'à ses yeux, un travail "terminé" n'a pas la même signification que pour la plupart des gens. Pour elle, un travail est terminé lorsque toutes les tâches qui y sont liées sont elles aussi effectuées, tous les détails éclaircis, et tous les problèmes résolus. Aux yeux des autres, elle est aussi perfectionniste qu'efficace.
     Mais tout le monde n'est pas aussi motivé que Keqing, et aucun des assistants qu'elle a employés n'a jamais atteint la barre des trois mois.
     On l'a exhorté un nombre incalculable de fois de faire preuve de plus de souplesse et de moins d'impatience, mais à chaque fois, sa réponse demeure la même : hors de question.
     Cela étant, elle a fini par réaliser, dans une certaine mesure, qu'un changement permanent d'assistants était en soi une marque d'inefficacité, et a donc mis de l'eau dans son vin.
     Si vous croisez à l'occasion l'un de ses employés planchant sur une tâche avec quatre ou cinq ans d'avance, soyez sans crainte : Keqing lui a juste donné une marge d'avance. Elle l'aura rattrapé avant peu.

=== "Histoire 3"
     "L'expérience d'hier deviendra la force de demain." Voici l'un des principes guidant la vie de Keqing.
     Passer son temps à la Terrasse Yujing signifie être témoin des mêmes choses, jour après jour ; mais pour être capable de penser et d'agir aussi vite que l'éclair, rien n'est aussi indispensable que l'expérience amassée.
     C'est pourquoi Keqing n'hésite jamais à mettre la main à la pâte. Il lui arriva par exemple de passer deux mois à travailler à l'usine dans l'unique but de mieux comprendre la vie des ouvriers, dans le cadre d'un plan de mise en valeur de leurs salaires.
     Difficile d'imaginer que cette jeune femme tirant un chariot au Gouffre, aidant les dockers à damer le sol, ou travaillant comme serveuse dans un restaurant local est en réalité l'une des personnes aux plus hautes responsabilités de la ville.
     En plus de se montrer dure à la tâche, elle accumule les données lors de ces expériences diverses. Il advint même qu'elle profita de l'une de ces occasions pour rassembler les preuves mettant à jour les abus d'un personnage important.
     Ce dernier tomba des nues lorsqu'il fut appréhendé, ignorant tout de la façon dont ses agissements avaient été découverts. Jusqu'à ce qu'on l'amène dans la salle d'interrogatoire, où se tenait une jeune femme à la mise soignée.
     "Hé mais... On s'est déjà rencontré, non ?", fit-il, en proie à une désagréable impression.
     Impression qui se confirma lorsque les Millelithes le reprirent rudement : "C'est à l'Alioth des Sept Étoiles que tu t'adresses ! Fais preuve d'un peu plus de respect !"
     L'Alioth ? Mais alors pourquoi...
     Étrange.


## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'améthyste vajrada x1 - Cœur de lapis x3 - Nectar de Fleur mensongère x3 |
| 40 | - Moras 40 000 - Fragment d'améthyste vajrada x3 - Prisme d'éclair x2 - Cœur de lapis x10 - Nectar de Fleur mensongère x15 |
| 50 | - Moras 60 000 - Fragment d'améthyste vajrada x6 - Prisme d'éclair x4 - Cœur de lapis x20 - Nectar miroitant x12 |
| 60 | - Moras 80 000 - Morceau d'améthyste vajrada x3 - Prisme d'éclair x8 - Cœur de lapis x30 - Nectar miroitant x18 |
| 70 | - Moras 100 000 - Morceau d'améthyste vajrada x6 - Prisme d'éclair x12 - Cœur de lapis x45 - Nectar élémentaire x12 |
| 80 | - Moras 120 000 - Pierre d'améthyste vajrada x6 - Prisme d'éclair x20 - Cœur de lapis x60 - Nectar élémentaire x24 |

## Aptitude
---

|![aptitude 1](imageee/aptitude/aptitude_1_keqing.png) Escrime des nuages | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Consomme de l'endurance pour déclencher deux coups d'épée rapides. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageee/aptitude/aptitude_2_keqing.png) Retour des étoiles | Aptitude de combat |
|---------|-------------|
| | Keqing lance une Lame de foudre qui attaque ses ennemis avec la puissance des cieux. Lorsqu'une Lame de foudre touche sa cible, elle inflige des DGT Électro de zone et laisse une marque à l'endroit visé. -
| Appui long | Ajuste la zone où la compétence sera lancée.
Les Lames de foudre peuvent aussi être suspendues dans les airs ; activez à nouveau la compétence Retour des étoiles pour permettre à Keqing de s'y téléporter. |
| Lame de foudre | Lorsque la marque est activé, Keqing peut utiliser à nouveau Retour des étoiles pour se téléporter ou lancer une attaque chargée et la marque disparaîtra avec les effets suivants selon le cas :
Réactivation de Retour des étoiles : Keqing se téléporte à l'emplacement de la marque et exécute une entaille qui inflige des DGT Électro de zone. Keqing passe à travers tous les obstacles topographiques lorsqu’elle se téléporte. |
| Attaque chargée : | une série de coups de tonnerre tranchants s'abat à l'emplacement de la marque, infligeant des DGT Électro de zone. « Les humains sont comme des papillons de nuit, attirés par la lumière des dieux et des Adeptes. Or seule la mienne guide mes ailes. » |

|![aptitude 3](imageee/aptitude/aptitude_3_keqing.png) Promenade céleste | Aptitude de combat |
|---------|-------------|
| | Keqing fait appel à la puissance de l'éclair, et inflige des DGT Électro de Zone.
Se fondant ensuite dans l'ombre de son épée, elle libère une série de coups tranchants rapides, infligeant aux ennemis proches des DGT Électro. Le coup final inflige d'importants DGT Électro de zone. « L'une des techniques les plus rapides de l'arsenal de Keqing, capable de faire tomber même les étoiles. » |

|![aptitude 4](imageee/aptitude/aptitude_4_keqing.png) Pénitence retentissante | Aptitude passive |
|---------|-------------|
| | Keqing obtient pendant 5 s un enchantement Électro lorsqu'une Lame de foudre est active et que Retour des étoiles est lancé à nouveau. |

|![aptitude 5](imageee/aptitude/aptitude_5_keqing.png) Trésor de l'Alioth | Aptitude passive |
|---------|-------------|
| | Keqing obtient pendant 8 s un bonus de taux CRIT et de recharge d'énergie de 15% lorsqu'elle libère Promenade céleste. |

|![aptitude 6](imageee/aptitude/aptitude_6_keqing.png) Agencement des terres | Aptitude passive |
|---------|-------------|
| | Réduit la durée d'une expédition de 25% lorsqu'elle se déroule à Liyue. |

## Constellations
---

|![constellation 1](imageee/constel/constelation_1_keqing.png) Force foudroyante | Constellation Niv.1 |
|---------|-------------|
| | Lorsque Keqing libère de nouveau Retour des étoiles lorsqu'une Lame de foudre est active, et inflige des DGT Électro de zone équivalent à 50% de son ATQ aux endroits où elle disparait et puis réapparait. |

|![constellation 2](imageee/constel/constelation_2_keqing.png) Taxation sévère | Constellation Niv.2 |
|---------|-------------|
| | Les attaques normales et chargées de Keqing touchant un ennemi affecté par l'élément Électro ont 50% de chance de générer une particule élémentaire. Cet effet peut être déclenché une fois toutes les 5 s. |

|![constellation 3](imageee/constel/constelation_3_keqing.png) Formation anticipée | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Promenade céleste +3. |
| | Niveau max : 15 |

|![constellation 4](imageee/constel/constelation_4_keqing.png) Syntonie | Constellation Niv.4 |
|---------|-------------|
| | Lorsque Keqing déclenche une réaction liée à l'élément Électro, son ATQ augmente de 25% pendant 10 s. |

|![constellation 5](imageee/constel/constelation_5_keqing.png) Lueur mouvante | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Retour des étoiles +3. |
| | Niveau max : 15 |

|![constellation 6](imageee/constel/constelation_6_keqing.png) Étoile tenace | Constellation Niv.6 |
|---------|-------------|
| | Les attaques normales, attaques chargées, compétences élémentaires et déchaînements élementaires de Keqing lui confèrent un bonus de 6% de DGT Électro pendant 8 s. Les effets déclenchés sont différents selon le type d'attaque. |