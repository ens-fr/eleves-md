!!! info inline end "Identité"
    Beidou (chinois : 北斗 Běidǒu, « Grande Ourse ») est un personnage électro jouable de Genshin Impact.

    Elle est la capitaine du Crux, un équipage renommé à Liyue. Outre ses capacités en tant que capitaine de flotte et son immense force, beaucoup à Liyue la connaissent pour son manque de peur envers le Tianquan du Liyue Qixing, Ningguang – un trait que l’autre apprécie, mais qui s’irrite.

![Beidou](https://images3.alphacoders.com/121/thumbbig-1212379.webp)

## Histoire
---

=== "Histoire 1"

    Beidou jouit d'une certaine réputation à Liyue. Bien qu'elle ne jouisse pas du renom des Sept Etoiles, il n'existe cependant pas un marchand en ville ignorant son nom - ou celui de son armada, le Crux. Bien évidemment, les rumeurs au sujet de quelqu'un de sa stature abondent. Les gens adorent narrer ses épiques aventures et ses exploits en mer , décrit généralement avec moult exagérations. Une autre rumeur existe cependant, selon laquelle Beidou serait capable de canaliser l'élément Electro à l'aide de son épée, et de couper en deux les monstres des bas-fond d'un seul coup de son arme. Ceux qui entendent ces rumeurs n'y accordent généralement aucun crédit, mettant ça sur le compte d'élucrubations d'ivrognes ; mais pour ceux qui ont voyagé à ses côtés, ces histoires ne sont point sujet de plaisanterie. A leur yeux, Beidou est la personne toute indiquée si par hasard un monstre marin était pris de l'envie de se faire trancher en deux. En d'autres termes, Beidou est aussi impressionnante que les rumeurs circulants à son sujet dans les cercles marchands ; et son surnom de " Reine des Mers " n'est point immérité.

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'améthyste vajrada x1 - Jade noctiluque x3 - Insigne du Pilleur x3
| 40 | - Moras 40 000 - Fragment d'améthyste vajrada x3 - Prisme d'éclair x2
Jade noctiluque x10 - Insigne du Pilleur x15
| 50 | - Moras 60 000 - Fragment d'améthyste vajrada x6 - Prisme d'éclair x4 - Jade noctiluque x20 - Insigne de corbeau en argent x12 |
| 60 | - Moras 80 000 - Morceau d'améthyste vajrada x3 - Prisme d'éclair x8 - Jade noctiluque x30 - Insigne de corbeau en argent x18 |
| 70 | - Moras 100 000 - Morceau d'améthyste vajrada x6 - Prisme d'éclair x12 - Jade noctiluque x45 - Insigne de corbeau en or x12 |
| 80 | - Moras 120 000 - Pierre d'améthyste vajrada x6 - Prisme d'éclair x20 - Jade noctiluque x60 - Insigne de corbeau en or x24 |


## Aptitudes
---

|![aptitude 1](imageee/aptitude/aptitude_1_beidou.png) Conquête des mers | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups d'épée. |
| Attaque chargée : | Exécute une succession d'entailles aussi rapides que tranchantes ; cette attaque consomme de l'endurance sur la durée. L'attaque chargée se termine sur un coup puissant. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageee/aptitude/aptitude_2_beidou.png) Invocation des marées | Aptitude de combat |
|---------|-------------|
| | N'ayez crainte. Que quelqu'un ose s'en prendre à Beidou ou aux siens, et sa lame et le tonnerre lui feront payer au centuple. |
| Appui simple | Beidou concentre le pouvoir de la foudre sur son épée, et exécute un coup violent qui inflige aux ennemis en face des DGT Électro. |
| Appui long | Beidou brandit son arme, qu'elle utilise comme bouclier. La quantité de DGT absorbés est proportionnelle aux PV max de Beidou. Lorsque vous relachez la compétence ou que celle-ci prend fin, Beidou brandit sa longue épée et effectue une contre-attaque libérant toute l'énergie accumulée, qui inflige des DGT Électro. Augmente considérablement les DGT infligés selon le nombre de coups portés lors de la durée de la conséquence. Deux coups minimum doivent être portés pour obtenir le bonus de DGT maximum.
Bouclier : Le bouclier est efficace à 250% contre les DGT Électro. Applique l'élément Électro à Beidou. À l'assaut de la tempête et des vagues dansantes ! |

|![aptitude 3](imageee/aptitude/aptitude_3_beidou.png) Brisure d'orage | Aptitude de combat |
|---------|-------------|
| | Se remémorant son combat contre Haishan, la bête géante, Beidou fait appel à la même force destructrice, et crée un bouclier de tonnerre qui inflige des DGT Électro aux ennemis à proximité. |
| Bouclier de tonnerre | Les attaques normales et chargées, lorsqu'elles touchent, libèrent une décharge électrique qui se propage d'ennemi en ennemi et inflige des DGT Électro ; Les actions de Beidou sont plus difficiles à interrompre et les DGT qu'elle reçoit diminuent tant que le bouclier est actif. Au maximum une décharge par seconde peut être libérée. Le plus puissant des éclairs peut fendre mers et montagnes. |

|![aptitude 4](imageee/aptitude/aptitude_4_beidou.png) Châtiment | Aptitude passive |
|---------|-------------|
| | La contre-attaque d'Invocation des marées inflige le maximum de DGT lorsqu'elle est exécutée juste après une attaque. |

|![aptitude 5](imageee/aptitude/aptitude_5_beidou.png) Tempête de coups de tonnerre | Aptitude passive |
|---------|-------------|
| | Invocation des marées confère pendant 10 s les effets suivants lorsqu'il est exécuté avec le maximum de DGT : Les attaques normales et chargées infligent 15% de DGT supplémentaires. Les attaques chargées se chargent beaucoup plus vite. |

|![aptitude 6](imageee/aptitude/aptitude_6_beidou.png) Braveuse de marée | Aptitude passive |
|---------|-------------|
| | Réduit la consommation d'endurance de vos personnages dans l'équipe de 20% lors de la nage. Ne peut pas être cumulé avec d'autres aptitudes ayant les mêmes effets. |

## Constellations
---

|![constellation 1](imageee/constel/constelation_1_beidou.png) Fléau des monstres marins | Constellation Niv.1 |
|---------|-------------|
| | L'activation de Brisure d'orage confère les effets suivants : Génère un bouclier qui absorbe une quantité de DGT équivalent à 16% des PV max et dure 15 s. Le bouclier est efficace à 250% contre les DGT Électro. |

|![constellation 2](imageee/constel/constelation_2_beidou.png) Sur la mer turbulente, le tonnerre gronde | Constellation Niv.2 |
|---------|-------------|
| | Les décharges de Brisure d'orage se propagent à 2 ennemis supplémentaires. |

|![constellation 3](imageee/constel/constelation_3_beidou.png) Invocateur de tonnerre | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Invocation des marées +3. |
| | Niveau max : 15 |

|![constellation 4](imageee/constel/constelation_4_beidou.png) Vengeance étonnante | Constellation Niv.4 |
|---------|-------------|
| | Les attaques normales de Beidou infligent 20% de DGT Électro supplémentaires pendant 10 s quand elle est touchée. |

|![constellation 5](imageee/constel/constelation_5_beidou.png) Marcheur de marée écarlate | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Brisure d'orage +3. |
| | Niveau max : 15 |

|![constellation 6](imageee/constel/constelation_6_beidou.png) Chancre du mal | Constellation Niv.6 |
|---------|-------------|
| | La RÉS Électro des ennemis proches est réduite de 15% tant que Brisure d'orage est actif. |