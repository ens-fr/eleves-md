!!! info inline end "Identité"
    « Elle souhaite débarrasser ce bas monde du désordre qui l'habite, puis renverser le cycle de la vie et de la mort. Personne ne peut réellement la comprendre, il est impossible de voir à travers elle... Son Excellence la Shogun est pleine de contradictions. »

![Raiden](https://images5.alphacoders.com/117/thumbbig-1170176.webp)


## Histoire
---

=== "Histoire 1"
    Le vrai nom de la Shogun Raiden est Raiden Ei.
    Pendant son long chemin, de nombreux sacrifices étalés sur des miliers d'années lui ont permis de forger la cité d'Inazuma telle qu'elle est aujourd'hui.
    Les temps heureux se sont envolés, les amis d'autrefois devenues ennemis d'aujourd'hui. En fin de compte, même brandir une lame perdu tout son sens.
    "Il faut aller de l'avant, même s'il y aura des pertes".
    Ei estime que le temps applique sur le monde une loi dure comme fer.
    Même les nations les plus prospères peuvent s'effondrer en une nuit. La plus ancienne des villes portuaires, Liyue, vit elle aussi disparaître son Archon Géo.
    Le vent des adieux souffle depuis les confins du temps.
    Le prestige de la Shogun Raiden est éclatant, mais après quelques centaines, quelques milliers d'années, arrivera le jour où Inazuma perdra la protection de sa déesse.
    En tant que guerrière, aucun ennemi ne lui échappe. Peu importe si ce dernier est une menace aussi intangible que le temps ou le néant, elle se doit de contre-attaquer et de trouver l'arme pour terrasser son adversaire.
    Sa solution ? "L'éternité". Elle seule peut figer toutes choses sous les cieux, rendre Inazuma éternelle.
    "Profitons de cette perfection, de cette immuabilité, atteignons l'éternité."

=== "Histoire 2"
     Être constitué d'un corps mortel et souhaiter poursuivre l'éternité implique de faire face à cette chose inévitable qu'est l'espérance de vie.
     La limite que lui imposait le temps emplissait Ei d'ennui et de mélancolie. Jusqu'au jour où une mystérieuse technique lui parvint par la force du destin.
     Grâce à elle, il lui était possible de créer de magnifiques marionnettes à la perfection impossible à différencier d'un corps mortel.
     En théorie, une telle marionnette pouvait parfaitement imiter Ei. Cet être artificiel pouvait lui permettre d'exister pour toujours et ainsi continuer à apporter une protection éternelle à Inazuma. Cependant, créer la réplique d'une divinité est plus facile à dire qu'à faire. Ei entreprit d'innombrables essais et essuya tout autant d'échecs, usa d'une quantité inimaginable de temps et de matériaux.
     Grâce à sa persévérance et son esprit combattant, elle finit par créer la marionnette parfaite. La "Shogun Raiden" nouveau-née se redressa calmement et écouta avec attention les paroles d'Ei, lui expliquant comment "elle" et cette dernière ne formaient qu'une. Là était le futur d'Inazuma.


## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'améthyste vajrada x1 - Fruit d'Amakumo x3 - Garde-main ancien x3 |
| 40 | - Moras 40 000 - Fragment d'améthyste vajrada x3 - Perles tempestives x2 - Fruit d'Amakumo x10 - Garde-main ancien x15 |
| 50 | - Moras 60 000 - Fragment d'améthyste vajrada x6 - Perles tempestives x4 - Fruit d'Amakumo x20 - Garde-main kageuchi x12 |
| 60 | - Moras 80 000 - Morceau d'améthyste vajrada x3 - Perles tempestives x8 - Fruit d'Amakumo x30 - Garde-main kageuchi x18 |
| 70 | - Moras 100 000 - Morceau d'améthyste vajrada x6 - Perles tempestives x12 - Fruit d'Amakumo x45 - Garde-main célèbre x12 |
| 80 | - Moras 120 000 - Pierre d'améthyste vajrada x6 - Perles tempestives x20 - Fruit d'Amakumo x60 - Garde-main célèbre x24 |

## Aptitudes
---

|![aptitude 1](imageee/aptitude/aptitude_1_raiden.png) Prémisse | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 coups de lance. |
| Attaque chargée : | Consomme de l'endurance pour exécuter une attaque ascendante. |
| Attaque descendante : | Plonge depuis les airs pour frapper le sol, infligeant des DGT aux ennemis sur la route et des DGT de zone à l'impact. |

|![aptitude 2](imageee/aptitude/aptitude_2_raiden.png) Présage funeste | Aptitude de combat |
|---------|-------------|
| | La Shogun Raiden dévoile un fragment de son euthymie, infligeant des DGT Éléctro de zone aux ennemis proches, et accordant à tous les personnages de l'équipe à proximité un œil de judgement orageux. |
| Œil de judgement orageux | Lorsqu'un personnage bénéficiant de cet effet attaque et inflige des DGT à adversaire, l'œil déclenche une attaque coordonné, infligeat des DGT Éléctro de zone à l'emplacement de l'ennemi. Les personnages qui obtiennent un œil de judgement orageux voient les DGTs de leurs dechaînements élémentaires augmenter pendant le durée de l'œil, en fonction de l'énergie élémentaire consommé par le dechaînement. L'oeil peut déclencher une attaque coordonné toutes les 0,9s par équipe. Les attaques coordonnées générées pa de personnages hors de votre équipe infligent 20% des DGT normaux. Les êtres d'une grande divinité peuvent affecter tout ce qui les entoure, et l'Archon d'Éléctro peut manipuler les étoiles menaçantes pour protéger ses partisans et infliger une sentence fulgurante à ses ennemis |

|![aptitude 3](imageee/aptitude/aptitude_3_raiden.png) Dogme d'idéal | Aptitude de combat |
|---------|-------------|
| | Rassemblant des vérités innombrables et des souhaits inquantifiables, la Shogun Raiden libre le Kendo d'idéal et inflige des DGT Éléctro de zone, avant de utiliser l'Harmonie d'idéal au combat pendant un certain temps. Les DGT infliges par le Kendo d'idéal et ceux infligés par les attaques sous Harmonie d'idéal augmentent selon le nombre de charges de resolution Chakre Desiderata consommées en utilisant l'Harmonie d'idéal. Dans cet état, la Shogun Raiden utilisera son tachi au combat, tandis que ses attaques normales, chargées, et plongeates seront imprégnées de DGT Éléctro ne pouvant pas être enchantés. Lorsque de telles attaques touchent des ennemis, la Shogun Raiden restaure l'énergie élémentaire de tous les personnages à proximité. La restauration peut être déclenchée une fois toutes les secondes de cette façon, et un maximum de 5 fois pendant toute la durée de cette aptitude. Dans cet état, la RÉS à l'interruption de la Shogun Raiden augmente, et elle est immunisée contre les Éléctrocutions Tant qu'Harmonie d'idéal est active, les DGT des attaques normales, chargées et plongeante considérés comme étant des DGT de déchaînement élémentaire. Les effets d'Harmonie d'idéal seront annulées lorsque la Shogun Raiden quite le champ de bataille |

|![aptitude 4](imageee/aptitude/aptitude_4_raiden.png) Chakra Desiderata | Constellation Niv.4 |
|---------|-------------|
| Lorsque les personnages de l'équipe à proximité utilisent un déchaînement élémentaire (hormis la Shogun Raiden), le Chakra Desiderata de la Shogun Raiden accumule de la résolution en fonction de l'énergie élémentaire consomée par le déchaînement. Il est possible de cumule jusqu'à 60 charges de résolution. La résolution obtenue par le Chakre desidarata sera annulées 300s aprés si la SHogun Raiden quitte le combat. C'est la nouvelle technique secréte de la Shogun Raiden. Abandonnant son <<insouciance>> dénuée de substance, elle porte desormais sur ses épaules un nouvel <<idéal>> : les rêves et les ambitions de tous. Tou comme <<dogme>>, la <<verité>>, signifie également le <<renouveau>>. Inazuma est aussi entrée dans un nouveau chapitre |

|![aptitude 5](imageee/aptitude/aptitude_5_raiden.png) Souhaits innombrables | Aptitude passive |
|---------|-------------|
| | Lorsque les personnages de l'équipe à proximité obtiennent un orbe ou un particle élémentaire, le Chakra Desiderata accumule 2 charges de résolution. Cet effet peut être déclenché une fois toutes les 3s. |

|![aptitude 6](imageee/aptitude/aptitude_6_raiden.png) L'illuminée | Aptitude passive |
|---------|-------------|
| | Tout les 1% de recharge d'énergie au-dessus de 100% la Shogun Raiden bénéficei des effets suivantes: La restuaration d'énergie élémentaire accordée par Harmonie d'idéal augmente de 0,6% Le bonus de DGT Éléctro augmente de 0,4%. |

## Constellations
---

|![constellation 1](imageee/constel/constelation_1_raiden.png) Inscription inquiétante | Constellation Niv.1 |
|---------|-------------|
| | Chakra Desiderata rassemblera Resolve encore plus rapidement. Lorsque les personnages Electro utilisent leurs bursts élémentaires, la résolution gagnée est augmentée de 80%. Lorsque des personnages d’autres types élémentaires utilisent leurs rafales élémentaires, la résolution gagnée est augmentée de 20%. |

|![constellation 2](imageee/constel/constelation_2_raiden.png) Brise-acier | Constellation Niv.2 |
|---------|-------------|
| | En utilisant Musou no Hitotachi et dans l’état Musou Isshin appliqué par Secret Art: Musou Shinsetsu, les attaques du Shogun Raiden ignorent 60% du DEF des adversaires. |

|![constellation 3](imageee/constel/constelation_3_raiden.png) Shinkage Bygones | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence d’art secret: Musou Shinsetsu  +3. |
| | Niveau max : 15. |

|![constellation 4](imageee/constel/constelation_4_raiden.png) Gage de bienséance | Constellation Niv.4 |
|---------|-------------|
| | Lorsque l’état Musou Isshin appliqué par Secret Art: Musou Shinsetsu expire, tous les membres du groupe à proximité (à l’exception du Shogun Raiden) gagnent 30% de bonus ATK pour 10s. |

|![constellation 5](imageee/constel/constelation_5_raiden.png) La descente du shogun | Constellation Nv.5 |
|---------|-------------|
| | Niveau de compétence transcendance: Baleful Omen +3. |
| | Niveau max : 15. |

|![constellation 6](imageee/constel/constelation_6_raiden.png) Porteur de vœux | Constellation Niv.6 |
|---------|-------------|
| Alors que dans l’état Musou Isshin appliqué par Secret Art: Musou Shinsetsu, les attaques du Shogun Raiden qui sont considérées comme faisant partie de son Elemental Burst diminueront le CD Elemental Burst de tous les membres du groupe à proximité (mais sans inclure le Raiden Shogun elle-même) de 1 lorsqu’ils frapperont leurs adversaires. Cet effet peut se déclencher une fois toutes les 1 et peut déclencher un total de 5 fois pendant la durée de l’état. |