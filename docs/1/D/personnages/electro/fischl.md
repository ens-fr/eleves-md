!!! info inline end "Identité"
    Fischl von Luftschloss Narfidort est un personnage électro jouable dans Genshin Impact.

    Fischl est un enquêteur de la Guilde des Aventuriers de Mondstadt, accompagné du corbeau de nuit Oz. Fischl prétend provenir d’un monde au-delà de Teyvat. C’est une aventurière audacieuse avec des théories apparemment farfelues qui s’avèrent vraies.

![Fischl](https://images6.alphacoders.com/110/thumbbig-1109278.webp)

## Histoire
---

=== "Histoire 1"
    En tant qu'enquêtrice de la Guilde des aventuriers, Fischl possède en Oz un avantage non négligeable.
    _"Mademoiselle, par pitié, ne m'envoyez plus espionner Stormterror, où je finirai boulotté... quoi que je resterais sans doute coincé dans ses crocs."_
    _"N'est-ce donc point ton honneur que de servir la Princesse du châtiment?"_
    Fischl est capable de voir par les yeux du corbeau, et il lui suffit de prendre possession de l'animal pour pouvoir observer la terre depuis haut dans le ciel. Qu'elle souhaite inspecter l'évolution de la flore des Montagnes du guet ou suivre quelque agitation sur le Territoire des Loups, tout lui est aisément révélé à travers les yeux de l'oiseau. Ce pouvoir unique, combiné à ses propres efforts et à une personnalité sortant de l'ordinaire, a rapidement fait d'elle une étoile montante reconnue de tous au sein de la Guilde.
    Son arrivée à quatorze ans au poste d'enquêtrice dans la Guilde est due, paraît-il, à l'intervention de ses parents. Nul doute qu'ils n'ont pas dû avoir à trop insister : si elle est la Princesse du châtiment, alors ses parents en sont eux le Kaiser et la Kaiserin...

=== "Histoire 1"
     Il paraît qu'un " Dictionnaire Fischl " circule parmi les aventuriers.

     Cet ouvrage remarquable serait, comme son nom l'indique, un outil utile dans la traduction du langage particulier employé par la jeune femme. Ainsi par exemple, "J'entends l'écho des vents du passé résonner à travers l'abysse du temps, soufflant sur la spirale oubliée du torrent du karma" se traduit par "les Ruines de Stormterror" ;

     _"Réjouissez vous et chantez, vous les serviteurs espérant la bénédiction de la princesse ; et avec la furie du tigre, partez à l'assaut !" signifie bien entendu que le rapport préliminaire de l'enquête est prêt ;_
     _Quant à la tournure " Tout est écrit dans ce sombre livre de prophéties", la traductionévidente est bien sûr que Fischl a mis à jour son journal suivant les derniers rapports connus._
     
     A dire vrai, ce fameux "Dictionnaire Fischl" n'est pas un ouvrage écrit, mais plutôt une plaisanterie d'initiés. En effet, les gens qui la connaissent ont pris l'habitude d'écouter soigneusement ses mots, et d'en déduire le sens caché ; ceci peut être vu comme une forme de respect à son égard.

     _"Ainsi donc toi aussi, tu me comprends ; le destin encore une fois réunit les esprits qui se ressemblent."_

     Ceux qui s'essaient à lui répondre à sa manière la voient ravie, et elle ne manque pas de les féliciter pour leurs capacités linguistiques.

     _"Une princesse se doit de rendre hommage lorsqu'hommage est dû ; et à présent, conversons, ami! Et ne t'y trompe point : car là résident les fondations du nouveau monde..."_

=== "Histoire 3"
     Indisponible

=== "Histoire 4"
     Indisponible

=== "Histoire 5"
     Indisponible

## Elevation
---
| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'améthyste vajrada x1 - Herbe à lampe x3 - Pointe de flèche robuste x3 |
| 40 | - Moras 40 000 - Fragment d'améthyste vajrada x3 - Prisme d'éclair x2 - Herbe à lampe x10 - Pointe de flèche robuste x15 |
| 50 | - Moras 60 000 - Fragment d'améthyste vajrada x6 - Prisme d'éclair x4 - Herbe à lampe x20 -  Pointe de flèche aiguisée x12 |
| 60 | - Moras 80 000 - Morceau d'améthyste vajrada x3 - Prisme d'éclair x8 - Herbe à lampe x30 -  Pointe de flèche aiguisée x18 |
| 70 | - Moras 100 000 - Morceau d'améthyste vajrada x6 - Prisme d'éclair x12 - Herbe à lampe x45 - Pointe de flèche usée x12 |
| 80 | - Moras 120 000 - Pierre d'améthyste vajrada x6 - Prisme d'éclair x20 - Herbe à lampe x60 - Pointe de flèche usée x24 |

## Aptitudes
---

|![aptitude 1](imageee/aptitude/aptitude_1_fishl.png) Flèche de culpabilité | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 5 tirs consécutifs. |
| Attaque chargée : | Effectue un tir visé plus précis et infligeant davantage de DGT. Lors de la visée, les ténébreux esprits de la nuit convergent sur la flèche. Celle-ci se charge en élément Électro, infligeant de considérables DGT Électro quand elle est complètement chargée. |
| Attaque descendante : | Tire une pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de zone à l'impact. |

|![aptitude 2](imageee/aptitude/aptitude_2_fishl.png) Ailes de surveillance nocturne | Aptitude de combat |
|---------|-------------|
| | Fischl invoque son fidèle Oz à ses côtés. Le corbeau d'ombre et de tonnerre descend sur ce monde, infligeant des DGT Électro dans une petite zone lors de son invocation. Oz tire en continu des balles électrifiées sur les ennemis proches tant qu'il est actif. |
| Appui long | |Ajuste la zone où la compétence sera lancée ; appuyez à nouveau lorsqu'Oz est actif pour le faire revenir. « Vous ne méritez pas que je gaspille mon pouvoir sur vous… Oz, fais pleuvoir les éclairs de justice. » — « Fleurs pour la princesse Fischl - Tome IV : Adieu, maîtresse. » |

|![aptitude 3](imageee/aptitude/aptitude_3_fishl.png) Incarnation de la nuit | Aptitude de combat |
|---------|-------------|
| | Répondant à l'appel de Fischl, Oz vient protéger sa maîtresse de ses ailes tel un voile nocturne. Pendant la durée de la compétence : Fischl prend la forme d'Oz, ce qui lui permet d'augmenter sa VIT de déplacement. Les ennemis sur sa route sont frappés par la foudre et subissent des DGT Électro. Chaque ennemi ne peut être frappé qu'une fois. Lorsque l'effet prend fin, Oz reste pour combattre les ennemis de sa maîtresse. Si Oz est déjà présent sur le terrain avant l'utilisation de cette compétence, son temps de présence sera remis à zéro. Restaure une partie de l'énergie élémentaire de Fischl une fois terminé. « … De toutes les bêtes suivant le sang maudit, seul le seigneur corbeau, dont les yeux ont été témoins de la destruction de mille mondes, était capable de comprendre sa grandeur. Et, la recouvrant d'un voile d'ébène, il fit le serment de la protéger à jamais. » — « Fleurs pour la princesse Fischl - Tome I : Apocalypse » |

|![aptitude 4](imageee/aptitude/aptitude_4_fishl.png) Corbeau maléfique | Aptitude passive |
|---------|-------------|
| | Oz libère sur les ennemis proches la foudre du jugement lorsque Fischl le touche d'une flèche complètement chargée, leur infligeant des DGT Électro de zone équivalent à 152,7% des DGT du tir. |

|![aptitude 5](imageee/aptitude/aptitude_5_fishl.png) Foudre divine | Aptitude passive |
|---------|-------------|
| | Lorsque votre personnage déployé déclenche une réaction liée à l'élément Électro en la présence d'Oz, l'ennemi est frappé par la foudre du jugement divin, et subit des DGT Électro équivalent à 80% de l'ATQ de Fischl. |

|![aptitude 6](imageee/aptitude/aptitude_6_fishl.png) Jardin secret | Aptitude passive |
|---------|-------------|
| | Réduit la durée d'une expédition de 25% lorsqu'elle se déroule à Mondstadt. |

## Constellations
---

|![constellation 1](imageee/constel/constelation_1_fishl.png) Œil du corbeau obscur | Constellation Niv.1 |
|---------|-------------|
| | Oz garde toujours un œil de corbeau sur Fischl, même quand il ne l'accompagne pas au combat : les attaques normales de Fischl infligent un bonus de DGT équivalent à 22% de l'ATQ. |

|![constellation 2](imageee/constel/constelation_2_fishl.png) Plume de jugement | Constellation Niv.2 |
|---------|-------------|
| | Ailes de surveillance nocturne inflige un bonus de DGT équivalent à 200% de l'ATQ ; la zone d'effet augmente de 50%. |

|![constellation 3](imageee/constel/constelation_3_fishl.png) Ailes noires de l'Abîme | Constellation Niv.3 |
|---------|-------------|
| | Niveau de compétence Ailes de surveillance nocturne +3. |
| | Niveau max : 15 |

|![constellation 4](imageee/constel/constelation_4_fishl.png) Les fantasmes de la princesse | Constellation Niv.4 |
|---------|-------------|
| | Incarnation de la nuit inflige un bonus de DGT Électro équivalent à 222% de l'ATQ aux ennemis proches. Fischl récupère 20% de PV à la fin de la compétence. |

|![constellation 5](imageee/constel/constelation_5_fishl.png) Apocalypse de la nuit | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Incarnation de la nuit +3. |
| | Niveau max : 15 |

|![constellation 6](imageee/constel/constelation_6_fishl.png) Oiseau de la nuit éternelle | Constellation Niv.6 |
|---------|-------------|
| | Prolonge la durée d'Oz de 2 s. De plus, Oz attaque aux côtés de votre personnage déployé, infligeant des DGT Électro équivalent à 30% de l'ATQ de Fischl. |