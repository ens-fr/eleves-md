!!! info inline end "Identité"
    « Lisa. est une sorcière intelligente avec un penchant pour la sieste.
    En tant que bibliothécaire de l'Ordre de Favonius, cette grande intellectuelle trouve toujours la meilleure solution à tous les problèmes.
    Malgré son air nonchalant, Lisa. maîtrise toute situation avec calme et sérénité. »

![Lisa.](https://images8.alphacoders.com/111/thumbbig-1119674.webp)


## Histoire
---

=== "Histoire 1"
    Lisa. est responsable de l'organisation et de l'entretien de la vaste collection d'ouvrages de la bibliothèque de l'Ordre ; elle est aussi chargée de s'assurer que le stock de potions et médicaments reste toujours approvisionné.
    Les seules rares occasions pour les citoyens de Mondstadt de rencontrer Lisa. sont lorsqu'ils viennent emprunter ou rendre un livre.
    Dans les deux cas, elle se trouve en général assise au guichet, semblable à quelque chat langoureux, baillant tandis qu'elle tient le compte des ouvrages entrants et sortants.
    Son attitude fait souvent se demander si elle est à la hauteur des responsabilités de son poste. Mais son travail ne présente jamais aucun défaut, et celui qui y trouverait à redire n'est pas encore né.

=== "Histoire 2"
     Le fait que Lisa. soit considérée par les érudits de l'Académie de Sumeru comme la meilleure étudiante qu'ils aient connus en l'espace de deux siècles est suffisamment parlant quant à ses capacités.
     Qu'il s'agisse de quelque savoir ésotérique interdit, de la conservation et de l'utilisa.tion de la flore élémentale ou encore des méthodes de distillation du vin les plus probantes, il n'est généralement pas un aspect de la connaissance que Lisa. n'ait pas déjà visité.
     Cette soif de savoir, combinée à un réel talent pour expliquer les choses claire et concise, en font l'interlocutrice idéale pour les jeunes chevaliers et alchimistes débordant de questions.
     Tant que ceux-ci viennent à des heures acceptables, cela va sans dire.
     Le malheureux venant à une heure indécente - comme par exemple lorsque Lisa. fait la grasse matinée - s'expose à de grave conséquences.

=== "Histoire 3"
     Les personnes rencontrant Lisa. pour la première fois sont souvent intimidées par la célèbre étudiante de l'Académie du Sumeru.
     Cependant, bien que son attitude semble pouvoir être résumée par les deux mots "impitoyable efficacité" au premier abord, il s'agit en fait du résultat d'une nature profondément paresseuse, cherchant à s'éviter tout problème futur à tout prix.
     Après avoir négocier avec Kaeya, elle a ainsi chargé Hoffman et Swann du restockage et de la préparation des réserves médicinales ; Flora et Donna ont été, quant à elles, chargées de la livraison quotidienne d'herbes. La seule chose dont Lisa. s'occupe personnellement est l'organisation de ses ouvrages.
     Elle ne se sent vraiment rassurée que lorsque c'est elle-même qui en a la charge.

=== "Histoire 4"
     À son arrivée dans l'Ordre de Favonius, le Grand Maître voulut la nommer Capitaine de la Huitième Compagnie.
     Cette décision fut fortement contestée par Nymphe, l'officier en charge de la compagnie, qui désapprouvait qu'une universitaire soit nommée à un poste si important.
     À la suggestion de Kaeya, un combat amical fut organisé, durant lequel les deux participantes pourraient faire montre de leur maîtrise de la magie.
     Le combat n'avait pas commencé depuis plus de deux minutes que Lisa. annonçait qu'elle renonçait gracieusement au titre, arguant que Nymphe possédait manifestement "toutes les qualités requises".
     L'année sui suivit vit le bureau du Grand Maître se couvrir de recommandations écrites de la main de Nymphe, et toutes portant le nom de Lisa. Minci. Le Grand Maître finit par les transmettre directement à Lisa., qui chaque fois refusait les honneurs sous quelque nouveau prétexte.
     Il ne fait aucun doute que la Huitième Compagnie aurait été plus forte sous son commandement ; mais à ses yeux, la compagnie était déjà assez forte dans l'état, et cela n'aurait servi qu'à augmenter des risques que la plupart des gens auraient été incapable de comprendre.
     Lisa. est confiante dans sa capacité à gérer n'importe quelle type de situation ; mais plus de risques aurait signifié plus de travail ; et cela restera à jamais à ses yeux inacceptable.

=== "Histoire 5"
     Ayant été témoin d'érudits à moitié criant dans les forêts de Sumeru et de puissants sages assis silencieux aux conseils, Lisa. savait comment la soif de connaissances peut changer un homme.
     Le prix lui semblait élevé ; quels sacrifices doit-on faire pour assouvir cette soif inextinguible ? C'est cette réflexion qui poussa Lisa. a quitter Sumeru.
     Depuis, Lisa. ne semble jamais prendre quoi que ce soit complètement au sérieux.
     "Celui qui demande les faveurs des dieux doit savoir d'abord s'il peut en payer le prix."
     Ce sont là ses mots, qu'elle n'utilisa. qu'à trois reprises : dans chaque cas, parce qu'elle savait que c'était ce que son interlocuteur avait besoin d'entendre.

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'améthyste vajrada x1 - Tombaie x3 - Bave de Blob x3 |
| 40 | - Moras 40 000 - Fragment d'améthyste vajrada x3 - Prisme d'éclair x2 - Tombaie x10 - Bave de Blob x15 |
| 50 | - Moras 60 000 - Fragment d'améthyste vajrada x6 - Prisme d'éclair x4 - Tombaie x20 - Mucus de Blob x12 |
Élévation 4 (Niveau 60 - Moras 80 000 - Morceau d'améthyste vajrada x3 - Prisme d'éclair x8 - Tombaie x30 - Mucus de Blob x18 |
| 70 | - Moras 100 000 - Morceau d'améthyste vajrada x6 - Prisme d'éclair x12 - Tombaie x45 - Essence de Blob x12 |
| 80 | - Moras 120 000 - Pierre d'améthyste vajrada x6 - Prisme d'éclair x20 - Tombaie x60 - Essence de Blob x24 |

## Aptitudes
---

|![aptitude 1](imageee/aptitude/aptitude_1_lisa.png) Touche d'éclair | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaîne jusqu'à 4 attaques électrifiées, infligeant des DGT Électro. |
| Attaque chargée : | Consomme de l'endurance pour infliger des DGT Électro de zone après un court délai. |
| Attaque descendante : | Plonge depuis les airs en concentrant ses pouvoirs Électro, infligeant des DGT aux ennemis en chemin et des DGT Électro de zone à l'impact. |

|![aptitude 2](imageee/aptitude/aptitude_2_lisa.png) Arc fulminant | Aptitude de combat |
|---------|-------------|
| | Canalise le pouvoir de la foudre pour nettoyer un peu le champ de bataille. |
| Appui simple | Libère un orbe électrique à tête chercheuse. Lorsqu'il touche une cible, l'orbe inflige des DGT Électro, ainsi que l'effet Conductivité aux ennemis proches, cumulable 3 fois. |
| Appui long | Après un court délai, des éclairs s'abattent des cieux et infligent des DGT Électro à tous les ennemis proches. Les ennemis affectés par Conductivité subissent des DGT supplémentaires proportionnels au nombre de cumuls. La couleur de la foudre est aussi la couleur de la Sorcière à la rose. |

|![aptitude 3](imageee/aptitude/aptitude_3_lisa.png) Rose de foudre | Aptitude de combat |
|---------|-------------|
| | Invoque une rose de foudre qui libère une puissante énergie électrique repoussant les ennemis proches et leur infligeant des DGT Électro. La rose de foudre envoie des décharges électriques qui infligent des DGT Électro aux ennemis proches tant qu'elle est active. Cette rose est belle, mais possède des épines mortelles. |

|![aptitude 4](imageee/aptitude/aptitude_4_lisa.png) Relique d'électricité | Aptitude passive |
|---------|-------------|
| | Les attaques chargées infligent l'effet Conductivité d'Arc fulminant aux ennemis lorsqu'elles touchent. |

|![aptitude 5](imageee/aptitude/aptitude_5_lisa.png) Champ électrostatique | Aptitude passive |
|---------|-------------|
| | Rose de foudre réduit la DÉF des ennemis qu'elle touche de 15 % pendant 10 s. |

|![aptitude 6](imageee/aptitude/aptitude_6_lisa.png) Connaissances médicinales | Aptitude passive |
|---------|-------------|
| | Lorsque vous fabriquez des potions, il y a 20 % de chance qu'une partie des matières premières vous soit rendue. |

## Constellations
---

|![constellation 1](imageee/constel/constelation_1_lisa.png) Circuit infini | Constellation Niv.1 |
|---------|-------------|
| | Lisa. regagne 2 pts d'énergie par ennemi touché tant qu'Arc fulminant reste activité. Jusqu'à 10 pts d'énergie élémentaire peuvent être restaurés de cette manière. |

|![constellation 2](imageee/constel/constelation_2_lisa.png) Champ électromagnétique | Constellation Niv.2 |
|---------|-------------|
| | Arc fulminant confère les effets suivants tant qu'il reste activé : DÉF augmentée de 25 %. Augmente la RÉS à l'interruption de Lisa.. |

|![constellation 3](imageee/constel/constelation_3_lisa.png) Champ électromagnétique | Constellation Niv.2 |
|---------|-------------|
| | Arc fulminant confère les effets suivants tant qu'il reste activé : DÉF augmentée de 25 %. Augmente la RÉS à l'interruption de Lisa.. |

|![constellation 4](imageee/constel/constelation_4_lisa.png) Éruption de plasma | Constellation Niv.4 |
|---------|-------------|
| | Rose de foudre libère 1 à 3 décharges supplémentaires. |

|![constellation 5](imageee/constel/constelation_5_lisa.png) Électrification | Constellation Niv.5 |
|---------|-------------|
| | Niveau de compétence Arc fulminant +3. |
| | Niveau max : 15 |

|![constellation 6](imageee/constel/constelation_6_lisa.png) Sorcière de pulse | Constellation Niv.6 |
|---------|-------------|
| | Lisa. inflige 3 cumuls de l'effet Conductivité d'Arc fulminant aux ennemis proches lorsqu'elle est déployée au combat. Cet effet peut être déclenché une fois toutes les 5 s. |