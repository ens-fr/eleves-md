!!! info inline end "Identité"
    Générale de la Commission administrative. Une jeune femme loyale au discours affirmé et pleine de vigueur.
    Elle est surnommée la « dévote au divin » et prête son entière allégeance à la Shogun Raiden.
    Accomplir la quête d'éternité poursuivie par la Shogun est aussi son seul et unique désir.

![Sara](https://images8.alphacoders.com/116/thumbbig-1162050.webp)

## Histoire
---

...

## Elevation
---

| Niveau | A recolter |
|---------|-------------|
| 20 | - Moras 20 000 - Éclat d'améthyste vajrada x1 - Dendrobium sanglant x3 - Masque endommagé x3 |
 40 | - Moras 40 000 - Fragment d'améthyste vajrada x3 - Perles tempestives x2 - Dendrobium sanglant x10 - Masque endommagé x15 |
| 50 | - Moras 60 000 - Fragment d'améthyste vajrada x6 - Perles tempestives x4 - Dendrobium sanglant x20 - Masque sale x12 |
| 60 | - Moras 80 000 - Morceau d'améthyste vajrada x3 - Perles tempestives x8 - Dendrobium sanglant x30 - Masque sale x18 |
| 70 | - Moras 100 000 - Morceau d'améthyste vajrada x6 - Perles tempestives x12 - Dendrobium sanglant x45 - Masque sinistre x12 |
| 80 | - Moras 120 000 - Pierre d'améthyste vajrada x6 - Perles tempestives x20 - Dendrobium sanglant x60 - Masque sinistre x24 |

## Aptitudes
---

|![aptitude 1](imageee/aptitude/aptitude_1_sara.png) Archerie tengu | Aptitude de combat |
|---------|-------------|
| Attaque normale : | Enchaine jusqu'à 5 tirs consécutifs. |
| Attaque chargée : | Effectue un tir visé plus précis infligeant davantage de DGT. Lors de la visée, des éclairs crépitants s'accumulent sur le carreau de la flèche. Une flèche complèteent chargée inflige des DGT Électro. Sous l'effet de la Protection corvidée, une flèche complètement chargée laisse une plume de corbeau à l'emplacement atteint. |
| Attaque descendante : | Tire une pluie de flèches depuis les airs et plonge au sol, infligeant des DGT de zone à l'impact. |

|![aptitude 2](imageee/aptitude/aptitude_2_sara.png) Tempestrier tengu | Aptitude de combat |
|---------|-------------|
| | Recule à la vitesse d'un Tengu et invoque la protection des corbeaux. Bénéficiee de la Protection corvidée pendant 18 s, un ffet qui est consommé lorsque Sara tire une flèche complètement chargée et laisse une plume de corbeau à l'emplacement atteint par la flèche. La plume de corbeau déclenche Tengu Juurai : Embuscade après un court moment, infligeant des DGT Électro aux ennemis proches, et accordant au personnage déployé à portée un bonus d'ATQ en fonction de l'ATQ de base de Sara. Les bonus d'ATQ des différents Tangu Juurai ne se sumulent pas, et leur effet et durée sont déterminé par le dernier Tangu Juurai actif. Les Tengu n'ont jamais manqué de membres aux talents exceptionnels possédant des yeux divins Électro. D'autre part, ils ont l'habitude de chanter les louanges de l'Archon Électro lorsqu'ils manipulent son élément. Leurs paroles ressemblent à peu près à cela : _« Ô digne Narukami, montre moi ton pouvoir et accorde à cette humble supplique ta puissance fulgurante ! Sowaka ! »_ |

|![aptitude 3](imageee/aptitude/aptitude_3_sara.png) Soumission : Chemin de lumière | Aptitude de combat |
|---------|-------------|
| | Dépose un Tengu Juurai : Brise-titan à l'emplacement de l'ennemi, infligeant des DGT Électro de zone. Puis ce Tengu Juurai : Brise-titan se divise en 4 Tengu Juurai : Tempête de pierres consécutifs, infligeant des DGT Électro de zone. Tengu Juurai : Brise-titan et Tengu Juurai : Tempête de pierres permettent au personnage déployé à portée de recevoir le même bonus d'ATQ que lors de la compétence élémentaire, Tempestrier tengu. Les bonus d'ATQ des différents Tengu Juurai ne se cumulent pas, et leur effet et durée sont déterminés par le dernier Tengu Juurai actif. Sara a quité son peuple, les Tengu, et a également temporairement perdu sa foi en la Commission administrative. Maid lorsque les nuages qui assombrissaient son cœur se sont dissipés, elle a retrouvé son propre éclair. |

|![aptitude 4](imageee/aptitude/aptitude_4_sara.png) Décorum | Aptitude passive |
|---------|-------------|
| | Lorsque Tengu Juurai : Embuscade touche un ennemi,Sara restaure 1,2 pts d'énergie dont elle dispose. Cet effet peut être déclenché une fois toutes les 3 s. |

|![aptitude 5](imageee/aptitude/aptitude_5_sara.png) Volonté immuab | Aptitude passive |
|---------|-------------|
| | Sous l'effet de la Protection corvidée de Tempestrier tengu, la durée de charge du tir visé est réduite de 60 %. |

|![aptitude 6](imageee/aptitude/aptitude_6_sara.png) Arpentage des terres | Aptitude passive |
|---------|-------------|
| | Réduit la durée d'une expédition de 25 % lorsqu'elle se déroule à Inazuma. |

## Constellations
---

|![constellation 1](imageee/constel/constelation_1_sara.png) Œil de corbeau | Constellation Niv.1 |
|---------|-------------|
| Lorsque Tengu Juurai accorde aux personnages des bonus d'ATQ ou touche des ennemis, le TdR de Tempestrier tengu est réduit de 1 s. Cet effet peut être déclenché une fois toutes les 3 s. |

|![constellation 2](imageee/constel/constelation_2_sara.png) Ailes sombres | Constellation Niv.2 |
|---------|-------------|
| | Lorsque Sara libére Tempestrier tengu, elle laisse une plume de corbeau plus faible à son emplacement d'origine, cette dernière infligeant 30 % des DGT d'origine. |

|![constellation 3](imageee/constel/constelation_3_sara.png) Conflit intérieur | Constellation Niv.3 |
|---------|-------------|
| | Niveau d'aptitude Soumission : Chemin de lumière +3. |
| | Niveau max : 15 |

|![constellation 4](imageee/constel/constelation_4_sara.png) Preuve concluante | Constellation Niv.4 |
|---------|-------------|
| | Le nombres de Tengu Juurai : Tempête de pierres déclenchés par Soumission : Chemin de lumière pass à 6. |

|![constellation 5](imageee/constel/constelation_5_sara.png) Anathémien | Constellation Niv.5 |
|---------|-------------|
| | Niveau d'aptitude Tempestrier tengu +3. |
| | Niveau max : 15 |

|![constellation 6](imageee/constel/constelation_6_sara.png) Péché d'orgueil | Constellation Niv.6 |
|---------|-------------|
| | Les personnages bénéficiant d'une augmentation d'ATQ accordée par Tengu Juurai voient les DGT CRIT de leurs DGT Électro augmenter de 60 %. |