### Genshin Impact, un jeu RPG à monde ouvert
---
Sorti le 8 septembre 2020 sur toute les plateformes, HoYoverse(1) nous plonge dans un univers fantastique en incarnant un voyageur _Aether_ ou _Lumine_.
    
![Aether, Lumine, Paimon](https://images8.alphacoders.com/122/thumbbig-1226879.webp)

### Introduction

Dans un monde imaginaire appelé Teyvat, Aether et Lumine se retrouvent séparés par une déesse inconnue. Le joueur commence son aventure en tant que l'un des deux personnages dont l'origine est inconnue dans le but ultime de retrouver son frère ou sa soeur jumelle/aux.
 
Il est possible tout au long de l'histoire, d'incarner d'autres personnages avec des capacités toutes différentes et originales.

L'histoire se déroule en plusieurs actes et parties dans le but de créer une impatience pour le joueur.

- Mais ne vous inquiétez pas ! Il y a plein de choses a faire en attendant les prochaines mises a jour comme :
    - Augmenter son niveau d'aventure pour des récompenses !
    - Explorer les différentes région de Teyvat !
    - Réaliser des invocations pour obtenir votre personnage préféré!
    - Trouvez des quêtes disponibles dans toute la carte !
    - Ouvrez des coffres et combatez des boss !
    - Augmentez le niveau de vos personnages pour les rendre encore plus forts !

???+ "A savoir" 
    Le jeu propose une carte du monde ouverte que le joueur peut explorer en marchant, en escaladant, en nageant et en planant. De nombreux objets et lieux importants sont répartis sur toute la carte. Le joueur peut contrôler jusqu'à quatre de ses personnages à la fois.

    Chaque personnage possède deux compétences de combat uniques : une compétence normale et une compétence spéciale. La compétence normale peut être utilisée à tout moment, mais est sujet à un délai de récupération après utilisation, alors que la compétence spéciale a un coût en énergie, obligeant le joueur à accumuler d'abord suffisamment d'énergie élémentaire.

    La cuisine est un autre aspect important du système de jeu de Genshin Impact. Le joueur peut collecter des ressources diverses et variées tout au long de son aventure, dont certaines peuvent être utilisées pour préparer des plats. Certains plats permettent de restaurer la santé des personnages ou de les ressusciter, tandis que d'autres augmentent les capacités offensives ou défensives

    Ce qui rend le jeu unique est le Système d'Éléments. Sept types d'éléments sont disponibles, à savoir le vent (Anémo), le feu (Pyro), la foudre (Electro), l'eau (Hydro), la glace (Cryo), l'énergie de la forêt (Dendro) et la terre (Géo). Les joueurs doivent tirer parti des synergies élémentaires, pour tuer des monstres ou accomplir des quêtes. Par exemple, les compétences de feu peuvent permettre de réduire en cendres le bouclier en bois de l'ennemi. Les compétences de glace peuvent être utilisées pour geler l'eau et ainsi traverser plus facilement une rivière...

    Genshin Impact possède aussi un mode multijoueur débloqué à partir du niveau 16 d'aventure, se jouant jusqu'a 4 joueurs

    _Il est à noter que la présence de joueurs dans le monde du joueur hôte n'oblige pas ce dernier à les inviter dans un Donjon, l'hôte peut partir en solo dans un donjon ou leur proposer de le rejoindre dans le Donjon. Ne pas être dans le donjon avec l'hôte n'expulse pas les joueurs invités du monde de l'hôte._