# Les Films Marvel

**Page réalisée par :**

![logo Marvel](images/logos/logo.jpg){width="300"}

_______

!!! info "Le MCU : Marvel Cinematic Univers"

    La liste des **films** et **séries** de l'univers cinématographique Marvel présente les œuvres de l'univers cinématographique Marvel (MCU) produit par _Marvel Studios_ depuis 2002 qui mettent en scène des personnages adaptés des comic books publiés par _Marvel Entertainment_. La liste est constituée à ce jour de **trente-deux** films et séries sortis tandis que plusieurs autres sont en cours de production.

Les films sont regroupés en plusieurs « phases » (I, II, III et IV) selon l'année de leur sortie en salle.

- La **phase I** : les films sortient de _2008_ à _2012_.
- La **phase II** : les films sortient de _2013_ à _2015_.
- La **phase III** : les films sortient de _2016_ à _2019_.
- La **phase IV** : les films et séries sortient _à partir de 2021_, (cette dernière phase n'est pas encore cloturée à l'heure actuelle).

!!! note "Les différentes sagas"
    Tous ces films sont aussi regroupés en différentes _sagas_.
    En effet, certains des héros de l'univers cinématographique Marvel possèdent leurs "_films solo_", mais apparaissent aussi dans des films regroupant plusieur d'entre eux, souvent pour former une équipe de héros afin d'affronter l'antagoniste principal du film en question. 
______
 
## Les films en solo :

### Iron Man

!!! example "Iron Man"
    ![Logo de Iron Man](images/logos/ironm.jpg){width="150"}
    
    1. **Iron Man** (2008)
    2. **Iron Man 2** (2010)
    3. **Iron Man 3** (2013)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Iron Man](images/posters/ironn.jpg){width="150"}|Alors qu'il fait l'essai d'une arme de son invention en Afghanistan, le milliardaire Tony Stark est capturé par des insurgés qui le forcent à travailler pour eux. Mais à leur insu, le scientifique crée pour lui-même une armure superpuissante au moyen de laquelle il s'évade et rentre aux États-Unis.|
        
    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Iron Man 2](images/posters/ironnn.jpg){width="150"}|Le monde sait désormais que l'inventeur milliardaire Tony Stark et le super-héros Iron Man ne font qu'un. Cependant, malgré les pressions, Tony n'est pas disposé à divulguer les secrets de son armure, redoutant que l'information atterrisse dans de mauvaises mains. Avec Pepper Potts et James Rhodey Rhodes à ses côtés, Tony va forger de nouvelles alliances et affronter de nouvelles forces toutes-puissantes.|
        
    === "3"
        |   |   |
        |:---:|:---|
        |![Poster du film Iron Man 3](images/posters/ironnnn.jpg){width="150"}|Tony Stark, alias Iron Man, mène une vie confortable aux côtés de sa compagne, Pepper. Cependant, il se retrouve confronté à Mandarin, chef d'une organisation terroriste, qui détruit sa maison et tout son univers. Tony Stark part alors à la recherche de Pepper, disparue, et cherche à se venger. Démuni, il ne peut compter que sur son ingéniosité, ses multiples inventions et son instinct de survie pour protéger ses proches.|
        
### Captain America

!!! example "Captain America"
    ![Logo de Captain America](images/logos/capt.jpg){width="150"}

    1. **Captain America : First Avenger** (2011)
    2. **Captain America : Le Soldat de l'hiver** (2014)
    3. **Captain America: Civil War** (2016)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Captain America : First Avenger](images/posters/cap.jpg){width="150"}|1941. La Seconde Guerre mondiale fait rage. Après avoir tenté vainement de s'engager dans l'armée pour se battre aux côtés des Alliés, Steve Rogers, frêle et timide, se porte volontaire pour participer à un programme expérimental qui va le transformer en un super soldat connu sous le nom de Captain America. Sous le commandement du colonel Chester Phillips, il s'apprête à affronter l'organisation scientifique secrète des nazis, aux côtés de Bucky Barnes et Peggy Carter.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Captain America : Le Soldat de l'hiver](images/posters/capp.jpg){width="150"}|Steve Rogers, plus connu sous le nom de Captain America, s'est adapté tant bien que mal à son nouvel environnement, et poursuit ses missions en tant qu'agent du S.H.I.E.L.D., l'agence militaire chargée d'assurer l'ordre international. Mais une organisation secrète aux desseins maléfiques a réussi à infiltrer le S.H.I.E.L.D. qu'elle gangrène de l'intérieur.|

    === "3"
        |   |   |
        |:---:|:---|
        |![Poster du film Captain America: Civil War](images/posters/cappp.jpg){width="150"}|Steve Rogers est désormais à la tête des Avengers, dont la mission est de protéger l'humanité. À la suite d'une de leurs interventions qui a causé d'importants dégâts collatéraux, le gouvernement décide de mettre en place un organisme de commandement et de supervision.|
    
### Thor

!!! example "Thor"
    ![Logo de Thor](images/logos/thor.webp){width="150"}

    1. **Thor** (2011)
    2. **Thor : Le Monde des ténèbres** (2013)
    3. **Thor : Ragnarok** (2017)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Thor](images/posters/thor.jpg){width="150"}|Le roi Odin règne avec sagesse sur son royaume. Une entente maintient la paix avec les Géants du monde glacé de Jotunheim. Les gestes irréfléchis du jeune Thor, pressenti pour prendre la place de son père sur le trône, mettent cependant en péril la paix fragile entre les deux peuples, ce qui pousse son père à le bannir sur Terre. Dépouillé de ses pouvoirs, Thor doit apprendre la véritable valeur d'un roi. Pendant ce temps, le demi-frère de Thor essaie de prendre possession du trône.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Thor : Le Monde des ténèbres](images/posters/thorr.jpg){width="150"}|Alors que sur Terre, l'astrophysicienne Jane Foster, grand amour de Thor, trouve par hasard une substance mystérieuse, l'Éther, que beaucoup croyaient perdue depuis longtemps, Thor part délivrer son frère adoptif, Loki, dans sa prison d'Asgard, car il a besoin de son aide. En effet, Malekith, un elfe noir jadis vaincu par Odin et les siens est de retour.|

    === "3"
        |   |   |
        |:---:|:---|
        |![Poster du film Thor : Ragnarok](images/posters/thorrr.jpg){width="150"}|Privé de son puissant marteau, Thor est retenu prisonnier sur une lointaine planète aux confins de l'univers. Pour sauver Asgard, il va devoir lutter contre le temps afin d'empêcher l'impitoyable Hela d'accomplir le Ragnarök, ou la destruction de son monde et la fin de la civilisation asgardienne. Pour y parvenir, il va d'abord devoir mener un combat titanesque de gladiateurs contre celui qui était autrefois son allié au sein des Avengers : l'incroyable Hulk.|

### Ant Man

!!! example "Ant Man"
    ![Logo de Ant Man](images/logos/ant.png){width="150"}

    1. **Ant-Man** (2015)
    2. **Ant-Man et la Guêpe** (2018)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Ant-Man](images/posters/ant.jpg){width="150"}|Doté d'une capacité étonnante, celle de rétrécir considérablement sa taille tout en multipliant sa force, Scott Lang, voleur professionnel, doit accepter le héros qui sommeille en lui afin de venir en aide à son mentor, le docteur Hank Pym, et ainsi protéger les secrets technologiques que renferme son costume. Face à une nouvelle génération d'opposants, Lang et Pym doivent mettre au point, et réussir un audacieux cambriolage qui pourrait sauver le monde.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Ant-Man et la Guêpe](images/posters/antt.jpg){width="150"}|Après les événements survenus dans Captain America, Civil War, Scott Lang a bien du mal à concilier sa vie de super-héros et ses responsabilités de père. Cependant, ses réflexions sur les conséquences de ses choix tournent court lorsque Hope van Dyne et le Dr Hank Pym lui confient une nouvelle mission urgente. Scott va devoir renfiler son costume et apprendre à se battre aux côtés de La Guêpe afin de faire la lumière sur des secrets enfouis de longue date.|

### Doctor Strange

!!! example "Doctor Strange"
    ![Logo de Doctor Strange](images/logos/doc.png){width="150"}

    1. **Doctor Strange** (2016)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Doctor Strange](images/posters/doc.jpg){width="150"}|Doctor Strange suit l'histoire du Docteur Stephen Strange, talentueux neurochirurgien qui, après un tragique accident de voiture, doit mettre son égo de côté et apprendre les secrets d'un monde caché de mysticisme et de dimensions alternatives. Basé à New York, Doctor Strange doit jouer les intermédiaires entre le monde réel et ce qui se trouve au-delà, en utlisant un vaste éventail d'aptitudes métaphysiques et d'artefacts pour protéger le Marvel Cinematic Universe.|

### Spider-Man

!!! example "Spider-Man"
    ![Logo de Spider-Man](images/logos/spidey.png){width="150"}

    1. **Spider-Man: Homecoming** (2017)
    2. **Spider-Man: Far From Home** (2019)
    3. **Spider-Man: No Way Home** (2021)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Spider-Man: Homecoming](images/posters/spid.jpg){width="150"}|Après ses spectaculaires débuts avec les Avengers, le jeune Peter Parker découvre peu à peu sa nouvelle identité, celle de Spider-Man, le superhéros lanceur de toile. Galvanisé par ses expériences précédentes, Peter rentre chez lui auprès de sa tante May, sous l'oeil attentif de son nouveau mentor, Tony Stark. L'apparition d'un nouvel ennemi, le Vautour, va mettre en danger tout ce qui compte pour lui.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Spider-Man: Far From Home](images/posters/spidd.jpg){width="150"}|L'araignée sympa du quartier décide de rejoindre ses meilleurs amis Ned, MJ, et le reste de la bande pour des vacances en Europe. Cependant, le projet de Peter de laisser son costume de super-héros derrière lui pendant quelques semaines est rapidement compromis quand il accepte à contrecoeur d'aider Nick Fury à découvrir le mystère de plusieurs attaques de créatures, qui ravagent le continent !|

    === "3"
        |   |   |
        |:---:|:---|
        |![Poster du film Spider-Man: No Way Home](images/posters/spiddd.jpg){width="150"}|Avec l'identité de Spiderman désormais révélée, celui-ci est démasqué et n'est plus en mesure de séparer sa vie normale en tant que Peter Parker des enjeux élevés d'être un superhéros. Lorsque Peter demande de l'aide au docteur Strange, les enjeux deviennent encore plus dangereux, l'obligeant à découvrir ce que signifie vraiment être Spiderman.|

### Black Panther

!!! example "Black Panther"
    ![Logo de Black Panther](images/logos/black.jpg){width="150"}

    1. **Black Panther** (2018)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Black Panther](images/posters/bla.jpg){width="150"}|Après les événements qui se sont déroulés dans Captain America : Civil War, T'Challa revient chez lui prendre sa place sur le trône du Wakanda, une nation africaine technologiquement très avancée mais lorsqu'un vieil ennemi resurgit, le courage de T'Challa est mis à rude épreuve, aussi bien en tant que souverain qu'en tant que Black Panther. Il se retrouve entraîné dans un conflit qui menace non seulement le destin du Wakanda mais celui du monde entier.|

### Captain Marvel

!!! example "Captain Marvel"
    ![Logo de Captain Marvel](images/logos/marvel.jpg){width="150"}

    1. **Captain Marvel** (2019)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Captain Marvel](images/posters/marv.jpg){width="150"}|Captain Marvel se bat dans le camp des Kree aux côtés de son maître, Yon-Rogg. Des images d'un passé qu'elle a oublié refont surface lorsqu'elle se fait enlever par les Skrulls, qui tentent d'extirper certaines informations de son cerveau. Après une bagarre, elle atterrit sur Terre. Elle est accueillie par Nick Fury, qui la prend sous son aile. Ensemble, ils déterreront des secrets bien enfouis et comprendront que tout n'est pas toujours aussi simple qu'on se l'imagine.|

### Black Widow

!!! example "Black Widow"
    ![Logo de Black Widow](images/logos/wido.jpg){width="150"}

    1. **Black Widow** (2021)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Black Widow](images/posters/bwid.jpg){width="150"}|Lorsqu'un complot dangereux en lien avec son passé ressurgit, Natasha Romanoff, alias Black Widow, doit y faire face. Tandis qu'elle se fait poursuivre par une force qui ne s'arrête devant rien, Natasha confronte des liens brisés ainsi que les conséquences de son passé en tant qu'espionne dans un temps avant qu'elle fasse partie des Avengers.|

### Shang-Chi

!!! example "Shang-Chi"
    ![Logo de Shang-Chi](images/logos/schi.png){width="150"}

    1. **Shang-Chi et la Légende des Dix Anneaux** (2021)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Shang-Chi et la Légende des Dix Anneaux](images/posters/shang.jpg){width="150"}|Shang-Chi est le fils du chef d'une puissante organisation criminelle. Le garçon a été élevé dès son plus jeune âge pour devenir un guerrier, mais il a décidé d'abandonner cette voie et de s'enfuir pour vivre une vie paisible. Mais tout change lorsqu'il est attaqué par un groupe d'assassins et qu'il est obligé de faire face à son passé.|

### Hulk

!!! warning "Le cas de : Hulk"
    ![Logo de Hulk](images/logos/hulk.jpg){width="150"}

    Les _Studios Marvel_ n'ont pas réalisé de film Hulk. Ce qui est particulier, c'est que _Disney_ ne possède pas les droits de diffusion en continu du film. Techniquement, _Universal Pictures_ possède toujours le personnage, bien qu'il soit prêté au MCU depuis plus d'une décennie maintenant. La plupart des fans peuvent se passer d'un film basé sur les origine du "_géant vert_" car ils les connaissent grâce aux comics. Mais si vous êtes nouveau dans le _fandom_ du MCU, nous pouvons vous conseiller le film suivant pour vous aider à mieux comprendre l'histoire du personnage.

    1. **L'incroyable Hulk** (2008)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film L'incroyable Hulk](images/posters/hulkk.jpg){width="150"}|Tapi dans les bas-fonds de Sao Paulo, Bruce Banner tente désespérément de percer le secret de la maladie qui l'afflige. Repéré par l'armée américaine, qui entend dupliquer le modèle pour en faire une arme guerrière, il leur file entre les doigts et rentre aux États-Unis, où il espère mettre la main sur le protocole médical qui l'a rendu mutant et également revoir sa fiancée Betty, biologiste qu'il a blessée gravement lors de sa première mutation.|

___
 
## Les films en équipe :

### Les Avengers

!!! example "Les Avengers"
    ![Logo des Avengers](images/logos/aven.jpg){width="150"}

    1. **Avengers** (2012)
    2. **Avengers : L'Ère d'Ultron** (2015)
    3. **Avengers: Infinity War** (2018)
    4. **Avengers: Endgame** (2019)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Avengers](images/affiches/aven.jpg){width="150"}|Quand un ennemi inattendu fait surface pour menacer la sécurité et l'équilibre mondial, Nick Fury, directeur de l'agence internationale pour le maintien de la paix, connue sous le nom du S.H.I.E.L.D., doit former une équipe pour éviter une catastrophe mondiale imminente. Un effort de recrutement à l'échelle mondiale est mis en place, pour finalement réunir l'équipe de super héros de rêve, dont Iron Man, l'incroyable Hulk, Thor, Captain America, Hawkeye et Black Widow.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Avengers : L'Ère d'Ultron](images/affiches/ultron.jpg){width="150"}|Alors qu'il tente de récupérer le sceptre de Loki avec l'aide de ses camarades Avengers, Tony Stark découvre que Strucker avait mis au point une intelligence artificielle révolutionnaire, plus puissante encore que Jarvis. Strucker, mis hors d'état de nuire, et le sceptre récupéré, Stark conçoit bientôt un projet insensé : relancer son programme de maintien de la paix, jusque-là en sommeil, grâce à cette conscience robotisée ultra-puissante.|

    === "3"
        |   |   |
        |:---:|:---|
        |![Poster du film Avengers: Infinity War](images/affiches/infin.jpg){width="150"}|Alors que les Avengers et leurs alliés ont continué de protéger le monde face à des menaces bien trop grandes pour être combattues par un héros seul, un nouveau danger est venu de l'espace : Thanos. Despote craint dans tout l'univers, Thanos a pour objectif de recueillir les six Pierres d'Infinité, des artefacts parmi les plus puissants de l'univers, et de les utiliser afin d'imposer sa volonté sur toute la réalité. Tous les combats que les Avengers ont menés culminent dans cette bataille.|

    === "4"
        |   |   |
        |:---:|:---|
        |![Poster du film Avengers: Endgame](images/affiches/endg.jpg){width="150"}|Le Titan Thanos, ayant réussi à s'approprier les six Pierres d'Infinité et à les réunir sur le Gantelet doré, a pu réaliser son objectif de pulvériser la moitié de la population de l'Univers. Cinq ans plus tard, Scott Lang, alias Ant-Man, parvient à s'échapper de la dimension subatomique où il était coincé. Il propose aux Avengers une solution pour faire revenir à la vie tous les êtres disparus, dont leurs alliés et coéquipiers : récupérer les Pierres d'Infinité dans le passé.|

### Les Gardiens de la Galaxie
    
!!! example "Les Gardiens de la Galaxie"
    ![Logo des Gardiens de la Galaxie](images/logos/gard.jpg){width="150"}

    1. **Les Gardiens de la Galaxie** (2014)
    2. **Les Gardiens de la Galaxie Vol. 2** (2017)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Les Gardiens de la Galaxie](images/affiches/gardd.jpg){width="150"}|Peter Quill est un aventurier traqué par tous les chasseurs de primes pour avoir volé un mystérieux globe convoité par le puissant Ronan, dont les agissements menacent l'univers tout entier. Lorsqu'il découvre le véritable pouvoir de ce globe et la menace qui pèse sur la galaxie, il conclut une alliance fragile avec quatre extraterrestres disparates.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Les Gardiens de la Galaxie Vol. 2](images/affiches/garddd.jpg){width="150"}|Musicalement accompagné de la 'Awesome Mixtape 2', ce deuxième volet propose à nouveau les aventures de l'équipe alors qu'elle traverse les confins du cosmos. Les gardiens doivent combattre pour rester unis alors qu'ils découvrent les mystères de la filiation de Peter Quill. Les vieux ennemis vont devenir de nouveaux alliés et des personnages bien connus des fans de comics vont venir aider nos héros et continuer à étendre l'univers Marvel.|

### Les Éternels

!!! example "Les Éternels"
    ![Logo des Éternels](images/logos/eter.jpg){width="150"}

    1. **Les Éternels** (2021)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Les Éternels](images/affiches/etern.jpg){width="150"}|Après les événements d'"Avengers : Endgame", une tragédie imprévue oblige les Éternels à sortir de l'ombre et à se rassembler à nouveau face à l'ennemi le plus ancien de la race humaine : les Déviants.|

____
 
## Les Séries (sur Disney+) :

### WandaVision

!!! example "WandaVision" 
    ![Poster de la série WandaVision](images/posters/wanda.webp){width="200"}

    - Saison 1 (2021)

    ??? note "Saison 1"
        (9 épisodes)

        Wanda et Vision vivent une adorable vie de couple dans la petite ville de Westview, et ce sous format de sitcom. Mais des évènements étranges poussent Vision à se demander si la vie qu'il vit est bien ce qu'elle paraît être, tandis que de son côté, Wanda voit son univers menacé par une nouvelle agence gouvernementale, le S.W.O.R.D., et par la perspicacité de son mari.

### Falcon et le Soldat de l'Hiver

!!! example "Falcon et le Soldat de l'Hiver" 
    ![Poster de la série Falcon et le Soldat de l'Hiver](images/posters/falcon.jpg){width="200"}

    - Saison 1 (2021)

    ??? note "Saison 1"
        (6 épisodes)

        Six mois après la fin des évènements liés à Thanos, Bucky Barnes fait équipe avec Sam Wilson, qui possédait le bouclier de Captain America avant de le remettre au gouvernement. Les deux hommes vont se lancer dans une aventure planétaire et vont devoir faire face aux Flag-Smashers, un groupe terroriste possédant des capacités physiques hors du commun, cherchant à faire revenir le monde à son état pendant l'Éclipse, et dirigé par Karli Morgenthau. Ils ont obtenu leurs capacités en volant le sérum des super-soldats à Power Broker, le dirigeant de l'île criminelle de Madripoor. Ils sont également confrontés à John Walker, un ancien militaire choisi par le gouvernement et le Conseil Mondial de Rapatriement (l'organisme international créé pour gérer les conséquences du retour des personnes disparues durant l'Éclipse) pour être le nouveau Captain America et qui a donc récupéré le bouclier.

### Loki

!!! example  "Loki"  
    ![Poster de la série Loki](images/posters/loki.jpg){width="200"}

    - Saison 1 (2021)

    ??? note "Saison 1"
        (6 épisodes)

        Après s'être emparé du Tesseract en 2012, Loki atterrit en plein désert de Gobi, dévisagé par des Mongols interloqués. C'est alors que surgissent par un portail spatio-temporel les « Minuteurs » du TVA menés par la chasseuse B-15, qui l'embarquent manu militari vers le tribunal des Gardiens du Temps. Il y apprend, incrédule, que cet organisme a été créé pour supprimer les différents flux temporels du « Multivers » afin qu'il n'y en ait plus qu'un, sa mission étant de préserver cet « éternel flux temporel ». Ceux qui tentent de le modifier créent un point de variation appelé « Nexus » et sont définis comme des « Variants », terme appliqué à Loki, puisque dans le flux temporel normal, il est censé mourir en 2018 des mains de Thanos.

### What if...?

!!! example  "What if...?" 
    ![Poster de la série What if...?](images/posters/what.jpg){width="200"}

    - Saison 1 (2021)

    ??? note "Saison 1"
        (9 épisodes)

        Uatu le Gardien observe des continuités alternatives de la Terre, où les événements ne se sont pas déroulés exactement de la même façon que dans la réalité présentée jusqu'alors dans l'univers cinématographique Marvel.

### Hawkeye

!!! example  "Hawkeye"
    ![Poster de la série Hawkeye](images/posters/hawk.jpg){width="200"}

    - Saison 1 (2021)

    ??? note "Saison 1"
        (6 épisodes)

        Kate Bishop, une jeune archère, se retrouve dans un complot, forçant Hawkeye à intervenir. Clint doit aider Kate à échapper à la "Mafia des Survêtes" et à résoudre un meurtre mystérieux. Après avoir échappé à une autre menace, Clint et Kate s'allient pour contrer un complot criminel.


