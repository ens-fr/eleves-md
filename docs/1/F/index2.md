# Le bon odre de visionnage :

!!! info "Pour un meilleur visionnage"
    Regarder les films _Marvel_ par "odre de sortie" (du plus ancien au plus récent) n'est pas forcément le meilleur choix. En effet, votre immersion dans l'univers fantastique des super-héros sera bien meilleure si vous décidez de suivre une certaine chronologie, celle du _MCU_. Entre autres, cela signifie que vous devez **regarder les films Marvel dans l'odre logique de l'univers dans lequel ils se déroulent**, car certains évènements d'un film se passent avant ceux d'un autre et sont donc nécessaire pour les comprendre. Tous les films (ainsi que les séries) doivent donc être visionnés selon un ordre précis pour ainsi mieux comprendre chacune des histoires et ne pas risquer d'être complètement perdu.

Voici donc, selon nous, ==l'odre parfait de visionnage== des films et séries Marvel en nous basant sur la continuité des évènements :

|Ordre|Affiche|Titre|Durée|Type|Année de sortie|
|:---:|:---:|:---:|:---:|:---:|:---:|
|1|![Poster du film Captain America : First Avenger](images/posters/cap.jpg){width="80"}|Captain America : First Avenger|2h04|Film|2011|
|2|![Poster du film Captain Marvel](images/posters/marv.jpg){width="80"}|Captain Marvel|2h04|Film|2019|
|3|![Poster du film Iron Man](images/posters/ironn.jpg){width="80"}|Iron Man|2h06|Film|2008|
|4|![Poster du film Iron Man 2](images/posters/ironnn.jpg){width="80"}|Iron Man 2|2h05|Film|2010|
|5|![Poster du film Thor](images/posters/thor.jpg){width="80"}|Thor|1h54|Film|2011|
|6|![Poster du film L'incroyable Hulk](images/posters/hulkk.jpg){width="80"}|L’Incroyable Hulk|1h52|Film|2008|
|7|![Poster du film Avengers](images/affiches/aven.jpg){width="80"}|Avengers|2h23|Film|2012|
|8|![Poster du film Thor : Le Monde des ténèbres](images/posters/thorr.jpg){width="80"}|Thor : Le Monde des ténèbres|1h52|Film|2013|
|9|![Poster du film Iron Man 3](images/posters/ironnnn.jpg){width="80"}|Iron Man 3|2h10|Film|2013|
|10|![Poster du film Captain America : Le Soldat de l'hiver](images/posters/capp.jpg){width="80"}|Captain America : Le Soldat de l’hiver|2h16|Film|2014|
|11|![Poster du film Les Gardiens de la Galaxie](images/affiches/gardd.jpg){width="80"}|Les Gardiens de la Galaxie|2h01|Film|2014|
|12|![Poster du film Les Gardiens de la Galaxie Vol. 2](images/affiches/garddd.jpg){width="80"}|Les Gardiens de la galaxie Vol. 2|2h17|Film|2017|
|13|![Poster du film Avengers : L'Ère d'Ultron](images/affiches/ultron.jpg){width="80"}|Avengers : L’Ère d’Ultron|2h22|Film|2015|
|14|![Poster du film Ant-Man](images/posters/ant.jpg){width="80"}|Ant-Man|1h58|Film|2015|
|15|![Poster du film Captain America: Civil War](images/posters/cappp.jpg){width="80"}|Captain America : Civil War|2h28|Film|2016|
|16|![Poster du film Black Widow](images/posters/bwid.jpg){width="80"}|Black Widow|2h13|Film|2021|
|17|![Poster du film Black Panther](images/posters/bla.jpg){width="80"}|Black Panther|2h15|Film|2018|
|18|![Poster du film Spider-Man: Homecoming](images/posters/spid.jpg){width="80"}|Spider-Man : Homecoming|2h13|Film|2017|
|19|![Poster du film Doctor Strange](images/posters/doc.jpg){width="80"}|Doctor Strange|1h55|Film|2016|
|20|![Poster du film Thor : Ragnarok](images/posters/thorrr.jpg){width="80"}|Thor Ragnarok|2h10|Film|2017|
|21|![Poster du film Ant-Man et la Guêpe](images/posters/antt.jpg){width="80"}|Ant-Man et la guêpe|1h58|Film|2018|
|22|![Poster du film Avengers: Infinity War](images/affiches/infin.jpg){width="80"}|Avengers : Infinity War|2h29|Film|2018|
|23|![Poster du film Avengers: Endgame](images/affiches/endg.jpg){width="80"}|Avengers : Endgame|3h02|Film|2019|
|24|![Poster du film Spider-Man: Far From Home](images/posters/spidd.jpg){width="80"}|Spider-Man : Far From Home|2h10|Film|2019|
|25|![Poster du film Spider-Man: No Way Home](images/posters/spiddd.jpg){width="80"}|Spider-Man : No Way Home|2h28|Film|2021|
|26|![Poster de la série Loki](images/posters/loki.jpg){width="80"}|Loki|43-55 min/ep|Série|2021|
|27|![Poster de la série What if...?](images/posters/what.jpg){width="80"}|What If…?|32-38 min/ep|Série|2021|
|28|![Poster du film Les Éternels](images/affiches/etern.jpg){width="80"}|Les Eternels|2h37|Film|2021|
|29|![Poster de la série WandaVision](images/posters/wanda.webp){width="80"}|WandaVision|31-51 min/ep|Série|2021|
|30|![Poster de la série Falcon et le Soldat de l'Hiver](images/posters/falcon.jpg){width="80"}|Falcon et le Soldat de l'hiver|51-62 min/ep|Série|2021|
|31|![Poster du film Shang-Chi et la Légende des Dix Anneaux](images/posters/shang.jpg){width="80"}|Shang-Chi et la Légende des Dix Anneaux|2h12|Film|2021|
|32|![Poster de la série Hawkeye](images/posters/hawk.jpg){width="80"}|Hawkeye|41-62 min/ep|Série|2021|


!!! warning "Attention !"
    Regarder tous ces films représente **59 heures et 34 minutes de visionnage**, soit environ **2 jours et demi** !
    Comptez en plus **26 heures et 41 minutes** si vous décidez d'aussi regarder les séries.
    Ce qui nous fait un total de ==**3 jours, 14 heures et 15 minutes**== !
    Vous avez intérêt à prévoire beaucoup de popcorn !