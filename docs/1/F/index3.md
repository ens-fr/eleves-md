# Les Films complémentaires

!!! info "D'autres films ?"
    Il est possible que pour comprendre certains des films du MCU, vous ayez besoin de visionner d'autres films qui, eux, **n'ont pas été réalisés par les Studios Marvel**, (comme expliqué plus tôt avec le cas du film _Hulk_).
    Par exemple, pour comprendre le film _Spider-Man : No Way Home_, vous aurez besoin de visionner les premiers long-métrages Spider-Man réalisés par la société de production _Columbia Pictures_ (filiale de ==_Sony Pictures Entertainment_==).

___

## Les Spider-Man

Plusieurs sagas sur _L'Homme Araignée_ ont été réalisées, ce qui fait que jusqu'à maintenant **3 acteurs différents** ont endossé le costume du super héro dans en tout **8 films différents**.

!!! example "Spider-Man : Tobey Maguire"
    ![Logo de Spider-Man](images/complements/spideyt.webp){width="150"}

    1. **Spider-Man** (2002)
    2. **Spider-Man 2** (2004)
    3. **Spider-Man 3** (2007)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Spider-Man](images/complements/tobey.jpg){width="150"}|Orphelin, Peter Parker est élevé par sa tante May et son oncle Ben dans le quartier Queens de New York. Tout en poursuivant ses études à l'université, il trouve un emploi de photographe au journal Daily Bugle. Il partage son appartement avec Harry Osborn, son meilleur ami, et rêve de séduire la belle Mary Jane.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Spider-Man 2](images/complements/tobeyy.jpg){width="150"}|Ecartelé entre son identité secrète de Spider-Man et sa vie d'étudiant, Peter Parker n'a pas réussi à garder celle qu'il aime, Mary Jane, qui est aujourd'hui comédienne et fréquente quelqu'un d'autre. Guidé par son seul sens du devoir, Peter vit désormais chacun de ses pouvoirs à la fois comme un don et comme une malédiction.|

    === "3"
        |   |   |
        |:---:|:---|
        |![Poster du film Spider-Man 3](images/complements/tobe.jpg){width="150"}|Peter Parker a enfin réussi à concilier son amour pour Mary-Jane et ses devoirs de super-héros, mais l'horizon s'obscurcit. La brutale mutation de son costume, qui devient noir, décuple ses pouvoirs et transforme également sa personnalité pour laisser ressortir l'aspect sombre et vengeur que Peter s'efforce de contrôler.|


!!! example "Spider-Man : Andrew Garfield"
    ![Logo de Spider-Man](images/complements/spideya.jpg){width="120"}

    1. **The Amazing Spider-Man** (2012)
    2. **The Amazing Spider-Man : Le Destin d'un héros** (2014)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film The Amazing Spider-Man](images/complements/andrew.jpg){width="150"}|Abandonné par ses parents lorsqu'il était enfant, Peter Parker, alias Spider-Man, a été élevé par son oncle et sa tante. Cependant, Peter voudrait comprendre qui il est pour pouvoir accepter son parcours. Il entame alors une quête pour élucider la disparition de ses parents, ce qui le conduit rapidement à Oscorp et au laboratoire du Dr Curt Connors. Spider-Man va bientôt se retrouver face au Lézard, l'alter ego de Connors. En décidant d'utiliser ses pouvoirs, il va embrasser son destin.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film The Amazing Spider-Man : Le Destin d'un héros](images/complements/andreww.jpg){width="150"}|Ce n'est un secret pour personne que le combat le plus rude de Spider-Man est celui qu'il mène contre lui-même en tentant de concilier la vie quotidienne de Peter Parker et les lourdes responsabilités de Spider-Man. Peter Parker va se rendre compte qu'il fait face à un conflit de bien plus grande ampleur. Face à Electro, Peter devra affronter un ennemi nettement plus puissant que lui. Au retour de son vieil ami Harry Osborn, il se rend compte que tous ses ennemis ont un point commun : OsCorp.|

___

## Venom

!!! example "Venom"
    ![Logo de Venom](images/complements/venom.png){width="150"}

    1. **Venom** (2018)
    2. **Venom : Let There Be Carnage** (2021)

    === "1"
        |   |   |
        |:---:|:---|
        |![Poster du film Venom](images/complements/venomm.jpg){width="150"}|Eddie Brock est un journaliste d'enquête très connu à San Francisco. Après avoir fait déraper une entrevue avec Carlton Drake, le fondateur de la puissante compagnie Life Foundation, il perd son emploi. Quelques mois plus tard, quand une employée de Drake le rejoint afin de lui faire part des dangereuses expériences qui sont menées dans les laboratoires de la Life Foundation, Eddie se rend sur place pour investiguer. Il entrera alors en contact avec un symbiote venu d'une autre planète.|

    === "2"
        |   |   |
        |:---:|:---|
        |![Poster du film Venom : Let There Be Carnage](images/complements/venommm.jpg){width="150"}|Après avoir choisi le journaliste d'enquête Eddie Brock comme hôte, le symbiote extraterrestre Venom doit affronter un nouvel ennemi du nom de Carnage, qui se trouve à être l'alter ego du tueur en série Cletus Kasady.|

    !!! warning "Information"
        Les films Venom sont nécessaires pour comprendre la **scène post-generique** du film _Spider-Man : No Way Home_.

___

Si vous souhaitez avoir plus de détails sur les films de l'Univers Cinématographique Marvel, rendez-vous sur la [page  Wikipédia du MCU](https://fr.wikipedia.org/wiki/Univers_cin%C3%A9matographique_Marvel).
Et si vous souhaitez visionner les différents films et séries présentés, ceux-ci sont disponibles sur [Disney +](https://www.disney.fr/disney-plus-tout-ce-que-vous-devez-savoir).

___