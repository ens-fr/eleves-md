# Système solaire

---------------------------------------------------------------------------------------------------------

🌠 Systema solaris


---------------------------------------------------------------------------------------------------------

Le **Système solaire** (avec majuscule), ou **système solaire** (sans majuscule), est le [système planétaire](https://fr.wikipedia.org/wiki/Syst%C3%A8me_plan%C3%A9taire) du [Soleil](https://fr.wikipedia.org/wiki/Soleil), auquel appartient la [Terre](https://fr.wikipedia.org/wiki/Terre). Il est composé de cette [étoile](https://fr.wikipedia.org/wiki/%C3%89toile) et des objets célestes gravitant autour d'elle : les huit [planètes](https://fr.wikipedia.org/wiki/Plan%C3%A8te) confirmées et leurs 214 satellites naturels connus (appelés usuellement des « lunes »), les cinq [planètes naines](https://fr.wikipedia.org/wiki/Plan%C3%A8te_naine) et leurs neuf satellites connus, ainsi que des milliards de petits corps (la presque totalité des [astéroïdes](https://fr.wikipedia.org/wiki/Ast%C3%A9ro%C3%AFde) et autres planètes mineures, les [comètes](https://fr.wikipedia.org/wiki/Com%C3%A8te), les [poussières cosmiques](https://fr.wikipedia.org/wiki/Poussi%C3%A8re_interstellaire), etc.).

|![systèmesolaire](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Solar_sys.jpg/1280px-Solar_sys.jpg)|
|-----|
|Principaux composants du Système solaire (échelle non respectée). De gauche à droite : Pluton, Neptune, Uranus, Saturne, Jupiter, la ceinture d'astéroïdes, le Soleil, Mercure, Vénus, la Terre et la Lune, enfin Mars. Une comète est également représentée sur la gauche.|

!!! info "Histoire"
    Le Système solaire fait partie de la [galaxie](https://fr.wikipedia.org/wiki/Galaxie) appelée [Voie lactée](https://fr.wikipedia.org/wiki/Voie_lact%C3%A9e), où il réside dans le [bras d'Orion](https://fr.wikipedia.org/w..iki/Bras_d%27Orion). Il est situé à environ 8 kpc (∼26 100 a.l.) du [centre galactique](https://fr.wikipedia.org/wiki/Centre_galactique), autour duquel il effectue une [révolution](https://fr.wikipedia.org/wiki/P%C3%A9riode_de_r%C3%A9volution) en 225 à 250 millions d'années. Il s'est formé il y a un peu moins de 4,6 milliards d'années à partir de l'[effondrement gravitationnel](https://fr.wikipedia.org/wiki/Effondrement_gravitationnel) d'un [nuage moléculaire](https://fr.wikipedia.org/wiki/Nuage_mol%C3%A9culaire), suivi de la constitution d'un [disque protoplanétaire](https://fr.wikipedia.org/wiki/Disque_protoplan%C3%A9taire) selon l'[hypothèse de la nébuleuse](https://fr.wikipedia.org/wiki/Hypoth%C3%A8se_de_la_n%C3%A9buleuse).

De façon schématique, le Système solaire est composé du Soleil, qui le domine gravitationnellement — il comprend 99,85 % de sa masse — et fournit de l'[énergie](https://fr.wikipedia.org/wiki/%C3%89nergie) par [fusion nucléaire](https://fr.wikipedia.org/wiki/Fusion_nucl%C3%A9aire) de l'[hydrogène en hélium](https://fr.wikipedia.org/wiki/Nucl%C3%A9osynth%C3%A8se_stellaire#Nucl%C3%A9osynth%C3%A8se_calme). Par ordre d'éloignement croissant à l'étoile, le [Système solaire interne](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire_interne_et_externe) comprend quatre [planètes telluriques](https://fr.wikipedia.org/wiki/Plan%C3%A8te_tellurique) internes, principalement composées de roches et de métaux ([Mercure](https://fr.wikipedia.org/wiki/Mercure_(plan%C3%A8te)), [Vénus](https://fr.wikipedia.org/wiki/V%C3%A9nus_(plan%C3%A8te)), la Terre et [Mars](https://fr.wikipedia.org/wiki/Mars_(plan%C3%A8te))) puis une [ceinture d'astéroïdes](https://fr.wikipedia.org/wiki/Ceinture_d%27ast%C3%A9ro%C3%AFdes) de petits corps rocheux, dont la planète naine [Cérès](https://fr.wikipedia.org/wiki/(1)_C%C3%A9r%C3%A8s). Plus loin orbitent les quatre [planètes géantes](https://fr.wikipedia.org/wiki/Plan%C3%A8te_g%C3%A9ante) du Système solaire externe : successivement deux [géantes gazeuses](https://fr.wikipedia.org/wiki/Plan%C3%A8te_g%C3%A9ante_gazeuse) constituées majoritairement d'hydrogène et d'hélium que sont [Jupiter](https://fr.wikipedia.org/wiki/Jupiter_(plan%C3%A8te)) et [Saturne](https://fr.wikipedia.org/wiki/Saturne_(plan%C3%A8te)) — qui contiennent par ailleurs la grande majorité de la masse totale en orbite autour du Soleil — et deux [géantes de glaces](https://fr.wikipedia.org/wiki/Plan%C3%A8te_g%C3%A9ante_de_glaces) que sont [Uranus](https://fr.wikipedia.org/wiki/Uranus_(plan%C3%A8te)) et [Neptune](https://fr.wikipedia.org/wiki/Neptune_(plan%C3%A8te)), contenant une plus grande part de [substances volatiles](https://fr.wikipedia.org/wiki/Substance_volatile) comme l'eau, l'ammoniac et le méthane. Tous ont une orbite proche du cercle et sont concentrés près du plan de l'[écliptique](https://fr.wikipedia.org/wiki/%C3%89cliptique), le plan de rotation de la Terre.

Les objets situés au-delà de l'orbite de Neptune, dits [transneptuniens](https://fr.wikipedia.org/wiki/Objet_transneptunien), comprennent notamment la [ceinture de Kuiper](https://fr.wikipedia.org/wiki/Ceinture_de_Kuiper) et le [disque des objets épars](https://fr.wikipedia.org/wiki/Disque_des_objets_%C3%A9pars), formés d'objets glacés. Quatre planètes naines glacées se trouvent dans la région transneptunienne et sont également appelées [plutoïdes](https://fr.wikipedia.org/wiki/Pluto%C3%AFde) : Pluton — auparavant classée comme planète —, [Hauméa](https://fr.wikipedia.org/wiki/(136108)_Haum%C3%A9a), [Makémaké](https://fr.wikipedia.org/wiki/(136472)_Mak%C3%A9mak%C3%A9) et [Éris](https://fr.wikipedia.org/wiki/(136199)_%C3%89ris). L'[héliopause](https://fr.wikipedia.org/wiki/H%C3%A9liopause), limite magnétique du Système solaire, est définie par l'arrêt des [vents solaires](https://fr.wikipedia.org/wiki/Vent_solaire) face aux vents du milieu int—, ou lunes, sont les objets en orbite autour des planètes, des planètes naines et des petits corps du Système solaire plutôt qu'autour du Soleil2. Les statuts ambigus de la Lune et surtout de Charon, qui pourraient former un système binaire avec respectivement la Terre et Pluton, ne sont pas encore définitivement tranchés, bien que ces corps soient toujours classés comme satellites15,16. s laquelle une zone sphérique hypothétique, le [nuage de Oort](https://fr.wikipedia.org/wiki/Nuage_de_Oort), pourrait exister et être la source des comètes à longue période.

Toutes les planètes du Système solaire à partir de la Terre possèdent des satellites en orbite — certains, tels que [Ganymède](https://fr.wikipedia.org/wiki/Ganym%C3%A8de_(lune)) et [Titan](https://fr.wikipedia.org/wiki/Titan_(lune)), sont plus grands que Mercure —, tandis que chacune des quatre planètes externes est en outre entourée d’un [système d'anneaux](https://fr.wikipedia.org/wiki/Anneau_plan%C3%A9taire) de poussières et d’autres particules, dont le plus proéminent est celui de Saturne. Toutes les planètes, sauf la Terre, portent les noms de dieux et déesses de la [mythologie romaine](https://fr.wikipedia.org/wiki/Mythologie_romaine). La Terre, dotée d'une épaisse atmosphère et de 71 % d'eau liquide, est la seule planète du Système solaire à abriter la vie et une espèce pensante qui agit sur son évolution. Dans l'état des connaissances humaines, cette planète du Système solaire ne connait pas d'équivalent dans l'[Univers](https://fr.wikipedia.org/wiki/Univers). 

| 🌌 | Caractéristiques générales |
|---------|-------------|
| Âge     | 4,567 Ga  |
| Localisation  | Nuage interstellaire local, Bulle locale, bras d'Orion, Voie lactée    |
| Masse du système   | 1,991 9 × 1030 kg  |
| Système planétaire le plus proche | Système de Proxima Centauri (4,22 al), dans le système Alpha Centauri (4,37 al)  |



### Terminologie

---------------------------------------------------------------------------------------------------------

Articles connexes : [Définition des planètes](https://fr.wikipedia.org/wiki/D%C3%A9finition_des_plan%C3%A8tes), [Définition des planètes de l'Union astronomique internationale](https://fr.wikipedia.org/wiki/D%C3%A9finition_des_plan%C3%A8tes) et [Planète naine](https://fr.wikipedia.org/wiki/Plan%C3%A8te_naine).

---------------------------------------------------------------------------------------------------------

>Depuis la décision prise le 24 août 2006 par l'Union astronomique internationale, les objets ou corps orbitant directement autour du Soleil sont officiellement divisés en trois classes : planètes, planètes naines et petits corps.

|![planetes](https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Solar_System_size_to_scale_fr.svg/1280px-Solar_System_size_to_scale_fr.svg.png)|
|-----|
|Planètes et planètes naines du Système solaire. Les dimensions du Soleil et des planètes sont à l’échelle, mais pas leurs distances.|


      1-Une planète est un corps en orbite autour du Soleil, suffisamment massif pour avoir une forme sphérique et avoir nettoyé son voisinage immédiat de tous les objets plus petits. On connaît huit planètes : Mercure, Vénus, la Terre, Mars, Jupiter, Saturne, Uranus et Neptune qui sont toutes nommées, sauf la Terre, d'après des divinités de la mythologie romaine. La plupart de leurs satellites naturels sont, eux aussi, nommés d'après des personnages de la mythologie grecque ou romaine

      2-Une planète naine est un corps en orbite autour du Soleil qui, bien que suffisamment massif pour avoir une forme sphérique — concept appelé équilibre hydrostatique —, n’a pas fait place nette dans son voisinage. En 2021, cinq corps sont officiellement désignés de la sorte : Cérès, Pluton, Éris, Makémaké et Hauméa. D’autres corps pourraient l’être dans le futur, tels que Gonggong, Quaoar, Sedna ou Orcus

      3-Tous les autres objets en orbite directe autour du Soleil sont classés comme petits corps du Système solaire.
   
  
Les 214 [satellites naturels](https://fr.wikipedia.org/wiki/Satellite_naturel) — en 2021, 158 sont confirmés et 56 sont non confirmés, donc sans nom —, ou lunes, sont les objets en orbite autour des planètes, des planètes naines et des petits corps du Système solaire plutôt qu'autour du Soleil. Les statuts ambigus de la Lune et surtout de Charon, qui pourraient former un [système binaire](https://fr.wikipedia.org/wiki/Syst%C3%A8me_binaire_(astronomie)) avec respectivement la Terre et Pluton, ne sont pas encore définitivement tranchés, bien que ces corps soient toujours classés comme satellites. 

La classification proposée par l'[Union astronomique internationale](https://fr.wikipedia.org/wiki/Union_astronomique_internationale) ne fait pas l'unanimité. À la suite du vote de 2006, une pétition réunissant les signatures de plus de 300 [planétologues](https://fr.wikipedia.org/wiki/Plan%C3%A9tologie) et astronomes majoritairement américains — Pluton étant alors la seule planète découverte par un Américain — est lancée pour contester la validité scientifique de la nouvelle définition d'une planète ainsi que son mode d'adoption. Les responsables de l'UAI annoncent qu'aucun retour en arrière n'aura lieu et les astronomes jugent très improbable que Pluton puisse être à nouveau considérée comme une planète.

!!! info "Étymologie"
    Concernant la majuscule au nom « Système solaire », la forme tout en minuscules est, au sens strict, suffisante, étant donné qu'il n'y a qu'un « système solaire » puisqu'il n'y a qu'un « Soleil ». Cependant, les autres étoiles étant parfois, par analogie, appelées des « soleils », le nom de « système solaire » est de la même façon parfois employé dans un sens général pour signifier « système planétaire » ; « Système solaire », écrit avec une majuscule, permet alors de distinguer notre système planétaire, par ellipse de « système planétaire solaire ».

|![syssolaire](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Solar-System-fr.png/800px-Solar-System-fr.png)
|-----|
|Vue d'ensemble du Système solaire. Le Soleil, les planètes, les planètes naines et les satellites naturels sont à l'échelle pour leurs tailles relatives, et non pour les distances. Les lunes sont répertoriées près de leurs planètes par ordre croissant d'orbites ; seules les plus grandes lunes pour chaque planète sont indiquées.|

|![échelle2](https://upload.wikimedia.org/wikipedia/commons/3/3d/Solar_system_scale_edit.jpg)|
|----|
|Échelle des distances des principaux corps du Système solaire jusqu'à Pluton selon leur demi-grand axe. Seules les distances au Soleil sont à l'échelle, pas les tailles des objets.|

### Structure
---------------------------------------------------------------------------------------------------------------
### **Généralités**
..........................................................................................................................................................................................

Le principal corps céleste du Système solaire est le Soleil, une [étoile naine jaune](https://fr.wikipedia.org/wiki/Naine_jaune) de la séquence principale qui contient 99,85 % de toute la masse connue du Système solaire et le domine gravitationnellement. Les huit planètes et Pluton représentent ensuite 0,135 % de la masse restante, Jupiter et Saturne représentant 90 % de celle-ci à elles seules. Les objets restants (y compris les autres planètes naines, les satellites naturels, les astéroïdes et les comètes) constituent ainsi ensemble environ 0,015 % de la masse totale du Système solaire. 

|![comparaison](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Planets_and_sun_size_comparison.jpg/800px-Planets_and_sun_size_comparison.jpg)
|-----|
|Comparaison de taille entre le Soleil et les planètes du Système solaire.|

La plupart des grands objets en orbite autour du Soleil le sont dans un plan proche de celui de l’orbite terrestre, le plan de l'[écliptique](https://fr.wikipedia.org/wiki/%C3%89cliptique). Le plan d’orbite des planètes est très proche de celui de l’écliptique, tandis que les comètes et les objets de la [ceinture de Kuiper](https://fr.wikipedia.org/wiki/Ceinture_de_Kuiper) ont pour la plupart une orbite qui forme un angle significativement plus grand par rapport à lui. À la suite de la formation du Système solaire, les planètes — et la grande majorité des autres objets — gravitent autour de l'étoile dans la même direction que la rotation du Soleil, soit le sens antihoraire vu du dessus du pôle Nord de la Terre. Il existe toutefois des exceptions, comme la [comète de Halley](https://fr.wikipedia.org/wiki/1P/Halley) orbitant dans un sens rétrograde. De même, la plupart des plus grandes lunes gravitent autour de leurs planètes dans cette direction prograde — Triton étant la plus grande exception rétrograde, autour de Neptune — et la plupart des grands objets ont un sens de rotation prograde — Vénus étant une exception rétrograde notable, comme Uranus dans une certaine mesure. 

|![syssolaire2](https://upload.wikimedia.org/wikipedia/commons/2/2b/Solarsystem.jpg)
|-----|
|Le plan de l’écliptique vu par la mission Clementine, alors que le Soleil était partiellement masqué par la Lune. Trois planètes sont visibles dans la partie gauche de l’image (par ordre d'éloignement au Soleil) : Mercure, Mars et Saturne.|

Le Système solaire se compose essentiellement, pour ses objets les plus massifs, du Soleil, de quatre planètes intérieures relativement petites entourées d'une ceinture d'astéroïdes principalement rocheux et de quatre planètes géantes entourées par la ceinture de Kuiper, constituée d'objets principalement glacés. Les astronomes divisent informellement cette structure en régions distinctes : le Système solaire interne comprenant les quatre [planètes telluriques](https://fr.wikipedia.org/wiki/Plan%C3%A8te_tellurique) et la ceinture d'astéroïdes puis le Système solaire externe comprenant tout ce qui est au-delà de la ceinture, notamment les quatre planètes géantes. Depuis la découverte de la ceinture de Kuiper, les parties les plus extérieures du Système solaire situées après l'orbite de Neptune sont considérées comme une région distincte constituée des [objets transneptuniens](https://fr.wikipedia.org/wiki/Objet_transneptunien). 

La plupart des planètes du Système solaire ont leur propre système secondaire, comprenant notamment des satellites naturels en orbite autour d'eux. Deux satellites, [Titan](https://fr.wikipedia.org/wiki/Titan_(lune)) (autour de Saturne) et [Ganymède](https://fr.wikipedia.org/wiki/Ganym%C3%A8de_(lune)) (autour de Jupiter), sont plus grands que la planète Mercure. Dans le cas des quatre planètes géantes, des anneaux planétaires — de fines bandes de minuscules particules — composent également l'entourage de la planète. La plupart des plus grands satellites naturels sont en [rotation synchrone](https://fr.wikipedia.org/wiki/Rotation_synchrone), c'est-à-dire qu'ils présentent en permanence une même face à la planète autour de laquelle ils gravitent. 

|![orbites](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Oort_cloud_Sedna_orbit-fr.svg/800px-Oort_cloud_Sedna_orbit-fr.svg.png)
|-----|
|Les orbites des principaux corps du Système solaire, à l’échelle.|

Les trajectoires des objets gravitant autour du Soleil suivent les [lois de Kepler](https://fr.wikipedia.org/wiki/Lois_de_Kepler) : ce sont approximativement des [ellipses](https://fr.wikipedia.org/wiki/Ellipse_(math%C3%A9matiques)), dont l'un des foyers est le Soleil. Les objets plus proches du Soleil (dont les demi-grand axes sont plus petits) se déplacent plus rapidement, car ils sont plus affectés par son influence gravitationnelle. Sur une [orbite elliptique](https://fr.wikipedia.org/wiki/Orbite_elliptique), la distance entre un corps et le Soleil varie au cours de son année : la distance la plus proche d'un corps avec le Soleil est son [périhélie](https://fr.wikipedia.org/wiki/Apside), tandis que son point le plus éloigné du Soleil est son aphélie. Les orbites des planètes sont presque circulaires, mais de nombreuses comètes, astéroïdes, objets de la ceinture de Kuiper et du [nuage de Oort](https://fr.wikipedia.org/wiki/Nuage_de_Oort) peuvent suivre des orbites très diverses, pouvant être hautement elliptiques — présentant une très grande excentricité orbitale — ou encore s'éloigner du plan de l'écliptique avec une forte inclinaison orbitale. 

|![vitesseorbitale](https://upload.wikimedia.org/wikipedia/commons/0/08/Solarsystem3DJupiter.gif)
|-----|
|Plus les planètes sont proches du Soleil, plus leur vitesse orbitale est grande (ici, elles sont toutes représentées sauf Neptune).|

>Bien que le Soleil domine le système en masse, il ne représente qu'environ 0,5 % à 2 % de son moment cinétique. Les planètes représentent ainsi la quasi-totalité du reste du moment cinétique en raison de la combinaison de leur masse, de leur orbite et de leur distance au Soleil ; la contribution des comètes est peut-être également significative39. Par exemple, Jupiter représente à elle seule environ 60 % du moment cinétique total.

???+ "Plus d'infos sur le Soleil"
     Le Soleil, qui comprend presque toute la matière du Système solaire, est composé en masse d'environ 70 % d'hydrogène et de 28 % d'hélium. Jupiter et Saturne, qui comprennent presque toute la matière restante, sont également principalement composés d'hydrogène et d'hélium et sont donc des planètes géantes gazeuses. Un gradient de composition est observé dans le Système solaire, créé par la chaleur et la [pression de rayonnement](https://fr.wikipedia.org/wiki/Pression_de_rayonnement) du Soleil. Les objets plus proches du Soleil, plus affectés par la chaleur et la pression lumineuse, sont composés d'éléments à [point de fusion](https://fr.wikipedia.org/wiki/Point_de_fusion) élevé, c'est-à-dire de roches telles que les [silicates](https://fr.wikipedia.org/wiki/Silicate), le fer ou le nickel, qui sont restées solides dans presque toutes les conditions dans la [protonébuleuse planétaire](https://fr.wikipedia.org/wiki/Proton%C3%A9buleuse_plan%C3%A9taire). Les objets plus éloignés du Soleil sont composés en grande partie de matériaux de points de fusion plus faibles : les gaz, des matériaux qui ont également une haute [pression de vapeur](https://fr.wikipedia.org/wiki/Pression_de_vapeur) et sont toujours en phase gazeuse, comme l'hydrogène, l'hélium et le néon, et les glaces qui ont des points de fusion allant jusqu'à quelques centaines de kelvins, comme l'eau, le méthane, l'ammoniac, le sulfure d'hydrogène et le dioxyde de carbone. Ces dernières peuvent être trouvées sous phases solide, liquide ou gazeuse à divers endroits du Système solaire, alors que dans la nébuleuse, elles sont soit en phase solide, soit en phase gazeuse. Les glaces composent la majorité des satellites des planètes géantes et sont en plus grandes proportions encore dans Uranus et Neptune (appelées « géantes de glace ») et les nombreux petits objets qui se trouvent au-delà de l'orbite de Neptune. Ensemble, les gaz et les glaces sont désignés sous le nom de [substances volatiles](https://fr.wikipedia.org/wiki/Substance_volatile). La limite du Système solaire au-delà de laquelle ces substances volatiles pourraient se condenser est la [ligne des glaces](https://fr.wikipedia.org/wiki/Ligne_des_glaces) et se situe à environ 5 ua du Soleil. 

### Distances et échelles 📏
..........................................................................................................................................................................................

La distance moyenne entre la Terre et le Soleil définit l'unité astronomique, qui vaut par convention près de 150 millions de kilomètres. Jupiter, la plus grande planète, est à 5,2 ua du Soleil et a un rayon de 71 000 km, alors que la planète la plus éloignée, Neptune, est située à environ 30 ua du Soleil. À quelques exceptions près, plus une planète ou une ceinture est éloignée du Soleil, plus la distance entre son orbite et l'orbite de l'objet suivant le plus proche du Soleil est grande. Par exemple, Vénus est environ 0,33 ua plus éloignée du Soleil que Mercure, tandis que Saturne est environ 4,3 ua plus éloignée de Jupiter et que l'orbite de Neptune se trouve 10,5 ua plus loin que celle d'Uranus. Par le passé, des astronomes ont tenté de déterminer une relation entre ces distances orbitales, notamment par la [loi de Titius-Bode](https://fr.wikipedia.org/wiki/Loi_de_Titius-Bode), mais aucune thèse de ce type n'a finalement été validée.

>Certaines [modélisations du Système solaire](https://fr.wikipedia.org/wiki/Mod%C3%A9lisation_du_Syst%C3%A8me_solaire) visent à vulgariser les échelles relatives du Système solaire. Ainsi des planétaires, ensembles mécaniques mobiles, tandis que d'autres représentations peuvent s'étendre à travers des villes ou des régions entières. Le plus grand modèle de ce type, le Système solaire suédois, utilise l'Avicii Arena à Stockholm — d'une hauteur de 110 mètres — en guise de Soleil et, suivant cette échelle, Jupiter est une sphère de 7,5 mètres à l'aéroport de Stockholm-Arlanda situé à 40 km du stade. L'objet le plus éloigné de la modélisation est Sedna, un objet transneptunien représenté par une sphère de 10 cm à [Luleå](https://fr.wikipedia.org/wiki/Lule%C3%A5), à 912 km de la capitale suédoise.

|![distances](https://upload.wikimedia.org/wikipedia/fr/timeline/jqllgnkyhcxcpx4sb06prz4l8mg1745.png)
|-----|
|Distances de différents corps du Système solaire au Soleil. Les côtés gauches et droits de chaque barres correspondent au périhélie et à l'aphélie de la trajectoire de chaque corps ; ainsi, de longues barres soulignent une grande excentricité orbitale.
Les diamètres du Soleil (1,4 million de kilomètres) et de la plus grande planète Jupiter (0,14 million de kilomètres) sont tous les deux trop petits pour apparaître en comparaison sur ce diagramme.|

### Soleil
---------------------------------------------------------------------------------------------------------------

|![soleil](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Solar_prominence_from_STEREO_spacecraft_September_29,_2008.jpg)
|-----|
|Le Soleil lors d'une importante éruption solaire, pris en 2008 par la sonde STEREO en ultraviolets et représenté avec de fausses couleurs.|

Le Soleil est une naine jaune, une étoile de [type spectral](https://fr.wikipedia.org/wiki/Type_spectral) G2V comme beaucoup d'autres au sein de notre galaxie : la Voie lactée contient entre 200 et 400 milliards d'étoiles, dont 10 % seraient des naines jaunes. Sa très grande masse, environ 333 000 fois la masse terrestre, permet à la densité en son cœur d’être suffisamment élevée pour provoquer des réactions de fusion nucléaire en continu. Chaque seconde, le cœur du Soleil fusionne 620 millions de tonnes d'hydrogène en 615,7 millions de tonnes d'hélium. La différence de masse est convertie en énergie selon la formule [E = mc2](https://fr.wikipedia.org/wiki/E%3Dmc2) et représente une puissance d'environ 4 × 1026 watts — environ un million de fois la consommation électrique annuelle des États-Unis toutes les secondes —, principalement diffusée dans l'espace sous forme de rayonnement électromagnétique solaire culminant dans la lumière visible. La température à sa surface visible est de 5 570 K tandis qu'elle atteint quinze millions de kelvins en son centre.

|![diagramme](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:HRDiagram-Fr.png)
|-----|
|Le diagramme de Hertzsprung-Russell ; la séquence principale va du bas à droite au haut à gauche.|

Le Soleil est une naine jaune modérément grande, sa température étant intermédiaire entre celle des étoiles bleues, plus chaudes, et celle des étoiles les plus froides. Les étoiles plus brillantes et plus chaudes que le Soleil sont rares, tandis que les étoiles sensiblement plus sombres et plus froides, appelées naines rouges, constituent 85 % des étoiles de la Voie lactée. Il se situe vers le milieu de la séquence principale du [diagramme de Hertzsprung-Russell](https://fr.wikipedia.org/wiki/Diagramme_de_Hertzsprung-Russell) et le calcul du rapport entre l’hydrogène et l’hélium à l’intérieur du Soleil suggère qu’il est environ à mi-chemin de son cycle de vie. Il devient progressivement plus brillant : au début de son histoire, sa luminosité était inférieure de plus d'un tiers à celle actuelle et, dans plus de cinq milliards d'années, il quittera la séquence principale et deviendra plus grand, plus brillant, plus froid et plus rouge, formant une géante rouge. À ce moment, sa luminosité sera un millier de fois celle d’aujourd’hui et sa taille aura suffisamment augmenté pour engloutir Vénus et potentiellement la Terre.

Le Soleil est une étoile de population I, formée à partir de la matière éjectée lors de l'explosion de [supernovas](https://fr.wikipedia.org/wiki/Supernova), et possède ainsi une plus grande abondance d'éléments plus lourds que l'hydrogène et l'hélium (des « métaux ») que les étoiles de population II plus âgées. Ces éléments métalliques se sont formés dans les noyaux d'étoiles plus anciennes, des supernovas et ont ensuite été éjectés lors de leur explosion. Les étoiles les plus anciennes contiennent peu de métaux tandis que les étoiles ultérieures en contiennent ainsi plus. Cette haute [métallicité](https://fr.wikipedia.org/wiki/M%C3%A9tallicit%C3%A9) est probablement cruciale pour le développement d'un système planétaire par le Soleil, car les planètes se forment à partir de l'accrétion de ces métaux.

### Système solaire interne
---------------------------------------------------------------------------------------------------------------

Le Système solaire interne comprend traditionnellement la région située entre le Soleil et la ceinture principale d'astéroïdes. Composés principalement de silicates et de métaux, les objets du Système solaire interne orbitent près du Soleil : le rayon de la région tout entière est plus petit que la distance entre les orbites de Jupiter et de Saturne. Cette région se situe en totalité avant la ligne des glaces, qui se trouve à un peu moins de 5 ua (environ 700 millions de kilomètres) du Soleil.

Il n'existe pas d'objets notables attestés dont l'orbite serait totalement intérieure à celle de la planète Mercure, bien que l'existence d'astéroïdes vulcanoïdes soit supposée par certains astronomes. Au xixe siècle, l'existence d'une planète hypothétique est postulée dans cette zone, Vulcain, avant d'être invalidée.

Dans ce qui suit, le demi-grand axe de l'objet céleste évoqué est indiqué entre parenthèses en unités astronomiques au début de la section dédiée.

### Planètes internes
..........................................................................................................................................................................................

Les quatre planètes internes du Système solaire sont des planètes telluriques : elles possèdent une composition dense et rocheuse et une surface solide. Par ailleurs, elles ont peu ou pas de satellites naturels et aucun système d’anneaux. De taille modeste (la plus grande de ces planètes étant la Terre, dont le diamètre est de 12 756 km), elles sont composées en grande partie de minéraux à point de fusion élevé, tels les silicates qui forment leur croûte solide et leur manteau semi-liquide, et de métaux comme le fer et le nickel, qui composent leur noyau. Trois des quatre planètes (Vénus, la Terre et Mars) ont une atmosphère substantielle ; toutes présentent des cratères d’impact et des caractéristiques tectoniques de surface, comme des rifts et des volcans.

|![internes](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Telluric_planets_size_comparison.jpg)
|-----|
|Les planètes internes. De la plus grande à la plus petite : la Terre, Vénus, Mars et Mercure (dimensions à l’échelle).|

Le terme « planète interne » est distinct de « planète inférieure », qui désigne en général les planètes plus proches du Soleil que la Terre, soit Mercure et Vénus ; de même concernant « planète externe » et « planète supérieure ».

|![animation](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Ecliptic_plane_top_view.gif)
|-----|
|Animation des orbites des planètes internes pendant une année terrestre.|

=== "Mercure"

   Mercure (0,4 ua) est la planète la plus proche du Soleil, ainsi que la plus petite (4 878 km de diamètre) et la moins massive avec un peu plus du vingtième de la masse terrestre.

   Elle ne possède aucun satellite naturel et ses seules caractéristiques géologiques connues, en dehors des cratères d’impact, sont des dorsa qui ont probablement été produites par contraction thermique lors de sa solidification interne tôt dans son histoire. Elle possède relativement à sa taille un très grand noyau de fer liquide — qui représenterait 85 % de son rayon, contre environ 55 % pour la Terre — et un fin manteau, ce qui n'est pas expliqué de façon certaine mais pourrait être du à un impact géant ou à l'importante température lors de son accrétion.

   Mercure a la particularité d'être en résonance spin-orbite 3:2, sa période de révolution (~88 jours) valant exactement 1,5 fois sa période de rotation (~59 jours), et donc la moitié d'un jour solaire (~176 jours). Ainsi, relativement aux étoiles fixes, elle tourne sur son axe exactement trois fois toutes les deux révolutions autour du Soleil. Par ailleurs, son orbite possède une excentricité de 0,2, soit plus de douze fois supérieure à celle de la Terre et de loin la plus élevée pour une planète du Système solaire.

   L'atmosphère de Mercure, quasiment inexistante et qualifiable d'exosphère, est formée d’atomes arrachés à sa surface (oxygène, sodium et potassium) par le vent solaire ou momentanément capturés à ce vent (hydrogène et hélium). Cette absence implique qu'elle n'est pas protégée des météorites et donc sa surface est très fortement cratérisée et globalement similaire à la face cachée de la Lune, car elle est géologiquement inactive depuis des milliards d'années. De plus, le manque d'atmosphère combiné à la proximité du Soleil engendre d'importantes variations de la température en surface, allant de 90 K (−183 °C) au fond des cratères polaires — là où les rayons du Soleil ne parviennent jamais — jusqu'à 700 K (427 °C) au point subsolaire au périhélie.

   **Images de Mercure**

   |![mercure1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Mercury_in_color_-_Prockter07_centered.jpg)  |![mercure2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:EW1027346412Gnomap.png)|![mercure3](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:The_Mighty_Caloris_(PIA19213).png) |
   |-------------|-------------|-------------|
   |Image de Mercure prise par MESSENGER lors de son premier survol en 2008. |Surface vue par MESSENGER en 2013, montrant notamment le bassin Tolstoï.|Mosaïque du bassin Caloris, plus grand cratère d'impact de Mercure (MESSENGER, 2015).|

=== "Vénus"

   Vénus (0,7 ua) est la planète la plus proche de la Terre en taille (0,95 rayon terrestre) et en masse (0,815 masse terrestre), qui lui valent d'être parfois appelée sa « planète sœur ». Comme elle, Vénus possède un épais manteau de silicate entourant un noyau métallique, une atmosphère significative et une activité géologique interne. Cependant, elle est beaucoup plus sèche et la pression de son atmosphère au sol est 92 fois plus élevée. Son importante atmosphère, composée à plus de 96 % de dioxyde de carbone, crée un très grand effet de serre qui en fait la planète la plus chaude du Système solaire par sa température de surface moyenne de 735 K (462 °C).

   La planète est également enveloppée d'une couche opaque de nuages d'acide sulfurique, hautement réfléchissants pour la lumière visible, empêchant sa surface d'être vue depuis l'espace et faisant de la planète le deuxième objet naturel le plus brillant du ciel nocturne terrestre après la Lune. Bien que la présence d'océans d'eau liquide à sa surface par le passé soit supposée, la surface de Vénus est un paysage désertique sec et rocheux où se déroule toujours un volcanisme. Comme elle ne possède pas de champ magnétique, son atmosphère est constamment appauvrie par le vent solaire et ce sont des éruptions volcaniques qui lui permettent de la réalimenter. La topographie de Vénus présente peu de reliefs élevés et consiste essentiellement en de vastes plaines géologiquement très jeunes de quelques centaines de millions d'années, notamment grâce à son épaisse atmosphère la protégeant des impacts météoritiques et à son volcanisme renouvelant le sol.

   Vénus orbite autour du Soleil tous les 224,7 jours terrestres et, avec une période de rotation de 243 jours terrestres, il lui faut plus de temps pour tourner autour de son propre axe que toute autre planète du Système solaire. Comme Uranus, elle possède une rotation rétrograde et tourne sur elle-même dans le sens opposé à celui des autres planètes : le soleil s'y lève à l'ouest et se couche à l'est. Vénus possède l'orbite la plus circulaire des planètes du Système solaire, son excentricité orbitale étant presque nulle, et, du fait de sa lente rotation, est quasiment sphérique (aplatissement considéré comme nul). Elle ne possède pas de satellite naturel. En revanche, à l'instar de celui de la Terre, Vénus est accompagnée sur son orbite par un anneau, disque de poussières circumsolaire très peu dense.

   **Images de Vénus**

   |![venus1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Venus-real_color.jpg)  |![venus2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Venuspioneeruv.jpg)|![venus3](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:PIA00103_Venus_-_3-D_Perspective_View_of_Lavinia_Planitia.jpg) |
   |-------------|-------------|-------------|
   |Image de Vénus en couleurs réelles prise par Mariner 10 en 1974. |Photo ultraviolette de Vénus montrant des nuages, prise par Pioneer Venus Orbiter en 1979.| Simulation en 3D de cratères d'impact sur Vénus, figurant au premier plan le cratère Saskia.|

=== "Terre"

   La Terre (1 ua) est la plus grande (12 756 km de diamètre) et la plus massive des planètes telluriques ainsi que la plus dense du Système solaire. Elle est notamment le seul objet céleste connu pour abriter la vie. Elle orbite autour du Soleil en 365,256 jours solaires — une année sidérale — et réalise une rotation sur elle-même relativement au Soleil en 23 h 56 min 4 s — un jour sidéral — soit un peu moins que son jour solaire de 24 h du fait de ce déplacement autour du Soleil. L'axe de rotation de la Terre possède une inclinaison de 23°, ce qui cause l'apparition de saisons.

   La Terre possède un satellite en rotation synchrone autour d'elle, la Lune, le seul satellite significativement grand d'une planète tellurique dans le Système solaire. Selon l'hypothèse de l'impact géant, ce satellite s'est formé à la suite d'une collision de la proto-Terre avec un impacteur de la taille de la planète Mars (nommé Théia) peu après la formation de la planète il y a 4,54 milliards d'années. L'interaction gravitationnelle avec son satellite crée les marées, stabilise son axe de rotation et réduit graduellement sa vitesse de rotation. La planète évolue également dans un disque de poussière autour du Soleil.

   Son enveloppe rigide — appelée la lithosphère — est divisée en différentes plaques tectoniques qui migrent de quelques centimètres par an. Environ 71 % de la surface de la planète est couverte d'eau liquide — fait unique parmi les planètes telluriques, avec notamment des océans, mais aussi des lacs et rivières, constituant l'hydrosphère — et les 29 % restants sont des continents et des îles, tandis que la majeure partie des régions polaires est couverte de glace. La structure interne de la Terre est géologiquement active, le noyau interne solide et le noyau externe liquide (composés tous deux essentiellement de fer) permettant notamment de générer le champ magnétique terrestre par effet dynamo et la convection du manteau terrestre (composé de roches silicatées) étant la cause de la tectonique des plaques, activité qu'elle est la seule planète à connaître. L’atmosphère terrestre est radicalement différente de celle des autres planètes, car elle a été altérée par la présence de formes de vie jusqu'à contenir de nos jours 21 % d’oxygène. Celle-ci augmente également la température moyenne de 33 kelvins par effet de serre, la faisant atteindre 288 K (15 °C) et permettant l'existence d'eau liquide. 

   **Images du système Terre-Lune**

   |![terre1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Blue_Marble_Western_Hemisphere.jpg)  |![terre2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Blue_Marble_Eastern_Hemisphere.jpg)|![terre3](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:NASA-Apollo8-Dec24-Earthrise.jpg) |
   |-------------|-------------|-------------|
   |Image composite de l'hémisphère ouest terrestre réalisée à partir de données satellites par la NASA en 2007. |Image composite de l'hémisphère est terrestre réalisée à partir de données satellites par la NASA en 2007.|Lever de Terre, pris par William Anders en 1968 durant la mission Apollo 8 vers la Lune. |

=== "Mars"

   Mars (1,5 ua) est deux fois plus petite que la Terre et Vénus, et fait seulement environ le dixième de la masse terrestre. Sa période de révolution autour du Soleil est de 687 jours terrestres et sa journée dure 24 heures et 39 minutes. La période de rotation de Mars est du même ordre que celle de la Terre et son obliquité lui confère un cycle des saisons similaire au cycle terrestre. Ces saisons sont toutefois marquées par une excentricité orbitale cinq fois et demie plus élevée que celle de la Terre, d'où une asymétrie saisonnière sensiblement plus prononcée entre les deux hémisphères et un climat qui peut être qualifié d'hyper-continental : en été, la température dépasse rarement les 20 à 25 °C à l'équateur, alors qu'elle peut chuter jusqu'à −120 °C, voire moins pendant l'hiver aux pôles.

   Elle possède une atmosphère ténue, principalement composée de dioxyde de carbone, et une surface désertique caractérisée visuellement par sa couleur rouge, due à l'abondance d'hématite amorphe ou oxyde de fer(III). Sa topographie présente des analogies aussi bien avec la Lune, par ses cratères et ses bassins d'impact en raison de sa proximité avec la ceinture d'astéroïdes, qu'avec la Terre, par des formations d'origine tectonique et climatique telles que des volcans, des rifts, des vallées, des mesas, des champs de dunes et des calottes polaires. Le plus haut volcan du Système solaire, Olympus Mons (qui est un volcan bouclier), et le plus grand canyon, Valles Marineris, se trouvent sur Mars. Ces structures géologiques montrent des signes d’une activité géologique, voire hydraulique, qui a peut-être persisté jusqu’à récemment, mais qui est presque totalement arrêtée de nos jours ; seuls des événements mineurs surviendraient encore épisodiquement à sa surface, tels que des glissements de terrain ou de rares éruptions volcaniques sous forme de petites coulées de lave. La planète est par ailleurs dépourvue de champ magnétique global.

   Mars possède deux très petits satellites naturels de quelques dizaines de kilomètres de diamètre, Phobos et Déimos, qui pourraient être des astéroïdes capturés, mais le consensus actuel privilégie une formation à la suite d'un choc avec la planète en raison de leur faible éloignement à la planète. Ceux-ci sont en rotation synchrone — montrant donc toujours la même face à la planète — mais, du fait des forces de marée avec la planète, l'orbite de Phobos diminue et le satellite se décomposera lorsqu'il aura franchi la limite de Roche, tandis que Déimos s'éloigne progressivement.

   **Images du système martien**

   |![mars1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Mars_Valles_Marineris_EDIT.jpg)  |![mars2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Mars_atmosphere.jpg)|![mars3](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Olympus_Mons_alt.jpg) |
   |-------------|-------------|-------------|
   |Photo de Mars prise par Viking 1 en 1980 et centrée sur Valles Marineris. |Image par Viking 1 de l'Argyre Planitia révélant la fine atmosphère de la planète.|Image composite d'Olympus Mons, le plus haut sommet d'une planète du Système solaire(Viking 1, 1978). |

#### Comparaison ⚖️

|![comparatif](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Terrestrial_planet_sizes.jpg)
|-----|
|Photomontage comparatif des tailles des planètes telluriques du Système solaire (de gauche à droite) : Mercure, Vénus (images radar), la Terre et Mars.|

**Comparaison de caractéristiques physiques des planètes telluriques du Système solaire**

| Planète | Rayon équatorial | Masse | Gravité | Inclinaison de l'axe |
|---------|------------------|-------|---------|----------------------|
| Mercure | 2 439,7 km | 3,301 × 1023 kg | 3,70 m/s2 | 0,03° |
| Venus | 6 051,8 km | 4,867 5 × 1024 kg | 8,87 m/s2 | 177,36° |
| Terre | 6 378,137 km | 5,972 4 × 1024 kg | 9,780 m/s2 | 23,44° |
| Mars | 3 396,2 km | 6,441 71 × 1023 kg | 3,69 m/s2 | 25,19° |

!!! info "Ceinture d'astéroïdes"
    Les astéroïdes sont principalement de petits corps du Système       solaire composés de roches et de minéraux métalliques non volatils, de forme et de tailles irrégulières — allant de plusieurs centaines de kilomètres à des poussières microscopiques — mais beaucoup plus petits que les planètes. Une région située entre les orbites de Mars et Jupiter en contient un très grand nombre et est ainsi appelée ceinture d'astéroïdes, ou ceinture principale pour la distinguer des autres regroupements d'astéroïdes du Système solaire comme la ceinture de Kuiper ou le nuage de Oort.

    |![asteroides](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:InnerSolarSystem-fr.png)
    |-----|
    |Schéma de la ceinture d'astéroïdes et des astéroïdes troyens de Jupiter.|

    Elle contiendrait entre un et deux millions d'astéroïdes plus larges qu'un kilomètre, certains comportant des lunes parfois aussi larges qu'eux-mêmes, mais peu dépassent les 100 kilomètres de diamètre. La masse totale de la ceinture d'astéroïdes vaut environ 5 % de celle de la Lune et les astéroïdes sont relativement éloignés les uns des autres, impliquant que de nombreuses sondes spatiales aient pu la traverser sans incident.

### Système solaire externe
---------------------------------------------------------------------------------------------------------------

Au-delà de la ceinture d'astéroïdes s'étend une région dominée par les géantes gazeuses et leurs satellites naturels. De nombreuses comètes à courte période, y compris les centaures, y résident également. Si cette dénomination s'appliquait un temps jusqu'aux limites du Système solaire, les parties les plus extérieures du Système solaire situées après l'orbite de Neptune sont désormais considérées comme une région distincte constituée des objets transneptuniens depuis la découverte de la ceinture de Kuiper.

Les objets solides de cette région sont composés d'une plus grande proportion de « glaces » (eau, ammoniac, méthane) que leurs correspondants du Système solaire interne, notamment parce qu'elle se trouve en grande partie après la ligne des glaces et que les températures plus basses permettent à ces composés de rester solides.

### Planètes externes
..........................................................................................................................................................................................

|![externes](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Size_planets_comparison.jpg)
|-----|
|Les planètes externes (par taille décroissante) : Jupiter, Saturne, Uranus, Neptune comparées aux planètes internes : la Terre, Vénus, Mars et Mercure (à l’échelle).|

Les quatre planètes extérieures, ou planètes géantes, représentent collectivement 99 % de la masse connue pour orbiter autour du Soleil. Jupiter et Saturne représentent ensemble plus de 400 fois la masse terrestre et sont constituées en grande partie d'hydrogène et d'hélium, d'où leur désignation de géantes gazeuses ; ces compositions, assez proches de celle du Soleil quoique comprenant plus d'éléments lourds, impliquent qu'elles ont des densités faibles. Uranus et Neptune sont beaucoup moins massives — faisant environ 20 masses terrestres chacune — et sont principalement composées de glaces, justifiant qu'elles appartiennent à la catégorie distincte des géantes de glaces. Les quatre planètes géantes possèdent un système d'anneaux planétaires, bien que seul le système d'anneaux de Saturne soit facilement observable depuis la Terre. En outre, elles ont en moyenne plus de satellites naturels que les planètes telluriques, de 14 pour Neptune à 82 pour Saturne135. Si elles n'ont pas de surface solide, elles possèdent des noyaux de fer et de silicates allant de quelques à plusieurs dizaines de masses terrestres.

|![planetaire](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Solar_system_orrery_outer_planets.png)
|-----|
|Planétaire montrant les orbites des planètes externes. Les sphères représentent les positions des planètes tous les 100 jours du 21 janvier 2023 (périhélie de Jupiter) au 2 décembre 2034 (aphélie de Jupiter).|
    
Le terme « planète externe » n'est pas strictement synonyme de « planète supérieure » ; le second désigne en général les planètes en dehors de l'orbite terrestre et comprend donc à la fois toutes les planètes externes et Mars.

=== "Jupiter"

   Jupiter (5,2 ua), par ses 317 masses terrestres, est aussi massive que 2,5 fois toutes les autres planètes réunies et son diamètre avoisine les 143 000 kilomètres40,157. Sa période de révolution est d'environ 12 ans et sa période de rotation est d'un peu moins de 10 heures.

   Elle est composée essentiellement d'hydrogène et d'hélium, d'un peu d'ammoniac et de vapeur d'eau ainsi que probablement un noyau solide rocheux, mais n'a pas de surface définie. Sa forte chaleur interne anime des vents violents, de près de 600 km/h, qui parcourent les couches supérieures de l'atmosphère de la planète et la divisent visiblement en plusieurs bandes colorées à différentes latitudes, séparées par des turbulences. Ce phénomène crée également un certain nombre de caractéristiques semi-permanentes, comme la Grande Tache rouge, un anticyclone observé depuis au moins le xviie siècle. Sa puissante magnétosphère, animée par un courant électrique dans sa couche interne d'hydrogène métallique, crée un des plus forts champ magnétique connu du Système solaire — dépassé seulement par les taches solaires — et des aurores polaires aux pôles de la planète. Si la température au niveau des nuages est d'environ 120 K (−153 °C), elle augmente rapidement avec la pression vers le centre de la planète du fait de la compression gravitationnelle et atteindrait 6 000 K et une pression un million de fois plus élevée que celle sur Terre à 10 000 km de profondeur.

   Jupiter possède 79 satellites connus. Les quatre plus gros, aussi appelés satellites galiléens car découverts par l'astronome italien Galilée au xviie siècle, Ganymède, Callisto, Io et Europe, présentent des similarités géologiques avec les planètes telluriques. Parmi les plus grands objets du Système solaire — ils sont tous plus grands que les planètes naines —, Ganymède est même la plus grande et la plus massive lune du Système solaire, dépassant en taille la planète Mercure. Par ailleurs, les trois lunes intérieures, Io, Europe et Ganymède, sont le seul exemple connu de résonance de Laplace du Système solaire : les trois corps sont en résonance orbitale 4:2:1, ce qui a un impact sur leur géologie et par exemple le volcanisme sur Io.

   Le système jovien comprend également les anneaux de Jupiter, mais l'influence de la planète s'étend à de nombreux objets du Système solaire, comme les astéroïdes troyens de Jupiter.

   **Images du système jovien**

   |![jupiter1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Jupiter_and_its_shrunken_Great_Red_Spot.jpg)  |![jupiter2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:790106-0203_Voyager_58M_to_31M_reduced.gif)|![jupiter3](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:PIA21641-Jupiter-SouthernStorms-JunoCam-20170525.jpg) |
   |-------------|-------------|-------------|
   |Jupiter vue par le télescope spatial Hubble en 2014 et présentant la Grande Tache rouge. |Animation du survol de Voyager 1 de janvier à février 1979.|Tempêtes du pôle Sud de Jupiter prises par Juno en 2017. |

   

=== "Saturne"

   Saturne (9,5 ua) possède des caractéristiques similaires à Jupiter, telles que sa composition atmosphérique et sa puissante magnétosphère. Bien qu'elle fasse 60 % du volume de l'autre planète géante gazeuse du fait de son diamètre équatorial d'environ 121 000 kilomètres, elle est beaucoup moins massive avec 95 masses terrestres. Sa période de révolution vaut un peu moins de 30 années tandis que sa période de rotation est estimée à 10 h 33 min.

   La caractéristique la plus célèbre de la planète est son système d'anneaux proéminent. Composés principalement de particules de glace et de poussières, et divisés en sections espacées de divisions, ils se seraient formés il y a moins de 100 millions d'années. De plus, elle est la planète possédant le plus grand nombre de satellites naturels, 82 étant confirmés et des centaines de satellites mineurs garnissant son cortège. Sa plus grande lune, Titan, est également la deuxième plus grande du Système solaire et est la seule lune connue à posséder une atmosphère substantielle. Une autre lune remarquable, Encelade, émet de puissants geysers de glace du fait de son cryovolcanisme et serait un habitat potentiel pour la vie microbienne.

   Seule planète du Système solaire moins dense que l'eau, l'intérieur de Saturne est très probablement composé d'un noyau rocheux de silicates et de fer entouré de couches constituées en volume à 96 % d'hydrogène qui est successivement métallique puis liquide puis gazeux, mêlé à de l'hélium. Un courant électrique dans la couche d'hydrogène métallique donne naissance à sa magnétosphère, la deuxième plus grande du Système solaire, mais beaucoup plus petite que celle de Jupiter, et à des aurores polaires. L'atmosphère de Saturne est généralement terne et manque de contraste, bien que des caractéristiques de longue durée puissent apparaître tel l'hexagone à son pôle nord. Les vents sur Saturne peuvent atteindre une vitesse de 1 800 km/h, soit les deuxièmes plus rapides du Système solaire après ceux de Neptune.

   **Images du système saturnien**

   |![saturne1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Saturn_with_auroras_(cropped_1in1).jpg)  |![saturne2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Saturn_Storm.jpg)|![saturne3](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:PIA20513_-_Basking_in_Light.jpg) |
   |-------------|-------------|-------------|
   |Vue d'aurores boréales au pôle Sud de Saturne par le télescope spatial Hubble en 2004.| Une grande tache blanche présente sur Saturne en 2010 et 2011, vue par Cassini.| L'hexagone de Saturne à son pôle Nord, pris par Cassini en 2016. |

=== "Uranus"

   Uranus (19,2 ua) est la moins massive des planètes géantes, par ses 14 masses terrestres. Son diamètre d'environ 51 000 kilomètres est légèrement supérieur à celui de sa presque jumelle Neptune, en raison de la compression gravitationnelle de cette dernière. Sa période de révolution est d'environ 84 ans et, caractéristique unique parmi les planètes du Système solaire, elle orbite le Soleil sur son côté en un peu plus de 17 heures, son axe de rotation étant pratiquement dans son plan de révolution, donnant l'impression qu'elle « roule » sur le plan de l'écliptique. Ses pôles Nord et Sud se trouvent donc là où la plupart des autres planètes ont leur équateur. La planète est pourvue d'une magnétosphère vrillée du fait de cette inclinaison de l'axe.

   Comme celles de Jupiter et Saturne, l'atmosphère d'Uranus est composée principalement d'hydrogène et d'hélium et de traces d'hydrocarbures. Cependant, comme Neptune, elle contient une proportion plus élevée de « glaces » au sens physique, c'est-à-dire de substances volatiles telles que l'eau, l'ammoniac et le méthane, tandis que l'intérieur de la planète est principalement composé de glaces et de roches, d'où leur nom de « géantes de glaces ». Par ailleurs, le méthane est le principal responsable de la teinte aigue-marine de la planète. Son atmosphère planétaire est la plus froide du Système solaire, atteignant 49 K (−224 °C) à la tropopause, car elle rayonne très peu de chaleur dans l'espace, et présente une structure nuageuse en couches. Cependant, la planète ne présente presque aucun relief à la lumière visible, comme les bandes de nuages ou les tempêtes associées aux autres planètes géantes, malgré des vents de l'ordre de 900 km/h.

   Première planète découverte à l’époque moderne avec un télescope — par William Herschel en 1781 — et non connue depuis l'Antiquité, Uranus possède un système d’anneaux et de nombreux satellites naturels : on lui connaît 13 anneaux étroits et 27 lunes, les plus grandes étant Titania, Obéron, Umbriel, Ariel et Miranda ; cette dernière est notamment remarquable en raison de la grande varitété de terrains qu'elle présente.

   **Images du système uranien**

   |![uranus1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Uranus2.jpg)  |![uranus2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Adding_to_Uranus's_legacy.tif)|![uranus3](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Uranusandrings.jpg) |
   |-------------|-------------|-------------|
   |Première image d'Uranus vue par Voyager 2 en janvier 1986.|Atmosphère d'Uranus prise par Hubble et le programme OPAL en 2019.| Image d'Hubble dans l'infrarouge proche montrant des bandes atmosphériques, des anneaux et des lunes d'Uranus en 1998. |

=== "Neptune"

   Neptune (30 ua) est la planète la plus éloignée du Soleil connue au sein du Système solaire. Légèrement plus massive qu'Uranus par ses 17 masses terrestres, mais plus petite, son diamètre équatorial étant d'environ 49 500 kilomètres par compression gravitationnelle, elle est en conséquence plus dense — faisant d'elle la planète géante la plus dense. Sa période de révolution est d'environ 165 ans et sa période de rotation dépasse légèrement 16 heures.

   N'étant pas visible à l'œil nu, elle est le premier objet céleste et la seule des huit planètes du Système solaire à avoir été découverte par déduction plutôt que par observation empirique, grâce aux perturbations gravitationnelles inexpliquées sur l'orbite d'Uranus : les calculs de l'astronome français Urbain Le Verrier permettent au prussien Johann Gottfried Galle de l'observer au télescope en 1846. On lui connaît 14 satellites naturels dont le plus grand est Triton, qui est géologiquement actif et présente des geysers d'azote liquide. Il s'agit par ailleurs du seul grand satellite du Système solaire situé sur une orbite rétrograde. La planète possède en outre un système d'anneaux faible et fragmenté et une magnétosphère, et est accompagnée sur son orbite de plusieurs planètes mineures, les astéroïdes troyens de Neptune.

   L'atmosphère de Neptune est similaire à celle d'Uranus, composée principalement d'hydrogène et d'hélium, de traces d'hydrocarbures ainsi que d'une proportion plus élevée de « glaces » (eau, ammoniac et méthane), faisant d'elle la deuxième « géante de glaces ». Par ailleurs, le méthane est partiellement responsable de la teinte bleue de la planète, mais l'origine exacte de son bleu azur reste encore inexpliquée. Contrairement à l'atmosphère brumeuse et relativement sans relief d'Uranus, celle de Neptune présente des conditions météorologiques actives et visibles, notamment une Grande Tache sombre comparable à la Grande Tache rouge de Jupiter, présente au moment du survol de Voyager 2 en 1989. Ces conditions météorologiques sont entraînées par les vents les plus forts connus du Système solaire, qui atteignent des vitesses de 2 100 km/h. En raison de sa grande distance du Soleil, l'extérieur de son atmosphère est l'un des endroits les plus froids du Système solaire, les températures au sommet des nuages approchant 55 K (−218,15 °C).

   **Images du système neptunien**

   |![neptune1](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Neptune.jpg)  |![neptune2](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Neptune_storms.jpg)|![neptune3](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Neptune-Methane_(cropped).jpg) |
   |-------------|-------------|-------------|
   |Première image de Neptune vue par Voyager 2 en août 1989.|Image de Voyager 2 révélant les diverses tempêtes présentes en 1989, dont la Grande Tache sombre.|Image d'Hubble en 2005 dans le proche infrarouge révélant une bande de méthane ainsi que ses lunes Protée, Larissa, Galatée et Despina.  |


#### Comparaison ⚖️

|![comparatifg](https://fr.wikipedia.org/wiki/Syst%C3%A8me_solaire#/media/Fichier:Gas_planet_size_comparisons.jpg)
|-----|
|Photomontage comparatif des tailles des planètes géantes du Système solaire (de gauche à droite) : Jupiter, Saturne, Uranus et Neptune.|

**Comparaison de caractéristiques physiques des planètes géantes du Système solaire**

| Planète | Rayon équatorial | Masse | Gravité | Inclinaison de l'axe |
|---------|------------------|-------|---------|----------------------|
| Jupiter | 71 492 km | 1 898,19 × 1024 kg | 23,12 m/s2 | 3,13° |
| Saturne | 60 268 km | 568,34 × 1024 kg | 8,96 m/s2 | 26,73° |
| Uranus | 25 559 km | 86,813 × 1024 kg | 8,69 m/s2 | 97,77° |
| Neptune | 24 764 km | 102,413 × 1024 kg | 11,00 m/s2 | 28,32° |


!!! info "Vidéo Explicative🎞️"
   [Lien](https://youtu.be/I7cajVnzm8k){ .md-button }
