# Les raccourcis

Petit bonus où je vous donne des petits raccourcis clavier sympa pour Windows et sur Linux

=== "Les raccourcis clavier pour Windows" 

    1. Pour tout selectionner : ++ctrl+a++

    1. Pour copier : ++ctrl+c++

    1. Pour coller : ++ctrl+v++
    
    1. Pour changer de fenetre sur un navigateur web : ++ctrl+tab++

    1. Pour changer de logiciel : ++alt+tab++

=== "Les raccourcis clavier pour Linux"

    Dans un terminal, ++ctrl+a++ déplace le curseur en début de ligne.

    Dans un terminal, ++ctrl+e++ déplace le curseur en fin de ligne.

    Dans un terminal, ++ctrl+shift+v++ colle le contenu du presse papier.