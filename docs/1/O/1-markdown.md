![image_informatique](https://www.ionos.fr/digitalguide/fileadmin/DigitalGuide/Teaser/ascii-code-t.jpg)

# Markdown : le guide du langage de balisage léger

Quand nous lisons des textes, que ce soit sur Internet, dans un magazine ou dans n’importe quel livre, nous nous attendons à une certaine mise en page. On utilise par exemple le gras pour mettre en avant des mots importants, on distingue au premier regard le titre d’un document et on utilise des présentations en forme de listes pour structurer certaines parties du document. Un tel formatage nous paraît évident, et derrière notre clavier d’ordinateur, lorsque nous rédigeons nos textes, tout cela ne nous pose aucun problème : nous changeons la taille des caractères, nous insérons des tirets et mettons certains mots et expressions en gras. N’importe quel traitement de texte offre à ses utilisateurs de nombreuses possibilités pour la mise en page des textes qu’ils éditent.

Pourtant tout n’est pas si simple. De votre côté, vous vous contentez de sélectionner le texte à formater, et votre logiciel lui donne les attributs que vous lui demandez. Avec les logiciels Word, vous ne voyez pas le véritable texte source, accompagné de ses balises. Et c’est tant mieux, à vrai dire : un tel texte est presque illisible pour un être humain.

N’importe quel éditeur de texte permet d’écrire du HTML ou du LaTeX, mais le résultat est difficilement déchiffrable pour le commun des mortels. Et c’est exactement ce que le langage de balisage Markdown souhaite changer. Il veut tirer profit du meilleur des deux univers, et être compris à la fois des machines et des hommes. Pour mettre en forme le texte, Markdown utilise des éléments intuitifs. Dans ce cas, même le texte accompagné de ses balises sera assez facile à comprendre pour les hommes.


## A quoi sert Markdown ?

Tout comme HTML ou LaTeX, Markdown est un **langage de balisage**. Contrairement aux deux premiers langages, Markdown se veut être facile à lire par les personnes. Les balises ne doivent pas être abstraites, mais proches de leur véritable signification. Le plus simple est d’illustrer notre propos par un exemple : pour mettre en gras un mot avec HTML, on utilise soit le ```'<b>'- soit la balise '<strong>'```.


```<p>Ce<b>Mot</b> en gras et celui-l&#xE0; <strong>aussi</strong>.</p>```

Si vous écrivez un docuement dans LaTeX, utilisez (en mode texte continu) l’élément \textbf.

```Ce \textbf{Mot} en gras.```
Ces deux balises se lisent certes assez facilement, mais sont plus difficiles à écrire, particulièrement dans le cadre de la rédaction de longs textes. Markdown simplifie ce processus en mettant tout simplement les éléments gras entre des étoiles.
```Ce **mot** en gras.```

Cette variante est d’une part plus lisible, car l’étoile indique facilement la mise en gras, et vous aurez par ailleurs plus vite fait de taper quatre étoiles que des balises compliquées. Avant la conversion (c’est à dire en Texte brut), un lecteur comprendra ce qu’a voulu dire le rédacteur, même s’il ne maîtrise pas la syntaxe Markdown.

Markdown présente donc avant tout un intérêt pour tous ceux qui n’ont pas de compétence particulière en informatique, ni en Webdesign, mais qui sont pourtant amenés à rédiger régulièrement des textes pour Internet. C’est le cas notamment des blogueurs qui utilisent un CMS (Content Management System). Il arrive même à des spécialistes et à des technophiles de recourir au Markdown pour rédiger des textes simples. Certains programmeurs utilisent le langage Markdown pour rédiger par exemple des documents d’accompagnement (ex. les fichiers Readme) sans même les convertir au préalable. En termes de lisibilité, le résultat sera sensiblement le même, que l’utilisateur ouvre le texte dans un lecteur Markdown ou qu’il le lise à l’état brut.

Pour les CMS les plus courants comme WordPress et Joomla, il existe des plugins permettant à ces logiciels de comprendre le Markdown. Un bon nombre de wikis, de forums (Reddit par exemple), ainsi que le générateur de site Jekyll sont capables d’interpréter ce langage de balisage simplifié.

!!! info "Remarque"
    Les langages de balisage ne sont pas des langages de programmation. Ils ont uniquement pour fonction de structurer la mise en page de votre texte. Les langages de programmation renferment quant à eux toutes sortes de boucles et de variables, et représentent le pilier même de la programmation des logiciels.


Markdown ne cherche pas à se substituer à HTML. Les fonctions qu’il propose sont assez réduites. Pour les concepteurs, le langage Markdown est plutôt un outil complémentaire. Il est d’ailleurs possible d’insérer des éléments HTML dans un document Markdown, ce qui élargit considérablement le spectre de ce langage, plutôt simple à la base. L’objectif du langage Markdown est avant tout de vous simplifier les tâches d’écriture (en particulier sur Internet). Une fois que les documents Markdown sont transformés par l’analyseur syntaxique (Parser en anglais), il en résulte des fichiers HTML susceptibles d’être utilisés avec les différents navigateurs.


!!! note "Note"
    Le nom « Markdown » est en fait un jeu de mots. Le terme anglais désignant les langages de balisage est « Markup Languages ». Le nom Markdown signifie clairement qu’on a affaire à un langage simplifié et léger, d’où l’appellation : Markdown.

## Tutoriel Markdown : la bonne syntaxe pour vos documents

Markdown étant conçu pour être un langage de balisage simple, la syntaxe Markdown est très intuitive. Pour pouvoir l’utiliser, vous devez cependant connaître les éléments de balisage. Nous avons réuni ci-dessous les principales fonctions.


### Gras et italique

Écrire en gras et en italique est particulièrement facile avec Markdown. Il suffit d’utiliser les étoiles, appelées aussi astérisques. Pour écrire en italique, insérez tout simplement une étoile devant et derrière l’expression le mot ou concerné. Pour le gras, insérez deux étoiles avant et après. Pour le gras et l’italique, vous devrez opter pour trois étoiles. Une autre option consiste à utiliser les tirets bas.

```
*Texte en italique*
_Texte en italique_
**Texte en gras**
__Texte en gras__
***Texte en italique et en gras***
___Texte en italique et en gras___

```

### Barré

Pour barrer un texte avec Markdown, précédez-le de deux tildes et refermez la chaîne avec deux autres tildes.

~~Ce texte est barré.~~``` mais pas celui-là.```


!!! info "Remarque"

    Markdown ne permet pas de souligner du texte. En HTML, on utilise la balise ```'<u>'``` pour souligner. Cette forme de typographie est cependant progressivement délaissée. Les hyperliens du net étant généralement représentés sous forme de texte souligné, on a tendance à ne pas souligner d’autres segments pour éviter toute confusion.


### Les titres

Par défaut, pour rédiger un titre avec Markdown, on utilise le dièse. On le sépare du texte avec une espace. Pour créer des sous-titres de hiérarchie inférieure, et donc rédigés en plus petits, il suffit d’insérer des dièses supplémentaires. Comme pour l’édition HTML, vous pourrez créer jusque 6 niveaux de sous-titres.


```
#  Titre 1
## Titre 2
###  Titre 3
#### Titre 4
#####  Titre 5
###### Titre 6
```

!!! note "Note"

    Certains ajoutent des dièses derrière les titres. Cela peut améliorer la lisibilité, bien que ce soit absolument superflu sur le plan technique. Au moment de la conversion, ces dièses seront de toute manière ignorés.


Une autre option consiste à utiliser le signe égal et le tiret pour délimiter les titres. Il suffit dans ce cas de les insérer en dessous de la ligne du titre. Cette variante ne permet cependant de créer qu’un titre et un seul niveau de sous-titre. Un seul caractère par titre suffit, bien qu’on ait tendance à en aligner plusieurs les uns derrière les autres. La raison est purement esthétique. Plusieurs caractères successifs donnent l’impression d’un soulignement simple ou double.

```
Titre 1
=
Titre 2
-
```

### Paragraphes

Le langage Markdown utilise des sauts de lignes pour créer des paragraphes séparés. Pour rédiger un nouveau paragraphe (balise

), il suffit d’insérer tout simplement une ligne vierge. Attention cependant : Markdown considère comme ligne vierge toute ligne vide sur le plan purement visuel. Si cette ligne comprend des espaces invisibles, comme des tabulations ou des espaces, l’analyseur syntaxique les ignorera et interprétera toute la ligne comme étant vierge. Si l’on souhaite insérer un saut de ligne correspondant à la balise
, il vous faudra insérer deux espaces à la fin d’une ligne.

### Les citations

Pour transformer une zone de texte en citation, il est possible de créer un élément de type Blockquote avec Markdown. Pour ce faire, vous utiliserez le signe supérieur à (>). Vous pourrez soit précéder chaque ligne de ce signe, soit en insérer un au début du paragraphe et terminer le paragraphe à mettre en exergue par une ligne vierge. D’autres éléments de formatage sont possibles dans un blockquote.

```
>Ceci est une **zone en retrait**.
>La zone continue ici

>Ceci est une autre **zone de retrait**.
Cette zone continue également dans la ligne suivante.
Cependant, cette ligne n’est plus en retrait

```
### Les listes

Si vous souhaitez établir une liste simple avec Markdown, vous avez le choix entre le signe plus, le tiret ou un astérisque. Ces trois options donnent le même rendu.
```
- Liste1
- Liste 2
- Liste 3
```
Pour créer une liste numérotée, il vous suffira d’inscrire un chiffre suivi d’un point.
```
1. Liste 1
2. Liste 2
3. Liste 3
```

!!! tip "Conseil"

    Le chiffre employé n’a aucune importance pour Markdown. Que vous écriviez trois fois le chiffre 1, ou que vous commenciez par le chiffre 3, le langage Markdown rédigera dans tous les cas une liste correctement numérotée.

Markdown permet aussi d’éditer des listes à cocher. Ces listes sont précédées d’une case à cocher pouvant être activée par clic. Ces cases peuvent d’ailleurs être cochées par défaut au moment de la création de la liste. Pour ce faire, vous devez utiliser les crochets et le X.
```
[ ] A
[x] B
[ ] C
```

!!! note "Note"

    Pour des cases à cocher vides, n’oubliez pas de laisser une espace entre les deux crochets. Sinon Markdown n’identifiera pas votre texte comme étant une liste.
    
### Code

Pour écrire un morceau de code dans un texte, Markdown l’identifie au moyen du caractère appelé le Backtick ou apostrophe inversée. Attention, à ne pas confondre avec les guillemets. Le marquage est donc constitué d’une apostrophe inversée au début et à la fin du segment correspondant au code. C’est ainsi que vous pourrez incorporer directement le code source ou une commande logicielle dans le texte.


C’est le `code`.


!!! warning "Note"

    Attention cependant à ne pas écrire par mégarde un accent grave, par exemple : à. C’est ce qui risque d’arriver si vous insérez ce caractère devant une voyelle. Pour éviter ce problème, pensez à insérer une espace entre l’apostrophe inversée et la voyelle.


Si votre codage renferme déjà l’apostrophe inversée, vous devez précéder la zone de code de deux fois ce caractère. Dans ce cas, Markdown n’interprétera pas l’apostrophe inversée comme une balise.


