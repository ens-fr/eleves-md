# Les entités de Minecraft

Minecraft comporte des entités, cela peut être des mobs[^mobs], des objets comme des wagons ![Wagon](images/blocs/wagon.webp "Wagon"){ width=30 } ou des portes-armures ![Porte-armure](images/blocs/porte-armure.webp "Porte-armure"){ width=30 } ou encore des tableaux ![Tableau](images/blocs/tableau.webp "Tableau"){ width=30 }. Mais ce n'est pas tout, tous les objets qui sont laché par terre le deviennent et même les joueurs sont des entités.
[^mobs]: Monstres/Créatures
## Les créatures de Minecraft

!!! info "Créatures pouvant être placé via un oeuf d'apparition ou déjà apparu sur le monde :"
    | Mobs hostiles | Images | Mobs passifs | Images | Mobs neutres | Images |
    |---------------|:----------------:|---------------|:---------------:|----------------|:---------------:|
    | Zombie | ![ Zombie ](images/Creatures/Hostiles/zombie.webp "Zombie"){ width=60 } | Cheval | ![ Cheval ](images/Creatures/Passifs/cheval.webp "Cheval"){ width=60 } | Abeille | ![ Abeille ](images/Creatures/Neutres/abeille.webp "Abeille"){ width=60 } |
    | Zombie villageois | ![ Zombie villageois ](images/Creatures/Hostiles/zombie_villageois.webp "Zombie villageois"){ width=60 } | Mule | ![ Mule ](images/Creatures/Passifs/mule.webp "Mule"){ width=60 } | Chèvre | ![ Chèvre ](images/Creatures/Neutres/chèvre.webp "Chèvre"){ width=60 } |
    | Zombie noyé | ![ Zombie noyé ](images/Creatures/Hostiles/zombie_noyé.webp "Zombie noyé"){ width=60 } | Âne | ![ Âne ](images/Creatures/Passifs/ane.webp "Âne"){ width=60 } | Dauphin | ![ Dauphin ](images/Creatures/Neutres/dauphin.webp "Dauphin"){ width=60 } |
    | Zombie momifié | ![ Zombie momifié ](images/Creatures/Hostiles/zombie_momifié.webp "Zombie momifié"){ width=60 } | Cheval-squelette | ![ Cheval-Squelette ](images/Creatures/Passifs/cheval-squelette.webp "Cheval-Squelette"){ width=60 } | Golem de fer | ![ Golem de fer ](images/Creatures/Neutres/face_golem_de_fer.webp "Golem de fer"){ width=60 } |
    | Zoglin | ![ Zoglin ](images/Creatures/Hostiles/zoglin.webp "Zoglin"){ width=60 } | Vache | ![ Vache ](images/Creatures/Passifs/vache.webp "Vache"){ width=60 } | Lama | ![ Lama ](images/Creatures/Neutres/lama.webp "Lama"){ width=60 } |
    | Wither squelette | ![ Wither squelette ](images/Creatures/Hostiles/wither_squelette.webp "Wither squelette"){ width=60 } | Cochon | ![ Cochon ](images/Creatures/Passifs/cochon.webp "Cochon"){ width=60 } | Lama de Marchand | ![ Lama de Marchand ](images/Creatures/Neutres/lama_marchand.webp "Lama de Marchand"){ width=60 } |
    | Wither squelette araignée | ![ Wither squelette araignée ](images/Creatures/Hostiles/wither_squelette_araignée.webp "Wither squelette araignée"){ width=60 } | Poule | ![ Poule ](images/Creatures/Passifs/poule.webp "Poule"){ width=60 } | Loup | ![ Loup ](images/Creatures/Neutres/loup.webp "Loup"){ width=60 } |
    | Vindicateur | ![ Vindicateur ](images/Creatures/Hostiles/vindicateur.webp "Vindicateur"){ width=60 } |Mouton | ![ Mouton ](images/Creatures/Passifs/mouton.webp "Mouton"){ width=60 } | Ours blanc | ![ Ours blanc ](images/Creatures/Neutres/ours_blanc.webp "Ours blanc"){ width=60 } |
    | Vex | ![ Vex ](images/Creatures/Hostiles/vex.webp "Vex"){ width=60 } | Lapin | ![ Lapin ](images/Creatures/Passifs/lapin.webp "Lapin"){ width=60 } | Panda | ![ Panda ](images/Creatures/Neutres/panda.webp "Panda"){ width=60 } |
    | Vagabond | ![ Vagabond ](images/Creatures/Hostiles/vagabond.webp "Vagabond"){ width=60 } | Chat | ![ Chat ](images/Creatures/Passifs/chat.webp "Chat"){ width=60 } | Piglin | ![ Piglin ](images/Creatures/Neutres/piglin.webp "Piglin"){ width=60 } |
    | Squelette | ![ Squelette ](images/Creatures/Hostiles/squelette.webp "Squelette"){ width=60 } | Chauve-souris | ![ Chauve-souris ](images/Creatures/Passifs/chauve-souris.webp "Chauve-souris"){ width=60 } | Araignée | ![ Araignée ](images/Creatures/Neutres/araignée.webp "Araignée"){ width=60 } |
    | Sorcière | ![ Sorcière ](images/Creatures/Hostiles/sorcière.webp "Sorcière"){ width=60 } | Champimeuh | ![ Champimeuh ](images/Creatures/Passifs/champimeuh.webp "Champimeuh"){ width=40 } ![ Champimeuh_marron ](images/Creatures/Passifs/champimeuh_marron.webp "Champimeuh_marron"){ width=40 } | Araignée venimeuse | ![ Araignée venimeuse ](images/Creatures/Neutres/araignée_venimeuse.webp "Araignée venimeuse"){ width=60 } |
    | Slime | ![ Slime ](images/Creatures/Hostiles/slime.webp "Slime"){ width=60 } | Perroquet | ![ Perroquet ](images/Creatures/Passifs/perroquet.webp "Perroquet"){ width=60 } | Piglin zombifié | ![ Piglin zombifié ](images/Creatures/Neutres/piglin_zombifié.webp "Piglin zombifié"){ width=60 } |
    | Shulker | ![ Shulker ](images/Creatures/Hostiles/shulker.webp "Shulker"){ width=60 } | Renard | ![ Renard ](images/Creatures/Passifs/renard.webp "Renard"){ width=40 } ![ Renard_neige ](images/Creatures/Passifs/renard_neige.webp "Renard_neige"){ width=40 } | Enderman | ![ Enderman ](images/Creatures/Neutres/enderman.webp "Enderman"){ width=60 } |
    | Ravageur | ![ Ravageur ](images/Creatures/Hostiles/ravageur.webp "Ravageur"){ width=60 } | Ocelot | ![ Ocelot ](images/Creatures/Passifs/ocelot.webp "Ocelot"){ width=60 } | Poisson-globe | ![ Poisson-globe ](images/Creatures/Neutres/poisson-globe.webp "Poisson-globe"){ width=60 } |
    | Poulet chevauché | ![ Poulet chevauché ](images/Creatures/Hostiles/poulet_chevauché.webp "Poulet chevauché"){ width=60 } | Golem de neige | ![ Golem de neige ](images/Creatures/Passifs/golem_de_neige.webp "Golem de neige"){ width=60 } |
    | Poisson argent | ![ Poisson argent ](images/Creatures/Hostiles/poisson_argent.webp "Poisson argent"){ width=60 } | Arpenteur | ![ Arpenteur ](images/Creatures/Passifs/arpenteur.webp "Arpenteur"){ width=60 } |
    | Pillard | ![ Pillard ](images/Creatures/Hostiles/pillard.webp "Pillard"){ width=60 } | Marchand ambulant | ![ Marchand ambulant ](images/Creatures/Passifs/marchand_ambulant.webp "Marchand_ambulant"){ width=60 } |
    | Piglin barbare | ![ Piglin barbare ](images/Creatures/Hostiles/piglin_barbare.webp "Piglin barbare"){ width=60 } | Villageois | ![ Villageois ](images/Creatures/Passifs/face_villageois.webp "Villageois"){ width=60 } |
    | Phantom | ![ Phantom ](images/Creatures/Hostiles/phantom.webp "Phantom"){ width=60 } | axolotl | ![ Axolotl ](images/Creatures/Passifs/axolotl.webp "Axolotl "){ width=60 } |
    | Magma cube | ![ Magma cube ](images/Creatures/Hostiles/magma_cube.webp "Magma cube"){ width=60 } | Poulpe | ![ Poulpe ](images/Creatures/Passifs/poulpe.webp "Poulpe"){ width=60 } |
    | Hoglin | ![ Hoglin ](images/Creatures/Hostiles/hoglin.webp "Hoglin"){ width=60 } | Poulpe luisant | ![ Poulpe luisant ](images/Creatures/Passifs/poulpe_luisant.webp "Poulpe luisant"){ width=60 } | 
    | Grand Gardien | ![ Grand Gardien ](images/Creatures/Hostiles/grand_gardien.webp "Grand Gardien"){ width=60 } | Saumon | ![ Saumon ](images/Creatures/Passifs/saumon.webp "Saumon"){ width=60 } |
    | Ghast | ![ Ghast ](images/Creatures/Hostiles/ghast.webp "Ghast"){ width=60 } | Morue | ![ Morue ](images/Creatures/Passifs/morue.webp "Morue"){ width=60 } |
    | Gardien | ![ Gardien ](images/Creatures/Hostiles/gardien.webp "Gardien"){ width=60 } | Tortue | ![ Tortue ](images/Creatures/Passifs/tortue.webp "Tortue"){ width=60 } |
    | Évocateur | ![ Évocateur ](images/Creatures/Hostiles/évocateur.webp "Évocateur"){ width=60 } | Poisson tropical | ![ Poisson tropical ](images/Creatures/Passifs/poisson_tropical.webp "Poisson tropical"){ width=60 } |
    | Endermite | ![ Endermite ](images/Creatures/Hostiles/endermite.webp "Endermite"){ width=60 } | Poisson-globe | 
    | Creeper | ![ Creeper ](images/Creatures/Hostiles/creeper.webp "Creeper"){ width=60 } |
    | Cavalier squelette | ![ Cavalier squelette ](images/Creatures/Hostiles/cavalier_squelette.webp "Cavalier squelette"){ width=60 } |
    | Blaze | ![ Blaze ](images/Creatures/Hostiles/blaze.webp "Blaze"){ width=60 } |
    | Araignée chevauchée | ![ Araignée chevauchée ](images/Creatures/Hostiles/araignée_chevauchée.webp "Araignée chevauchée"){ width=60 } |
    


