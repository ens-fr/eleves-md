# album

_____________________________________________________________________________________________________________________________________________

![album](https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Marriage_and_coronation_of_King_Babar_and_Queen_Celeste.jpg/800px-Marriage_and_coronation_of_King_Babar_and_Queen_Celeste.jpg)

un **album**est un livre contenant avant tout des images ou des icônes.

C'est un support d'expression artistique à part entière « sur lequel s'inscrivent, en interaction, des images et du texte » au sein d'un support1.

La plupart des albums publiés appartiennent le plus souvent au domaine de la littérature d'enfance et de jeunesse).

Géographiquement, il est notamment caractérisé par des zones montagneuses ou désertiques et est l'un des seuls pays — avec l'Espagne et la France — à comporter des rives sur la mer Méditerranée d'un côté et l'océan Atlantique de l'autre. Sa population est de près de 34 millions d'habitants (recensement de 2014) et sa superficie de 446 550 km222 (47,51 hab./km2), ou de 710 850 km2 en incluant [Sahara occidental](https://fr.wikipedia.org/wiki/Sahara_occidental).

## Étymologie

Le terme tire ses origines de l'expression latine utilisée dans l'antiquité « espace recouvert de plâtre où l'on inscrivait ce que l'on voulait porter à la connaissance du public » et, à l'époque romantique, de _l'album_ amicorum : _« petit cahier blanc des voyageurs destiné à recevoir des autographes ou des sentences »_
## Histoire

### **En France**
1.En France, les premières collections d'albums pour la jeunesse apparaissent à la fin du XIXe siècle (collection Trim chez [Hachette](https://fr.wikipedia.org/wiki/Litt%C3%A9rature_d%27enfance_et_de_jeunesse) et collection des Albums [Stahl](https://fr.wikipedia.org/wiki/Pierre-Jules_Hetzel) chez [Hetzel](https://fr.wikipedia.org/wiki/Pierre-Jules_Hetzel)).

[Maurice Boutet de Monvel](https://fr.wikipedia.org/wiki/Louis-Maurice_Boutet_de_Monvel), [Nathalie Parain](https://fr.wikipedia.org/wiki/Nathalie_Parain), [Jean de Brunhoff](https://fr.wikipedia.org/wiki/Jean_de_Brunhoff), [Tomi Ungerer](https://fr.wikipedia.org/wiki/Tomi_Ungerer), [Philippe Corentin](https://fr.wikipedia.org/wiki/Philippe_Corentin), [Grégoire Solotareff](https://fr.wikipedia.org/wiki/Gr%C3%A9goire_Solotareff), [Claude Ponti](https://fr.wikipedia.org/wiki/Claude_Ponti),[Anne Herbauts](https://fr.wikipedia.org/wiki/Anne_Herbauts), [Olivier Douzou](https://fr.wikipedia.org/wiki/Olivier_Douzou) sont quelques-unes des auteurs-illustrateurs français qui ont contribué à son rayonnement. 

### **Au Royaume Uni**

2.En anglais, l'album prend le terme de « **picturebook** ». [Randolph Caldecott](https://fr.wikipedia.org/wiki/Randolph_Caldecott), [Walter Crane](https://fr.wikipedia.org/wiki/Walter_Crane) et [Kate Greenaway](https://fr.wikipedia.org/wiki/Kate_Greenaway), à la fin du XIXe siècle, à Londres, donnèrent toute sa dimension artistique à ce support d'expression.
[Maurice Sendak](https://fr.wikipedia.org/wiki/Maurice_Sendak) est considéré comme le plus grand et le plus important des créateurs d'album, en particulier grâce à son chef-d'œuvre [Where the wild things are](https://fr.wikipedia.org/wiki/Max_et_les_Maximonstres) (1963), traduit en français par [Robert Delpire](https://fr.wikipedia.org/wiki/Robert_Delpire) en 1967, [Max et les Maximonstres.](https://fr.wikipedia.org/wiki/Max_et_les_Maximonstres)

## Composition 

L’album est composé d’images et de textes. Parfois il s’agit de la même personne qui est [l’auteur](https://fr.wikipedia.org/wiki/%C3%89crivain)-[illustrateur](https://fr.wikipedia.org/wiki/Illustrateur), parfois il s’agit de deux personnes qui ont collaboré pour faire un album.

## Une littérature pour l'enfance et la jeunesse

-C’est souvent le premier objet littéraire et artistique auquel ont accès les enfants dès le plus jeune âge.

-Plus d'un enfant développe du plaisir à lire ce genre de livre, ce qui l'amène à explorer différents thèmes, sensibilités artistiques ou [mondes imaginaires.](https://fr.wikipedia.org/wiki/Monde_imaginaire)

-Plus qu'un genre, l'album de littérature de jeunesse est une forme d'expression possédant ses propres codes. Inscrite dans un format spécifique, l'interaction image-texte est constitutive d'une narration toujours singulière. 
