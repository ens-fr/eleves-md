# L'haltérophilie

---

## Histoire 

Les tentatives d'évaluation de la force physique des hommes sont très anciennes. Les épreuves traditionnelles de force en sont aujourd'hui les témoins encore bien vivants. 

À partir du XIXe siècle, certains hommes forts acquièrent une grande notoriété et popularisent les poids et haltères, notamment en Allemagne, en Autriche et en France. Dès lors, on tente de codifier les poids et les mouvements afin de pouvoir comparer et classer les performances des athlètes. Des clubs d'haltérophile voient le jour en Allemagne dès le début des années 1880 mais c'est à Londres que se tient en 1887 le premier concours. Les premiers championnats d'Europe sont organisés à Rotterdam en 1896. 

!!! info "Le saviez-vous ?"
    L'haltérophilie est le seul sport de force à être présent aux Jeux Olympiques. Sa première apparition à ces derniers est lors de l'été 1896.

### L'arraché

La barre est soulevée bras tendus au-dessus de la tête en **un seul mouvement** très dynamique.

L'haltérophile saisit la barre au sol, une prise de main large est utilisée. Il se relève en accélérant progressivement, et se redresse en montant en extension sur la pointe des pieds et en haussant les épaules. Les bras restent tendus et le dos plat afin de transmettre le maximum d'énergie à la barre. Après cette extension, l'athlète chute le plus vite possible sous la barre grâce à une action de fléchissement des jambes. Il la récupère en position accroupie, bras tendus au-dessus de la tête. Il se relève pour atteindre la position finale : jambes tendues, bras tendus et le corps stable. 

![Un arraché](images/zebulon_arrache.jpg)


### L'épaulé-jeté

La barre est soulevée bras tendus au-dessus de la tête **en deux mouvements**.

L'haltérophile saisit la barre au sol, une prise de main correspondant à la largeur des épaules est utilisée. Il se relève en accélérant progressivement, et se redresse en montant en extension sur la pointe des pieds et en haussant les épaules. Les bras restent tendus et le dos plat afin de transmettre le maximum d'énergie à la barre. Après cette extension, l'athlète chute le plus vite possible sous la barre et la récupère en position accroupie, la barre reposant sur ses clavicules, les coudes étant relevés. Il se relève, la barre restant au contact des épaules. Une fois debout, la première phase, l'épaulé, est terminée. L'haltérophile prend alors une impulsion, jette la barre et s'abaisse à nouveau pour pouvoir saisir la barre bras tendus au-dessus de la tête (le jeté). Ce mouvement se fait généralement en fente, c’est-à-dire une jambe en avant, l'autre en arrière. Un mouvement latéral des pieds ou une flexion sont parfois utilisés. Enfin, il se relève pour atteindre la position finale. 

![Un épaulé](images/numero_1_jete.jpg)

---

## Equipement

- La barre mesure 2 200 mm et pèse 20 kg pour les hommes, ou 2 010 mm et 15 kg pour les femmes. Sur la partie centrale de la barre, un moletage rend la barre rugueuse et facilite la prise. Sur chaque côté, on y glisse des disques qui sont maintenus par un collier pesant 2,5 kg. 

- Les disques sont en métal et recouverts de caoutchouc (afin d'amortir les chocs). Ils respectent le code de couleur suivant :

| Couleur | Taille | Poids  |
| ------- | ------ | -----  |
| Rouge   | Grand  | 25 kg  |
|         | Petit  | 2,5 kg |
| Bleu    | Grand  | 20 kg  |
|         | Petit  |  2 kg  |
| Jaune   | Grand  | 15 kg  |
|         | Petit  | 1,5 kg |
| Vert    | Grand  | 10 kg  |
|         | Petit  | 1  kg  |
| Blanc   | Grand  |  5 kg  |
|         | Petit  | 0,5 kg |

- Des disques de 2,0 kg à 0,5 kg ont été ajoutés depuis que la règle du 1,0 kg prévaut (2005) lors des compétitions : la charge doit être un multiple de 1,0 kg.
- Le plateau, au sol, mesure 4 × 4 m.
- L'haltérophile porte des chaussures spéciales, rigides et stables. Elles comportent un talon de quelques centimètres. En compétition, il doit porter un maillot réglementaire. Il peut porter, s'il le souhaite, une ceinture de cuir large (maximum 120 mm de hauteur) pour soutenir le bas du dos, ainsi que des bandes aux poignets et aux genoux.
- Pour améliorer la prise de barre, l'haltérophile utilise de la magnésie, comme les gymnastes.

---

## Catégories de poids 

Depuis le 5 juillet 2018, les catégories sont :

- Catégories masculines : - 55 kg, - 61 kg, - 67 kg, - 73 kg, - 81 kg, - 89 kg, - 96 kg, - 102 kg, - 109 kg, + 109 kg.
- Catégories féminines : - 45 kg, - 49 kg, - 55 kg, - 59 kg, - 64 kg, - 71 kg, - 76 kg, - 81 kg, - 87 kg, + 87 kg.

L'attribution à des catégories de poids se fait en fonction du poids de corps de l'athlète

---

## Dans la région (PACA)

### Quelques noms de clubs

Comme clubs dans la région PACA, on peut citer le CHCM[^1](Mondragon, Vaucluse), EEAR Monteux[^2] (Monteux, Vaucluse) ou encore l'ASLDD[^3] (Toulon, Var).

### Championnat de France par équipes de ligue (23 - 24 / 04 / 2020)

#### La région PACA et ses jeunes haltérophiles
Les 23 et 24 avrils 2022, lors du championnat de France par équipes de ligue à St-Marcelin, les jeunes athlètes provençaux ont représentés la région PACA à travers les équipes mixtes U17 et U20.

![Les équipes U17 et U20 de la région PACA](images/equipe_paca.jpg)

#### Résultats 

Ci-dessous le _score sheet_ (qu'on pourrait traduire par "feuille des résultats) des athlètes.
![score_sheet de ma compétition](images/score_sheet.png)

Signification des différents sigles :

1. Licence : numéro de licence 
2. Nom : nom et prénom
3. AN : année de naissance
4. Club : nom du club
5. NAT : nationnalité
6. P.C. : poids de corps 
7. 1 ; 2 ; 3 : numéro de l'essai (vert = réussi ; rouge = manqué[^4])
8. ARR : meilleure perfomance à l'arraché
9. EPJ : meilleure performance à l'épaulé-jeté
10. TOTAL : addition de ARR et EPJ
11. Série : niveau associé aux performances (régional, fédéral ou national par exemple)
12. Catégorie : catégorie d'âge et de poids
13. IWF : score calculé en fonction du total effectué et du poids de corps

[^1]: [lien du site internet du CHCM](http://chcm.fr)
[^2]: [lien du site internet de l'EEAR Monteux](http://eearhalterophilie.fr/)
[^3]: [lien du site internet de l'ASLDD](https://www.dumonthaltero.com/)
[^4]: en haltérophilie, lorsqu'un essai est manqué, on dit "essai", soulignant le fait que ce n'est pas un échec mais une tentative.