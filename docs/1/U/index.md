# L'histoire de L'OM ⚪🔵
---
## 1ère Partie : La création du club




### Le créateur du club : René Dufaure de Montmirail
René Dufaure de Montmirail était un directeur sportif qui a crée **le club de rugby de marseille** à 21 ans et à 23 ans, il crée l'**Olympique de Marseille**

---

De 1904 à 1937, le club joue au stade de la Huveaune situé dans le 8ème arrondissement de la ville


![stade de la Huveaune](../Documents/lycee/huveaune.jpg)

---

## 2ème Partie : Le Palmarès du club

!!! info les trophées 
    Le club a gagné en tout 26 trophées nationals et 2 trophées européens 
### compétitions nationales :

| nom de la coupe | date | nombre |
|---------|-------------|---|
| Ligue 1 | 1937, 1948, 1971, 1972, 1989, 1990, 1991, 1992, 1993 et 2010 | 26 |
| Ligue 2 | 1995 | 1 |
| Coupe de France  | 1924, 1926, 1927, 1935, 1938, 1943, 1969, 1972, 1976 et 1989  | 10 |
| Coupe de la Ligue | 2010,2011,2012 | 3 |
| Trophées des Champions | 2010,2011 | 2 |

### compétitions européennes :

| nom de la coupe | date | nombre |
|---------|-------------|---|
| Ligue des champions[^ch] | 1993 | 1 |
| Coupe Intertoto | 2005 | 1 |

!!! info  
    En 1993, l'OM soulève le trophée de la ligue des champions avec une magnifique finale contre l'AC Milan. A ce jour, l'OM reste le seul club francais à avoir obtenu ce titre. En 1991, le club avait perdu en finale de la même coupe contre l'étoile rouge de Belgrade
    
---
## Les joueurs importants du club

![Jean Pierre Papin](../Documents/lycee/papin.jpg) 
La première légende du club qui vient en tête est **Jean Pierre Papin**. Il a été 4 fois champion de France avec l'OM, 1 fois vainqueur de la Coupe de France en 1989 et 1 fois finaliste à la finale de la Ligue des Champions avec le club. En 1991, il devient le seul ballon d'or de ligue 1.

![Didier Deschamps](../Documents/lycee/DeschampsDidier.jpg)

Didier Deschamps fait partie des joueyrs clés du club : en 1993 il a remporté la ligue des Champions avec l'OM en tant que capitaine. En 2009, il devient entraineur de l'OM jusqu'en 2012

![Basile Boli](../Documents/lycee/Boli.jpg) 

Basile Boli est arrivé a l'OM en 1991, cette même année, il a inscrit 8 buts en tant que défenseur. Il continue ensuite avec 2 très bonnes saisons dont une où il remporte la ligue des champions en inscrivant le but de la finale.

On aurait pû citer beaucoup d'autres joueurs comme Eric Cantona, Marcel Desailly, Fabien Barthez, Mandanda, et d'autres.

!!! info les maillots
    [en cliquant ici, vous accederez à une page contenant tus les maillots du club](https://www.om1899.com/page.php?id=/historique/maillot/maillot.htm)
    
---

## partie 4 : l'actualité du club

Cette saison(2021-2022), l'OM a été éliminé en phase de poules d'Europa ligue[^eu] et a donc été reversé en Conference ligue[^co].

![Europa et Conference Ligue](../documents/lycee/Europa-Conference-League.jpg)


Après avoir battu :
- Qarabag FK(Azerbaïdjan)
- le FC Bale(Suisse)
- le Paok(Grèce) 

L'OM a été battu par le Feyenoord Rotterdam(Pays-Bas) sur le score cumulé de 3-2.

Malgrès cet échec en demi-finale, l'OM a su resté 2ème une grosse partie de la saison. Pourtant, à l'avant dernière journée, Rennes a battu l'OM, ce qui a causé de faire redéscendre le club à la troisième position, ce qui met l'OM dans une position inconfortable : le club est dans l'obligation de faire au moins un nul à la dernière journée pour rester à la troisième place.


![classement ligue 1](../documents/lycee/classement.jpg)

## Bonus

### Les logos du club

1899-1935
![1899](../documents/lycee/1899.jpg)

1935-1972
![1935](../documents/lycee/1935.jpg)

1972-1986
![1972](../documents/lycee/1972.jpg)

1986-1989
![1986](../documents/lycee/1986.jpg)

1989-1990
![1989](../documents/lycee/1989.jpg)

1990-1993
![1990](../documents/lycee/1990.jpg)

1993-1998
![1993](../documents/lycee/1993.jpg)

1998-2000
![1999](../documents/lycee/1999.jpg)

1999-2000
![2000](../documents/lycee/2000.jpg)

2000-2005
![2001](../documents/lycee/2001.jpg)

2005-
![2005](../documents/lycee/2005.jpg)

---
### annexe

- https://fr.wikipedia.org/wiki/Olympique_de_Marseille

- https://www.om1899.com

- http://www.om4ever.com







[^ch]:1ère coupe d'Europe (surnommée C1) où les clubs de la 1ère à la 4ème place de leur championnat participent selon le classement UEFA
[^eu]:2ème coupe d'Europe(surnommée C3) où participent les clubs qui n'ont pas réussi à se qualifier pour la ligue des champions
[^co]:3ème coupe d'Europe récemment crée en 2021 (surnommée C4), où les équipes ne s'étant pas qualifiées pour l'Europa ligue sont envoyées














