# La Voiture sous toute ses formes

!!! info Le terme voiture

    Le terme voiture possède différentes    significations selon le contexte. 

# Transports

Dans les transports, le terme voiture désigne tout type de véhicule, motorisé ou non, destiné au transport de voyageurs que ce soit sur Terre, sur mer ou dans les airs :

- Une voiture automobile souvent simplifié en voiture, voire abusivement en automobile, est un véhicule destiné au transport de quelques voyageurs et de leurs bagages, tel que :
ㅤ
- voiture électrique ou autonome, voiture de transport avec chauffeur, voiture sans permis ou la voiture de sécurité utilisée en cas d’arrêt ou de suspension d'une compétition automobile ;
ㅤ
- Une voiture volante qui peut se mouvoir aussi bien sur terre que dans les airs ;
ㅤ
- La voiture-balai servant à baliser la fin d'un groupe de cyclistes et à assister, ou emmener, les retardataires. Curieusement c'est souvent une camionnette apte à ramener aussi bien le cycliste que son vélo, mais aussi a lui porter assistance si nécessaire ;
ㅤ
- Une voiture de chemin de fer : véhicule ferroviaire réservé au transport de voyageurs de chemin de fer tel que :
ㅤ
- Voiture CFF, voiture UIC, voiture standard européenne, etc...
ㅤ
- Voiture-ambulance, voiture-lits, voiture-couchettes, voiture-restaurant, Pullman (voiture), etc...
ㅤ
- Une voiture hippomobile ou voiture à cheval désignant divers types de véhicules, tractés par un ou plusieurs chevaux, (carrosse, roulotte, diligence, fiacre, sulky, etc...).
ㅤ
- Une voiture d'enfant appelé aussi poussette ou landau, selon leur forme.

![Bah c'est une fiat multiplat](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Fiat_Multipla_%282002%29_%2829392161886%29.jpg/1280px-Fiat_Multipla_%282002%29_%2829392161886%29.jpg)

[Vidéo explicative](https://www.youtube.com/watch?v=4tkCidRlX9s)

## Patronyme

- Vincent Voiture, poète français du XVIIe siècle.

~~Droit d'auteur~~ : les textes sont disponibles sous licence TOUCHE PAS A MON TRAVAIL, partage dans les mêmes conditions ; d’autres conditions peuvent s’appliquer. Voyez les conditions d’utilisation pour plus de détails, ainsi que les crédits graphiques. En cas de réutilisation des textes de cette page, voyez comment citer les auteurs et mentionner la licence.
Wikipedia® est une marque déposée de la Wikimedia Foundation, Inc., organisation de bienfaisance régie par le paragraphe 501(c)(3) du code fiscal des États-Unis.

# Différents types de voitures

| **Voiture** | Wiki | Video |
|---------|----------|---|
| Voiture de sport    | [Lien wikipedia](https://fr.wikipedia.org/wiki/Voiture_de_sport) | [Video voiture de Sport](https://www.youtube.com/watch?v=4qNf14e3buQ) |
| Voiture sans permis   | [Lien wikipedia](https://fr.wikipedia.org/wiki/Voiture_sans_permis)   | [Video voiture sans permis](https://www.youtube.com/watch?v=t_AdBo0y3dE)   |
| Voiture électrique   | [Lien wikipedia](https://fr.wikipedia.org/wiki/Voiture_%C3%A9lectrique)  | [Video voiture électrique](https://www.youtube.com/watch?v=t_AdBo0y3dE) |
| voiture moteur d'avion | [Lien wikipedia](https://fr.wikipedia.org/wiki/Voiture_%C3%A0_moteur_d%27avion) | [Video moteur avion](https://www.youtube.com/watch?v=FaXXQebPriw)   |


!!! danger Risque de danger
    
    > - Depuis une soixante d'années les chiffres du nombre d'accidents de la route en France sont édifiants. Les causes sont multiples, mais le facteur humain est en tête dans la majorité des accidents corporels et des accidents graves. Véritable enjeu majeur en France, les pouvoirs publics s'investissent avec des politiques de sécurité routière qui peu à peu inversent les courbes. Le pic de mortalité est atteint en 1972 mais depuis 2001 on constate des baisses.
    > - 58 013 accidents ont eu lieu en France en 2019, entrainant la mort de 3 383 personnes. Quelles sont les villes les plus dangereuses de France en matière de sécurité routière ? Dans quelles villes recense-t-on le plus de tués sur les routes ? Dans quelles conditions surviennent les accidents dans votre commune, département ou région ? A partir des données publiées par le ministère de l'Intérieur, Linternaute.com fait le point sur les accidents de la route en France.

---

!!! info "✔️ La voiture est-elle dangereuse ?✔️"

     - [ ] oui
     - [ ] non
---

++"La voiture"+"Alcool"++ = Dangereux

!!! done "QCM"

    !!! faq "Est-il correcte de ne pas avoir sa ceinture ?"
        - [ ] `oui`
        - [ ] `non`
        - [ ] `C'est pas grave tant que les flics ne remarquent pas`
        
        
    ??? done "Réponse"
        - ❌ `oui`
        - ✅ `non`
        - ❌ `C'est pas grave tant que les flics ne remarquent pas`;il ne faut plus prendre la route

---
```mermaid
graph LR
A[Voiture] -- utilisation régulière --> B((Mauvais pour l'environnement))
A --> C(Moyen de déplacement avancée)
B --> D{Utilisation peux recommander}
C --> D
```
!!! note Pour conduire un voiture sans permis , il faut avoir l'ASSR 2
!!! note Pour obtenir le permis de conduire , il faut avoir des heures de conduites
!!! tldr Quels sont les innovations de l'automobile ?
Les innovations automobiles incontournables
- 1/ La ceinture de sécurité : 1896. ...
- 2/ L'essuie-glace : 1903. ...
- 3/ Le démarreur électrique 1911. ...
- 4/ La direction assistée : 1931. ...
- 5/ Le Pneu Radial : 1946. ...
- 6/ Le coussin gonflable de sécurité : 1953. ...
- 7/ Le système Stop & Start : 1981. ...
- 8/ Le GPS : 1995.

Merci d'avoir consulter ce site.  
:octicons-heart-fill-24:{ .heart }
```python

```
