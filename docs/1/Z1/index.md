# Les metiers de l'informatique 👨‍💻

![image](images/informatique.jpg)

!!! tldr "L'informatique"
    L'informatique est un sujet très vaste qui peut comporter de nombreuses choses...Parlons aujourd'hui des métiers dans l'informatique et plus précisement :

1. Web designer    
2. ingenieur de cybersécurite 

(_je ne compte pas m'arreter là je ferais le reste pour mon propre plaisir_)

---

## Web designer 💻
![web_designer](images/webdesigner.jpg)

!!! example "En quoi consiste ce métier 🤨"

    !!! tldr "qu'est ce que le web designer?"
        ??? info "qu'est ce que le web designer"
            `web designer`
            :   **Mi-graphiste**, **mi-informaticien** , le web- designer est spécialisé dans la création des pages Web. Il s'occupe de tout l'aspect graphique d'un site Internet (illustrations, animations, typographie...). 
        
        Il choisit la place des photos, la taille des caractères et les couleurs qui rendront la consultation agréable pour l'utilisateur. Il crée aussi les pictogrammes qui facilitent la lecture et la navigation dans le site. Dans certains cas, il sera amené à établir la charte graphique et à créer l'identité visuelle du site. C'est un exercice délicat car il faut respecter à la fois la demande du client, les impératifs de marketing et de communication, et ceux du public visé... De plus, Internet comporte des contraintes spécifiques à prendre en compte : par exemple, les temps de téléchargement trop lents qui peuvent décourager les internautes.

        !!! warning "Ce n'est pas tout"
            Le webdesigner doit maîtriser les logiciels de graphisme (Photoshop, Illustrator...) et connaître les règles de l'ergonomie pour capter l'attention des visiteurs. Il peut travailler pour un studio de création de sites Internet, le service communication d'une entreprise, ou à son compte.

!!! example "Quels études ? 📚"


    !!! tldr " Après le bac"  
        Après le bac, 3 ans pour préparer le BUT métiers du multimédia et de l'Internet, ou une licence professionnelle en webdesign, création web, design numérique, services et produits multimédias.... Autre solution : préparer en 3 ans le DN MADE mention numérique. 
    !!! warning "important"
        Comme dans n'importe quel milieu de travail le niveaux d'études correspondra a "son poste" ce que nous voulons dire c'est que si vous etudiez plus qu'un autre vous aurez des compétences qu'il n'a pas ce qui fait que vous serez plus qualifiez... 
    Je vais vous presentez quelques niveaux d'études et leurs possibilités mais pour [se renseigner](https://www.onisep.fr/Ressources/Univers-Metier/Metiers/webdesigner) suivez ce lien il vous guidera mieux que moi.

    | niveaux d'études | débouchés | plus d'infos |
    |:---:|:------:|:----:|
    | Bac +3 | Chef(fe) de projet web | [info](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/chef-fe-de-projet-web)
    | Bac +4 | Infographie 3D temps réel et jeux vidéos | [info](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/infographie-3d-temps-reel-et-jeux-videos)
    | Bac +5 | Master mention création numérique | [info](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/master-mention-creation-numerique)

    !!! tip " Ce n'est pas tout"
        Malgré les études qui sont bien evidemment conseillés, les gens le font parfois en tant que particuler c'est a dire qu'ils se forment seul et travaillent pour des entreprises ( souvent de petites entreprises) cela peut servir de revenus en plus 

!!! example "Les avis 🧐"

    !!! note " qu'en pensent t'ils ?"
        Nombreux sont les avis concernant ce métier "de l'avenir" comme ils disent et ils sont tous principalement positifs car la demande est non seulement croissante, ils ne manquent pas de travail mais aussi parce que pour faire ce métier il faut être passionés et ils le sont tous c'est donc un plaisir de se lever tout les matins pour eux 
        !!! warning " Attention "
            Malgré l'envie et la motivation il faut aussi certaine qualité comme:  
            1. La créativité  
            2. La logique  
            3. La technique 
    
    !!! faq "Ce metier t'interesse ? 😉"
        [Formation web designer](https://www.wildcodeschool.com/fr-FR/formations/formation-web-designer/remote-francais?utm_campaign=FR-SEARCH-Generic-Remote-HorsIDF&utm_term=formation%20web%20designer&utm_source=adwords&utm_medium=ppc&hsa_grp=139189715227&hsa_src=g&hsa_cam=17113022381&hsa_ad=595694061064&hsa_ver=3&hsa_kw=formation%20web%20designer&hsa_net=adwords&hsa_tgt=kwd-1137826165&hsa_mt=p&hsa_acc=4421706736&gclid=Cj0KCQjwg_iTBhDrARIsAD3Ib5jNKAKil5tzU4_WhR-wgMqLjyIpWlWt0ZEX5-wqRbwTLOcrqpmX3e0aAoxmEALw_wcB){ .md-button .md-button--primary }

---

## Ingenieur en cybersécurite 🔒

![cybersécurite](images/cybersécurite.jpg)

!!! example "En quoi consiste ce metier 🤨"
    !!! tldr "Ingenieur en cybersécurite "
        ??? info "qu'est ce qu'un ingenieur en cybersécurite ?"
            `Ingenieur cybersécurite`
            :    **L'Ingénieur Cybersécurité** participe à la définition des règles de sécurité applicatives et  logiques en réponse aux exigences fixées par des référentiels de bonnes pratiques ou par des réglementations propres à l'activité de l'entreprise.
        Ses ennemis : les virus et les hackers (pirates informatiques). Sa hantise : une faille dans le réseau. Avec des informations de plus en plus nombreuses en ligne, les virus contaminent serveurs et messageries en quelques clics. L'expert en sécurité est là pour protéger les données et traquer les failles de sécurité des réseaux Internet et intranet. Il évalue d'abord le niveau de vulnérabilité des sites, traque d'éventuels virus et met en échec les tentatives d'intrusion de hackers. Ensuite, il met en place tout un système de protection : mots de passe, cryptologie, pare-feu, antivirus, etc. Les parades ne manquent pas pour réduire les risques.
        !!! warning " Mais il y a des obligations !"
            Toujours au fait des dernières tendances et menaces sur le Net, cet expert est de plus en plus recherché par les entreprises. Ce métier demande de l'intégrité, de la disponibilité mais aussi un respect total de la confidentialité.

!!! example "Quels études ? 📚 "
    !!! tldr " Après le bac"
        5 ans pour obtenir un diplôme d'ingénieur ou un master en informatique, et plusieurs années d'expérience dans les réseaux informatiques sont nécessaires.
        !!! warning "Ce n'est pas tout"
            Il faut bien avoir en tête que l'école est suffisante, il faut aussi s'entrainer chez soi pour acquerir de l'experience en plus !!
    Je vais presenté quelques niveaux d'études, encore une fois je ne ferais que résumer si vous avez besoin de [plus d'infos](https://www.onisep.fr/Ressources/Univers-Metier/Metiers/expert-experte-en-securite-informatique) suivez ce lien vers un site certifié.

    | niveaux d'études | débouchés | plus d'infos |
    |:---:|:------:|:----:|
    | Bac +5 | Manager en infrastructures et cybersécurité des systèmes d'information (CESI ESA) | [info](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/manager-en-infrastructures-et-cybersecurite-des-systemes-d-information-cesi-esa)
    | Bac +6 | Master of Science manager de la cybersécurité | [info](https://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/master-of-science-manager-de-la-cybersecurite)

    ??? bug "panneaux coulissants"
        J'ai le même bug que certain élèves les panneaux coulissants ne marchent pas sur mon pc je voulais en mettre ici mais du coup je n'ai pas reussis  
        Bonne continuation et bonne lecture
    !!! tip "Ce n'est pas tout"
        Encore une fois il est très conseillés de continuer a s'entrainer seul en même temps des études des aptitudes en plus ne font pas de mal...

!!! example "Les avis 🧐"
    !!! note "Qu'est ce qu'en pense les pratiquants ?"
        Encore une fois les avis sont quasi les mêmes, ces métiers pour l'instant très peu connus du grand public seront bientôt très connus ce qui en faits des métiers d'avenir.Pour accéder a ce métier il précise tous qu'il faut beaucoup de perseverance et beaucoup d'envis car le niveau d'études est très élevés mais si vous êtes passionés la suite sera génial!!  
        !!! cite "nathan"
            nous ne cessons d'en apprendre, chaque jour est un plaisir de ce lever
        Bref dans un monde qui prends enormément d'ampleur informatique ces métiers ont un très beau futur qui s'ouvre devant eux.

        !!! warning "Certaines qualités sont requises"
            - La perseverance
            - Parler couramment anglais
            - Fort sens logique
            - Perfectionniste
    !!! faq " Alors sa te tentes ? 😉 "
        [Formation cybersécurite](https://www.esilv.fr/formations/cycle-ingenieur/majeures/informatique-objets-connectes-et-securite/){ .md-button .md-button--primary }

---

## Developpeur / Developpeuse informatique ⌨️

![developpeur_informatique](images/Logo_CDI.gif)

!!! example " En quoi consistes ce metier "
    !!! tldr " developpeur informatique"
        ??? info "qu'est ce qu'un developpeur informatique"
            En informatique, un développeur ou analyste programmeur est un informaticien qui réalise des logiciels et les met en œuvre à l'aide de langages de programmation.  
            En l'occurence Le développeur informatique est le pro des langages informatiques, tels que C++ ou Java ! Responsable de la programmation, c'est-à-dire de la production de lignes de code, il rédige et suit un cahier des charges précisant les spécificités techniques à suivre pour créer le programme.
        Afin de concevoir des programmes informatiques « sur mesure », il participe en amont à l'analyse des besoins des utilisateurs, puis à la phase d'essai. En aval, il adapte le logiciel à la demande du client en apportant les retouches nécessaires. Le dévelop-peur prend en charge la formation des utilisateurs de l'application et peut même rédiger un guide d'utilisateur. Par la suite, il intervient pour effectuer la maintenance ou faire évoluer les programmes. Grâce aux progiciels (des logiciels standards de programmation prêts à être utilisés), il passe moins de temps à écrire les programmes, si bien que son activité évolue vers plus d'analyse que de programmation.
        !!! done " Ce n'est pas tout !"
            En tête des gros recruteurs : les ESN (entreprises de services du numérique), très friandes d'informaticiens.
    
    
    En cours...

 