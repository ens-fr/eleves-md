# Le Japon

## L'histoire du Japon 🗾

### Drapeau
Le drapeau du Japon se nomme aussi Hinomaru dont la signification est "le disque du soleil". Le japon étant le pays du Soleil-Levant, le rouge représente d'une part ce dernier, mais aussi la déesse du soleil Amaterasu Omikami. Le rouge est aussi le miroir de l'éthique shintoîste qui repose sur un coeur pur et juste. Comme pour la majorité des drapeaux, la pureté et l'intégrité sont représentées par le blanc.

![drapeau du japon](image_histoire/image.jpg)

### Antiquite
L’histoire du Japon commence avec le peuplement d'un groupe d'îles au sud-est de la péninsule coréenne, environ 100 000 ans av. J.-C. Les premières traces d'industrie, des pierres polies, remontent à 32 000 ans. Des poteries, parmi les plus anciennes de l'humanité, sont produites vers 13 000 ans avant notre ère lors de la période Jōmon, et comprennent les premières formes d'œuvres artistiques : les dogū. 400 ans avant notre ère, au cours de la période Yayoi, sont introduites des technologies venant de Chine et de Corée comme la riziculture et la fonte du bronze et du fer.

#### Epoque historique Importante

=== "Epoque Yamato"

    La période Yamato désigne la période de l'histoire du Japon où une structure politique et sociale avec un souverain à sa tête se met en place dans la province de Yamato, autour de Nara, vers 2501.

=== "Epoque Nara "

    L'époque de Nara  est l'une des 14 subdivisions traditionnelles de l'histoire du Japon. Cette période se situe entre 710 et 794 . Elle est précédée par la période d'Asuka (du milieu du VIe siècle jusqu'en 710) et suivie par l'époque de Heian .

    L'ère Tenpyō (ou Tempyō) (729-749), seconde période de Nara après l'ère Hakuhō selon certains historiens d'art3, sert, parfois, à évoquer toute la période dans le domaine artistique, car la culture a été particulièrement brillante à ce moment-là. Le Japon constitue alors un foyer culturel de première importance à côté de Silla, en Corée, et de la Chine, se présentant comme un petit empire « civilisé » (selon des critères posés par l'empire chinois et auxquels le Japon adhère). 
=== "Epoque Heian"

    L'époque de Heian (平安時代, Heian-jidai?) est l'une des 14 subdivisions traditionnelles de l'histoire du Japon2. Cette période, précédée par l'époque de Nara, commence en 7943 et s'achève en 1185 avec l'époque de Kamakura4. L'ancienne capitale, Nara (Kansai), est abandonnée au profit de la création de Heian-kyō, future Kyoto (Kansai).
    L'époque de Heian (mot qui signifie « paix » en japonais) est considérée comme l'apogée de la cour impériale japonaise, et est célébrée comme l'âge d'or de la culture et de l'art japonais, notamment la poésie japonaise, la littérature japonaise et la peinture dans le style japonais, Yamato-e. 
=== "Epoque Sengoku"

    L'époque Sengoku (戦国時代, Sengoku-jidai?, littéralement « époque/ère des provinces en guerre », en référence à la période des Royaumes combattants chinois) est une époque de l'histoire du Japon marquée par des turbulences sociales, des intrigues politiques et des conflits militaires quasi permanents, qui s'étend du milieu du XVe siècle à la fin du XVIe siècle au Japon. Au sens strict, cette période débute à la fin des guerres d'Ōnin en 1477 et dure jusqu'en 1573, lorsque le seigneur de la guerre Oda Nobunaga destitue le dernier shogun Ashikaga. Elle couvre approximativement la seconde moitié de l'époque de Muromachi entendue au sens large, qui correspond au shogunat des Ashikaga et s'étend de 1336 à 15731.
=== "Epoque Edo"

    L'époque d'Edo ou période Tokugawa est la subdivision traditionnelle de l'histoire du Japon qui commence vers 1600, avec la prise de pouvoir de Tokugawa Ieyasu lors de la bataille de Sekigahara, et se termine vers 1868 avec la restauration Meiji. Elle est dominée par le shogunat Tokugawa dont Edo est la capitale
=== "Epoque Empire Du Japon"

    L'empire du Japon est le régime politique du Japon durant la période allant de l'ère Meiji à l'ère Shōwa et englobant la Première Guerre mondiale et la Seconde Guerre mondiale. C'est a partir de cette periode que la capitale du japon est devenue Tokyo
=== "Japon contemporain"

    Le nom d'histoire contemporaine du Japon désigne la période ayant suivi la fin de la Seconde Guerre mondiale. La Constitution d'après-guerre a été modifiée, le nouveau texte entrant en application en 1947. Cette période est appelée l'après-guerre ou le Japon d'après-guerre



![frise chronologique du japon](image_histoire/frise1.png)
 


### Empereur du Japon depuis l'ère contemporaine

|Nom|Date de regne| photo|
|---|----|---------|
|<font size="5">Hirohito </font> |<font size="5"> Empereur de 1926 à 1989</font>   | ![photo de hirohito](image_histoire/hirohito.jpg)    |
| <font size="5">Akihito </font> |<font size="5"> Empereur de 1989 à 2019</font>  |![photo de akihito](image_histoire/akihito.jpg)   |
| <font size="5">Naruhito </font>  |<font size="5"> Empereur de 2019 à 20... </font>       |  ![photo de naruhito](image_histoire/naruhito.jpg)  |

   
<iframe width="560" height="315" src="https://www.youtube.com/embed/8TYTDmCngoc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



L’histoire du Japon commence avec le peuplement d'un groupe d'îles au sud-est de la péninsule coréenne, environ 100 000 ans av. J.-C. Les premières traces d'industrie, des pierres polies, remontent à 32 000 ans. Des poteries, parmi les plus anciennes de l'humanité, sont produites vers 13 000 ans avant notre ère lors de la période Jōmon, et comprennent les premières formes d'œuvres artistiques : les dogū. 400 ans avant notre ère, au cours de la période Yayoi, sont introduites des technologies venant de Chine et de Corée comme la riziculture et la fonte du bronze et du fer. 


## Le japonais 

Il y a 46 hiragana et 46 katakana.Les hiragana sont les symboles les plus employés pour écrire le japonais dans le langage courant. C'est souvent par là qu'on commence en étant étudiant (même les enfants japonais débutent par les hiragana).

Les katakana ne sont utilisés que pour la transformation des mots d'origine étrangère. Ainsi, le nom d'un pays ou d'une ville sera écrit en katakana et pas en hiragana.
Dans les deux cas, les caractères ne correspondent pas à une lettre (a, b, c, d...) mais à une syllabe (ke, se, re...). Par exemple, le mot chat se dit neko en japonais. Il faut quatre lettres en français pour l'écrire mais seulement deux caractères en japonais. Les seules "lettres" qui existent sont les voyelles a, e, i, o, u.

### Les Lettres
![lettre japon](langue/lettre.png)

Chaque ligne est consacrée a une voyelle et chaque colonne a une consonne .Les kana sont plus facile a apprendre que les katakana ils sont constitues de syllabes pour la plupart et non pas de simple lettres .
Les voyelles a,i,o se prononcent comme en francais tandis que le e se dit é et le u se pronoce ou

### Les Chiffres
    
|Chiffre en francais|kanji|hiragana|
|------------|-------------|----------|
|1|ich |いち [ichi] |
|2|ni|に [ni] |
|3|san|さん [san]|
|4|yon|よん [yon]|
|5|go|ご [go]|
|6|roku|ろく [roku] |
|7|nana|なな [nana]|
|8|hashi|はち [hachi] |
|9|kyuu||きゅう [kyû]|
|10|jyuu|じゅう [jyû]|
|100|hyaku| ひゃく[jyû]|
|1000|issen|せん [sen]|
|10000|ichiman| まん[man]|


!!! danger "Chiffre quatre et sept"
    Pour les chiffres 4 et 7 ils se prononcent "yon" et "nana" Même si on peut dire "shi"
 	 et "shichi" on préfere dire yon et nana car shi veut aussi dire la mort et dans les croyances populaires ont penser donc que ses chiffres portaient malheur.


Pour faire les autres nombres il suffit de fusionne ce présent dans le tableau par exemple 33 se dira sanjyuu san
666 se dire rokuhyaku rokujyuu roku
 	 



## Culture
### Les Samourais
Le terme « samouraï », mentionné pour la première fois dans un texte du xe siècle, vient du verbe saburau qui signifie « servir ». L'appellation est largement utilisée dans son sens actuel depuis le début de la période Edo, vers 1600. Auparavant, on désignait les guerriers plutôt par les termes mono no fu (jusqu'au viiie siècle), puis tsuwamono (強者?)1 ou bushi (武士?), qui peuvent l'un ou l'autre se traduire par « homme d'armes ».

Les guerriers sont souvent décrits comme des « Ebisu », c’est-à-dire des barbares dans le Dit des Heike. À partir de la période Edo, les termes bushi et samouraï ne sont pas tout à fait synonymes, il existe une différence subtile (voir l'article Bushi).

On trouve aussi parfois le terme buke : il désigne la noblesse militaire attachée au bakufu (gouvernement militaire), par opposition aux kuge, la noblesse de cour attachée à l'empereur. Les buke sont apparus durant l'ère Kamakura (1185-1333).

![kanji de samurai](culture/samurai.png)

Le ==bushido== (voie du guerrier) est un ensemble de principes que devait respecter le samouraï.
Un samouraï n'ayant pas de rattachement à un clan ou à un daimyō (seigneur féodal) était nommé rōnin.


Un samouraï qui était un vassal direct du [shogun](https://fr.wikipedia.org/wiki/Shogun) était appelé hatamoto.

Cependant, tous les soldats n'étaient pas samouraïs, ceux-ci constituant une élite équivalent en quelque sorte aux chevaliers européens ; l'armée, à partir de la période Kamakura, reposait sur de larges troupes de fantassins de base nommés ashigaru et recrutés principalement parmi les paysans. était appelé hatamoto.



!!! cite "Elon Musk"
     `Ma mentalité est celle d'un samouraï. Je préfère commettre un seppuku qu'échouer.`

Les samouraïs utilisaient environ 40 armes  et les plus importantes étaient :

!!! example "Différents types d'armes"
    - Le **katana** 
        Le katana  est un sabre de plus de 60 cm.
    - Le **tanto**
        Le tanto était un petit poignard Il était utilisé quand un samouraï devait faire seppuku ou hara-kiri (suicide).
    - Le **Yumi**
        Le Yumi était un arc à fleches qui servait essentiellement pour des assasinats car en guerre inefficace contre les armures. 
    

!!! note " Armure et baton "
   - On peut noter également l'armure des samourais qui étaient très épaisse , elles
    sont des Équipement protecteur passif qui couvre le samouraï partiellement ou totalement de la tête aux pieds selon l'époque et le modèle
    - Ou encore le baton de commandement qui pouvait être aperçu de loin. Il s'agissait d'un bâton orné à une extrémité d'un faisceau de poils de yak, de lamelles de papier laqué, de lanières de cuir ou de bandelettes de tissu. Le bâton était fixé à l'armure à l'aide d'une corde. Son utilisation remonte aux années 1570.

![armure de samourai](culture/armure.jpg)

![baton de commandement](culture/baton.jpg)





### Les Mangas et Animés
Les Mangas sont la base de la culture japonaise
De nombreux mangas connaissent un succès ressplendissant pas qu'a travers le japon mais a travers le monde entier.
Certains de ces mangas ont aussi une adaptation par des studios d'animation qui permettent a l'oeuvre d'agrandir sa visibilité.

Les plus célèbre studios sont:

- <font size="5">Mappa </font> 
- <font size="5">Toei Animation </font> 
- <font size="5">Bones </font> 
- <font size="5">Madhouse </font> 
- <font size="5">Ufotable </font> 

!!! info "Studios et leur animation"

    === "Mappa"
        ??? L'Attaque Des Titans
            date de sortie: 2013
            Nom de l'auteur: Hajime Isayama
            ![image de L'Attaque Des Titans](culture/manga/snk.jpg)

        ??? Jujutsu kaisen
            date de sortie: 2020
            Nom de l'auteur:Gege Akutami
            ![image de jjk](culture/manga/jjk.jpg)

        ??? Prochainement Vinland Saga
            date de sortie: 2019
            Nom de l'auteur:Makoto Yukimura
            ![image de vinland saga](culture/manga/vs.jpg)

    === "Toei Animation"
        ??? One Piece
            date de sortie: 1999
            Nom de l'auteur:Eiichiro Oda
            ![image de one piece](culture/manga/op.jpg)

            

        ??? Dragon Ball
            date de sortie: 1989
            Nom de l'auteur:Eiichiro Oda
            ![image de db](culture/manga/db.jpg)

    === "Bones"
        ??? My hero academia
            date de sortie: 2014
            Nom de l'auteur: Kōhei Horikoshi
            ![image de mha](culture/manga/mha.jpg)

        ??? Full Metal Alchemist
            date de sortie: 2009
            Nom de l'auteur: Hiromu Arakawa
            ![image de fma](culture/manga/fma.jpg)

    === "Madhouse"
        ??? Death Note
            date de sortie: 2006
            Nom de l'auteur:Tsugumi Ōba*
            ![image de death note](culture/manga/dn.jpg)

        ??? One Punch Man
            date de sortie: 2009
            Nom de l'auteur: One
            ![image de opm](culture/manga/opm.jpg)

        ??? Hunter x Hunter
            date de sortie: 2011
            Nom de l'auteur:Yoshihiro Togashi
            ![image de hxh](culture/manga/hxh.jpg)
    === "Ufotable"
        ??? Demon Slayer
            date de sortie: 2019
            Nom de l'auteur:Koyoharu Gotōge
            ![image de demon slayer](culture/manga/dms.jpg)

        ??? Fate
            date de sortie: 2004
            Nom de l'auteur:Kinoko Nasu
            ![image de fate](culture/manga/fsn.jpg)

Certains mangas sont connus pour leur histoire et d'autres pour leur dessin qui sont parfois des plus magnifique :

![image de manga](culture/manga/wano.jpg)
![image de manga](culture/manga/to.webp)
![image de manga](culture/manga/tk.jpg)


Le Manga et les animés sont donc le coeur meme de la sociéte japonnaise.

### La nourriture 🍡

<font size="5">La culture japonaise possède aussi des plats typiques de chez eux qui donne l'eau à la bouche </font> 

<font size="4">On va donc faire un top 5 des meilleurs nourritures japonaises à gouter :</font> 

<font size="5">TOP 5</font>

Les Mochis :
![image de manga](culture/manger/mochi.jpg)
Consommés en grande quantité lors des événements traditionnels japonais, il s’agit de boules à base de farine de riz gluant, parfumées de différents ingrédients. Il en existe aux haricots rouges, au sirop, façon teriyaki ou encore au soja.Les gouts sont très nombreux et très insolite .

<font size="5">TOP 4</font>

LA SOUPE MISO :
![image de manga](culture/manger/soupe.jpg)
La soupe miso est l’un des plats les plus populaires du Japon. Elle servirait à se rincer la bouche entre les dégustations, pour apprécier indépendamment la finesse de chaque bouchée. Le miso est une pâte de soja fermentée qui sert à la confection du bouillon, auquel on ajoute des légumes, des champignons, et parfois du tofu.


<font size="5">TOP 3</font>

Les Takoyakis :
![image de manga](culture/manger/tako.jpg)
Les Takoyakis sont des boules de pieuvre qui nous vient tout droit de Osaka. Ce met raffiné a base de farine est une pate contenant des morceaux de pieuvre , accompagné souvent d'une sauce . Les Takoyakis se placent dnc en 3^ème^ position.

<font size="5">TOP 2</font>

Les Suchis : 
![image de manga](culture/manger/sushi.jpg)
Les sushis sont presque plus populaires en Europe qu’au Japon, où ils sont consommés occasionnellement. Il s’agit d’assortiments de riz vinaigré sucré et de tranches de poisson cru (aussi appelées sashimis), ainsi que de légumes. Une boule de riz recouverte simplement d’un sashimi sera appelée nigiri, alors qu’un rouleau de riz et de poisson enveloppé dans une algue nori est un maki. On les sert avec du wasabi et de la sauce soja, mais attention : au Japon, il faut les déguster rapidement et en une seule bouché et de préférence sans sauce . Il ne se place pas en 1^ère^ position car très présent en Europe.

<font size="5">TOP 1</font>

Les Ramens : 
![image de manga](culture/manger/ramen.jpg)
Un rāmen est une recette de cuisine traditionnelle de la cuisine japonaise, à base de nombreuses variantes de bouillons de nouilles, agrémentés de poissons, viandes, légumes, algues, œuf, et assaisonnements miso ou sauce de soja... 
Il est le plat japonais par excellence .Sa popularite vient entre autre des mangas.


Un peu de musique d'animés avant le quizz 🎵

<iframe width="560" height="315" src="https://www.youtube.com/embed/xEVcTStgA4A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


- SOURCES :
     - [Wikipédia sur le Japon](https://fr.wikipedia.org/wiki/Japon)
     - [Site Markdown](https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/)
     - [Wikipédia sur les samourais](https://fr.wikipedia.org/wiki/Samoura%C3%AF)
     - [Wikipédia sur les epoques du japon](https://fr.wikipedia.org/wiki/%C3%88res_du_Japon)


