## Top 5 Marques d'Ordinateurs :boom:

| |**Marques**|**Fonctions**|**Image**|
|-|-----------|-------------|-------------|
|1|<font size = "4">Apple</font>|<font size = "4">Haut de Gamme</font>|![Apple](image/ap.png)|
|2|<font size = "4">Microsoft</font>|<font size = "4">Innovants et Ergonomiques</font>|![microsoft](image/mi.png)|
|3|<font size = "4">Lenovo</font>|<font size = "4">La Marque Montante</font>|![Lenovo](image/le.png)|
|4|<font size = "4">Dell</font>|<font size = "4">Le Positionnement Professionnel</font>|![dell](image/de.png)|
|5|<font size = "4">Asus</font>|<font size = "4">La Polyvalence</font>|![Asus](image/as.png)|

## Quelques Raccourcis :mag:

|:trophy:|**Raccourcis**|**Fonctions**|
|-|-----------|-------------|
|:anchor:|++"↪ Ctrl"+"C"++|==Copier== du texte|
|:anchor:|++"↪ Ctrl"+"V"++|==Coller== du texte|
|:anchor:|++"↪ Ctrl"+"X"++|==Couper== du texte|
|:anchor:|++"↪ Ctrl"+"Z"++|==Annuler==|
|:anchor:|++"↪ Ctrl"+"Y"++|==Rétablir==|
|:anchor:|++"↪ Ctrl"+"P"++|==Imprimer==|

## Quelques Définitions :thought_balloon: 

`Processeur`
: Partie d'un ordinateur qui interprète et ^^exécute^^ les instructions dans un language donné 

`Carte Graphique`
: Elle permet l'affichage de graphismes ^^2D ou 3D^^ sur l'écran d'un PC 

`Ordinateur`
: Machine automatique de ^^traitement de l'information^^, obéissant à des programmes formés par des suites d'opérations arithmétiques et logiques

## Quelques liens externes :file_folder:

[Apple](https://www.apple.com/)

Acer :

    https://store.acer.com

[Lenovo](https://www.lenovo.com)

>**2 Manières de Mettre un Lien** :
>
> - Avec un texte cliquable !
> - Avec un texte copiable !

Quelques astuces !

## 💻 Une vidéo astuce en informatique 💻

<iframe width="560" height="315" src="https://www.youtube.com/embed/DVNPrVA8-ME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


A VENIR ...

