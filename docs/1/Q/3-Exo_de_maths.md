# Exos de Maths

!!! tldr "Des **petits** exos de _Mathématiques_ !"
=== "`Premier Exo`"
      
      Faites le calcul suivant:
       5 + (2 + 9) * (15 - 11) + 17 * 3   
      

      - [ ] 610
      - [ ] 880
      - [ ] 100
      - [ ] 115

=== "`Second Exo`"

      Calculer la fonction dérivée suivante et trouvez le résultat pour f'(2) :
            f'(x) = 3x³ + 9x - 11

      - [ ] 54
      - [ ] 42
      - [ ] 21
      - [ ] 43

!!! danger "Exo Python"

      Définir une `fonction` qui va déterminer le plus petit chiffre d'une liste prédéfini :
            l'exemple du sujet sera: 4, 3, 6, 2, 6, 8, 9 !

# Correction

!!! done "Corrections Exos Maths"

        === "`Exo n°1 de Maths`"

            - [x] La réponse est 100 !

            ??? tldr "Explication"

                1. On fait d'abord la double distributivité 
                2. Ensuite on multiplie 17 par 3
                3. On ajoute tous
                4. Et on trouve 100

        === "`Exo n°2 de Maths`"

            - [x] La réponse est 54 !

            ??? tldr "Explication"

                1. On dérive la fonction
                2. On remplace x par 2
                3. On calcule
                4. On trouve 54 !!!

??? done "Exo Python Correction"
    
    ```python
        def min2(val1, val2):
            if val1 < val2:
                return val1
            else:
                return val2
        valMin = int(input())
        for iVal in range(9):
          valMin = min2(valMin, int(input()))
        print(valMin)
    ```
    ??? tldr "Explication"

        1. On `défini` une fonction "min"
        2. On met une `condition` dans laquelle si la Valeur1 plus petite que la 2
        3. Alors on `retourne` la Valeur1
        4. Sinon
        5. On `retourne` la Valeur2
        6. On `défini` une Valeur minimale avec un nombre aléatoire
        7. On créé une `boucle` de 9 tours !
        8. On défini une `variable` valMin qui prend le minimum 
        9. On `print` la Valeur minimale !?!




   


