# La société AMD

!!! danger "Introduction"

        `Advanced Micro Devices (AMD) est un fabricant américain de semi-conducteurs, microprocesseurs, cartes graphiques basé à Santa Clara (Californie). 
        Ryzen (se prononce « raille-zeune ») est une unité centrale (UC) d'AMD qui cible les segments commerciaux des serveurs, ordinateurs de bureau, stations de travail, PC multimédias et équipements tout-en-un. Une jouabilité et un gameplay fluides constituent des facteurs essentiels pour un processeur hautes performances. Ryzen offre d'excellentes performances avec les titres les plus exigeants sur le plan graphique, comme Ashes of the SingularityTM, un jeu de stratégie en temps réel très populaire.`

## Les processeurs AMD[^1]

!!! info "Les _caractéristiques_ du processeur"

        - nombre de coeurs
        - nombre de threads
        - la cadence
        - mémoire cache

|:trophy:|**Nom**|**Prix**|**Qualité**|
|-|-----------|-------------|-------------|
|🥇|<font size = "3"> ThreadRipper 3960X </font> |<font size = "3">1 710€ </font>|Haut de Gamme|
|🥈|<font size = "3">Ryzen 9 5900X </font>|<font size = "3">440€</font>|Millieu de Gamme|
|🥉|<font size = "3">Ryzen 5 4500 </font>|<font size = "3">150€</font>|Bas de Gamme|



## Les catégories de carte graphique

!!! note "Les _caractéristiques_ d'une carte graphique"

        - la mémoire
        - son processeur graphique "GPU"

|:trophy:|**Nom**|**Prix**|**Qualité**|
|-|-----------|-------------|-------------|                                                     
|🥇|<font size = "3">RX 6900 XT </font>|<font size = "3">1 400€</font>|Haut de Gamme|
|🥈|<font size = "3">RX 6600</font>|<font size = "3">500€</font>|Milieu de Gamme|
|🥉|<font size = "3">RX 6500</font>|<font size = "3">300€</font>|Bas de Gamme|   

![Carte graphique Amd](image/amd%20cg.jpeg)

??? warning "RECOMMANDATION" 
    La carte graphique recommandée est la RX 6600 et non ~~la RX 6600 XT~~ , pour des choix un peu plus modestes il est reccomandé de prendre la RX 570 qui est très polyvalente ou encore de prendre un processeur avec un côté graphique dédié !

[Site AMD sponsorisé](https://www.amd.com){.md-button}


[^1]: AMD a été créé en 1969, c'est l'une des plus grandes entreprises informatiques en concurence avec Intel !

---

# La société Intel

!!! danger "Introduction"

        `Intel est une entreprise nord-américaine spécialisée dans la fabrication de microprocesseurs et de semi-conducteurs. Elle a été fondée en 1968 par trois hommes : Andrew Grove, Robert Noyce et Gordon Moore. C'est à ce dernier que l'on doit la fameuse « loi de Moore » énoncée en 1965.Résultat de recherche d'images pour "qu'est ce que intel" Le processeur est le cerveau de l'ordinateur, c'est lui qui organise les échanges de données entre les différents composants (disque dur, mémoire RAM, carte graphique) et qui fait les calculs qui font que l'ordinateur interagit avec vous et affiche votre système à l'écran. Elle fabrique des microprocesseurs — c'est d'ailleurs elle qui a créé le premier microprocesseur x86 —, des cartes mères, des mémoires flash et des processeurs graphiques notamment.`


## Les processeurs Intel[^2]

!!! info "Les _caractéristiques_ du processeur"

    - nombre de coeurs
    - nombre de threads
    - la cadence
    - mémoire cache

|:trophy:|**Nom**|**Prix**|**Qualité**|
|-|-----------|-------------|-------------|                                                     
|🥇|<font size = "3">i9 12900K</font>|<font size = "3">710€</font>|Haut de Gamme|
|🥈|<font size = "3">i5 12 400F</font>|<font size = "3">200€</font>|Milieu de Gamme|
|🥉|<font size = "3">i3 10 100F</font>|<font size = "3">100€</font>|Bas de Gamme|

![Le processeur Intel](image/intel_process.jpeg)

## Les catégories de carte graphique (NVIDIA)[^3]

!!! note "Les _caractéristiques_ d'une carte graphique"

        - la mémoire
        - son processeur graphique "GPU"

|:trophy:|**Nom**|**Prix**|**Qualité**|
|-|-----------|-------------|-------------|                                                     
|🥇|<font size = "3">RTX 3090</font>|<font size = "3">2 400€</font>|Haut de Gamme|
|🥈|<font size = "3">GTX 1080</font>|<font size = "3">500€</font>|Milieu de Gamme|
|🥉|<font size = "3">GTX 560 Ti</font>|<font size = "3">100€</font>|Bas de Gamme|

![Carte Graphique NVIDIA](image/nvidia.jpeg)                                                  ![Carte graphique NVIDIA](image/nvidia2.jpeg)

??? warning "RECOMMANDATION" 
    La carte graphique recommandée est la RTX 3080 et non ~~la RTX 3090~~ , pour des choix un peu plus modestes il est reccomandé de prendre la RTX 2060 qui est très polyvalente ou encore sa petite soeur la GTX 1060 !

[^2]: Intel a été créé en 1974, c'est l'une des plus grandes entreprises informatiques !
[^3]: NVIDIA a été créé en 1993 ! Connu pour être la plus grande entreprise graphique !!! [Site NVIDIA](https://www.nvidia.fr)

[Site Intel sponsorisé](https://www.intel.fr){.md-button}

A VENIR ...