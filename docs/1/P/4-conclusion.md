#🎩
## Et voilà 🎉

!!! tldr "Enfin ‼️"

    ![a](images/downhoho.gif){ width="200", align=right }

    **A présent vous savez tout pour vous lancer 🎊, l'aventure ne fait que commencer !**  
    Le monde de la programmation vous attend alors n'hésitez pas et n'abandonnez rien.
    
    --> ^^revoir^^ :

    1. [langages de programmation ⬅️](1-index.md) 
    2. [langages les plus utilisés ⬅️](2-most_used_langage.md)
    3. [Apprendre à coder ⬅️](3-learn_to_code.md)

---
> **_NOTE:_** Travail à faire pour le mardi 17 mai
