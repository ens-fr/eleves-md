# La face cachée de _So la Lune_ 🌑


 Avec sa voix **atypique** , sa maîtrise des mélodies, et ses textes **torturés**, So La Lune est l’un des rappeurs les plus talentueux et _intrigants_ du moment. Portrait d’un artiste en pleine ascension.
    
---

![S/O la lune](https://cdn.radiofrance.fr/s3/cruiser-production/2022/01/92762a2a-d2c0-4e9b-8013-c1a664b75074/801x410_maxresdefault.jpg)

---
## Introduction du rappeur 🌕

 Avec    7 projets consécutifs publiés entre mars et décembre, _So La Lune_ a marqué l’année 2021 de son empreinte. Cette stratégie d’occupation constante du terrain a porté ses fruits, tant l’intérêt autour de sa musique et de son univers est allé crescendo ces derniers mois. Signe que le rappeur a le vent en poupe, il a été récemment sélectionné par Booska-P[^BP] dans sa désormais célèbre liste des 11 rappeurs à suivre , un signe encourageant, puisque celle-ci a vu certains de ses jeunes espoirs, comme _Maes_ [^maes], _Kofs_ [^Kofs]ou _DA Uzi_[^Da], devenir des têtes d’affiche _confirmées_.
 [^BP]:média rap de renommée , [(voir plus)](https://www.booska-p.com/)
 [^maes]:rappeur français celèbre , pour plus d'information [cliquez ici](https://fr.wikipedia.org/wiki/Maes_(rappeur)).
  [^Kofs]: Rappeur marseillais en plus essort ectuellement , [(voir plus)](https://fr.wikipedia.org/wiki/Kofs) 
  [^Da]:Rappeur français , [(voir plus)](https://fr.wikipedia.org/wiki/Da_Uzi).
  
 
---

## Pourquoi cela fonctionne pour lui ? 🌗
- Avec son statut d’artiste sur la rampe de lancement, _So La Lune_ **intrigue** forcément, d’autant que son empreinte artistique est très caractéristique. Alors que le rap français tend parfois un peu trop à _l’uniformisation_, avec des tendances et des sonorités communes à une majorité de projets,
 - la proposition de So La Lune ne peut qu’attirer l’attention. Évidemment, produire quelque chose de différent ne suffit pas à garantir un résultat intéressant. Se démarquer nécessite à la fois d’avoir les bons éléments en main pour sortir du lot, mais aussi de trouver le bon angle, de développer un univers qui aille de paire avec la direction artistique choisie.

---

## La voix, un élément à double tranchant🌘

| ![le rappeur S\o la lune](https://t2.genius.com/unsafe/234x234/https%3A%2F%2Fimages.genius.com%2Fd6fb11dd5936a2e798fd5267f93be4f2.1000x1000x1.jpg)|Le cas de _So La Lune_ est particulier, car l'élément qui frappe le plus à la première écoute, sa voix, pourrait suffire à créer le décalage avec le reste du plateau. Nasillarde, éraillée, elle tend vers les aigus avec une sensation constante de déchirement vocal. Extrêmement particulière, et tout à fait naturelle, cette voix à la signature si reconnaissable est à la fois un atout formidable et un frein potentiel pour le rappeur. Ce timbre très atypique est le meilleur moyen de laisser une trace dans l’esprit de l’auditeur, qui sera forcément interpellé : impossible de ne pas le remarquer sur un featuring ou de le confondre avec un autre artiste.|![s/o la lune](https://raplume.eu/wp-content/uploads/2021/06/https___images.genius.com_0fff5e39259b16afebd8110b8b36dd11.1000x1000x1.jpg)|
|:-------|:------:|------:|

## Conséquences ? 🌑

 - Le revers de la médaille, c’est évidemment la réaction de l’auditeur. Certains accrochent, d’autres hésitent mais poussent l’écoute pour se faire un avis plus définitif ; malheureusement, d’autres peuvent être rebutés par cette voix qui sort de l’ordinaire. On ne peut évidemment pas leur en vouloir : dans la musique, l’empreinte vocale d’un chanteur est un élément trop central pour pouvoir être ignoré. Celle de So La Lune ne se contente pas de sortir du lot : elle est faite de nombreuses aspérités et de véritables fissures, ce qui peut faire obstacle à l’accessibilité globale de son œuvre.
 - Tremplin et barrière, cette *tessiture* vocale aurait pu suffire à faire de _So La Lune_ un rappeur au style à part. Il aurait en effet très bien pu se contenter d’une production générique, de titres déjà entendus des centaines de fois, puisque sa voix lui aurait permis de se démarquer et d’aboutir à une proposition originale sans trop d’efforts.
 
!!! warning "Un artiste spécial"
    **Seulement , _So La Lune_ est un artiste avec une vision complète, poussé par l’envie d’imposer son propre univers, lui aussi très singulier.**

---

## Un rappeur qui fait cohabiter les contraires 🌒

![le rappeur s/o la lune](https://generations.fr/media/video/thumb/870x489_6196561acafb4-dso.webp)

---

Elément le plus évident avant même de se lancer dans l’écoute de sa musique, la figure de _la Lune_ est omniprésente chez le rappeur. Présent jusque dans son pseudonyme, notre satellite naturel a donné son nom aux cinq premiers projets de sa discographie : _Tsuki_ (la Lune, en japonais), _Théia_ (la protoplanète qui aurait percuté la Terre il y a 4 milliards pour donner naissance à la Lune), Satellite Naturel, Orbite, et Apollon 11 (le premier vol habité vers l’astre). Les explications sont multiples : il y a évidemment les origines de So, rappeur issu de la diaspora comorienne [^DC], l’archipel étant surnommées “les îles de la Lune” (bien que l’origine de cette appellation soit sujette à débat).Ce n’est pas tout : dans l’imaginaire collectif, l’astre lunaire, en opposition au soleil, représente la nuit. Dans l’univers de So, tout _ou presque_ se déroule en effet entre le _crépuscule et l’aube_. Quand le monde extérieur s’endort, So La Lune voit son **propre univers** prendre vie : “_je suis plus un mec qui vit la nuit, j’ai vraiment une vie nocturne. C’est ce que j’essaye de faire ressortir dans ma musique, parce que c’est ce que je vis au quotidien_”, raconte-t-il ainsi au site Dans _Ta Face B_ au cours de l’une de ses rares interviews. En décalage horaire avec la majorité de la société humaine, So La Lune peut laisser libre court à son imagination et à ses démons.
 [^DC]:La diaspora comorienne en France regroupe la part des personnes originaires de l'Union des Comores ayant émigré sur le territoire français [(voir plus)](https://www.mouv.fr/musique/rap-fr/la-longue-histoire-d-amour-entre-les-comores-et-le-rap-francais-367749)


|![s/o la lune](https://images.genius.com/68a673d365b35ee0c66a806666f96456.660x660x1.png)|Dans son esprit, que l’on sent tiraillé entre un idéal de sérénité et une réalité plus chaotique, le rappeur fait cohabiter les émotions contradictoires. A travers cette recherche permanente d’équilibre, le contraste entre les différents sentiments apparaît parfois comme vertigineux, la mélancolie profonde de certains textes contrastant avec une interprétation beaucoup plus légère en apparence. Très porté sur la mélodie, So La Lune conjugue des éléments que l’on aurait plutôt tendance à opposer : on remarque ce contraste entre le propos globalement sombre et les éclaircis de positivité, mais aussi l’énergie qui se dégage de certains titres malgré la nonchalance palpable du rappeur.|
|:------|------:|

## Entre autodestruction et recherche du bonheur 🌓

So La Lune est le type de rappeur chez qui l’écriture pourrait être considérée comme un palliatif à la _psychanalyse_. Il se raconte énormément, et surtout, n’hésite jamais à mettre le doigt sur ses propres failles. Entre **autodestruction** par l’abus d’alcool ou de fumette, et démons intérieurs qui le rongent inlassablement, So a beaucoup de mal à entrevoir la lumière, et à savoir quel chemin emprunter pour se sortir de ce cycle dissolu. “J’le sens, j’vais finir devant la clinique, ambiance sinistre”, avoue-t-il dans Bonheur. La recherche de la sérénité intérieure semble vaine, mais elle a le mérite de donner un sens à une vie qui avance dans le noir.

A défaut d’être salvatrice, la musique a le même type de vertu : en se racontant, en explorant les méandres de ses pensées pour les raconter sur disque, So La Lune s’offre la possibilité de prendre de la hauteur sur lui-même. A travers l’introspection permanente qui constitue le socle de son œuvre, il explore sa propre face cachée en même temps qu’il nous la dévoile. Si son esprit est parfois très loin au-dessus des nuages (les références à la lune), ses textes ne sont finalement que l’expression immédiate de son quotidien, de ses vices, et de ce qui l’empêche justement de s’élever : “j'drop un projet où j'te parle de la juge, de comment j'me détruis le foie”.

---

## Evolution et futur du rappeur 🌕

!!! done "Après avoir beaucoup occupé le terrain ces derniers mois, So La Lune est passé du statut d’artiste confidentiel à celui de rappeur attendu. S’il refuse d’être considéré comme un rookie, son parcours n’étant pas celui d’une jeune pousse toute fraîche, il est sans aucun doute l’un des noms à suivre en ce début d’année 2022. Le choix d'enchaîner des publications de formats courts à un rythme soutenu s’est avéré très pertinent dans le cadre d’une montée en puissance progressive. Enfin placé sur orbite, So La Lune a enfin toutes les cartes en main pour devenir incontournable et marqué le rap français de sa plume et de sa voix atypique ."

!!! hint "un potentiel prometteur ?" 
     So La Lune fait parti de ces Freshmen du rap francophone, avec un potentiel énorme. Il sait faire preuve de polyvalence dans ses flows qu’il met au service de ses récits. Même si on peut penser que ses thèmes sont des marronniers du rap aujourd’hui. Mais le mélange de ces thèmes, de cette voix, de cette sensibilité, cette écriture sans fioritures et cette interprétation donne à So La Lune cette couleur unique au sein des rookies de la planète rap.

---
![SO la lune](https://www.abcdrduson.com/wp-content/uploads/2021/10/SoLaLune1.jpg)
## Où retrouver s/o la lune ? 🔎

Il est possible de retrouver s/o la lune sur [sa page youtube](https://www.youtube.com/channel/UCTGKo4bqLYXjB78f9VCzGNg) ( vous y retrouverez ses clips ainsi que certaines de ses musiques ) , pour ecouter l'entiereté se ses projets , il est possible d'écouter _s/o la lune_ sur toutes les platformes de streaming

- [deezer](https://www.deezer.com/fr/){ .md-button } 
- [spotify](https://www.spotify.com/fr/){ .md-button } 
- [apple music](https://www.apple.com/fr/apple-music/){ .md-button }
- et toutes celles que vous connaissez ...

??? tip "Quelques recommandations personnelles"
    Personnellement je vous conseille d'écouter [_Fissure de vie 23_](https://www.youtube.com/watch?v=1fwv4J6C3O4) pour tenter une premiere approche envers le rappeur , c'est un morceau où il rappe plus qu'il ne chante ( ce qui n'est pas le cas de tous les morceaux de So la lune ) puis cela vous permettra une premiere approche avec son timbre de voix (qui peut étonner à premier abord mais qui s'avere être l'étincelle qui fait briller son art) et son univers ( aussi abyssal qu'occulte )
    Une fois la premiere approche faite , je vous conseille d'écouter [_Tsukithèse_](https://www.youtube.com/watch?v=7LLzFPkfXYk) .Dans ce morceau So la lune laisse s'exprimer sa voix et en dévoile plus sur son train de vie et ses emotions , on y découvre son univers et son art plus en détail .
    
    ---

    Une fois ces deux écoutes faites , si vous êtes comme moi interressé par ce rappeur , ses anciens morceaux ou ceux à venir , je vous invite à écouter le reste de sa discographie dont je vous fais l'éloge par avance 

    ---

    (pour les amoureux de rap je vous conseille aussi [_Éternelle_](https://www.youtube.com/watch?v=ATLCmEF4aY0) , [_Lune City_](https://www.youtube.com/watch?v=3EXNiZSnQqo) , [_Billy_](https://www.youtube.com/watch?v=6sO3Fnw8esI) , [_Malade_](https://www.youtube.com/watch?v=tYIwHP_Qa6s) et [_Bonhomme de neige_](https://www.youtube.com/watch?v=xbOr4Sdg_44) )



??? help " Vous avez aimé cet article et souhaitez nous soutenir et lire plus de nos article ? "

     ++"Pour nous suivre , ne manquez aucun article"+"et être à jour avec toutes les nouveautés musicales du moment en temps réel"++
     [cliquez ici](https://raplume.eu/){ .md-button .md-button--primary }




---

## Script python sans lien avec ma page 🐍 

```python linenums="1"

x = 0
for _ in range (16) :
    print("ça ne sert absolument à rien")    
    x += 1

```
## Les Lois de Newton 🥼


=== "Première loi de Newton 📚 "
    !!!info "Première loi de Newton" 
        La première loi de Newton, ou le principe d'inertie, indique que tout corps conservera son état de repos ou de mouvement uniforme en ligne droite dans lequel il se trouve, à moins qu'une force ne soit appliquée sur ce corps.
        Cette affirmation signifie qu’une bille se déplaçant de façon linéaire sur le sol continuera à rouler en ligne droite à l’infini à moins qu’une force n’agisse sur elle. Aussi, une bille au repos ne se déplacera pas tant qu'une force n'agira pas sur elle.

    Évidemment, dans la réalité, la bille qui se déplace finira par s’arrêter en raison de la force de frottement entre la bille et le sol, force qui s’oppose au mouvement de la bille. Sans cette force, la bille n’arrêterait jamais son mouvement uniforme en ligne droite. Cette même bille lancée dans l’espace, en absence de résistance de l’air ou de frottement, conserverait son mouvement à l’infini.

    Pour garder son immobilité ou sa vitesse constante, il est possible qu’un objet soit soumis à plusieurs forces. Cependant, la somme de toutes les forces qui agissent sur lui doit être égale à zéro pour que l’objet garde son immobilité ou sa vitesse constante. C'est le principe d'inertie qui décrit cette idée.

    ![Première loi de Newton](https://cms.alloprof.qc.ca/sites/default/files/styles/1920w/public/2020-08/image_17.jpg?itok=38znOwVQ)

    Toutefois, si la somme des forces n'est pas nulle, la force résultante provoquera une accélération de l'objet. Ce dernier ne se déplacera donc pas à vitesse constante. 

    ![Première loi de Newton](https://cms.alloprof.qc.ca/sites/default/files/styles/1920w/public/2020-08/p1088i3.jpg?itok=hTKQTz7R)
=== "Deuxième loi de Newton 🎓 "
    !!!info "Deuxième loi de Newton" 
        La deuxième loi de Newton, ou principe fondamental de la dynamique, mentionne qu'une force résultante exercée sur un objet est toujours égale au produit de la masse de cet objet par son accélération. De plus, l'accélération produite et la force résultante ont la même orientation.

    La deuxième loi de Newton se résume par l'application de l'équation suivante:

    ---
     $\overrightarrow{F}_R = m \times \vec a$
     ---
      $\overrightarrow{F}_R$ représente la force résultante (N)
     --- 
     $m$ représente la masse de l'objet ($\text{kg}$)
     ---
     $\vec a$ représente l'accélération de l'objet($\text N/ \text kg$ ou $\text m/\text s^2$)
=== "Troisième loi de Newton 🌀"
    !!!info "Troisième loi de Newton" 
        La troisième loi de Newton explique le principe d'action-réaction: lorsqu’un corps A exerce une force sur un corps B, le corps B exercera une force sur le corps A de même grandeur, mais dans le sens opposé
    Si une personne pousse sur un mur, elle exerce une force sur le mur. Or, en même temps, le mur exerce une force de grandeur équivalente, mais de sens opposé à la force exercée par la personne. Bien que cette force ne soit pas nécessairement simple à visualiser, il suffit d'imaginer cette même personne en train d'exercer cette force sur le mur alors qu'elle est debout sur une planche à roulettes. Si elle pousse sur le mur, elle se déplacera en s'éloignant du mur, car le mur a exercé une force sur cette personne.
 
    ---

    Lorsqu'on dépose un livre sur une table, le livre exerce une force vers le bas. La table exerce une force vers le haut de même grandeur.
    
    ---
    ![exemple d'application de cette loi](https://cms.alloprof.qc.ca/sites/default/files/styles/1920w/public/2020-08/image_15.jpg?itok=kS3lyWXc)
    ---
    Le principe est similaire pour une personne qui marche. Cette personne exerce une force sur le sol. Le sol, en réaction à cette force, exerce une force sur la personne. Or, ce n'est pas la force d'un être humain qui fait en sorte qu'elle avance: si elle avait les pieds dans le vide et qu'elle appliquait la même force, elle ne pourrait pas se déplacer. C'est donc le sol qui permet à une personne de marcher grâce à la force de réaction.

    La loi d'action-réaction s'applique également pour les nageurs. Si un nageur veut se déplacer selon une direction et un sens donnés, il doit exercer une poussée avec ses bras et ses jambes dans le sens opposé. Dans ce cas, ses bras ou ses jambes exerceront une force dans un sens, alors que l'eau exerce une force en sens opposé
       
  
---

##  🌏 Quelques verbes irréguliers en anglais sans lien avec ma page 🌏



| Racine | Prétérit | Participe passé| Traduction| Image |
| -------- | -------- | -------- | -------- | ------ |
| spill     | spilt     | spilt     |  renverser| ![tasse renversée](https://previews.123rf.com/images/ifong/ifong1406/ifong140600005/29651958-tasse-de-renverser-du-caf%C3%A9-noir-avec-splash.jpg)
| stand     | stood     | stood     |  être debout| ![bonhomme debout](https://www.educol.net/image-etre-debout-dm14857.jpg)|
| steal     | stole     | stolen     |  dérober| ![voleur](https://i.ytimg.com/vi/r-V8mGyFpik/maxresdefault.jpg) |
| strike     | struck     | struck     |  frapper| ![un bonhomme frappe un autre bonhomme](https://thumbs.dreamstime.com/z/frapper-l-ic%C3%B4ne-%C3%A9l%C3%A9ment-de-combat-pour-le-concept-et-mobiles-d-applis-web-ligne-mince-la-conception-site-d%C3%A9veloppement-app-134540399.jpg) |
| swear      | swore     | sworn     |  jurer| ![Un homme jure](https://webservices.wkf.fr/editorial/medias/images/actu-105512-convictions-religieuses-.jpg)|
| sweep     | swept     | swept     |  balayer | ![un balais](https://aio.caqe.com/img/large/2919_180724-voulez-balayer-salete-tas.jpg) |
| swim     | swam     | swum     |  nager| ![nageur](https://us.123rf.com/450wm/artinspiring/artinspiring1610/artinspiring161000261/63730488-isolated-ic%C3%B4ne-de-bain-silhouette-noire-de-l-homme-nage-dans-les-vagues-concept-de-la-piscine-la-com.jpg?ver=6)|
| take     | took     | taken     |  prendre| ![bonhomme qui prend un carton](https://www.ricochet-jeunes.org/sites/default/files/lapointeart6-7.jpg)|
| teach     | taught     | taught     |  enseigner| ![enseignant](https://i.la-croix.com/1400x933/smart/2019/11/13/1201060070/premier-degre-enseignant-debutant-gagne-9-moins-moyenne-lOCDE_0.jpg)|
| tear     | tore     | torn     |  déchirer| ![page dechirée](https://i.ytimg.com/vi/YyidaMW3AMw/maxresdefault.jpg)|
| tell     | told     | told     |   raconter |![une mere raconte une histoire à sa fille](https://www.educatout.com/images/medium/Raconter-une-histoire-en-images-FB.jpg) |
| think     | thought     | thought     |  penser| ![une femme réfléchit](https://st.depositphotos.com/2069237/2453/i/600/depositphotos_24538213-stock-photo-woman-thinking-blackboard.jpg)|
| throw     | threw     | thrown     |  lancer|![un garçon jette une pierre](https://previews.123rf.com/images/bigandt/bigandt1509/bigandt150900052/47169795-boy-jeter-la-pierre-dans-la-rivi%C3%A8re-sur-une-journ%C3%A9e-d-%C3%A9t%C3%A9-ensoleill%C3%A9e-en-norv%C3%A8ge-.jpg) |
| understand     | understood     | understood     |  comprendre|![reflexion](https://i1.wp.com/apprendre-reviser-memoriser.fr/wp-content/uploads/2018/06/comprendre-m%C3%A9canismes-attention-enfants.png?resize=1024%2C768) |
| wake     | woke     | woken     |  reveiller|![Une femme se reveille](https://avogel.fr/blog/wp-content/uploads/2020/09/guide-se-reveiller-en-forme-1580x770.jpg) |
| wear     | wore     | worn     |  porter | ![Bonhomme qui porte un carton](https://previews.123rf.com/images/6kor3dos/6kor3dos1210/6kor3dos121000025/15870259-personne-3d-porte-une-bo%C3%AEte-en-carton-scell%C3%A9e.jpg)|
| win     | won     | won     |  gagner|![bonhomme avec une coupe](https://fotomelia.com/wp-content/uploads/edd/2015/11/images-et-photos-gratuites-libres-de-droits-t%C3%A9l%C3%A9chargement-gratuits193-1560x1560.jpg) |
| write     |  wrote     | written     |  écrire| ![Une femme qui écrit](https://media.istockphoto.com/photos/writing-in-notebook-closeup-picture-id1080259016?k=20&m=1080259016&s=612x612&w=0&h=sgVODsfYPU_oDWeDkfJZLgwEu6u95dP_6EQzlfmMX8E=)|










