# Travaux d'élèves 2022

Devoirs maison de création web en utilisant des technologies variées autour de MkDocs.


## Premières

Après deux séances de découvertes (Markdown, puis MkDocs), les élèves avaient pour consignes de créer le site web de leur choix, dans le respect du règlement intérieur. Leur créativité est parfois époustouflante !!! Certains élèves ont fait des recherches poussées pour utiliser des techniques avancées.

- [L'animation](1/A/Animation.md) 👍👍👍👍👍
    - un travail remarquable rempli d'âme,
    - une technique utilisée à bon escient.
- [Langages de programmation](https://okonore.github.io/languages_programmation/) 👍👍👍👍👍 ; :warning: Site web autonome :warning:
    - Un travail remarquable avec une technique très avancée,
    - en Français et en Anglais,
    - avec intégration continue et moteur de blog.
- [Wiki _Elden Ring_](1/B/Home.md) 👍👍👍👍👍
    - Un travail conséquent avec de nombreuses solutions techniques avancées
- [Alan Turing](1/S/index_1.md) 👍👍👍👍👍
    - Un travail particulièrement soigné de recherche
- [Genshin Impact, c'est quoi ?](1/D/pre%C2%82sentation.md) 👍👍👍👍
- [Blender](1/H/1-index.md) 👍👍👍👍
- [La programmation](1/P/1-index.md) 👍👍👍
- [Escalade](1/I/page1.md) 👍👍👍
- [Le système solaire](1/Z/index.md) 👍👍👍
- [Site Marvel](1/F/index.md) 👍👍👍
- [R-Markdown](1/O/1-markdown.md) 👍👍
- [Ingénieur](1/J/index.md) 👍👍
- [Histoire du Japon](1/C/index2_.md) 👍👍
- [Matériel informatique](1/Q/1-amd_society.md) 👍
- [Sophrologie](1/L/index.md) 👍
- [Langages de programmation](1/G/index.md) 👍
- [Minecraft](1/R/1-index.md) 👍
- [Le panda géant](1/V/panda-geant.md) 👍
- [Muse](1/W/index.md) 👍
- [So la Lune](1/X/index.md) 👍
- [Métiers de l'informatique](1/Z1/index.md) 👍
- [Solutions d'un Rubik's cube](1/Z3/solution-d'un-rubiks-cube.md) 👍
- [Football](1/N/Football.md) 👍
- [Haltérophilie](1/K/index.md) 👍
- [Python](1/Y/index.md)
- [Markdown](1/E/Pr%C2%82sentation%20Markdown.md)
- [Voiture sous toutes ses formes](1/T/index.md)
- [Histoire de l'OM](1/U/index.md)
- [album](1/Z2/index.md)


## Terminales

Les élèves avaient fait un tel DM l'an passé en première et souhaitaient en refaire un ; cela leur avait beaucoup plu. Une séance de rappel a suffi pour revoir le fonctionnement de MkDocs.

- [Introduction à la langue finnoise](T/A/intro.md) 👍👍👍👍👍
    - Bravo pour ce travail qui préfigure des succès futurs
- [Intégration](T/K/integration.md), 👍👍👍
- [Géométrie dans l'espace](T/L/index.md), 👍👍👍
- [Probabilités](T/M/index.md), 👍👍
- [ADN et stockage informatique](T/B/index_0.md), 👍👍
- [Génération Minecraft](T/C/generation_minecraft.md), 👍👍
- [Au sujet del'IA](T/D/questIA.md), 👍
- [Spirale des nombres premiers](T/E/spirale-nb-premiers.md), 👍
- [Comment trier une liste ?](T/G/index.md), 👍
- [Au sujet de Markdown](T/F/index.md)
- [Satellites artificiels](T/H/index.md)
- [Découvrir Linux(Humour)](T/I/linux.md)
- [Colibri](T/J/index.md)
