def groupe(verbe : str) -> int: #

    if verbe[-2:] in term_g1 : return 1 #Groupe 1 -VA
    elif verbe[-2:] == 'da' or verbe[-2:] == 'dä' : return 2 #Groupe 2 -dA
    elif verbe[-3:] in term_g3 : return 3 #Groupe 3 -CCA/-stA
    elif verbe[-2:] == 'ta' or verbe[-2:] == 'tä' : #Groupe 4, 5 ou 6 -VtA
        if verbe[-3:] in term_g4 : return 4 #Groupe 4 -VtA -> V(A, O, U)
        elif verbe[-3:] == 'ita' or verbe[-2:] == 'itä' : return 5 #Groupe 5 -itA
        elif verbe[-3:] == 'eta' or verbe[-2:] == 'etä' : return 6 #Groupe 6 - etA
    

def gradation(verbe : str, groupe : int, gradation_dict : dict) -> str:

    
    
    return

def flexion_présent_rég(verbe : str, term_personnelles : list) -> str: #conjugaison

    if harmonie == 'ä': #Harmonie vocalique A
        term_personnelles[5] = 'vät'

    ###Régulier
    
    if groupe(verbe) == 1 :
        radical = verbe[:-1] #suppr -_A
        term_personnelles[2] = verbe[-2]
        
    if groupe(verbe) == 2 :
        radical = verbe[:-2] #suppr -dA

    if groupe(verbe) == 3 :
        radical = verbe[:-2] #suppr -CA
        radical += 'e'
        term_personnelles[2] = 'e'
    
    if groupe(verbe) == 4 :
        if verbe[-3:] == 'ata' or verbe[-3:] == 'ätä':
            radical = verbe[:-2]
            radical += harmonie
        else : 
            radical = verbe[:-2]
            radical += harmonie
            term_personnelles[2] = harmonie
        
    if groupe(verbe) == 5 :
        radical = verbe[:-2]
        radical += 'tse'
        term_personnelles[2] = 'e'
    
    if groupe(verbe) == 6 :
        radical = verbe[:-2]
        radical += 'ne'
        term_personnelles[2] = 'e'

    for terminaison in term_personnelles :
        print(radical + terminaison)

def flexion_présent_irr(verbe : str, term_personnelles : list) -> str:

    if harmonie == 'ä': #Harmonie vocalique A
        term_personnelles[5] = 'vät'

    pronoms = 0

    if verbe == 'Olla' : #G3 irr.3P

        term_personnelles[2] = 'n'

        for terminaison in term_personnelles :
            if pronoms == 2 or pronoms == 5 : print('O'+terminaison)
            else : print('Ole' + terminaison)
            pronoms += 1

    if verbe == 'Nähdä' :
        term_personnelles[2] = 'e'
        for terminaison in term_personnelles :
            if pronoms == 2 or pronoms == 5 : print('Näke'+terminaison)
            else : print('Näe' + terminaison)
            pronoms +=1

    if verbe == 'Tehdä' :
        term_personnelles[2] = 'e'
        for terminaison in term_personnelles :
            if pronoms == 2 or pronoms == 5 : print('Teke'+terminaison)
            else : print('Tee' + terminaison)
            pronoms +=1

    if verbe == 'Selvitä' :
        radical = verbe[:-2]
        radical += harmonie
        term_personnelles[2] = harmonie
        for terminaison in term_personnelles :
            print(radical + terminaison)

#Utilitaires

term_g1 = ["aa", "ää", "ea", "eä", "ia", "iä", "oa", "ua", "yä"]
term_g3 = ["nna", "nnä", "lla", "llä", "rra", "rrä", "sta", "stä"]
term_g4 = ["ata", "ätä", "ota", "ötä", "uta", "ytä"]

term_personnelles = ['n', 't', '', 'mme', 'tte', 'vat']
verbe_irr = ['Olla', 'Nähdä', 'Tehdä', 'Selvitä']

gradation_dict = {'kk' : 'k', 'pp' : 'p', 'tt' : 't', 
                  'nt' : 'nn', 'nk' : 'ng', 'mp' : 'mm',
                  'lt' : 'll', 'rt' : 'rr',
                  't' : 'd', 'k' : '', 'p' : 'v',
                  'lk' : 'lj', 'rk' : 'rj'}

#Entrée
verbe = input('Entrée (Pensez à la majuscule) : ')
harmonie = verbe[-1]
if verbe in verbe_irr : flexion_présent_irr(verbe, term_personnelles)
else : flexion_présent_rég(verbe, term_personnelles)

