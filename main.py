import os

def define_env(env):
    "Hook function"

    @env.macro
    def script(lang: str, nom: str, indentation=0, stop="") -> str:
        """Renvoie le script dans une balise bloc avec langage spécifié

        - lang: le nom du lexer pour la coloration syntaxique
        - nom: le chemin du script relativement au .md d'appel
        - indentation: nb d'espaces pour l'insertion dans un environnement
        - stop: si cette ligne est rencontrée, elle n'est pas affichée, ni la suite
        """
        # par Franck Chambon
        sortie = []
        with open("docs/" + os.path.dirname(env.variables.page.url.rstrip('/')) + f"/{nom}", 'r') as f:
            for line in f.readlines():
                if line.upper().strip() == stop:
                    break
                nb_agc = 0 # nb accents graves consécutifs
                maxi_agc = 0
                for c in line:
                    if c == '`':
                        nb_agc += 1
                        if nb_agc > maxi_agc: maxi_agc = nb_agc
                    else:
                        nb_agc = 0
                sortie.append(" " * indentation + line)
        n = max(3, 1 + nb_agc)
        sortie = ["`" * n + lang + "\n"] + sortie
        sortie.append(" " * indentation + "`" * n + "\n")
        return "".join(sortie)


    @env.macro
    def py(nom: str, indentation=0, stop="") -> str:
        "macro python rapide"
        return script('python', nom + ".py", indentation, stop)
